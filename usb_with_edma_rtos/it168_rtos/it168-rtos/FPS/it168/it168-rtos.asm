
it168-rtos.elf:     file format elf32-littleriscv


Disassembly of section .init:

00000000 <reset_vector>:
   0:	342022f3          	csrr	t0,mcause
   4:	00028463          	beqz	t0,c <_start>
   8:	0dc0006f          	j	e4 <nmi_handler>

0000000c <_start>:
   c:	00006197          	auipc	gp,0x6
  10:	35018193          	addi	gp,gp,848 # 635c <__global_pointer$>
  14:	000a0297          	auipc	t0,0xa0
  18:	fec28293          	addi	t0,t0,-20 # a0000 <_stack>
  1c:	8116                	c.mv	sp,t0
  1e:	00005297          	auipc	t0,0x5
  22:	69e28293          	addi	t0,t0,1694 # 56bc <_ITB_BASE_>
  26:	80029073          	csrw	uitb,t0
  2a:	00000297          	auipc	t0,0x0
  2e:	02628293          	addi	t0,t0,38 # 50 <trap_entry>
  32:	30529073          	csrw	mtvec,t0
  36:	00000097          	auipc	ra,0x0
  3a:	160080e7          	jalr	352(ra) # 196 <__platform_init>
  3e:	00000097          	auipc	ra,0x0
  42:	0aa080e7          	jalr	170(ra) # e8 <reset_handler>
  46:	a001                	c.j	46 <_start+0x3a>
  48:	8082                	c.jr	ra

Disassembly of section .text:

00000050 <trap_entry>:
      50:	fe512e23          	sw	t0,-4(sp)
      54:	342022f3          	csrr	t0,mcause
      58:	0002db63          	bgez	t0,6e <trap_entry+0x1e>
      5c:	0ff2f293          	andi	t0,t0,255
      60:	12e5                	c.addi	t0,-7
      62:	00029663          	bnez	t0,6e <trap_entry+0x1e>
      66:	ffc12283          	lw	t0,-4(sp)
      6a:	7e30406f          	j	504c <FreeRTOS_MTIMER_Handler>
      6e:	ffc12283          	lw	t0,-4(sp)
      72:	7119                	c.addi16sp	sp,-128
      74:	c006                	c.swsp	ra,0(sp)
      76:	c212                	c.swsp	tp,4(sp)
      78:	c416                	c.swsp	t0,8(sp)
      7a:	c61a                	c.swsp	t1,12(sp)
      7c:	c81e                	c.swsp	t2,16(sp)
      7e:	ca2a                	c.swsp	a0,20(sp)
      80:	cc2e                	c.swsp	a1,24(sp)
      82:	ce32                	c.swsp	a2,28(sp)
      84:	d036                	c.swsp	a3,32(sp)
      86:	d23a                	c.swsp	a4,36(sp)
      88:	d43e                	c.swsp	a5,40(sp)
      8a:	d642                	c.swsp	a6,44(sp)
      8c:	d846                	c.swsp	a7,48(sp)
      8e:	da72                	c.swsp	t3,52(sp)
      90:	dc76                	c.swsp	t4,56(sp)
      92:	de7a                	c.swsp	t5,60(sp)
      94:	c0fe                	c.swsp	t6,64(sp)
      96:	341022f3          	csrr	t0,mepc
      9a:	30002373          	csrr	t1,mstatus
      9e:	c296                	c.swsp	t0,68(sp)
      a0:	c49a                	c.swsp	t1,72(sp)
      a2:	34202573          	csrr	a0,mcause
      a6:	858a                	c.mv	a1,sp
      a8:	2079                	c.jal	136 <trap_handler>
      aa:	300477f3          	csrrci	a5,mstatus,8
      ae:	4296                	c.lwsp	t0,68(sp)
      b0:	4326                	c.lwsp	t1,72(sp)
      b2:	34129073          	csrw	mepc,t0
      b6:	30031073          	csrw	mstatus,t1
      ba:	4082                	c.lwsp	ra,0(sp)
      bc:	4212                	c.lwsp	tp,4(sp)
      be:	42a2                	c.lwsp	t0,8(sp)
      c0:	4332                	c.lwsp	t1,12(sp)
      c2:	43c2                	c.lwsp	t2,16(sp)
      c4:	4552                	c.lwsp	a0,20(sp)
      c6:	45e2                	c.lwsp	a1,24(sp)
      c8:	4672                	c.lwsp	a2,28(sp)
      ca:	5682                	c.lwsp	a3,32(sp)
      cc:	5712                	c.lwsp	a4,36(sp)
      ce:	57a2                	c.lwsp	a5,40(sp)
      d0:	5832                	c.lwsp	a6,44(sp)
      d2:	58c2                	c.lwsp	a7,48(sp)
      d4:	5e52                	c.lwsp	t3,52(sp)
      d6:	5ee2                	c.lwsp	t4,56(sp)
      d8:	5f72                	c.lwsp	t5,60(sp)
      da:	4f86                	c.lwsp	t6,64(sp)
      dc:	6109                	c.addi16sp	sp,128
      de:	30200073          	mret
      e2:	a001                	c.j	e2 <trap_entry+0x92>

000000e4 <nmi_handler>:
      e4:	a001                	c.j	e4 <nmi_handler>
	...

000000e8 <reset_handler>:
      e8:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
      ea:	207d                	c.jal	198 <c_startup>
      ec:	28f9                	c.jal	1ca <system_init>
      ee:	212050ef          	jal	ra,5300 <__libc_init_array>
      f2:	10c050ef          	jal	ra,51fe <main>
      f6:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

000000f8 <_init>:
      f8:	8082                	c.jr	ra

000000fa <_fini>:
      fa:	8082                	c.jr	ra

000000fc <mtime_handler>:
      fc:	08000793          	li	a5,128
     100:	3047b7f3          	csrrc	a5,mie,a5
     104:	8082                	c.jr	ra

00000106 <mswi_handler>:
     106:	304477f3          	csrrci	a5,mie,8
     10a:	8082                	c.jr	ra

0000010c <syscall_handler>:
     10c:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     10e:	87ba                	c.mv	a5,a4
     110:	8736                	c.mv	a4,a3
     112:	86b2                	c.mv	a3,a2
     114:	862e                	c.mv	a2,a1
     116:	85aa                	c.mv	a1,a0
     118:	00006517          	auipc	a0,0x6
     11c:	8dc50513          	addi	a0,a0,-1828 # 59f4 <_ITB_BASE_+0x338>
     120:	8e10                	exec.it	#71     !jal	ra,3c36 <printf>
     122:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

00000124 <except_handler>:
     124:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     126:	862e                	c.mv	a2,a1
     128:	85aa                	c.mv	a1,a0
     12a:	00005517          	auipc	a0,0x5
     12e:	78250513          	addi	a0,a0,1922 # 58ac <_ITB_BASE_+0x1f0>
     132:	8e10                	exec.it	#71     !jal	ra,3c36 <printf>
     134:	a001                	c.j	134 <except_handler+0x10>

00000136 <trap_handler>:
     136:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
     138:	842e                	c.mv	s0,a1
     13a:	02055b63          	bgez	a0,170 <trap_handler+0x3a>
     13e:	780520db          	bfoz	ra,a0,30,0
     142:	00b0ef5b          	bnec	ra,11,160 <trap_handler+0x2a>
     146:	f1402373          	csrr	t1,mhartid
     14a:	e4200737          	lui	a4,0xe4200
     14e:	00470513          	addi	a0,a4,4 # e4200004 <_stack+0xe4160004>
     152:	00c31393          	slli	t2,t1,0xc
     156:	00a385b3          	add	a1,t2,a0
     15a:	4188                	c.lw	a0,0(a1)
     15c:	207d                	c.jal	20a <mext_interrupt>
     15e:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>
     160:	0070e45b          	bnec	ra,7,168 <trap_handler+0x32>
     164:	3f61                	c.jal	fc <mtime_handler>
     166:	bfe5                	c.j	15e <trap_handler+0x28>
     168:	0230e25b          	bnec	ra,3,18c <trap_handler+0x56>
     16c:	3f69                	c.jal	106 <mswi_handler>
     16e:	bfc5                	c.j	15e <trap_handler+0x28>
     170:	00b56e5b          	bnec	a0,11,18c <trap_handler+0x56>
     174:	5198                	c.lw	a4,32(a1)
     176:	4dd4                	c.lw	a3,28(a1)
     178:	4d90                	c.lw	a2,24(a1)
     17a:	5808                	c.lw	a0,48(s0)
     17c:	49cc                	c.lw	a1,20(a1)
     17e:	3779                	c.jal	10c <syscall_handler>
     180:	407c                	c.lw	a5,68(s0)
     182:	00478293          	addi	t0,a5,4
     186:	04542223          	sw	t0,68(s0)
     18a:	bfd1                	c.j	15e <trap_handler+0x28>
     18c:	406c                	c.lw	a1,68(s0)
     18e:	8622                	c.mv	a2,s0
     190:	3f51                	c.jal	124 <except_handler>
     192:	c068                	c.sw	a0,68(s0)
     194:	b7e9                	c.j	15e <trap_handler+0x28>

00000196 <__platform_init>:
     196:	8082                	c.jr	ra

00000198 <c_startup>:
     198:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
     19a:	00006417          	auipc	s0,0x6
     19e:	e0640413          	addi	s0,s0,-506 # 5fa0 <MEM_SIZE>
     1a2:	00006517          	auipc	a0,0x6
     1a6:	99e50513          	addi	a0,a0,-1634 # 5b40 <__data_lmastart>
     1aa:	40a40633          	sub	a2,s0,a0
     1ae:	00006597          	auipc	a1,0x6
     1b2:	99258593          	addi	a1,a1,-1646 # 5b40 <__data_lmastart>
     1b6:	8c20                	exec.it	#22     !jal	ra,5350 <memcpy>
     1b8:	00027617          	auipc	a2,0x27
     1bc:	0b860613          	addi	a2,a2,184 # 27270 <_end>
     1c0:	8e01                	c.sub	a2,s0
     1c2:	4581                	c.li	a1,0
     1c4:	8522                	c.mv	a0,s0
     1c6:	8430                	exec.it	#19     !jal	ra,5526 <memset>
     1c8:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>

000001ca <system_init>:
     1ca:	00000717          	auipc	a4,0x0
     1ce:	e3670713          	addi	a4,a4,-458 # 0 <MEM_BEGIN>
     1d2:	f01007b7          	lui	a5,0xf0100
     1d6:	cbb8                	c.sw	a4,80(a5)
     1d8:	7d0022f3          	csrr	t0,mmisc_ctl
     1dc:	0012fc5b          	bbc	t0,1,1f4 <system_init+0x2a>
     1e0:	e4000337          	lui	t1,0xe4000
     1e4:	438d                	c.li	t2,3
     1e6:	04000513          	li	a0,64
     1ea:	00732023          	sw	t2,0(t1) # e4000000 <_stack+0xe3f60000>
     1ee:	7d0527f3          	csrrs	a5,mmisc_ctl,a0
     1f2:	8082                	c.jr	ra
     1f4:	e4000337          	lui	t1,0xe4000
     1f8:	4385                	c.li	t2,1
     1fa:	b7f5                	c.j	1e6 <system_init+0x1c>

000001fc <default_irq_handler>:
     1fc:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     1fe:	00005517          	auipc	a0,0x5
     202:	78650513          	addi	a0,a0,1926 # 5984 <_ITB_BASE_+0x2c8>
     206:	8e10                	exec.it	#71     !jal	ra,3c36 <printf>
     208:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

0000020a <mext_interrupt>:
     20a:	8a04                	exec.it	#76     !csrrsi	a5,mstatus,8
     20c:	8e418793          	addi	a5,gp,-1820 # 5c40 <__preinit_array_start>
     210:	0ca782db          	lea.w	t0,a5,a0
     214:	0002a383          	lw	t2,0(t0)
     218:	02038563          	beqz	t2,242 <mext_interrupt+0x38>
     21c:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     21e:	1141                	c.addi	sp,-16
     220:	c62a                	c.swsp	a0,12(sp)
     222:	9382                	c.jalr	t2
     224:	4532                	c.lwsp	a0,12(sp)
     226:	f1402873          	csrr	a6,mhartid
     22a:	e4200e37          	lui	t3,0xe4200
     22e:	00c81893          	slli	a7,a6,0xc
     232:	004e0e93          	addi	t4,t3,4 # e4200004 <_stack+0xe4160004>
     236:	01d88f33          	add	t5,a7,t4
     23a:	00af2023          	sw	a0,0(t5)
     23e:	0141                	c.addi	sp,16
     240:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
     242:	f1402373          	csrr	t1,mhartid
     246:	e4200737          	lui	a4,0xe4200
     24a:	00c31593          	slli	a1,t1,0xc
     24e:	00470613          	addi	a2,a4,4 # e4200004 <_stack+0xe4160004>
     252:	00c586b3          	add	a3,a1,a2
     256:	c288                	c.sw	a0,0(a3)
     258:	8082                	c.jr	ra

0000025a <registerISR>:
     25a:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     25c:	8e418793          	addi	a5,gp,-1820 # 5c40 <__preinit_array_start>
     260:	0ca780db          	lea.w	ra,a5,a0
     264:	00b0a023          	sw	a1,0(ra)
     268:	2ad1                	c.jal	43c <isr_SetupIrq>
     26a:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

0000026c <uart_Open>:
     26c:	96018893          	addi	a7,gp,-1696 # 5cbc <uartDiv>
     270:	577d                	c.li	a4,-1
     272:	4801                	c.li	a6,0
     274:	87c6                	c.mv	a5,a7
     276:	0008a303          	lw	t1,0(a7)
     27a:	00b31363          	bne	t1,a1,280 <uart_Open+0x14>
     27e:	8742                	c.mv	a4,a6
     280:	0805                	c.addi	a6,1
     282:	08c1                	c.addi	a7,16
     284:	be78695b          	bnec	a6,7,276 <uart_Open+0xa>
     288:	55fd                	c.li	a1,-1
     28a:	08b71063          	bne	a4,a1,30a <uart_Open+0x9e>
     28e:	9a41ae03          	lw	t3,-1628(gp) # 5d00 <uartDiv+0x44>
     292:	9a81ae83          	lw	t4,-1624(gp) # 5d04 <uartDiv+0x48>
     296:	9ac1af03          	lw	t5,-1620(gp) # 5d08 <uartDiv+0x4c>
     29a:	920007b7          	lui	a5,0x92000
     29e:	055a                	c.slli	a0,0x16
     2a0:	01478313          	addi	t1,a5,20 # 92000014 <_stack+0x91f60014>
     2a4:	10000fb7          	lui	t6,0x10000
     2a8:	69cfa423          	sw	t3,1672(t6) # 10000688 <_stack+0xff60688>
     2ac:	00650733          	add	a4,a0,t1
     2b0:	02c78813          	addi	a6,a5,44
     2b4:	01e72023          	sw	t5,0(a4)
     2b8:	010502b3          	add	t0,a0,a6
     2bc:	02078593          	addi	a1,a5,32
     2c0:	08000893          	li	a7,128
     2c4:	0112a023          	sw	a7,0(t0)
     2c8:	00b503b3          	add	t2,a0,a1
     2cc:	0ffefe13          	andi	t3,t4,255
     2d0:	02478f13          	addi	t5,a5,36
     2d4:	01c3a023          	sw	t3,0(t2)
     2d8:	01e50fb3          	add	t6,a0,t5
     2dc:	008ede93          	srli	t4,t4,0x8
     2e0:	01dfa023          	sw	t4,0(t6)
     2e4:	068a                	c.slli	a3,0x2
     2e6:	0002a023          	sw	zero,0(t0)
     2ea:	4309                	c.li	t1,2
     2ec:	00368713          	addi	a4,a3,3
     2f0:	006fa023          	sw	t1,0(t6)
     2f4:	0ec7065b          	lea.d	a2,a4,a2
     2f8:	02878793          	addi	a5,a5,40
     2fc:	00c2a023          	sw	a2,0(t0)
     300:	953e                	c.add	a0,a5
     302:	4805                	c.li	a6,1
     304:	01052023          	sw	a6,0(a0)
     308:	8082                	c.jr	ra
     30a:	00471293          	slli	t0,a4,0x4
     30e:	005783b3          	add	t2,a5,t0
     312:	0043ae03          	lw	t3,4(t2)
     316:	0083ae83          	lw	t4,8(t2)
     31a:	00c3af03          	lw	t5,12(t2)
     31e:	bfb5                	c.j	29a <uart_Open+0x2e>

00000320 <uart_putc>:
     320:	920007b7          	lui	a5,0x92000
     324:	055a                	c.slli	a0,0x16
     326:	03478293          	addi	t0,a5,52 # 92000034 <_stack+0x91f60034>
     32a:	8240                	exec.it	#96     !add	t1,a0,t0
     32c:	00032703          	lw	a4,0(t1) # e4000000 <_stack+0xe3f60000>
     330:	be577e5b          	bbc	a4,5,32c <uart_putc+0xc>
     334:	920003b7          	lui	t2,0x92000
     338:	02038613          	addi	a2,t2,32 # 92000020 <_stack+0x91f60020>
     33c:	00c506b3          	add	a3,a0,a2
     340:	c28c                	c.sw	a1,0(a3)
     342:	8082                	c.jr	ra

00000344 <edma_Init>:
     344:	47d1                	c.li	a5,20
     346:	8224                	exec.it	#88     !mul	a0,a0,a5
     348:	8860                	exec.it	#52     !lui	t0,0x70000
     34a:	04828713          	addi	a4,t0,72 # 70000048 <_stack+0x6ff60048>
     34e:	04c28393          	addi	t2,t0,76
     352:	04428813          	addi	a6,t0,68
     356:	00e50333          	add	t1,a0,a4
     35a:	00b32023          	sw	a1,0(t1)
     35e:	007505b3          	add	a1,a0,t2
     362:	c190                	c.sw	a2,0(a1)
     364:	05028613          	addi	a2,t0,80
     368:	00c506b3          	add	a3,a0,a2
     36c:	0006a023          	sw	zero,0(a3)
     370:	010508b3          	add	a7,a0,a6
     374:	0008a023          	sw	zero,0(a7)
     378:	8082                	c.jr	ra

0000037a <edma_SetSrcParams>:
     37a:	48d1                	c.li	a7,20
     37c:	03150533          	mul	a0,a0,a7
     380:	8860                	exec.it	#52     !lui	t0,0x70000
     382:	05028313          	addi	t1,t0,80 # 70000050 <_stack+0x6ff60050>
     386:	07ba                	c.slli	a5,0xe
     388:	0822                	c.slli	a6,0x8
     38a:	0107ef33          	or	t5,a5,a6
     38e:	0746                	c.slli	a4,0x11
     390:	00ef6fb3          	or	t6,t5,a4
     394:	06d2                	c.slli	a3,0x14
     396:	00dfe8b3          	or	a7,t6,a3
     39a:	8474                	exec.it	#59     !add	t2,a0,t1
     39c:	00b3a023          	sw	a1,0(t2)
     3a0:	04428593          	addi	a1,t0,68
     3a4:	00b50e33          	add	t3,a0,a1
     3a8:	8640                	exec.it	#98     !lw	t4,0(t3)
     3aa:	065a                	c.slli	a2,0x16
     3ac:	00c8e533          	or	a0,a7,a2
     3b0:	01d562b3          	or	t0,a0,t4
     3b4:	005e2023          	sw	t0,0(t3)
     3b8:	8082                	c.jr	ra

000003ba <edma_SetDstParams>:
     3ba:	47d1                	c.li	a5,20
     3bc:	8224                	exec.it	#88     !mul	a0,a0,a5
     3be:	8860                	exec.it	#52     !lui	t0,0x70000
     3c0:	04428313          	addi	t1,t0,68 # 70000044 <_stack+0x6ff60044>
     3c4:	06b2                	c.slli	a3,0xc
     3c6:	0712                	c.slli	a4,0x4
     3c8:	00e6e8b3          	or	a7,a3,a4
     3cc:	0642                	c.slli	a2,0x10
     3ce:	00c8ee33          	or	t3,a7,a2
     3d2:	05ca                	c.slli	a1,0x12
     3d4:	00be6eb3          	or	t4,t3,a1
     3d8:	8474                	exec.it	#59     !add	t2,a0,t1
     3da:	0003a803          	lw	a6,0(t2)
     3de:	010eef33          	or	t5,t4,a6
     3e2:	01e3a023          	sw	t5,0(t2)
     3e6:	8082                	c.jr	ra

000003e8 <edma_Start>:
     3e8:	47d1                	c.li	a5,20
     3ea:	8224                	exec.it	#88     !mul	a0,a0,a5
     3ec:	8860                	exec.it	#52     !lui	t0,0x70000
     3ee:	04428313          	addi	t1,t0,68 # 70000044 <_stack+0x6ff60044>
     3f2:	05f6                	c.slli	a1,0x1d
     3f4:	0015e693          	ori	a3,a1,1
     3f8:	8474                	exec.it	#59     !add	t2,a0,t1
     3fa:	0003a603          	lw	a2,0(t2)
     3fe:	00c6e733          	or	a4,a3,a2
     402:	00e3a023          	sw	a4,0(t2)
     406:	8082                	c.jr	ra

00000408 <edma_Check_sts>:
     408:	700007b7          	lui	a5,0x70000
     40c:	5b88                	c.lw	a0,48(a5)
     40e:	8082                	c.jr	ra

00000410 <edma_clear_sts>:
     410:	67c1                	c.lui	a5,0x10
     412:	70000737          	lui	a4,0x70000
     416:	10178293          	addi	t0,a5,257 # 10101 <__global_pointer$+0x9da5>
     41a:	02572823          	sw	t0,48(a4) # 70000030 <_stack+0x6ff60030>
     41e:	8082                	c.jr	ra

00000420 <isr_Init>:
     420:	6785                	c.lui	a5,0x1
     422:	88878293          	addi	t0,a5,-1912 # 888 <setup+0x5c>
     426:	3042b773          	csrrc	a4,mie,t0
     42a:	10000737          	lui	a4,0x10000
     42e:	56fd                	c.li	a3,-1
     430:	32d72023          	sw	a3,800(a4) # 10000320 <_stack+0xff60320>
     434:	3042a7f3          	csrrs	a5,mie,t0
     438:	8a04                	exec.it	#76     !csrrsi	a5,mstatus,8
     43a:	8082                	c.jr	ra

0000043c <isr_SetupIrq>:
     43c:	e40007b7          	lui	a5,0xe4000
     440:	0ca782db          	lea.w	t0,a5,a0
     444:	4705                	c.li	a4,1
     446:	00e2a023          	sw	a4,0(t0)
     44a:	f14026f3          	csrr	a3,mhartid
     44e:	00555313          	srli	t1,a0,0x5
     452:	e4002637          	lui	a2,0xe4002
     456:	0c6603db          	lea.w	t2,a2,t1
     45a:	00769593          	slli	a1,a3,0x7
     45e:	00b38833          	add	a6,t2,a1
     462:	00082883          	lw	a7,0(a6)
     466:	00a71533          	sll	a0,a4,a0
     46a:	01156e33          	or	t3,a0,a7
     46e:	01c82023          	sw	t3,0(a6)
     472:	8082                	c.jr	ra

00000474 <isr_Disable>:
     474:	300477f3          	csrrci	a5,mstatus,8
     478:	8082                	c.jr	ra

0000047a <isr_Enable>:
     47a:	8a04                	exec.it	#76     !csrrsi	a5,mstatus,8
     47c:	8082                	c.jr	ra

0000047e <usbd_IOISR>:
     47e:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     480:	c4418713          	addi	a4,gp,-956 # 5fa0 <MEM_SIZE>
     484:	00072083          	lw	ra,0(a4)
     488:	1141                	c.addi	sp,-16
     48a:	8864                	exec.it	#60     !addi	t0,ra,1
     48c:	06400793          	li	a5,100
     490:	c63e                	c.swsp	a5,12(sp)
     492:	8e34                	exec.it	#95     !sw	t0,0(a4)
     494:	00030337          	lui	t1,0x30
     498:	00532023          	sw	t0,0(t1) # 30000 <_end+0x8d90>
     49c:	006c                	c.addi4spn	a1,sp,12
     49e:	4681                	c.li	a3,0
     4a0:	4601                	c.li	a2,0
     4a2:	4b70252b          	lwgp	a0,134324 # 27010 <pUSBDCfg+0x8>
     4a6:	3c3030ef          	jal	ra,4068 <xQueueGenericSendFromISR>
     4aa:	0141                	c.addi	sp,16
     4ac:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

000004ae <usbd_CtrlTsk>:
     4ae:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     4b0:	1141                	c.addi	sp,-16
     4b2:	7d000613          	li	a2,2000
     4b6:	006c                	c.addi4spn	a1,sp,12
     4b8:	4b70252b          	lwgp	a0,134324 # 27010 <pUSBDCfg+0x8>
     4bc:	44d030ef          	jal	ra,4108 <xQueueReceive>
     4c0:	be15695b          	bnec	a0,1,4b2 <usbd_CtrlTsk+0x4>
     4c4:	47b2                	c.lwsp	a5,12(sp)
     4c6:	fe47e6db          	bnec	a5,100,4b2 <usbd_CtrlTsk+0x4>
     4ca:	2631                	c.jal	7d6 <mscIsr>
     4cc:	b7dd                	c.j	4b2 <usbd_CtrlTsk+0x4>

000004ce <usbd_Init>:
     4ce:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     4d0:	000307b7          	lui	a5,0x30
     4d4:	4631                	c.li	a2,12
     4d6:	4581                	c.li	a1,0
     4d8:	00027517          	auipc	a0,0x27
     4dc:	b3050513          	addi	a0,a0,-1232 # 27008 <pUSBDCfg>
     4e0:	0007a023          	sw	zero,0(a5) # 30000 <_end+0x8d90>
     4e4:	8430                	exec.it	#19     !jal	ra,5526 <memset>
     4e6:	4601                	c.li	a2,0
     4e8:	4591                	c.li	a1,4
     4ea:	4529                	c.li	a0,10
     4ec:	225030ef          	jal	ra,3f10 <xQueueGenericCreate>
     4f0:	00000597          	auipc	a1,0x0
     4f4:	f8e58593          	addi	a1,a1,-114 # 47e <usbd_IOISR>
     4f8:	4aa04bab          	swgp	a0,134324 # 27010 <pUSBDCfg+0x8>
     4fc:	4531                	c.li	a0,12
     4fe:	3bb1                	c.jal	25a <registerISR>
     500:	00027797          	auipc	a5,0x27
     504:	b0878793          	addi	a5,a5,-1272 # 27008 <pUSBDCfg>
     508:	4705                	c.li	a4,1
     50a:	4681                	c.li	a3,0
     50c:	40000613          	li	a2,1024
     510:	00005597          	auipc	a1,0x5
     514:	49058593          	addi	a1,a1,1168 # 59a0 <_ITB_BASE_+0x2e4>
     518:	00001517          	auipc	a0,0x1
     51c:	ade50513          	addi	a0,a0,-1314 # ff6 <mscApp>
     520:	3ba040ef          	jal	ra,48da <xTaskCreate>
     524:	52fd                	c.li	t0,-1
     526:	02156c5b          	bnec	a0,1,55e <usbd_Init+0x90>
     52a:	00027797          	auipc	a5,0x27
     52e:	ae278793          	addi	a5,a5,-1310 # 2700c <pUSBDCfg+0x4>
     532:	4705                	c.li	a4,1
     534:	4681                	c.li	a3,0
     536:	40000613          	li	a2,1024
     53a:	00005597          	auipc	a1,0x5
     53e:	4f258593          	addi	a1,a1,1266 # 5a2c <_ITB_BASE_+0x370>
     542:	00000517          	auipc	a0,0x0
     546:	f6c50513          	addi	a0,a0,-148 # 4ae <usbd_CtrlTsk>
     54a:	390040ef          	jal	ra,48da <xTaskCreate>
     54e:	4281                	c.li	t0,0
     550:	0015575b          	beqc	a0,1,55e <usbd_Init+0x90>
     554:	4af0252b          	lwgp	a0,134316 # 27008 <pUSBDCfg>
     558:	3d0040ef          	jal	ra,4928 <vTaskDelete>
     55c:	52fd                	c.li	t0,-1
     55e:	8516                	c.mv	a0,t0
     560:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

00000562 <reqComplete>:
     562:	8082                	c.jr	ra

00000564 <bulkInCmpl>:
     564:	599c                	c.lw	a5,48(a1)
     566:	e781                	c.bnez	a5,56e <bulkInCmpl+0xa>
     568:	4285                	c.li	t0,1
     56a:	c6518d23          	sb	t0,-902(gp) # 5fd6 <packet_sent>
     56e:	8082                	c.jr	ra

00000570 <bulkOutCmpl>:
     570:	599c                	c.lw	a5,48(a1)
     572:	c789                	c.beqz	a5,57c <bulkOutCmpl+0xc>
     574:	4305                	c.li	t1,1
     576:	c6618c23          	sb	t1,-904(gp) # 5fd4 <packet_aborted>
     57a:	8082                	c.jr	ra
     57c:	4285                	c.li	t0,1
     57e:	c6518ca3          	sb	t0,-903(gp) # 5fd5 <packet_received>
     582:	8082                	c.jr	ra

00000584 <connect>:
     584:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     586:	8c74                	exec.it	#63     !lw	a5,-936(gp) # 5fb4 <drv>
     588:	1141                	c.addi	sp,-16
     58a:	8a30                	exec.it	#85     !lw	t1,64(a5)
     58c:	006c                	c.addi4spn	a1,sp,12
     58e:	9302                	c.jalr	t1
     590:	0141                	c.addi	sp,16
     592:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

00000594 <disconnect>:
     594:	8c74                	exec.it	#63     !lw	a5,-936(gp) # 5fb4 <drv>
     596:	cf89                	c.beqz	a5,5b0 <disconnect+0x1c>
     598:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     59a:	1141                	c.addi	sp,-16
     59c:	8a30                	exec.it	#85     !lw	t1,64(a5)
     59e:	006c                	c.addi4spn	a1,sp,12
     5a0:	9302                	c.jalr	t1
     5a2:	c4018a23          	sb	zero,-940(gp) # 5fb0 <configValue>
     5a6:	8e54                	exec.it	#111     !sb	zero,-903(gp) # 5fd5 <packet_received>
     5a8:	8a00                	exec.it	#68     !sb	zero,-902(gp) # 5fd6 <packet_sent>
     5aa:	8260                	exec.it	#112     !sb	zero,-912(gp) # 5fcc <msc_failed_flag>
     5ac:	0141                	c.addi	sp,16
     5ae:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
     5b0:	8082                	c.jr	ra

000005b2 <resume>:
     5b2:	8082                	c.jr	ra

000005b4 <msc_failed>:
     5b4:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
     5b6:	3d7d                	c.jal	474 <isr_Disable>
     5b8:	8034                	exec.it	#25     !lw	a1,-924(gp) # 5fc0 <epIn>
     5ba:	59bc                	c.lw	a5,112(a1)
     5bc:	c7418413          	addi	s0,gp,-908 # 5fd0 <pD>
     5c0:	00c7a303          	lw	t1,12(a5)
     5c4:	4008                	c.lw	a0,0(s0)
     5c6:	9302                	c.jalr	t1
     5c8:	8c40                	exec.it	#38     !lw	a1,-920(gp) # 5fc4 <epOut>
     5ca:	8630                	exec.it	#83     !lw	t0,112(a1)
     5cc:	4008                	c.lw	a0,0(s0)
     5ce:	00c2a383          	lw	t2,12(t0)
     5d2:	9382                	c.jalr	t2
     5d4:	4505                	c.li	a0,1
     5d6:	c6a18823          	sb	a0,-912(gp) # 5fcc <msc_failed_flag>
     5da:	3545                	c.jal	47a <isr_Enable>
     5dc:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>

000005de <get_unicode_string>:
     5de:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
     5e0:	842a                	c.mv	s0,a0
     5e2:	1141                	c.addi	sp,-16
     5e4:	852e                	c.mv	a0,a1
     5e6:	c62e                	c.swsp	a1,12(sp)
     5e8:	004050ef          	jal	ra,55ec <strlen>
     5ec:	0506                	c.slli	a0,0x1
     5ee:	00250793          	addi	a5,a0,2
     5f2:	428d                	c.li	t0,3
     5f4:	00f40023          	sb	a5,0(s0)
     5f8:	005400a3          	sb	t0,1(s0)
     5fc:	06400313          	li	t1,100
     600:	00240713          	addi	a4,s0,2
     604:	45b2                	c.lwsp	a1,12(sp)
     606:	00a37463          	bgeu	t1,a0,60e <get_unicode_string+0x30>
     60a:	06400513          	li	a0,100
     60e:	83ba                	c.mv	t2,a4
     610:	40e386b3          	sub	a3,t2,a4
     614:	00a6e463          	bltu	a3,a0,61c <get_unicode_string+0x3e>
     618:	0141                	c.addi	sp,16
     61a:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>
     61c:	0585                	c.addi	a1,1
     61e:	fff5c083          	lbu	ra,-1(a1)
     622:	0389                	c.addi	t2,2
     624:	fe138f23          	sb	ra,-2(t2)
     628:	fe038fa3          	sb	zero,-1(t2)
     62c:	b7d5                	c.j	610 <get_unicode_string+0x32>

0000062e <requestMemAlloc>:
     62e:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     630:	852e                	c.mv	a0,a1
     632:	8454                	exec.it	#43     !jal	ra,4e7a <pvPortMalloc>
     634:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

00000636 <requestMemFree>:
     636:	c589                	c.beqz	a1,640 <requestMemFree+0xa>
     638:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     63a:	852e                	c.mv	a0,a1
     63c:	8444                	exec.it	#42     !jal	ra,4f5e <vPortFree>
     63e:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
     640:	8082                	c.jr	ra

00000642 <usb_to_storage>:
     642:	8e40                	exec.it	#102     !lbu	a4,-939(gp) # 5fb1 <data_dir>
     644:	8270                	exec.it	#113     !lwgp	a5,134828 # 27208 <dev_num_of_bytes>
     646:	c719                	c.beqz	a4,654 <usb_to_storage+0x12>
     648:	c781                	c.beqz	a5,650 <usb_to_storage+0xe>
     64a:	4e05                	c.li	t3,1
     64c:	c5c18b23          	sb	t3,-938(gp) # 5fb2 <dir_error>
     650:	4501                	c.li	a0,0
     652:	8082                	c.jr	ra
     654:	dff5                	c.beqz	a5,650 <usb_to_storage+0xe>
     656:	8644                	exec.it	#106     !lw	t2,-916(gp) # 5fc8 <host_num_of_bytes>
     658:	fe038ce3          	beqz	t2,650 <usb_to_storage+0xe>
     65c:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     65e:	70050513          	addi	a0,a0,1792
     662:	c4c1a083          	lw	ra,-948(gp) # 5fa8 <bulkOutReq>
     666:	00951293          	slli	t0,a0,0x9
     66a:	00000697          	auipc	a3,0x0
     66e:	f0668693          	addi	a3,a3,-250 # 570 <bulkOutCmpl>
     672:	0050a823          	sw	t0,16(ra)
     676:	8234                	exec.it	#89     !sw	a3,40(ra)
     678:	00959713          	slli	a4,a1,0x9
     67c:	00e3f363          	bgeu	t2,a4,682 <usb_to_storage+0x40>
     680:	871e                	c.mv	a4,t2
     682:	00e0a623          	sw	a4,12(ra)
     686:	8074                	exec.it	#57     !sh	zero,32(ra)
     688:	33f5                	c.jal	474 <isr_Disable>
     68a:	8c40                	exec.it	#38     !lw	a1,-920(gp) # 5fc4 <epOut>
     68c:	0705a303          	lw	t1,112(a1)
     690:	8210                	exec.it	#65     !lw	a2,-948(gp) # 5fa8 <bulkOutReq>
     692:	01832803          	lw	a6,24(t1)
     696:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     698:	9802                	c.jalr	a6
     69a:	33c5                	c.jal	47a <isr_Enable>
     69c:	c791c583          	lbu	a1,-903(gp) # 5fd5 <packet_received>
     6a0:	c589                	c.beqz	a1,6aa <usb_to_storage+0x68>
     6a2:	8210                	exec.it	#65     !lw	a2,-948(gp) # 5fa8 <bulkOutReq>
     6a4:	8e54                	exec.it	#111     !sb	zero,-903(gp) # 5fd5 <packet_received>
     6a6:	5a48                	c.lw	a0,52(a2)
     6a8:	a029                	c.j	6b2 <usb_to_storage+0x70>
     6aa:	8a50                	exec.it	#101     !lbu	a7,-940(gp) # 5fb0 <configValue>
     6ac:	fe0898e3          	bnez	a7,69c <usb_to_storage+0x5a>
     6b0:	4501                	c.li	a0,0
     6b2:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

000006b4 <storage_to_usb>:
     6b4:	8e40                	exec.it	#102     !lbu	a4,-939(gp) # 5fb1 <data_dir>
     6b6:	8270                	exec.it	#113     !lwgp	a5,134828 # 27208 <dev_num_of_bytes>
     6b8:	e719                	c.bnez	a4,6c6 <storage_to_usb+0x12>
     6ba:	c781                	c.beqz	a5,6c2 <storage_to_usb+0xe>
     6bc:	4e05                	c.li	t3,1
     6be:	c5c18b23          	sb	t3,-938(gp) # 5fb2 <dir_error>
     6c2:	4501                	c.li	a0,0
     6c4:	8082                	c.jr	ra
     6c6:	dff5                	c.beqz	a5,6c2 <storage_to_usb+0xe>
     6c8:	8644                	exec.it	#106     !lw	t2,-916(gp) # 5fc8 <host_num_of_bytes>
     6ca:	fe038ce3          	beqz	t2,6c2 <storage_to_usb+0xe>
     6ce:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     6d0:	70050513          	addi	a0,a0,1792
     6d4:	c481a083          	lw	ra,-952(gp) # 5fa4 <bulkInReq>
     6d8:	00951293          	slli	t0,a0,0x9
     6dc:	00000697          	auipc	a3,0x0
     6e0:	e8868693          	addi	a3,a3,-376 # 564 <bulkInCmpl>
     6e4:	0050a823          	sw	t0,16(ra)
     6e8:	8234                	exec.it	#89     !sw	a3,40(ra)
     6ea:	00959713          	slli	a4,a1,0x9
     6ee:	00e3f363          	bgeu	t2,a4,6f4 <storage_to_usb+0x40>
     6f2:	871e                	c.mv	a4,t2
     6f4:	00e0a623          	sw	a4,12(ra)
     6f8:	8074                	exec.it	#57     !sh	zero,32(ra)
     6fa:	3bad                	c.jal	474 <isr_Disable>
     6fc:	8034                	exec.it	#25     !lw	a1,-924(gp) # 5fc0 <epIn>
     6fe:	0705a303          	lw	t1,112(a1)
     702:	8600                	exec.it	#66     !lw	a2,-952(gp) # 5fa4 <bulkInReq>
     704:	01832803          	lw	a6,24(t1)
     708:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     70a:	9802                	c.jalr	a6
     70c:	33bd                	c.jal	47a <isr_Enable>
     70e:	c7a1c583          	lbu	a1,-902(gp) # 5fd6 <packet_sent>
     712:	c589                	c.beqz	a1,71c <storage_to_usb+0x68>
     714:	8600                	exec.it	#66     !lw	a2,-952(gp) # 5fa4 <bulkInReq>
     716:	8a00                	exec.it	#68     !sb	zero,-902(gp) # 5fd6 <packet_sent>
     718:	5a48                	c.lw	a0,52(a2)
     71a:	a029                	c.j	724 <storage_to_usb+0x70>
     71c:	8a50                	exec.it	#101     !lbu	a7,-940(gp) # 5fb0 <configValue>
     71e:	fe0898e3          	bnez	a7,70e <storage_to_usb+0x5a>
     722:	4501                	c.li	a0,0
     724:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

00000726 <send_data>:
     726:	8e40                	exec.it	#102     !lbu	a4,-939(gp) # 5fb1 <data_dir>
     728:	8270                	exec.it	#113     !lwgp	a5,134828 # 27208 <dev_num_of_bytes>
     72a:	e719                	c.bnez	a4,738 <send_data+0x12>
     72c:	c781                	c.beqz	a5,734 <send_data+0xe>
     72e:	4805                	c.li	a6,1
     730:	c5018b23          	sb	a6,-938(gp) # 5fb2 <dir_error>
     734:	4501                	c.li	a0,0
     736:	8082                	c.jr	ra
     738:	dff5                	c.beqz	a5,734 <send_data+0xe>
     73a:	8644                	exec.it	#106     !lw	t2,-916(gp) # 5fc8 <host_num_of_bytes>
     73c:	fe038ce3          	beqz	t2,734 <send_data+0xe>
     740:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     742:	c481a083          	lw	ra,-952(gp) # 5fa4 <bulkInReq>
     746:	00000697          	auipc	a3,0x0
     74a:	e1e68693          	addi	a3,a3,-482 # 564 <bulkInCmpl>
     74e:	00a0a423          	sw	a0,8(ra)
     752:	00a0a823          	sw	a0,16(ra)
     756:	8234                	exec.it	#89     !sw	a3,40(ra)
     758:	00b3f363          	bgeu	t2,a1,75e <send_data+0x38>
     75c:	859e                	c.mv	a1,t2
     75e:	00b0a623          	sw	a1,12(ra)
     762:	8074                	exec.it	#57     !sh	zero,32(ra)
     764:	3b01                	c.jal	474 <isr_Disable>
     766:	8034                	exec.it	#25     !lw	a1,-924(gp) # 5fc0 <epIn>
     768:	8630                	exec.it	#83     !lw	t0,112(a1)
     76a:	8600                	exec.it	#66     !lw	a2,-952(gp) # 5fa4 <bulkInReq>
     76c:	0182a303          	lw	t1,24(t0)
     770:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     772:	9302                	c.jalr	t1
     774:	3319                	c.jal	47a <isr_Enable>
     776:	c7a1c503          	lbu	a0,-902(gp) # 5fd6 <packet_sent>
     77a:	c511                	c.beqz	a0,786 <send_data+0x60>
     77c:	c481a583          	lw	a1,-952(gp) # 5fa4 <bulkInReq>
     780:	8a00                	exec.it	#68     !sb	zero,-902(gp) # 5fd6 <packet_sent>
     782:	59c8                	c.lw	a0,52(a1)
     784:	a029                	c.j	78e <send_data+0x68>
     786:	c541c603          	lbu	a2,-940(gp) # 5fb0 <configValue>
     78a:	f675                	c.bnez	a2,776 <send_data+0x50>
     78c:	4501                	c.li	a0,0
     78e:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

00000790 <suspend>:
     790:	8082                	c.jr	ra

00000792 <showBuff>:
     792:	8820                	exec.it	#20     !jal	t0,52b6 <__riscv_save_4>
     794:	842a                	c.mv	s0,a0
     796:	00b509b3          	add	s3,a0,a1
     79a:	1f400913          	li	s2,500
     79e:	1f400a13          	li	s4,500
     7a2:	00027497          	auipc	s1,0x27
     7a6:	87248493          	addi	s1,s1,-1934 # 27014 <showBuffTemp>
     7aa:	00005a97          	auipc	s5,0x5
     7ae:	132a8a93          	addi	s5,s5,306 # 58dc <_ITB_BASE_+0x220>
     7b2:	412a0533          	sub	a0,s4,s2
     7b6:	01341663          	bne	s0,s3,7c2 <showBuff+0x30>
     7ba:	94aa                	c.add	s1,a0
     7bc:	00048023          	sb	zero,0(s1)
     7c0:	8c34                	exec.it	#31     !j	52ea <__riscv_restore_4>
     7c2:	00044683          	lbu	a3,0(s0)
     7c6:	85ca                	c.mv	a1,s2
     7c8:	8656                	c.mv	a2,s5
     7ca:	9526                	c.add	a0,s1
     7cc:	8e60                	exec.it	#118     !jal	ra,55d0 <snprintf>
     7ce:	0405                	c.addi	s0,1
     7d0:	40a90933          	sub	s2,s2,a0
     7d4:	bff9                	c.j	7b2 <showBuff+0x20>

000007d6 <mscIsr>:
     7d6:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     7d8:	8c74                	exec.it	#63     !lw	a5,-936(gp) # 5fb4 <drv>
     7da:	0147a303          	lw	t1,20(a5)
     7de:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     7e0:	9302                	c.jalr	t1
     7e2:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

000007e4 <EDMA_EP0_write_IN_fifiodata>:
     7e4:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
     7e6:	842a                	c.mv	s0,a0
     7e8:	00b54363          	blt	a0,a1,7ee <EDMA_EP0_write_IN_fifiodata+0xa>
     7ec:	842e                	c.mv	s0,a1
     7ee:	30000637          	lui	a2,0x30000
     7f2:	10060613          	addi	a2,a2,256 # 30000100 <_stack+0x2ff60100>
     7f6:	c5c1a583          	lw	a1,-932(gp) # 5fb8 <ep0Buff>
     7fa:	4501                	c.li	a0,0
     7fc:	36a1                	c.jal	344 <edma_Init>
     7fe:	4801                	c.li	a6,0
     800:	4781                	c.li	a5,0
     802:	85a2                	c.mv	a1,s0
     804:	4701                	c.li	a4,0
     806:	4681                	c.li	a3,0
     808:	4601                	c.li	a2,0
     80a:	4501                	c.li	a0,0
     80c:	36bd                	c.jal	37a <edma_SetSrcParams>
     80e:	4701                	c.li	a4,0
     810:	4681                	c.li	a3,0
     812:	4601                	c.li	a2,0
     814:	4581                	c.li	a1,0
     816:	4501                	c.li	a0,0
     818:	364d                	c.jal	3ba <edma_SetDstParams>
     81a:	4585                	c.li	a1,1
     81c:	4501                	c.li	a0,0
     81e:	6441                	c.lui	s0,0x10
     820:	36e1                	c.jal	3e8 <edma_Start>
     822:	36dd                	c.jal	408 <edma_Check_sts>
     824:	8d61                	c.and	a0,s0
     826:	dd75                	c.beqz	a0,822 <EDMA_EP0_write_IN_fifiodata+0x3e>
     828:	36e5                	c.jal	410 <edma_clear_sts>
     82a:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>

0000082c <setup>:
     82c:	8660                	exec.it	#114     !jal	t0,52b6 <__riscv_save_4>
     82e:	8c74                	exec.it	#63     !lw	a5,-936(gp) # 5fb4 <drv>
     830:	1141                	c.addi	sp,-16
     832:	8a30                	exec.it	#85     !lw	t1,64(a5)
     834:	84ae                	c.mv	s1,a1
     836:	006c                	c.addi4spn	a1,sp,12
     838:	892a                	c.mv	s2,a0
     83a:	9302                	c.jalr	t1
     83c:	46b2                	c.lwsp	a3,12(sp)
     83e:	4709                	c.li	a4,2
     840:	00c6a283          	lw	t0,12(a3)
     844:	00e2eb63          	bltu	t0,a4,85a <setup+0x2e>
     848:	408d                	c.li	ra,3
     84a:	0850f563          	bgeu	ra,t0,8d4 <setup+0xa8>
     84e:	0052e65b          	bnec	t0,5,85a <setup+0x2e>
     852:	b0018a13          	addi	s4,gp,-1280 # 5e5c <devSsDesc>
     856:	a0818993          	addi	s3,gp,-1528 # 5d64 <confSsDesc>
     85a:	c601a383          	lw	t2,-928(gp) # 5fbc <ep0Req>
     85e:	8610                	exec.it	#67     !lw	a0,-932(gp) # 5fb8 <ep0Buff>
     860:	00000597          	auipc	a1,0x0
     864:	d0258593          	addi	a1,a1,-766 # 562 <reqComplete>
     868:	00a3a423          	sw	a0,8(t2)
     86c:	00a3a823          	sw	a0,16(t2)
     870:	02b3a423          	sw	a1,40(t2)
     874:	0004c803          	lbu	a6,0(s1)
     878:	06087613          	andi	a2,a6,96
     87c:	c22d                	c.beqz	a2,8de <setup+0xb2>
     87e:	000664db          	bnec	a2,32,886 <setup+0x5a>
     882:	3d80006f          	j	c5a <setup+0x42e>
     886:	c801a503          	lw	a0,-896(gp) # 5fdc <string_index>
     88a:	00156f5b          	bnec	a0,1,8a8 <setup+0x7c>
     88e:	0064d683          	lhu	a3,6(s1)
     892:	00d45363          	bge	s0,a3,898 <setup+0x6c>
     896:	86a2                	c.mv	a3,s0
     898:	0ff6fe13          	andi	t3,a3,255
     89c:	30000eb7          	lui	t4,0x30000
     8a0:	01ce80a3          	sb	t3,1(t4) # 30000001 <_stack+0x2ff60001>
     8a4:	c801a023          	sw	zero,-896(gp) # 5fdc <string_index>
     8a8:	4781                	c.li	a5,0
     8aa:	2a040c63          	beqz	s0,b62 <setup+0x336>
     8ae:	0064df03          	lhu	t5,6(s1)
     8b2:	c601a603          	lw	a2,-928(gp) # 5fbc <ep0Req>
     8b6:	01e45363          	bge	s0,t5,8bc <setup+0x90>
     8ba:	8f22                	c.mv	t5,s0
     8bc:	4832                	c.lwsp	a6,12(sp)
     8be:	01e62623          	sw	t5,12(a2)
     8c2:	00882583          	lw	a1,8(a6)
     8c6:	854a                	c.mv	a0,s2
     8c8:	0705af83          	lw	t6,112(a1)
     8cc:	018fa303          	lw	t1,24(t6)
     8d0:	9302                	c.jalr	t1
     8d2:	a479                	c.j	b60 <setup+0x334>
     8d4:	aec18a13          	addi	s4,gp,-1300 # 5e48 <devHsDesc>
     8d8:	9fc18993          	addi	s3,gp,-1540 # 5d58 <confHsDesc>
     8dc:	bfbd                	c.j	85a <setup+0x2e>
     8de:	0014c583          	lbu	a1,1(s1)
     8e2:	462d                	c.li	a2,11
     8e4:	12b66063          	bltu	a2,a1,a04 <setup+0x1d8>
     8e8:	00005897          	auipc	a7,0x5
     8ec:	1a088893          	addi	a7,a7,416 # 5a88 <_ITB_BASE_+0x3cc>
     8f0:	0cb88e5b          	lea.w	t3,a7,a1
     8f4:	8640                	exec.it	#98     !lw	t4,0(t3)
     8f6:	011e8f33          	add	t5,t4,a7
     8fa:	8f02                	c.jr	t5
     8fc:	00f87593          	andi	a1,a6,15
     900:	57fd                	c.li	a5,-1
     902:	2615d05b          	beqc	a1,1,b62 <setup+0x336>
     906:	f1c1                	c.bnez	a1,886 <setup+0x5a>
     908:	0024df83          	lhu	t6,2(s1)
     90c:	40b9                	c.li	ra,14
     90e:	008fd313          	srli	t1,t6,0x8
     912:	fff30293          	addi	t0,t1,-1
     916:	3c02a75b          	bfoz	a4,t0,15,0
     91a:	0ee0e563          	bltu	ra,a4,a04 <setup+0x1d8>
     91e:	00005397          	auipc	t2,0x5
     922:	19a38393          	addi	t2,t2,410 # 5ab8 <_ITB_BASE_+0x3fc>
     926:	0ce387db          	lea.w	a5,t2,a4
     92a:	4390                	c.lw	a2,0(a5)
     92c:	007608b3          	add	a7,a2,t2
     930:	8882                	c.jr	a7
     932:	4649                	c.li	a2,18
     934:	85d2                	c.mv	a1,s4
     936:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     938:	45c9                	c.li	a1,18
     93a:	8610                	exec.it	#67     !lw	a0,-932(gp) # 5fb8 <ep0Buff>
     93c:	3d99                	c.jal	792 <showBuff>
     93e:	0064d583          	lhu	a1,6(s1)
     942:	4405                	c.li	s0,1
     944:	4549                	c.li	a0,18
     946:	3d79                	c.jal	7e4 <EDMA_EP0_write_IN_fifiodata>
     948:	c881a023          	sw	s0,-896(gp) # 5fdc <string_index>
     94c:	4449                	c.li	s0,18
     94e:	bf25                	c.j	886 <setup+0x5a>
     950:	46d4                	c.lw	a3,12(a3)
     952:	0029d403          	lhu	s0,2(s3)
     956:	0836d85b          	beqc	a3,3,9e6 <setup+0x1ba>
     95a:	0856d95b          	beqc	a3,5,9ec <setup+0x1c0>
     95e:	0026e85b          	bnec	a3,2,96e <setup+0x142>
     962:	04000e13          	li	t3,64
     966:	c3c19023          	sh	t3,-992(gp) # 5f7c <endpointEpInDesc+0x4>
     96a:	c3c19423          	sh	t3,-984(gp) # 5f84 <endpointEpOutDesc+0x4>
     96e:	85ce                	c.mv	a1,s3
     970:	4625                	c.li	a2,9
     972:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     974:	c5c1ae83          	lw	t4,-932(gp) # 5fb8 <ep0Buff>
     978:	4625                	c.li	a2,9
     97a:	b1418593          	addi	a1,gp,-1260 # 5e70 <interfaceDesc>
     97e:	009e8513          	addi	a0,t4,9
     982:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     984:	c5c1af03          	lw	t5,-932(gp) # 5fb8 <ep0Buff>
     988:	461d                	c.li	a2,7
     98a:	c1c18593          	addi	a1,gp,-996 # 5f78 <endpointEpInDesc>
     98e:	012f0513          	addi	a0,t5,18
     992:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     994:	4832                	c.lwsp	a6,12(sp)
     996:	49e5                	c.li	s3,25
     998:	00c82f83          	lw	t6,12(a6)
     99c:	005fea5b          	bnec	t6,5,9b0 <setup+0x184>
     9a0:	c5c1a303          	lw	t1,-932(gp) # 5fb8 <ep0Buff>
     9a4:	4619                	c.li	a2,6
     9a6:	8e44                	exec.it	#110     !addi	a1,gp,-1004 # 5f70 <compDesc>
     9a8:	01930513          	addi	a0,t1,25
     9ac:	49fd                	c.li	s3,31
     9ae:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     9b0:	c5c1a283          	lw	t0,-932(gp) # 5fb8 <ep0Buff>
     9b4:	8a54                	exec.it	#109     !addi	a1,gp,-988 # 5f80 <endpointEpOutDesc>
     9b6:	461d                	c.li	a2,7
     9b8:	01328533          	add	a0,t0,s3
     9bc:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     9be:	45b2                	c.lwsp	a1,12(sp)
     9c0:	45d8                	c.lw	a4,12(a1)
     9c2:	00576a5b          	bnec	a4,5,9d6 <setup+0x1aa>
     9c6:	099d                	c.addi	s3,7
     9c8:	c5c1a383          	lw	t2,-932(gp) # 5fb8 <ep0Buff>
     9cc:	4619                	c.li	a2,6
     9ce:	8e44                	exec.it	#110     !addi	a1,gp,-1004 # 5f70 <compDesc>
     9d0:	01338533          	add	a0,t2,s3
     9d4:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     9d6:	0064d583          	lhu	a1,6(s1)
     9da:	8522                	c.mv	a0,s0
     9dc:	3521                	c.jal	7e4 <EDMA_EP0_write_IN_fifiodata>
     9de:	4785                	c.li	a5,1
     9e0:	c8f1a023          	sw	a5,-896(gp) # 5fdc <string_index>
     9e4:	b54d                	c.j	886 <setup+0x5a>
     9e6:	20000e13          	li	t3,512
     9ea:	bfb5                	c.j	966 <setup+0x13a>
     9ec:	40000e13          	li	t3,1024
     9f0:	bf9d                	c.j	966 <setup+0x13a>
     9f2:	0ffff613          	andi	a2,t6,255
     9f6:	00165f5b          	beqc	a2,1,a14 <setup+0x1e8>
     9fa:	c619                	c.beqz	a2,a08 <setup+0x1dc>
     9fc:	0226515b          	beqc	a2,2,a1e <setup+0x1f2>
     a00:	0236545b          	beqc	a2,3,a28 <setup+0x1fc>
     a04:	57fd                	c.li	a5,-1
     a06:	aab1                	c.j	b62 <setup+0x336>
     a08:	c341c403          	lbu	s0,-972(gp) # 5f90 <languageDesc>
     a0c:	c3418593          	addi	a1,gp,-972 # 5f90 <languageDesc>
     a10:	8622                	c.mv	a2,s0
     a12:	b7c9                	c.j	9d4 <setup+0x1a8>
     a14:	de41c403          	lbu	s0,-540(gp) # 6140 <vendorDesc>
     a18:	de418593          	addi	a1,gp,-540 # 6140 <vendorDesc>
     a1c:	bfd5                	c.j	a10 <setup+0x1e4>
     a1e:	d881c403          	lbu	s0,-632(gp) # 60e4 <productDesc>
     a22:	d8818593          	addi	a1,gp,-632 # 60e4 <productDesc>
     a26:	b7ed                	c.j	a10 <setup+0x1e4>
     a28:	dc81c403          	lbu	s0,-568(gp) # 6124 <serialDesc>
     a2c:	dc818593          	addi	a1,gp,-568 # 6124 <serialDesc>
     a30:	b7c5                	c.j	a10 <setup+0x1e4>
     a32:	4615                	c.li	a2,5
     a34:	c0418593          	addi	a1,gp,-1020 # 5f60 <bosDesc>
     a38:	c061d403          	lhu	s0,-1018(gp) # 5f62 <bosDesc+0x2>
     a3c:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     a3e:	4532                	c.lwsp	a0,12(sp)
     a40:	4e85                	c.li	t4,1
     a42:	4914                	c.lw	a3,16(a0)
     a44:	4815                	c.li	a6,5
     a46:	ffb68e13          	addi	t3,a3,-5
     a4a:	01ceeb63          	bltu	t4,t3,a60 <setup+0x234>
     a4e:	c5c1af03          	lw	t5,-932(gp) # 5fb8 <ep0Buff>
     a52:	4629                	c.li	a2,10
     a54:	9f018593          	addi	a1,gp,-1552 # 5d4c <capabilitySsDesc>
     a58:	005f0513          	addi	a0,t5,5
     a5c:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     a5e:	483d                	c.li	a6,15
     a60:	c5c1af83          	lw	t6,-932(gp) # 5fb8 <ep0Buff>
     a64:	461d                	c.li	a2,7
     a66:	c0c18593          	addi	a1,gp,-1012 # 5f68 <capabilityExtDesc>
     a6a:	010f8533          	add	a0,t6,a6
     a6e:	b79d                	c.j	9d4 <setup+0x1a8>
     a70:	4629                	c.li	a2,10
     a72:	b2018593          	addi	a1,gp,-1248 # 5e7c <qualifierDesc>
     a76:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     a78:	4429                	c.li	s0,10
     a7a:	b531                	c.j	886 <setup+0x5a>
     a7c:	46cc                	c.lw	a1,12(a3)
     a7e:	57fd                	c.li	a5,-1
     a80:	0e55d15b          	beqc	a1,5,b62 <setup+0x336>
     a84:	0029d403          	lhu	s0,2(s3)
     a88:	0025d75b          	beqc	a1,2,a96 <setup+0x26a>
     a8c:	b635ec5b          	bnec	a1,3,a04 <setup+0x1d8>
     a90:	04000313          	li	t1,64
     a94:	a019                	c.j	a9a <setup+0x26e>
     a96:	20000313          	li	t1,512
     a9a:	85ce                	c.mv	a1,s3
     a9c:	4625                	c.li	a2,9
     a9e:	c2619023          	sh	t1,-992(gp) # 5f7c <endpointEpInDesc+0x4>
     aa2:	c2619423          	sh	t1,-984(gp) # 5f84 <endpointEpOutDesc+0x4>
     aa6:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     aa8:	c5c1a983          	lw	s3,-932(gp) # 5fb8 <ep0Buff>
     aac:	429d                	c.li	t0,7
     aae:	4625                	c.li	a2,9
     ab0:	b1418593          	addi	a1,gp,-1260 # 5e70 <interfaceDesc>
     ab4:	00998513          	addi	a0,s3,9
     ab8:	005980a3          	sb	t0,1(s3)
     abc:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     abe:	c5c1a703          	lw	a4,-932(gp) # 5fb8 <ep0Buff>
     ac2:	461d                	c.li	a2,7
     ac4:	c1c18593          	addi	a1,gp,-996 # 5f78 <endpointEpInDesc>
     ac8:	01270513          	addi	a0,a4,18
     acc:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     ace:	43b2                	c.lwsp	t2,12(sp)
     ad0:	49e5                	c.li	s3,25
     ad2:	00c3a783          	lw	a5,12(t2)
     ad6:	0057ea5b          	bnec	a5,5,aea <setup+0x2be>
     ada:	c5c1a083          	lw	ra,-932(gp) # 5fb8 <ep0Buff>
     ade:	4619                	c.li	a2,6
     ae0:	8e44                	exec.it	#110     !addi	a1,gp,-1004 # 5f70 <compDesc>
     ae2:	01908513          	addi	a0,ra,25
     ae6:	49fd                	c.li	s3,31
     ae8:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     aea:	c5c1a883          	lw	a7,-932(gp) # 5fb8 <ep0Buff>
     aee:	01388533          	add	a0,a7,s3
     af2:	461d                	c.li	a2,7
     af4:	8a54                	exec.it	#109     !addi	a1,gp,-988 # 5f80 <endpointEpOutDesc>
     af6:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     af8:	099d                	c.addi	s3,7
     afa:	8610                	exec.it	#67     !lw	a0,-932(gp) # 5fb8 <ep0Buff>
     afc:	461d                	c.li	a2,7
     afe:	8a54                	exec.it	#109     !addi	a1,gp,-988 # 5f80 <endpointEpOutDesc>
     b00:	954e                	c.add	a0,s3
     b02:	8014                	exec.it	#9     !jal	ra,5438 <memmove>
     b04:	85a2                	c.mv	a1,s0
     b06:	8610                	exec.it	#67     !lw	a0,-932(gp) # 5fb8 <ep0Buff>
     b08:	3169                	c.jal	792 <showBuff>
     b0a:	bbb5                	c.j	886 <setup+0x5a>
     b0c:	0024d083          	lhu	ra,2(s1)
     b10:	4705                	c.li	a4,1
     b12:	57fd                	c.li	a5,-1
     b14:	04176763          	bltu	a4,ra,b62 <setup+0x336>
     b18:	04009c63          	bnez	ra,b70 <setup+0x344>
     b1c:	c4018a23          	sb	zero,-940(gp) # 5fb0 <configValue>
     b20:	428c                	c.lw	a1,0(a3)
     b22:	08100413          	li	s0,129
     b26:	00b68a63          	beq	a3,a1,b3a <setup+0x30e>
     b2a:	07a5c083          	lbu	ra,122(a1)
     b2e:	02809d63          	bne	ra,s0,b68 <setup+0x33c>
     b32:	59bc                	c.lw	a5,112(a1)
     b34:	854a                	c.mv	a0,s2
     b36:	43d4                	c.lw	a3,4(a5)
     b38:	9682                	c.jalr	a3
     b3a:	43b2                	c.lwsp	t2,12(sp)
     b3c:	0003a583          	lw	a1,0(t2)
     b40:	00b38c63          	beq	t2,a1,b58 <setup+0x32c>
     b44:	07a5c603          	lbu	a2,122(a1)
     b48:	0216625b          	bnec	a2,1,b6c <setup+0x340>
     b4c:	0705a883          	lw	a7,112(a1)
     b50:	854a                	c.mv	a0,s2
     b52:	0048ae03          	lw	t3,4(a7)
     b56:	9e02                	c.jalr	t3
     b58:	4eb2                	c.lwsp	t4,12(sp)
     b5a:	4f11                	c.li	t5,4
     b5c:	01eeaa23          	sw	t5,20(t4)
     b60:	4781                	c.li	a5,0
     b62:	853e                	c.mv	a0,a5
     b64:	0141                	c.addi	sp,16
     b66:	8674                	exec.it	#123     !j	52ea <__riscv_restore_4>
     b68:	418c                	c.lw	a1,0(a1)
     b6a:	bf75                	c.j	b26 <setup+0x2fa>
     b6c:	418c                	c.lw	a1,0(a1)
     b6e:	bfc9                	c.j	b40 <setup+0x314>
     b70:	c541c783          	lbu	a5,-940(gp) # 5fb0 <configValue>
     b74:	be17d65b          	beqc	a5,1,b60 <setup+0x334>
     b78:	c4e18a23          	sb	a4,-940(gp) # 5fb0 <configValue>
     b7c:	428c                	c.lw	a1,0(a3)
     b7e:	08100493          	li	s1,129
     b82:	00b68d63          	beq	a3,a1,b9c <setup+0x370>
     b86:	07a5c383          	lbu	t2,122(a1)
     b8a:	04939963          	bne	t2,s1,bdc <setup+0x3b0>
     b8e:	59b0                	c.lw	a2,112(a1)
     b90:	854a                	c.mv	a0,s2
     b92:	00062883          	lw	a7,0(a2)
     b96:	c1c18613          	addi	a2,gp,-996 # 5f78 <endpointEpInDesc>
     b9a:	9882                	c.jalr	a7
     b9c:	4e32                	c.lwsp	t3,12(sp)
     b9e:	000e2583          	lw	a1,0(t3)
     ba2:	00be0e63          	beq	t3,a1,bbe <setup+0x392>
     ba6:	07a5ce83          	lbu	t4,122(a1)
     baa:	021eeb5b          	bnec	t4,1,be0 <setup+0x3b4>
     bae:	0705af03          	lw	t5,112(a1)
     bb2:	c2418613          	addi	a2,gp,-988 # 5f80 <endpointEpOutDesc>
     bb6:	000f2803          	lw	a6,0(t5)
     bba:	854a                	c.mv	a0,s2
     bbc:	9802                	c.jalr	a6
     bbe:	45b2                	c.lwsp	a1,12(sp)
     bc0:	4515                	c.li	a0,5
     bc2:	c9c8                	c.sw	a0,20(a1)
     bc4:	00798303          	lb	t1,7(s3)
     bc8:	c581af83          	lw	t6,-936(gp) # 5fb4 <drv>
     bcc:	00637c5b          	bbc	t1,6,be4 <setup+0x3b8>
     bd0:	04cfa703          	lw	a4,76(t6)
     bd4:	d751                	c.beqz	a4,b60 <setup+0x334>
     bd6:	854a                	c.mv	a0,s2
     bd8:	9702                	c.jalr	a4
     bda:	b759                	c.j	b60 <setup+0x334>
     bdc:	418c                	c.lw	a1,0(a1)
     bde:	b755                	c.j	b82 <setup+0x356>
     be0:	418c                	c.lw	a1,0(a1)
     be2:	b7c1                	c.j	ba2 <setup+0x376>
     be4:	050fa283          	lw	t0,80(t6)
     be8:	f6028ce3          	beqz	t0,b60 <setup+0x334>
     bec:	04cfa703          	lw	a4,76(t6)
     bf0:	b7dd                	c.j	bd6 <setup+0x3aa>
     bf2:	c5418803          	lb	a6,-940(gp) # 5fb0 <configValue>
     bf6:	01050023          	sb	a6,0(a0)
     bfa:	4405                	c.li	s0,1
     bfc:	b169                	c.j	886 <setup+0x5a>
     bfe:	0024d803          	lhu	a6,2(s1)
     c02:	57fd                	c.li	a5,-1
     c04:	f4081fe3          	bnez	a6,b62 <setup+0x336>
     c08:	0044d683          	lhu	a3,4(s1)
     c0c:	fab9                	c.bnez	a3,b62 <setup+0x336>
     c0e:	0064d503          	lhu	a0,6(s1)
     c12:	f921                	c.bnez	a0,b62 <setup+0x336>
     c14:	4401                	c.li	s0,0
     c16:	b985                	c.j	886 <setup+0x5a>
     c18:	0024df83          	lhu	t6,2(s1)
     c1c:	57fd                	c.li	a5,-1
     c1e:	f40f92e3          	bnez	t6,b62 <setup+0x336>
     c22:	0044d303          	lhu	t1,4(s1)
     c26:	f2031ee3          	bnez	t1,b62 <setup+0x336>
     c2a:	0064d283          	lhu	t0,6(s1)
     c2e:	b212ea5b          	bnec	t0,1,b62 <setup+0x336>
     c32:	00050023          	sb	zero,0(a0)
     c36:	b7d1                	c.j	bfa <setup+0x3ce>
     c38:	0024d083          	lhu	ra,2(s1)
     c3c:	57fd                	c.li	a5,-1
     c3e:	f20092e3          	bnez	ra,b62 <setup+0x336>
     c42:	0044d883          	lhu	a7,4(s1)
     c46:	f0089ee3          	bnez	a7,b62 <setup+0x336>
     c4a:	0064d603          	lhu	a2,6(s1)
     c4e:	b0266a5b          	bnec	a2,2,b62 <setup+0x336>
     c52:	00051023          	sh	zero,0(a0)
     c56:	4409                	c.li	s0,2
     c58:	b13d                	c.j	886 <setup+0x5a>
     c5a:	1c0836db          	bfos	a3,a6,7,0
     c5e:	0014c883          	lbu	a7,1(s1)
     c62:	0206d663          	bgez	a3,c8e <setup+0x462>
     c66:	0fe00293          	li	t0,254
     c6a:	57fd                	c.li	a5,-1
     c6c:	ee589be3          	bne	a7,t0,b62 <setup+0x336>
     c70:	0024d703          	lhu	a4,2(s1)
     c74:	ee0717e3          	bnez	a4,b62 <setup+0x336>
     c78:	0064d083          	lhu	ra,6(s1)
     c7c:	ae10e35b          	bnec	ra,1,b62 <setup+0x336>
     c80:	0044d383          	lhu	t2,4(s1)
     c84:	ec039fe3          	bnez	t2,b62 <setup+0x336>
     c88:	1d1000ef          	jal	ra,1658 <devGetMaxLun>
     c8c:	b7bd                	c.j	bfa <setup+0x3ce>
     c8e:	0ff00513          	li	a0,255
     c92:	57fd                	c.li	a5,-1
     c94:	eca897e3          	bne	a7,a0,b62 <setup+0x336>
     c98:	0024de03          	lhu	t3,2(s1)
     c9c:	ec0e13e3          	bnez	t3,b62 <setup+0x336>
     ca0:	0064de83          	lhu	t4,6(s1)
     ca4:	ea0e9fe3          	bnez	t4,b62 <setup+0x336>
     ca8:	0044df03          	lhu	t5,4(s1)
     cac:	ea0f1be3          	bnez	t5,b62 <setup+0x336>
     cb0:	8034                	exec.it	#25     !lw	a1,-924(gp) # 5fc0 <epIn>
     cb2:	8260                	exec.it	#112     !sb	zero,-912(gp) # 5fcc <msc_failed_flag>
     cb4:	59a0                	c.lw	s0,112(a1)
     cb6:	4601                	c.li	a2,0
     cb8:	00842f83          	lw	t6,8(s0) # 10008 <__global_pointer$+0x9cac>
     cbc:	854a                	c.mv	a0,s2
     cbe:	9f82                	c.jalr	t6
     cc0:	8c40                	exec.it	#38     !lw	a1,-920(gp) # 5fc4 <epOut>
     cc2:	59bc                	c.lw	a5,112(a1)
     cc4:	4601                	c.li	a2,0
     cc6:	0087a303          	lw	t1,8(a5)
     cca:	854a                	c.mv	a0,s2
     ccc:	9302                	c.jalr	t1
     cce:	b799                	c.j	c14 <setup+0x3e8>

00000cd0 <mscProtocol>:
     cd0:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
     cd2:	1101                	c.addi	sp,-32
     cd4:	c701c783          	lbu	a5,-912(gp) # 5fcc <msc_failed_flag>
     cd8:	fff5                	c.bnez	a5,cd4 <mscProtocol+0x4>
     cda:	8a70                	exec.it	#117     !jal	ra,474 <isr_Disable>
     cdc:	c701c283          	lbu	t0,-912(gp) # 5fcc <msc_failed_flag>
     ce0:	00028363          	beqz	t0,ce6 <mscProtocol+0x16>
     ce4:	8260                	exec.it	#112     !sb	zero,-912(gp) # 5fcc <msc_failed_flag>
     ce6:	8e00                	exec.it	#70     !jal	ra,47a <isr_Enable>
     ce8:	c541c303          	lbu	t1,-940(gp) # 5fb0 <configValue>
     cec:	4481                	c.li	s1,0
     cee:	02030e63          	beqz	t1,d2a <mscProtocol+0x5a>
     cf2:	c4c1a083          	lw	ra,-948(gp) # 5fa8 <bulkOutReq>
     cf6:	c501a703          	lw	a4,-944(gp) # 5fac <command_buff>
     cfa:	00000397          	auipc	t2,0x0
     cfe:	87638393          	addi	t2,t2,-1930 # 570 <bulkOutCmpl>
     d02:	44fd                	c.li	s1,31
     d04:	00e0a423          	sw	a4,8(ra)
     d08:	00e0a823          	sw	a4,16(ra)
     d0c:	0270a423          	sw	t2,40(ra)
     d10:	0090a623          	sw	s1,12(ra)
     d14:	8074                	exec.it	#57     !sh	zero,32(ra)
     d16:	8a70                	exec.it	#117     !jal	ra,474 <isr_Disable>
     d18:	8c40                	exec.it	#38     !lw	a1,-920(gp) # 5fc4 <epOut>
     d1a:	59a8                	c.lw	a0,112(a1)
     d1c:	8210                	exec.it	#65     !lw	a2,-948(gp) # 5fa8 <bulkOutReq>
     d1e:	4d14                	c.lw	a3,24(a0)
     d20:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     d22:	9682                	c.jalr	a3
     d24:	c511                	c.beqz	a0,d30 <mscProtocol+0x60>
     d26:	8e00                	exec.it	#70     !jal	ra,47a <isr_Enable>
     d28:	4485                	c.li	s1,1
     d2a:	8526                	c.mv	a0,s1
     d2c:	6105                	c.addi16sp	sp,32
     d2e:	8c14                	exec.it	#15     !j	52f4 <__riscv_restore_0>
     d30:	8e00                	exec.it	#70     !jal	ra,47a <isr_Enable>
     d32:	c791c583          	lbu	a1,-903(gp) # 5fd5 <packet_received>
     d36:	c185                	c.beqz	a1,d56 <mscProtocol+0x86>
     d38:	8210                	exec.it	#65     !lw	a2,-948(gp) # 5fa8 <bulkOutReq>
     d3a:	03464803          	lbu	a6,52(a2)
     d3e:	8e54                	exec.it	#111     !sb	zero,-903(gp) # 5fd5 <packet_received>
     d40:	4485                	c.li	s1,1
     d42:	fe0804e3          	beqz	a6,d2a <mscProtocol+0x5a>
     d46:	8a50                	exec.it	#101     !lbu	a7,-940(gp) # 5fb0 <configValue>
     d48:	fe0881e3          	beqz	a7,d2a <mscProtocol+0x5a>
     d4c:	01f85c5b          	beqc	a6,31,d64 <mscProtocol+0x94>
     d50:	865ff0ef          	jal	ra,5b4 <msc_failed>
     d54:	bfd9                	c.j	d2a <mscProtocol+0x5a>
     d56:	c781ce83          	lbu	t4,-904(gp) # 5fd4 <packet_aborted>
     d5a:	fc0e8ce3          	beqz	t4,d32 <mscProtocol+0x62>
     d5e:	c6018c23          	sb	zero,-904(gp) # 5fd4 <packet_aborted>
     d62:	b7d9                	c.j	d28 <mscProtocol+0x58>
     d64:	c501ae03          	lw	t3,-944(gp) # 5fac <command_buff>
     d68:	43425f37          	lui	t5,0x43425
     d6c:	8640                	exec.it	#98     !lw	t4,0(t3)
     d6e:	355f0f93          	addi	t6,t5,853 # 43425355 <_stack+0x43385355>
     d72:	fdfe9fe3          	bne	t4,t6,d50 <mscProtocol+0x80>
     d76:	008e2783          	lw	a5,8(t3)
     d7a:	00ce0283          	lb	t0,12(t3)
     d7e:	004e2303          	lw	t1,4(t3)
     d82:	c7c1a403          	lw	s0,-900(gp) # 5fd8 <response_buff>
     d86:	086c                	c.addi4spn	a1,sp,28
     d88:	00fe0513          	addi	a0,t3,15
     d8c:	6a0047ab          	swgp	zero,134828 # 27208 <dev_num_of_bytes>
     d90:	c4018b23          	sb	zero,-938(gp) # 5fb2 <dir_error>
     d94:	c6f1a623          	sw	a5,-916(gp) # 5fc8 <host_num_of_bytes>
     d98:	c4518aa3          	sb	t0,-939(gp) # 5fb1 <data_dir>
     d9c:	00642223          	sw	t1,4(s0)
     da0:	29a1                	c.jal	11f8 <scsiExecCmd>
     da2:	c541c703          	lbu	a4,-940(gp) # 5fb0 <configValue>
     da6:	d351                	c.beqz	a4,d2a <mscProtocol+0x5a>
     da8:	534250b7          	lui	ra,0x53425
     dac:	35508393          	addi	t2,ra,853 # 53425355 <_stack+0x53385355>
     db0:	00742023          	sw	t2,0(s0)
     db4:	45f2                	c.lwsp	a1,28(sp)
     db6:	c6c1a683          	lw	a3,-916(gp) # 5fc8 <host_num_of_bytes>
     dba:	40b68633          	sub	a2,a3,a1
     dbe:	c410                	c.sw	a2,8(s0)
     dc0:	00a40623          	sb	a0,12(s0)
     dc4:	00556b5b          	bnec	a0,5,dda <mscProtocol+0x10a>
     dc8:	8034                	exec.it	#25     !lw	a1,-924(gp) # 5fc0 <epIn>
     dca:	59a8                	c.lw	a0,112(a1)
     dcc:	4605                	c.li	a2,1
     dce:	00852803          	lw	a6,8(a0)
     dd2:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     dd4:	9802                	c.jalr	a6
     dd6:	00940623          	sb	s1,12(s0)
     dda:	c6c1a483          	lw	s1,-916(gp) # 5fc8 <host_num_of_bytes>
     dde:	e889                	c.bnez	s1,df0 <mscProtocol+0x120>
     de0:	6af020ab          	lwgp	ra,134828 # 27208 <dev_num_of_bytes>
     de4:	04008863          	beqz	ra,e34 <mscProtocol+0x164>
     de8:	4389                	c.li	t2,2
     dea:	00740623          	sb	t2,12(s0)
     dee:	a099                	c.j	e34 <mscProtocol+0x164>
     df0:	c551ce03          	lbu	t3,-939(gp) # 5fb1 <data_dir>
     df4:	c561c883          	lbu	a7,-938(gp) # 5fb2 <dir_error>
     df8:	060e0d63          	beqz	t3,e72 <mscProtocol+0x1a2>
     dfc:	00088a63          	beqz	a7,e10 <mscProtocol+0x140>
     e00:	8034                	exec.it	#25     !lw	a1,-924(gp) # 5fc0 <epIn>
     e02:	8630                	exec.it	#83     !lw	t0,112(a1)
     e04:	4605                	c.li	a2,1
     e06:	0082a703          	lw	a4,8(t0)
     e0a:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     e0c:	9702                	c.jalr	a4
     e0e:	bfe9                	c.j	de8 <mscProtocol+0x118>
     e10:	6af02f2b          	lwgp	t5,134828 # 27208 <dev_num_of_bytes>
     e14:	009f7a63          	bgeu	t5,s1,e28 <mscProtocol+0x158>
     e18:	8034                	exec.it	#25     !lw	a1,-924(gp) # 5fc0 <epIn>
     e1a:	59bc                	c.lw	a5,112(a1)
     e1c:	4605                	c.li	a2,1
     e1e:	0087a303          	lw	t1,8(a5)
     e22:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     e24:	9302                	c.jalr	t1
     e26:	a039                	c.j	e34 <mscProtocol+0x164>
     e28:	01e4f663          	bgeu	s1,t5,e34 <mscProtocol+0x164>
     e2c:	1ff4ff93          	andi	t6,s1,511
     e30:	fa0f8ce3          	beqz	t6,de8 <mscProtocol+0x118>
     e34:	c481a683          	lw	a3,-952(gp) # 5fa4 <bulkInReq>
     e38:	fffff597          	auipc	a1,0xfffff
     e3c:	72c58593          	addi	a1,a1,1836 # 564 <bulkInCmpl>
     e40:	4635                	c.li	a2,13
     e42:	c680                	c.sw	s0,8(a3)
     e44:	ca80                	c.sw	s0,16(a3)
     e46:	d68c                	c.sw	a1,40(a3)
     e48:	c6d0                	c.sw	a2,12(a3)
     e4a:	02069023          	sh	zero,32(a3)
     e4e:	8a70                	exec.it	#117     !jal	ra,474 <isr_Disable>
     e50:	8034                	exec.it	#25     !lw	a1,-924(gp) # 5fc0 <epIn>
     e52:	59a8                	c.lw	a0,112(a1)
     e54:	8600                	exec.it	#66     !lw	a2,-952(gp) # 5fa4 <bulkInReq>
     e56:	01852803          	lw	a6,24(a0)
     e5a:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     e5c:	9802                	c.jalr	a6
     e5e:	c62a                	c.swsp	a0,12(sp)
     e60:	8e00                	exec.it	#70     !jal	ra,47a <isr_Enable>
     e62:	44b2                	c.lwsp	s1,12(sp)
     e64:	f4ed                	c.bnez	s1,e4e <mscProtocol+0x17e>
     e66:	c7a1c883          	lbu	a7,-902(gp) # 5fd6 <packet_sent>
     e6a:	02088163          	beqz	a7,e8c <mscProtocol+0x1bc>
     e6e:	8a00                	exec.it	#68     !sb	zero,-902(gp) # 5fd6 <packet_sent>
     e70:	a015                	c.j	e94 <mscProtocol+0x1c4>
     e72:	00088463          	beqz	a7,e7a <mscProtocol+0x1aa>
     e76:	8c40                	exec.it	#38     !lw	a1,-920(gp) # 5fc4 <epOut>
     e78:	b769                	c.j	e02 <mscProtocol+0x132>
     e7a:	6af02eab          	lwgp	t4,134828 # 27208 <dev_num_of_bytes>
     e7e:	009ef463          	bgeu	t4,s1,e86 <mscProtocol+0x1b6>
     e82:	8c40                	exec.it	#38     !lw	a1,-920(gp) # 5fc4 <epOut>
     e84:	bf59                	c.j	e1a <mscProtocol+0x14a>
     e86:	fbd4f7e3          	bgeu	s1,t4,e34 <mscProtocol+0x164>
     e8a:	bfb9                	c.j	de8 <mscProtocol+0x118>
     e8c:	c541ce03          	lbu	t3,-940(gp) # 5fb0 <configValue>
     e90:	fc0e1be3          	bnez	t3,e66 <mscProtocol+0x196>
     e94:	4481                	c.li	s1,0
     e96:	bd51                	c.j	d2a <mscProtocol+0x5a>

00000e98 <mscAppInit>:
     e98:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     e9a:	1141                	c.addi	sp,-16
     e9c:	00005597          	auipc	a1,0x5
     ea0:	bdc58593          	addi	a1,a1,-1060 # 5a78 <_ITB_BASE_+0x3bc>
     ea4:	de418513          	addi	a0,gp,-540 # 6140 <vendorDesc>
     ea8:	8274                	exec.it	#121     !jal	ra,5de <get_unicode_string>
     eaa:	00005597          	auipc	a1,0x5
     eae:	b8e58593          	addi	a1,a1,-1138 # 5a38 <_ITB_BASE_+0x37c>
     eb2:	d8818513          	addi	a0,gp,-632 # 60e4 <productDesc>
     eb6:	8274                	exec.it	#121     !jal	ra,5de <get_unicode_string>
     eb8:	00005597          	auipc	a1,0x5
     ebc:	af458593          	addi	a1,a1,-1292 # 59ac <_ITB_BASE_+0x2f0>
     ec0:	dc818513          	addi	a0,gp,-568 # 6124 <serialDesc>
     ec4:	8274                	exec.it	#121     !jal	ra,5de <get_unicode_string>
     ec6:	d1c18293          	addi	t0,gp,-740 # 6078 <ep0BuffAlloc>
     eca:	0072f713          	andi	a4,t0,7
     ece:	c709                	c.beqz	a4,ed8 <mscAppInit+0x40>
     ed0:	40e287b3          	sub	a5,t0,a4
     ed4:	00878293          	addi	t0,a5,8
     ed8:	cf418393          	addi	t2,gp,-780 # 6050 <command_buffAlloc>
     edc:	c451ae23          	sw	t0,-932(gp) # 5fb8 <ep0Buff>
     ee0:	0073f093          	andi	ra,t2,7
     ee4:	00008663          	beqz	ra,ef0 <mscAppInit+0x58>
     ee8:	40138333          	sub	t1,t2,ra
     eec:	00830393          	addi	t2,t1,8
     ef0:	c471a823          	sw	t2,-944(gp) # 5fac <command_buff>
     ef4:	db418693          	addi	a3,gp,-588 # 6110 <response_buffAlloc>
     ef8:	0076f513          	andi	a0,a3,7
     efc:	c509                	c.beqz	a0,f06 <mscAppInit+0x6e>
     efe:	40a685b3          	sub	a1,a3,a0
     f02:	00858693          	addi	a3,a1,8
     f06:	c6d1ae23          	sw	a3,-900(gp) # 5fd8 <response_buff>
     f0a:	c581a603          	lw	a2,-936(gp) # 5fb4 <drv>
     f0e:	04062803          	lw	a6,64(a2)
     f12:	006c                	c.addi4spn	a1,sp,12
     f14:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     f16:	9802                	c.jalr	a6
     f18:	4eb2                	c.lwsp	t4,12(sp)
     f1a:	c581a883          	lw	a7,-936(gp) # 5fb4 <drv>
     f1e:	0388ae03          	lw	t3,56(a7)
     f22:	008ea583          	lw	a1,8(t4)
     f26:	c6018613          	addi	a2,gp,-928 # 5fbc <ep0Req>
     f2a:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     f2c:	9e02                	c.jalr	t3
     f2e:	4f32                	c.lwsp	t5,12(sp)
     f30:	4711                	c.li	a4,4
     f32:	010f2f83          	lw	t6,16(t5)
     f36:	01f76c63          	bltu	a4,t6,f4e <mscAppInit+0xb6>
     f3a:	47b1                	c.li	a5,12
     f3c:	4285                	c.li	t0,1
     f3e:	20100093          	li	ra,513
     f42:	c0f19323          	sh	a5,-1018(gp) # 5f62 <bosDesc+0x2>
     f46:	c0518423          	sb	t0,-1016(gp) # 5f64 <bosDesc+0x4>
     f4a:	ae119723          	sh	ra,-1298(gp) # 5e4a <devHsDesc+0x2>
     f4e:	2cbd                	c.jal	11cc <scsiInit>
     f50:	fffff397          	auipc	t2,0xfffff
     f54:	76438393          	addi	t2,t2,1892 # 6b4 <storage_to_usb>
     f58:	6a704dab          	swgp	t2,134840 # 27214 <storage_send_data>
     f5c:	fffff317          	auipc	t1,0xfffff
     f60:	7ca30313          	addi	t1,t1,1994 # 726 <send_data>
     f64:	fffff517          	auipc	a0,0xfffff
     f68:	6de50513          	addi	a0,a0,1758 # 642 <usb_to_storage>
     f6c:	46b2                	c.lwsp	a3,12(sp)
     f6e:	6a604bab          	swgp	t1,134836 # 27210 <scsi_send_data>
     f72:	6aa04fab          	swgp	a0,134844 # 27218 <storage_rec_data>
     f76:	428c                	c.lw	a1,0(a3)
     f78:	c641ae03          	lw	t3,-924(gp) # 5fc0 <epIn>
     f7c:	4881                	c.li	a7,0
     f7e:	08100613          	li	a2,129
     f82:	00b69763          	bne	a3,a1,f90 <mscAppInit+0xf8>
     f86:	02088363          	beqz	a7,fac <mscAppInit+0x114>
     f8a:	c7c1a223          	sw	t3,-924(gp) # 5fc0 <epIn>
     f8e:	a839                	c.j	fac <mscAppInit+0x114>
     f90:	07a5c803          	lbu	a6,122(a1)
     f94:	02c81963          	bne	a6,a2,fc6 <mscAppInit+0x12e>
     f98:	c581ae83          	lw	t4,-936(gp) # 5fb4 <drv>
     f9c:	038eaf03          	lw	t5,56(t4)
     fa0:	c4818613          	addi	a2,gp,-952 # 5fa4 <bulkInReq>
     fa4:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     fa6:	c6b1a223          	sw	a1,-924(gp) # 5fc0 <epIn>
     faa:	9f02                	c.jalr	t5
     fac:	4fb2                	c.lwsp	t6,12(sp)
     fae:	c681a783          	lw	a5,-920(gp) # 5fc4 <epOut>
     fb2:	000fa583          	lw	a1,0(t6)
     fb6:	4281                	c.li	t0,0
     fb8:	00bf9b63          	bne	t6,a1,fce <mscAppInit+0x136>
     fbc:	02028763          	beqz	t0,fea <mscAppInit+0x152>
     fc0:	c6f1a423          	sw	a5,-920(gp) # 5fc4 <epOut>
     fc4:	a01d                	c.j	fea <mscAppInit+0x152>
     fc6:	8e2e                	c.mv	t3,a1
     fc8:	4885                	c.li	a7,1
     fca:	418c                	c.lw	a1,0(a1)
     fcc:	bf5d                	c.j	f82 <mscAppInit+0xea>
     fce:	07a5c703          	lbu	a4,122(a1)
     fd2:	00176e5b          	bnec	a4,1,fee <mscAppInit+0x156>
     fd6:	c581a083          	lw	ra,-936(gp) # 5fb4 <drv>
     fda:	0380a303          	lw	t1,56(ra)
     fde:	c4c18613          	addi	a2,gp,-948 # 5fa8 <bulkOutReq>
     fe2:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
     fe4:	c6b1a423          	sw	a1,-920(gp) # 5fc4 <epOut>
     fe8:	9302                	c.jalr	t1
     fea:	0141                	c.addi	sp,16
     fec:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
     fee:	87ae                	c.mv	a5,a1
     ff0:	4285                	c.li	t0,1
     ff2:	418c                	c.lw	a1,0(a1)
     ff4:	b7d1                	c.j	fb8 <mscAppInit+0x120>

00000ff6 <mscApp>:
     ff6:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
     ff8:	c2c1a083          	lw	ra,-980(gp) # 5f88 <g_dbg_enable_log>
     ffc:	1141                	c.addi	sp,-16
     ffe:	06400793          	li	a5,100
    1002:	0780e293          	ori	t0,ra,120
    1006:	c2f1a823          	sw	a5,-976(gp) # 5f8c <g_dbg_log_lvl>
    100a:	c251a623          	sw	t0,-980(gp) # 5f88 <g_dbg_enable_log>
    100e:	525010ef          	jal	ra,2d32 <CUSBD_GetInstance>
    1012:	00052303          	lw	t1,0(a0)
    1016:	c4a1ac23          	sw	a0,-936(gp) # 5fb4 <drv>
    101a:	002c                	c.addi4spn	a1,sp,8
    101c:	a1418513          	addi	a0,gp,-1516 # 5d70 <config>
    1020:	9302                	c.jalr	t1
    1022:	e139                	c.bnez	a0,1068 <mscApp+0x72>
    1024:	4522                	c.lwsp	a0,8(sp)
    1026:	8454                	exec.it	#43     !jal	ra,4e7a <pvPortMalloc>
    1028:	c6a1aa23          	sw	a0,-908(gp) # 5fd0 <pD>
    102c:	cd15                	c.beqz	a0,1068 <mscApp+0x72>
    102e:	4532                	c.lwsp	a0,12(sp)
    1030:	8454                	exec.it	#43     !jal	ra,4e7a <pvPortMalloc>
    1032:	aea1a223          	sw	a0,-1308(gp) # 5e40 <config+0xd0>
    1036:	c90d                	c.beqz	a0,1068 <mscApp+0x72>
    1038:	aea1a423          	sw	a0,-1304(gp) # 5e44 <config+0xd4>
    103c:	c581a383          	lw	t2,-936(gp) # 5fb4 <drv>
    1040:	0043a683          	lw	a3,4(t2)
    1044:	9d018613          	addi	a2,gp,-1584 # 5d2c <callback>
    1048:	a1418593          	addi	a1,gp,-1516 # 5d70 <config>
    104c:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
    104e:	9682                	c.jalr	a3
    1050:	ed01                	c.bnez	a0,1068 <mscApp+0x72>
    1052:	3599                	c.jal	e98 <mscAppInit>
    1054:	c581a403          	lw	s0,-936(gp) # 5fb4 <drv>
    1058:	444c                	c.lw	a1,12(s0)
    105a:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
    105c:	9582                	c.jalr	a1
    105e:	c541c503          	lbu	a0,-940(gp) # 5fb0 <configValue>
    1062:	dd75                	c.beqz	a0,105e <mscApp+0x68>
    1064:	31b5                	c.jal	cd0 <mscProtocol>
    1066:	bffd                	c.j	1064 <mscApp+0x6e>
    1068:	8c10                	exec.it	#7     !lw	a0,-908(gp) # 5fd0 <pD>
    106a:	c111                	c.beqz	a0,106e <mscApp+0x78>
    106c:	8444                	exec.it	#42     !jal	ra,4f5e <vPortFree>
    106e:	ae41a503          	lw	a0,-1308(gp) # 5e40 <config+0xd0>
    1072:	c111                	c.beqz	a0,1076 <mscApp+0x80>
    1074:	8444                	exec.it	#42     !jal	ra,4f5e <vPortFree>
    1076:	4501                	c.li	a0,0
    1078:	0b1030ef          	jal	ra,4928 <vTaskDelete>
    107c:	0141                	c.addi	sp,16
    107e:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

00001080 <scsiInquiryCopyString>:
    1080:	962e                	c.add	a2,a1
    1082:	4781                	c.li	a5,0
    1084:	02000813          	li	a6,32
    1088:	05e00893          	li	a7,94
    108c:	8874                	exec.it	#61     !lbu	a4,0(a1)
    108e:	00173693          	seqz	a3,a4
    1092:	8fd5                	c.or	a5,a3
    1094:	ef91                	c.bnez	a5,10b0 <scsiInquiryCopyString+0x30>
    1096:	fe070293          	addi	t0,a4,-32
    109a:	0ff2f313          	andi	t1,t0,255
    109e:	0068e963          	bltu	a7,t1,10b0 <scsiInquiryCopyString+0x30>
    10a2:	00e50023          	sb	a4,0(a0)
    10a6:	0585                	c.addi	a1,1
    10a8:	0505                	c.addi	a0,1
    10aa:	feb611e3          	bne	a2,a1,108c <scsiInquiryCopyString+0xc>
    10ae:	8082                	c.jr	ra
    10b0:	01050023          	sb	a6,0(a0)
    10b4:	bfcd                	c.j	10a6 <scsiInquiryCopyString+0x26>

000010b6 <scsiWrite_10>:
    10b6:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    10b8:	00354783          	lbu	a5,3(a0)
    10bc:	00154703          	lbu	a4,1(a0)
    10c0:	00254303          	lbu	t1,2(a0)
    10c4:	00454803          	lbu	a6,4(a0)
    10c8:	ff010e37          	lui	t3,0xff010
    10cc:	86ae                	c.mv	a3,a1
    10ce:	00879593          	slli	a1,a5,0x8
    10d2:	00b363b3          	or	t2,t1,a1
    10d6:	72c1                	c.lui	t0,0xffff0
    10d8:	00575093          	srli	ra,a4,0x5
    10dc:	fffe0713          	addi	a4,t3,-1 # ff00ffff <_stack+0xfef6ffff>
    10e0:	01081893          	slli	a7,a6,0x10
    10e4:	00e3feb3          	and	t4,t2,a4
    10e8:	00554783          	lbu	a5,5(a0)
    10ec:	0ff28f13          	addi	t5,t0,255 # ffff00ff <_stack+0xfff500ff>
    10f0:	010002b7          	lui	t0,0x1000
    10f4:	011eefb3          	or	t6,t4,a7
    10f8:	fff28313          	addi	t1,t0,-1 # ffffff <_stack+0xf5ffff>
    10fc:	01879593          	slli	a1,a5,0x18
    1100:	006ff3b3          	and	t2,t6,t1
    1104:	00b3ee33          	or	t3,t2,a1
    1108:	018e1893          	slli	a7,t3,0x18
    110c:	018e5813          	srli	a6,t3,0x18
    1110:	0108eeb3          	or	t4,a7,a6
    1114:	008e1793          	slli	a5,t3,0x8
    1118:	00ff0fb7          	lui	t6,0xff0
    111c:	68c1                	c.lui	a7,0x10
    111e:	01f7f5b3          	and	a1,a5,t6
    1122:	008e5393          	srli	t2,t3,0x8
    1126:	f0088893          	addi	a7,a7,-256 # ff00 <__global_pointer$+0x9ba4>
    112a:	00bee2b3          	or	t0,t4,a1
    112e:	0113f833          	and	a6,t2,a7
    1132:	0102e5b3          	or	a1,t0,a6
    1136:	00754e83          	lbu	t4,7(a0)
    113a:	00854283          	lbu	t0,8(a0)
    113e:	f00e7e13          	andi	t3,t3,-256
    1142:	ee05                	c.bnez	a2,117a <scsiWrite_10+0xc4>
    1144:	01de6533          	or	a0,t3,t4
    1148:	02a2                	c.slli	t0,0x8
    114a:	01e57e33          	and	t3,a0,t5
    114e:	005e6333          	or	t1,t3,t0
    1152:	3c0327db          	bfoz	a5,t1,15,0
    1156:	0087d813          	srli	a6,a5,0x8
    115a:	20f7a8db          	bfoz	a7,a5,8,15
    115e:	01186633          	or	a2,a6,a7
    1162:	00961393          	slli	t2,a2,0x9
    1166:	6a7047ab          	swgp	t2,134828 # 27208 <dev_num_of_bytes>
    116a:	00026517          	auipc	a0,0x26
    116e:	0a050123          	sb	zero,162(a0) # 2720c <dev_data_dir>
    1172:	8506                	c.mv	a0,ra
    1174:	2b89                	c.jal	16c6 <devWriteSector>
    1176:	4501                	c.li	a0,0
    1178:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
    117a:	00654603          	lbu	a2,6(a0)
    117e:	008e9393          	slli	t2,t4,0x8
    1182:	00ce67b3          	or	a5,t3,a2
    1186:	01e7ff33          	and	t5,a5,t5
    118a:	007f6eb3          	or	t4,t5,t2
    118e:	00eef733          	and	a4,t4,a4
    1192:	01029813          	slli	a6,t0,0x10
    1196:	00954503          	lbu	a0,9(a0)
    119a:	010762b3          	or	t0,a4,a6
    119e:	01851e13          	slli	t3,a0,0x18
    11a2:	0062f333          	and	t1,t0,t1
    11a6:	01c367b3          	or	a5,t1,t3
    11aa:	01879613          	slli	a2,a5,0x18
    11ae:	0187d393          	srli	t2,a5,0x18
    11b2:	00879e93          	slli	t4,a5,0x8
    11b6:	00766f33          	or	t5,a2,t2
    11ba:	01feffb3          	and	t6,t4,t6
    11be:	0087d713          	srli	a4,a5,0x8
    11c2:	01ff6833          	or	a6,t5,t6
    11c6:	011778b3          	and	a7,a4,a7
    11ca:	bf51                	c.j	115e <scsiWrite_10+0xa8>

000011cc <scsiInit>:
    11cc:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    11ce:	2141                	c.jal	164e <devInit>
    11d0:	e0818293          	addi	t0,gp,-504 # 6164 <scsi_data_alloc>
    11d4:	0072f713          	andi	a4,t0,7
    11d8:	c709                	c.beqz	a4,11e2 <scsiInit+0x16>
    11da:	40e287b3          	sub	a5,t0,a4
    11de:	00878293          	addi	t0,a5,8
    11e2:	c851a223          	sw	t0,-892(gp) # 5fe0 <scsi_data>
    11e6:	de01ac23          	sw	zero,-520(gp) # 6154 <lastSenseCode>
    11ea:	de01ae23          	sw	zero,-516(gp) # 6158 <lastSenseCode+0x4>
    11ee:	e001a023          	sw	zero,-512(gp) # 615c <lastSenseCode+0x8>
    11f2:	e001a223          	sw	zero,-508(gp) # 6160 <lastSenseCode+0xc>
    11f6:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

000011f8 <scsiExecCmd>:
    11f8:	8830                	exec.it	#21     !jal	t0,52b6 <__riscv_save_4>
    11fa:	8650                	exec.it	#99     !sw	zero,0(a1)
    11fc:	00054783          	lbu	a5,0(a0)
    1200:	1101                	c.addi	sp,-32
    1202:	84aa                	c.mv	s1,a0
    1204:	892e                	c.mv	s2,a1
    1206:	3057dadb          	beqc	a5,37,151a <scsiExecCmd+0x322>
    120a:	02500713          	li	a4,37
    120e:	04f76c63          	bltu	a4,a5,1266 <scsiExecCmd+0x6e>
    1212:	49c9                	c.li	s3,18
    1214:	1f27d95b          	beqc	a5,18,1406 <scsiExecCmd+0x20e>
    1218:	00f9ee63          	bltu	s3,a5,1234 <scsiExecCmd+0x3c>
    121c:	1237d65b          	beqc	a5,3,1348 <scsiExecCmd+0x150>
    1220:	1c47d05b          	beqc	a5,4,13e0 <scsiExecCmd+0x1e8>
    1224:	42079363          	bnez	a5,164a <scsiExecCmd+0x452>
    1228:	00154503          	lbu	a0,1(a0)
    122c:	8115                	c.srli	a0,0x5
    122e:	2311                	c.jal	1732 <devTestUnitReady>
    1230:	842a                	c.mv	s0,a0
    1232:	aabd                	c.j	13b0 <scsiExecCmd+0x1b8>
    1234:	17e7dd5b          	beqc	a5,30,13ae <scsiExecCmd+0x1b6>
    1238:	2437d4db          	beqc	a5,35,1480 <scsiExecCmd+0x288>
    123c:	01a7d45b          	beqc	a5,26,1244 <scsiExecCmd+0x4c>
    1240:	40a0006f          	j	164a <scsiExecCmd+0x452>
    1244:	4411                	c.li	s0,4
    1246:	4611                	c.li	a2,4
    1248:	084c                	c.addi4spn	a1,sp,20
    124a:	8030                	exec.it	#17     !lw	a0,-892(gp) # 5fe0 <scsi_data>
    124c:	ca22                	c.swsp	s0,20(sp)
    124e:	8c20                	exec.it	#22     !jal	ra,5350 <memcpy>
    1250:	f8000513          	li	a0,-128
    1254:	6a8047ab          	swgp	s0,134828 # 27208 <dev_num_of_bytes>
    1258:	4591                	c.li	a1,4
    125a:	00026417          	auipc	s0,0x26
    125e:	faa40923          	sb	a0,-78(s0) # 2720c <dev_data_dir>
    1262:	8030                	exec.it	#17     !lw	a0,-892(gp) # 5fe0 <scsi_data>
    1264:	a281                	c.j	13a4 <scsiExecCmd+0x1ac>
    1266:	0a000713          	li	a4,160
    126a:	36e78f63          	beq	a5,a4,15e8 <scsiExecCmd+0x3f0>
    126e:	0af76f63          	bltu	a4,a5,132c <scsiExecCmd+0x134>
    1272:	32a7d2db          	beqc	a5,42,1596 <scsiExecCmd+0x39e>
    1276:	32f7d2db          	beqc	a5,47,159a <scsiExecCmd+0x3a2>
    127a:	0087d4db          	beqc	a5,40,1282 <scsiExecCmd+0x8a>
    127e:	3cc0006f          	j	164a <scsiExecCmd+0x452>
    1282:	0034c783          	lbu	a5,3(s1)
    1286:	0024c603          	lbu	a2,2(s1)
    128a:	0044c403          	lbu	s0,4(s1)
    128e:	00879093          	slli	ra,a5,0x8
    1292:	0054ce03          	lbu	t3,5(s1)
    1296:	01041513          	slli	a0,s0,0x10
    129a:	001668b3          	or	a7,a2,ra
    129e:	00a8eeb3          	or	t4,a7,a0
    12a2:	018e1693          	slli	a3,t3,0x18
    12a6:	00deef33          	or	t5,t4,a3
    12aa:	0074cf83          	lbu	t6,7(s1)
    12ae:	f00f7713          	andi	a4,t5,-256
    12b2:	0084c303          	lbu	t1,8(s1)
    12b6:	75c1                	c.lui	a1,0xffff0
    12b8:	01f762b3          	or	t0,a4,t6
    12bc:	0ff58813          	addi	a6,a1,255 # ffff00ff <_stack+0xfff500ff>
    12c0:	0102f0b3          	and	ra,t0,a6
    12c4:	00831793          	slli	a5,t1,0x8
    12c8:	00f0e5b3          	or	a1,ra,a5
    12cc:	f8000613          	li	a2,-128
    12d0:	3c05a85b          	bfoz	a6,a1,15,0
    12d4:	018f1e13          	slli	t3,t5,0x18
    12d8:	018f5693          	srli	a3,t5,0x18
    12dc:	008f1f93          	slli	t6,t5,0x8
    12e0:	00026417          	auipc	s0,0x26
    12e4:	f2c40623          	sb	a2,-212(s0) # 2720c <dev_data_dir>
    12e8:	00ff0737          	lui	a4,0xff0
    12ec:	67c1                	c.lui	a5,0x10
    12ee:	0014c383          	lbu	t2,1(s1)
    12f2:	00885413          	srli	s0,a6,0x8
    12f6:	20f8255b          	bfoz	a0,a6,8,15
    12fa:	00de6eb3          	or	t4,t3,a3
    12fe:	00eff2b3          	and	t0,t6,a4
    1302:	008f5f13          	srli	t5,t5,0x8
    1306:	f0078093          	addi	ra,a5,-256 # ff00 <__global_pointer$+0x9ba4>
    130a:	00856633          	or	a2,a0,s0
    130e:	005ee333          	or	t1,t4,t0
    1312:	001f75b3          	and	a1,t5,ra
    1316:	00961893          	slli	a7,a2,0x9
    131a:	86ca                	c.mv	a3,s2
    131c:	00b365b3          	or	a1,t1,a1
    1320:	0053d513          	srli	a0,t2,0x5
    1324:	6b1047ab          	swgp	a7,134828 # 27208 <dev_num_of_bytes>
    1328:	26b9                	c.jal	1676 <devReadSector>
    132a:	a051                	c.j	13ae <scsiExecCmd+0x1b6>
    132c:	0a800713          	li	a4,168
    1330:	f4e789e3          	beq	a5,a4,1282 <scsiExecCmd+0x8a>
    1334:	0aa00293          	li	t0,170
    1338:	24578c63          	beq	a5,t0,1590 <scsiExecCmd+0x398>
    133c:	0a100313          	li	t1,161
    1340:	30679563          	bne	a5,t1,164a <scsiExecCmd+0x452>
    1344:	4405                	c.li	s0,1
    1346:	a0ad                	c.j	13b0 <scsiExecCmd+0x1b8>
    1348:	00154403          	lbu	s0,1(a0)
    134c:	4649                	c.li	a2,18
    134e:	4581                	c.li	a1,0
    1350:	8030                	exec.it	#17     !lw	a0,-892(gp) # 5fe0 <scsi_data>
    1352:	8430                	exec.it	#19     !jal	ra,5526 <memset>
    1354:	8015                	c.srli	s0,0x5
    1356:	df818693          	addi	a3,gp,-520 # 6154 <lastSenseCode>
    135a:	0a86885b          	lea.h	a6,a3,s0
    135e:	00085883          	lhu	a7,0(a6)
    1362:	8030                	exec.it	#17     !lw	a0,-892(gp) # 5fe0 <scsi_data>
    1364:	07000613          	li	a2,112
    1368:	00f8fe13          	andi	t3,a7,15
    136c:	4ea9                	c.li	t4,10
    136e:	0088df13          	srli	t5,a7,0x8
    1372:	00c50023          	sb	a2,0(a0)
    1376:	01c50123          	sb	t3,2(a0)
    137a:	01d503a3          	sb	t4,7(a0)
    137e:	01e50623          	sb	t5,12(a0)
    1382:	00448783          	lb	a5,4(s1)
    1386:	0ff7ff93          	andi	t6,a5,255
    138a:	01f9f363          	bgeu	s3,t6,1390 <scsiExecCmd+0x198>
    138e:	47c9                	c.li	a5,18
    1390:	0ff7f593          	andi	a1,a5,255
    1394:	f8000713          	li	a4,-128
    1398:	6ab047ab          	swgp	a1,134828 # 27208 <dev_num_of_bytes>
    139c:	00026417          	auipc	s0,0x26
    13a0:	e6e40823          	sb	a4,-400(s0) # 2720c <dev_data_dir>
    13a4:	6b70242b          	lwgp	s0,134836 # 27210 <scsi_send_data>
    13a8:	9402                	c.jalr	s0
    13aa:	00a92023          	sw	a0,0(s2)
    13ae:	4401                	c.li	s0,0
    13b0:	0014c503          	lbu	a0,1(s1)
    13b4:	459d                	c.li	a1,7
    13b6:	00555f13          	srli	t5,a0,0x5
    13ba:	df818793          	addi	a5,gp,-520 # 6154 <lastSenseCode>
    13be:	001f1393          	slli	t2,t5,0x1
    13c2:	2885e063          	bltu	a1,s0,1642 <scsiExecCmd+0x44a>
    13c6:	00004817          	auipc	a6,0x4
    13ca:	72e80813          	addi	a6,a6,1838 # 5af4 <_ITB_BASE_+0x438>
    13ce:	0c88065b          	lea.w	a2,a6,s0
    13d2:	00062883          	lw	a7,0(a2)
    13d6:	00778fb3          	add	t6,a5,t2
    13da:	01088e33          	add	t3,a7,a6
    13de:	8e02                	c.jr	t3
    13e0:	00154283          	lbu	t0,1(a0)
    13e4:	4419                	c.li	s0,6
    13e6:	01f2f313          	andi	t1,t0,31
    13ea:	bd73635b          	bnec	t1,23,13b0 <scsiExecCmd+0x1b8>
    13ee:	00355083          	lhu	ra,3(a0)
    13f2:	0052d513          	srli	a0,t0,0x5
    13f6:	0080d593          	srli	a1,ra,0x8
    13fa:	20f0a3db          	bfoz	t2,ra,8,15
    13fe:	00b3e5b3          	or	a1,t2,a1
    1402:	262d                	c.jal	172c <devFormatUnit>
    1404:	b535                	c.j	1230 <scsiExecCmd+0x38>
    1406:	02400613          	li	a2,36
    140a:	4581                	c.li	a1,0
    140c:	8030                	exec.it	#17     !lw	a0,-892(gp) # 5fe0 <scsi_data>
    140e:	8430                	exec.it	#19     !jal	ra,5526 <memset>
    1410:	00028537          	lui	a0,0x28
    1414:	c841a403          	lw	s0,-892(gp) # 5fe0 <scsi_data>
    1418:	46fd                	c.li	a3,31
    141a:	4889                	c.li	a7,2
    141c:	c008                	c.sw	a0,0(s0)
    141e:	461d                	c.li	a2,7
    1420:	00004597          	auipc	a1,0x4
    1424:	64c58593          	addi	a1,a1,1612 # 5a6c <_ITB_BASE_+0x3b0>
    1428:	00840513          	addi	a0,s0,8
    142c:	00d40223          	sb	a3,4(s0)
    1430:	011403a3          	sb	a7,7(s0)
    1434:	31b1                	c.jal	1080 <scsiInquiryCopyString>
    1436:	4631                	c.li	a2,12
    1438:	00004597          	auipc	a1,0x4
    143c:	4d058593          	addi	a1,a1,1232 # 5908 <_ITB_BASE_+0x24c>
    1440:	8464                	exec.it	#58     !addi	a0,s0,16
    1442:	393d                	c.jal	1080 <scsiInquiryCopyString>
    1444:	4611                	c.li	a2,4
    1446:	00004597          	auipc	a1,0x4
    144a:	4f658593          	addi	a1,a1,1270 # 593c <_ITB_BASE_+0x280>
    144e:	02040513          	addi	a0,s0,32
    1452:	313d                	c.jal	1080 <scsiInquiryCopyString>
    1454:	00448f03          	lb	t5,4(s1)
    1458:	02400e93          	li	t4,36
    145c:	0fff7e13          	andi	t3,t5,255
    1460:	01cef463          	bgeu	t4,t3,1468 <scsiExecCmd+0x270>
    1464:	02400f13          	li	t5,36
    1468:	0fff7593          	andi	a1,t5,255
    146c:	f8000f93          	li	t6,-128
    1470:	6ab047ab          	swgp	a1,134828 # 27208 <dev_num_of_bytes>
    1474:	00026697          	auipc	a3,0x26
    1478:	d9f68c23          	sb	t6,-616(a3) # 2720c <dev_data_dir>
    147c:	8522                	c.mv	a0,s0
    147e:	b71d                	c.j	13a4 <scsiExecCmd+0x1ac>
    1480:	42a1                	c.li	t0,8
    1482:	4309                	c.li	t1,2
    1484:	ca02                	c.swsp	zero,20(sp)
    1486:	ce02                	c.swsp	zero,28(sp)
    1488:	4631                	c.li	a2,12
    148a:	084c                	c.addi4spn	a1,sp,20
    148c:	8030                	exec.it	#17     !lw	a0,-892(gp) # 5fe0 <scsi_data>
    148e:	00510ba3          	sb	t0,23(sp)
    1492:	00610e23          	sb	t1,28(sp)
    1496:	cc02                	c.swsp	zero,24(sp)
    1498:	8c20                	exec.it	#22     !jal	ra,5350 <memcpy>
    149a:	0014c383          	lbu	t2,1(s1)
    149e:	00e10613          	addi	a2,sp,14
    14a2:	080c                	c.addi4spn	a1,sp,16
    14a4:	0053d513          	srli	a0,t2,0x5
    14a8:	2a65                	c.jal	1660 <devGetCapacities>
    14aa:	842a                	c.mv	s0,a0
    14ac:	f00512e3          	bnez	a0,13b0 <scsiExecCmd+0x1b8>
    14b0:	40c2                	c.lwsp	ra,16(sp)
    14b2:	00ff08b7          	lui	a7,0xff0
    14b6:	0180d613          	srli	a2,ra,0x18
    14ba:	01809593          	slli	a1,ra,0x18
    14be:	00809693          	slli	a3,ra,0x8
    14c2:	00c5e833          	or	a6,a1,a2
    14c6:	0116fe33          	and	t3,a3,a7
    14ca:	00e15303          	lhu	t1,14(sp)
    14ce:	01c86eb3          	or	t4,a6,t3
    14d2:	6fc1                	c.lui	t6,0x10
    14d4:	4831                	c.li	a6,12
    14d6:	0080df13          	srli	t5,ra,0x8
    14da:	f00f8793          	addi	a5,t6,-256 # ff00 <__global_pointer$+0x9ba4>
    14de:	00831393          	slli	t2,t1,0x8
    14e2:	00835093          	srli	ra,t1,0x8
    14e6:	6b0047ab          	swgp	a6,134828 # 27208 <dev_num_of_bytes>
    14ea:	8030                	exec.it	#17     !lw	a0,-892(gp) # 5fe0 <scsi_data>
    14ec:	0013e5b3          	or	a1,t2,ra
    14f0:	00ff7733          	and	a4,t5,a5
    14f4:	f8000613          	li	a2,-128
    14f8:	00eee2b3          	or	t0,t4,a4
    14fc:	00b51523          	sh	a1,10(a0) # 2800a <_end+0xd9a>
    1500:	00026697          	auipc	a3,0x26
    1504:	d0c68623          	sb	a2,-756(a3) # 2720c <dev_data_dir>
    1508:	45b1                	c.li	a1,12
    150a:	00552223          	sw	t0,4(a0)
    150e:	6b70232b          	lwgp	t1,134836 # 27210 <scsi_send_data>
    1512:	9302                	c.jalr	t1
    1514:	00a92023          	sw	a0,0(s2)
    1518:	bd61                	c.j	13b0 <scsiExecCmd+0x1b8>
    151a:	00154503          	lbu	a0,1(a0)
    151e:	0810                	c.addi4spn	a2,sp,16
    1520:	084c                	c.addi4spn	a1,sp,20
    1522:	8115                	c.srli	a0,0x5
    1524:	2a35                	c.jal	1660 <devGetCapacities>
    1526:	842a                	c.mv	s0,a0
    1528:	e80514e3          	bnez	a0,13b0 <scsiExecCmd+0x1b8>
    152c:	4e52                	c.lwsp	t3,20(sp)
    152e:	00ff0337          	lui	t1,0xff0
    1532:	fffe0e93          	addi	t4,t3,-1
    1536:	018e9f93          	slli	t6,t4,0x18
    153a:	018ed693          	srli	a3,t4,0x18
    153e:	008e9293          	slli	t0,t4,0x8
    1542:	65c1                	c.lui	a1,0x10
    1544:	00dfe733          	or	a4,t6,a3
    1548:	0062ff33          	and	t5,t0,t1
    154c:	008ed793          	srli	a5,t4,0x8
    1550:	f0058393          	addi	t2,a1,-256 # ff00 <__global_pointer$+0x9ba4>
    1554:	01e760b3          	or	ra,a4,t5
    1558:	0077f833          	and	a6,a5,t2
    155c:	8030                	exec.it	#17     !lw	a0,-892(gp) # 5fe0 <scsi_data>
    155e:	0100e633          	or	a2,ra,a6
    1562:	c110                	c.sw	a2,0(a0)
    1564:	01015883          	lhu	a7,16(sp)
    1568:	4721                	c.li	a4,8
    156a:	00889e13          	slli	t3,a7,0x8
    156e:	006e7eb3          	and	t4,t3,t1
    1572:	01889f93          	slli	t6,a7,0x18
    1576:	01fee6b3          	or	a3,t4,t6
    157a:	f8000293          	li	t0,-128
    157e:	c154                	c.sw	a3,4(a0)
    1580:	6ae047ab          	swgp	a4,134828 # 27208 <dev_num_of_bytes>
    1584:	00026697          	auipc	a3,0x26
    1588:	c8568423          	sb	t0,-888(a3) # 2720c <dev_data_dir>
    158c:	45a1                	c.li	a1,8
    158e:	b741                	c.j	150e <scsiExecCmd+0x316>
    1590:	4605                	c.li	a2,1
    1592:	3615                	c.jal	10b6 <scsiWrite_10>
    1594:	b971                	c.j	1230 <scsiExecCmd+0x38>
    1596:	4601                	c.li	a2,0
    1598:	bfed                	c.j	1592 <scsiExecCmd+0x39a>
    159a:	00252783          	lw	a5,2(a0)
    159e:	00755283          	lhu	t0,7(a0)
    15a2:	0187d593          	srli	a1,a5,0x18
    15a6:	00879613          	slli	a2,a5,0x8
    15aa:	01879093          	slli	ra,a5,0x18
    15ae:	00ff0437          	lui	s0,0xff0
    15b2:	66c1                	c.lui	a3,0x10
    15b4:	00867533          	and	a0,a2,s0
    15b8:	00b0e833          	or	a6,ra,a1
    15bc:	0087de13          	srli	t3,a5,0x8
    15c0:	f0068e93          	addi	t4,a3,-256 # ff00 <__global_pointer$+0x9ba4>
    15c4:	0014cf83          	lbu	t6,1(s1)
    15c8:	00a868b3          	or	a7,a6,a0
    15cc:	0082d313          	srli	t1,t0,0x8
    15d0:	20f2a3db          	bfoz	t2,t0,8,15
    15d4:	01de7f33          	and	t5,t3,t4
    15d8:	0063e633          	or	a2,t2,t1
    15dc:	01e8e5b3          	or	a1,a7,t5
    15e0:	005fd513          	srli	a0,t6,0x5
    15e4:	2a0d                	c.jal	1716 <devVerifySector>
    15e6:	b1a9                	c.j	1230 <scsiExecCmd+0x38>
    15e8:	02400613          	li	a2,36
    15ec:	4581                	c.li	a1,0
    15ee:	8030                	exec.it	#17     !lw	a0,-892(gp) # 5fe0 <scsi_data>
    15f0:	8430                	exec.it	#19     !jal	ra,5526 <memset>
    15f2:	8030                	exec.it	#17     !lw	a0,-892(gp) # 5fe0 <scsi_data>
    15f4:	020003b7          	lui	t2,0x2000
    15f8:	00752023          	sw	t2,0(a0)
    15fc:	01000837          	lui	a6,0x1000
    1600:	4631                	c.li	a2,12
    1602:	f8000893          	li	a7,-128
    1606:	01052423          	sw	a6,8(a0)
    160a:	6ac047ab          	swgp	a2,134828 # 27208 <dev_num_of_bytes>
    160e:	00026417          	auipc	s0,0x26
    1612:	bf140f23          	sb	a7,-1026(s0) # 2720c <dev_data_dir>
    1616:	45b1                	c.li	a1,12
    1618:	b371                	c.j	13a4 <scsiExecCmd+0x1ac>
    161a:	000f9023          	sh	zero,0(t6)
    161e:	8522                	c.mv	a0,s0
    1620:	6105                	c.addi16sp	sp,32
    1622:	8024                	exec.it	#24     !j	52ea <__riscv_restore_4>
    1624:	4695                	c.li	a3,5
    1626:	00df9023          	sh	a3,0(t6)
    162a:	bfd5                	c.j	161e <scsiExecCmd+0x426>
    162c:	6e89                	c.lui	t4,0x2
    162e:	10de8693          	addi	a3,t4,269 # 210d <sdudcEpEnable+0x14f>
    1632:	bfd5                	c.j	1626 <scsiExecCmd+0x42e>
    1634:	40200693          	li	a3,1026
    1638:	b7fd                	c.j	1626 <scsiExecCmd+0x42e>
    163a:	46b9                	c.li	a3,14
    163c:	b7ed                	c.j	1626 <scsiExecCmd+0x42e>
    163e:	4691                	c.li	a3,4
    1640:	b7dd                	c.j	1626 <scsiExecCmd+0x42e>
    1642:	00778fb3          	add	t6,a5,t2
    1646:	46ad                	c.li	a3,11
    1648:	bff9                	c.j	1626 <scsiExecCmd+0x42e>
    164a:	4415                	c.li	s0,5
    164c:	bfc9                	c.j	161e <scsiExecCmd+0x426>

0000164e <devInit>:
    164e:	4785                	c.li	a5,1
    1650:	c8f18423          	sb	a5,-888(gp) # 5fe4 <initialized>
    1654:	4501                	c.li	a0,0
    1656:	8082                	c.jr	ra

00001658 <devGetMaxLun>:
    1658:	00050023          	sb	zero,0(a0)
    165c:	4501                	c.li	a0,0
    165e:	8082                	c.jr	ra

00001660 <devGetCapacities>:
    1660:	e909                	c.bnez	a0,1672 <devGetCapacities+0x12>
    1662:	04000793          	li	a5,64
    1666:	c19c                	c.sw	a5,0(a1)
    1668:	20000293          	li	t0,512
    166c:	00561023          	sh	t0,0(a2)
    1670:	8082                	c.jr	ra
    1672:	4505                	c.li	a0,1
    1674:	8082                	c.jr	ra

00001676 <devReadSector>:
    1676:	e531                	c.bnez	a0,16c2 <devReadSector+0x4c>
    1678:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    167a:	00c587b3          	add	a5,a1,a2
    167e:	04000713          	li	a4,64
    1682:	1141                	c.addi	sp,-16
    1684:	842e                	c.mv	s0,a1
    1686:	4509                	c.li	a0,2
    1688:	00f76663          	bltu	a4,a5,1694 <devReadSector+0x1e>
    168c:	07f00913          	li	s2,127
    1690:	e601                	c.bnez	a2,1698 <devReadSector+0x22>
    1692:	4501                	c.li	a0,0
    1694:	0141                	c.addi	sp,16
    1696:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    1698:	08000493          	li	s1,128
    169c:	00c96363          	bltu	s2,a2,16a2 <devReadSector+0x2c>
    16a0:	84b2                	c.mv	s1,a2
    16a2:	8522                	c.mv	a0,s0
    16a4:	85a6                	c.mv	a1,s1
    16a6:	6bb0232b          	lwgp	t1,134840 # 27214 <storage_send_data>
    16aa:	c636                	c.swsp	a3,12(sp)
    16ac:	c432                	c.swsp	a2,8(sp)
    16ae:	9302                	c.jalr	t1
    16b0:	46b2                	c.lwsp	a3,12(sp)
    16b2:	4622                	c.lwsp	a2,8(sp)
    16b4:	0006a283          	lw	t0,0(a3)
    16b8:	8e05                	c.sub	a2,s1
    16ba:	9516                	c.add	a0,t0
    16bc:	c288                	c.sw	a0,0(a3)
    16be:	9426                	c.add	s0,s1
    16c0:	bfc1                	c.j	1690 <devReadSector+0x1a>
    16c2:	4505                	c.li	a0,1
    16c4:	8082                	c.jr	ra

000016c6 <devWriteSector>:
    16c6:	e531                	c.bnez	a0,1712 <devWriteSector+0x4c>
    16c8:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    16ca:	00c587b3          	add	a5,a1,a2
    16ce:	04000713          	li	a4,64
    16d2:	1141                	c.addi	sp,-16
    16d4:	842e                	c.mv	s0,a1
    16d6:	4509                	c.li	a0,2
    16d8:	00f76663          	bltu	a4,a5,16e4 <devWriteSector+0x1e>
    16dc:	07f00913          	li	s2,127
    16e0:	e601                	c.bnez	a2,16e8 <devWriteSector+0x22>
    16e2:	4501                	c.li	a0,0
    16e4:	0141                	c.addi	sp,16
    16e6:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    16e8:	08000493          	li	s1,128
    16ec:	00c96363          	bltu	s2,a2,16f2 <devWriteSector+0x2c>
    16f0:	84b2                	c.mv	s1,a2
    16f2:	8522                	c.mv	a0,s0
    16f4:	85a6                	c.mv	a1,s1
    16f6:	6bf0232b          	lwgp	t1,134844 # 27218 <storage_rec_data>
    16fa:	c636                	c.swsp	a3,12(sp)
    16fc:	c432                	c.swsp	a2,8(sp)
    16fe:	9302                	c.jalr	t1
    1700:	46b2                	c.lwsp	a3,12(sp)
    1702:	4622                	c.lwsp	a2,8(sp)
    1704:	0006a283          	lw	t0,0(a3)
    1708:	8e05                	c.sub	a2,s1
    170a:	9516                	c.add	a0,t0
    170c:	c288                	c.sw	a0,0(a3)
    170e:	9426                	c.add	s0,s1
    1710:	bfc1                	c.j	16e0 <devWriteSector+0x1a>
    1712:	4505                	c.li	a0,1
    1714:	8082                	c.jr	ra

00001716 <devVerifySector>:
    1716:	e909                	c.bnez	a0,1728 <devVerifySector+0x12>
    1718:	00c58533          	add	a0,a1,a2
    171c:	8060                	exec.it	#48     !li	a1,64
    171e:	00a5b2b3          	sltu	t0,a1,a0
    1722:	00129513          	slli	a0,t0,0x1
    1726:	8082                	c.jr	ra
    1728:	4505                	c.li	a0,1
    172a:	8082                	c.jr	ra

0000172c <devFormatUnit>:
    172c:	00a03533          	snez	a0,a0
    1730:	8082                	c.jr	ra

00001732 <devTestUnitReady>:
    1732:	c881c783          	lbu	a5,-888(gp) # 5fe4 <initialized>
    1736:	4501                	c.li	a0,0
    1738:	e391                	c.bnez	a5,173c <devTestUnitReady+0xa>
    173a:	450d                	c.li	a0,3
    173c:	8082                	c.jr	ra

0000173e <sdudcEp0DataReceive>:
    173e:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    1740:	6785                	c.lui	a5,0x1
    1742:	00f500b3          	add	ra,a0,a5
    1746:	6080a483          	lw	s1,1544(ra)
    174a:	842a                	c.mv	s0,a0
    174c:	58d8                	c.lw	a4,52(s1)
    174e:	c5c0a583          	lw	a1,-932(ra)
    1752:	6100a503          	lw	a0,1552(ra)
    1756:	9702                	c.jalr	a4
    1758:	21042683          	lw	a3,528(s0)
    175c:	21040293          	addi	t0,s0,528
    1760:	02568863          	beq	a3,t0,1790 <sdudcEp0DataReceive+0x52>
    1764:	46d8                	c.lw	a4,12(a3)
    1766:	00e57363          	bgeu	a0,a4,176c <sdudcEp0DataReceive+0x2e>
    176a:	872a                	c.mv	a4,a0
    176c:	6505                	c.lui	a0,0x1
    176e:	00a40333          	add	t1,s0,a0
    1772:	438d                	c.li	t2,3
    1774:	60732223          	sw	t2,1540(t1) # ff0604 <_stack+0xf50604>
    1778:	c3835603          	lhu	a2,-968(t1)
    177c:	0244a803          	lw	a6,36(s1)
    1780:	4a94                	c.lw	a3,16(a3)
    1782:	c5c32583          	lw	a1,-932(t1)
    1786:	61032503          	lw	a0,1552(t1)
    178a:	4781                	c.li	a5,0
    178c:	9802                	c.jalr	a6
    178e:	8c14                	exec.it	#15     !j	52f4 <__riscv_restore_0>
    1790:	00c02783          	lw	a5,12(zero) # c <_start>
    1794:	9002                	c.ebreak

00001796 <sdudcEpXDataReceive>:
    1796:	8040                	exec.it	#32     !jal	t0,52b6 <__riscv_save_4>
    1798:	6985                	c.lui	s3,0x1
    179a:	99aa                	c.add	s3,a0
    179c:	0385a903          	lw	s2,56(a1)
    17a0:	6089aa03          	lw	s4,1544(s3) # 1608 <scsiExecCmd+0x410>
    17a4:	84aa                	c.mv	s1,a0
    17a6:	034a2783          	lw	a5,52(s4)
    17aa:	842e                	c.mv	s0,a1
    17ac:	8440                	exec.it	#34     !lw	a0,1552(s3)
    17ae:	8c60                	exec.it	#54     !lw	a1,152(s2)
    17b0:	9782                	c.jalr	a5
    17b2:	07c92283          	lw	t0,124(s2)
    17b6:	8b2a                	c.mv	s6,a0
    17b8:	02ca2303          	lw	t1,44(s4)
    17bc:	8c60                	exec.it	#54     !lw	a1,152(s2)
    17be:	8440                	exec.it	#34     !lw	a0,1552(s3)
    17c0:	0032ca83          	lbu	s5,3(t0)
    17c4:	9302                	c.jalr	t1
    17c6:	4389                	c.li	t2,2
    17c8:	00a3fa63          	bgeu	t2,a0,17dc <sdudcEpXDataReceive+0x46>
    17cc:	1504c083          	lbu	ra,336(s1)
    17d0:	02008e63          	beqz	ra,180c <sdudcEpXDataReceive+0x76>
    17d4:	003afa93          	andi	s5,s5,3
    17d8:	021aea5b          	bnec	s5,1,180c <sdudcEpXDataReceive+0x76>
    17dc:	5854                	c.lw	a3,52(s0)
    17de:	4458                	c.lw	a4,12(s0)
    17e0:	00e6e563          	bltu	a3,a4,17ea <sdudcEpXDataReceive+0x54>
    17e4:	04044503          	lbu	a0,64(s0)
    17e8:	c115                	c.beqz	a0,180c <sdudcEpXDataReceive+0x76>
    17ea:	04040023          	sb	zero,64(s0)
    17ee:	8f15                	c.sub	a4,a3
    17f0:	00eb7363          	bgeu	s6,a4,17f6 <sdudcEpXDataReceive+0x60>
    17f4:	875a                	c.mv	a4,s6
    17f6:	4810                	c.lw	a2,16(s0)
    17f8:	6585                	c.lui	a1,0x1
    17fa:	94ae                	c.add	s1,a1
    17fc:	96b2                	c.add	a3,a2
    17fe:	8624                	exec.it	#90     !lw	a6,36(s4)
    1800:	07495603          	lhu	a2,116(s2)
    1804:	8c60                	exec.it	#54     !lw	a1,152(s2)
    1806:	8450                	exec.it	#35     !lw	a0,1552(s1)
    1808:	4781                	c.li	a5,0
    180a:	9802                	c.jalr	a6
    180c:	8050                	exec.it	#33     !j	52ea <__riscv_restore_4>

0000180e <sdudcEpXDataSend>:
    180e:	8040                	exec.it	#32     !jal	t0,52b6 <__riscv_save_4>
    1810:	6985                	c.lui	s3,0x1
    1812:	99aa                	c.add	s3,a0
    1814:	5d84                	c.lw	s1,56(a1)
    1816:	6089aa03          	lw	s4,1544(s3) # 1608 <scsiExecCmd+0x410>
    181a:	842a                	c.mv	s0,a0
    181c:	034a2783          	lw	a5,52(s4)
    1820:	892e                	c.mv	s2,a1
    1822:	8440                	exec.it	#34     !lw	a0,1552(s3)
    1824:	8854                	exec.it	#45     !lw	a1,152(s1)
    1826:	9782                	c.jalr	a5
    1828:	07c4a283          	lw	t0,124(s1)
    182c:	8b2a                	c.mv	s6,a0
    182e:	02ca2303          	lw	t1,44(s4)
    1832:	8854                	exec.it	#45     !lw	a1,152(s1)
    1834:	8440                	exec.it	#34     !lw	a0,1552(s3)
    1836:	0032ca83          	lbu	s5,3(t0)
    183a:	9302                	c.jalr	t1
    183c:	4389                	c.li	t2,2
    183e:	00a3fa63          	bgeu	t2,a0,1852 <sdudcEpXDataSend+0x44>
    1842:	15044083          	lbu	ra,336(s0)
    1846:	02008c63          	beqz	ra,187e <sdudcEpXDataSend+0x70>
    184a:	003afa93          	andi	s5,s5,3
    184e:	021ae85b          	bnec	s5,1,187e <sdudcEpXDataSend+0x70>
    1852:	03492683          	lw	a3,52(s2)
    1856:	00c92703          	lw	a4,12(s2)
    185a:	8f15                	c.sub	a4,a3
    185c:	00eb7363          	bgeu	s6,a4,1862 <sdudcEpXDataSend+0x54>
    1860:	875a                	c.mv	a4,s6
    1862:	01092603          	lw	a2,16(s2)
    1866:	6505                	c.lui	a0,0x1
    1868:	942a                	c.add	s0,a0
    186a:	04090023          	sb	zero,64(s2)
    186e:	96b2                	c.add	a3,a2
    1870:	8624                	exec.it	#90     !lw	a6,36(s4)
    1872:	0744d603          	lhu	a2,116(s1)
    1876:	8854                	exec.it	#45     !lw	a1,152(s1)
    1878:	8844                	exec.it	#44     !lw	a0,1552(s0)
    187a:	4781                	c.li	a5,0
    187c:	9802                	c.jalr	a6
    187e:	8050                	exec.it	#33     !j	52ea <__riscv_restore_4>

00001880 <sdudcFifoStatus>:
    1880:	47d9                	c.li	a5,22
    1882:	c199                	c.beqz	a1,1888 <sdudcFifoStatus+0x8>
    1884:	c111                	c.beqz	a0,1888 <sdudcFifoStatus+0x8>
    1886:	4781                	c.li	a5,0
    1888:	853e                	c.mv	a0,a5
    188a:	8082                	c.jr	ra

0000188c <sdudcFifoFlush>:
    188c:	8082                	c.jr	ra

0000188e <sdudcGetDevInstance>:
    188e:	c119                	c.beqz	a0,1894 <sdudcGetDevInstance+0x6>
    1890:	c191                	c.beqz	a1,1894 <sdudcGetDevInstance+0x6>
    1892:	c188                	c.sw	a0,0(a1)
    1894:	8082                	c.jr	ra

00001896 <sdudcWakeUp>:
    1896:	c119                	c.beqz	a0,189c <sdudcWakeUp+0x6>
    1898:	8c44                	exec.it	#46     !li	a0,134
    189a:	8082                	c.jr	ra
    189c:	4559                	c.li	a0,22
    189e:	8082                	c.jr	ra

000018a0 <sdudcSetSelfPowered>:
    18a0:	c901                	c.beqz	a0,18b0 <sdudcSetSelfPowered+0x10>
    18a2:	6785                	c.lui	a5,0x1
    18a4:	953e                	c.add	a0,a5
    18a6:	4285                	c.li	t0,1
    18a8:	605506a3          	sb	t0,1549(a0) # 160d <scsiExecCmd+0x415>
    18ac:	4501                	c.li	a0,0
    18ae:	8082                	c.jr	ra
    18b0:	4559                	c.li	a0,22
    18b2:	8082                	c.jr	ra

000018b4 <sdudcClearSelfPowered>:
    18b4:	c519                	c.beqz	a0,18c2 <sdudcClearSelfPowered+0xe>
    18b6:	6785                	c.lui	a5,0x1
    18b8:	953e                	c.add	a0,a5
    18ba:	600506a3          	sb	zero,1549(a0)
    18be:	4501                	c.li	a0,0
    18c0:	8082                	c.jr	ra
    18c2:	4559                	c.li	a0,22
    18c4:	8082                	c.jr	ra

000018c6 <sdudcVbusSession>:
    18c6:	c119                	c.beqz	a0,18cc <sdudcVbusSession+0x6>
    18c8:	8c44                	exec.it	#46     !li	a0,134
    18ca:	8082                	c.jr	ra
    18cc:	4559                	c.li	a0,22
    18ce:	8082                	c.jr	ra

000018d0 <sdudcIoctl>:
    18d0:	c119                	c.beqz	a0,18d6 <sdudcIoctl+0x6>
    18d2:	8c44                	exec.it	#46     !li	a0,134
    18d4:	8082                	c.jr	ra
    18d6:	4559                	c.li	a0,22
    18d8:	8082                	c.jr	ra

000018da <sdudcEpXSetHalt>:
    18da:	1cf032ef          	jal	t0,52a8 <__riscv_save_10>
    18de:	5dfc                	c.lw	a5,124(a1)
    18e0:	44d9                	c.li	s1,22
    18e2:	00378283          	lb	t0,3(a5) # 1003 <mscApp+0xd>
    18e6:	0032f313          	andi	t1,t0,3
    18ea:	0813535b          	beqc	t1,1,1970 <sdudcEpXSetHalt+0x96>
    18ee:	08c5ab03          	lw	s6,140(a1) # 108c <scsiInquiryCopyString+0xc>
    18f2:	08c58713          	addi	a4,a1,140
    18f6:	08452083          	lw	ra,132(a0)
    18fa:	00eb1363          	bne	s6,a4,1900 <sdudcEpXSetHalt+0x26>
    18fe:	4b01                	c.li	s6,0
    1900:	0885ca03          	lbu	s4,136(a1)
    1904:	e211                	c.bnez	a2,1908 <sdudcEpXSetHalt+0x2e>
    1906:	8e04                	exec.it	#78     !sb	zero,160(a1)
    1908:	8aaa                	c.mv	s5,a0
    190a:	0895c503          	lbu	a0,137(a1)
    190e:	fffa0493          	addi	s1,s4,-1
    1912:	8932                	c.mv	s2,a2
    1914:	842e                	c.mv	s0,a1
    1916:	00349393          	slli	t2,s1,0x3
    191a:	1a208993          	addi	s3,ra,418
    191e:	c52d                	c.beqz	a0,1988 <sdudcEpXSetHalt+0xae>
    1920:	00e38693          	addi	a3,t2,14 # 200000e <_stack+0x1f6000e>
    1924:	00d084b3          	add	s1,ra,a3
    1928:	8526                	c.mv	a0,s1
    192a:	8020                	exec.it	#16     !jal	ra,2d38 <CPS_UncachedRead8>
    192c:	010a6593          	ori	a1,s4,16
    1930:	8baa                	c.mv	s7,a0
    1932:	04090263          	beqz	s2,1976 <sdudcEpXSetHalt+0x9c>
    1936:	04056813          	ori	a6,a0,64
    193a:	854e                	c.mv	a0,s3
    193c:	0ff87b93          	andi	s7,a6,255
    1940:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    1942:	050a6593          	ori	a1,s4,80
    1946:	854e                	c.mv	a0,s3
    1948:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    194a:	8526                	c.mv	a0,s1
    194c:	85de                	c.mv	a1,s7
    194e:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    1950:	08442883          	lw	a7,132(s0)
    1954:	4481                	c.li	s1,0
    1956:	0028dd5b          	beqc	a7,2,1970 <sdudcEpXSetHalt+0x96>
    195a:	00091b63          	bnez	s2,1970 <sdudcEpXSetHalt+0x96>
    195e:	040b0f63          	beqz	s6,19bc <sdudcEpXSetHalt+0xe2>
    1962:	08944e03          	lbu	t3,137(s0)
    1966:	85da                	c.mv	a1,s6
    1968:	8556                	c.mv	a0,s5
    196a:	040e0863          	beqz	t3,19ba <sdudcEpXSetHalt+0xe0>
    196e:	3545                	c.jal	180e <sdudcEpXDataSend>
    1970:	8526                	c.mv	a0,s1
    1972:	16f0306f          	j	52e0 <__riscv_restore_10>
    1976:	854e                	c.mv	a0,s3
    1978:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    197a:	030a6593          	ori	a1,s4,48
    197e:	854e                	c.mv	a0,s3
    1980:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    1982:	0bfbfb93          	andi	s7,s7,191
    1986:	b7d1                	c.j	194a <sdudcEpXSetHalt+0x70>
    1988:	00a38593          	addi	a1,t2,10
    198c:	00b084b3          	add	s1,ra,a1
    1990:	8526                	c.mv	a0,s1
    1992:	8020                	exec.it	#16     !jal	ra,2d38 <CPS_UncachedRead8>
    1994:	8baa                	c.mv	s7,a0
    1996:	00090c63          	beqz	s2,19ae <sdudcEpXSetHalt+0xd4>
    199a:	04056613          	ori	a2,a0,64
    199e:	85d2                	c.mv	a1,s4
    19a0:	854e                	c.mv	a0,s3
    19a2:	0ff67b93          	andi	s7,a2,255
    19a6:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    19a8:	040a6593          	ori	a1,s4,64
    19ac:	bf69                	c.j	1946 <sdudcEpXSetHalt+0x6c>
    19ae:	85d2                	c.mv	a1,s4
    19b0:	854e                	c.mv	a0,s3
    19b2:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    19b4:	020a6593          	ori	a1,s4,32
    19b8:	b7d9                	c.j	197e <sdudcEpXSetHalt+0xa4>
    19ba:	3bf1                	c.jal	1796 <sdudcEpXDataReceive>
    19bc:	84ca                	c.mv	s1,s2
    19be:	bf4d                	c.j	1970 <sdudcEpXSetHalt+0x96>

000019c0 <sdudcEp0Enable>:
    19c0:	47d9                	c.li	a5,22
    19c2:	cd51                	c.beqz	a0,1a5e <sdudcEp0Enable+0x9e>
    19c4:	cdc9                	c.beqz	a1,1a5e <sdudcEp0Enable+0x9e>
    19c6:	0845a703          	lw	a4,132(a1)
    19ca:	47c1                	c.li	a5,16
    19cc:	eb49                	c.bnez	a4,1a5e <sdudcEp0Enable+0x9e>
    19ce:	8040                	exec.it	#32     !jal	t0,52b6 <__riscv_save_4>
    19d0:	0885ca03          	lbu	s4,136(a1)
    19d4:	4405                	c.li	s0,1
    19d6:	014410b3          	sll	ra,s0,s4
    19da:	08452983          	lw	s3,132(a0)
    19de:	0895c303          	lbu	t1,137(a1)
    19e2:	fff0c293          	not	t0,ra
    19e6:	84ae                	c.mv	s1,a1
    19e8:	892a                	c.mv	s2,a0
    19ea:	3c02b45b          	bfos	s0,t0,15,0
    19ee:	00798a93          	addi	s5,s3,7
    19f2:	04031a63          	bnez	t1,1a46 <sdudcEp0Enable+0x86>
    19f6:	19698b13          	addi	s6,s3,406
    19fa:	855a                	c.mv	a0,s6
    19fc:	8264                	exec.it	#120     !jal	ra,2d42 <CPS_UncachedRead16>
    19fe:	8c69                	c.and	s0,a0
    1a00:	3c0425db          	bfoz	a1,s0,15,0
    1a04:	855a                	c.mv	a0,s6
    1a06:	8c04                	exec.it	#14     !jal	ra,2d56 <CPS_UncachedWrite16>
    1a08:	03000593          	li	a1,48
    1a0c:	8556                	c.mv	a0,s5
    1a0e:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    1a10:	6505                	c.lui	a0,0x1
    1a12:	4385                	c.li	t2,1
    1a14:	992a                	c.add	s2,a0
    1a16:	0604ae23          	sw	zero,124(s1)
    1a1a:	0874a223          	sw	t2,132(s1)
    1a1e:	61092503          	lw	a0,1552(s2)
    1a22:	cd01                	c.beqz	a0,1a3a <sdudcEp0Enable+0x7a>
    1a24:	60892583          	lw	a1,1544(s2)
    1a28:	4681                	c.li	a3,0
    1a2a:	01c5a803          	lw	a6,28(a1)
    1a2e:	0894c583          	lbu	a1,137(s1)
    1a32:	8652                	c.mv	a2,s4
    1a34:	9802                	c.jalr	a6
    1a36:	08a4ac23          	sw	a0,152(s1)
    1a3a:	1e098513          	addi	a0,s3,480
    1a3e:	8060                	exec.it	#48     !li	a1,64
    1a40:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    1a42:	4501                	c.li	a0,0
    1a44:	8050                	exec.it	#33     !j	52ea <__riscv_restore_4>
    1a46:	19498b13          	addi	s6,s3,404
    1a4a:	855a                	c.mv	a0,s6
    1a4c:	8264                	exec.it	#120     !jal	ra,2d42 <CPS_UncachedRead16>
    1a4e:	8c69                	c.and	s0,a0
    1a50:	3c0425db          	bfoz	a1,s0,15,0
    1a54:	855a                	c.mv	a0,s6
    1a56:	8c04                	exec.it	#14     !jal	ra,2d56 <CPS_UncachedWrite16>
    1a58:	02000593          	li	a1,32
    1a5c:	bf45                	c.j	1a0c <sdudcEp0Enable+0x4c>
    1a5e:	853e                	c.mv	a0,a5
    1a60:	8082                	c.jr	ra

00001a62 <sdudcEp0Callback.isra.0>:
    1a62:	00052023          	sw	zero,0(a0) # 1000 <mscApp+0xa>
    1a66:	421c                	c.lw	a5,0(a2)
    1a68:	4258                	c.lw	a4,4(a2)
    1a6a:	c3d8                	c.sw	a4,4(a5)
    1a6c:	00462283          	lw	t0,4(a2)
    1a70:	00062303          	lw	t1,0(a2)
    1a74:	0062a023          	sw	t1,0(t0)
    1a78:	00062223          	sw	zero,4(a2)
    1a7c:	00062023          	sw	zero,0(a2)
    1a80:	09c5a383          	lw	t2,156(a1)
    1a84:	8214                	exec.it	#73     !addi	a0,t2,-1
    1a86:	08a5ae23          	sw	a0,156(a1)
    1a8a:	03062803          	lw	a6,48(a2)
    1a8e:	417863db          	bnec	a6,119,1a94 <sdudcEp0Callback.isra.0+0x32>
    1a92:	da14                	c.sw	a3,48(a2)
    1a94:	5614                	c.lw	a3,40(a2)
    1a96:	c691                	c.beqz	a3,1aa2 <sdudcEp0Callback.isra.0+0x40>
    1a98:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    1a9a:	852e                	c.mv	a0,a1
    1a9c:	85b2                	c.mv	a1,a2
    1a9e:	9682                	c.jalr	a3
    1aa0:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
    1aa2:	8082                	c.jr	ra

00001aa4 <sdudcEpSetHalt>:
    1aa4:	cd8d                	c.beqz	a1,1ade <sdudcEpSetHalt+0x3a>
    1aa6:	48d9                	c.li	a7,22
    1aa8:	c90d                	c.beqz	a0,1ada <sdudcEpSetHalt+0x36>
    1aaa:	07a5c703          	lbu	a4,122(a1)
    1aae:	c709                	c.beqz	a4,1ab8 <sdudcEpSetHalt+0x14>
    1ab0:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    1ab2:	3525                	c.jal	18da <sdudcEpXSetHalt>
    1ab4:	4501                	c.li	a0,0
    1ab6:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
    1ab8:	08c5a283          	lw	t0,140(a1)
    1abc:	08c58613          	addi	a2,a1,140
    1ac0:	4881                	c.li	a7,0
    1ac2:	00c29c63          	bne	t0,a2,1ada <sdudcEpSetHalt+0x36>
    1ac6:	6305                	c.lui	t1,0x1
    1ac8:	8474                	exec.it	#59     !add	t2,a0,t1
    1aca:	6043a503          	lw	a0,1540(t2)
    1ace:	4811                	c.li	a6,4
    1ad0:	8470                	exec.it	#51     !addi	a1,a0,-1
    1ad2:	00b86463          	bltu	a6,a1,1ada <sdudcEpSetHalt+0x36>
    1ad6:	6003a223          	sw	zero,1540(t2)
    1ada:	8546                	c.mv	a0,a7
    1adc:	8082                	c.jr	ra
    1ade:	48d9                	c.li	a7,22
    1ae0:	bfed                	c.j	1ada <sdudcEpSetHalt+0x36>

00001ae2 <sdudcFreeRequest>:
    1ae2:	c919                	c.beqz	a0,1af8 <sdudcFreeRequest+0x16>
    1ae4:	c991                	c.beqz	a1,1af8 <sdudcFreeRequest+0x16>
    1ae6:	ca09                	c.beqz	a2,1af8 <sdudcFreeRequest+0x16>
    1ae8:	17c52383          	lw	t2,380(a0)
    1aec:	00038663          	beqz	t2,1af8 <sdudcFreeRequest+0x16>
    1af0:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    1af2:	85b2                	c.mv	a1,a2
    1af4:	9382                	c.jalr	t2
    1af6:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
    1af8:	8082                	c.jr	ra

00001afa <sdudcEp0StageSetup>:
    1afa:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    1afc:	842a                	c.mv	s0,a0
    1afe:	8e14                	exec.it	#79     !lw	a0,132(a0)
    1b00:	1141                	c.addi	sp,-16
    1b02:	8840                	exec.it	#36     !li	a1,128
    1b04:	0509                	c.addi	a0,2
    1b06:	18000493          	li	s1,384
    1b0a:	18800913          	li	s2,392
    1b0e:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    1b10:	08442083          	lw	ra,132(s0)
    1b14:	00908533          	add	a0,ra,s1
    1b18:	8020                	exec.it	#16     !jal	ra,2d38 <CPS_UncachedRead8>
    1b1a:	0030                	c.addi4spn	a2,sp,8
    1b1c:	009607b3          	add	a5,a2,s1
    1b20:	e8a78023          	sb	a0,-384(a5)
    1b24:	0485                	c.addi	s1,1
    1b26:	ff2495e3          	bne	s1,s2,1b10 <sdudcEp0StageSetup+0x16>
    1b2a:	8c70                	exec.it	#55     !lw	t0,132(s0)
    1b2c:	448d                	c.li	s1,3
    1b2e:	00228513          	addi	a0,t0,2
    1b32:	8020                	exec.it	#16     !jal	ra,2d38 <CPS_UncachedRead8>
    1b34:	4475765b          	bbs	a0,7,1b80 <sdudcEp0StageSetup+0x86>
    1b38:	08442303          	lw	t1,132(s0)
    1b3c:	4585                	c.li	a1,1
    1b3e:	18c30513          	addi	a0,t1,396 # 118c <scsiWrite_10+0xd6>
    1b42:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    1b44:	21042603          	lw	a2,528(s0)
    1b48:	21040393          	addi	t2,s0,528
    1b4c:	00760b63          	beq	a2,t2,1b62 <sdudcEp0StageSetup+0x68>
    1b50:	ca09                	c.beqz	a2,1b62 <sdudcEp0StageSetup+0x68>
    1b52:	6705                	c.lui	a4,0x1
    1b54:	60470813          	addi	a6,a4,1540 # 1604 <scsiExecCmd+0x40c>
    1b58:	5e0c                	c.lw	a1,56(a2)
    1b5a:	4681                	c.li	a3,0
    1b5c:	01040533          	add	a0,s0,a6
    1b60:	3709                	c.jal	1a62 <sdudcEp0Callback.isra.0>
    1b62:	00e15583          	lhu	a1,14(sp)
    1b66:	0e058a63          	beqz	a1,1c5a <sdudcEp0StageSetup+0x160>
    1b6a:	00810e83          	lb	t4,8(sp)
    1b6e:	0e0ed163          	bgez	t4,1c50 <sdudcEp0StageSetup+0x156>
    1b72:	6f85                	c.lui	t6,0x1
    1b74:	01f408b3          	add	a7,s0,t6
    1b78:	4e05                	c.li	t3,1
    1b7a:	4481                	c.li	s1,0
    1b7c:	61c8a223          	sw	t3,1540(a7) # ff0604 <_stack+0xf50604>
    1b80:	8c54                	exec.it	#47     !lw	a0,132(s0)
    1b82:	1a950513          	addi	a0,a0,425
    1b86:	8020                	exec.it	#16     !jal	ra,2d38 <CPS_UncachedRead8>
    1b88:	fff50613          	addi	a2,a0,-1
    1b8c:	0ff67293          	andi	t0,a2,255
    1b90:	430d                	c.li	t1,3
    1b92:	4381                	c.li	t2,0
    1b94:	00536a63          	bltu	t1,t0,1ba8 <sdudcEp0StageSetup+0xae>
    1b98:	00004097          	auipc	ra,0x4
    1b9c:	fa408093          	addi	ra,ra,-92 # 5b3c <CSWTCH.52>
    1ba0:	005087b3          	add	a5,ra,t0
    1ba4:	0007c383          	lbu	t2,0(a5)
    1ba8:	6705                	c.lui	a4,0x1
    1baa:	00e40533          	add	a0,s0,a4
    1bae:	8a34                	exec.it	#93     !sw	t2,12(s0)
    1bb0:	60452803          	lw	a6,1540(a0)
    1bb4:	3228545b          	beqc	a6,2,1edc <sdudcEp0StageSetup+0x3e2>
    1bb8:	0a58555b          	beqc	a6,5,1c62 <sdudcEp0StageSetup+0x168>
    1bbc:	3218655b          	bnec	a6,1,1ee6 <sdudcEp0StageSetup+0x3ec>
    1bc0:	00810583          	lb	a1,8(sp)
    1bc4:	0605f693          	andi	a3,a1,96
    1bc8:	e2f9                	c.bnez	a3,1c8e <sdudcEp0StageSetup+0x194>
    1bca:	00914883          	lbu	a7,9(sp)
    1bce:	0c089063          	bnez	a7,1c8e <sdudcEp0StageSetup+0x194>
    1bd2:	62c52e03          	lw	t3,1580(a0)
    1bd6:	000e00a3          	sb	zero,1(t3)
    1bda:	62c52e83          	lw	t4,1580(a0)
    1bde:	000e8023          	sb	zero,0(t4)
    1be2:	00810f03          	lb	t5,8(sp)
    1be6:	00ff7f93          	andi	t6,t5,15
    1bea:	2a1fd45b          	beqc	t6,1,1e92 <sdudcEp0StageSetup+0x398>
    1bee:	240f8163          	beqz	t6,1e30 <sdudcEp0StageSetup+0x336>
    1bf2:	082fee5b          	bnec	t6,2,1c8e <sdudcEp0StageSetup+0x194>
    1bf6:	00c15503          	lhu	a0,12(sp)
    1bfa:	00f57293          	andi	t0,a0,15
    1bfe:	28028a63          	beqz	t0,1e92 <sdudcEp0StageSetup+0x398>
    1c02:	0a400613          	li	a2,164
    1c06:	1c05335b          	bfos	t1,a0,7,0
    1c0a:	02c280b3          	mul	ra,t0,a2
    1c0e:	2a035e63          	bgez	t1,1eca <sdudcEp0StageSetup+0x3d0>
    1c12:	18408713          	addi	a4,ra,388
    1c16:	00e40833          	add	a6,s0,a4
    1c1a:	07c82583          	lw	a1,124(a6) # 100007c <_stack+0xf6007c>
    1c1e:	4559                	c.li	a0,22
    1c20:	c1d5                	c.beqz	a1,1cc4 <sdudcEp0StageSetup+0x1ca>
    1c22:	fff28693          	addi	a3,t0,-1
    1c26:	08442483          	lw	s1,132(s0)
    1c2a:	00369893          	slli	a7,a3,0x3
    1c2e:	2a035463          	bgez	t1,1ed6 <sdudcEp0StageSetup+0x3dc>
    1c32:	00e88e13          	addi	t3,a7,14
    1c36:	01c48533          	add	a0,s1,t3
    1c3a:	8020                	exec.it	#16     !jal	ra,2d38 <CPS_UncachedRead8>
    1c3c:	6e85                	c.lui	t4,0x1
    1c3e:	01d40f33          	add	t5,s0,t4
    1c42:	62cf2f83          	lw	t6,1580(t5)
    1c46:	1865255b          	bfoz	a0,a0,6,6
    1c4a:	00af8023          	sb	a0,0(t6) # 1000 <mscApp+0xa>
    1c4e:	a491                	c.j	1e92 <sdudcEp0StageSetup+0x398>
    1c50:	6f05                	c.lui	t5,0x1
    1c52:	01e408b3          	add	a7,s0,t5
    1c56:	4e09                	c.li	t3,2
    1c58:	b70d                	c.j	1b7a <sdudcEp0StageSetup+0x80>
    1c5a:	6685                	c.lui	a3,0x1
    1c5c:	8a24                	exec.it	#92     !add	a7,s0,a3
    1c5e:	4e15                	c.li	t3,5
    1c60:	bf29                	c.j	1b7a <sdudcEp0StageSetup+0x80>
    1c62:	00814e03          	lbu	t3,8(sp)
    1c66:	060e7e93          	andi	t4,t3,96
    1c6a:	020e9263          	bnez	t4,1c8e <sdudcEp0StageSetup+0x194>
    1c6e:	00914f03          	lbu	t5,9(sp)
    1c72:	063f5d5b          	beqc	t5,3,1cec <sdudcEp0StageSetup+0x1f2>
    1c76:	025f5b5b          	beqc	t5,5,1cac <sdudcEp0StageSetup+0x1b2>
    1c7a:	001f6a5b          	bnec	t5,1,1c8e <sdudcEp0StageSetup+0x194>
    1c7e:	00fe7f93          	andi	t6,t3,15
    1c82:	0a1fda5b          	beqc	t6,1,1d36 <sdudcEp0StageSetup+0x23c>
    1c86:	140f8663          	beqz	t6,1dd2 <sdudcEp0StageSetup+0x2d8>
    1c8a:	142fdb5b          	beqc	t6,2,1de0 <sdudcEp0StageSetup+0x2e6>
    1c8e:	16c42603          	lw	a2,364(s0)
    1c92:	24060e63          	beqz	a2,1eee <sdudcEp0StageSetup+0x3f4>
    1c96:	002c                	c.addi4spn	a1,sp,8
    1c98:	8522                	c.mv	a0,s0
    1c9a:	9602                	c.jalr	a2
    1c9c:	63a1                	c.lui	t2,0x8
    1c9e:	fff38813          	addi	a6,t2,-1 # 7fff <__global_pointer$+0x1ca3>
    1ca2:	25050463          	beq	a0,a6,1eea <sdudcEp0StageSetup+0x3f0>
    1ca6:	00a03533          	snez	a0,a0
    1caa:	a829                	c.j	1cc4 <sdudcEp0StageSetup+0x1ca>
    1cac:	00a10e83          	lb	t4,10(sp)
    1cb0:	4f91                	c.li	t6,4
    1cb2:	07feff13          	andi	t5,t4,127
    1cb6:	61e50723          	sb	t5,1550(a0)
    1cba:	01f42a23          	sw	t6,20(s0)
    1cbe:	60052223          	sw	zero,1540(a0)
    1cc2:	450d                	c.li	a0,3
    1cc4:	8470                	exec.it	#51     !addi	a1,a0,-1
    1cc6:	4785                	c.li	a5,1
    1cc8:	22b7e563          	bltu	a5,a1,1ef2 <sdudcEp0StageSetup+0x3f8>
    1ccc:	8230                	exec.it	#81     !lw	t4,132(s0)
    1cce:	002e8513          	addi	a0,t4,2 # 1002 <mscApp+0xc>
    1cd2:	8020                	exec.it	#16     !jal	ra,2d38 <CPS_UncachedRead8>
    1cd4:	00156f13          	ori	t5,a0,1
    1cd8:	8c54                	exec.it	#47     !lw	a0,132(s0)
    1cda:	0fff7593          	andi	a1,t5,255
    1cde:	0509                	c.addi	a0,2
    1ce0:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    1ce2:	6f85                	c.lui	t6,0x1
    1ce4:	947e                	c.add	s0,t6
    1ce6:	60042223          	sw	zero,1540(s0)
    1cea:	a401                	c.j	1eea <sdudcEp0StageSetup+0x3f0>
    1cec:	00fe7293          	andi	t0,t3,15
    1cf0:	0412d35b          	beqc	t0,1,1d36 <sdudcEp0StageSetup+0x23c>
    1cf4:	00028863          	beqz	t0,1d04 <sdudcEp0StageSetup+0x20a>
    1cf8:	0822d75b          	beqc	t0,2,1d86 <sdudcEp0StageSetup+0x28c>
    1cfc:	60052223          	sw	zero,1540(a0)
    1d00:	4559                	c.li	a0,22
    1d02:	b7c9                	c.j	1cc4 <sdudcEp0StageSetup+0x1ca>
    1d04:	00a11e83          	lh	t4,10(sp)
    1d08:	4511                	c.li	a0,4
    1d0a:	fffe8f13          	addi	t5,t4,-1
    1d0e:	3c0f2fdb          	bfoz	t6,t5,15,0
    1d12:	03f56263          	bltu	a0,t6,1d36 <sdudcEp0StageSetup+0x23c>
    1d16:	00004297          	auipc	t0,0x4
    1d1a:	dfe28293          	addi	t0,t0,-514 # 5b14 <_ITB_BASE_+0x458>
    1d1e:	0df2835b          	lea.w	t1,t0,t6
    1d22:	00032083          	lw	ra,0(t1)
    1d26:	00508633          	add	a2,ra,t0
    1d2a:	8602                	c.jr	a2
    1d2c:	6685                	c.lui	a3,0x1
    1d2e:	8a24                	exec.it	#92     !add	a7,s0,a3
    1d30:	4705                	c.li	a4,1
    1d32:	60e88623          	sb	a4,1548(a7)
    1d36:	08442f83          	lw	t6,132(s0)
    1d3a:	4589                	c.li	a1,2
    1d3c:	002f8513          	addi	a0,t6,2 # 1002 <mscApp+0xc>
    1d40:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    1d42:	6285                	c.lui	t0,0x1
    1d44:	00540333          	add	t1,s0,t0
    1d48:	60032223          	sw	zero,1540(t1)
    1d4c:	4501                	c.li	a0,0
    1d4e:	bf9d                	c.j	1cc4 <sdudcEp0StageSetup+0x1ca>
    1d50:	01944583          	lbu	a1,25(s0)
    1d54:	e589                	c.bnez	a1,1d5e <sdudcEp0StageSetup+0x264>
    1d56:	6f05                	c.lui	t5,0x1
    1d58:	01e40533          	add	a0,s0,t5
    1d5c:	b745                	c.j	1cfc <sdudcEp0StageSetup+0x202>
    1d5e:	4785                	c.li	a5,1
    1d60:	00f40da3          	sb	a5,27(s0)
    1d64:	bfc9                	c.j	1d36 <sdudcEp0StageSetup+0x23c>
    1d66:	01944383          	lbu	t2,25(s0)
    1d6a:	fe0386e3          	beqz	t2,1d56 <sdudcEp0StageSetup+0x25c>
    1d6e:	4805                	c.li	a6,1
    1d70:	01040e23          	sb	a6,28(s0)
    1d74:	b7c9                	c.j	1d36 <sdudcEp0StageSetup+0x23c>
    1d76:	01944e03          	lbu	t3,25(s0)
    1d7a:	fc0e0ee3          	beqz	t3,1d56 <sdudcEp0StageSetup+0x25c>
    1d7e:	4e85                	c.li	t4,1
    1d80:	01d40ea3          	sb	t4,29(s0)
    1d84:	bf4d                	c.j	1d36 <sdudcEp0StageSetup+0x23c>
    1d86:	00c15303          	lhu	t1,12(sp)
    1d8a:	4839                	c.li	a6,14
    1d8c:	00f37093          	andi	ra,t1,15
    1d90:	fff08613          	addi	a2,ra,-1
    1d94:	0ff67393          	andi	t2,a2,255
    1d98:	f67862e3          	bltu	a6,t2,1cfc <sdudcEp0StageSetup+0x202>
    1d9c:	00a15583          	lhu	a1,10(sp)
    1da0:	fdb1                	c.bnez	a1,1cfc <sdudcEp0StageSetup+0x202>
    1da2:	0a400793          	li	a5,164
    1da6:	02f086b3          	mul	a3,ra,a5
    1daa:	00737e5b          	bbc	t1,7,1dc6 <sdudcEp0StageSetup+0x2cc>
    1dae:	18468713          	addi	a4,a3,388 # 1184 <scsiWrite_10+0xce>
    1db2:	00e405b3          	add	a1,s0,a4
    1db6:	07c5ae03          	lw	t3,124(a1)
    1dba:	4605                	c.li	a2,1
    1dbc:	f80e0de3          	beqz	t3,1d56 <sdudcEp0StageSetup+0x25c>
    1dc0:	8522                	c.mv	a0,s0
    1dc2:	3e21                	c.jal	18da <sdudcEpXSetHalt>
    1dc4:	bf8d                	c.j	1d36 <sdudcEp0StageSetup+0x23c>
    1dc6:	6485                	c.lui	s1,0x1
    1dc8:	bc448893          	addi	a7,s1,-1084 # bc4 <setup+0x398>
    1dcc:	01168733          	add	a4,a3,a7
    1dd0:	b7cd                	c.j	1db2 <sdudcEp0StageSetup+0x2b8>
    1dd2:	00a15e03          	lhu	t3,10(sp)
    1dd6:	aa1e6c5b          	bnec	t3,1,1c8e <sdudcEp0StageSetup+0x194>
    1dda:	60050623          	sb	zero,1548(a0)
    1dde:	b5c5                	c.j	1cbe <sdudcEp0StageSetup+0x1c4>
    1de0:	00c15503          	lhu	a0,12(sp)
    1de4:	4639                	c.li	a2,14
    1de6:	07f57293          	andi	t0,a0,127
    1dea:	fff28793          	addi	a5,t0,-1 # fff <mscApp+0x9>
    1dee:	0ff7f313          	andi	t1,a5,255
    1df2:	e8666ee3          	bltu	a2,t1,1c8e <sdudcEp0StageSetup+0x194>
    1df6:	00a15083          	lhu	ra,10(sp)
    1dfa:	e8009ae3          	bnez	ra,1c8e <sdudcEp0StageSetup+0x194>
    1dfe:	0a400493          	li	s1,164
    1e02:	029283b3          	mul	t2,t0,s1
    1e06:	00757f5b          	bbc	a0,7,1e24 <sdudcEp0StageSetup+0x32a>
    1e0a:	18438693          	addi	a3,t2,388
    1e0e:	00d405b3          	add	a1,s0,a3
    1e12:	07c5a883          	lw	a7,124(a1)
    1e16:	f40880e3          	beqz	a7,1d56 <sdudcEp0StageSetup+0x25c>
    1e1a:	0a05c703          	lbu	a4,160(a1)
    1e1e:	ff01                	c.bnez	a4,1d36 <sdudcEp0StageSetup+0x23c>
    1e20:	4601                	c.li	a2,0
    1e22:	bf79                	c.j	1dc0 <sdudcEp0StageSetup+0x2c6>
    1e24:	6805                	c.lui	a6,0x1
    1e26:	bc480593          	addi	a1,a6,-1084 # bc4 <setup+0x398>
    1e2a:	00b386b3          	add	a3,t2,a1
    1e2e:	b7c5                	c.j	1e0e <sdudcEp0StageSetup+0x314>
    1e30:	60d54303          	lbu	t1,1549(a0)
    1e34:	62c52283          	lw	t0,1580(a0)
    1e38:	00603633          	snez	a2,t1
    1e3c:	00c28023          	sb	a2,0(t0)
    1e40:	62c52083          	lw	ra,1580(a0)
    1e44:	60c54703          	lbu	a4,1548(a0)
    1e48:	00008383          	lb	t2,0(ra)
    1e4c:	00e03833          	snez	a6,a4
    1e50:	00181593          	slli	a1,a6,0x1
    1e54:	00b3e4b3          	or	s1,t2,a1
    1e58:	00908023          	sb	s1,0(ra)
    1e5c:	01944683          	lbu	a3,25(s0)
    1e60:	ca8d                	c.beqz	a3,1e92 <sdudcEp0StageSetup+0x398>
    1e62:	62c52883          	lw	a7,1580(a0)
    1e66:	01b44783          	lbu	a5,27(s0)
    1e6a:	01d44e83          	lbu	t4,29(s0)
    1e6e:	00088503          	lb	a0,0(a7)
    1e72:	01c44303          	lbu	t1,28(s0)
    1e76:	00379e13          	slli	t3,a5,0x3
    1e7a:	004e9f13          	slli	t5,t4,0x4
    1e7e:	01ee6fb3          	or	t6,t3,t5
    1e82:	00afe2b3          	or	t0,t6,a0
    1e86:	00531613          	slli	a2,t1,0x5
    1e8a:	00c2e0b3          	or	ra,t0,a2
    1e8e:	00188023          	sb	ra,0(a7)
    1e92:	00e15383          	lhu	t2,14(sp)
    1e96:	4809                	c.li	a6,2
    1e98:	3c03a75b          	bfoz	a4,t2,15,0
    1e9c:	00787363          	bgeu	a6,t2,1ea2 <sdudcEp0StageSetup+0x3a8>
    1ea0:	4709                	c.li	a4,2
    1ea2:	6585                	c.lui	a1,0x1
    1ea4:	00b404b3          	add	s1,s0,a1
    1ea8:	6084a683          	lw	a3,1544(s1)
    1eac:	1f845603          	lhu	a2,504(s0)
    1eb0:	0246a883          	lw	a7,36(a3)
    1eb4:	21c42583          	lw	a1,540(s0)
    1eb8:	6304a683          	lw	a3,1584(s1)
    1ebc:	8450                	exec.it	#35     !lw	a0,1552(s1)
    1ebe:	4781                	c.li	a5,0
    1ec0:	9882                	c.jalr	a7
    1ec2:	4711                	c.li	a4,4
    1ec4:	60e4a223          	sw	a4,1540(s1)
    1ec8:	b551                	c.j	1d4c <sdudcEp0StageSetup+0x252>
    1eca:	6785                	c.lui	a5,0x1
    1ecc:	bc478393          	addi	t2,a5,-1084 # bc4 <setup+0x398>
    1ed0:	00708733          	add	a4,ra,t2
    1ed4:	b389                	c.j	1c16 <sdudcEp0StageSetup+0x11c>
    1ed6:	00a88e13          	addi	t3,a7,10
    1eda:	bbb1                	c.j	1c36 <sdudcEp0StageSetup+0x13c>
    1edc:	08442083          	lw	ra,132(s0)
    1ee0:	00008023          	sb	zero,0(ra)
    1ee4:	b36d                	c.j	1c8e <sdudcEp0StageSetup+0x194>
    1ee6:	9a34e45b          	bnec	s1,3,1c8e <sdudcEp0StageSetup+0x194>
    1eea:	0141                	c.addi	sp,16
    1eec:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    1eee:	4505                	c.li	a0,1
    1ef0:	bbd1                	c.j	1cc4 <sdudcEp0StageSetup+0x1ca>
    1ef2:	6685                	c.lui	a3,0x1
    1ef4:	8a24                	exec.it	#92     !add	a7,s0,a3
    1ef6:	6048a703          	lw	a4,1540(a7)
    1efa:	be57685b          	bnec	a4,5,1eea <sdudcEp0StageSetup+0x3f0>
    1efe:	6008a223          	sw	zero,1540(a7)
    1f02:	8620                	exec.it	#82     !lw	t3,132(s0)
    1f04:	4589                	c.li	a1,2
    1f06:	002e0513          	addi	a0,t3,2
    1f0a:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    1f0c:	bff9                	c.j	1eea <sdudcEp0StageSetup+0x3f0>

00001f0e <sdudcEp0DataSend>:
    1f0e:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    1f10:	6405                	c.lui	s0,0x1
    1f12:	942a                	c.add	s0,a0
    1f14:	60842903          	lw	s2,1544(s0) # 1608 <scsiExecCmd+0x410>
    1f18:	21c52583          	lw	a1,540(a0)
    1f1c:	84aa                	c.mv	s1,a0
    1f1e:	03492783          	lw	a5,52(s2)
    1f22:	8844                	exec.it	#44     !lw	a0,1552(s0)
    1f24:	9782                	c.jalr	a5
    1f26:	02c92303          	lw	t1,44(s2)
    1f2a:	21c4a583          	lw	a1,540(s1)
    1f2e:	8844                	exec.it	#44     !lw	a0,1552(s0)
    1f30:	9302                	c.jalr	t1
    1f32:	4289                	c.li	t0,2
    1f34:	00a2ea63          	bltu	t0,a0,1f48 <sdudcEp0DataSend+0x3a>
    1f38:	30000537          	lui	a0,0x30000
    1f3c:	4091                	c.li	ra,4
    1f3e:	4589                	c.li	a1,2
    1f40:	0509                	c.addi	a0,2
    1f42:	60142223          	sw	ra,1540(s0)
    1f46:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    1f48:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>

00001f4a <sdudcEpSetWedge>:
    1f4a:	c991                	c.beqz	a1,1f5e <sdudcEpSetWedge+0x14>
    1f4c:	42d9                	c.li	t0,22
    1f4e:	c909                	c.beqz	a0,1f60 <sdudcEpSetWedge+0x16>
    1f50:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    1f52:	4705                	c.li	a4,1
    1f54:	4605                	c.li	a2,1
    1f56:	0ae58023          	sb	a4,160(a1) # 10a0 <scsiInquiryCopyString+0x20>
    1f5a:	36a9                	c.jal	1aa4 <sdudcEpSetHalt>
    1f5c:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
    1f5e:	42d9                	c.li	t0,22
    1f60:	8516                	c.mv	a0,t0
    1f62:	8082                	c.jr	ra

00001f64 <sdudcGetFrame>:
    1f64:	47d9                	c.li	a5,22
    1f66:	c919                	c.beqz	a0,1f7c <sdudcGetFrame+0x18>
    1f68:	c991                	c.beqz	a1,1f7c <sdudcGetFrame+0x18>
    1f6a:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
    1f6c:	8e14                	exec.it	#79     !lw	a0,132(a0) # 30000084 <_stack+0x2ff60084>
    1f6e:	842e                	c.mv	s0,a1
    1f70:	1a450513          	addi	a0,a0,420
    1f74:	8264                	exec.it	#120     !jal	ra,2d42 <CPS_UncachedRead16>
    1f76:	c008                	c.sw	a0,0(s0)
    1f78:	4501                	c.li	a0,0
    1f7a:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>
    1f7c:	853e                	c.mv	a0,a5
    1f7e:	8082                	c.jr	ra

00001f80 <sdudcEpAllocRequest>:
    1f80:	42d9                	c.li	t0,22
    1f82:	c519                	c.beqz	a0,1f90 <sdudcEpAllocRequest+0x10>
    1f84:	c591                	c.beqz	a1,1f90 <sdudcEpAllocRequest+0x10>
    1f86:	c609                	c.beqz	a2,1f90 <sdudcEpAllocRequest+0x10>
    1f88:	17852783          	lw	a5,376(a0)
    1f8c:	e781                	c.bnez	a5,1f94 <sdudcEpAllocRequest+0x14>
    1f8e:	42b1                	c.li	t0,12
    1f90:	8516                	c.mv	a0,t0
    1f92:	8082                	c.jr	ra
    1f94:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    1f96:	84ae                	c.mv	s1,a1
    1f98:	04400593          	li	a1,68
    1f9c:	8932                	c.mv	s2,a2
    1f9e:	9782                	c.jalr	a5
    1fa0:	4331                	c.li	t1,12
    1fa2:	842a                	c.mv	s0,a0
    1fa4:	c919                	c.beqz	a0,1fba <sdudcEpAllocRequest+0x3a>
    1fa6:	04400613          	li	a2,68
    1faa:	4581                	c.li	a1,0
    1fac:	8430                	exec.it	#19     !jal	ra,5526 <memset>
    1fae:	4301                	c.li	t1,0
    1fb0:	00892023          	sw	s0,0(s2)
    1fb4:	c000                	c.sw	s0,0(s0)
    1fb6:	c040                	c.sw	s0,4(s0)
    1fb8:	dc04                	c.sw	s1,56(s0)
    1fba:	851a                	c.mv	a0,t1
    1fbc:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>

00001fbe <sdudcEpEnable>:
    1fbe:	e511                	c.bnez	a0,1fca <sdudcEpEnable+0xc>
    1fc0:	4559                	c.li	a0,22
    1fc2:	8082                	c.jr	ra
    1fc4:	4559                	c.li	a0,22
    1fc6:	31a0306f          	j	52e0 <__riscv_restore_10>
    1fca:	2de032ef          	jal	t0,52a8 <__riscv_save_10>
    1fce:	892e                	c.mv	s2,a1
    1fd0:	d9f5                	c.beqz	a1,1fc4 <sdudcEpEnable+0x6>
    1fd2:	da6d                	c.beqz	a2,1fc4 <sdudcEpEnable+0x6>
    1fd4:	0885c403          	lbu	s0,136(a1)
    1fd8:	8e04                	exec.it	#78     !sb	zero,160(a1)
    1fda:	8b32                	c.mv	s6,a2
    1fdc:	1c040363          	beqz	s0,21a2 <sdudcEpEnable+0x1e4>
    1fe0:	0845a783          	lw	a5,132(a1)
    1fe4:	84aa                	c.mv	s1,a0
    1fe6:	4541                	c.li	a0,16
    1fe8:	fff9                	c.bnez	a5,1fc6 <sdudcEpEnable+0x8>
    1fea:	00465283          	lhu	t0,4(a2)
    1fee:	7ff2f593          	andi	a1,t0,2047
    1ff2:	d9e9                	c.beqz	a1,1fc4 <sdudcEpEnable+0x6>
    1ff4:	00360703          	lb	a4,3(a2)
    1ff8:	8bae                	c.mv	s7,a1
    1ffa:	00377313          	andi	t1,a4,3
    1ffe:	00235e5b          	beqc	t1,2,201a <sdudcEpEnable+0x5c>
    2002:	1233585b          	beqc	t1,3,2132 <sdudcEpEnable+0x174>
    2006:	1213695b          	bnec	t1,1,2138 <sdudcEpEnable+0x17a>
    200a:	00b2d393          	srli	t2,t0,0xb
    200e:	1013d75b          	beqc	t2,1,211c <sdudcEpEnable+0x15e>
    2012:	1023da5b          	beqc	t2,2,2126 <sdudcEpEnable+0x168>
    2016:	4a81                	c.li	s5,0
    2018:	a229                	c.j	2122 <sdudcEpEnable+0x164>
    201a:	4a81                	c.li	s5,0
    201c:	4a21                	c.li	s4,8
    201e:	002b0683          	lb	a3,2(s6)
    2022:	0844a983          	lw	s3,132(s1)
    2026:	08994603          	lbu	a2,137(s2)
    202a:	454d                	c.li	a0,19
    202c:	1006d963          	bgez	a3,213e <sdudcEpEnable+0x180>
    2030:	da59                	c.beqz	a2,1fc6 <sdudcEpEnable+0x8>
    2032:	4799                	c.li	a5,6
    2034:	02f402b3          	mul	t0,s0,a5
    2038:	00548c33          	add	s8,s1,t0
    203c:	094c5703          	lhu	a4,148(s8)
    2040:	f8b762e3          	bltu	a4,a1,1fc4 <sdudcEpEnable+0x6>
    2044:	1f040313          	addi	t1,s0,496
    2048:	0a69855b          	lea.h	a0,s3,t1
    204c:	8c04                	exec.it	#14     !jal	ra,2d56 <CPS_UncachedWrite16>
    204e:	090c0383          	lb	t2,144(s8)
    2052:	fff40c93          	addi	s9,s0,-1
    2056:	fff38613          	addi	a2,t2,-1
    205a:	f8066693          	ori	a3,a2,-128
    205e:	00da6533          	or	a0,s4,a3
    2062:	00aae833          	or	a6,s5,a0
    2066:	0f9988db          	lea.d	a7,s3,s9
    206a:	0ff87593          	andi	a1,a6,255
    206e:	00e88513          	addi	a0,a7,14
    2072:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    2074:	0844ae03          	lw	t3,132(s1)
    2078:	03046593          	ori	a1,s0,48
    207c:	1a8e0513          	addi	a0,t3,424
    2080:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    2082:	1a298a93          	addi	s5,s3,418
    2086:	01046593          	ori	a1,s0,16
    208a:	8556                	c.mv	a0,s5
    208c:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    208e:	07046593          	ori	a1,s0,112
    2092:	8556                	c.mv	a0,s5
    2094:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    2096:	6585                	c.lui	a1,0x1
    2098:	00b48eb3          	add	t4,s1,a1
    209c:	610ea503          	lw	a0,1552(t4)
    20a0:	cd19                	c.beqz	a0,20be <sdudcEpEnable+0x100>
    20a2:	608eaf03          	lw	t5,1544(t4)
    20a6:	ffca0093          	addi	ra,s4,-4
    20aa:	01cf2f83          	lw	t6,28(t5) # 101c <mscApp+0x26>
    20ae:	08994583          	lbu	a1,137(s2)
    20b2:	0010b693          	seqz	a3,ra
    20b6:	8622                	c.mv	a2,s0
    20b8:	9f82                	c.jalr	t6
    20ba:	08a92c23          	sw	a0,152(s2)
    20be:	044a685b          	bnec	s4,4,210e <sdudcEpEnable+0x150>
    20c2:	1504c783          	lbu	a5,336(s1)
    20c6:	eb99                	c.bnez	a5,20dc <sdudcEpEnable+0x11e>
    20c8:	19898a13          	addi	s4,s3,408
    20cc:	8552                	c.mv	a0,s4
    20ce:	8020                	exec.it	#16     !jal	ra,2d38 <CPS_UncachedRead8>
    20d0:	00256293          	ori	t0,a0,2
    20d4:	0ff2f593          	andi	a1,t0,255
    20d8:	8552                	c.mv	a0,s4
    20da:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    20dc:	08994703          	lbu	a4,137(s2)
    20e0:	cf11                	c.beqz	a4,20fc <sdudcEpEnable+0x13e>
    20e2:	4305                	c.li	t1,1
    20e4:	00831433          	sll	s0,t1,s0
    20e8:	3c04245b          	bfoz	s0,s0,15,0
    20ec:	85a2                	c.mv	a1,s0
    20ee:	1dc98513          	addi	a0,s3,476
    20f2:	8c04                	exec.it	#14     !jal	ra,2d56 <CPS_UncachedWrite16>
    20f4:	85a2                	c.mv	a1,s0
    20f6:	1d898513          	addi	a0,s3,472
    20fa:	8c04                	exec.it	#14     !jal	ra,2d56 <CPS_UncachedWrite16>
    20fc:	6385                	c.lui	t2,0x1
    20fe:	949e                	c.add	s1,t2
    2100:	6084a603          	lw	a2,1544(s1)
    2104:	8c60                	exec.it	#54     !lw	a1,152(s2)
    2106:	5e14                	c.lw	a3,56(a2)
    2108:	8450                	exec.it	#35     !lw	a0,1552(s1)
    210a:	865e                	c.mv	a2,s7
    210c:	9682                	c.jalr	a3
    210e:	4505                	c.li	a0,1
    2110:	08a92223          	sw	a0,132(s2)
    2114:	07692e23          	sw	s6,124(s2)
    2118:	4501                	c.li	a0,0
    211a:	b575                	c.j	1fc6 <sdudcEpEnable+0x8>
    211c:	00159b93          	slli	s7,a1,0x1
    2120:	4ac1                	c.li	s5,16
    2122:	4a11                	c.li	s4,4
    2124:	bded                	c.j	201e <sdudcEpEnable+0x60>
    2126:	4b8d                	c.li	s7,3
    2128:	03758bb3          	mul	s7,a1,s7
    212c:	02000a93          	li	s5,32
    2130:	bfcd                	c.j	2122 <sdudcEpEnable+0x164>
    2132:	4a81                	c.li	s5,0
    2134:	4a31                	c.li	s4,12
    2136:	b5e5                	c.j	201e <sdudcEpEnable+0x60>
    2138:	4a81                	c.li	s5,0
    213a:	4a01                	c.li	s4,0
    213c:	b5cd                	c.j	201e <sdudcEpEnable+0x60>
    213e:	e80614e3          	bnez	a2,1fc6 <sdudcEpEnable+0x8>
    2142:	4c19                	c.li	s8,6
    2144:	03840533          	mul	a0,s0,s8
    2148:	00a48c33          	add	s8,s1,a0
    214c:	0f4c5803          	lhu	a6,244(s8)
    2150:	e6b86ae3          	bltu	a6,a1,1fc4 <sdudcEpEnable+0x6>
    2154:	0f040093          	addi	ra,s0,240
    2158:	0a19855b          	lea.h	a0,s3,ra
    215c:	8c04                	exec.it	#14     !jal	ra,2d56 <CPS_UncachedWrite16>
    215e:	090c0583          	lb	a1,144(s8)
    2162:	fff40c93          	addi	s9,s0,-1
    2166:	fff58893          	addi	a7,a1,-1 # fff <mscApp+0x9>
    216a:	f808ee13          	ori	t3,a7,-128
    216e:	01ca6eb3          	or	t4,s4,t3
    2172:	01daeab3          	or	s5,s5,t4
    2176:	0f998f5b          	lea.d	t5,s3,s9
    217a:	00af0513          	addi	a0,t5,10
    217e:	0ffaf593          	andi	a1,s5,255
    2182:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    2184:	0844af83          	lw	t6,132(s1)
    2188:	02046593          	ori	a1,s0,32
    218c:	1a8f8513          	addi	a0,t6,424
    2190:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    2192:	1a298a93          	addi	s5,s3,418
    2196:	85a2                	c.mv	a1,s0
    2198:	8556                	c.mv	a0,s5
    219a:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    219c:	06046593          	ori	a1,s0,96
    21a0:	bdcd                	c.j	2092 <sdudcEpEnable+0xd4>
    21a2:	81fff0ef          	jal	ra,19c0 <sdudcEp0Enable>
    21a6:	b505                	c.j	1fc6 <sdudcEpEnable+0x8>

000021a8 <sdudcInitDeviceEp>:
    21a8:	8820                	exec.it	#20     !jal	t0,52b6 <__riscv_save_4>
    21aa:	08c58793          	addi	a5,a1,140
    21ae:	08c58423          	sb	a2,136(a1)
    21b2:	08d584a3          	sb	a3,137(a1)
    21b6:	0805a223          	sw	zero,132(a1)
    21ba:	c18c                	c.sw	a1,0(a1)
    21bc:	c1cc                	c.sw	a1,4(a1)
    21be:	08f5a623          	sw	a5,140(a1)
    21c2:	08f5a823          	sw	a5,144(a1)
    21c6:	0805ae23          	sw	zero,156(a1)
    21ca:	84aa                	c.mv	s1,a0
    21cc:	842e                	c.mv	s0,a1
    21ce:	8932                	c.mv	s2,a2
    21d0:	8a36                	c.mv	s4,a3
    21d2:	00c58513          	addi	a0,a1,12
    21d6:	00004717          	auipc	a4,0x4
    21da:	89e70713          	addi	a4,a4,-1890 # 5a74 <_ITB_BASE_+0x3b8>
    21de:	e689                	c.bnez	a3,21e8 <sdudcInitDeviceEp+0x40>
    21e0:	00003717          	auipc	a4,0x3
    21e4:	70470713          	addi	a4,a4,1796 # 58e4 <_ITB_BASE_+0x228>
    21e8:	86ca                	c.mv	a3,s2
    21ea:	00003617          	auipc	a2,0x3
    21ee:	6fe60613          	addi	a2,a2,1790 # 58e8 <_ITB_BASE_+0x22c>
    21f2:	06400593          	li	a1,100
    21f6:	8e60                	exec.it	#118     !jal	ra,55d0 <snprintf>
    21f8:	b9418a93          	addi	s5,gp,-1132 # 5ef0 <sdudcEpOps>
    21fc:	02091263          	bnez	s2,2220 <sdudcInitDeviceEp+0x78>
    2200:	0944d383          	lhu	t2,148(s1)
    2204:	08944603          	lbu	a2,137(s0)
    2208:	06741a23          	sh	t2,116(s0)
    220c:	0016635b          	bnec	a2,1,2212 <sdudcInitDeviceEp+0x6a>
    2210:	c480                	c.sw	s0,8(s1)
    2212:	85a2                	c.mv	a1,s0
    2214:	8526                	c.mv	a0,s1
    2216:	07542823          	sw	s5,112(s0)
    221a:	fa6ff0ef          	jal	ra,19c0 <sdudcEp0Enable>
    221e:	8c34                	exec.it	#31     !j	52ea <__riscv_restore_4>
    2220:	4099                	c.li	ra,6
    2222:	021902b3          	mul	t0,s2,ra
    2226:	fff90993          	addi	s3,s2,-1
    222a:	0844a503          	lw	a0,132(s1)
    222e:	098e                	c.slli	s3,0x3
    2230:	00548333          	add	t1,s1,t0
    2234:	060a0563          	beqz	s4,229e <sdudcInitDeviceEp+0xf6>
    2238:	09235583          	lhu	a1,146(t1)
    223c:	c591                	c.beqz	a1,2248 <sdudcInitDeviceEp+0xa0>
    223e:	0d090893          	addi	a7,s2,208
    2242:	0d15055b          	lea.w	a0,a0,a7
    2246:	8c04                	exec.it	#14     !jal	ra,2d56 <CPS_UncachedWrite16>
    2248:	0844ae03          	lw	t3,132(s1)
    224c:	09b9                	c.addi	s3,14
    224e:	4581                	c.li	a1,0
    2250:	013e0533          	add	a0,t3,s3
    2254:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    2256:	4e99                	c.li	t4,6
    2258:	03d90f33          	mul	t5,s2,t4
    225c:	01e48fb3          	add	t6,s1,t5
    2260:	094fd803          	lhu	a6,148(t6)
    2264:	07041a23          	sh	a6,116(s0)
    2268:	07542823          	sw	s5,112(s0)
    226c:	000a0463          	beqz	s4,2274 <sdudcInitDeviceEp+0xcc>
    2270:	08096913          	ori	s2,s2,128
    2274:	6785                	c.lui	a5,0x1
    2276:	00f48533          	add	a0,s1,a5
    227a:	07240d23          	sb	s2,122(s0)
    227e:	06042b23          	sw	zero,118(s0)
    2282:	63450083          	lb	ra,1588(a0)
    2286:	8864                	exec.it	#60     !addi	t0,ra,1
    2288:	62550a23          	sb	t0,1588(a0)
    228c:	0044a303          	lw	t1,4(s1)
    2290:	c0c0                	c.sw	s0,4(s1)
    2292:	c004                	c.sw	s1,0(s0)
    2294:	00642223          	sw	t1,4(s0)
    2298:	00832023          	sw	s0,0(t1)
    229c:	b749                	c.j	221e <sdudcInitDeviceEp+0x76>
    229e:	0f235583          	lhu	a1,242(t1)
    22a2:	c591                	c.beqz	a1,22ae <sdudcInitDeviceEp+0x106>
    22a4:	0c090393          	addi	t2,s2,192
    22a8:	0c75055b          	lea.w	a0,a0,t2
    22ac:	8c04                	exec.it	#14     !jal	ra,2d56 <CPS_UncachedWrite16>
    22ae:	8220                	exec.it	#80     !lw	a2,132(s1)
    22b0:	09a9                	c.addi	s3,10
    22b2:	4581                	c.li	a1,0
    22b4:	01360533          	add	a0,a2,s3
    22b8:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    22ba:	4599                	c.li	a1,6
    22bc:	02b906b3          	mul	a3,s2,a1
    22c0:	00d48733          	add	a4,s1,a3
    22c4:	0f475803          	lhu	a6,244(a4)
    22c8:	bf71                	c.j	2264 <sdudcInitDeviceEp+0xbc>

000022ca <sdudcSetup>:
    22ca:	7df022ef          	jal	t0,52a8 <__riscv_save_10>
    22ce:	478d                	c.li	a5,3
    22d0:	842a                	c.mv	s0,a0
    22d2:	00050c23          	sb	zero,24(a0)
    22d6:	00052a23          	sw	zero,20(a0)
    22da:	c91c                	c.sw	a5,16(a0)
    22dc:	06400593          	li	a1,100
    22e0:	4685                	c.li	a3,1
    22e2:	00003617          	auipc	a2,0x3
    22e6:	66260613          	addi	a2,a2,1634 # 5944 <_ITB_BASE_+0x288>
    22ea:	0579                	c.addi	a0,30
    22ec:	8e60                	exec.it	#118     !jal	ra,55d0 <snprintf>
    22ee:	6585                	c.lui	a1,0x1
    22f0:	15844283          	lbu	t0,344(s0)
    22f4:	bc458393          	addi	t2,a1,-1084 # bc4 <setup+0x398>
    22f8:	4309                	c.li	t1,2
    22fa:	18440913          	addi	s2,s0,388
    22fe:	09040a13          	addi	s4,s0,144
    2302:	4481                	c.li	s1,0
    2304:	ac458b13          	addi	s6,a1,-1340
    2308:	4a8d                	c.li	s5,3
    230a:	ac858b93          	addi	s7,a1,-1336
    230e:	a4058c13          	addi	s8,a1,-1472
    2312:	007409b3          	add	s3,s0,t2
    2316:	00540c23          	sb	t0,24(s0)
    231a:	00540ca3          	sb	t0,25(s0)
    231e:	00642623          	sw	t1,12(s0)
    2322:	c000                	c.sw	s0,0(s0)
    2324:	c040                	c.sw	s0,4(s0)
    2326:	e0cd                	c.bnez	s1,23c8 <sdudcSetup+0xfe>
    2328:	4685                	c.li	a3,1
    232a:	4601                	c.li	a2,0
    232c:	85ca                	c.mv	a1,s2
    232e:	8522                	c.mv	a0,s0
    2330:	3da5                	c.jal	21a8 <sdudcInitDeviceEp>
    2332:	4681                	c.li	a3,0
    2334:	4601                	c.li	a2,0
    2336:	85ce                	c.mv	a1,s3
    2338:	8522                	c.mv	a0,s0
    233a:	35bd                	c.jal	21a8 <sdudcInitDeviceEp>
    233c:	0485                	c.addi	s1,1
    233e:	0ff4f493          	andi	s1,s1,255
    2342:	0a490913          	addi	s2,s2,164
    2346:	0a19                	c.addi	s4,6
    2348:	bd04ef5b          	bnec	s1,16,2326 <sdudcSetup+0x5c>
    234c:	08442703          	lw	a4,132(s0)
    2350:	8060                	exec.it	#48     !li	a1,64
    2352:	1e070513          	addi	a0,a4,480
    2356:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    2358:	08442803          	lw	a6,132(s0)
    235c:	4581                	c.li	a1,0
    235e:	19680513          	addi	a0,a6,406
    2362:	8c04                	exec.it	#14     !jal	ra,2d56 <CPS_UncachedWrite16>
    2364:	08442883          	lw	a7,132(s0)
    2368:	4581                	c.li	a1,0
    236a:	19488513          	addi	a0,a7,404
    236e:	8c04                	exec.it	#14     !jal	ra,2d56 <CPS_UncachedWrite16>
    2370:	8620                	exec.it	#82     !lw	t3,132(s0)
    2372:	64c1                	c.lui	s1,0x10
    2374:	18ae0513          	addi	a0,t3,394
    2378:	8204                	exec.it	#72     !addi	a1,s1,-1 # ffff <__global_pointer$+0x9ca3>
    237a:	8c04                	exec.it	#14     !jal	ra,2d56 <CPS_UncachedWrite16>
    237c:	8230                	exec.it	#81     !lw	t4,132(s0)
    237e:	8204                	exec.it	#72     !addi	a1,s1,-1
    2380:	188e8513          	addi	a0,t4,392
    2384:	8c04                	exec.it	#14     !jal	ra,2d56 <CPS_UncachedWrite16>
    2386:	08442f03          	lw	t5,132(s0)
    238a:	0ef00593          	li	a1,239
    238e:	18cf0513          	addi	a0,t5,396
    2392:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    2394:	08442f83          	lw	t6,132(s0)
    2398:	45c1                	c.li	a1,16
    239a:	1a2f8513          	addi	a0,t6,418
    239e:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    23a0:	08442783          	lw	a5,132(s0)
    23a4:	07000593          	li	a1,112
    23a8:	1a278513          	addi	a0,a5,418 # 11a2 <scsiWrite_10+0xec>
    23ac:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    23ae:	8c70                	exec.it	#55     !lw	t0,132(s0)
    23b0:	06000593          	li	a1,96
    23b4:	1a228513          	addi	a0,t0,418
    23b8:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    23ba:	6305                	c.lui	t1,0x1
    23bc:	941a                	c.add	s0,t1
    23be:	4585                	c.li	a1,1
    23c0:	62b404a3          	sb	a1,1577(s0)
    23c4:	71d0206f          	j	52e0 <__riscv_restore_10>
    23c8:	000a4083          	lbu	ra,0(s4)
    23cc:	00008f63          	beqz	ra,23ea <sdudcSetup+0x120>
    23d0:	4685                	c.li	a3,1
    23d2:	8626                	c.mv	a2,s1
    23d4:	85ca                	c.mv	a1,s2
    23d6:	8522                	c.mv	a0,s0
    23d8:	3bc1                	c.jal	21a8 <sdudcInitDeviceEp>
    23da:	060a4503          	lbu	a0,96(s4)
    23de:	c919                	c.beqz	a0,23f4 <sdudcSetup+0x12a>
    23e0:	4681                	c.li	a3,0
    23e2:	8626                	c.mv	a2,s1
    23e4:	018905b3          	add	a1,s2,s8
    23e8:	bf81                	c.j	2338 <sdudcSetup+0x6e>
    23ea:	09592223          	sw	s5,132(s2)
    23ee:	08990423          	sb	s1,136(s2)
    23f2:	b7e5                	c.j	23da <sdudcSetup+0x110>
    23f4:	01690633          	add	a2,s2,s6
    23f8:	01562023          	sw	s5,0(a2)
    23fc:	017906b3          	add	a3,s2,s7
    2400:	00968023          	sb	s1,0(a3) # 1000 <mscApp+0xa>
    2404:	bf25                	c.j	233c <sdudcSetup+0x72>

00002406 <sdudcStart>:
    2406:	cd39                	c.beqz	a0,2464 <sdudcStart+0x5e>
    2408:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    240a:	15b54783          	lbu	a5,347(a0)
    240e:	842a                	c.mv	s0,a0
    2410:	08452483          	lw	s1,132(a0)
    2414:	eb81                	c.bnez	a5,2424 <sdudcStart+0x1e>
    2416:	6285                	c.lui	t0,0x1
    2418:	8240                	exec.it	#96     !add	t1,a0,t0
    241a:	62934383          	lbu	t2,1577(t1) # 1629 <scsiExecCmd+0x431>
    241e:	04039263          	bnez	t2,2462 <sdudcStart+0x5c>
    2422:	3565                	c.jal	22ca <sdudcSetup>
    2424:	19848493          	addi	s1,s1,408
    2428:	8526                	c.mv	a0,s1
    242a:	8020                	exec.it	#16     !jal	ra,2d38 <CPS_UncachedRead8>
    242c:	01156593          	ori	a1,a0,17
    2430:	0ff5f593          	andi	a1,a1,255
    2434:	8526                	c.mv	a0,s1
    2436:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    2438:	8c54                	exec.it	#47     !lw	a0,132(s0)
    243a:	8a14                	exec.it	#77     !addi	a0,a0,419
    243c:	8020                	exec.it	#16     !jal	ra,2d38 <CPS_UncachedRead8>
    243e:	08442603          	lw	a2,132(s0)
    2442:	0bf57593          	andi	a1,a0,191
    2446:	1a360513          	addi	a0,a2,419
    244a:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    244c:	15b44683          	lbu	a3,347(s0)
    2450:	ca89                	c.beqz	a3,2462 <sdudcStart+0x5c>
    2452:	6085                	c.lui	ra,0x1
    2454:	9406                	c.add	s0,ra
    2456:	60842703          	lw	a4,1544(s0)
    245a:	8844                	exec.it	#44     !lw	a0,1552(s0)
    245c:	00c72803          	lw	a6,12(a4)
    2460:	9802                	c.jalr	a6
    2462:	8c14                	exec.it	#15     !j	52f4 <__riscv_restore_0>
    2464:	8082                	c.jr	ra

00002466 <sdudcProbe>:
    2466:	10000737          	lui	a4,0x10000
    246a:	02072e23          	sw	zero,60(a4) # 1000003c <_stack+0xff6003c>
    246e:	300012b7          	lui	t0,0x30001
    2472:	11100693          	li	a3,273
    2476:	02d2a023          	sw	a3,32(t0) # 30001020 <_stack+0x2ff61020>
    247a:	4305                	c.li	t1,1
    247c:	0062ac23          	sw	t1,24(t0)
    2480:	0062ae23          	sw	t1,28(t0)
    2484:	c929                	c.beqz	a0,24d6 <sdudcProbe+0x70>
    2486:	87aa                	c.mv	a5,a0
    2488:	4559                	c.li	a0,22
    248a:	c9a1                	c.beqz	a1,24da <sdudcProbe+0x74>
    248c:	0c87a383          	lw	t2,200(a5)
    2490:	ffc38613          	addi	a2,t2,-4 # ffc <mscApp+0x6>
    2494:	ffb67813          	andi	a6,a2,-5
    2498:	04081163          	bnez	a6,24da <sdudcProbe+0x74>
    249c:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
    249e:	0007a083          	lw	ra,0(a5)
    24a2:	1101                	c.addi	sp,-32
    24a4:	40008513          	addi	a0,ra,1024 # 1400 <scsiExecCmd+0x208>
    24a8:	842e                	c.mv	s0,a1
    24aa:	c82a                	c.swsp	a0,16(sp)
    24ac:	8a60                	exec.it	#116     !jal	ra,379a <CUSBDMA_GetInstance>
    24ae:	00052883          	lw	a7,0(a0)
    24b2:	002c                	c.addi4spn	a1,sp,8
    24b4:	0808                	c.addi4spn	a0,sp,16
    24b6:	9882                	c.jalr	a7
    24b8:	ed09                	c.bnez	a0,24d2 <sdudcProbe+0x6c>
    24ba:	45a2                	c.lwsp	a1,8(sp)
    24bc:	6e05                	c.lui	t3,0x1
    24be:	4fb2                	c.lwsp	t6,12(sp)
    24c0:	638e0e93          	addi	t4,t3,1592 # 1638 <scsiExecCmd+0x440>
    24c4:	01d58f33          	add	t5,a1,t4
    24c8:	040f8713          	addi	a4,t6,64
    24cc:	01e42023          	sw	t5,0(s0)
    24d0:	c058                	c.sw	a4,4(s0)
    24d2:	6105                	c.addi16sp	sp,32
    24d4:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>
    24d6:	4559                	c.li	a0,22
    24d8:	8082                	c.jr	ra
    24da:	8082                	c.jr	ra

000024dc <sdudcInit>:
    24dc:	47d9                	c.li	a5,22
    24de:	10050963          	beqz	a0,25f0 <sdudcInit+0x114>
    24e2:	10058763          	beqz	a1,25f0 <sdudcInit+0x114>
    24e6:	10060563          	beqz	a2,25f0 <sdudcInit+0x114>
    24ea:	8830                	exec.it	#21     !jal	t0,52b6 <__riscv_save_4>
    24ec:	6985                	c.lui	s3,0x1
    24ee:	1101                	c.addi	sp,-32
    24f0:	84ae                	c.mv	s1,a1
    24f2:	8932                	c.mv	s2,a2
    24f4:	4581                	c.li	a1,0
    24f6:	63898613          	addi	a2,s3,1592 # 1638 <scsiExecCmd+0x440>
    24fa:	842a                	c.mv	s0,a0
    24fc:	8430                	exec.it	#19     !jal	ra,5526 <memset>
    24fe:	85ca                	c.mv	a1,s2
    2500:	02000613          	li	a2,32
    2504:	16440513          	addi	a0,s0,356
    2508:	8c20                	exec.it	#22     !jal	ra,5350 <memcpy>
    250a:	0d800613          	li	a2,216
    250e:	85a6                	c.mv	a1,s1
    2510:	08c40513          	addi	a0,s0,140
    2514:	8c20                	exec.it	#22     !jal	ra,5350 <memcpy>
    2516:	0004a283          	lw	t0,0(s1)
    251a:	b2c18313          	addi	t1,gp,-1236 # 5e88 <sdudcDrv>
    251e:	08542223          	sw	t0,132(s0)
    2522:	08642423          	sw	t1,136(s0)
    2526:	8a60                	exec.it	#116     !jal	ra,379a <CUSBDMA_GetInstance>
    2528:	63898393          	addi	t2,s3,1592
    252c:	01340933          	add	s2,s0,s3
    2530:	007409b3          	add	s3,s0,t2
    2534:	60a92423          	sw	a0,1544(s2)
    2538:	61392823          	sw	s3,1552(s2)
    253c:	4088                	c.lw	a0,0(s1)
    253e:	40050593          	addi	a1,a0,1024
    2542:	60b92a23          	sw	a1,1556(s2)
    2546:	0d04a603          	lw	a2,208(s1)
    254a:	60c92e23          	sw	a2,1564(s2)
    254e:	0d44a683          	lw	a3,212(s1)
    2552:	62d92023          	sw	a3,1568(s2)
    2556:	8a60                	exec.it	#116     !jal	ra,379a <CUSBDMA_GetInstance>
    2558:	4118                	c.lw	a4,0(a0)
    255a:	002c                	c.addi4spn	a1,sp,8
    255c:	0808                	c.addi4spn	a0,sp,16
    255e:	9702                	c.jalr	a4
    2560:	0d04a883          	lw	a7,208(s1)
    2564:	4832                	c.lwsp	a6,12(sp)
    2566:	01088e33          	add	t3,a7,a6
    256a:	63c92623          	sw	t3,1580(s2)
    256e:	0d44ae83          	lw	t4,212(s1)
    2572:	010e8f33          	add	t5,t4,a6
    2576:	63e92823          	sw	t5,1584(s2)
    257a:	0c44cf83          	lbu	t6,196(s1)
    257e:	060f8663          	beqz	t6,25ea <sdudcInit+0x10e>
    2582:	50fd                	c.li	ra,-1
    2584:	60192c23          	sw	ra,1560(s2)
    2588:	0cf4c783          	lbu	a5,207(s1)
    258c:	cf8d                	c.beqz	a5,25c6 <sdudcInit+0xea>
    258e:	6285                	c.lui	t0,0x1
    2590:	005404b3          	add	s1,s0,t0
    2594:	6084a583          	lw	a1,1544(s1)
    2598:	61428513          	addi	a0,t0,1556 # 1614 <scsiExecCmd+0x41c>
    259c:	00000317          	auipc	t1,0x0
    25a0:	3f630313          	addi	t1,t1,1014 # 2992 <sdudcCallbackTransfer>
    25a4:	6264a223          	sw	t1,1572(s1)
    25a8:	62428393          	addi	t2,t0,1572
    25ac:	41d4                	c.lw	a3,4(a1)
    25ae:	00a405b3          	add	a1,s0,a0
    25b2:	8450                	exec.it	#35     !lw	a0,1552(s1)
    25b4:	00740633          	add	a2,s0,t2
    25b8:	9682                	c.jalr	a3
    25ba:	6084a603          	lw	a2,1544(s1)
    25be:	8450                	exec.it	#35     !lw	a0,1552(s1)
    25c0:	5e58                	c.lw	a4,60(a2)
    25c2:	85a2                	c.mv	a1,s0
    25c4:	9702                	c.jalr	a4
    25c6:	08442803          	lw	a6,132(s0)
    25ca:	1a380513          	addi	a0,a6,419
    25ce:	8020                	exec.it	#16     !jal	ra,2d38 <CPS_UncachedRead8>
    25d0:	8620                	exec.it	#82     !lw	t3,132(s0)
    25d2:	04056893          	ori	a7,a0,64
    25d6:	0ff8f593          	andi	a1,a7,255
    25da:	1a3e0513          	addi	a0,t3,419
    25de:	8400                	exec.it	#2     !jal	ra,2d50 <CPS_UncachedWrite8>
    25e0:	8522                	c.mv	a0,s0
    25e2:	31e5                	c.jal	22ca <sdudcSetup>
    25e4:	4501                	c.li	a0,0
    25e6:	6105                	c.addi16sp	sp,32
    25e8:	8024                	exec.it	#24     !j	52ea <__riscv_restore_4>
    25ea:	60092c23          	sw	zero,1560(s2)
    25ee:	bf69                	c.j	2588 <sdudcInit+0xac>
    25f0:	853e                	c.mv	a0,a5
    25f2:	8082                	c.jr	ra

000025f4 <sdudcGetConfigParams>:
    25f4:	8082                	c.jr	ra

000025f6 <sdudcVbusDraw>:
    25f6:	c119                	c.beqz	a0,25fc <sdudcVbusDraw+0x6>
    25f8:	8c44                	exec.it	#46     !li	a0,134
    25fa:	8082                	c.jr	ra
    25fc:	4559                	c.li	a0,22
    25fe:	8082                	c.jr	ra

00002600 <sdudcPullUp>:
    2600:	c119                	c.beqz	a0,2606 <sdudcPullUp+0x6>
    2602:	8c44                	exec.it	#46     !li	a0,134
    2604:	8082                	c.jr	ra
    2606:	4559                	c.li	a0,22
    2608:	8082                	c.jr	ra

0000260a <sdudcEpXCallback.isra.3>:
    260a:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    260c:	4198                	c.lw	a4,0(a1)
    260e:	0045a803          	lw	a6,4(a1)
    2612:	01072223          	sw	a6,4(a4)
    2616:	0045a083          	lw	ra,4(a1)
    261a:	0005a283          	lw	t0,0(a1)
    261e:	0050a023          	sw	t0,0(ra)
    2622:	0005a223          	sw	zero,4(a1)
    2626:	8650                	exec.it	#99     !sw	zero,0(a1)
    2628:	09c52303          	lw	t1,156(a0)
    262c:	8604                	exec.it	#74     !addi	t2,t1,-1
    262e:	08752e23          	sw	t2,156(a0)
    2632:	d990                	c.sw	a2,48(a1)
    2634:	5590                	c.lw	a2,40(a1)
    2636:	9602                	c.jalr	a2
    2638:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

0000263a <sdudcAbortEndpoint>:
    263a:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    263c:	842e                	c.mv	s0,a1
    263e:	0985a583          	lw	a1,152(a1)
    2642:	84aa                	c.mv	s1,a0
    2644:	c185                	c.beqz	a1,2664 <sdudcAbortEndpoint+0x2a>
    2646:	08844783          	lbu	a5,136(s0)
    264a:	cf89                	c.beqz	a5,2664 <sdudcAbortEndpoint+0x2a>
    264c:	6085                	c.lui	ra,0x1
    264e:	001502b3          	add	t0,a0,ra
    2652:	6082a703          	lw	a4,1544(t0)
    2656:	6102a503          	lw	a0,1552(t0)
    265a:	02072303          	lw	t1,32(a4)
    265e:	9302                	c.jalr	t1
    2660:	08042c23          	sw	zero,152(s0)
    2664:	08844383          	lbu	t2,136(s0)
    2668:	02039663          	bnez	t2,2694 <sdudcAbortEndpoint+0x5a>
    266c:	09842583          	lw	a1,152(s0)
    2670:	c195                	c.beqz	a1,2694 <sdudcAbortEndpoint+0x5a>
    2672:	6505                	c.lui	a0,0x1
    2674:	15b4c683          	lbu	a3,347(s1)
    2678:	00a48633          	add	a2,s1,a0
    267c:	60862803          	lw	a6,1544(a2)
    2680:	61062503          	lw	a0,1552(a2)
    2684:	e689                	c.bnez	a3,268e <sdudcAbortEndpoint+0x54>
    2686:	62864883          	lbu	a7,1576(a2)
    268a:	0218e25b          	bnec	a7,1,26ae <sdudcAbortEndpoint+0x74>
    268e:	02882e03          	lw	t3,40(a6)
    2692:	9e02                	c.jalr	t3
    2694:	6585                	c.lui	a1,0x1
    2696:	60458e93          	addi	t4,a1,1540 # 1604 <scsiExecCmd+0x40c>
    269a:	08c40913          	addi	s2,s0,140
    269e:	94f6                	c.add	s1,t4
    26a0:	08c42583          	lw	a1,140(s0)
    26a4:	01259b63          	bne	a1,s2,26ba <sdudcAbortEndpoint+0x80>
    26a8:	08042223          	sw	zero,132(s0)
    26ac:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    26ae:	02082903          	lw	s2,32(a6)
    26b2:	9902                	c.jalr	s2
    26b4:	08042c23          	sw	zero,152(s0)
    26b8:	bff1                	c.j	2694 <sdudcAbortEndpoint+0x5a>
    26ba:	07a44f03          	lbu	t5,122(s0)
    26be:	000f1863          	bnez	t5,26ce <sdudcAbortEndpoint+0x94>
    26c2:	862e                	c.mv	a2,a1
    26c4:	4691                	c.li	a3,4
    26c6:	85a2                	c.mv	a1,s0
    26c8:	8526                	c.mv	a0,s1
    26ca:	8664                	exec.it	#122     !jal	ra,1a62 <sdudcEp0Callback.isra.0>
    26cc:	bfd1                	c.j	26a0 <sdudcAbortEndpoint+0x66>
    26ce:	4611                	c.li	a2,4
    26d0:	8522                	c.mv	a0,s0
    26d2:	3f25                	c.jal	260a <sdudcEpXCallback.isra.3>
    26d4:	b7f1                	c.j	26a0 <sdudcAbortEndpoint+0x66>

000026d6 <sdudcStopActivity>:
    26d6:	8660                	exec.it	#114     !jal	t0,52b6 <__riscv_save_4>
    26d8:	6785                	c.lui	a5,0x1
    26da:	737d                	c.lui	t1,0xfffff
    26dc:	bc478413          	addi	s0,a5,-1084 # bc4 <setup+0x398>
    26e0:	60478293          	addi	t0,a5,1540
    26e4:	892a                	c.mv	s2,a0
    26e6:	942a                	c.add	s0,a0
    26e8:	005504b3          	add	s1,a0,t0
    26ec:	64430a13          	addi	s4,t1,1604 # fffff644 <_stack+0xfff5f644>
    26f0:	5c030993          	addi	s3,t1,1472
    26f4:	014403b3          	add	t2,s0,s4
    26f8:	0003a503          	lw	a0,0(t2)
    26fc:	0035565b          	beqc	a0,3,2708 <sdudcStopActivity+0x32>
    2700:	013405b3          	add	a1,s0,s3
    2704:	854a                	c.mv	a0,s2
    2706:	3f15                	c.jal	263a <sdudcAbortEndpoint>
    2708:	08442583          	lw	a1,132(s0)
    270c:	0035d55b          	beqc	a1,3,2716 <sdudcStopActivity+0x40>
    2710:	85a2                	c.mv	a1,s0
    2712:	854a                	c.mv	a0,s2
    2714:	371d                	c.jal	263a <sdudcAbortEndpoint>
    2716:	0a440413          	addi	s0,s0,164
    271a:	fc941de3          	bne	s0,s1,26f4 <sdudcStopActivity+0x1e>
    271e:	8674                	exec.it	#123     !j	52ea <__riscv_restore_4>

00002720 <sdudcEpDisable>:
    2720:	cd4d                	c.beqz	a0,27da <sdudcEpDisable+0xba>
    2722:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    2724:	84aa                	c.mv	s1,a0
    2726:	4559                	c.li	a0,22
    2728:	c9c9                	c.beqz	a1,27ba <sdudcEpDisable+0x9a>
    272a:	15b4c783          	lbu	a5,347(s1)
    272e:	c789                	c.beqz	a5,2738 <sdudcEpDisable+0x18>
    2730:	07a5c283          	lbu	t0,122(a1)
    2734:	08028363          	beqz	t0,27ba <sdudcEpDisable+0x9a>
    2738:	0885c083          	lbu	ra,136(a1)
    273c:	8e04                	exec.it	#78     !sb	zero,160(a1)
    273e:	842e                	c.mv	s0,a1
    2740:	08008a63          	beqz	ra,27d4 <sdudcEpDisable+0xb4>
    2744:	07c5a303          	lw	t1,124(a1)
    2748:	00330383          	lb	t2,3(t1)
    274c:	0033f513          	andi	a0,t2,3
    2750:	0215605b          	bnec	a0,1,2770 <sdudcEpDisable+0x50>
    2754:	1504c583          	lbu	a1,336(s1)
    2758:	ed81                	c.bnez	a1,2770 <sdudcEpDisable+0x50>
    275a:	8220                	exec.it	#80     !lw	a2,132(s1)
    275c:	19860513          	addi	a0,a2,408
    2760:	2be1                	c.jal	2d38 <CPS_UncachedRead8>
    2762:	0844a683          	lw	a3,132(s1)
    2766:	0fd57593          	andi	a1,a0,253
    276a:	19868513          	addi	a0,a3,408
    276e:	23cd                	c.jal	2d50 <CPS_UncachedWrite8>
    2770:	08844703          	lbu	a4,136(s0)
    2774:	08944e83          	lbu	t4,137(s0)
    2778:	fff70893          	addi	a7,a4,-1
    277c:	0844a803          	lw	a6,132(s1)
    2780:	00389e13          	slli	t3,a7,0x3
    2784:	020e8c63          	beqz	t4,27bc <sdudcEpDisable+0x9c>
    2788:	00ee0093          	addi	ra,t3,14
    278c:	00180533          	add	a0,a6,ra
    2790:	2365                	c.jal	2d38 <CPS_UncachedRead8>
    2792:	08844303          	lbu	t1,136(s0)
    2796:	00331393          	slli	t2,t1,0x3
    279a:	00638293          	addi	t0,t2,6
    279e:	8220                	exec.it	#80     !lw	a2,132(s1)
    27a0:	07f57593          	andi	a1,a0,127
    27a4:	00560533          	add	a0,a2,t0
    27a8:	2365                	c.jal	2d50 <CPS_UncachedWrite8>
    27aa:	85a2                	c.mv	a1,s0
    27ac:	8526                	c.mv	a0,s1
    27ae:	3571                	c.jal	263a <sdudcAbortEndpoint>
    27b0:	06042e23          	sw	zero,124(s0)
    27b4:	08042223          	sw	zero,132(s0)
    27b8:	4501                	c.li	a0,0
    27ba:	8c14                	exec.it	#15     !j	52f4 <__riscv_restore_0>
    27bc:	00ae0f13          	addi	t5,t3,10
    27c0:	01e80533          	add	a0,a6,t5
    27c4:	2b95                	c.jal	2d38 <CPS_UncachedRead8>
    27c6:	08844f83          	lbu	t6,136(s0)
    27ca:	003f9793          	slli	a5,t6,0x3
    27ce:	00278293          	addi	t0,a5,2
    27d2:	b7f1                	c.j	279e <sdudcEpDisable+0x7e>
    27d4:	8526                	c.mv	a0,s1
    27d6:	3595                	c.jal	263a <sdudcAbortEndpoint>
    27d8:	b7c5                	c.j	27b8 <sdudcEpDisable+0x98>
    27da:	4559                	c.li	a0,22
    27dc:	8082                	c.jr	ra

000027de <sdudcReset>:
    27de:	8830                	exec.it	#21     !jal	t0,52b6 <__riscv_save_4>
    27e0:	455c                	c.lw	a5,12(a0)
    27e2:	842a                	c.mv	s0,a0
    27e4:	cb91                	c.beqz	a5,27f8 <sdudcReset+0x1a>
    27e6:	16452303          	lw	t1,356(a0) # 1164 <scsiWrite_10+0xae>
    27ea:	00052623          	sw	zero,12(a0)
    27ee:	00052a23          	sw	zero,20(a0)
    27f2:	00030363          	beqz	t1,27f8 <sdudcReset+0x1a>
    27f6:	9302                	c.jalr	t1
    27f8:	408d                	c.li	ra,3
    27fa:	6285                	c.lui	t0,0x1
    27fc:	00042d23          	sw	zero,26(s0)
    2800:	00142a23          	sw	ra,20(s0)
    2804:	005403b3          	add	t2,s0,t0
    2808:	8522                	c.mv	a0,s0
    280a:	3e800493          	li	s1,1000
    280e:	490d                	c.li	s2,3
    2810:	00003997          	auipc	s3,0x3
    2814:	32c98993          	addi	s3,s3,812 # 5b3c <CSWTCH.52>
    2818:	60038723          	sb	zero,1550(t2)
    281c:	6003a223          	sw	zero,1540(t2)
    2820:	3d5d                	c.jal	26d6 <sdudcStopActivity>
    2822:	8c54                	exec.it	#47     !lw	a0,132(s0)
    2824:	1a950513          	addi	a0,a0,425
    2828:	2b01                	c.jal	2d38 <CPS_UncachedRead8>
    282a:	8470                	exec.it	#51     !addi	a1,a0,-1
    282c:	0ff5f613          	andi	a2,a1,255
    2830:	00c97763          	bgeu	s2,a2,283e <sdudcReset+0x60>
    2834:	00042623          	sw	zero,12(s0)
    2838:	14fd                	c.addi	s1,-1
    283a:	f4e5                	c.bnez	s1,2822 <sdudcReset+0x44>
    283c:	a801                	c.j	284c <sdudcReset+0x6e>
    283e:	00c986b3          	add	a3,s3,a2
    2842:	0006c703          	lbu	a4,0(a3)
    2846:	c458                	c.sw	a4,12(s0)
    2848:	be37685b          	bnec	a4,3,2838 <sdudcReset+0x5a>
    284c:	8024                	exec.it	#24     !j	52ea <__riscv_restore_4>

0000284e <sdudcStop>:
    284e:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    2850:	6485                	c.lui	s1,0x1
    2852:	94aa                	c.add	s1,a0
    2854:	6294c783          	lbu	a5,1577(s1) # 1629 <scsiExecCmd+0x431>
    2858:	c3b1                	c.beqz	a5,289c <sdudcStop+0x4e>
    285a:	842a                	c.mv	s0,a0
    285c:	08452903          	lw	s2,132(a0)
    2860:	3fbd                	c.jal	27de <sdudcReset>
    2862:	8c54                	exec.it	#47     !lw	a0,132(s0)
    2864:	8a14                	exec.it	#77     !addi	a0,a0,419
    2866:	29c9                	c.jal	2d38 <CPS_UncachedRead8>
    2868:	8c70                	exec.it	#55     !lw	t0,132(s0)
    286a:	04056593          	ori	a1,a0,64
    286e:	0ff5f593          	andi	a1,a1,255
    2872:	1a328513          	addi	a0,t0,419 # 11a3 <scsiWrite_10+0xed>
    2876:	29e9                	c.jal	2d50 <CPS_UncachedWrite8>
    2878:	4581                	c.li	a1,0
    287a:	19890513          	addi	a0,s2,408
    287e:	29c9                	c.jal	2d50 <CPS_UncachedWrite8>
    2880:	15b44303          	lbu	t1,347(s0)
    2884:	00030863          	beqz	t1,2894 <sdudcStop+0x46>
    2888:	6084a083          	lw	ra,1544(s1)
    288c:	8450                	exec.it	#35     !lw	a0,1552(s1)
    288e:	0100a383          	lw	t2,16(ra) # 1010 <mscApp+0x1a>
    2892:	9382                	c.jalr	t2
    2894:	6605                	c.lui	a2,0x1
    2896:	9432                	c.add	s0,a2
    2898:	620404a3          	sb	zero,1577(s0)
    289c:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>

0000289e <sdudcDestroy>:
    289e:	c51d                	c.beqz	a0,28cc <sdudcDestroy+0x2e>
    28a0:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
    28a2:	842a                	c.mv	s0,a0
    28a4:	8e14                	exec.it	#79     !lw	a0,132(a0)
    28a6:	8060                	exec.it	#48     !li	a1,64
    28a8:	8a14                	exec.it	#77     !addi	a0,a0,419
    28aa:	215d                	c.jal	2d50 <CPS_UncachedWrite8>
    28ac:	16442783          	lw	a5,356(s0)
    28b0:	00042623          	sw	zero,12(s0)
    28b4:	00042a23          	sw	zero,20(s0)
    28b8:	c399                	c.beqz	a5,28be <sdudcDestroy+0x20>
    28ba:	8522                	c.mv	a0,s0
    28bc:	9782                	c.jalr	a5
    28be:	8522                	c.mv	a0,s0
    28c0:	3d19                	c.jal	26d6 <sdudcStopActivity>
    28c2:	6285                	c.lui	t0,0x1
    28c4:	9416                	c.add	s0,t0
    28c6:	620404a3          	sb	zero,1577(s0)
    28ca:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>
    28cc:	8082                	c.jr	ra

000028ce <sdudcIsr>:
    28ce:	8830                	exec.it	#21     !jal	t0,52b6 <__riscv_save_4>
    28d0:	08452903          	lw	s2,132(a0)
    28d4:	842a                	c.mv	s0,a0
    28d6:	18c90993          	addi	s3,s2,396
    28da:	854e                	c.mv	a0,s3
    28dc:	29b1                	c.jal	2d38 <CPS_UncachedRead8>
    28de:	84aa                	c.mv	s1,a0
    28e0:	19890513          	addi	a0,s2,408
    28e4:	2991                	c.jal	2d38 <CPS_UncachedRead8>
    28e6:	00a4f2b3          	and	t0,s1,a0
    28ea:	0ff2f493          	andi	s1,t0,255
    28ee:	c4c1                	c.beqz	s1,2976 <sdudcIsr+0xa8>
    28f0:	1c04b7db          	bfos	a5,s1,7,0
    28f4:	0007d563          	bgez	a5,28fe <sdudcIsr+0x30>
    28f8:	8840                	exec.it	#36     !li	a1,128
    28fa:	854e                	c.mv	a0,s3
    28fc:	2991                	c.jal	2d50 <CPS_UncachedWrite8>
    28fe:	0244f65b          	bbc	s1,4,292a <sdudcIsr+0x5c>
    2902:	45c1                	c.li	a1,16
    2904:	854e                	c.mv	a0,s3
    2906:	21a9                	c.jal	2d50 <CPS_UncachedWrite8>
    2908:	6905                	c.lui	s2,0x1
    290a:	9922                	c.add	s2,s0
    290c:	4305                	c.li	t1,1
    290e:	8522                	c.mv	a0,s0
    2910:	62690423          	sb	t1,1576(s2) # 1628 <scsiExecCmd+0x430>
    2914:	35e9                	c.jal	27de <sdudcReset>
    2916:	438d                	c.li	t2,3
    2918:	62090423          	sb	zero,1576(s2)
    291c:	16842583          	lw	a1,360(s0)
    2920:	00742a23          	sw	t2,20(s0)
    2924:	c199                	c.beqz	a1,292a <sdudcIsr+0x5c>
    2926:	8522                	c.mv	a0,s0
    2928:	9582                	c.jalr	a1
    292a:	0054f85b          	bbc	s1,5,293a <sdudcIsr+0x6c>
    292e:	854e                	c.mv	a0,s3
    2930:	02000593          	li	a1,32
    2934:	2931                	c.jal	2d50 <CPS_UncachedWrite8>
    2936:	450d                	c.li	a0,3
    2938:	c448                	c.sw	a0,12(s0)
    293a:	0004fa5b          	bbc	s1,0,294e <sdudcIsr+0x80>
    293e:	6085                	c.lui	ra,0x1
    2940:	00140633          	add	a2,s0,ra
    2944:	8522                	c.mv	a0,s0
    2946:	60062223          	sw	zero,1540(a2) # 1604 <scsiExecCmd+0x40c>
    294a:	9b0ff0ef          	jal	ra,1afa <sdudcEp0StageSetup>
    294e:	0014fa5b          	bbc	s1,1,2962 <sdudcIsr+0x94>
    2952:	4589                	c.li	a1,2
    2954:	854e                	c.mv	a0,s3
    2956:	2eed                	c.jal	2d50 <CPS_UncachedWrite8>
    2958:	18042683          	lw	a3,384(s0)
    295c:	c299                	c.beqz	a3,2962 <sdudcIsr+0x94>
    295e:	8522                	c.mv	a0,s0
    2960:	9682                	c.jalr	a3
    2962:	0024f55b          	bbc	s1,2,296c <sdudcIsr+0x9e>
    2966:	4591                	c.li	a1,4
    2968:	854e                	c.mv	a0,s3
    296a:	26dd                	c.jal	2d50 <CPS_UncachedWrite8>
    296c:	0034f55b          	bbc	s1,3,2976 <sdudcIsr+0xa8>
    2970:	45a1                	c.li	a1,8
    2972:	854e                	c.mv	a0,s3
    2974:	2ef1                	c.jal	2d50 <CPS_UncachedWrite8>
    2976:	15844703          	lbu	a4,344(s0)
    297a:	cb19                	c.beqz	a4,2990 <sdudcIsr+0xc2>
    297c:	6805                	c.lui	a6,0x1
    297e:	9442                	c.add	s0,a6
    2980:	60842883          	lw	a7,1544(s0)
    2984:	0148ae03          	lw	t3,20(a7)
    2988:	000e0463          	beqz	t3,2990 <sdudcIsr+0xc2>
    298c:	8844                	exec.it	#44     !lw	a0,1552(s0)
    298e:	9e02                	c.jalr	t3
    2990:	8024                	exec.it	#24     !j	52ea <__riscv_restore_4>

00002992 <sdudcCallbackTransfer>:
    2992:	8040                	exec.it	#32     !jal	t0,52b6 <__riscv_save_4>
    2994:	842a                	c.mv	s0,a0
    2996:	0e059e63          	bnez	a1,2a92 <sdudcCallbackTransfer+0x100>
    299a:	6285                	c.lui	t0,0x1
    299c:	8240                	exec.it	#96     !add	t1,a0,t0
    299e:	60432783          	lw	a5,1540(t1)
    29a2:	4391                	c.li	t2,4
    29a4:	0cf3e863          	bltu	t2,a5,2a74 <sdudcCallbackTransfer+0xe2>
    29a8:	00003517          	auipc	a0,0x3
    29ac:	18050513          	addi	a0,a0,384 # 5b28 <_ITB_BASE_+0x46c>
    29b0:	0cf5065b          	lea.w	a2,a0,a5
    29b4:	4214                	c.lw	a3,0(a2)
    29b6:	00a68733          	add	a4,a3,a0
    29ba:	8702                	c.jr	a4
    29bc:	8522                	c.mv	a0,s0
    29be:	d50ff0ef          	jal	ra,1f0e <sdudcEp0DataSend>
    29c2:	8050                	exec.it	#33     !j	52ea <__riscv_restore_4>
    29c4:	8522                	c.mv	a0,s0
    29c6:	d79fe0ef          	jal	ra,173e <sdudcEp0DataReceive>
    29ca:	bfe5                	c.j	29c2 <sdudcCallbackTransfer+0x30>
    29cc:	6485                	c.lui	s1,0x1
    29ce:	bc448913          	addi	s2,s1,-1084 # bc4 <setup+0x398>
    29d2:	012409b3          	add	s3,s0,s2
    29d6:	00940933          	add	s2,s0,s1
    29da:	60892803          	lw	a6,1544(s2)
    29de:	21042a03          	lw	s4,528(s0)
    29e2:	21040e13          	addi	t3,s0,528
    29e6:	c5c92583          	lw	a1,-932(s2)
    29ea:	03082883          	lw	a7,48(a6) # 1030 <mscApp+0x3a>
    29ee:	61092503          	lw	a0,1552(s2)
    29f2:	17ca0263          	beq	s4,t3,2b56 <sdudcCallbackTransfer+0x1c4>
    29f6:	9882                	c.jalr	a7
    29f8:	02aa2a23          	sw	a0,52(s4)
    29fc:	60092223          	sw	zero,1540(s2)
    2a00:	8230                	exec.it	#81     !lw	t4,132(s0)
    2a02:	4589                	c.li	a1,2
    2a04:	002e8513          	addi	a0,t4,2
    2a08:	26a1                	c.jal	2d50 <CPS_UncachedWrite8>
    2a0a:	60448513          	addi	a0,s1,1540
    2a0e:	4681                	c.li	a3,0
    2a10:	8652                	c.mv	a2,s4
    2a12:	85ce                	c.mv	a1,s3
    2a14:	9522                	c.add	a0,s0
    2a16:	8664                	exec.it	#122     !jal	ra,1a62 <sdudcEp0Callback.isra.0>
    2a18:	b76d                	c.j	29c2 <sdudcCallbackTransfer+0x30>
    2a1a:	21042483          	lw	s1,528(s0)
    2a1e:	21040f13          	addi	t5,s0,528
    2a22:	03e48e63          	beq	s1,t5,2a5e <sdudcCallbackTransfer+0xcc>
    2a26:	cc91                	c.beqz	s1,2a42 <sdudcCallbackTransfer+0xb0>
    2a28:	6f85                	c.lui	t6,0x1
    2a2a:	01f400b3          	add	ra,s0,t6
    2a2e:	6080a583          	lw	a1,1544(ra) # 1608 <scsiExecCmd+0x410>
    2a32:	6100a503          	lw	a0,1552(ra)
    2a36:	0305a303          	lw	t1,48(a1)
    2a3a:	21c42583          	lw	a1,540(s0)
    2a3e:	9302                	c.jalr	t1
    2a40:	d8c8                	c.sw	a0,52(s1)
    2a42:	8c70                	exec.it	#55     !lw	t0,132(s0)
    2a44:	4589                	c.li	a1,2
    2a46:	00228513          	addi	a0,t0,2 # 1002 <mscApp+0xc>
    2a4a:	2619                	c.jal	2d50 <CPS_UncachedWrite8>
    2a4c:	c899                	c.beqz	s1,2a62 <sdudcCallbackTransfer+0xd0>
    2a4e:	6385                	c.lui	t2,0x1
    2a50:	4681                	c.li	a3,0
    2a52:	8626                	c.mv	a2,s1
    2a54:	18440593          	addi	a1,s0,388
    2a58:	60438513          	addi	a0,t2,1540 # 1604 <scsiExecCmd+0x40c>
    2a5c:	bf65                	c.j	2a14 <sdudcCallbackTransfer+0x82>
    2a5e:	4481                	c.li	s1,0
    2a60:	b7cd                	c.j	2a42 <sdudcCallbackTransfer+0xb0>
    2a62:	6785                	c.lui	a5,0x1
    2a64:	943e                	c.add	s0,a5
    2a66:	60042223          	sw	zero,1540(s0)
    2a6a:	bfa1                	c.j	29c2 <sdudcCallbackTransfer+0x30>
    2a6c:	8522                	c.mv	a0,s0
    2a6e:	88cff0ef          	jal	ra,1afa <sdudcEp0StageSetup>
    2a72:	bf81                	c.j	29c2 <sdudcCallbackTransfer+0x30>
    2a74:	08452603          	lw	a2,132(a0)
    2a78:	1a360513          	addi	a0,a2,419
    2a7c:	2c75                	c.jal	2d38 <CPS_UncachedRead8>
    2a7e:	08442703          	lw	a4,132(s0)
    2a82:	00156693          	ori	a3,a0,1
    2a86:	0ff6f593          	andi	a1,a3,255
    2a8a:	1a370513          	addi	a0,a4,419
    2a8e:	24c9                	c.jal	2d50 <CPS_UncachedWrite8>
    2a90:	bf0d                	c.j	29c2 <sdudcCallbackTransfer+0x30>
    2a92:	0a400493          	li	s1,164
    2a96:	8a32                	c.mv	s4,a2
    2a98:	029585b3          	mul	a1,a1,s1
    2a9c:	c649                	c.beqz	a2,2b26 <sdudcCallbackTransfer+0x194>
    2a9e:	18458313          	addi	t1,a1,388
    2aa2:	006404b3          	add	s1,s0,t1
    2aa6:	08c4a903          	lw	s2,140(s1)
    2aaa:	08c48b13          	addi	s6,s1,140
    2aae:	012b1363          	bne	s6,s2,2ab4 <sdudcCallbackTransfer+0x122>
    2ab2:	4901                	c.li	s2,0
    2ab4:	8854                	exec.it	#45     !lw	a1,152(s1)
    2ab6:	cdb5                	c.beqz	a1,2b32 <sdudcCallbackTransfer+0x1a0>
    2ab8:	6985                	c.lui	s3,0x1
    2aba:	99a2                	c.add	s3,s0
    2abc:	6089a783          	lw	a5,1544(s3) # 1608 <scsiExecCmd+0x410>
    2ac0:	8440                	exec.it	#34     !lw	a0,1552(s3)
    2ac2:	0307a383          	lw	t2,48(a5) # 1030 <mscApp+0x3a>
    2ac6:	9382                	c.jalr	t2
    2ac8:	8aaa                	c.mv	s5,a0
    2aca:	6089a503          	lw	a0,1544(s3)
    2ace:	8854                	exec.it	#45     !lw	a1,152(s1)
    2ad0:	5950                	c.lw	a2,52(a0)
    2ad2:	8440                	exec.it	#34     !lw	a0,1552(s3)
    2ad4:	9602                	c.jalr	a2
    2ad6:	03492683          	lw	a3,52(s2)
    2ada:	01568733          	add	a4,a3,s5
    2ade:	02e92a23          	sw	a4,52(s2)
    2ae2:	03492803          	lw	a6,52(s2)
    2ae6:	00c92883          	lw	a7,12(s2)
    2aea:	01180463          	beq	a6,a7,2af2 <sdudcCallbackTransfer+0x160>
    2aee:	04aaff63          	bgeu	s5,a0,2b4c <sdudcCallbackTransfer+0x1ba>
    2af2:	4601                	c.li	a2,0
    2af4:	85ca                	c.mv	a1,s2
    2af6:	8526                	c.mv	a0,s1
    2af8:	3e09                	c.jal	260a <sdudcEpXCallback.isra.3>
    2afa:	07c4ae03          	lw	t3,124(s1)
    2afe:	ec0e02e3          	beqz	t3,29c2 <sdudcCallbackTransfer+0x30>
    2b02:	003e0e83          	lb	t4,3(t3)
    2b06:	003eff13          	andi	t5,t4,3
    2b0a:	001f665b          	bnec	t5,1,2b16 <sdudcCallbackTransfer+0x184>
    2b0e:	15044f83          	lbu	t6,336(s0)
    2b12:	ea0f98e3          	bnez	t6,29c2 <sdudcCallbackTransfer+0x30>
    2b16:	08c4a583          	lw	a1,140(s1)
    2b1a:	00bb1f63          	bne	s6,a1,2b38 <sdudcCallbackTransfer+0x1a6>
    2b1e:	4585                	c.li	a1,1
    2b20:	08b4a223          	sw	a1,132(s1)
    2b24:	bd79                	c.j	29c2 <sdudcCallbackTransfer+0x30>
    2b26:	6085                	c.lui	ra,0x1
    2b28:	bc408293          	addi	t0,ra,-1084 # bc4 <setup+0x398>
    2b2c:	00558333          	add	t1,a1,t0
    2b30:	bf8d                	c.j	2aa2 <sdudcCallbackTransfer+0x110>
    2b32:	4501                	c.li	a0,0
    2b34:	4a81                	c.li	s5,0
    2b36:	b775                	c.j	2ae2 <sdudcCallbackTransfer+0x150>
    2b38:	d1fd                	c.beqz	a1,2b1e <sdudcCallbackTransfer+0x18c>
    2b3a:	4089                	c.li	ra,2
    2b3c:	0814a223          	sw	ra,132(s1)
    2b40:	8522                	c.mv	a0,s0
    2b42:	000a0763          	beqz	s4,2b50 <sdudcCallbackTransfer+0x1be>
    2b46:	cc9fe0ef          	jal	ra,180e <sdudcEpXDataSend>
    2b4a:	bda5                	c.j	29c2 <sdudcCallbackTransfer+0x30>
    2b4c:	85ca                	c.mv	a1,s2
    2b4e:	bfcd                	c.j	2b40 <sdudcCallbackTransfer+0x1ae>
    2b50:	c47fe0ef          	jal	ra,1796 <sdudcEpXDataReceive>
    2b54:	b5bd                	c.j	29c2 <sdudcCallbackTransfer+0x30>
    2b56:	9882                	c.jalr	a7
    2b58:	02002a23          	sw	zero,52(zero) # 34 <_start+0x28>
    2b5c:	9002                	c.ebreak

00002b5e <sdudcEpXQueue>:
    2b5e:	e199                	c.bnez	a1,2b64 <sdudcEpXQueue+0x6>
    2b60:	4559                	c.li	a0,22
    2b62:	8082                	c.jr	ra
    2b64:	de75                	c.beqz	a2,2b60 <sdudcEpXQueue+0x2>
    2b66:	dd6d                	c.beqz	a0,2b60 <sdudcEpXQueue+0x2>
    2b68:	4618                	c.lw	a4,8(a2)
    2b6a:	db7d                	c.beqz	a4,2b60 <sdudcEpXQueue+0x2>
    2b6c:	07700293          	li	t0,119
    2b70:	00c62303          	lw	t1,12(a2)
    2b74:	02062a23          	sw	zero,52(a2)
    2b78:	02562823          	sw	t0,48(a2)
    2b7c:	87ae                	c.mv	a5,a1
    2b7e:	00031563          	bnez	t1,2b88 <sdudcEpXQueue+0x2a>
    2b82:	4385                	c.li	t2,1
    2b84:	04760023          	sb	t2,64(a2)
    2b88:	de1c                	c.sw	a5,56(a2)
    2b8a:	5fec                	c.lw	a1,124(a5)
    2b8c:	d9f1                	c.beqz	a1,2b60 <sdudcEpXQueue+0x2>
    2b8e:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    2b90:	85b2                	c.mv	a1,a2
    2b92:	0907a603          	lw	a2,144(a5)
    2b96:	08c78693          	addi	a3,a5,140
    2b9a:	08b7a823          	sw	a1,144(a5)
    2b9e:	c194                	c.sw	a3,0(a1)
    2ba0:	c1d0                	c.sw	a2,4(a1)
    2ba2:	c20c                	c.sw	a1,0(a2)
    2ba4:	0847a803          	lw	a6,132(a5)
    2ba8:	0218655b          	bnec	a6,1,2bd2 <sdudcEpXQueue+0x74>
    2bac:	08c7a703          	lw	a4,140(a5)
    2bb0:	4881                	c.li	a7,0
    2bb2:	00e59b63          	bne	a1,a4,2bc8 <sdudcEpXQueue+0x6a>
    2bb6:	4089                	c.li	ra,2
    2bb8:	0817a223          	sw	ra,132(a5)
    2bbc:	0897c783          	lbu	a5,137(a5)
    2bc0:	c791                	c.beqz	a5,2bcc <sdudcEpXQueue+0x6e>
    2bc2:	c4dfe0ef          	jal	ra,180e <sdudcEpXDataSend>
    2bc6:	4881                	c.li	a7,0
    2bc8:	8546                	c.mv	a0,a7
    2bca:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
    2bcc:	bcbfe0ef          	jal	ra,1796 <sdudcEpXDataReceive>
    2bd0:	bfdd                	c.j	2bc6 <sdudcEpXQueue+0x68>
    2bd2:	4881                	c.li	a7,0
    2bd4:	be286a5b          	bnec	a6,2,2bc8 <sdudcEpXQueue+0x6a>
    2bd8:	15054e03          	lbu	t3,336(a0)
    2bdc:	fe0e06e3          	beqz	t3,2bc8 <sdudcEpXQueue+0x6a>
    2be0:	07c7ae83          	lw	t4,124(a5)
    2be4:	003e8f03          	lb	t5,3(t4)
    2be8:	003f7f93          	andi	t6,t5,3
    2bec:	bc1fee5b          	bnec	t6,1,2bc8 <sdudcEpXQueue+0x6a>
    2bf0:	b7f1                	c.j	2bbc <sdudcEpXQueue+0x5e>

00002bf2 <sdudcEpQueue>:
    2bf2:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    2bf4:	e581                	c.bnez	a1,2bfc <sdudcEpQueue+0xa>
    2bf6:	44d9                	c.li	s1,22
    2bf8:	8526                	c.mv	a0,s1
    2bfa:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    2bfc:	8432                	c.mv	s0,a2
    2bfe:	de65                	c.beqz	a2,2bf6 <sdudcEpQueue+0x4>
    2c00:	892a                	c.mv	s2,a0
    2c02:	d975                	c.beqz	a0,2bf6 <sdudcEpQueue+0x4>
    2c04:	07a58703          	lb	a4,122(a1)
    2c08:	87ae                	c.mv	a5,a1
    2c0a:	00f77293          	andi	t0,a4,15
    2c0e:	00028563          	beqz	t0,2c18 <sdudcEpQueue+0x26>
    2c12:	37b1                	c.jal	2b5e <sdudcEpXQueue>
    2c14:	84aa                	c.mv	s1,a0
    2c16:	b7cd                	c.j	2bf8 <sdudcEpQueue+0x6>
    2c18:	07700313          	li	t1,119
    2c1c:	08452803          	lw	a6,132(a0)
    2c20:	02062a23          	sw	zero,52(a2)
    2c24:	02662823          	sw	t1,48(a2)
    2c28:	dc0c                	c.sw	a1,56(s0)
    2c2a:	08c5a603          	lw	a2,140(a1)
    2c2e:	08c58393          	addi	t2,a1,140
    2c32:	44c1                	c.li	s1,16
    2c34:	fc7612e3          	bne	a2,t2,2bf8 <sdudcEpQueue+0x6>
    2c38:	6685                	c.lui	a3,0x1
    2c3a:	00d504b3          	add	s1,a0,a3
    2c3e:	6044a583          	lw	a1,1540(s1)
    2c42:	4895                	c.li	a7,5
    2c44:	fab8e9e3          	bltu	a7,a1,2bf6 <sdudcEpQueue+0x4>
    2c48:	4e05                	c.li	t3,1
    2c4a:	00be1eb3          	sll	t4,t3,a1
    2c4e:	026eff13          	andi	t5,t4,38
    2c52:	fa0f02e3          	beqz	t5,2bf6 <sdudcEpQueue+0x4>
    2c56:	0907af83          	lw	t6,144(a5)
    2c5a:	0887a823          	sw	s0,144(a5)
    2c5e:	c010                	c.sw	a2,0(s0)
    2c60:	01f42223          	sw	t6,4(s0)
    2c64:	008fa023          	sw	s0,0(t6) # 1000 <mscApp+0xa>
    2c68:	09c7a703          	lw	a4,156(a5)
    2c6c:	00170293          	addi	t0,a4,1
    2c70:	0857ae23          	sw	t0,156(a5)
    2c74:	6044a783          	lw	a5,1540(s1)
    2c78:	0027d95b          	beqc	a5,2,2c8a <sdudcEpQueue+0x98>
    2c7c:	0057db5b          	beqc	a5,5,2c92 <sdudcEpQueue+0xa0>
    2c80:	0017e75b          	bnec	a5,1,2c8e <sdudcEpQueue+0x9c>
    2c84:	a8aff0ef          	jal	ra,1f0e <sdudcEp0DataSend>
    2c88:	a019                	c.j	2c8e <sdudcEpQueue+0x9c>
    2c8a:	ab5fe0ef          	jal	ra,173e <sdudcEp0DataReceive>
    2c8e:	4481                	c.li	s1,0
    2c90:	b7a5                	c.j	2bf8 <sdudcEpQueue+0x6>
    2c92:	4444                	c.lw	s1,12(s0)
    2c94:	f0ad                	c.bnez	s1,2bf6 <sdudcEpQueue+0x4>
    2c96:	00280513          	addi	a0,a6,2
    2c9a:	4589                	c.li	a1,2
    2c9c:	2855                	c.jal	2d50 <CPS_UncachedWrite8>
    2c9e:	6505                	c.lui	a0,0x1
    2ca0:	60450813          	addi	a6,a0,1540 # 1604 <scsiExecCmd+0x40c>
    2ca4:	5c0c                	c.lw	a1,56(s0)
    2ca6:	4681                	c.li	a3,0
    2ca8:	8622                	c.mv	a2,s0
    2caa:	01090533          	add	a0,s2,a6
    2cae:	8664                	exec.it	#122     !jal	ra,1a62 <sdudcEp0Callback.isra.0>
    2cb0:	b7a1                	c.j	2bf8 <sdudcEpQueue+0x6>

00002cb2 <sdudcEpXDequeue>:
    2cb2:	47d9                	c.li	a5,22
    2cb4:	cdb9                	c.beqz	a1,2d12 <sdudcEpXDequeue+0x60>
    2cb6:	ce31                	c.beqz	a2,2d12 <sdudcEpXDequeue+0x60>
    2cb8:	cd29                	c.beqz	a0,2d12 <sdudcEpXDequeue+0x60>
    2cba:	5e18                	c.lw	a4,56(a2)
    2cbc:	04b71b63          	bne	a4,a1,2d12 <sdudcEpXDequeue+0x60>
    2cc0:	08c5a303          	lw	t1,140(a1)
    2cc4:	08c58293          	addi	t0,a1,140
    2cc8:	04531063          	bne	t1,t0,2d08 <sdudcEpXDequeue+0x56>
    2ccc:	47d9                	c.li	a5,22
    2cce:	04c31263          	bne	t1,a2,2d12 <sdudcEpXDequeue+0x60>
    2cd2:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    2cd4:	0845a083          	lw	ra,132(a1)
    2cd8:	84b2                	c.mv	s1,a2
    2cda:	842e                	c.mv	s0,a1
    2cdc:	0020ef5b          	bnec	ra,2,2cfa <sdudcEpXDequeue+0x48>
    2ce0:	6385                	c.lui	t2,0x1
    2ce2:	951e                	c.add	a0,t2
    2ce4:	60852583          	lw	a1,1544(a0)
    2ce8:	61052503          	lw	a0,1552(a0)
    2cec:	5590                	c.lw	a2,40(a1)
    2cee:	09842583          	lw	a1,152(s0)
    2cf2:	9602                	c.jalr	a2
    2cf4:	4685                	c.li	a3,1
    2cf6:	08d42223          	sw	a3,132(s0)
    2cfa:	8522                	c.mv	a0,s0
    2cfc:	4615                	c.li	a2,5
    2cfe:	85a6                	c.mv	a1,s1
    2d00:	90bff0ef          	jal	ra,260a <sdudcEpXCallback.isra.3>
    2d04:	4501                	c.li	a0,0
    2d06:	8c14                	exec.it	#15     !j	52f4 <__riscv_restore_0>
    2d08:	fcc305e3          	beq	t1,a2,2cd2 <sdudcEpXDequeue+0x20>
    2d0c:	00032303          	lw	t1,0(t1)
    2d10:	bf65                	c.j	2cc8 <sdudcEpXDequeue+0x16>
    2d12:	853e                	c.mv	a0,a5
    2d14:	8082                	c.jr	ra

00002d16 <sdudcEpDequeue>:
    2d16:	c999                	c.beqz	a1,2d2c <sdudcEpDequeue+0x16>
    2d18:	47d9                	c.li	a5,22
    2d1a:	ca11                	c.beqz	a2,2d2e <sdudcEpDequeue+0x18>
    2d1c:	c909                	c.beqz	a0,2d2e <sdudcEpDequeue+0x18>
    2d1e:	07a5c283          	lbu	t0,122(a1)
    2d22:	00028663          	beqz	t0,2d2e <sdudcEpDequeue+0x18>
    2d26:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    2d28:	3769                	c.jal	2cb2 <sdudcEpXDequeue>
    2d2a:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
    2d2c:	47d9                	c.li	a5,22
    2d2e:	853e                	c.mv	a0,a5
    2d30:	8082                	c.jr	ra

00002d32 <CUSBD_GetInstance>:
    2d32:	b2c18513          	addi	a0,gp,-1236 # 5e88 <sdudcDrv>
    2d36:	8082                	c.jr	ra

00002d38 <CPS_UncachedRead8>:
    2d38:	00050503          	lb	a0,0(a0)
    2d3c:	0ff57513          	andi	a0,a0,255
    2d40:	8082                	c.jr	ra

00002d42 <CPS_UncachedRead16>:
    2d42:	00051503          	lh	a0,0(a0)
    2d46:	3c05255b          	bfoz	a0,a0,15,0
    2d4a:	8082                	c.jr	ra

00002d4c <CPS_UncachedRead32>:
    2d4c:	4108                	c.lw	a0,0(a0)
    2d4e:	8082                	c.jr	ra

00002d50 <CPS_UncachedWrite8>:
    2d50:	00b50023          	sb	a1,0(a0)
    2d54:	8082                	c.jr	ra

00002d56 <CPS_UncachedWrite16>:
    2d56:	00b51023          	sh	a1,0(a0)
    2d5a:	8082                	c.jr	ra

00002d5c <CPS_UncachedWrite32>:
    2d5c:	c10c                	c.sw	a1,0(a0)
    2d5e:	8082                	c.jr	ra

00002d60 <sgdmaProbe>:
    2d60:	cd01                	c.beqz	a0,2d78 <sgdmaProbe+0x18>
    2d62:	4559                	c.li	a0,22
    2d64:	c999                	c.beqz	a1,2d7a <sgdmaProbe+0x1a>
    2d66:	6285                	c.lui	t0,0x1
    2d68:	678d                	c.lui	a5,0x3
    2d6a:	e3028313          	addi	t1,t0,-464 # e30 <mscProtocol+0x160>
    2d6e:	c1dc                	c.sw	a5,4(a1)
    2d70:	0065a023          	sw	t1,0(a1)
    2d74:	4501                	c.li	a0,0
    2d76:	8082                	c.jr	ra
    2d78:	4559                	c.li	a0,22
    2d7a:	8082                	c.jr	ra

00002d7c <sgdmaStop>:
    2d7c:	c119                	c.beqz	a0,2d82 <sgdmaStop+0x6>
    2d7e:	4501                	c.li	a0,0
    2d80:	8082                	c.jr	ra
    2d82:	4559                	c.li	a0,22
    2d84:	8082                	c.jr	ra

00002d86 <sgdmaErrIsr>:
    2d86:	cd1d                	c.beqz	a0,2dc4 <sgdmaErrIsr+0x3e>
    2d88:	03000793          	li	a5,48
    2d8c:	02f585b3          	mul	a1,a1,a5
    2d90:	c61d                	c.beqz	a2,2dbe <sgdmaErrIsr+0x38>
    2d92:	31c58293          	addi	t0,a1,796
    2d96:	005503b3          	add	t2,a0,t0
    2d9a:	0103a703          	lw	a4,16(t2) # 1010 <mscApp+0x1a>
    2d9e:	4309                	c.li	t1,2
    2da0:	00e37463          	bgeu	t1,a4,2da8 <sgdmaErrIsr+0x22>
    2da4:	0063a823          	sw	t1,16(t2)
    2da8:	4d14                	c.lw	a3,24(a0)
    2daa:	ce89                	c.beqz	a3,2dc4 <sgdmaErrIsr+0x3e>
    2dac:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    2dae:	0073c603          	lbu	a2,7(t2)
    2db2:	0063c583          	lbu	a1,6(t2)
    2db6:	62452503          	lw	a0,1572(a0)
    2dba:	9682                	c.jalr	a3
    2dbc:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
    2dbe:	01c58293          	addi	t0,a1,28
    2dc2:	bfd1                	c.j	2d96 <sgdmaErrIsr+0x10>
    2dc4:	8082                	c.jr	ra

00002dc6 <sgdmaGetActualLength>:
    2dc6:	c509                	c.beqz	a0,2dd0 <sgdmaGetActualLength+0xa>
    2dc8:	4559                	c.li	a0,22
    2dca:	c581                	c.beqz	a1,2dd2 <sgdmaGetActualLength+0xc>
    2dcc:	5588                	c.lw	a0,40(a1)
    2dce:	8082                	c.jr	ra
    2dd0:	4559                	c.li	a0,22
    2dd2:	8082                	c.jr	ra

00002dd4 <sgdmaSetMaxLength>:
    2dd4:	cd11                	c.beqz	a0,2df0 <sgdmaSetMaxLength+0x1c>
    2dd6:	4559                	c.li	a0,22
    2dd8:	cd89                	c.beqz	a1,2df2 <sgdmaSetMaxLength+0x1e>
    2dda:	01c5c783          	lbu	a5,28(a1)
    2dde:	c781                	c.beqz	a5,2de6 <sgdmaSetMaxLength+0x12>
    2de0:	c5d0                	c.sw	a2,12(a1)
    2de2:	4501                	c.li	a0,0
    2de4:	8082                	c.jr	ra
    2de6:	62c1                	c.lui	t0,0x10
    2de8:	fec2fce3          	bgeu	t0,a2,2de0 <sgdmaSetMaxLength+0xc>
    2dec:	6641                	c.lui	a2,0x10
    2dee:	bfcd                	c.j	2de0 <sgdmaSetMaxLength+0xc>
    2df0:	4559                	c.li	a0,22
    2df2:	8082                	c.jr	ra

00002df4 <sgdmaGetMaxLength>:
    2df4:	c509                	c.beqz	a0,2dfe <sgdmaGetMaxLength+0xa>
    2df6:	4559                	c.li	a0,22
    2df8:	c581                	c.beqz	a1,2e00 <sgdmaGetMaxLength+0xc>
    2dfa:	4588                	c.lw	a0,8(a1)
    2dfc:	8082                	c.jr	ra
    2dfe:	4559                	c.li	a0,22
    2e00:	8082                	c.jr	ra

00002e02 <sgdmaSetParentPriv>:
    2e02:	c119                	c.beqz	a0,2e08 <sgdmaSetParentPriv+0x6>
    2e04:	62b52223          	sw	a1,1572(a0)
    2e08:	8082                	c.jr	ra

00002e0a <sgdmaSetHostMode>:
    2e0a:	c501                	c.beqz	a0,2e12 <sgdmaSetHostMode+0x8>
    2e0c:	4785                	c.li	a5,1
    2e0e:	62f50023          	sb	a5,1568(a0)
    2e12:	8082                	c.jr	ra

00002e14 <sgdmaGetChannelStatus>:
    2e14:	c929                	c.beqz	a0,2e66 <sgdmaGetChannelStatus+0x52>
    2e16:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    2e18:	84aa                	c.mv	s1,a0
    2e1a:	4559                	c.li	a0,22
    2e1c:	c5a1                	c.beqz	a1,2e64 <sgdmaGetChannelStatus+0x50>
    2e1e:	4998                	c.lw	a4,16(a1)
    2e20:	4789                	c.li	a5,2
    2e22:	842e                	c.mv	s0,a1
    2e24:	02e7ff63          	bgeu	a5,a4,2e62 <sgdmaGetChannelStatus+0x4e>
    2e28:	00758583          	lb	a1,7(a1)
    2e2c:	00640083          	lb	ra,6(s0)
    2e30:	4088                	c.lw	a0,0(s1)
    2e32:	0015e2b3          	or	t0,a1,ra
    2e36:	0ff2f593          	andi	a1,t0,255
    2e3a:	0571                	c.addi	a0,28
    2e3c:	3705                	c.jal	2d5c <CPS_UncachedWrite32>
    2e3e:	0004a303          	lw	t1,0(s1)
    2e42:	02830513          	addi	a0,t1,40
    2e46:	3719                	c.jal	2d4c <CPS_UncachedRead32>
    2e48:	0004a383          	lw	t2,0(s1)
    2e4c:	892a                	c.mv	s2,a0
    2e4e:	02c38513          	addi	a0,t2,44
    2e52:	3ded                	c.jal	2d4c <CPS_UncachedRead32>
    2e54:	4069755b          	bbs	s2,6,2e5e <sgdmaGetChannelStatus+0x4a>
    2e58:	468d                	c.li	a3,3
    2e5a:	0095735b          	bbc	a0,9,2e60 <sgdmaGetChannelStatus+0x4c>
    2e5e:	4691                	c.li	a3,4
    2e60:	c814                	c.sw	a3,16(s0)
    2e62:	4808                	c.lw	a0,16(s0)
    2e64:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    2e66:	4559                	c.li	a0,22
    2e68:	8082                	c.jr	ra

00002e6a <sgdmaChannelAlloc>:
    2e6a:	43e022ef          	jal	t0,52a8 <__riscv_save_10>
    2e6e:	47bd                	c.li	a5,15
    2e70:	00052903          	lw	s2,0(a0)
    2e74:	00c7f663          	bgeu	a5,a2,2e80 <sgdmaChannelAlloc+0x16>
    2e78:	4401                	c.li	s0,0
    2e7a:	8522                	c.mv	a0,s0
    2e7c:	4640206f          	j	52e0 <__riscv_restore_10>
    2e80:	03c90a93          	addi	s5,s2,60
    2e84:	842e                	c.mv	s0,a1
    2e86:	84aa                	c.mv	s1,a0
    2e88:	8556                	c.mv	a0,s5
    2e8a:	89b6                	c.mv	s3,a3
    2e8c:	8b32                	c.mv	s6,a2
    2e8e:	3d7d                	c.jal	2d4c <CPS_UncachedRead32>
    2e90:	c845                	c.beqz	s0,2f40 <sgdmaChannelAlloc+0xd6>
    2e92:	03000693          	li	a3,48
    2e96:	02db0833          	mul	a6,s6,a3
    2e9a:	010488b3          	add	a7,s1,a6
    2e9e:	32c8ae03          	lw	t3,812(a7)
    2ea2:	fc0e1be3          	bnez	t3,2e78 <sgdmaChannelAlloc+0xe>
    2ea6:	31c80a13          	addi	s4,a6,796
    2eaa:	f8000b93          	li	s7,-128
    2eae:	010b0c13          	addi	s8,s6,16
    2eb2:	4e85                	c.li	t4,1
    2eb4:	337881a3          	sb	s7,803(a7)
    2eb8:	01448433          	add	s0,s1,s4
    2ebc:	018e9f33          	sll	t5,t4,s8
    2ec0:	00c4da03          	lhu	s4,12(s1)
    2ec4:	00af6bb3          	or	s7,t5,a0
    2ec8:	0a099463          	bnez	s3,2f70 <sgdmaChannelAlloc+0x106>
    2ecc:	02040623          	sb	zero,44(s0)
    2ed0:	00744783          	lbu	a5,7(s0)
    2ed4:	02040993          	addi	s3,s0,32
    2ed8:	4f85                	c.li	t6,1
    2eda:	0167e5b3          	or	a1,a5,s6
    2ede:	01c90513          	addi	a0,s2,28
    2ee2:	03342023          	sw	s3,32(s0)
    2ee6:	03342223          	sw	s3,36(s0)
    2eea:	01f42823          	sw	t6,16(s0)
    2eee:	00040ea3          	sb	zero,29(s0)
    2ef2:	c004                	c.sw	s1,0(s0)
    2ef4:	8c64                	exec.it	#62     !sw	zero,24(s0)
    2ef6:	01640323          	sb	s6,6(s0)
    2efa:	02490c13          	addi	s8,s2,36
    2efe:	3db9                	c.jal	2d5c <CPS_UncachedWrite32>
    2f00:	8562                	c.mv	a0,s8
    2f02:	35a9                	c.jal	2d4c <CPS_UncachedRead32>
    2f04:	61c4a283          	lw	t0,1564(s1)
    2f08:	89aa                	c.mv	s3,a0
    2f0a:	0022ea5b          	bnec	t0,2,2f1e <sgdmaChannelAlloc+0xb4>
    2f0e:	416a5a33          	sra	s4,s4,s6
    2f12:	6089                	c.lui	ra,0x2
    2f14:	400a735b          	bbs	s4,0,2f1a <sgdmaChannelAlloc+0xb0>
    2f18:	6085                	c.lui	ra,0x1
    2f1a:	0019e9b3          	or	s3,s3,ra
    2f1e:	02c90513          	addi	a0,s2,44
    2f22:	08c00593          	li	a1,140
    2f26:	3d1d                	c.jal	2d5c <CPS_UncachedWrite32>
    2f28:	0019e593          	ori	a1,s3,1
    2f2c:	8562                	c.mv	a0,s8
    2f2e:	353d                	c.jal	2d5c <CPS_UncachedWrite32>
    2f30:	85de                	c.mv	a1,s7
    2f32:	8556                	c.mv	a0,s5
    2f34:	3525                	c.jal	2d5c <CPS_UncachedWrite32>
    2f36:	8840                	exec.it	#36     !li	a1,128
    2f38:	03490513          	addi	a0,s2,52
    2f3c:	3505                	c.jal	2d5c <CPS_UncachedWrite32>
    2f3e:	bf35                	c.j	2e7a <sgdmaChannelAlloc+0x10>
    2f40:	03000093          	li	ra,48
    2f44:	021b02b3          	mul	t0,s6,ra
    2f48:	00548333          	add	t1,s1,t0
    2f4c:	02c32703          	lw	a4,44(t1)
    2f50:	f20714e3          	bnez	a4,2e78 <sgdmaChannelAlloc+0xe>
    2f54:	4585                	c.li	a1,1
    2f56:	01c28393          	addi	t2,t0,28 # 1001c <__global_pointer$+0x9cc0>
    2f5a:	01659633          	sll	a2,a1,s6
    2f5e:	020301a3          	sb	zero,35(t1)
    2f62:	00748433          	add	s0,s1,t2
    2f66:	00a66bb3          	or	s7,a2,a0
    2f6a:	00e4da03          	lhu	s4,14(s1)
    2f6e:	bfa9                	c.j	2ec8 <sgdmaChannelAlloc+0x5e>
    2f70:	4505                	c.li	a0,1
    2f72:	02a40623          	sb	a0,44(s0)
    2f76:	bfa9                	c.j	2ed0 <sgdmaChannelAlloc+0x66>

00002f78 <sgdmaStart>:
    2f78:	c955                	c.beqz	a0,302c <sgdmaStart+0xb4>
    2f7a:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
    2f7c:	00e55283          	lhu	t0,14(a0)
    2f80:	00c55683          	lhu	a3,12(a0)
    2f84:	4789                	c.li	a5,2
    2f86:	6741                	c.lui	a4,0x10
    2f88:	60f52e23          	sw	a5,1564(a0)
    2f8c:	00d2f633          	and	a2,t0,a3
    2f90:	fff70313          	addi	t1,a4,-1 # ffff <__global_pointer$+0x9ca3>
    2f94:	842a                	c.mv	s0,a0
    2f96:	06661163          	bne	a2,t1,2ff8 <sgdmaStart+0x80>
    2f9a:	4108                	c.lw	a0,0(a0)
    2f9c:	20000593          	li	a1,512
    2fa0:	3b75                	c.jal	2d5c <CPS_UncachedWrite32>
    2fa2:	60042e23          	sw	zero,1564(s0)
    2fa6:	00e45583          	lhu	a1,14(s0)
    2faa:	00c45803          	lhu	a6,12(s0)
    2fae:	02440293          	addi	t0,s0,36
    2fb2:	4781                	c.li	a5,0
    2fb4:	6e41                	c.lui	t3,0x10
    2fb6:	4e85                	c.li	t4,1
    2fb8:	5f79                	c.li	t5,-2
    2fba:	40f5d8b3          	sra	a7,a1,a5
    2fbe:	0408f95b          	bbc	a7,0,3010 <sgdmaStart+0x98>
    2fc2:	01d28a23          	sb	t4,20(t0)
    2fc6:	01c2a223          	sw	t3,4(t0)
    2fca:	01e2a023          	sw	t5,0(t0)
    2fce:	0202a023          	sw	zero,32(t0)
    2fd2:	40f85fb3          	sra	t6,a6,a5
    2fd6:	040ff45b          	bbc	t6,0,301e <sgdmaStart+0xa6>
    2fda:	31d28a23          	sb	t4,788(t0)
    2fde:	31c2a223          	sw	t3,772(t0)
    2fe2:	31e2a023          	sw	t5,768(t0)
    2fe6:	3202a023          	sw	zero,800(t0)
    2fea:	0785                	c.addi	a5,1
    2fec:	03028293          	addi	t0,t0,48
    2ff0:	bd07e55b          	bnec	a5,16,2fba <sgdmaStart+0x42>
    2ff4:	4501                	c.li	a0,0
    2ff6:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>
    2ff8:	00d2e3b3          	or	t2,t0,a3
    2ffc:	fa0395e3          	bnez	t2,2fa6 <sgdmaStart+0x2e>
    3000:	4108                	c.lw	a0,0(a0)
    3002:	10000593          	li	a1,256
    3006:	3b99                	c.jal	2d5c <CPS_UncachedWrite32>
    3008:	4505                	c.li	a0,1
    300a:	60a42e23          	sw	a0,1564(s0)
    300e:	bf61                	c.j	2fa6 <sgdmaStart+0x2e>
    3010:	00028a23          	sb	zero,20(t0)
    3014:	01c2a223          	sw	t3,4(t0)
    3018:	01c2a023          	sw	t3,0(t0)
    301c:	bf4d                	c.j	2fce <sgdmaStart+0x56>
    301e:	30028a23          	sb	zero,788(t0)
    3022:	31c2a223          	sw	t3,772(t0)
    3026:	31c2a023          	sw	t3,768(t0)
    302a:	bf75                	c.j	2fe6 <sgdmaStart+0x6e>
    302c:	4559                	c.li	a0,22
    302e:	8082                	c.jr	ra

00003030 <sgdmaDestroy>:
    3030:	c131                	c.beqz	a0,3074 <sgdmaDestroy+0x44>
    3032:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    3034:	842a                	c.mv	s0,a0
    3036:	31c50493          	addi	s1,a0,796
    303a:	61c50913          	addi	s2,a0,1564
    303e:	d104a783          	lw	a5,-752(s1)
    3042:	d0048593          	addi	a1,s1,-768
    3046:	e799                	c.bnez	a5,3054 <sgdmaDestroy+0x24>
    3048:	00442083          	lw	ra,4(s0)
    304c:	8522                	c.mv	a0,s0
    304e:	0200a303          	lw	t1,32(ra) # 1020 <mscApp+0x2a>
    3052:	9302                	c.jalr	t1
    3054:	0104a283          	lw	t0,16(s1)
    3058:	00029963          	bnez	t0,306a <sgdmaDestroy+0x3a>
    305c:	00442383          	lw	t2,4(s0)
    3060:	85a6                	c.mv	a1,s1
    3062:	0203a603          	lw	a2,32(t2)
    3066:	8522                	c.mv	a0,s0
    3068:	9602                	c.jalr	a2
    306a:	03048493          	addi	s1,s1,48
    306e:	fd2498e3          	bne	s1,s2,303e <sgdmaDestroy+0xe>
    3072:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    3074:	8082                	c.jr	ra

00003076 <sgdmaDmaReset>:
    3076:	cd01                	c.beqz	a0,308e <sgdmaDmaReset+0x18>
    3078:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
    307a:	842a                	c.mv	s0,a0
    307c:	62050023          	sb	zero,1568(a0)
    3080:	4108                	c.lw	a0,0(a0)
    3082:	31e9                	c.jal	2d4c <CPS_UncachedRead32>
    3084:	00156593          	ori	a1,a0,1
    3088:	4008                	c.lw	a0,0(s0)
    308a:	39c9                	c.jal	2d5c <CPS_UncachedWrite32>
    308c:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>
    308e:	8082                	c.jr	ra

00003090 <sgdmaInit>:
    3090:	47d9                	c.li	a5,22
    3092:	cd39                	c.beqz	a0,30f0 <sgdmaInit+0x60>
    3094:	cdb1                	c.beqz	a1,30f0 <sgdmaInit+0x60>
    3096:	ce29                	c.beqz	a2,30f0 <sgdmaInit+0x60>
    3098:	4598                	c.lw	a4,8(a1)
    309a:	cb39                	c.beqz	a4,30f0 <sgdmaInit+0x60>
    309c:	00c5a283          	lw	t0,12(a1)
    30a0:	04028863          	beqz	t0,30f0 <sgdmaInit+0x60>
    30a4:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    30a6:	8932                	c.mv	s2,a2
    30a8:	6605                	c.lui	a2,0x1
    30aa:	84ae                	c.mv	s1,a1
    30ac:	e3060613          	addi	a2,a2,-464 # e30 <mscProtocol+0x160>
    30b0:	4581                	c.li	a1,0
    30b2:	842a                	c.mv	s0,a0
    30b4:	8430                	exec.it	#19     !jal	ra,5526 <memset>
    30b6:	4488                	c.lw	a0,8(s1)
    30b8:	660d                	c.lui	a2,0x3
    30ba:	4581                	c.li	a1,0
    30bc:	8430                	exec.it	#19     !jal	ra,5526 <memset>
    30be:	00c4a303          	lw	t1,12(s1)
    30c2:	85a6                	c.mv	a1,s1
    30c4:	62642623          	sw	t1,1580(s0)
    30c8:	0084a383          	lw	t2,8(s1)
    30cc:	4641                	c.li	a2,16
    30ce:	62742423          	sw	t2,1576(s0)
    30d2:	4088                	c.lw	a0,0(s1)
    30d4:	c008                	c.sw	a0,0(s0)
    30d6:	00840513          	addi	a0,s0,8
    30da:	8c20                	exec.it	#22     !jal	ra,5350 <memcpy>
    30dc:	bbc18593          	addi	a1,gp,-1092 # 5f18 <sgdmaDrv>
    30e0:	c04c                	c.sw	a1,4(s0)
    30e2:	00092683          	lw	a3,0(s2)
    30e6:	4501                	c.li	a0,0
    30e8:	cc14                	c.sw	a3,24(s0)
    30ea:	62040023          	sb	zero,1568(s0)
    30ee:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    30f0:	853e                	c.mv	a0,a5
    30f2:	8082                	c.jr	ra

000030f4 <divRoundUp>:
    30f4:	c591                	c.beqz	a1,3100 <divRoundUp+0xc>
    30f6:	157d                	c.addi	a0,-1
    30f8:	00b502b3          	add	t0,a0,a1
    30fc:	02b2d5b3          	divu	a1,t0,a1
    3100:	852e                	c.mv	a0,a1
    3102:	8082                	c.jr	ra

00003104 <sgdmaChannelProgram>:
    3104:	4859                	c.li	a6,22
    3106:	30050e63          	beqz	a0,3422 <sgdmaChannelProgram+0x31e>
    310a:	30058c63          	beqz	a1,3422 <sgdmaChannelProgram+0x31e>
    310e:	8040                	exec.it	#32     !jal	t0,52b6 <__riscv_save_4>
    3110:	84ae                	c.mv	s1,a1
    3112:	45cc                	c.lw	a1,12(a1)
    3114:	842a                	c.mv	s0,a0
    3116:	853a                	c.mv	a0,a4
    3118:	8abe                	c.mv	s5,a5
    311a:	89ba                	c.mv	s3,a4
    311c:	8b36                	c.mv	s6,a3
    311e:	8a32                	c.mv	s4,a2
    3120:	3fd1                	c.jal	30f4 <divRoundUp>
    3122:	6b040793          	addi	a5,s0,1712
    3126:	4901                	c.li	s2,0
    3128:	0007c703          	lbu	a4,0(a5) # 3000 <sgdmaStart+0x88>
    312c:	eb0d                	c.bnez	a4,315e <sgdmaChannelProgram+0x5a>
    312e:	00950593          	addi	a1,a0,9
    3132:	0035d093          	srli	ra,a1,0x3
    3136:	4381                	c.li	t2,0
    3138:	4681                	c.li	a3,0
    313a:	08000613          	li	a2,128
    313e:	00d402b3          	add	t0,s0,a3
    3142:	6302c303          	lbu	t1,1584(t0)
    3146:	0385                	c.addi	t2,1
    3148:	00030363          	beqz	t1,314e <sgdmaChannelProgram+0x4a>
    314c:	4381                	c.li	t2,0
    314e:	00168e13          	addi	t3,a3,1 # 1001 <mscApp+0xb>
    3152:	00708e63          	beq	ra,t2,316e <sgdmaChannelProgram+0x6a>
    3156:	86f2                	c.mv	a3,t3
    3158:	fece13e3          	bne	t3,a2,313e <sgdmaChannelProgram+0x3a>
    315c:	a031                	c.j	3168 <sgdmaChannelProgram+0x64>
    315e:	0905                	c.addi	s2,1
    3160:	03c78793          	addi	a5,a5,60
    3164:	bc0962db          	bnec	s2,32,3128 <sgdmaChannelProgram+0x24>
    3168:	4731                	c.li	a4,12
    316a:	853a                	c.mv	a0,a4
    316c:	8050                	exec.it	#33     !j	52ea <__riscv_restore_4>
    316e:	03c00893          	li	a7,60
    3172:	03190eb3          	mul	t4,s2,a7
    3176:	45b1                	c.li	a1,12
    3178:	4705                	c.li	a4,1
    317a:	01d40833          	add	a6,s0,t4
    317e:	6ca82a23          	sw	a0,1748(a6)
    3182:	401e0533          	sub	a0,t3,ra
    3186:	00351793          	slli	a5,a0,0x3
    318a:	02b78333          	mul	t1,a5,a1
    318e:	6ae80823          	sb	a4,1712(a6)
    3192:	6c182823          	sw	ra,1744(a6)
    3196:	6cf82c23          	sw	a5,1752(a6)
    319a:	6cf82e23          	sw	a5,1756(a6)
    319e:	6a982a23          	sw	s1,1716(a6)
    31a2:	6c082423          	sw	zero,1736(a6)
    31a6:	62842603          	lw	a2,1576(s0)
    31aa:	6b0e8f13          	addi	t5,t4,1712
    31ae:	6e4e8e93          	addi	t4,t4,1764
    31b2:	006603b3          	add	t2,a2,t1
    31b6:	6a782c23          	sw	t2,1720(a6)
    31ba:	62c42e03          	lw	t3,1580(s0)
    31be:	01e40fb3          	add	t6,s0,t5
    31c2:	006e08b3          	add	a7,t3,t1
    31c6:	6b182e23          	sw	a7,1724(a6)
    31ca:	6c080623          	sb	zero,1740(a6)
    31ce:	01d40f33          	add	t5,s0,t4
    31d2:	0244a803          	lw	a6,36(s1)
    31d6:	02048713          	addi	a4,s1,32
    31da:	03e4a223          	sw	t5,36(s1)
    31de:	02efaa23          	sw	a4,52(t6)
    31e2:	030fac23          	sw	a6,56(t6)
    31e6:	01e82023          	sw	t5,0(a6)
    31ea:	01d48f83          	lb	t6,29(s1)
    31ee:	401400b3          	sub	ra,s0,ra
    31f2:	001f8513          	addi	a0,t6,1
    31f6:	8596                	c.mv	a1,t0
    31f8:	00d083b3          	add	t2,ra,a3
    31fc:	57fd                	c.li	a5,-1
    31fe:	00a48ea3          	sb	a0,29(s1)
    3202:	04759363          	bne	a1,t2,3248 <sgdmaChannelProgram+0x144>
    3206:	03c00293          	li	t0,60
    320a:	025906b3          	mul	a3,s2,t0
    320e:	4785                	c.li	a5,1
    3210:	00d403b3          	add	t2,s0,a3
    3214:	6d63a223          	sw	s6,1732(t2)
    3218:	6d33a023          	sw	s3,1728(t2)
    321c:	6f53a023          	sw	s5,1760(t2)
    3220:	01449223          	sh	s4,4(s1)
    3224:	6d43a583          	lw	a1,1748(t2)
    3228:	6c43a983          	lw	s3,1732(t2)
    322c:	6b43aa03          	lw	s4,1716(t2)
    3230:	1ab7ff63          	bgeu	a5,a1,33ee <sgdmaChannelProgram+0x2ea>
    3234:	00ca2603          	lw	a2,12(s4)
    3238:	88ce                	c.mv	a7,s3
    323a:	4801                	c.li	a6,0
    323c:	4fb1                	c.li	t6,12
    323e:	41100f13          	li	t5,1041
    3242:	40100e93          	li	t4,1025
    3246:	a80d                	c.j	3278 <sgdmaChannelProgram+0x174>
    3248:	62f58823          	sb	a5,1584(a1)
    324c:	15fd                	c.addi	a1,-1
    324e:	bf55                	c.j	3202 <sgdmaChannelProgram+0xfe>
    3250:	01ca4303          	lbu	t1,28(s4)
    3254:	01152023          	sw	a7,0(a0)
    3258:	6b83a503          	lw	a0,1720(t2)
    325c:	00e500b3          	add	ra,a0,a4
    3260:	00c0a223          	sw	a2,4(ra)
    3264:	6b83a283          	lw	t0,1720(t2)
    3268:	00e286b3          	add	a3,t0,a4
    326c:	16030e63          	beqz	t1,33e8 <sgdmaChannelProgram+0x2e4>
    3270:	01d6a423          	sw	t4,8(a3)
    3274:	98b2                	c.add	a7,a2
    3276:	0805                	c.addi	a6,1
    3278:	03f80733          	mul	a4,a6,t6
    327c:	6d43a083          	lw	ra,1748(t2)
    3280:	6b83a303          	lw	t1,1720(t2)
    3284:	fff08293          	addi	t0,ra,-1
    3288:	41388e33          	sub	t3,a7,s3
    328c:	00e30533          	add	a0,t1,a4
    3290:	fc5860e3          	bltu	a6,t0,3250 <sgdmaChannelProgram+0x14c>
    3294:	03c00993          	li	s3,60
    3298:	033905b3          	mul	a1,s2,s3
    329c:	46b1                	c.li	a3,12
    329e:	42500313          	li	t1,1061
    32a2:	02d803b3          	mul	t2,a6,a3
    32a6:	00b407b3          	add	a5,s0,a1
    32aa:	6b87a603          	lw	a2,1720(a5)
    32ae:	00760833          	add	a6,a2,t2
    32b2:	01182023          	sw	a7,0(a6)
    32b6:	6b87a883          	lw	a7,1720(a5)
    32ba:	6c07af03          	lw	t5,1728(a5)
    32be:	00788fb3          	add	t6,a7,t2
    32c2:	41cf0e33          	sub	t3,t5,t3
    32c6:	01cfa223          	sw	t3,4(t6)
    32ca:	6b87ae83          	lw	t4,1720(a5)
    32ce:	007e8733          	add	a4,t4,t2
    32d2:	00672423          	sw	t1,8(a4)
    32d6:	6c0786a3          	sb	zero,1741(a5)
    32da:	01ca4503          	lbu	a0,28(s4)
    32de:	c129                	c.beqz	a0,3320 <sgdmaChannelProgram+0x21c>
    32e0:	02ca4a03          	lbu	s4,44(s4)
    32e4:	00c38093          	addi	ra,t2,12
    32e8:	6b87a283          	lw	t0,1720(a5)
    32ec:	100a0563          	beqz	s4,33f6 <sgdmaChannelProgram+0x2f2>
    32f0:	6bc7a803          	lw	a6,1724(a5)
    32f4:	4605                	c.li	a2,1
    32f6:	6cc786a3          	sb	a2,1741(a5)
    32fa:	001288b3          	add	a7,t0,ra
    32fe:	0108a023          	sw	a6,0(a7)
    3302:	6b87af83          	lw	t6,1720(a5)
    3306:	6709                	c.lui	a4,0x2
    3308:	001f8f33          	add	t5,t6,ra
    330c:	000f2223          	sw	zero,4(t5)
    3310:	6b87ae03          	lw	t3,1720(a5)
    3314:	81170313          	addi	t1,a4,-2031 # 1811 <sdudcEpXDataSend+0x3>
    3318:	001e0eb3          	add	t4,t3,ra
    331c:	006ea423          	sw	t1,8(t4)
    3320:	03c00513          	li	a0,60
    3324:	02a900b3          	mul	ra,s2,a0
    3328:	0204a423          	sw	zero,40(s1)
    332c:	401c                	c.lw	a5,0(s0)
    332e:	01c78513          	addi	a0,a5,28
    3332:	001409b3          	add	s3,s0,ra
    3336:	6b49aa03          	lw	s4,1716(s3)
    333a:	007a0283          	lb	t0,7(s4)
    333e:	006a0683          	lb	a3,6(s4)
    3342:	00d2e3b3          	or	t2,t0,a3
    3346:	0ff3f593          	andi	a1,t2,255
    334a:	3c09                	c.jal	2d5c <CPS_UncachedWrite32>
    334c:	400c                	c.lw	a1,0(s0)
    334e:	02c58513          	addi	a0,a1,44
    3352:	3aed                	c.jal	2d4c <CPS_UncachedRead32>
    3354:	010a2603          	lw	a2,16(s4)
    3358:	0816535b          	beqc	a2,1,33de <sgdmaChannelProgram+0x2da>
    335c:	8624                	exec.it	#90     !lw	a6,36(s4)
    335e:	020a2883          	lw	a7,32(s4)
    3362:	07180e63          	beq	a6,a7,33de <sgdmaChannelProgram+0x2da>
    3366:	00482f83          	lw	t6,4(a6)
    336a:	4e31                	c.li	t3,12
    336c:	ff0faf03          	lw	t5,-16(t6)
    3370:	fd4fa703          	lw	a4,-44(t6)
    3374:	03ee0eb3          	mul	t4,t3,t5
    3378:	4505                	c.li	a0,1
    337a:	feaf84a3          	sb	a0,-23(t6)
    337e:	6289                	c.lui	t0,0x2
    3380:	6bc9a083          	lw	ra,1724(s3)
    3384:	81128693          	addi	a3,t0,-2031 # 1811 <sdudcEpXDataSend+0x3>
    3388:	4e01                	c.li	t3,0
    338a:	01d70333          	add	t1,a4,t4
    338e:	00132023          	sw	ra,0(t1)
    3392:	00032223          	sw	zero,4(t1)
    3396:	00d32423          	sw	a3,8(t1)
    339a:	ff0fa383          	lw	t2,-16(t6)
    339e:	00238f93          	addi	t6,t2,2
    33a2:	07fe6b63          	bltu	t3,t6,3418 <sgdmaChannelProgram+0x314>
    33a6:	401c                	c.lw	a5,0(s0)
    33a8:	02878513          	addi	a0,a5,40
    33ac:	3245                	c.jal	2d4c <CPS_UncachedRead32>
    33ae:	4265785b          	bbs	a0,6,33de <sgdmaChannelProgram+0x2da>
    33b2:	03c00593          	li	a1,60
    33b6:	02b90933          	mul	s2,s2,a1
    33ba:	4010                	c.lw	a2,0(s0)
    33bc:	02060513          	addi	a0,a2,32 # 3020 <sgdmaStart+0xa8>
    33c0:	9922                	c.add	s2,s0
    33c2:	6bc92583          	lw	a1,1724(s2)
    33c6:	3a59                	c.jal	2d5c <CPS_UncachedWrite32>
    33c8:	00042803          	lw	a6,0(s0)
    33cc:	8060                	exec.it	#48     !li	a1,64
    33ce:	02880513          	addi	a0,a6,40
    33d2:	3269                	c.jal	2d5c <CPS_UncachedWrite32>
    33d4:	8a40                	exec.it	#100     !lw	a7,0(s0)
    33d6:	8840                	exec.it	#36     !li	a1,128
    33d8:	02c88513          	addi	a0,a7,44
    33dc:	3241                	c.jal	2d5c <CPS_UncachedWrite32>
    33de:	4e8d                	c.li	t4,3
    33e0:	01d4a823          	sw	t4,16(s1)
    33e4:	4701                	c.li	a4,0
    33e6:	b351                	c.j	316a <sgdmaChannelProgram+0x66>
    33e8:	01e6a423          	sw	t5,8(a3)
    33ec:	b561                	c.j	3274 <sgdmaChannelProgram+0x170>
    33ee:	88ce                	c.mv	a7,s3
    33f0:	4e01                	c.li	t3,0
    33f2:	4801                	c.li	a6,0
    33f4:	b545                	c.j	3294 <sgdmaChannelProgram+0x190>
    33f6:	001286b3          	add	a3,t0,ra
    33fa:	0006a023          	sw	zero,0(a3)
    33fe:	6b87a383          	lw	t2,1720(a5)
    3402:	001389b3          	add	s3,t2,ra
    3406:	0009a223          	sw	zero,4(s3)
    340a:	6b87a583          	lw	a1,1720(a5)
    340e:	001587b3          	add	a5,a1,ra
    3412:	0007a423          	sw	zero,8(a5)
    3416:	b729                	c.j	3320 <sgdmaChannelProgram+0x21c>
    3418:	001e0f13          	addi	t5,t3,1 # 10001 <__global_pointer$+0x9ca5>
    341c:	3c0f2e5b          	bfoz	t3,t5,15,0
    3420:	b749                	c.j	33a2 <sgdmaChannelProgram+0x29e>
    3422:	8542                	c.mv	a0,a6
    3424:	8082                	c.jr	ra

00003426 <sgdmaFreeTrbChain>:
    3426:	4f01                	c.li	t5,0
    3428:	5198                	c.lw	a4,32(a1)
    342a:	02ef6963          	bltu	t5,a4,345c <sgdmaFreeTrbChain+0x36>
    342e:	0045a283          	lw	t0,4(a1)
    3432:	01d28783          	lb	a5,29(t0)
    3436:	fff78313          	addi	t1,a5,-1
    343a:	00628ea3          	sb	t1,29(t0)
    343e:	0345a383          	lw	t2,52(a1)
    3442:	5d88                	c.lw	a0,56(a1)
    3444:	00058023          	sb	zero,0(a1)
    3448:	00a3a223          	sw	a0,4(t2)
    344c:	5d90                	c.lw	a2,56(a1)
    344e:	59d4                	c.lw	a3,52(a1)
    3450:	c214                	c.sw	a3,0(a2)
    3452:	0205ac23          	sw	zero,56(a1)
    3456:	0205aa23          	sw	zero,52(a1)
    345a:	8082                	c.jr	ra
    345c:	0285a803          	lw	a6,40(a1)
    3460:	01e50e33          	add	t3,a0,t5
    3464:	00385893          	srli	a7,a6,0x3
    3468:	011e0eb3          	add	t4,t3,a7
    346c:	620e8823          	sb	zero,1584(t4)
    3470:	0f05                	c.addi	t5,1
    3472:	bf5d                	c.j	3428 <sgdmaFreeTrbChain+0x2>

00003474 <sgdmaChannelAbort>:
    3474:	c12d                	c.beqz	a0,34d6 <sgdmaChannelAbort+0x62>
    3476:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    3478:	84aa                	c.mv	s1,a0
    347a:	4559                	c.li	a0,22
    347c:	c5b9                	c.beqz	a1,34ca <sgdmaChannelAbort+0x56>
    347e:	842e                	c.mv	s0,a1
    3480:	00640783          	lb	a5,6(s0)
    3484:	00758583          	lb	a1,7(a1)
    3488:	4088                	c.lw	a0,0(s1)
    348a:	00f5e0b3          	or	ra,a1,a5
    348e:	0ff0f593          	andi	a1,ra,255
    3492:	0571                	c.addi	a0,28
    3494:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    3496:	0004a283          	lw	t0,0(s1)
    349a:	02040913          	addi	s2,s0,32
    349e:	02428513          	addi	a0,t0,36
    34a2:	8054                	exec.it	#41     !jal	ra,2d4c <CPS_UncachedRead32>
    34a4:	0004a303          	lw	t1,0(s1)
    34a8:	ffe57593          	andi	a1,a0,-2
    34ac:	02430513          	addi	a0,t1,36
    34b0:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    34b2:	0004a383          	lw	t2,0(s1)
    34b6:	55fd                	c.li	a1,-1
    34b8:	02c38513          	addi	a0,t2,44
    34bc:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    34be:	5010                	c.lw	a2,32(s0)
    34c0:	01261663          	bne	a2,s2,34cc <sgdmaChannelAbort+0x58>
    34c4:	4685                	c.li	a3,1
    34c6:	4501                	c.li	a0,0
    34c8:	c814                	c.sw	a3,16(s0)
    34ca:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    34cc:	fcc60593          	addi	a1,a2,-52
    34d0:	8526                	c.mv	a0,s1
    34d2:	3f91                	c.jal	3426 <sgdmaFreeTrbChain>
    34d4:	b7ed                	c.j	34be <sgdmaChannelAbort+0x4a>
    34d6:	4559                	c.li	a0,22
    34d8:	8082                	c.jr	ra

000034da <sgdmaChannelRelease>:
    34da:	47d9                	c.li	a5,22
    34dc:	c5d5                	c.beqz	a1,3588 <sgdmaChannelRelease+0xae>
    34de:	c54d                	c.beqz	a0,3588 <sgdmaChannelRelease+0xae>
    34e0:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    34e2:	842a                	c.mv	s0,a0
    34e4:	4108                	c.lw	a0,0(a0)
    34e6:	892e                	c.mv	s2,a1
    34e8:	03c50513          	addi	a0,a0,60
    34ec:	8054                	exec.it	#41     !jal	ra,2d4c <CPS_UncachedRead32>
    34ee:	00794703          	lbu	a4,7(s2)
    34f2:	00694283          	lbu	t0,6(s2)
    34f6:	c311                	c.beqz	a4,34fa <sgdmaChannelRelease+0x20>
    34f8:	02c1                	c.addi	t0,16
    34fa:	4485                	c.li	s1,1
    34fc:	005490b3          	sll	ra,s1,t0
    3500:	00042383          	lw	t2,0(s0)
    3504:	00694583          	lbu	a1,6(s2)
    3508:	fff0c313          	not	t1,ra
    350c:	00a374b3          	and	s1,t1,a0
    3510:	8dd9                	c.or	a1,a4
    3512:	01c38513          	addi	a0,t2,28
    3516:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    3518:	4010                	c.lw	a2,0(s0)
    351a:	4581                	c.li	a1,0
    351c:	02460513          	addi	a0,a2,36
    3520:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    3522:	4014                	c.lw	a3,0(s0)
    3524:	85a6                	c.mv	a1,s1
    3526:	03c68513          	addi	a0,a3,60
    352a:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    352c:	00042803          	lw	a6,0(s0)
    3530:	4581                	c.li	a1,0
    3532:	03480513          	addi	a0,a6,52
    3536:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    3538:	8a40                	exec.it	#100     !lw	a7,0(s0)
    353a:	4585                	c.li	a1,1
    353c:	02888513          	addi	a0,a7,40
    3540:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    3542:	00042e03          	lw	t3,0(s0)
    3546:	6ea1                	c.lui	t4,0x8
    3548:	08ce8593          	addi	a1,t4,140 # 808c <__global_pointer$+0x1d30>
    354c:	02ce0513          	addi	a0,t3,44
    3550:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    3552:	01092f03          	lw	t5,16(s2)
    3556:	4f89                	c.li	t6,2
    3558:	01eff463          	bgeu	t6,t5,3560 <sgdmaChannelRelease+0x86>
    355c:	01f92823          	sw	t6,16(s2)
    3560:	405c                	c.lw	a5,4(s0)
    3562:	85ca                	c.mv	a1,s2
    3564:	5798                	c.lw	a4,40(a5)
    3566:	8522                	c.mv	a0,s0
    3568:	02090493          	addi	s1,s2,32
    356c:	9702                	c.jalr	a4
    356e:	02092503          	lw	a0,32(s2)
    3572:	00951663          	bne	a0,s1,357e <sgdmaChannelRelease+0xa4>
    3576:	00092823          	sw	zero,16(s2)
    357a:	4501                	c.li	a0,0
    357c:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    357e:	fcc50593          	addi	a1,a0,-52
    3582:	8522                	c.mv	a0,s0
    3584:	354d                	c.jal	3426 <sgdmaFreeTrbChain>
    3586:	b7e5                	c.j	356e <sgdmaChannelRelease+0x94>
    3588:	853e                	c.mv	a0,a5
    358a:	8082                	c.jr	ra

0000358c <sgdmaUpdateDescBuffer.isra.8>:
    358c:	8830                	exec.it	#21     !jal	t0,52b6 <__riscv_save_4>
    358e:	41c4                	c.lw	s1,4(a1)
    3590:	892a                	c.mv	s2,a0
    3592:	02c4c783          	lbu	a5,44(s1)
    3596:	842e                	c.mv	s0,a1
    3598:	cb91                	c.beqz	a5,35ac <sgdmaUpdateDescBuffer.isra.8+0x20>
    359a:	01d5c283          	lbu	t0,29(a1)
    359e:	00028763          	beqz	t0,35ac <sgdmaUpdateDescBuffer.isra.8+0x20>
    35a2:	50d8                	c.lw	a4,36(s1)
    35a4:	0204a303          	lw	t1,32(s1)
    35a8:	0c670263          	beq	a4,t1,366c <sgdmaUpdateDescBuffer.isra.8+0xe0>
    35ac:	000206b7          	lui	a3,0x20
    35b0:	4081                	c.li	ra,0
    35b2:	4531                	c.li	a0,12
    35b4:	fff68393          	addi	t2,a3,-1 # 1ffff <__global_pointer$+0x19ca3>
    35b8:	a80d                	c.j	35ea <sgdmaUpdateDescBuffer.isra.8+0x5e>
    35ba:	03042983          	lw	s3,48(s0)
    35be:	02a086b3          	mul	a3,ra,a0
    35c2:	00098663          	beqz	s3,35ce <sgdmaUpdateDescBuffer.isra.8+0x42>
    35c6:	0c1985db          	lea.w	a1,s3,ra
    35ca:	c25d                	c.beqz	a2,3670 <sgdmaUpdateDescBuffer.isra.8+0xe4>
    35cc:	8650                	exec.it	#99     !sw	zero,0(a1)
    35ce:	00842e83          	lw	t4,8(s0)
    35d2:	4c18                	c.lw	a4,24(s0)
    35d4:	00de8f33          	add	t5,t4,a3
    35d8:	004f2f83          	lw	t6,4(t5)
    35dc:	0085                	c.addi	ra,1
    35de:	007ff7b3          	and	a5,t6,t2
    35e2:	00f702b3          	add	t0,a4,a5
    35e6:	00542c23          	sw	t0,24(s0)
    35ea:	504c                	c.lw	a1,36(s0)
    35ec:	01c44603          	lbu	a2,28(s0)
    35f0:	fcb0e5e3          	bltu	ra,a1,35ba <sgdmaUpdateDescBuffer.isra.8+0x2e>
    35f4:	c211                	c.beqz	a2,35f8 <sgdmaUpdateDescBuffer.isra.8+0x6c>
    35f6:	8c64                	exec.it	#62     !sw	zero,24(s0)
    35f8:	00092803          	lw	a6,0(s2)
    35fc:	01c80513          	addi	a0,a6,28
    3600:	8054                	exec.it	#41     !jal	ra,2d4c <CPS_UncachedRead32>
    3602:	01842883          	lw	a7,24(s0)
    3606:	89aa                	c.mv	s3,a0
    3608:	0314a423          	sw	a7,40(s1)
    360c:	01d44e03          	lbu	t3,29(s0)
    3610:	000e1a63          	bnez	t3,3624 <sgdmaUpdateDescBuffer.isra.8+0x98>
    3614:	03442e83          	lw	t4,52(s0)
    3618:	000e8663          	beqz	t4,3624 <sgdmaUpdateDescBuffer.isra.8+0x98>
    361c:	85a2                	c.mv	a1,s0
    361e:	854a                	c.mv	a0,s2
    3620:	4401                	c.li	s0,0
    3622:	3511                	c.jal	3426 <sgdmaFreeTrbChain>
    3624:	0244af83          	lw	t6,36(s1)
    3628:	02048f13          	addi	t5,s1,32
    362c:	01ef9463          	bne	t6,t5,3634 <sgdmaUpdateDescBuffer.isra.8+0xa8>
    3630:	4785                	c.li	a5,1
    3632:	c89c                	c.sw	a5,16(s1)
    3634:	01892703          	lw	a4,24(s2)
    3638:	cb01                	c.beqz	a4,3648 <sgdmaUpdateDescBuffer.isra.8+0xbc>
    363a:	0074c603          	lbu	a2,7(s1)
    363e:	0064c583          	lbu	a1,6(s1)
    3642:	62492503          	lw	a0,1572(s2)
    3646:	9702                	c.jalr	a4
    3648:	cc01                	c.beqz	s0,3660 <sgdmaUpdateDescBuffer.isra.8+0xd4>
    364a:	01d44283          	lbu	t0,29(s0)
    364e:	00028963          	beqz	t0,3660 <sgdmaUpdateDescBuffer.isra.8+0xd4>
    3652:	03442303          	lw	t1,52(s0)
    3656:	00030563          	beqz	t1,3660 <sgdmaUpdateDescBuffer.isra.8+0xd4>
    365a:	85a2                	c.mv	a1,s0
    365c:	854a                	c.mv	a0,s2
    365e:	33e1                	c.jal	3426 <sgdmaFreeTrbChain>
    3660:	00092083          	lw	ra,0(s2)
    3664:	85ce                	c.mv	a1,s3
    3666:	01c08513          	addi	a0,ra,28
    366a:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    366c:	4501                	c.li	a0,0
    366e:	8024                	exec.it	#24     !j	52ea <__riscv_restore_4>
    3670:	4410                	c.lw	a2,8(s0)
    3672:	00d60833          	add	a6,a2,a3
    3676:	00482883          	lw	a7,4(a6)
    367a:	0078fe33          	and	t3,a7,t2
    367e:	01c5a023          	sw	t3,0(a1)
    3682:	b7b1                	c.j	35ce <sgdmaUpdateDescBuffer.isra.8+0x42>

00003684 <sgdmaIsr>:
    3684:	41d012ef          	jal	t0,52a0 <__riscv_save_12>
    3688:	4104                	c.lw	s1,0(a0)
    368a:	8a2a                	c.mv	s4,a0
    368c:	04048513          	addi	a0,s1,64
    3690:	8054                	exec.it	#41     !jal	ra,2d4c <CPS_UncachedRead32>
    3692:	cd5d                	c.beqz	a0,3750 <sgdmaIsr+0xcc>
    3694:	8b2a                	c.mv	s6,a0
    3696:	4981                	c.li	s3,0
    3698:	4c3d                	c.li	s8,15
    369a:	01c48c93          	addi	s9,s1,28
    369e:	02c48913          	addi	s2,s1,44
    36a2:	03000d13          	li	s10,48
    36a6:	4685                	c.li	a3,1
    36a8:	013697b3          	sll	a5,a3,s3
    36ac:	0167f0b3          	and	ra,a5,s6
    36b0:	0e008163          	beqz	ra,3792 <sgdmaIsr+0x10e>
    36b4:	0ff9f413          	andi	s0,s3,255
    36b8:	4d81                	c.li	s11,0
    36ba:	013c5763          	bge	s8,s3,36c8 <sgdmaIsr+0x44>
    36be:	1441                	c.addi	s0,-16
    36c0:	0ff47413          	andi	s0,s0,255
    36c4:	08000d93          	li	s11,128
    36c8:	01b465b3          	or	a1,s0,s11
    36cc:	8566                	c.mv	a0,s9
    36ce:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    36d0:	854a                	c.mv	a0,s2
    36d2:	8054                	exec.it	#41     !jal	ra,2d4c <CPS_UncachedRead32>
    36d4:	03a402b3          	mul	t0,s0,s10
    36d8:	8aaa                	c.mv	s5,a0
    36da:	060d8d63          	beqz	s11,3754 <sgdmaIsr+0xd0>
    36de:	31c28313          	addi	t1,t0,796
    36e2:	080afd93          	andi	s11,s5,128
    36e6:	006a0433          	add	s0,s4,t1
    36ea:	000d8563          	beqz	s11,36f4 <sgdmaIsr+0x70>
    36ee:	8840                	exec.it	#36     !li	a1,128
    36f0:	854a                	c.mv	a0,s2
    36f2:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    36f4:	00caf393          	andi	t2,s5,12
    36f8:	00039663          	bnez	t2,3704 <sgdmaIsr+0x80>
    36fc:	4c08                	c.lw	a0,24(s0)
    36fe:	00c57593          	andi	a1,a0,12
    3702:	c1bd                	c.beqz	a1,3768 <sgdmaIsr+0xe4>
    3704:	5010                	c.lw	a2,32(s0)
    3706:	45b1                	c.li	a1,12
    3708:	854a                	c.mv	a0,s2
    370a:	fcc60b93          	addi	s7,a2,-52
    370e:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    3710:	040d9863          	bnez	s11,3760 <sgdmaIsr+0xdc>
    3714:	01c44703          	lbu	a4,28(s0)
    3718:	c721                	c.beqz	a4,3760 <sgdmaIsr+0xdc>
    371a:	01dbc803          	lbu	a6,29(s7)
    371e:	04081163          	bnez	a6,3760 <sgdmaIsr+0xdc>
    3722:	620a4e03          	lbu	t3,1568(s4)
    3726:	020e1a63          	bnez	t3,375a <sgdmaIsr+0xd6>
    372a:	00744e83          	lbu	t4,7(s0)
    372e:	020e9663          	bnez	t4,375a <sgdmaIsr+0xd6>
    3732:	8840                	exec.it	#36     !li	a1,128
    3734:	854a                	c.mv	a0,s2
    3736:	02448493          	addi	s1,s1,36
    373a:	8c64                	exec.it	#62     !sw	zero,24(s0)
    373c:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    373e:	8526                	c.mv	a0,s1
    3740:	8054                	exec.it	#41     !jal	ra,2d4c <CPS_UncachedRead32>
    3742:	ffe57593          	andi	a1,a0,-2
    3746:	8526                	c.mv	a0,s1
    3748:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    374a:	85de                	c.mv	a1,s7
    374c:	8552                	c.mv	a0,s4
    374e:	3d3d                	c.jal	358c <sgdmaUpdateDescBuffer.isra.8>
    3750:	38d0106f          	j	52dc <__riscv_restore_12>
    3754:	01c28313          	addi	t1,t0,28
    3758:	b769                	c.j	36e2 <sgdmaIsr+0x5e>
    375a:	01542c23          	sw	s5,24(s0)
    375e:	bfcd                	c.j	3750 <sgdmaIsr+0xcc>
    3760:	85de                	c.mv	a1,s7
    3762:	8552                	c.mv	a0,s4
    3764:	8c64                	exec.it	#62     !sw	zero,24(s0)
    3766:	351d                	c.jal	358c <sgdmaUpdateDescBuffer.isra.8>
    3768:	00eaf55b          	bbc	s5,14,3772 <sgdmaIsr+0xee>
    376c:	6591                	c.lui	a1,0x4
    376e:	854a                	c.mv	a0,s2
    3770:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    3772:	004af55b          	bbc	s5,4,377c <sgdmaIsr+0xf8>
    3776:	45c1                	c.li	a1,16
    3778:	854a                	c.mv	a0,s2
    377a:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    377c:	00fafb5b          	bbc	s5,15,3792 <sgdmaIsr+0x10e>
    3780:	65a1                	c.lui	a1,0x8
    3782:	854a                	c.mv	a0,s2
    3784:	8810                	exec.it	#5     !jal	ra,2d5c <CPS_UncachedWrite32>
    3786:	02042883          	lw	a7,32(s0)
    378a:	4b85                	c.li	s7,1
    378c:	ff788423          	sb	s7,-24(a7)
    3790:	bf95                	c.j	3704 <sgdmaIsr+0x80>
    3792:	0985                	c.addi	s3,1
    3794:	b009e9db          	bnec	s3,32,36a6 <sgdmaIsr+0x22>
    3798:	bf65                	c.j	3750 <sgdmaIsr+0xcc>

0000379a <CUSBDMA_GetInstance>:
    379a:	bbc18513          	addi	a0,gp,-1092 # 5f18 <sgdmaDrv>
    379e:	8082                	c.jr	ra

000037a0 <do_printf>:
    37a0:	301012ef          	jal	t0,52a0 <__riscv_save_12>
    37a4:	1101                	c.addi	sp,-32
    37a6:	89aa                	c.mv	s3,a0
    37a8:	8a2e                	c.mv	s4,a1
    37aa:	8ab2                	c.mv	s5,a2
    37ac:	4d81                	c.li	s11,0
    37ae:	4481                	c.li	s1,0
    37b0:	4401                	c.li	s0,0
    37b2:	4901                	c.li	s2,0
    37b4:	06e00b93          	li	s7,110
    37b8:	4b25                	c.li	s6,9
    37ba:	07300c13          	li	s8,115
    37be:	c636                	c.swsp	a3,12(sp)
    37c0:	0009c503          	lbu	a0,0(s3)
    37c4:	e509                	c.bnez	a0,37ce <do_printf+0x2e>
    37c6:	8526                	c.mv	a0,s1
    37c8:	6105                	c.addi16sp	sp,32
    37ca:	3130106f          	j	52dc <__riscv_restore_12>
    37ce:	0429585b          	beqc	s2,2,381e <do_printf+0x7e>
    37d2:	0639575b          	beqc	s2,3,3840 <do_printf+0xa0>
    37d6:	0019555b          	beqc	s2,1,37e0 <do_printf+0x40>
    37da:	00556fdb          	bnec	a0,37,37f8 <do_printf+0x58>
    37de:	0985                	c.addi	s3,1
    37e0:	0009c783          	lbu	a5,0(s3)
    37e4:	0057efdb          	bnec	a5,37,3802 <do_printf+0x62>
    37e8:	006c                	c.addi4spn	a1,sp,12
    37ea:	02500513          	li	a0,37
    37ee:	0485                	c.addi	s1,1
    37f0:	9a82                	c.jalr	s5
    37f2:	4d81                	c.li	s11,0
    37f4:	4401                	c.li	s0,0
    37f6:	a021                	c.j	37fe <do_printf+0x5e>
    37f8:	006c                	c.addi4spn	a1,sp,12
    37fa:	0485                	c.addi	s1,1
    37fc:	9a82                	c.jalr	s5
    37fe:	4901                	c.li	s2,0
    3800:	a801                	c.j	3810 <do_printf+0x70>
    3802:	00d7e9db          	bnec	a5,45,3814 <do_printf+0x74>
    3806:	fe04765b          	bbs	s0,0,37f2 <do_printf+0x52>
    380a:	00146413          	ori	s0,s0,1
    380e:	4905                	c.li	s2,1
    3810:	0985                	c.addi	s3,1
    3812:	b77d                	c.j	37c0 <do_printf+0x20>
    3814:	0107e5db          	bnec	a5,48,381e <do_printf+0x7e>
    3818:	0985                	c.addi	s3,1
    381a:	04046413          	ori	s0,s0,64
    381e:	0009c703          	lbu	a4,0(s3)
    3822:	fd070293          	addi	t0,a4,-48
    3826:	0ff2f313          	andi	t1,t0,255
    382a:	006b6b63          	bltu	s6,t1,3840 <do_printf+0xa0>
    382e:	4f29                	c.li	t5,10
    3830:	03ed8fb3          	mul	t6,s11,t5
    3834:	4909                	c.li	s2,2
    3836:	fd0f8793          	addi	a5,t6,-48
    383a:	00f70db3          	add	s11,a4,a5
    383e:	bfc9                	c.j	3810 <do_printf+0x70>
    3840:	0009c383          	lbu	t2,0(s3)
    3844:	4063e65b          	bnec	t2,70,3850 <do_printf+0xb0>
    3848:	08046413          	ori	s0,s0,128
    384c:	490d                	c.li	s2,3
    384e:	b7c9                	c.j	3810 <do_printf+0x70>
    3850:	490d                	c.li	s2,3
    3852:	fae3df5b          	beqc	t2,78,3810 <do_printf+0x70>
    3856:	40c3e5db          	bnec	t2,108,3860 <do_printf+0xc0>
    385a:	00846413          	ori	s0,s0,8
    385e:	bf4d                	c.j	3810 <do_printf+0x70>
    3860:	4083e5db          	bnec	t2,104,386a <do_printf+0xca>
    3864:	01046413          	ori	s0,s0,16
    3868:	b7d5                	c.j	384c <do_printf+0xac>
    386a:	00010fa3          	sb	zero,31(sp)
    386e:	40e3dedb          	beqc	t2,110,388a <do_printf+0xea>
    3872:	087be563          	bltu	s7,t2,38fc <do_printf+0x15c>
    3876:	06300513          	li	a0,99
    387a:	4a33d9db          	beqc	t2,99,392c <do_printf+0x18c>
    387e:	00756863          	bltu	a0,t2,388e <do_printf+0xee>
    3882:	00246413          	ori	s0,s0,2
    3886:	f783e65b          	bnec	t2,88,37f2 <do_printf+0x52>
    388a:	46c1                	c.li	a3,16
    388c:	a801                	c.j	389c <do_printf+0xfc>
    388e:	4043d4db          	beqc	t2,100,3896 <do_printf+0xf6>
    3892:	f693e0db          	bnec	t2,105,37f2 <do_printf+0x52>
    3896:	00446413          	ori	s0,s0,4
    389a:	46a9                	c.li	a3,10
    389c:	000a2c83          	lw	s9,0(s4)
    38a0:	0a11                	c.addi	s4,4
    38a2:	0024785b          	bbc	s0,2,38b2 <do_printf+0x112>
    38a6:	000cd663          	bgez	s9,38b2 <do_printf+0x112>
    38aa:	02046413          	ori	s0,s0,32
    38ae:	41900cb3          	neg	s9,s9
    38b2:	01f10913          	addi	s2,sp,31
    38b6:	00247093          	andi	ra,s0,2
    38ba:	02dcf633          	remu	a2,s9,a3
    38be:	197d                	c.addi	s2,-1
    38c0:	0ff67813          	andi	a6,a2,255
    38c4:	04cb6c63          	bltu	s6,a2,391c <do_printf+0x17c>
    38c8:	03080893          	addi	a7,a6,48
    38cc:	02dcdcb3          	divu	s9,s9,a3
    38d0:	01190023          	sb	a7,0(s2)
    38d4:	fe0c93e3          	bnez	s9,38ba <do_printf+0x11a>
    38d8:	854a                	c.mv	a0,s2
    38da:	513010ef          	jal	ra,55ec <strlen>
    38de:	8d2a                	c.mv	s10,a0
    38e0:	0054745b          	bbc	s0,5,38e8 <do_printf+0x148>
    38e4:	00150d13          	addi	s10,a0,1
    38e8:	06047e13          	andi	t3,s0,96
    38ec:	440e6adb          	bnec	t3,96,3940 <do_printf+0x1a0>
    38f0:	006c                	c.addi4spn	a1,sp,12
    38f2:	02d00513          	li	a0,45
    38f6:	9a82                	c.jalr	s5
    38f8:	0485                	c.addi	s1,1
    38fa:	a099                	c.j	3940 <do_printf+0x1a0>
    38fc:	4933dcdb          	beqc	t2,115,3994 <do_printf+0x1f4>
    3900:	007c6863          	bltu	s8,t2,3910 <do_printf+0x170>
    3904:	46a1                	c.li	a3,8
    3906:	f8f3dbdb          	beqc	t2,111,389c <do_printf+0xfc>
    390a:	f903d0db          	beqc	t2,112,388a <do_printf+0xea>
    390e:	b5d5                	c.j	37f2 <do_printf+0x52>
    3910:	46a9                	c.li	a3,10
    3912:	f953d5db          	beqc	t2,117,389c <do_printf+0xfc>
    3916:	f783dadb          	beqc	t2,120,388a <do_printf+0xea>
    391a:	bde1                	c.j	37f2 <do_printf+0x52>
    391c:	00008563          	beqz	ra,3926 <do_printf+0x186>
    3920:	03780893          	addi	a7,a6,55
    3924:	b765                	c.j	38cc <do_printf+0x12c>
    3926:	05780893          	addi	a7,a6,87
    392a:	b74d                	c.j	38cc <do_printf+0x12c>
    392c:	000a2583          	lw	a1,0(s4)
    3930:	fbf47413          	andi	s0,s0,-65
    3934:	0a11                	c.addi	s4,4
    3936:	01e10913          	addi	s2,sp,30
    393a:	4d05                	c.li	s10,1
    393c:	00b10f23          	sb	a1,30(sp)
    3940:	8cee                	c.mv	s9,s11
    3942:	40047e5b          	bbs	s0,0,395e <do_printf+0x1be>
    3946:	02000e93          	li	t4,32
    394a:	0064745b          	bbc	s0,6,3952 <do_printf+0x1b2>
    394e:	03000e93          	li	t4,48
    3952:	8cee                	c.mv	s9,s11
    3954:	9da6                	c.add	s11,s1
    3956:	419d84b3          	sub	s1,s11,s9
    395a:	059d6363          	bltu	s10,s9,39a0 <do_printf+0x200>
    395e:	06047413          	andi	s0,s0,96
    3962:	000467db          	bnec	s0,32,3970 <do_printf+0x1d0>
    3966:	006c                	c.addi4spn	a1,sp,12
    3968:	02d00513          	li	a0,45
    396c:	0485                	c.addi	s1,1
    396e:	9a82                	c.jalr	s5
    3970:	41248db3          	sub	s11,s1,s2
    3974:	00094503          	lbu	a0,0(s2)
    3978:	01b904b3          	add	s1,s2,s11
    397c:	e90d                	c.bnez	a0,39ae <do_printf+0x20e>
    397e:	e7aceae3          	bltu	s9,s10,37f2 <do_printf+0x52>
    3982:	41ac8cb3          	sub	s9,s9,s10
    3986:	8966                	c.mv	s2,s9
    3988:	02091763          	bnez	s2,39b6 <do_printf+0x216>
    398c:	94e6                	c.add	s1,s9
    398e:	4d81                	c.li	s11,0
    3990:	4401                	c.li	s0,0
    3992:	bdbd                	c.j	3810 <do_printf+0x70>
    3994:	000a2903          	lw	s2,0(s4)
    3998:	fbf47413          	andi	s0,s0,-65
    399c:	0a11                	c.addi	s4,4
    399e:	bf2d                	c.j	38d8 <do_printf+0x138>
    39a0:	8576                	c.mv	a0,t4
    39a2:	006c                	c.addi4spn	a1,sp,12
    39a4:	c476                	c.swsp	t4,8(sp)
    39a6:	9a82                	c.jalr	s5
    39a8:	1cfd                	c.addi	s9,-1
    39aa:	4ea2                	c.lwsp	t4,8(sp)
    39ac:	b76d                	c.j	3956 <do_printf+0x1b6>
    39ae:	006c                	c.addi4spn	a1,sp,12
    39b0:	0905                	c.addi	s2,1
    39b2:	9a82                	c.jalr	s5
    39b4:	b7c1                	c.j	3974 <do_printf+0x1d4>
    39b6:	006c                	c.addi4spn	a1,sp,12
    39b8:	02000513          	li	a0,32
    39bc:	9a82                	c.jalr	s5
    39be:	197d                	c.addi	s2,-1
    39c0:	b7e1                	c.j	3988 <do_printf+0x1e8>

000039c2 <printchar>:
    39c2:	cd01                	c.beqz	a0,39da <printchar+0x18>
    39c4:	00052383          	lw	t2,0(a0)
    39c8:	832a                	c.mv	t1,a0
    39ca:	00b38023          	sb	a1,0(t2)
    39ce:	4108                	c.lw	a0,0(a0)
    39d0:	00150593          	addi	a1,a0,1
    39d4:	00b32023          	sw	a1,0(t1)
    39d8:	8082                	c.jr	ra
    39da:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
    39dc:	47fd                	c.li	a5,31
    39de:	842e                	c.mv	s0,a1
    39e0:	00b7cb63          	blt	a5,a1,39f6 <printchar+0x34>
    39e4:	00d5d95b          	beqc	a1,13,39f6 <printchar+0x34>
    39e8:	00a5de5b          	beqc	a1,10,3a04 <printchar+0x42>
    39ec:	ff858293          	addi	t0,a1,-8 # 7ff8 <__global_pointer$+0x1c9c>
    39f0:	4705                	c.li	a4,1
    39f2:	00576863          	bltu	a4,t0,3a02 <printchar+0x40>
    39f6:	85a2                	c.mv	a1,s0
    39f8:	4501                	c.li	a0,0
    39fa:	927fc0ef          	jal	ra,320 <uart_putc>
    39fe:	c88184a3          	sb	s0,-887(gp) # 5fe5 <prev.1823>
    3a02:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>
    3a04:	c891c083          	lbu	ra,-887(gp) # 5fe5 <prev.1823>
    3a08:	bed0d75b          	beqc	ra,13,39f6 <printchar+0x34>
    3a0c:	45b5                	c.li	a1,13
    3a0e:	913fc0ef          	jal	ra,320 <uart_putc>
    3a12:	b7d5                	c.j	39f6 <printchar+0x34>

00003a14 <prints>:
    3a14:	8820                	exec.it	#20     !jal	t0,52b6 <__riscv_save_4>
    3a16:	8a2a                	c.mv	s4,a0
    3a18:	84ae                	c.mv	s1,a1
    3a1a:	8432                	c.mv	s0,a2
    3a1c:	02000993          	li	s3,32
    3a20:	02c05363          	blez	a2,3a46 <prints+0x32>
    3a24:	87ae                	c.mv	a5,a1
    3a26:	a011                	c.j	3a2a <prints+0x16>
    3a28:	0785                	c.addi	a5,1
    3a2a:	0007c603          	lbu	a2,0(a5)
    3a2e:	40978733          	sub	a4,a5,s1
    3a32:	fa7d                	c.bnez	a2,3a28 <prints+0x14>
    3a34:	00875e63          	bge	a4,s0,3a50 <prints+0x3c>
    3a38:	8c19                	c.sub	s0,a4
    3a3a:	02000993          	li	s3,32
    3a3e:	0016f45b          	bbc	a3,1,3a46 <prints+0x32>
    3a42:	03000993          	li	s3,48
    3a46:	4281                	c.li	t0,0
    3a48:	4206f25b          	bbs	a3,0,3a6c <prints+0x58>
    3a4c:	8922                	c.mv	s2,s0
    3a4e:	a039                	c.j	3a5c <prints+0x48>
    3a50:	4401                	c.li	s0,0
    3a52:	b7e5                	c.j	3a3a <prints+0x26>
    3a54:	85ce                	c.mv	a1,s3
    3a56:	8552                	c.mv	a0,s4
    3a58:	197d                	c.addi	s2,-1
    3a5a:	37a5                	c.jal	39c2 <printchar>
    3a5c:	ff204ce3          	bgtz	s2,3a54 <prints+0x40>
    3a60:	82a2                	c.mv	t0,s0
    3a62:	00045363          	bgez	s0,3a68 <prints+0x54>
    3a66:	4281                	c.li	t0,0
    3a68:	40540433          	sub	s0,s0,t0
    3a6c:	40928933          	sub	s2,t0,s1
    3a70:	a021                	c.j	3a78 <prints+0x64>
    3a72:	8552                	c.mv	a0,s4
    3a74:	0485                	c.addi	s1,1
    3a76:	37b1                	c.jal	39c2 <printchar>
    3a78:	0004c583          	lbu	a1,0(s1)
    3a7c:	01248ab3          	add	s5,s1,s2
    3a80:	f9ed                	c.bnez	a1,3a72 <prints+0x5e>
    3a82:	84a2                	c.mv	s1,s0
    3a84:	00904863          	bgtz	s1,3a94 <prints+0x80>
    3a88:	00045363          	bgez	s0,3a8e <prints+0x7a>
    3a8c:	4401                	c.li	s0,0
    3a8e:	01540533          	add	a0,s0,s5
    3a92:	8c34                	exec.it	#31     !j	52ea <__riscv_restore_4>
    3a94:	85ce                	c.mv	a1,s3
    3a96:	8552                	c.mv	a0,s4
    3a98:	372d                	c.jal	39c2 <printchar>
    3a9a:	14fd                	c.addi	s1,-1
    3a9c:	b7e5                	c.j	3a84 <prints+0x70>

00003a9e <printi>:
    3a9e:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    3aa0:	8436                	c.mv	s0,a3
    3aa2:	1101                	c.addi	sp,-32
    3aa4:	86be                	c.mv	a3,a5
    3aa6:	e991                	c.bnez	a1,3aba <printi+0x1c>
    3aa8:	03000313          	li	t1,48
    3aac:	863a                	c.mv	a2,a4
    3aae:	084c                	c.addi4spn	a1,sp,20
    3ab0:	00611a23          	sh	t1,20(sp)
    3ab4:	3785                	c.jal	3a14 <prints>
    3ab6:	6105                	c.addi16sp	sp,32
    3ab8:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    3aba:	892a                	c.mv	s2,a0
    3abc:	c419                	c.beqz	s0,3aca <printi+0x2c>
    3abe:	04a6615b          	bnec	a2,10,3b00 <printi+0x62>
    3ac2:	0205df63          	bgez	a1,3b00 <printi+0x62>
    3ac6:	40b005b3          	neg	a1,a1
    3aca:	01f10493          	addi	s1,sp,31
    3ace:	40a5                	c.li	ra,9
    3ad0:	fc680813          	addi	a6,a6,-58
    3ad4:	00010fa3          	sb	zero,31(sp)
    3ad8:	e595                	c.bnez	a1,3b04 <printi+0x66>
    3ada:	cc09                	c.beqz	s0,3af4 <printi+0x56>
    3adc:	c329                	c.beqz	a4,3b1e <printi+0x80>
    3ade:	0416f05b          	bbc	a3,1,3b1e <printi+0x80>
    3ae2:	02d00593          	li	a1,45
    3ae6:	854a                	c.mv	a0,s2
    3ae8:	c636                	c.swsp	a3,12(sp)
    3aea:	c43a                	c.swsp	a4,8(sp)
    3aec:	3dd9                	c.jal	39c2 <printchar>
    3aee:	4722                	c.lwsp	a4,8(sp)
    3af0:	46b2                	c.lwsp	a3,12(sp)
    3af2:	177d                	c.addi	a4,-1
    3af4:	863a                	c.mv	a2,a4
    3af6:	85a6                	c.mv	a1,s1
    3af8:	854a                	c.mv	a0,s2
    3afa:	3f29                	c.jal	3a14 <prints>
    3afc:	9522                	c.add	a0,s0
    3afe:	bf65                	c.j	3ab6 <printi+0x18>
    3b00:	4401                	c.li	s0,0
    3b02:	b7e1                	c.j	3aca <printi+0x2c>
    3b04:	02c5f533          	remu	a0,a1,a2
    3b08:	00a0f363          	bgeu	ra,a0,3b0e <printi+0x70>
    3b0c:	9542                	c.add	a0,a6
    3b0e:	14fd                	c.addi	s1,-1
    3b10:	03050293          	addi	t0,a0,48
    3b14:	00548023          	sb	t0,0(s1)
    3b18:	02c5d5b3          	divu	a1,a1,a2
    3b1c:	bf75                	c.j	3ad8 <printi+0x3a>
    3b1e:	02d00793          	li	a5,45
    3b22:	fef48fa3          	sb	a5,-1(s1)
    3b26:	4401                	c.li	s0,0
    3b28:	14fd                	c.addi	s1,-1
    3b2a:	b7e9                	c.j	3af4 <printi+0x56>

00003b2c <print>:
    3b2c:	77c012ef          	jal	t0,52a8 <__riscv_save_10>
    3b30:	1141                	c.addi	sp,-16
    3b32:	89aa                	c.mv	s3,a0
    3b34:	892e                	c.mv	s2,a1
    3b36:	8432                	c.mv	s0,a2
    3b38:	4481                	c.li	s1,0
    3b3a:	4a25                	c.li	s4,9
    3b3c:	4aa9                	c.li	s5,10
    3b3e:	00002b17          	auipc	s6,0x2
    3b42:	f0eb0b13          	addi	s6,s6,-242 # 5a4c <_ITB_BASE_+0x390>
    3b46:	00094783          	lbu	a5,0(s2)
    3b4a:	eb99                	c.bnez	a5,3b60 <print+0x34>
    3b4c:	00098663          	beqz	s3,3b58 <print+0x2c>
    3b50:	0009a883          	lw	a7,0(s3)
    3b54:	00088023          	sb	zero,0(a7)
    3b58:	8526                	c.mv	a0,s1
    3b5a:	0141                	c.addi	sp,16
    3b5c:	7840106f          	j	52e0 <__riscv_restore_10>
    3b60:	0c57e5db          	bnec	a5,37,3c2a <print+0xfe>
    3b64:	00194683          	lbu	a3,1(s2)
    3b68:	d2f5                	c.beqz	a3,3b4c <print+0x20>
    3b6a:	00190713          	addi	a4,s2,1
    3b6e:	0a56dddb          	beqc	a3,37,3c28 <print+0xfc>
    3b72:	4781                	c.li	a5,0
    3b74:	00d6e5db          	bnec	a3,45,3b7e <print+0x52>
    3b78:	00290713          	addi	a4,s2,2
    3b7c:	4785                	c.li	a5,1
    3b7e:	893a                	c.mv	s2,a4
    3b80:	00094303          	lbu	t1,0(s2)
    3b84:	00190293          	addi	t0,s2,1
    3b88:	030359db          	beqc	t1,48,3bba <print+0x8e>
    3b8c:	4701                	c.li	a4,0
    3b8e:	00094383          	lbu	t2,0(s2)
    3b92:	00190513          	addi	a0,s2,1
    3b96:	fd038613          	addi	a2,t2,-48
    3b9a:	0ff67593          	andi	a1,a2,255
    3b9e:	02ba7263          	bgeu	s4,a1,3bc2 <print+0x96>
    3ba2:	4333e6db          	bnec	t2,115,3bce <print+0xa2>
    3ba6:	8850                	exec.it	#37     !addi	s7,s0,4
    3ba8:	4000                	c.lw	s0,0(s0)
    3baa:	85da                	c.mv	a1,s6
    3bac:	c011                	c.beqz	s0,3bb0 <print+0x84>
    3bae:	85a2                	c.mv	a1,s0
    3bb0:	86be                	c.mv	a3,a5
    3bb2:	863a                	c.mv	a2,a4
    3bb4:	854e                	c.mv	a0,s3
    3bb6:	3db9                	c.jal	3a14 <prints>
    3bb8:	a025                	c.j	3be0 <print+0xb4>
    3bba:	0027e793          	ori	a5,a5,2
    3bbe:	8916                	c.mv	s2,t0
    3bc0:	b7c1                	c.j	3b80 <print+0x54>
    3bc2:	03570833          	mul	a6,a4,s5
    3bc6:	892a                	c.mv	s2,a0
    3bc8:	01060733          	add	a4,a2,a6
    3bcc:	b7c9                	c.j	3b8e <print+0x62>
    3bce:	4043eddb          	bnec	t2,100,3be8 <print+0xbc>
    3bd2:	8850                	exec.it	#37     !addi	s7,s0,4
    3bd4:	8a20                	exec.it	#84     !li	a6,97
    3bd6:	4685                	c.li	a3,1
    3bd8:	4629                	c.li	a2,10
    3bda:	400c                	c.lw	a1,0(s0)
    3bdc:	854e                	c.mv	a0,s3
    3bde:	35c1                	c.jal	3a9e <printi>
    3be0:	94aa                	c.add	s1,a0
    3be2:	845e                	c.mv	s0,s7
    3be4:	0905                	c.addi	s2,1
    3be6:	b785                	c.j	3b46 <print+0x1a>
    3be8:	4183e7db          	bnec	t2,120,3bf6 <print+0xca>
    3bec:	8850                	exec.it	#37     !addi	s7,s0,4
    3bee:	8a20                	exec.it	#84     !li	a6,97
    3bf0:	4681                	c.li	a3,0
    3bf2:	4641                	c.li	a2,16
    3bf4:	b7dd                	c.j	3bda <print+0xae>
    3bf6:	4183e65b          	bnec	t2,88,3c02 <print+0xd6>
    3bfa:	8850                	exec.it	#37     !addi	s7,s0,4
    3bfc:	04100813          	li	a6,65
    3c00:	bfc5                	c.j	3bf0 <print+0xc4>
    3c02:	4153e6db          	bnec	t2,117,3c0e <print+0xe2>
    3c06:	8850                	exec.it	#37     !addi	s7,s0,4
    3c08:	8a20                	exec.it	#84     !li	a6,97
    3c0a:	4681                	c.li	a3,0
    3c0c:	b7f1                	c.j	3bd8 <print+0xac>
    3c0e:	fc33ebdb          	bnec	t2,99,3be4 <print+0xb8>
    3c12:	00042083          	lw	ra,0(s0)
    3c16:	8850                	exec.it	#37     !addi	s7,s0,4
    3c18:	00110623          	sb	ra,12(sp)
    3c1c:	000106a3          	sb	zero,13(sp)
    3c20:	86be                	c.mv	a3,a5
    3c22:	863a                	c.mv	a2,a4
    3c24:	006c                	c.addi4spn	a1,sp,12
    3c26:	b779                	c.j	3bb4 <print+0x88>
    3c28:	893a                	c.mv	s2,a4
    3c2a:	00094583          	lbu	a1,0(s2)
    3c2e:	854e                	c.mv	a0,s3
    3c30:	3b49                	c.jal	39c2 <printchar>
    3c32:	0485                	c.addi	s1,1
    3c34:	bf45                	c.j	3be4 <print+0xb8>

00003c36 <printf>:
    3c36:	7139                	c.addi16sp	sp,-64
    3c38:	d22e                	c.swsp	a1,36(sp)
    3c3a:	d432                	c.swsp	a2,40(sp)
    3c3c:	85aa                	c.mv	a1,a0
    3c3e:	1050                	c.addi4spn	a2,sp,36
    3c40:	4501                	c.li	a0,0
    3c42:	ce06                	c.swsp	ra,28(sp)
    3c44:	d636                	c.swsp	a3,44(sp)
    3c46:	d83a                	c.swsp	a4,48(sp)
    3c48:	da3e                	c.swsp	a5,52(sp)
    3c4a:	dc42                	c.swsp	a6,56(sp)
    3c4c:	de46                	c.swsp	a7,60(sp)
    3c4e:	c632                	c.swsp	a2,12(sp)
    3c50:	3df1                	c.jal	3b2c <print>
    3c52:	40f2                	c.lwsp	ra,28(sp)
    3c54:	6121                	c.addi16sp	sp,64
    3c56:	8082                	c.jr	ra

00003c58 <vListInitialise>:
    3c58:	00850793          	addi	a5,a0,8
    3c5c:	577d                	c.li	a4,-1
    3c5e:	c15c                	c.sw	a5,4(a0)
    3c60:	c518                	c.sw	a4,8(a0)
    3c62:	c55c                	c.sw	a5,12(a0)
    3c64:	c91c                	c.sw	a5,16(a0)
    3c66:	00052023          	sw	zero,0(a0)
    3c6a:	8082                	c.jr	ra

00003c6c <vListInitialiseItem>:
    3c6c:	00052823          	sw	zero,16(a0)
    3c70:	8082                	c.jr	ra

00003c72 <vListInsertEnd>:
    3c72:	415c                	c.lw	a5,4(a0)
    3c74:	c1dc                	c.sw	a5,4(a1)
    3c76:	4798                	c.lw	a4,8(a5)
    3c78:	c598                	c.sw	a4,8(a1)
    3c7a:	0087a283          	lw	t0,8(a5)
    3c7e:	00b2a223          	sw	a1,4(t0)
    3c82:	c78c                	c.sw	a1,8(a5)
    3c84:	c988                	c.sw	a0,16(a1)
    3c86:	00052303          	lw	t1,0(a0)
    3c8a:	00130393          	addi	t2,t1,1
    3c8e:	00752023          	sw	t2,0(a0)
    3c92:	8082                	c.jr	ra

00003c94 <vListInsert>:
    3c94:	4198                	c.lw	a4,0(a1)
    3c96:	56fd                	c.li	a3,-1
    3c98:	00850793          	addi	a5,a0,8
    3c9c:	02d71363          	bne	a4,a3,3cc2 <vListInsert+0x2e>
    3ca0:	491c                	c.lw	a5,16(a0)
    3ca2:	0047a303          	lw	t1,4(a5)
    3ca6:	0065a223          	sw	t1,4(a1)
    3caa:	00b32423          	sw	a1,8(t1)
    3cae:	c59c                	c.sw	a5,8(a1)
    3cb0:	c3cc                	c.sw	a1,4(a5)
    3cb2:	c988                	c.sw	a0,16(a1)
    3cb4:	00052383          	lw	t2,0(a0)
    3cb8:	00138593          	addi	a1,t2,1
    3cbc:	c10c                	c.sw	a1,0(a0)
    3cbe:	8082                	c.jr	ra
    3cc0:	8796                	c.mv	a5,t0
    3cc2:	0047a283          	lw	t0,4(a5)
    3cc6:	0002a603          	lw	a2,0(t0)
    3cca:	fec77be3          	bgeu	a4,a2,3cc0 <vListInsert+0x2c>
    3cce:	bfd1                	c.j	3ca2 <vListInsert+0xe>

00003cd0 <uxListRemove>:
    3cd0:	4158                	c.lw	a4,4(a0)
    3cd2:	4514                	c.lw	a3,8(a0)
    3cd4:	491c                	c.lw	a5,16(a0)
    3cd6:	c714                	c.sw	a3,8(a4)
    3cd8:	00852283          	lw	t0,8(a0)
    3cdc:	00452303          	lw	t1,4(a0)
    3ce0:	0062a223          	sw	t1,4(t0)
    3ce4:	0047a383          	lw	t2,4(a5)
    3ce8:	00a39463          	bne	t2,a0,3cf0 <uxListRemove+0x20>
    3cec:	450c                	c.lw	a1,8(a0)
    3cee:	c3cc                	c.sw	a1,4(a5)
    3cf0:	00052823          	sw	zero,16(a0)
    3cf4:	4388                	c.lw	a0,0(a5)
    3cf6:	fff50613          	addi	a2,a0,-1
    3cfa:	c390                	c.sw	a2,0(a5)
    3cfc:	4388                	c.lw	a0,0(a5)
    3cfe:	8082                	c.jr	ra

00003d00 <prvIsQueueEmpty>:
    3d00:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
    3d02:	1141                	c.addi	sp,-16
    3d04:	c62a                	c.swsp	a0,12(sp)
    3d06:	8420                	exec.it	#18     !jal	ra,46f6 <vTaskEnterCritical>
    3d08:	4532                	c.lwsp	a0,12(sp)
    3d0a:	5d00                	c.lw	s0,56(a0)
    3d0c:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    3d0e:	00143513          	seqz	a0,s0
    3d12:	0141                	c.addi	sp,16
    3d14:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>

00003d16 <prvCopyDataToQueue>:
    3d16:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    3d18:	8932                	c.mv	s2,a2
    3d1a:	5d04                	c.lw	s1,56(a0)
    3d1c:	4130                	c.lw	a2,64(a0)
    3d1e:	842a                	c.mv	s0,a0
    3d20:	ee11                	c.bnez	a2,3d3c <prvCopyDataToQueue+0x26>
    3d22:	00052e03          	lw	t3,0(a0)
    3d26:	4501                	c.li	a0,0
    3d28:	000e1763          	bnez	t3,3d36 <prvCopyDataToQueue+0x20>
    3d2c:	4408                	c.lw	a0,8(s0)
    3d2e:	13d000ef          	jal	ra,466a <xTaskPriorityDisinherit>
    3d32:	00042423          	sw	zero,8(s0)
    3d36:	0485                	c.addi	s1,1
    3d38:	dc04                	c.sw	s1,56(s0)
    3d3a:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    3d3c:	02091263          	bnez	s2,3d60 <prvCopyDataToQueue+0x4a>
    3d40:	4148                	c.lw	a0,4(a0)
    3d42:	8c20                	exec.it	#22     !jal	ra,5350 <memcpy>
    3d44:	4048                	c.lw	a0,4(s0)
    3d46:	402c                	c.lw	a1,64(s0)
    3d48:	00842803          	lw	a6,8(s0)
    3d4c:	00b50633          	add	a2,a0,a1
    3d50:	c050                	c.sw	a2,4(s0)
    3d52:	4501                	c.li	a0,0
    3d54:	ff0661e3          	bltu	a2,a6,3d36 <prvCopyDataToQueue+0x20>
    3d58:	8a40                	exec.it	#100     !lw	a7,0(s0)
    3d5a:	01142223          	sw	a7,4(s0)
    3d5e:	bfe1                	c.j	3d36 <prvCopyDataToQueue+0x20>
    3d60:	4548                	c.lw	a0,12(a0)
    3d62:	8c20                	exec.it	#22     !jal	ra,5350 <memcpy>
    3d64:	403c                	c.lw	a5,64(s0)
    3d66:	4458                	c.lw	a4,12(s0)
    3d68:	40f002b3          	neg	t0,a5
    3d6c:	00570333          	add	t1,a4,t0
    3d70:	4014                	c.lw	a3,0(s0)
    3d72:	00642623          	sw	t1,12(s0)
    3d76:	00d37763          	bgeu	t1,a3,3d84 <prvCopyDataToQueue+0x6e>
    3d7a:	00842083          	lw	ra,8(s0)
    3d7e:	005083b3          	add	t2,ra,t0
    3d82:	8a34                	exec.it	#93     !sw	t2,12(s0)
    3d84:	4501                	c.li	a0,0
    3d86:	ba29685b          	bnec	s2,2,3d36 <prvCopyDataToQueue+0x20>
    3d8a:	d4d5                	c.beqz	s1,3d36 <prvCopyDataToQueue+0x20>
    3d8c:	14fd                	c.addi	s1,-1
    3d8e:	b765                	c.j	3d36 <prvCopyDataToQueue+0x20>

00003d90 <prvCopyDataFromQueue>:
    3d90:	4130                	c.lw	a2,64(a0)
    3d92:	c21d                	c.beqz	a2,3db8 <prvCopyDataFromQueue+0x28>
    3d94:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    3d96:	4558                	c.lw	a4,12(a0)
    3d98:	4514                	c.lw	a3,8(a0)
    3d9a:	00c700b3          	add	ra,a4,a2
    3d9e:	00152623          	sw	ra,12(a0)
    3da2:	87aa                	c.mv	a5,a0
    3da4:	00d0e663          	bltu	ra,a3,3db0 <prvCopyDataFromQueue+0x20>
    3da8:	00052283          	lw	t0,0(a0)
    3dac:	00552623          	sw	t0,12(a0)
    3db0:	852e                	c.mv	a0,a1
    3db2:	47cc                	c.lw	a1,12(a5)
    3db4:	8c20                	exec.it	#22     !jal	ra,5350 <memcpy>
    3db6:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
    3db8:	8082                	c.jr	ra

00003dba <prvUnlockQueue>:
    3dba:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    3dbc:	842a                	c.mv	s0,a0
    3dbe:	8420                	exec.it	#18     !jal	ra,46f6 <vTaskEnterCritical>
    3dc0:	04540483          	lb	s1,69(s0)
    3dc4:	02440913          	addi	s2,s0,36
    3dc8:	1c04b4db          	bfos	s1,s1,7,0
    3dcc:	02904463          	bgtz	s1,3df4 <prvUnlockQueue+0x3a>
    3dd0:	50fd                	c.li	ra,-1
    3dd2:	041402a3          	sb	ra,69(s0)
    3dd6:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    3dd8:	8420                	exec.it	#18     !jal	ra,46f6 <vTaskEnterCritical>
    3dda:	04440283          	lb	t0,68(s0)
    3dde:	01040913          	addi	s2,s0,16
    3de2:	1c02b4db          	bfos	s1,t0,7,0
    3de6:	02904263          	bgtz	s1,3e0a <prvUnlockQueue+0x50>
    3dea:	53fd                	c.li	t2,-1
    3dec:	04740223          	sb	t2,68(s0)
    3df0:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    3df2:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    3df4:	505c                	c.lw	a5,36(s0)
    3df6:	dfe9                	c.beqz	a5,3dd0 <prvUnlockQueue+0x16>
    3df8:	854a                	c.mv	a0,s2
    3dfa:	8a10                	exec.it	#69     !jal	ra,45c6 <xTaskRemoveFromEventList>
    3dfc:	c119                	c.beqz	a0,3e02 <prvUnlockQueue+0x48>
    3dfe:	051000ef          	jal	ra,464e <vTaskMissedYield>
    3e02:	8204                	exec.it	#72     !addi	a1,s1,-1
    3e04:	1c05b4db          	bfos	s1,a1,7,0
    3e08:	b7d1                	c.j	3dcc <prvUnlockQueue+0x12>
    3e0a:	01042303          	lw	t1,16(s0)
    3e0e:	fc030ee3          	beqz	t1,3dea <prvUnlockQueue+0x30>
    3e12:	854a                	c.mv	a0,s2
    3e14:	8a10                	exec.it	#69     !jal	ra,45c6 <xTaskRemoveFromEventList>
    3e16:	c119                	c.beqz	a0,3e1c <prvUnlockQueue+0x62>
    3e18:	037000ef          	jal	ra,464e <vTaskMissedYield>
    3e1c:	fff48513          	addi	a0,s1,-1
    3e20:	1c0534db          	bfos	s1,a0,7,0
    3e24:	b7c9                	c.j	3de6 <prvUnlockQueue+0x2c>

00003e26 <xQueueGenericReset>:
    3e26:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    3e28:	842a                	c.mv	s0,a0
    3e2a:	84ae                	c.mv	s1,a1
    3e2c:	e519                	c.bnez	a0,3e3a <xQueueGenericReset+0x14>
    3e2e:	10300593          	li	a1,259
    3e32:	8c50                	exec.it	#39     !auipc	a0,0x2
    3e34:	ae650513          	addi	a0,a0,-1306 # 5918 <_ITB_BASE_+0x25c>
    3e38:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    3e3a:	8420                	exec.it	#18     !jal	ra,46f6 <vTaskEnterCritical>
    3e3c:	4030                	c.lw	a2,64(s0)
    3e3e:	5c58                	c.lw	a4,60(s0)
    3e40:	4014                	c.lw	a3,0(s0)
    3e42:	02e607b3          	mul	a5,a2,a4
    3e46:	02042c23          	sw	zero,56(s0)
    3e4a:	557d                	c.li	a0,-1
    3e4c:	04a40223          	sb	a0,68(s0)
    3e50:	c054                	c.sw	a3,4(s0)
    3e52:	04a402a3          	sb	a0,69(s0)
    3e56:	40c78333          	sub	t1,a5,a2
    3e5a:	00f682b3          	add	t0,a3,a5
    3e5e:	006683b3          	add	t2,a3,t1
    3e62:	00542423          	sw	t0,8(s0)
    3e66:	8a34                	exec.it	#93     !sw	t2,12(s0)
    3e68:	ec81                	c.bnez	s1,3e80 <xQueueGenericReset+0x5a>
    3e6a:	01042083          	lw	ra,16(s0)
    3e6e:	00008663          	beqz	ra,3e7a <xQueueGenericReset+0x54>
    3e72:	8464                	exec.it	#58     !addi	a0,s0,16
    3e74:	8a10                	exec.it	#69     !jal	ra,45c6 <xTaskRemoveFromEventList>
    3e76:	c111                	c.beqz	a0,3e7a <xQueueGenericReset+0x54>
    3e78:	8c30                	exec.it	#23     !jal	ra,5154 <vPortYield>
    3e7a:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    3e7c:	4505                	c.li	a0,1
    3e7e:	8c14                	exec.it	#15     !j	52f4 <__riscv_restore_0>
    3e80:	8464                	exec.it	#58     !addi	a0,s0,16
    3e82:	3bd9                	c.jal	3c58 <vListInitialise>
    3e84:	8064                	exec.it	#56     !addi	a0,s0,36
    3e86:	3bc9                	c.jal	3c58 <vListInitialise>
    3e88:	bfcd                	c.j	3e7a <xQueueGenericReset+0x54>

00003e8a <xQueueGenericCreateStatic>:
    3e8a:	8660                	exec.it	#114     !jal	t0,52b6 <__riscv_save_4>
    3e8c:	1141                	c.addi	sp,-16
    3e8e:	892a                	c.mv	s2,a0
    3e90:	84ae                	c.mv	s1,a1
    3e92:	89b2                	c.mv	s3,a2
    3e94:	8436                	c.mv	s0,a3
    3e96:	8a3a                	c.mv	s4,a4
    3e98:	e519                	c.bnez	a0,3ea6 <xQueueGenericCreateStatic+0x1c>
    3e9a:	13a00593          	li	a1,314
    3e9e:	8c50                	exec.it	#39     !auipc	a0,0x2
    3ea0:	a7a50513          	addi	a0,a0,-1414 # 5918 <_ITB_BASE_+0x25c>
    3ea4:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    3ea6:	e419                	c.bnez	s0,3eb4 <xQueueGenericCreateStatic+0x2a>
    3ea8:	13e00593          	li	a1,318
    3eac:	8c50                	exec.it	#39     !auipc	a0,0x2
    3eae:	a6c50513          	addi	a0,a0,-1428 # 5918 <_ITB_BASE_+0x25c>
    3eb2:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    3eb4:	04098663          	beqz	s3,3f00 <xQueueGenericCreateStatic+0x76>
    3eb8:	14200593          	li	a1,322
    3ebc:	c4a9                	c.beqz	s1,3f06 <xQueueGenericCreateStatic+0x7c>
    3ebe:	05000793          	li	a5,80
    3ec2:	c63e                	c.swsp	a5,12(sp)
    3ec4:	42b2                	c.lwsp	t0,12(sp)
    3ec6:	4102d85b          	beqc	t0,80,3ed6 <xQueueGenericCreateStatic+0x4c>
    3eca:	14b00593          	li	a1,331
    3ece:	8c50                	exec.it	#39     !auipc	a0,0x2
    3ed0:	a4a50513          	addi	a0,a0,-1462 # 5918 <_ITB_BASE_+0x25c>
    3ed4:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    3ed6:	47b2                	c.lwsp	a5,12(sp)
    3ed8:	cc11                	c.beqz	s0,3ef4 <xQueueGenericCreateStatic+0x6a>
    3eda:	4085                	c.li	ra,1
    3edc:	04140323          	sb	ra,70(s0)
    3ee0:	ec89                	c.bnez	s1,3efa <xQueueGenericCreateStatic+0x70>
    3ee2:	c000                	c.sw	s0,0(s0)
    3ee4:	4585                	c.li	a1,1
    3ee6:	8522                	c.mv	a0,s0
    3ee8:	03242e23          	sw	s2,60(s0)
    3eec:	c024                	c.sw	s1,64(s0)
    3eee:	3f25                	c.jal	3e26 <xQueueGenericReset>
    3ef0:	05440623          	sb	s4,76(s0)
    3ef4:	8522                	c.mv	a0,s0
    3ef6:	0141                	c.addi	sp,16
    3ef8:	8674                	exec.it	#123     !j	52ea <__riscv_restore_4>
    3efa:	01342023          	sw	s3,0(s0)
    3efe:	b7dd                	c.j	3ee4 <xQueueGenericCreateStatic+0x5a>
    3f00:	dcdd                	c.beqz	s1,3ebe <xQueueGenericCreateStatic+0x34>
    3f02:	14300593          	li	a1,323
    3f06:	8c50                	exec.it	#39     !auipc	a0,0x2
    3f08:	a1250513          	addi	a0,a0,-1518 # 5918 <_ITB_BASE_+0x25c>
    3f0c:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    3f0e:	bf45                	c.j	3ebe <xQueueGenericCreateStatic+0x34>

00003f10 <xQueueGenericCreate>:
    3f10:	8830                	exec.it	#21     !jal	t0,52b6 <__riscv_save_4>
    3f12:	892a                	c.mv	s2,a0
    3f14:	84ae                	c.mv	s1,a1
    3f16:	89b2                	c.mv	s3,a2
    3f18:	e519                	c.bnez	a0,3f26 <xQueueGenericCreate+0x16>
    3f1a:	17600593          	li	a1,374
    3f1e:	8c50                	exec.it	#39     !auipc	a0,0x2
    3f20:	9fa50513          	addi	a0,a0,-1542 # 5918 <_ITB_BASE_+0x25c>
    3f24:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    3f26:	02990533          	mul	a0,s2,s1
    3f2a:	05050513          	addi	a0,a0,80
    3f2e:	8454                	exec.it	#43     !jal	ra,4e7a <pvPortMalloc>
    3f30:	842a                	c.mv	s0,a0
    3f32:	cd09                	c.beqz	a0,3f4c <xQueueGenericCreate+0x3c>
    3f34:	04050323          	sb	zero,70(a0)
    3f38:	ec81                	c.bnez	s1,3f50 <xQueueGenericCreate+0x40>
    3f3a:	c008                	c.sw	a0,0(s0)
    3f3c:	4585                	c.li	a1,1
    3f3e:	8522                	c.mv	a0,s0
    3f40:	03242e23          	sw	s2,60(s0)
    3f44:	c024                	c.sw	s1,64(s0)
    3f46:	35c5                	c.jal	3e26 <xQueueGenericReset>
    3f48:	05340623          	sb	s3,76(s0)
    3f4c:	8522                	c.mv	a0,s0
    3f4e:	8024                	exec.it	#24     !j	52ea <__riscv_restore_4>
    3f50:	05050793          	addi	a5,a0,80
    3f54:	c11c                	c.sw	a5,0(a0)
    3f56:	b7dd                	c.j	3f3c <xQueueGenericCreate+0x2c>

00003f58 <xQueueGenericSend>:
    3f58:	8820                	exec.it	#20     !jal	t0,52b6 <__riscv_save_4>
    3f5a:	1101                	c.addi	sp,-32
    3f5c:	c632                	c.swsp	a2,12(sp)
    3f5e:	842a                	c.mv	s0,a0
    3f60:	892e                	c.mv	s2,a1
    3f62:	84b6                	c.mv	s1,a3
    3f64:	e519                	c.bnez	a0,3f72 <xQueueGenericSend+0x1a>
    3f66:	2f100593          	li	a1,753
    3f6a:	8c50                	exec.it	#39     !auipc	a0,0x2
    3f6c:	9ae50513          	addi	a0,a0,-1618 # 5918 <_ITB_BASE_+0x25c>
    3f70:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    3f72:	00091a63          	bnez	s2,3f86 <xQueueGenericSend+0x2e>
    3f76:	403c                	c.lw	a5,64(s0)
    3f78:	c799                	c.beqz	a5,3f86 <xQueueGenericSend+0x2e>
    3f7a:	2f200593          	li	a1,754
    3f7e:	8c50                	exec.it	#39     !auipc	a0,0x2
    3f80:	99a50513          	addi	a0,a0,-1638 # 5918 <_ITB_BASE_+0x25c>
    3f84:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    3f86:	0024ec5b          	bnec	s1,2,3f9e <xQueueGenericSend+0x46>
    3f8a:	03c42083          	lw	ra,60(s0)
    3f8e:	0010d85b          	beqc	ra,1,3f9e <xQueueGenericSend+0x46>
    3f92:	2f300593          	li	a1,755
    3f96:	8c50                	exec.it	#39     !auipc	a0,0x2
    3f98:	98250513          	addi	a0,a0,-1662 # 5918 <_ITB_BASE_+0x25c>
    3f9c:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    3f9e:	8670                	exec.it	#115     !jal	ra,4656 <xTaskGetSchedulerState>
    3fa0:	e911                	c.bnez	a0,3fb4 <xQueueGenericSend+0x5c>
    3fa2:	42b2                	c.lwsp	t0,12(sp)
    3fa4:	00028863          	beqz	t0,3fb4 <xQueueGenericSend+0x5c>
    3fa8:	2f600593          	li	a1,758
    3fac:	8c50                	exec.it	#39     !auipc	a0,0x2
    3fae:	96c50513          	addi	a0,a0,-1684 # 5918 <_ITB_BASE_+0x25c>
    3fb2:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    3fb4:	4a01                	c.li	s4,0
    3fb6:	59fd                	c.li	s3,-1
    3fb8:	01040a93          	addi	s5,s0,16
    3fbc:	8420                	exec.it	#18     !jal	ra,46f6 <vTaskEnterCritical>
    3fbe:	5c18                	c.lw	a4,56(s0)
    3fc0:	03c42303          	lw	t1,60(s0)
    3fc4:	00676463          	bltu	a4,t1,3fcc <xQueueGenericSend+0x74>
    3fc8:	0224e25b          	bnec	s1,2,3fec <xQueueGenericSend+0x94>
    3fcc:	8626                	c.mv	a2,s1
    3fce:	85ca                	c.mv	a1,s2
    3fd0:	8522                	c.mv	a0,s0
    3fd2:	3391                	c.jal	3d16 <prvCopyDataToQueue>
    3fd4:	02442e03          	lw	t3,36(s0)
    3fd8:	000e0463          	beqz	t3,3fe0 <xQueueGenericSend+0x88>
    3fdc:	8064                	exec.it	#56     !addi	a0,s0,36
    3fde:	8a10                	exec.it	#69     !jal	ra,45c6 <xTaskRemoveFromEventList>
    3fe0:	c111                	c.beqz	a0,3fe4 <xQueueGenericSend+0x8c>
    3fe2:	8c30                	exec.it	#23     !jal	ra,5154 <vPortYield>
    3fe4:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    3fe6:	4505                	c.li	a0,1
    3fe8:	6105                	c.addi16sp	sp,32
    3fea:	8c34                	exec.it	#31     !j	52ea <__riscv_restore_4>
    3fec:	43b2                	c.lwsp	t2,12(sp)
    3fee:	00039563          	bnez	t2,3ff8 <xQueueGenericSend+0xa0>
    3ff2:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    3ff4:	4501                	c.li	a0,0
    3ff6:	bfcd                	c.j	3fe8 <xQueueGenericSend+0x90>
    3ff8:	000a1563          	bnez	s4,4002 <xQueueGenericSend+0xaa>
    3ffc:	0828                	c.addi4spn	a0,sp,24
    3ffe:	640000ef          	jal	ra,463e <vTaskInternalSetTimeOutState>
    4002:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    4004:	26f1                	c.jal	43d0 <vTaskSuspendAll>
    4006:	8420                	exec.it	#18     !jal	ra,46f6 <vTaskEnterCritical>
    4008:	04440503          	lb	a0,68(s0)
    400c:	1c0535db          	bfos	a1,a0,7,0
    4010:	01359363          	bne	a1,s3,4016 <xQueueGenericSend+0xbe>
    4014:	8e30                	exec.it	#87     !sb	zero,68(s0)
    4016:	04540603          	lb	a2,69(s0)
    401a:	1c0636db          	bfos	a3,a2,7,0
    401e:	01369363          	bne	a3,s3,4024 <xQueueGenericSend+0xcc>
    4022:	8e20                	exec.it	#86     !sb	zero,69(s0)
    4024:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    4026:	006c                	c.addi4spn	a1,sp,12
    4028:	0828                	c.addi4spn	a0,sp,24
    402a:	297000ef          	jal	ra,4ac0 <xTaskCheckForTimeOut>
    402e:	e90d                	c.bnez	a0,4060 <xQueueGenericSend+0x108>
    4030:	8420                	exec.it	#18     !jal	ra,46f6 <vTaskEnterCritical>
    4032:	03842803          	lw	a6,56(s0)
    4036:	03c42883          	lw	a7,60(s0)
    403a:	01181d63          	bne	a6,a7,4054 <xQueueGenericSend+0xfc>
    403e:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    4040:	45b2                	c.lwsp	a1,12(sp)
    4042:	8556                	c.mv	a0,s5
    4044:	52c000ef          	jal	ra,4570 <vTaskPlaceOnEventList>
    4048:	8522                	c.mv	a0,s0
    404a:	3b85                	c.jal	3dba <prvUnlockQueue>
    404c:	8824                	exec.it	#28     !jal	ra,49ea <xTaskResumeAll>
    404e:	e519                	c.bnez	a0,405c <xQueueGenericSend+0x104>
    4050:	8c30                	exec.it	#23     !jal	ra,5154 <vPortYield>
    4052:	a029                	c.j	405c <xQueueGenericSend+0x104>
    4054:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    4056:	8522                	c.mv	a0,s0
    4058:	338d                	c.jal	3dba <prvUnlockQueue>
    405a:	8824                	exec.it	#28     !jal	ra,49ea <xTaskResumeAll>
    405c:	4a05                	c.li	s4,1
    405e:	bfb9                	c.j	3fbc <xQueueGenericSend+0x64>
    4060:	8522                	c.mv	a0,s0
    4062:	3ba1                	c.jal	3dba <prvUnlockQueue>
    4064:	8824                	exec.it	#28     !jal	ra,49ea <xTaskResumeAll>
    4066:	b779                	c.j	3ff4 <xQueueGenericSend+0x9c>

00004068 <xQueueGenericSendFromISR>:
    4068:	8820                	exec.it	#20     !jal	t0,52b6 <__riscv_save_4>
    406a:	842a                	c.mv	s0,a0
    406c:	8a2e                	c.mv	s4,a1
    406e:	89b2                	c.mv	s3,a2
    4070:	8936                	c.mv	s2,a3
    4072:	e519                	c.bnez	a0,4080 <xQueueGenericSendFromISR+0x18>
    4074:	3c300593          	li	a1,963
    4078:	8c50                	exec.it	#39     !auipc	a0,0x2
    407a:	8a050513          	addi	a0,a0,-1888 # 5918 <_ITB_BASE_+0x25c>
    407e:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4080:	000a1a63          	bnez	s4,4094 <xQueueGenericSendFromISR+0x2c>
    4084:	403c                	c.lw	a5,64(s0)
    4086:	c799                	c.beqz	a5,4094 <xQueueGenericSendFromISR+0x2c>
    4088:	3c400593          	li	a1,964
    408c:	8c50                	exec.it	#39     !auipc	a0,0x2
    408e:	88c50513          	addi	a0,a0,-1908 # 5918 <_ITB_BASE_+0x25c>
    4092:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4094:	00296c5b          	bnec	s2,2,40ac <xQueueGenericSendFromISR+0x44>
    4098:	03c42083          	lw	ra,60(s0)
    409c:	0010d85b          	beqc	ra,1,40ac <xQueueGenericSendFromISR+0x44>
    40a0:	3c500593          	li	a1,965
    40a4:	8c50                	exec.it	#39     !auipc	a0,0x2
    40a6:	87450513          	addi	a0,a0,-1932 # 5918 <_ITB_BASE_+0x25c>
    40aa:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    40ac:	749000ef          	jal	ra,4ff4 <vPortSetInterruptMask>
    40b0:	5c18                	c.lw	a4,56(s0)
    40b2:	03c42283          	lw	t0,60(s0)
    40b6:	8aaa                	c.mv	s5,a0
    40b8:	00576563          	bltu	a4,t0,40c2 <xQueueGenericSendFromISR+0x5a>
    40bc:	4481                	c.li	s1,0
    40be:	0229605b          	bnec	s2,2,40de <xQueueGenericSendFromISR+0x76>
    40c2:	04540483          	lb	s1,69(s0)
    40c6:	864a                	c.mv	a2,s2
    40c8:	85d2                	c.mv	a1,s4
    40ca:	8522                	c.mv	a0,s0
    40cc:	31a9                	c.jal	3d16 <prvCopyDataToQueue>
    40ce:	1c04b4db          	bfos	s1,s1,7,0
    40d2:	537d                	c.li	t1,-1
    40d4:	02649363          	bne	s1,t1,40fa <xQueueGenericSendFromISR+0x92>
    40d8:	504c                	c.lw	a1,36(s0)
    40da:	e599                	c.bnez	a1,40e8 <xQueueGenericSendFromISR+0x80>
    40dc:	4485                	c.li	s1,1
    40de:	8556                	c.mv	a0,s5
    40e0:	70f000ef          	jal	ra,4fee <vPortClearInterruptMask>
    40e4:	8526                	c.mv	a0,s1
    40e6:	8c34                	exec.it	#31     !j	52ea <__riscv_restore_4>
    40e8:	8064                	exec.it	#56     !addi	a0,s0,36
    40ea:	29f1                	c.jal	45c6 <xTaskRemoveFromEventList>
    40ec:	d965                	c.beqz	a0,40dc <xQueueGenericSendFromISR+0x74>
    40ee:	fe0987e3          	beqz	s3,40dc <xQueueGenericSendFromISR+0x74>
    40f2:	4605                	c.li	a2,1
    40f4:	00c9a023          	sw	a2,0(s3)
    40f8:	b7d5                	c.j	40dc <xQueueGenericSendFromISR+0x74>
    40fa:	00148393          	addi	t2,s1,1
    40fe:	1c03b55b          	bfos	a0,t2,7,0
    4102:	04a402a3          	sb	a0,69(s0)
    4106:	bfd9                	c.j	40dc <xQueueGenericSendFromISR+0x74>

00004108 <xQueueReceive>:
    4108:	8820                	exec.it	#20     !jal	t0,52b6 <__riscv_save_4>
    410a:	1101                	c.addi	sp,-32
    410c:	c632                	c.swsp	a2,12(sp)
    410e:	842a                	c.mv	s0,a0
    4110:	892e                	c.mv	s2,a1
    4112:	e901                	c.bnez	a0,4122 <xQueueReceive+0x1a>
    4114:	50000593          	li	a1,1280
    4118:	00002517          	auipc	a0,0x2
    411c:	80050513          	addi	a0,a0,-2048 # 5918 <_ITB_BASE_+0x25c>
    4120:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4122:	00091b63          	bnez	s2,4138 <xQueueReceive+0x30>
    4126:	403c                	c.lw	a5,64(s0)
    4128:	cb81                	c.beqz	a5,4138 <xQueueReceive+0x30>
    412a:	50400593          	li	a1,1284
    412e:	00001517          	auipc	a0,0x1
    4132:	7ea50513          	addi	a0,a0,2026 # 5918 <_ITB_BASE_+0x25c>
    4136:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4138:	8670                	exec.it	#115     !jal	ra,4656 <xTaskGetSchedulerState>
    413a:	e919                	c.bnez	a0,4150 <xQueueReceive+0x48>
    413c:	40b2                	c.lwsp	ra,12(sp)
    413e:	00008963          	beqz	ra,4150 <xQueueReceive+0x48>
    4142:	50900593          	li	a1,1289
    4146:	00001517          	auipc	a0,0x1
    414a:	7d250513          	addi	a0,a0,2002 # 5918 <_ITB_BASE_+0x25c>
    414e:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4150:	4a01                	c.li	s4,0
    4152:	59fd                	c.li	s3,-1
    4154:	02440a93          	addi	s5,s0,36
    4158:	8420                	exec.it	#18     !jal	ra,46f6 <vTaskEnterCritical>
    415a:	5c04                	c.lw	s1,56(s0)
    415c:	c085                	c.beqz	s1,417c <xQueueReceive+0x74>
    415e:	85ca                	c.mv	a1,s2
    4160:	8522                	c.mv	a0,s0
    4162:	313d                	c.jal	3d90 <prvCopyDataFromQueue>
    4164:	14fd                	c.addi	s1,-1
    4166:	dc04                	c.sw	s1,56(s0)
    4168:	4810                	c.lw	a2,16(s0)
    416a:	c609                	c.beqz	a2,4174 <xQueueReceive+0x6c>
    416c:	8464                	exec.it	#58     !addi	a0,s0,16
    416e:	29a1                	c.jal	45c6 <xTaskRemoveFromEventList>
    4170:	c111                	c.beqz	a0,4174 <xQueueReceive+0x6c>
    4172:	8c30                	exec.it	#23     !jal	ra,5154 <vPortYield>
    4174:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    4176:	4505                	c.li	a0,1
    4178:	6105                	c.addi16sp	sp,32
    417a:	8c34                	exec.it	#31     !j	52ea <__riscv_restore_4>
    417c:	42b2                	c.lwsp	t0,12(sp)
    417e:	00029563          	bnez	t0,4188 <xQueueReceive+0x80>
    4182:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    4184:	4501                	c.li	a0,0
    4186:	bfcd                	c.j	4178 <xQueueReceive+0x70>
    4188:	000a1463          	bnez	s4,4190 <xQueueReceive+0x88>
    418c:	0828                	c.addi4spn	a0,sp,24
    418e:	2945                	c.jal	463e <vTaskInternalSetTimeOutState>
    4190:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    4192:	2c3d                	c.jal	43d0 <vTaskSuspendAll>
    4194:	8420                	exec.it	#18     !jal	ra,46f6 <vTaskEnterCritical>
    4196:	04440303          	lb	t1,68(s0)
    419a:	1c0333db          	bfos	t2,t1,7,0
    419e:	01339363          	bne	t2,s3,41a4 <xQueueReceive+0x9c>
    41a2:	8e30                	exec.it	#87     !sb	zero,68(s0)
    41a4:	04540503          	lb	a0,69(s0)
    41a8:	1c0535db          	bfos	a1,a0,7,0
    41ac:	01359363          	bne	a1,s3,41b2 <xQueueReceive+0xaa>
    41b0:	8e20                	exec.it	#86     !sb	zero,69(s0)
    41b2:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    41b4:	006c                	c.addi4spn	a1,sp,12
    41b6:	0828                	c.addi4spn	a0,sp,24
    41b8:	109000ef          	jal	ra,4ac0 <xTaskCheckForTimeOut>
    41bc:	e115                	c.bnez	a0,41e0 <xQueueReceive+0xd8>
    41be:	8522                	c.mv	a0,s0
    41c0:	3681                	c.jal	3d00 <prvIsQueueEmpty>
    41c2:	c919                	c.beqz	a0,41d8 <xQueueReceive+0xd0>
    41c4:	45b2                	c.lwsp	a1,12(sp)
    41c6:	8556                	c.mv	a0,s5
    41c8:	2665                	c.jal	4570 <vTaskPlaceOnEventList>
    41ca:	8522                	c.mv	a0,s0
    41cc:	36fd                	c.jal	3dba <prvUnlockQueue>
    41ce:	8824                	exec.it	#28     !jal	ra,49ea <xTaskResumeAll>
    41d0:	e111                	c.bnez	a0,41d4 <xQueueReceive+0xcc>
    41d2:	8c30                	exec.it	#23     !jal	ra,5154 <vPortYield>
    41d4:	4a05                	c.li	s4,1
    41d6:	b749                	c.j	4158 <xQueueReceive+0x50>
    41d8:	8522                	c.mv	a0,s0
    41da:	36c5                	c.jal	3dba <prvUnlockQueue>
    41dc:	8824                	exec.it	#28     !jal	ra,49ea <xTaskResumeAll>
    41de:	bfdd                	c.j	41d4 <xQueueReceive+0xcc>
    41e0:	8522                	c.mv	a0,s0
    41e2:	3ee1                	c.jal	3dba <prvUnlockQueue>
    41e4:	8824                	exec.it	#28     !jal	ra,49ea <xTaskResumeAll>
    41e6:	8522                	c.mv	a0,s0
    41e8:	3e21                	c.jal	3d00 <prvIsQueueEmpty>
    41ea:	d56d                	c.beqz	a0,41d4 <xQueueReceive+0xcc>
    41ec:	bf61                	c.j	4184 <xQueueReceive+0x7c>

000041ee <vQueueAddToRegistry>:
    41ee:	00023717          	auipc	a4,0x23
    41f2:	02e70713          	addi	a4,a4,46 # 2721c <xQueueRegistry>
    41f6:	4781                	c.li	a5,0
    41f8:	86ba                	c.mv	a3,a4
    41fa:	4310                	c.lw	a2,0(a4)
    41fc:	ea01                	c.bnez	a2,420c <vQueueAddToRegistry+0x1e>
    41fe:	0ef682db          	lea.d	t0,a3,a5
    4202:	00b2a023          	sw	a1,0(t0)
    4206:	00a2a223          	sw	a0,4(t0)
    420a:	8082                	c.jr	ra
    420c:	0785                	c.addi	a5,1
    420e:	0721                	c.addi	a4,8
    4210:	bea7e55b          	bnec	a5,10,41fa <vQueueAddToRegistry+0xc>
    4214:	8082                	c.jr	ra

00004216 <vQueueWaitForMessageRestricted>:
    4216:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
    4218:	1141                	c.addi	sp,-16
    421a:	842a                	c.mv	s0,a0
    421c:	c62e                	c.swsp	a1,12(sp)
    421e:	c432                	c.swsp	a2,8(sp)
    4220:	29d9                	c.jal	46f6 <vTaskEnterCritical>
    4222:	04440783          	lb	a5,68(s0)
    4226:	577d                	c.li	a4,-1
    4228:	1c07b2db          	bfos	t0,a5,7,0
    422c:	4622                	c.lwsp	a2,8(sp)
    422e:	45b2                	c.lwsp	a1,12(sp)
    4230:	00e29363          	bne	t0,a4,4236 <vQueueWaitForMessageRestricted+0x20>
    4234:	8e30                	exec.it	#87     !sb	zero,68(s0)
    4236:	04540083          	lb	ra,69(s0)
    423a:	53fd                	c.li	t2,-1
    423c:	1c00b35b          	bfos	t1,ra,7,0
    4240:	00731363          	bne	t1,t2,4246 <vQueueWaitForMessageRestricted+0x30>
    4244:	8e20                	exec.it	#86     !sb	zero,69(s0)
    4246:	c632                	c.swsp	a2,12(sp)
    4248:	c42e                	c.swsp	a1,8(sp)
    424a:	21d1                	c.jal	470e <vTaskExitCritical>
    424c:	5c08                	c.lw	a0,56(s0)
    424e:	45a2                	c.lwsp	a1,8(sp)
    4250:	4632                	c.lwsp	a2,12(sp)
    4252:	e119                	c.bnez	a0,4258 <vQueueWaitForMessageRestricted+0x42>
    4254:	8064                	exec.it	#56     !addi	a0,s0,36
    4256:	2689                	c.jal	4598 <vTaskPlaceOnEventListRestricted>
    4258:	8522                	c.mv	a0,s0
    425a:	3685                	c.jal	3dba <prvUnlockQueue>
    425c:	0141                	c.addi	sp,16
    425e:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>

00004260 <prvResetNextTaskUnblockTime>:
    4260:	c901a783          	lw	a5,-880(gp) # 5fec <pxDelayedTaskList>
    4264:	0007a283          	lw	t0,0(a5)
    4268:	00029663          	bnez	t0,4274 <prvResetNextTaskUnblockTime+0x14>
    426c:	567d                	c.li	a2,-1
    426e:	cac1a823          	sw	a2,-848(gp) # 600c <xNextTaskUnblockTime>
    4272:	8082                	c.jr	ra
    4274:	c901a303          	lw	t1,-880(gp) # 5fec <pxDelayedTaskList>
    4278:	00c32383          	lw	t2,12(t1)
    427c:	00c3a503          	lw	a0,12(t2)
    4280:	414c                	c.lw	a1,4(a0)
    4282:	cab1a823          	sw	a1,-848(gp) # 600c <xNextTaskUnblockTime>
    4286:	8082                	c.jr	ra

00004288 <prvInitialiseNewTask.isra.2>:
    4288:	8040                	exec.it	#32     !jal	t0,52b6 <__riscv_save_4>
    428a:	8aaa                	c.mv	s5,a0
    428c:	84ae                	c.mv	s1,a1
    428e:	89b2                	c.mv	s3,a2
    4290:	8b36                	c.mv	s6,a3
    4292:	893a                	c.mv	s2,a4
    4294:	8a3e                	c.mv	s4,a5
    4296:	8442                	c.mv	s0,a6
    4298:	e599                	c.bnez	a1,42a6 <prvInitialiseNewTask.isra.2+0x1e>
    429a:	36000593          	li	a1,864
    429e:	8424                	exec.it	#26     !auipc	a0,0x1
    42a0:	7b650513          	addi	a0,a0,1974 # 5a54 <_ITB_BASE_+0x398>
    42a4:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    42a6:	098a                	c.slli	s3,0x2
    42a8:	5808                	c.lw	a0,48(s0)
    42aa:	864e                	c.mv	a2,s3
    42ac:	0a500593          	li	a1,165
    42b0:	8430                	exec.it	#19     !jal	ra,5526 <memset>
    42b2:	5810                	c.lw	a2,48(s0)
    42b4:	ffc98293          	addi	t0,s3,-4
    42b8:	00560333          	add	t1,a2,t0
    42bc:	ff037993          	andi	s3,t1,-16
    42c0:	85a6                	c.mv	a1,s1
    42c2:	03440793          	addi	a5,s0,52
    42c6:	01048713          	addi	a4,s1,16
    42ca:	0005c683          	lbu	a3,0(a1)
    42ce:	00d78023          	sb	a3,0(a5)
    42d2:	0005c083          	lbu	ra,0(a1)
    42d6:	00008663          	beqz	ra,42e2 <prvInitialiseNewTask.isra.2+0x5a>
    42da:	0585                	c.addi	a1,1
    42dc:	0785                	c.addi	a5,1
    42de:	fee596e3          	bne	a1,a4,42ca <prvInitialiseNewTask.isra.2+0x42>
    42e2:	040401a3          	sb	zero,67(s0)
    42e6:	4399                	c.li	t2,6
    42e8:	0123f363          	bgeu	t2,s2,42ee <prvInitialiseNewTask.isra.2+0x66>
    42ec:	4919                	c.li	s2,6
    42ee:	8250                	exec.it	#97     !addi	a0,s0,4
    42f0:	03242623          	sw	s2,44(s0)
    42f4:	05242823          	sw	s2,80(s0)
    42f8:	04042a23          	sw	zero,84(s0)
    42fc:	971ff0ef          	jal	ra,3c6c <vListInitialiseItem>
    4300:	8634                	exec.it	#91     !addi	a0,s0,24
    4302:	96bff0ef          	jal	ra,3c6c <vListInitialiseItem>
    4306:	451d                	c.li	a0,7
    4308:	412504b3          	sub	s1,a0,s2
    430c:	04042c23          	sw	zero,88(s0)
    4310:	865a                	c.mv	a2,s6
    4312:	85d6                	c.mv	a1,s5
    4314:	854e                	c.mv	a0,s3
    4316:	c800                	c.sw	s0,16(s0)
    4318:	cc04                	c.sw	s1,24(s0)
    431a:	d040                	c.sw	s0,36(s0)
    431c:	04042223          	sw	zero,68(s0)
    4320:	04040e23          	sb	zero,92(s0)
    4324:	04040f23          	sb	zero,94(s0)
    4328:	4d7000ef          	jal	ra,4ffe <pxPortInitialiseStack>
    432c:	c008                	c.sw	a0,0(s0)
    432e:	000a0463          	beqz	s4,4336 <prvInitialiseNewTask.isra.2+0xae>
    4332:	008a2023          	sw	s0,0(s4)
    4336:	8050                	exec.it	#33     !j	52ea <__riscv_restore_4>

00004338 <prvDeleteTCB>:
    4338:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
    433a:	05d54783          	lbu	a5,93(a0)
    433e:	e799                	c.bnez	a5,434c <prvDeleteTCB+0x14>
    4340:	842a                	c.mv	s0,a0
    4342:	5908                	c.lw	a0,48(a0)
    4344:	8444                	exec.it	#42     !jal	ra,4f5e <vPortFree>
    4346:	8522                	c.mv	a0,s0
    4348:	8444                	exec.it	#42     !jal	ra,4f5e <vPortFree>
    434a:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>
    434c:	be17de5b          	beqc	a5,1,4348 <prvDeleteTCB+0x10>
    4350:	be27dd5b          	beqc	a5,2,434a <prvDeleteTCB+0x12>
    4354:	6585                	c.lui	a1,0x1
    4356:	ec758593          	addi	a1,a1,-313 # ec7 <mscAppInit+0x2f>
    435a:	8424                	exec.it	#26     !auipc	a0,0x1
    435c:	6fa50513          	addi	a0,a0,1786 # 5a54 <_ITB_BASE_+0x398>
    4360:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4362:	b7e5                	c.j	434a <prvDeleteTCB+0x12>

00004364 <prvAddCurrentTaskToDelayedList>:
    4364:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    4366:	cbc1a483          	lw	s1,-836(gp) # 6018 <xTickCount>
    436a:	842a                	c.mv	s0,a0
    436c:	8070                	exec.it	#49     !lw	a5,-884(gp) # 5fe8 <pxCurrentTCB>
    436e:	c8c1a503          	lw	a0,-884(gp) # 5fe8 <pxCurrentTCB>
    4372:	1141                	c.addi	sp,-16
    4374:	0511                	c.addi	a0,4
    4376:	c62e                	c.swsp	a1,12(sp)
    4378:	04078f23          	sb	zero,94(a5)
    437c:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    437e:	52fd                	c.li	t0,-1
    4380:	45b2                	c.lwsp	a1,12(sp)
    4382:	00541c63          	bne	s0,t0,439a <prvAddCurrentTaskToDelayedList+0x36>
    4386:	c991                	c.beqz	a1,439a <prvAddCurrentTaskToDelayedList+0x36>
    4388:	c8c1a683          	lw	a3,-884(gp) # 5fe8 <pxCurrentTCB>
    438c:	00468593          	addi	a1,a3,4
    4390:	efc18513          	addi	a0,gp,-260 # 6258 <xSuspendedTaskList>
    4394:	8c24                	exec.it	#30     !jal	ra,3c72 <vListInsertEnd>
    4396:	0141                	c.addi	sp,16
    4398:	8c14                	exec.it	#15     !j	52f4 <__riscv_restore_0>
    439a:	9426                	c.add	s0,s1
    439c:	8a44                	exec.it	#108     !lw	ra,-884(gp) # 5fe8 <pxCurrentTCB>
    439e:	0080a223          	sw	s0,4(ra)
    43a2:	00947a63          	bgeu	s0,s1,43b6 <prvAddCurrentTaskToDelayedList+0x52>
    43a6:	c941a503          	lw	a0,-876(gp) # 5ff0 <pxOverflowDelayedTaskList>
    43aa:	c8c1a603          	lw	a2,-884(gp) # 5fe8 <pxCurrentTCB>
    43ae:	00460593          	addi	a1,a2,4
    43b2:	8460                	exec.it	#50     !jal	ra,3c94 <vListInsert>
    43b4:	b7cd                	c.j	4396 <prvAddCurrentTaskToDelayedList+0x32>
    43b6:	c901a503          	lw	a0,-880(gp) # 5fec <pxDelayedTaskList>
    43ba:	8654                	exec.it	#107     !lw	t1,-884(gp) # 5fe8 <pxCurrentTCB>
    43bc:	00430593          	addi	a1,t1,4
    43c0:	8460                	exec.it	#50     !jal	ra,3c94 <vListInsert>
    43c2:	cb01a383          	lw	t2,-848(gp) # 600c <xNextTaskUnblockTime>
    43c6:	fc7478e3          	bgeu	s0,t2,4396 <prvAddCurrentTaskToDelayedList+0x32>
    43ca:	ca81a823          	sw	s0,-848(gp) # 600c <xNextTaskUnblockTime>
    43ce:	b7e1                	c.j	4396 <prvAddCurrentTaskToDelayedList+0x32>

000043d0 <vTaskSuspendAll>:
    43d0:	ca418713          	addi	a4,gp,-860 # 6000 <uxSchedulerSuspended>
    43d4:	431c                	c.lw	a5,0(a4)
    43d6:	00178293          	addi	t0,a5,1
    43da:	8e34                	exec.it	#95     !sw	t0,0(a4)
    43dc:	8082                	c.jr	ra

000043de <xTaskIncrementTick>:
    43de:	8820                	exec.it	#20     !jal	t0,52b6 <__riscv_save_4>
    43e0:	8254                	exec.it	#105     !lw	a5,-860(gp) # 6000 <uxSchedulerSuspended>
    43e2:	0e079f63          	bnez	a5,44e0 <xTaskIncrementTick+0x102>
    43e6:	cbc1a483          	lw	s1,-836(gp) # 6018 <xTickCount>
    43ea:	0485                	c.addi	s1,1
    43ec:	ca91ae23          	sw	s1,-836(gp) # 6018 <xTickCount>
    43f0:	ec8d                	c.bnez	s1,442a <xTaskIncrementTick+0x4c>
    43f2:	c901a303          	lw	t1,-880(gp) # 5fec <pxDelayedTaskList>
    43f6:	00032383          	lw	t2,0(t1)
    43fa:	00038963          	beqz	t2,440c <xTaskIncrementTick+0x2e>
    43fe:	6585                	c.lui	a1,0x1
    4400:	a6258593          	addi	a1,a1,-1438 # a62 <setup+0x236>
    4404:	8424                	exec.it	#26     !auipc	a0,0x1
    4406:	65050513          	addi	a0,a0,1616 # 5a54 <_ITB_BASE_+0x398>
    440a:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    440c:	c901a403          	lw	s0,-880(gp) # 5fec <pxDelayedTaskList>
    4410:	c941a703          	lw	a4,-876(gp) # 5ff0 <pxOverflowDelayedTaskList>
    4414:	c8e1a823          	sw	a4,-880(gp) # 5fec <pxDelayedTaskList>
    4418:	c881aa23          	sw	s0,-876(gp) # 5ff0 <pxOverflowDelayedTaskList>
    441c:	cb41a503          	lw	a0,-844(gp) # 6010 <xNumOfOverflows>
    4420:	00150693          	addi	a3,a0,1
    4424:	cad1aa23          	sw	a3,-844(gp) # 6010 <xNumOfOverflows>
    4428:	3d25                	c.jal	4260 <prvResetNextTaskUnblockTime>
    442a:	cb01a603          	lw	a2,-848(gp) # 600c <xNextTaskUnblockTime>
    442e:	e3418993          	addi	s3,gp,-460 # 6190 <pxReadyTasksLists>
    4432:	4401                	c.li	s0,0
    4434:	02c4fb63          	bgeu	s1,a2,446a <xTaskIncrementTick+0x8c>
    4438:	8200                	exec.it	#64     !lw	a4,-884(gp) # 5fe8 <pxCurrentTCB>
    443a:	5748                	c.lw	a0,44(a4)
    443c:	46d1                	c.li	a3,20
    443e:	02d50633          	mul	a2,a0,a3
    4442:	4885                	c.li	a7,1
    4444:	99b2                	c.add	s3,a2
    4446:	0009a803          	lw	a6,0(s3)
    444a:	0108f363          	bgeu	a7,a6,4450 <xTaskIncrementTick+0x72>
    444e:	4405                	c.li	s0,1
    4450:	ca01ae03          	lw	t3,-864(gp) # 5ffc <uxPendedTicks>
    4454:	000e1463          	bnez	t3,445c <xTaskIncrementTick+0x7e>
    4458:	60b000ef          	jal	ra,5262 <vApplicationTickHook>
    445c:	cc01ae83          	lw	t4,-832(gp) # 601c <xYieldPending>
    4460:	000e8363          	beqz	t4,4466 <xTaskIncrementTick+0x88>
    4464:	4405                	c.li	s0,1
    4466:	8522                	c.mv	a0,s0
    4468:	8c34                	exec.it	#31     !j	52ea <__riscv_restore_4>
    446a:	4ad1                	c.li	s5,20
    446c:	c901a803          	lw	a6,-880(gp) # 5fec <pxDelayedTaskList>
    4470:	00082883          	lw	a7,0(a6)
    4474:	00089663          	bnez	a7,4480 <xTaskIncrementTick+0xa2>
    4478:	55fd                	c.li	a1,-1
    447a:	cab1a823          	sw	a1,-848(gp) # 600c <xNextTaskUnblockTime>
    447e:	bf6d                	c.j	4438 <xTaskIncrementTick+0x5a>
    4480:	c901a903          	lw	s2,-880(gp) # 5fec <pxDelayedTaskList>
    4484:	00c92e03          	lw	t3,12(s2)
    4488:	00ce2903          	lw	s2,12(t3)
    448c:	00492e83          	lw	t4,4(s2)
    4490:	01d4f563          	bgeu	s1,t4,449a <xTaskIncrementTick+0xbc>
    4494:	cbd1a823          	sw	t4,-848(gp) # 600c <xNextTaskUnblockTime>
    4498:	b745                	c.j	4438 <xTaskIncrementTick+0x5a>
    449a:	00490a13          	addi	s4,s2,4
    449e:	8552                	c.mv	a0,s4
    44a0:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    44a2:	02892f03          	lw	t5,40(s2)
    44a6:	000f0563          	beqz	t5,44b0 <xTaskIncrementTick+0xd2>
    44aa:	01890513          	addi	a0,s2,24
    44ae:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    44b0:	02c92f83          	lw	t6,44(s2)
    44b4:	cac1a783          	lw	a5,-852(gp) # 6008 <uxTopReadyPriority>
    44b8:	01f7f463          	bgeu	a5,t6,44c0 <xTaskIncrementTick+0xe2>
    44bc:	cbf1a623          	sw	t6,-852(gp) # 6008 <uxTopReadyPriority>
    44c0:	035f80b3          	mul	ra,t6,s5
    44c4:	85d2                	c.mv	a1,s4
    44c6:	00198533          	add	a0,s3,ra
    44ca:	8c24                	exec.it	#30     !jal	ra,3c72 <vListInsertEnd>
    44cc:	c8c1a283          	lw	t0,-884(gp) # 5fe8 <pxCurrentTCB>
    44d0:	02c92303          	lw	t1,44(s2)
    44d4:	02c2a383          	lw	t2,44(t0)
    44d8:	f8736ae3          	bltu	t1,t2,446c <xTaskIncrementTick+0x8e>
    44dc:	4405                	c.li	s0,1
    44de:	b779                	c.j	446c <xTaskIncrementTick+0x8e>
    44e0:	ca01a083          	lw	ra,-864(gp) # 5ffc <uxPendedTicks>
    44e4:	8864                	exec.it	#60     !addi	t0,ra,1
    44e6:	ca51a023          	sw	t0,-864(gp) # 5ffc <uxPendedTicks>
    44ea:	4401                	c.li	s0,0
    44ec:	577000ef          	jal	ra,5262 <vApplicationTickHook>
    44f0:	b7b5                	c.j	445c <xTaskIncrementTick+0x7e>

000044f2 <vTaskSwitchContext>:
    44f2:	8254                	exec.it	#105     !lw	a5,-860(gp) # 6000 <uxSchedulerSuspended>
    44f4:	c789                	c.beqz	a5,44fe <vTaskSwitchContext+0xc>
    44f6:	4e85                	c.li	t4,1
    44f8:	cdd1a023          	sw	t4,-832(gp) # 601c <xYieldPending>
    44fc:	8082                	c.jr	ra
    44fe:	8820                	exec.it	#20     !jal	t0,52b6 <__riscv_save_4>
    4500:	cc01a023          	sw	zero,-832(gp) # 601c <xYieldPending>
    4504:	e3418493          	addi	s1,gp,-460 # 6190 <pxReadyTasksLists>
    4508:	6985                	c.lui	s3,0x1
    450a:	cac1a403          	lw	s0,-852(gp) # 6008 <uxTopReadyPriority>
    450e:	4a51                	c.li	s4,20
    4510:	8926                	c.mv	s2,s1
    4512:	b8898993          	addi	s3,s3,-1144 # b88 <setup+0x35c>
    4516:	00001a97          	auipc	s5,0x1
    451a:	53ea8a93          	addi	s5,s5,1342 # 5a54 <_ITB_BASE_+0x398>
    451e:	03440733          	mul	a4,s0,s4
    4522:	00e482b3          	add	t0,s1,a4
    4526:	0002a683          	lw	a3,0(t0)
    452a:	ce8d                	c.beqz	a3,4564 <vTaskSwitchContext+0x72>
    452c:	0042a303          	lw	t1,4(t0)
    4530:	00870513          	addi	a0,a4,8
    4534:	00432383          	lw	t2,4(t1)
    4538:	94aa                	c.add	s1,a0
    453a:	0072a223          	sw	t2,4(t0)
    453e:	00939663          	bne	t2,s1,454a <vTaskSwitchContext+0x58>
    4542:	0043a583          	lw	a1,4(t2)
    4546:	00b2a223          	sw	a1,4(t0)
    454a:	4651                	c.li	a2,20
    454c:	02c40833          	mul	a6,s0,a2
    4550:	9942                	c.add	s2,a6
    4552:	00492883          	lw	a7,4(s2)
    4556:	00c8ae03          	lw	t3,12(a7)
    455a:	c9c1a623          	sw	t3,-884(gp) # 5fe8 <pxCurrentTCB>
    455e:	ca81a623          	sw	s0,-852(gp) # 6008 <uxTopReadyPriority>
    4562:	8c34                	exec.it	#31     !j	52ea <__riscv_restore_4>
    4564:	e401                	c.bnez	s0,456c <vTaskSwitchContext+0x7a>
    4566:	85ce                	c.mv	a1,s3
    4568:	8556                	c.mv	a0,s5
    456a:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    456c:	147d                	c.addi	s0,-1
    456e:	bf45                	c.j	451e <vTaskSwitchContext+0x2c>

00004570 <vTaskPlaceOnEventList>:
    4570:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    4572:	842a                	c.mv	s0,a0
    4574:	84ae                	c.mv	s1,a1
    4576:	e901                	c.bnez	a0,4586 <vTaskPlaceOnEventList+0x16>
    4578:	6585                	c.lui	a1,0x1
    457a:	b9f58593          	addi	a1,a1,-1121 # b9f <setup+0x373>
    457e:	8424                	exec.it	#26     !auipc	a0,0x1
    4580:	4d650513          	addi	a0,a0,1238 # 5a54 <_ITB_BASE_+0x398>
    4584:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4586:	8a44                	exec.it	#108     !lw	ra,-884(gp) # 5fe8 <pxCurrentTCB>
    4588:	01808593          	addi	a1,ra,24
    458c:	8522                	c.mv	a0,s0
    458e:	8460                	exec.it	#50     !jal	ra,3c94 <vListInsert>
    4590:	4585                	c.li	a1,1
    4592:	8526                	c.mv	a0,s1
    4594:	3bc1                	c.jal	4364 <prvAddCurrentTaskToDelayedList>
    4596:	8c14                	exec.it	#15     !j	52f4 <__riscv_restore_0>

00004598 <vTaskPlaceOnEventListRestricted>:
    4598:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    459a:	892a                	c.mv	s2,a0
    459c:	842e                	c.mv	s0,a1
    459e:	84b2                	c.mv	s1,a2
    45a0:	e901                	c.bnez	a0,45b0 <vTaskPlaceOnEventListRestricted+0x18>
    45a2:	6585                	c.lui	a1,0x1
    45a4:	bca58593          	addi	a1,a1,-1078 # bca <setup+0x39e>
    45a8:	8424                	exec.it	#26     !auipc	a0,0x1
    45aa:	4ac50513          	addi	a0,a0,1196 # 5a54 <_ITB_BASE_+0x398>
    45ae:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    45b0:	8a44                	exec.it	#108     !lw	ra,-884(gp) # 5fe8 <pxCurrentTCB>
    45b2:	01808593          	addi	a1,ra,24
    45b6:	854a                	c.mv	a0,s2
    45b8:	8c24                	exec.it	#30     !jal	ra,3c72 <vListInsertEnd>
    45ba:	c091                	c.beqz	s1,45be <vTaskPlaceOnEventListRestricted+0x26>
    45bc:	547d                	c.li	s0,-1
    45be:	85a6                	c.mv	a1,s1
    45c0:	8522                	c.mv	a0,s0
    45c2:	334d                	c.jal	4364 <prvAddCurrentTaskToDelayedList>
    45c4:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>

000045c6 <xTaskRemoveFromEventList>:
    45c6:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
    45c8:	455c                	c.lw	a5,12(a0)
    45ca:	1141                	c.addi	sp,-16
    45cc:	47c0                	c.lw	s0,12(a5)
    45ce:	e801                	c.bnez	s0,45de <xTaskRemoveFromEventList+0x18>
    45d0:	6585                	c.lui	a1,0x1
    45d2:	bfa58593          	addi	a1,a1,-1030 # bfa <setup+0x3ce>
    45d6:	8424                	exec.it	#26     !auipc	a0,0x1
    45d8:	47e50513          	addi	a0,a0,1150 # 5a54 <_ITB_BASE_+0x398>
    45dc:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    45de:	01840093          	addi	ra,s0,24
    45e2:	8506                	c.mv	a0,ra
    45e4:	c606                	c.swsp	ra,12(sp)
    45e6:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    45e8:	ca41a283          	lw	t0,-860(gp) # 6000 <uxSchedulerSuspended>
    45ec:	45b2                	c.lwsp	a1,12(sp)
    45ee:	ee818513          	addi	a0,gp,-280 # 6244 <xPendingReadyList>
    45f2:	02029663          	bnez	t0,461e <xTaskRemoveFromEventList+0x58>
    45f6:	00440313          	addi	t1,s0,4
    45fa:	851a                	c.mv	a0,t1
    45fc:	c61a                	c.swsp	t1,12(sp)
    45fe:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    4600:	5448                	c.lw	a0,44(s0)
    4602:	cac1a383          	lw	t2,-852(gp) # 6008 <uxTopReadyPriority>
    4606:	45b2                	c.lwsp	a1,12(sp)
    4608:	00a3f463          	bgeu	t2,a0,4610 <xTaskRemoveFromEventList+0x4a>
    460c:	caa1a623          	sw	a0,-852(gp) # 6008 <uxTopReadyPriority>
    4610:	4651                	c.li	a2,20
    4612:	02c506b3          	mul	a3,a0,a2
    4616:	e3418713          	addi	a4,gp,-460 # 6190 <pxReadyTasksLists>
    461a:	00d70533          	add	a0,a4,a3
    461e:	8c24                	exec.it	#30     !jal	ra,3c72 <vListInsertEnd>
    4620:	c8c1a803          	lw	a6,-884(gp) # 5fe8 <pxCurrentTCB>
    4624:	02c42883          	lw	a7,44(s0)
    4628:	02c82e03          	lw	t3,44(a6)
    462c:	4501                	c.li	a0,0
    462e:	011e7663          	bgeu	t3,a7,463a <xTaskRemoveFromEventList+0x74>
    4632:	4e85                	c.li	t4,1
    4634:	cdd1a023          	sw	t4,-832(gp) # 601c <xYieldPending>
    4638:	4505                	c.li	a0,1
    463a:	0141                	c.addi	sp,16
    463c:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>

0000463e <vTaskInternalSetTimeOutState>:
    463e:	cb41a783          	lw	a5,-844(gp) # 6010 <xNumOfOverflows>
    4642:	cbc1a283          	lw	t0,-836(gp) # 6018 <xTickCount>
    4646:	c11c                	c.sw	a5,0(a0)
    4648:	00552223          	sw	t0,4(a0)
    464c:	8082                	c.jr	ra

0000464e <vTaskMissedYield>:
    464e:	4785                	c.li	a5,1
    4650:	ccf1a023          	sw	a5,-832(gp) # 601c <xYieldPending>
    4654:	8082                	c.jr	ra

00004656 <xTaskGetSchedulerState>:
    4656:	8244                	exec.it	#104     !lw	a5,-840(gp) # 6014 <xSchedulerRunning>
    4658:	4505                	c.li	a0,1
    465a:	c799                	c.beqz	a5,4668 <xTaskGetSchedulerState+0x12>
    465c:	ca41a503          	lw	a0,-860(gp) # 6000 <uxSchedulerSuspended>
    4660:	00153293          	seqz	t0,a0
    4664:	00129513          	slli	a0,t0,0x1
    4668:	8082                	c.jr	ra

0000466a <xTaskPriorityDisinherit>:
    466a:	e511                	c.bnez	a0,4676 <xTaskPriorityDisinherit+0xc>
    466c:	4501                	c.li	a0,0
    466e:	8082                	c.jr	ra
    4670:	4501                	c.li	a0,0
    4672:	0141                	c.addi	sp,16
    4674:	8404                	exec.it	#10     !j	52f4 <__riscv_restore_0>
    4676:	8004                	exec.it	#8     !jal	t0,52d0 <__riscv_save_0>
    4678:	8070                	exec.it	#49     !lw	a5,-884(gp) # 5fe8 <pxCurrentTCB>
    467a:	1141                	c.addi	sp,-16
    467c:	842a                	c.mv	s0,a0
    467e:	00a78963          	beq	a5,a0,4690 <xTaskPriorityDisinherit+0x26>
    4682:	6585                	c.lui	a1,0x1
    4684:	f7a58593          	addi	a1,a1,-134 # f7a <mscAppInit+0xe2>
    4688:	8424                	exec.it	#26     !auipc	a0,0x1
    468a:	3cc50513          	addi	a0,a0,972 # 5a54 <_ITB_BASE_+0x398>
    468e:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4690:	05442283          	lw	t0,84(s0)
    4694:	00029963          	bnez	t0,46a6 <xTaskPriorityDisinherit+0x3c>
    4698:	6085                	c.lui	ra,0x1
    469a:	f7b08593          	addi	a1,ra,-133 # f7b <mscAppInit+0xe3>
    469e:	8424                	exec.it	#26     !auipc	a0,0x1
    46a0:	3b650513          	addi	a0,a0,950 # 5a54 <_ITB_BASE_+0x398>
    46a4:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    46a6:	05442303          	lw	t1,84(s0)
    46aa:	5454                	c.lw	a3,44(s0)
    46ac:	8604                	exec.it	#74     !addi	t2,t1,-1
    46ae:	4838                	c.lw	a4,80(s0)
    46b0:	04742a23          	sw	t2,84(s0)
    46b4:	fae68ee3          	beq	a3,a4,4670 <xTaskPriorityDisinherit+0x6>
    46b8:	fa039ce3          	bnez	t2,4670 <xTaskPriorityDisinherit+0x6>
    46bc:	00440613          	addi	a2,s0,4
    46c0:	8532                	c.mv	a0,a2
    46c2:	c632                	c.swsp	a2,12(sp)
    46c4:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    46c6:	4828                	c.lw	a0,80(s0)
    46c8:	481d                	c.li	a6,7
    46ca:	40a808b3          	sub	a7,a6,a0
    46ce:	d448                	c.sw	a0,44(s0)
    46d0:	01142c23          	sw	a7,24(s0)
    46d4:	cac1ae03          	lw	t3,-852(gp) # 6008 <uxTopReadyPriority>
    46d8:	45b2                	c.lwsp	a1,12(sp)
    46da:	00ae7463          	bgeu	t3,a0,46e2 <xTaskPriorityDisinherit+0x78>
    46de:	caa1a623          	sw	a0,-852(gp) # 6008 <uxTopReadyPriority>
    46e2:	4ed1                	c.li	t4,20
    46e4:	03d50f33          	mul	t5,a0,t4
    46e8:	e3418f93          	addi	t6,gp,-460 # 6190 <pxReadyTasksLists>
    46ec:	01ef8533          	add	a0,t6,t5
    46f0:	8c24                	exec.it	#30     !jal	ra,3c72 <vListInsertEnd>
    46f2:	4505                	c.li	a0,1
    46f4:	bfbd                	c.j	4672 <xTaskPriorityDisinherit+0x8>

000046f6 <vTaskEnterCritical>:
    46f6:	8870                	exec.it	#53     !csrci	mstatus,8
    46f8:	8244                	exec.it	#104     !lw	a5,-840(gp) # 6014 <xSchedulerRunning>
    46fa:	cb89                	c.beqz	a5,470c <vTaskEnterCritical+0x16>
    46fc:	8200                	exec.it	#64     !lw	a4,-884(gp) # 5fe8 <pxCurrentTCB>
    46fe:	04472283          	lw	t0,68(a4)
    4702:	8070                	exec.it	#49     !lw	a5,-884(gp) # 5fe8 <pxCurrentTCB>
    4704:	00128313          	addi	t1,t0,1
    4708:	04672223          	sw	t1,68(a4)
    470c:	8082                	c.jr	ra

0000470e <vTaskExitCritical>:
    470e:	8244                	exec.it	#104     !lw	a5,-840(gp) # 6014 <xSchedulerRunning>
    4710:	c395                	c.beqz	a5,4734 <vTaskExitCritical+0x26>
    4712:	c8c1a283          	lw	t0,-884(gp) # 5fe8 <pxCurrentTCB>
    4716:	0442a303          	lw	t1,68(t0)
    471a:	00030d63          	beqz	t1,4734 <vTaskExitCritical+0x26>
    471e:	8200                	exec.it	#64     !lw	a4,-884(gp) # 5fe8 <pxCurrentTCB>
    4720:	04472383          	lw	t2,68(a4)
    4724:	c8c1a583          	lw	a1,-884(gp) # 5fe8 <pxCurrentTCB>
    4728:	8214                	exec.it	#73     !addi	a0,t2,-1
    472a:	c368                	c.sw	a0,68(a4)
    472c:	41f0                	c.lw	a2,68(a1)
    472e:	e219                	c.bnez	a2,4734 <vTaskExitCritical+0x26>
    4730:	30046073          	csrsi	mstatus,8
    4734:	8082                	c.jr	ra

00004736 <prvAddNewTaskToReadyList>:
    4736:	8830                	exec.it	#21     !jal	t0,52b6 <__riscv_save_4>
    4738:	842a                	c.mv	s0,a0
    473a:	3f75                	c.jal	46f6 <vTaskEnterCritical>
    473c:	c981a783          	lw	a5,-872(gp) # 5ff4 <uxCurrentNumberOfTasks>
    4740:	00178293          	addi	t0,a5,1
    4744:	c851ac23          	sw	t0,-872(gp) # 5ff4 <uxCurrentNumberOfTasks>
    4748:	8654                	exec.it	#107     !lw	t1,-884(gp) # 5fe8 <pxCurrentTCB>
    474a:	e3418913          	addi	s2,gp,-460 # 6190 <pxReadyTasksLists>
    474e:	08031d63          	bnez	t1,47e8 <prvAddNewTaskToReadyList+0xb2>
    4752:	c881a623          	sw	s0,-884(gp) # 5fe8 <pxCurrentTCB>
    4756:	c981a583          	lw	a1,-872(gp) # 5ff4 <uxCurrentNumberOfTasks>
    475a:	0415e15b          	bnec	a1,1,479c <prvAddNewTaskToReadyList+0x66>
    475e:	4481                	c.li	s1,0
    4760:	49d1                	c.li	s3,20
    4762:	03348633          	mul	a2,s1,s3
    4766:	0485                	c.addi	s1,1
    4768:	00c90533          	add	a0,s2,a2
    476c:	8834                	exec.it	#29     !jal	ra,3c58 <vListInitialise>
    476e:	be74ea5b          	bnec	s1,7,4762 <prvAddNewTaskToReadyList+0x2c>
    4772:	ec018993          	addi	s3,gp,-320 # 621c <xDelayedTaskList1>
    4776:	854e                	c.mv	a0,s3
    4778:	8834                	exec.it	#29     !jal	ra,3c58 <vListInitialise>
    477a:	ed418493          	addi	s1,gp,-300 # 6230 <xDelayedTaskList2>
    477e:	8526                	c.mv	a0,s1
    4780:	8834                	exec.it	#29     !jal	ra,3c58 <vListInitialise>
    4782:	ee818513          	addi	a0,gp,-280 # 6244 <xPendingReadyList>
    4786:	8834                	exec.it	#29     !jal	ra,3c58 <vListInitialise>
    4788:	f1018513          	addi	a0,gp,-240 # 626c <xTasksWaitingTermination>
    478c:	8834                	exec.it	#29     !jal	ra,3c58 <vListInitialise>
    478e:	efc18513          	addi	a0,gp,-260 # 6258 <xSuspendedTaskList>
    4792:	8834                	exec.it	#29     !jal	ra,3c58 <vListInitialise>
    4794:	c931a823          	sw	s3,-880(gp) # 5fec <pxDelayedTaskList>
    4798:	c891aa23          	sw	s1,-876(gp) # 5ff0 <pxOverflowDelayedTaskList>
    479c:	ca81a683          	lw	a3,-856(gp) # 6004 <uxTaskNumber>
    47a0:	00168813          	addi	a6,a3,1
    47a4:	02c42883          	lw	a7,44(s0)
    47a8:	cb01a423          	sw	a6,-856(gp) # 6004 <uxTaskNumber>
    47ac:	05042423          	sw	a6,72(s0)
    47b0:	cac1ae03          	lw	t3,-852(gp) # 6008 <uxTopReadyPriority>
    47b4:	011e7463          	bgeu	t3,a7,47bc <prvAddNewTaskToReadyList+0x86>
    47b8:	cb11a623          	sw	a7,-852(gp) # 6008 <uxTopReadyPriority>
    47bc:	4ed1                	c.li	t4,20
    47be:	03d88f33          	mul	t5,a7,t4
    47c2:	00440593          	addi	a1,s0,4
    47c6:	01e90533          	add	a0,s2,t5
    47ca:	8c24                	exec.it	#30     !jal	ra,3c72 <vListInsertEnd>
    47cc:	3789                	c.jal	470e <vTaskExitCritical>
    47ce:	cb81af83          	lw	t6,-840(gp) # 6014 <xSchedulerRunning>
    47d2:	000f8a63          	beqz	t6,47e6 <prvAddNewTaskToReadyList+0xb0>
    47d6:	8070                	exec.it	#49     !lw	a5,-884(gp) # 5fe8 <pxCurrentTCB>
    47d8:	02c7a283          	lw	t0,44(a5)
    47dc:	02c42303          	lw	t1,44(s0)
    47e0:	0062f363          	bgeu	t0,t1,47e6 <prvAddNewTaskToReadyList+0xb0>
    47e4:	8c30                	exec.it	#23     !jal	ra,5154 <vPortYield>
    47e6:	8024                	exec.it	#24     !j	52ea <__riscv_restore_4>
    47e8:	cb81a083          	lw	ra,-840(gp) # 6014 <xSchedulerRunning>
    47ec:	fa0098e3          	bnez	ra,479c <prvAddNewTaskToReadyList+0x66>
    47f0:	c8c1a383          	lw	t2,-884(gp) # 5fe8 <pxCurrentTCB>
    47f4:	02c3a703          	lw	a4,44(t2)
    47f8:	5448                	c.lw	a0,44(s0)
    47fa:	fae561e3          	bltu	a0,a4,479c <prvAddNewTaskToReadyList+0x66>
    47fe:	c881a623          	sw	s0,-884(gp) # 5fe8 <pxCurrentTCB>
    4802:	bf69                	c.j	479c <prvAddNewTaskToReadyList+0x66>

00004804 <xTaskCreateStatic>:
    4804:	8040                	exec.it	#32     !jal	t0,52b6 <__riscv_save_4>
    4806:	1141                	c.addi	sp,-16
    4808:	892a                	c.mv	s2,a0
    480a:	89ae                	c.mv	s3,a1
    480c:	8a32                	c.mv	s4,a2
    480e:	8ab6                	c.mv	s5,a3
    4810:	8b3a                	c.mv	s6,a4
    4812:	84be                	c.mv	s1,a5
    4814:	8442                	c.mv	s0,a6
    4816:	e799                	c.bnez	a5,4824 <xTaskCreateStatic+0x20>
    4818:	25e00593          	li	a1,606
    481c:	8424                	exec.it	#26     !auipc	a0,0x1
    481e:	23850513          	addi	a0,a0,568 # 5a54 <_ITB_BASE_+0x398>
    4822:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4824:	e419                	c.bnez	s0,4832 <xTaskCreateStatic+0x2e>
    4826:	25f00593          	li	a1,607
    482a:	8424                	exec.it	#26     !auipc	a0,0x1
    482c:	22a50513          	addi	a0,a0,554 # 5a54 <_ITB_BASE_+0x398>
    4830:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4832:	06000793          	li	a5,96
    4836:	c43e                	c.swsp	a5,8(sp)
    4838:	42a2                	c.lwsp	t0,8(sp)
    483a:	4002d8db          	beqc	t0,96,484a <xTaskCreateStatic+0x46>
    483e:	26700593          	li	a1,615
    4842:	8424                	exec.it	#26     !auipc	a0,0x1
    4844:	21250513          	addi	a0,a0,530 # 5a54 <_ITB_BASE_+0x398>
    4848:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    484a:	47a2                	c.lwsp	a5,8(sp)
    484c:	4501                	c.li	a0,0
    484e:	c015                	c.beqz	s0,4872 <xTaskCreateStatic+0x6e>
    4850:	c08d                	c.beqz	s1,4872 <xTaskCreateStatic+0x6e>
    4852:	4089                	c.li	ra,2
    4854:	8822                	c.mv	a6,s0
    4856:	007c                	c.addi4spn	a5,sp,12
    4858:	875a                	c.mv	a4,s6
    485a:	86d6                	c.mv	a3,s5
    485c:	8652                	c.mv	a2,s4
    485e:	85ce                	c.mv	a1,s3
    4860:	854a                	c.mv	a0,s2
    4862:	04140ea3          	sb	ra,93(s0)
    4866:	d804                	c.sw	s1,48(s0)
    4868:	a21ff0ef          	jal	ra,4288 <prvInitialiseNewTask.isra.2>
    486c:	8522                	c.mv	a0,s0
    486e:	35e1                	c.jal	4736 <prvAddNewTaskToReadyList>
    4870:	4532                	c.lwsp	a0,12(sp)
    4872:	0141                	c.addi	sp,16
    4874:	8050                	exec.it	#33     !j	52ea <__riscv_restore_4>

00004876 <vTaskStartScheduler>:
    4876:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    4878:	1141                	c.addi	sp,-16
    487a:	0070                	c.addi4spn	a2,sp,12
    487c:	002c                	c.addi4spn	a1,sp,8
    487e:	0048                	c.addi4spn	a0,sp,4
    4880:	c202                	c.swsp	zero,4(sp)
    4882:	c402                	c.swsp	zero,8(sp)
    4884:	1e1000ef          	jal	ra,5264 <vApplicationGetIdleTaskMemory>
    4888:	4812                	c.lwsp	a6,4(sp)
    488a:	47a2                	c.lwsp	a5,8(sp)
    488c:	4632                	c.lwsp	a2,12(sp)
    488e:	4701                	c.li	a4,0
    4890:	4681                	c.li	a3,0
    4892:	00001597          	auipc	a1,0x1
    4896:	1ee58593          	addi	a1,a1,494 # 5a80 <_ITB_BASE_+0x3c4>
    489a:	00000517          	auipc	a0,0x0
    489e:	10a50513          	addi	a0,a0,266 # 49a4 <prvIdleTask>
    48a2:	378d                	c.jal	4804 <xTaskCreateStatic>
    48a4:	cd11                	c.beqz	a0,48c0 <vTaskStartScheduler+0x4a>
    48a6:	261d                	c.jal	4bcc <xTimerCreateTimerTask>
    48a8:	00156e5b          	bnec	a0,1,48c4 <vTaskStartScheduler+0x4e>
    48ac:	8870                	exec.it	#53     !csrci	mstatus,8
    48ae:	50fd                	c.li	ra,-1
    48b0:	ca11a823          	sw	ra,-848(gp) # 600c <xNextTaskUnblockTime>
    48b4:	caa1ac23          	sw	a0,-840(gp) # 6014 <xSchedulerRunning>
    48b8:	ca01ae23          	sw	zero,-836(gp) # 6018 <xTickCount>
    48bc:	03b000ef          	jal	ra,50f6 <xPortStartScheduler>
    48c0:	0141                	c.addi	sp,16
    48c2:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
    48c4:	57fd                	c.li	a5,-1
    48c6:	fef51de3          	bne	a0,a5,48c0 <vTaskStartScheduler+0x4a>
    48ca:	6585                	c.lui	a1,0x1
    48cc:	80458593          	addi	a1,a1,-2044 # 804 <EDMA_EP0_write_IN_fifiodata+0x20>
    48d0:	8424                	exec.it	#26     !auipc	a0,0x1
    48d2:	18450513          	addi	a0,a0,388 # 5a54 <_ITB_BASE_+0x398>
    48d6:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    48d8:	b7e5                	c.j	48c0 <vTaskStartScheduler+0x4a>

000048da <xTaskCreate>:
    48da:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    48dc:	1101                	c.addi	sp,-32
    48de:	892a                	c.mv	s2,a0
    48e0:	00261513          	slli	a0,a2,0x2
    48e4:	ce2e                	c.swsp	a1,28(sp)
    48e6:	cc36                	c.swsp	a3,24(sp)
    48e8:	ca3a                	c.swsp	a4,20(sp)
    48ea:	c83e                	c.swsp	a5,16(sp)
    48ec:	c632                	c.swsp	a2,12(sp)
    48ee:	8454                	exec.it	#43     !jal	ra,4e7a <pvPortMalloc>
    48f0:	c915                	c.beqz	a0,4924 <xTaskCreate+0x4a>
    48f2:	84aa                	c.mv	s1,a0
    48f4:	06000513          	li	a0,96
    48f8:	8454                	exec.it	#43     !jal	ra,4e7a <pvPortMalloc>
    48fa:	842a                	c.mv	s0,a0
    48fc:	c115                	c.beqz	a0,4920 <xTaskCreate+0x46>
    48fe:	47c2                	c.lwsp	a5,16(sp)
    4900:	4752                	c.lwsp	a4,20(sp)
    4902:	46e2                	c.lwsp	a3,24(sp)
    4904:	4632                	c.lwsp	a2,12(sp)
    4906:	45f2                	c.lwsp	a1,28(sp)
    4908:	882a                	c.mv	a6,a0
    490a:	d904                	c.sw	s1,48(a0)
    490c:	04050ea3          	sb	zero,93(a0)
    4910:	854a                	c.mv	a0,s2
    4912:	977ff0ef          	jal	ra,4288 <prvInitialiseNewTask.isra.2>
    4916:	8522                	c.mv	a0,s0
    4918:	3d39                	c.jal	4736 <prvAddNewTaskToReadyList>
    491a:	4505                	c.li	a0,1
    491c:	6105                	c.addi16sp	sp,32
    491e:	8c00                	exec.it	#6     !j	52f4 <__riscv_restore_0>
    4920:	8526                	c.mv	a0,s1
    4922:	8444                	exec.it	#42     !jal	ra,4f5e <vPortFree>
    4924:	557d                	c.li	a0,-1
    4926:	bfdd                	c.j	491c <xTaskCreate+0x42>

00004928 <vTaskDelete>:
    4928:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    492a:	842a                	c.mv	s0,a0
    492c:	33e9                	c.jal	46f6 <vTaskEnterCritical>
    492e:	e019                	c.bnez	s0,4934 <vTaskDelete+0xc>
    4930:	c8c1a403          	lw	s0,-884(gp) # 5fe8 <pxCurrentTCB>
    4934:	00440493          	addi	s1,s0,4
    4938:	8526                	c.mv	a0,s1
    493a:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    493c:	541c                	c.lw	a5,40(s0)
    493e:	c399                	c.beqz	a5,4944 <vTaskDelete+0x1c>
    4940:	8634                	exec.it	#91     !addi	a0,s0,24
    4942:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    4944:	ca81a083          	lw	ra,-856(gp) # 6004 <uxTaskNumber>
    4948:	8864                	exec.it	#60     !addi	t0,ra,1
    494a:	ca51a423          	sw	t0,-856(gp) # 6004 <uxTaskNumber>
    494e:	8654                	exec.it	#107     !lw	t1,-884(gp) # 5fe8 <pxCurrentTCB>
    4950:	02831f63          	bne	t1,s0,498e <vTaskDelete+0x66>
    4954:	85a6                	c.mv	a1,s1
    4956:	f1018513          	addi	a0,gp,-240 # 626c <xTasksWaitingTermination>
    495a:	8c24                	exec.it	#30     !jal	ra,3c72 <vListInsertEnd>
    495c:	c9c1a583          	lw	a1,-868(gp) # 5ff8 <uxDeletedTasksWaitingCleanUp>
    4960:	00158613          	addi	a2,a1,1
    4964:	c8c1ae23          	sw	a2,-868(gp) # 5ff8 <uxDeletedTasksWaitingCleanUp>
    4968:	335d                	c.jal	470e <vTaskExitCritical>
    496a:	cb81a683          	lw	a3,-840(gp) # 6014 <xSchedulerRunning>
    496e:	ce99                	c.beqz	a3,498c <vTaskDelete+0x64>
    4970:	8200                	exec.it	#64     !lw	a4,-884(gp) # 5fe8 <pxCurrentTCB>
    4972:	00871d63          	bne	a4,s0,498c <vTaskDelete+0x64>
    4976:	ca41a803          	lw	a6,-860(gp) # 6000 <uxSchedulerSuspended>
    497a:	00080863          	beqz	a6,498a <vTaskDelete+0x62>
    497e:	4b600593          	li	a1,1206
    4982:	8424                	exec.it	#26     !auipc	a0,0x1
    4984:	0d250513          	addi	a0,a0,210 # 5a54 <_ITB_BASE_+0x398>
    4988:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    498a:	8c30                	exec.it	#23     !jal	ra,5154 <vPortYield>
    498c:	8c14                	exec.it	#15     !j	52f4 <__riscv_restore_0>
    498e:	c981a383          	lw	t2,-872(gp) # 5ff4 <uxCurrentNumberOfTasks>
    4992:	8214                	exec.it	#73     !addi	a0,t2,-1
    4994:	c8a1ac23          	sw	a0,-872(gp) # 5ff4 <uxCurrentNumberOfTasks>
    4998:	8522                	c.mv	a0,s0
    499a:	99fff0ef          	jal	ra,4338 <prvDeleteTCB>
    499e:	8c3ff0ef          	jal	ra,4260 <prvResetNextTaskUnblockTime>
    49a2:	b7d9                	c.j	4968 <vTaskDelete+0x40>

000049a4 <prvIdleTask>:
    49a4:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    49a6:	4485                	c.li	s1,1
    49a8:	c9c1a783          	lw	a5,-868(gp) # 5ff8 <uxDeletedTasksWaitingCleanUp>
    49ac:	eb89                	c.bnez	a5,49be <prvIdleTask+0x1a>
    49ae:	e341a083          	lw	ra,-460(gp) # 6190 <pxReadyTasksLists>
    49b2:	0014f363          	bgeu	s1,ra,49b8 <prvIdleTask+0x14>
    49b6:	8c30                	exec.it	#23     !jal	ra,5154 <vPortYield>
    49b8:	09d000ef          	jal	ra,5254 <vApplicationIdleHook>
    49bc:	b7f5                	c.j	49a8 <prvIdleTask+0x4>
    49be:	3b25                	c.jal	46f6 <vTaskEnterCritical>
    49c0:	f1c1a283          	lw	t0,-228(gp) # 6278 <xTasksWaitingTermination+0xc>
    49c4:	00c2a403          	lw	s0,12(t0)
    49c8:	8250                	exec.it	#97     !addi	a0,s0,4
    49ca:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    49cc:	c981a303          	lw	t1,-872(gp) # 5ff4 <uxCurrentNumberOfTasks>
    49d0:	8604                	exec.it	#74     !addi	t2,t1,-1
    49d2:	c871ac23          	sw	t2,-872(gp) # 5ff4 <uxCurrentNumberOfTasks>
    49d6:	c9c1a503          	lw	a0,-868(gp) # 5ff8 <uxDeletedTasksWaitingCleanUp>
    49da:	8470                	exec.it	#51     !addi	a1,a0,-1
    49dc:	c8b1ae23          	sw	a1,-868(gp) # 5ff8 <uxDeletedTasksWaitingCleanUp>
    49e0:	333d                	c.jal	470e <vTaskExitCritical>
    49e2:	8522                	c.mv	a0,s0
    49e4:	955ff0ef          	jal	ra,4338 <prvDeleteTCB>
    49e8:	b7c1                	c.j	49a8 <prvIdleTask+0x4>

000049ea <xTaskResumeAll>:
    49ea:	8830                	exec.it	#21     !jal	t0,52b6 <__riscv_save_4>
    49ec:	8254                	exec.it	#105     !lw	a5,-860(gp) # 6000 <uxSchedulerSuspended>
    49ee:	1141                	c.addi	sp,-16
    49f0:	eb81                	c.bnez	a5,4a00 <xTaskResumeAll+0x16>
    49f2:	6585                	c.lui	a1,0x1
    49f4:	86858593          	addi	a1,a1,-1944 # 868 <setup+0x3c>
    49f8:	8424                	exec.it	#26     !auipc	a0,0x1
    49fa:	05c50513          	addi	a0,a0,92 # 5a54 <_ITB_BASE_+0x398>
    49fe:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4a00:	39dd                	c.jal	46f6 <vTaskEnterCritical>
    4a02:	ca41a283          	lw	t0,-860(gp) # 6000 <uxSchedulerSuspended>
    4a06:	fff28313          	addi	t1,t0,-1
    4a0a:	ca61a223          	sw	t1,-860(gp) # 6000 <uxSchedulerSuspended>
    4a0e:	ca41a383          	lw	t2,-860(gp) # 6000 <uxSchedulerSuspended>
    4a12:	00038863          	beqz	t2,4a22 <xTaskResumeAll+0x38>
    4a16:	4681                	c.li	a3,0
    4a18:	c636                	c.swsp	a3,12(sp)
    4a1a:	39d5                	c.jal	470e <vTaskExitCritical>
    4a1c:	4532                	c.lwsp	a0,12(sp)
    4a1e:	0141                	c.addi	sp,16
    4a20:	8024                	exec.it	#24     !j	52ea <__riscv_restore_4>
    4a22:	c981a083          	lw	ra,-872(gp) # 5ff4 <uxCurrentNumberOfTasks>
    4a26:	4401                	c.li	s0,0
    4a28:	fe0087e3          	beqz	ra,4a16 <xTaskResumeAll+0x2c>
    4a2c:	e3418493          	addi	s1,gp,-460 # 6190 <pxReadyTasksLists>
    4a30:	4951                	c.li	s2,20
    4a32:	4985                	c.li	s3,1
    4a34:	ee81a503          	lw	a0,-280(gp) # 6244 <xPendingReadyList>
    4a38:	e51d                	c.bnez	a0,4a66 <xTaskResumeAll+0x7c>
    4a3a:	c019                	c.beqz	s0,4a40 <xTaskResumeAll+0x56>
    4a3c:	825ff0ef          	jal	ra,4260 <prvResetNextTaskUnblockTime>
    4a40:	ca01a403          	lw	s0,-864(gp) # 5ffc <uxPendedTicks>
    4a44:	c819                	c.beqz	s0,4a5a <xTaskResumeAll+0x70>
    4a46:	4485                	c.li	s1,1
    4a48:	997ff0ef          	jal	ra,43de <xTaskIncrementTick>
    4a4c:	c119                	c.beqz	a0,4a52 <xTaskResumeAll+0x68>
    4a4e:	cc91a023          	sw	s1,-832(gp) # 601c <xYieldPending>
    4a52:	147d                	c.addi	s0,-1
    4a54:	f875                	c.bnez	s0,4a48 <xTaskResumeAll+0x5e>
    4a56:	ca01a023          	sw	zero,-864(gp) # 5ffc <uxPendedTicks>
    4a5a:	cc01a603          	lw	a2,-832(gp) # 601c <xYieldPending>
    4a5e:	de45                	c.beqz	a2,4a16 <xTaskResumeAll+0x2c>
    4a60:	8c30                	exec.it	#23     !jal	ra,5154 <vPortYield>
    4a62:	4685                	c.li	a3,1
    4a64:	bf55                	c.j	4a18 <xTaskResumeAll+0x2e>
    4a66:	ef41a403          	lw	s0,-268(gp) # 6250 <xPendingReadyList+0xc>
    4a6a:	4440                	c.lw	s0,12(s0)
    4a6c:	8634                	exec.it	#91     !addi	a0,s0,24
    4a6e:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    4a70:	00440713          	addi	a4,s0,4
    4a74:	853a                	c.mv	a0,a4
    4a76:	c63a                	c.swsp	a4,12(sp)
    4a78:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    4a7a:	02c42803          	lw	a6,44(s0)
    4a7e:	cac1a883          	lw	a7,-852(gp) # 6008 <uxTopReadyPriority>
    4a82:	45b2                	c.lwsp	a1,12(sp)
    4a84:	0108f463          	bgeu	a7,a6,4a8c <xTaskResumeAll+0xa2>
    4a88:	cb01a623          	sw	a6,-852(gp) # 6008 <uxTopReadyPriority>
    4a8c:	03280e33          	mul	t3,a6,s2
    4a90:	01c48533          	add	a0,s1,t3
    4a94:	8c24                	exec.it	#30     !jal	ra,3c72 <vListInsertEnd>
    4a96:	c8c1ae83          	lw	t4,-884(gp) # 5fe8 <pxCurrentTCB>
    4a9a:	02c42f03          	lw	t5,44(s0)
    4a9e:	02ceaf83          	lw	t6,44(t4)
    4aa2:	f9ff69e3          	bltu	t5,t6,4a34 <xTaskResumeAll+0x4a>
    4aa6:	cd31a023          	sw	s3,-832(gp) # 601c <xYieldPending>
    4aaa:	b769                	c.j	4a34 <xTaskResumeAll+0x4a>

00004aac <xTaskGetTickCount>:
    4aac:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    4aae:	1141                	c.addi	sp,-16
    4ab0:	3199                	c.jal	46f6 <vTaskEnterCritical>
    4ab2:	cbc1a503          	lw	a0,-836(gp) # 6018 <xTickCount>
    4ab6:	c62a                	c.swsp	a0,12(sp)
    4ab8:	3999                	c.jal	470e <vTaskExitCritical>
    4aba:	4532                	c.lwsp	a0,12(sp)
    4abc:	0141                	c.addi	sp,16
    4abe:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>

00004ac0 <xTaskCheckForTimeOut>:
    4ac0:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    4ac2:	1141                	c.addi	sp,-16
    4ac4:	84aa                	c.mv	s1,a0
    4ac6:	842e                	c.mv	s0,a1
    4ac8:	e901                	c.bnez	a0,4ad8 <xTaskCheckForTimeOut+0x18>
    4aca:	6585                	c.lui	a1,0x1
    4acc:	c6558593          	addi	a1,a1,-923 # c65 <setup+0x439>
    4ad0:	8424                	exec.it	#26     !auipc	a0,0x1
    4ad2:	f8450513          	addi	a0,a0,-124 # 5a54 <_ITB_BASE_+0x398>
    4ad6:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4ad8:	e801                	c.bnez	s0,4ae8 <xTaskCheckForTimeOut+0x28>
    4ada:	6085                	c.lui	ra,0x1
    4adc:	c6608593          	addi	a1,ra,-922 # c66 <setup+0x43a>
    4ae0:	8424                	exec.it	#26     !auipc	a0,0x1
    4ae2:	f7450513          	addi	a0,a0,-140 # 5a54 <_ITB_BASE_+0x398>
    4ae6:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4ae8:	3139                	c.jal	46f6 <vTaskEnterCritical>
    4aea:	cbc1a703          	lw	a4,-836(gp) # 6018 <xTickCount>
    4aee:	8070                	exec.it	#49     !lw	a5,-884(gp) # 5fe8 <pxCurrentTCB>
    4af0:	05e7c283          	lbu	t0,94(a5)
    4af4:	00028863          	beqz	t0,4b04 <xTaskCheckForTimeOut+0x44>
    4af8:	c8c1ae03          	lw	t3,-884(gp) # 5fe8 <pxCurrentTCB>
    4afc:	040e0f23          	sb	zero,94(t3)
    4b00:	4e85                	c.li	t4,1
    4b02:	a82d                	c.j	4b3c <xTaskCheckForTimeOut+0x7c>
    4b04:	00042303          	lw	t1,0(s0)
    4b08:	56fd                	c.li	a3,-1
    4b0a:	4e81                	c.li	t4,0
    4b0c:	02d30863          	beq	t1,a3,4b3c <xTaskCheckForTimeOut+0x7c>
    4b10:	4088                	c.lw	a0,0(s1)
    4b12:	cb41a603          	lw	a2,-844(gp) # 6010 <xNumOfOverflows>
    4b16:	0044a383          	lw	t2,4(s1)
    4b1a:	00c50563          	beq	a0,a2,4b24 <xTaskCheckForTimeOut+0x64>
    4b1e:	4e85                	c.li	t4,1
    4b20:	00777e63          	bgeu	a4,t2,4b3c <xTaskCheckForTimeOut+0x7c>
    4b24:	40770833          	sub	a6,a4,t2
    4b28:	00687f63          	bgeu	a6,t1,4b46 <xTaskCheckForTimeOut+0x86>
    4b2c:	410308b3          	sub	a7,t1,a6
    4b30:	8526                	c.mv	a0,s1
    4b32:	01142023          	sw	a7,0(s0)
    4b36:	b09ff0ef          	jal	ra,463e <vTaskInternalSetTimeOutState>
    4b3a:	4e81                	c.li	t4,0
    4b3c:	c676                	c.swsp	t4,12(sp)
    4b3e:	3ec1                	c.jal	470e <vTaskExitCritical>
    4b40:	4532                	c.lwsp	a0,12(sp)
    4b42:	0141                	c.addi	sp,16
    4b44:	8c14                	exec.it	#15     !j	52f4 <__riscv_restore_0>
    4b46:	00042023          	sw	zero,0(s0)
    4b4a:	bf5d                	c.j	4b00 <xTaskCheckForTimeOut+0x40>

00004b4c <prvCheckForValidListAndQueue>:
    4b4c:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    4b4e:	3665                	c.jal	46f6 <vTaskEnterCritical>
    4b50:	8e50                	exec.it	#103     !lw	a5,-816(gp) # 602c <xTimerQueue>
    4b52:	ef9d                	c.bnez	a5,4b90 <prvCheckForValidListAndQueue+0x44>
    4b54:	f7418493          	addi	s1,gp,-140 # 62d0 <xActiveTimerList1>
    4b58:	8526                	c.mv	a0,s1
    4b5a:	8834                	exec.it	#29     !jal	ra,3c58 <vListInitialise>
    4b5c:	f8818413          	addi	s0,gp,-120 # 62e4 <xActiveTimerList2>
    4b60:	8522                	c.mv	a0,s0
    4b62:	8834                	exec.it	#29     !jal	ra,3c58 <vListInitialise>
    4b64:	cc91a223          	sw	s1,-828(gp) # 6020 <pxCurrentTimerList>
    4b68:	cc81a423          	sw	s0,-824(gp) # 6024 <pxOverflowTimerList>
    4b6c:	f2418613          	addi	a2,gp,-220 # 6280 <ucStaticTimerQueueStorage.3059>
    4b70:	4701                	c.li	a4,0
    4b72:	f9c18693          	addi	a3,gp,-100 # 62f8 <xStaticTimerQueue.3058>
    4b76:	45c1                	c.li	a1,16
    4b78:	4515                	c.li	a0,5
    4b7a:	b10ff0ef          	jal	ra,3e8a <xQueueGenericCreateStatic>
    4b7e:	cca1a823          	sw	a0,-816(gp) # 602c <xTimerQueue>
    4b82:	c519                	c.beqz	a0,4b90 <prvCheckForValidListAndQueue+0x44>
    4b84:	00001597          	auipc	a1,0x1
    4b88:	e3858593          	addi	a1,a1,-456 # 59bc <_ITB_BASE_+0x300>
    4b8c:	e62ff0ef          	jal	ra,41ee <vQueueAddToRegistry>
    4b90:	3ebd                	c.jal	470e <vTaskExitCritical>
    4b92:	8c14                	exec.it	#15     !j	52f4 <__riscv_restore_0>

00004b94 <prvInsertTimerInActiveList>:
    4b94:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    4b96:	c14c                	c.sw	a1,4(a0)
    4b98:	c908                	c.sw	a0,16(a0)
    4b9a:	87aa                	c.mv	a5,a0
    4b9c:	00b66e63          	bltu	a2,a1,4bb8 <prvInsertTimerInActiveList+0x24>
    4ba0:	4d18                	c.lw	a4,24(a0)
    4ba2:	8e15                	c.sub	a2,a3
    4ba4:	4505                	c.li	a0,1
    4ba6:	00e67863          	bgeu	a2,a4,4bb6 <prvInsertTimerInActiveList+0x22>
    4baa:	00478593          	addi	a1,a5,4
    4bae:	cc81a503          	lw	a0,-824(gp) # 6024 <pxOverflowTimerList>
    4bb2:	8460                	exec.it	#50     !jal	ra,3c94 <vListInsert>
    4bb4:	4501                	c.li	a0,0
    4bb6:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
    4bb8:	00d67563          	bgeu	a2,a3,4bc2 <prvInsertTimerInActiveList+0x2e>
    4bbc:	4505                	c.li	a0,1
    4bbe:	fed5fce3          	bgeu	a1,a3,4bb6 <prvInsertTimerInActiveList+0x22>
    4bc2:	00478593          	addi	a1,a5,4
    4bc6:	cc41a503          	lw	a0,-828(gp) # 6020 <pxCurrentTimerList>
    4bca:	b7e5                	c.j	4bb2 <prvInsertTimerInActiveList+0x1e>

00004bcc <xTimerCreateTimerTask>:
    4bcc:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    4bce:	1141                	c.addi	sp,-16
    4bd0:	3fb5                	c.jal	4b4c <prvCheckForValidListAndQueue>
    4bd2:	8e50                	exec.it	#103     !lw	a5,-816(gp) # 602c <xTimerQueue>
    4bd4:	cf85                	c.beqz	a5,4c0c <xTimerCreateTimerTask+0x40>
    4bd6:	0070                	c.addi4spn	a2,sp,12
    4bd8:	002c                	c.addi4spn	a1,sp,8
    4bda:	0048                	c.addi4spn	a0,sp,4
    4bdc:	c202                	c.swsp	zero,4(sp)
    4bde:	c402                	c.swsp	zero,8(sp)
    4be0:	6a2000ef          	jal	ra,5282 <vApplicationGetTimerTaskMemory>
    4be4:	4812                	c.lwsp	a6,4(sp)
    4be6:	47a2                	c.lwsp	a5,8(sp)
    4be8:	4632                	c.lwsp	a2,12(sp)
    4bea:	4719                	c.li	a4,6
    4bec:	4681                	c.li	a3,0
    4bee:	00001597          	auipc	a1,0x1
    4bf2:	dd658593          	addi	a1,a1,-554 # 59c4 <_ITB_BASE_+0x308>
    4bf6:	00000517          	auipc	a0,0x0
    4bfa:	10050513          	addi	a0,a0,256 # 4cf6 <prvTimerTask>
    4bfe:	3119                	c.jal	4804 <xTaskCreateStatic>
    4c00:	cca1aa23          	sw	a0,-812(gp) # 6030 <xTimerTaskHandle>
    4c04:	c501                	c.beqz	a0,4c0c <xTimerCreateTimerTask+0x40>
    4c06:	4505                	c.li	a0,1
    4c08:	0141                	c.addi	sp,16
    4c0a:	8410                	exec.it	#3     !j	52f4 <__riscv_restore_0>
    4c0c:	00001517          	auipc	a0,0x1
    4c10:	ce450513          	addi	a0,a0,-796 # 58f0 <_ITB_BASE_+0x234>
    4c14:	11200593          	li	a1,274
    4c18:	8010                	exec.it	#1     !jal	ra,5220 <vAssertCalled>
    4c1a:	4501                	c.li	a0,0
    4c1c:	b7f5                	c.j	4c08 <xTimerCreateTimerTask+0x3c>

00004c1e <xTimerGenericCommand>:
    4c1e:	8830                	exec.it	#21     !jal	t0,52b6 <__riscv_save_4>
    4c20:	1101                	c.addi	sp,-32
    4c22:	84aa                	c.mv	s1,a0
    4c24:	842e                	c.mv	s0,a1
    4c26:	8936                	c.mv	s2,a3
    4c28:	89ba                	c.mv	s3,a4
    4c2a:	e911                	c.bnez	a0,4c3e <xTimerGenericCommand+0x20>
    4c2c:	18600593          	li	a1,390
    4c30:	00001517          	auipc	a0,0x1
    4c34:	cc050513          	addi	a0,a0,-832 # 58f0 <_ITB_BASE_+0x234>
    4c38:	c632                	c.swsp	a2,12(sp)
    4c3a:	23dd                	c.jal	5220 <vAssertCalled>
    4c3c:	4632                	c.lwsp	a2,12(sp)
    4c3e:	8e50                	exec.it	#103     !lw	a5,-816(gp) # 602c <xTimerQueue>
    4c40:	4501                	c.li	a0,0
    4c42:	c39d                	c.beqz	a5,4c68 <xTimerGenericCommand+0x4a>
    4c44:	c822                	c.swsp	s0,16(sp)
    4c46:	ca32                	c.swsp	a2,20(sp)
    4c48:	cc26                	c.swsp	s1,24(sp)
    4c4a:	4715                	c.li	a4,5
    4c4c:	02874063          	blt	a4,s0,4c6c <xTimerGenericCommand+0x4e>
    4c50:	8670                	exec.it	#115     !jal	ra,4656 <xTaskGetSchedulerState>
    4c52:	cd01a283          	lw	t0,-816(gp) # 602c <xTimerQueue>
    4c56:	4681                	c.li	a3,0
    4c58:	864e                	c.mv	a2,s3
    4c5a:	0025535b          	beqc	a0,2,4c60 <xTimerGenericCommand+0x42>
    4c5e:	4601                	c.li	a2,0
    4c60:	080c                	c.addi4spn	a1,sp,16
    4c62:	8516                	c.mv	a0,t0
    4c64:	af4ff0ef          	jal	ra,3f58 <xQueueGenericSend>
    4c68:	6105                	c.addi16sp	sp,32
    4c6a:	8024                	exec.it	#24     !j	52ea <__riscv_restore_4>
    4c6c:	4681                	c.li	a3,0
    4c6e:	864a                	c.mv	a2,s2
    4c70:	080c                	c.addi4spn	a1,sp,16
    4c72:	853e                	c.mv	a0,a5
    4c74:	bf4ff0ef          	jal	ra,4068 <xQueueGenericSendFromISR>
    4c78:	bfc5                	c.j	4c68 <xTimerGenericCommand+0x4a>

00004c7a <prvSwitchTimerLists>:
    4c7a:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    4c7c:	1141                	c.addi	sp,-16
    4c7e:	00001497          	auipc	s1,0x1
    4c82:	c7248493          	addi	s1,s1,-910 # 58f0 <_ITB_BASE_+0x234>
    4c86:	cc41a783          	lw	a5,-828(gp) # 6020 <pxCurrentTimerList>
    4c8a:	4398                	c.lw	a4,0(a5)
    4c8c:	eb09                	c.bnez	a4,4c9e <prvSwitchTimerLists+0x24>
    4c8e:	cc81a283          	lw	t0,-824(gp) # 6024 <pxOverflowTimerList>
    4c92:	cc51a223          	sw	t0,-828(gp) # 6020 <pxCurrentTimerList>
    4c96:	ccf1a423          	sw	a5,-824(gp) # 6024 <pxOverflowTimerList>
    4c9a:	0141                	c.addi	sp,16
    4c9c:	8c14                	exec.it	#15     !j	52f4 <__riscv_restore_0>
    4c9e:	00c7a083          	lw	ra,12(a5)
    4ca2:	00c0a403          	lw	s0,12(ra)
    4ca6:	0000a603          	lw	a2,0(ra)
    4caa:	00440593          	addi	a1,s0,4
    4cae:	852e                	c.mv	a0,a1
    4cb0:	c632                	c.swsp	a2,12(sp)
    4cb2:	c42e                	c.swsp	a1,8(sp)
    4cb4:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    4cb6:	02442303          	lw	t1,36(s0)
    4cba:	8522                	c.mv	a0,s0
    4cbc:	9302                	c.jalr	t1
    4cbe:	01c42383          	lw	t2,28(s0)
    4cc2:	45a2                	c.lwsp	a1,8(sp)
    4cc4:	4632                	c.lwsp	a2,12(sp)
    4cc6:	bc13e05b          	bnec	t2,1,4c86 <prvSwitchTimerLists+0xc>
    4cca:	4c08                	c.lw	a0,24(s0)
    4ccc:	00a606b3          	add	a3,a2,a0
    4cd0:	00d67863          	bgeu	a2,a3,4ce0 <prvSwitchTimerLists+0x66>
    4cd4:	cc41a503          	lw	a0,-828(gp) # 6020 <pxCurrentTimerList>
    4cd8:	c054                	c.sw	a3,4(s0)
    4cda:	c800                	c.sw	s0,16(s0)
    4cdc:	8460                	exec.it	#50     !jal	ra,3c94 <vListInsert>
    4cde:	b765                	c.j	4c86 <prvSwitchTimerLists+0xc>
    4ce0:	4701                	c.li	a4,0
    4ce2:	4681                	c.li	a3,0
    4ce4:	4581                	c.li	a1,0
    4ce6:	8522                	c.mv	a0,s0
    4ce8:	3f1d                	c.jal	4c1e <xTimerGenericCommand>
    4cea:	fd51                	c.bnez	a0,4c86 <prvSwitchTimerLists+0xc>
    4cec:	37200593          	li	a1,882
    4cf0:	8526                	c.mv	a0,s1
    4cf2:	233d                	c.jal	5220 <vAssertCalled>
    4cf4:	bf49                	c.j	4c86 <prvSwitchTimerLists+0xc>

00004cf6 <prvTimerTask>:
    4cf6:	8820                	exec.it	#20     !jal	t0,52b6 <__riscv_save_4>
    4cf8:	1141                	c.addi	sp,-16
    4cfa:	4a85                	c.li	s5,1
    4cfc:	00001997          	auipc	s3,0x1
    4d00:	bf498993          	addi	s3,s3,-1036 # 58f0 <_ITB_BASE_+0x234>
    4d04:	4a25                	c.li	s4,9
    4d06:	cc41a783          	lw	a5,-828(gp) # 6020 <pxCurrentTimerList>
    4d0a:	4398                	c.lw	a4,0(a5)
    4d0c:	c321                	c.beqz	a4,4d4c <prvTimerTask+0x56>
    4d0e:	00c7a083          	lw	ra,12(a5)
    4d12:	4401                	c.li	s0,0
    4d14:	0000a903          	lw	s2,0(ra)
    4d18:	8e70                	exec.it	#119     !jal	ra,43d0 <vTaskSuspendAll>
    4d1a:	3b49                	c.jal	4aac <xTaskGetTickCount>
    4d1c:	ccc1a283          	lw	t0,-820(gp) # 6028 <xLastTime.3012>
    4d20:	84aa                	c.mv	s1,a0
    4d22:	02556863          	bltu	a0,t0,4d52 <prvTimerTask+0x5c>
    4d26:	cca1a623          	sw	a0,-820(gp) # 6028 <xLastTime.3012>
    4d2a:	cc49                	c.beqz	s0,4dc4 <prvTimerTask+0xce>
    4d2c:	cc81a303          	lw	t1,-824(gp) # 6024 <pxOverflowTimerList>
    4d30:	00032603          	lw	a2,0(t1)
    4d34:	00163613          	seqz	a2,a2
    4d38:	409905b3          	sub	a1,s2,s1
    4d3c:	cd01a503          	lw	a0,-816(gp) # 602c <xTimerQueue>
    4d40:	cd6ff0ef          	jal	ra,4216 <vQueueWaitForMessageRestricted>
    4d44:	315d                	c.jal	49ea <xTaskResumeAll>
    4d46:	e911                	c.bnez	a0,4d5a <prvTimerTask+0x64>
    4d48:	2131                	c.jal	5154 <vPortYield>
    4d4a:	a801                	c.j	4d5a <prvTimerTask+0x64>
    4d4c:	4405                	c.li	s0,1
    4d4e:	4901                	c.li	s2,0
    4d50:	b7e1                	c.j	4d18 <prvTimerTask+0x22>
    4d52:	3725                	c.jal	4c7a <prvSwitchTimerLists>
    4d54:	cc91a623          	sw	s1,-820(gp) # 6028 <xLastTime.3012>
    4d58:	3949                	c.jal	49ea <xTaskResumeAll>
    4d5a:	4601                	c.li	a2,0
    4d5c:	858a                	c.mv	a1,sp
    4d5e:	cd01a503          	lw	a0,-816(gp) # 602c <xTimerQueue>
    4d62:	ba6ff0ef          	jal	ra,4108 <xQueueReceive>
    4d66:	d145                	c.beqz	a0,4d06 <prvTimerTask+0x10>
    4d68:	4882                	c.lwsp	a7,0(sp)
    4d6a:	0008d663          	bgez	a7,4d76 <prvTimerTask+0x80>
    4d6e:	45b2                	c.lwsp	a1,12(sp)
    4d70:	4522                	c.lwsp	a0,8(sp)
    4d72:	4e12                	c.lwsp	t3,4(sp)
    4d74:	9e02                	c.jalr	t3
    4d76:	4e82                	c.lwsp	t4,0(sp)
    4d78:	fe0ec1e3          	bltz	t4,4d5a <prvTimerTask+0x64>
    4d7c:	4922                	c.lwsp	s2,8(sp)
    4d7e:	01492f03          	lw	t5,20(s2)
    4d82:	000f0563          	beqz	t5,4d8c <prvTimerTask+0x96>
    4d86:	00490513          	addi	a0,s2,4
    4d8a:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    4d8c:	3305                	c.jal	4aac <xTaskGetTickCount>
    4d8e:	ccc1af83          	lw	t6,-820(gp) # 6028 <xLastTime.3012>
    4d92:	84aa                	c.mv	s1,a0
    4d94:	01f57363          	bgeu	a0,t6,4d9a <prvTimerTask+0xa4>
    4d98:	35cd                	c.jal	4c7a <prvSwitchTimerLists>
    4d9a:	4782                	c.lwsp	a5,0(sp)
    4d9c:	cc91a623          	sw	s1,-820(gp) # 6028 <xLastTime.3012>
    4da0:	fafa6de3          	bltu	s4,a5,4d5a <prvTimerTask+0x64>
    4da4:	00fa90b3          	sll	ra,s5,a5
    4da8:	2100f713          	andi	a4,ra,528
    4dac:	e74d                	c.bnez	a4,4e56 <prvTimerTask+0x160>
    4dae:	0c70f613          	andi	a2,ra,199
    4db2:	ee39                	c.bnez	a2,4e10 <prvTimerTask+0x11a>
    4db4:	ba50f35b          	bbc	ra,5,4d5a <prvTimerTask+0x64>
    4db8:	02c94683          	lbu	a3,44(s2)
    4dbc:	fed9                	c.bnez	a3,4d5a <prvTimerTask+0x64>
    4dbe:	854a                	c.mv	a0,s2
    4dc0:	2a79                	c.jal	4f5e <vPortFree>
    4dc2:	bf61                	c.j	4d5a <prvTimerTask+0x64>
    4dc4:	4601                	c.li	a2,0
    4dc6:	f72569e3          	bltu	a0,s2,4d38 <prvTimerTask+0x42>
    4dca:	3105                	c.jal	49ea <xTaskResumeAll>
    4dcc:	cc41a383          	lw	t2,-828(gp) # 6020 <pxCurrentTimerList>
    4dd0:	00c3a503          	lw	a0,12(t2)
    4dd4:	4540                	c.lw	s0,12(a0)
    4dd6:	8250                	exec.it	#97     !addi	a0,s0,4
    4dd8:	8814                	exec.it	#13     !jal	ra,3cd0 <uxListRemove>
    4dda:	4c4c                	c.lw	a1,28(s0)
    4ddc:	0215e65b          	bnec	a1,1,4e08 <prvTimerTask+0x112>
    4de0:	01842803          	lw	a6,24(s0)
    4de4:	86ca                	c.mv	a3,s2
    4de6:	8626                	c.mv	a2,s1
    4de8:	010905b3          	add	a1,s2,a6
    4dec:	8522                	c.mv	a0,s0
    4dee:	335d                	c.jal	4b94 <prvInsertTimerInActiveList>
    4df0:	cd01                	c.beqz	a0,4e08 <prvTimerTask+0x112>
    4df2:	4701                	c.li	a4,0
    4df4:	4681                	c.li	a3,0
    4df6:	864a                	c.mv	a2,s2
    4df8:	4581                	c.li	a1,0
    4dfa:	8522                	c.mv	a0,s0
    4dfc:	350d                	c.jal	4c1e <xTimerGenericCommand>
    4dfe:	e509                	c.bnez	a0,4e08 <prvTimerTask+0x112>
    4e00:	1e800593          	li	a1,488
    4e04:	854e                	c.mv	a0,s3
    4e06:	2929                	c.jal	5220 <vAssertCalled>
    4e08:	5054                	c.lw	a3,36(s0)
    4e0a:	8522                	c.mv	a0,s0
    4e0c:	9682                	c.jalr	a3
    4e0e:	b7b1                	c.j	4d5a <prvTimerTask+0x64>
    4e10:	01892383          	lw	t2,24(s2)
    4e14:	4692                	c.lwsp	a3,4(sp)
    4e16:	8626                	c.mv	a2,s1
    4e18:	007685b3          	add	a1,a3,t2
    4e1c:	854a                	c.mv	a0,s2
    4e1e:	3b9d                	c.jal	4b94 <prvInsertTimerInActiveList>
    4e20:	f2050de3          	beqz	a0,4d5a <prvTimerTask+0x64>
    4e24:	02492403          	lw	s0,36(s2)
    4e28:	854a                	c.mv	a0,s2
    4e2a:	9402                	c.jalr	s0
    4e2c:	01c92503          	lw	a0,28(s2)
    4e30:	b215655b          	bnec	a0,1,4d5a <prvTimerTask+0x64>
    4e34:	01892583          	lw	a1,24(s2)
    4e38:	4812                	c.lwsp	a6,4(sp)
    4e3a:	4701                	c.li	a4,0
    4e3c:	00b80633          	add	a2,a6,a1
    4e40:	4681                	c.li	a3,0
    4e42:	4581                	c.li	a1,0
    4e44:	854a                	c.mv	a0,s2
    4e46:	3be1                	c.jal	4c1e <xTimerGenericCommand>
    4e48:	f00519e3          	bnez	a0,4d5a <prvTimerTask+0x64>
    4e4c:	30100593          	li	a1,769
    4e50:	854e                	c.mv	a0,s3
    4e52:	26f9                	c.jal	5220 <vAssertCalled>
    4e54:	b719                	c.j	4d5a <prvTimerTask+0x64>
    4e56:	4292                	c.lwsp	t0,4(sp)
    4e58:	00592c23          	sw	t0,24(s2)
    4e5c:	00029663          	bnez	t0,4e68 <prvTimerTask+0x172>
    4e60:	31800593          	li	a1,792
    4e64:	854e                	c.mv	a0,s3
    4e66:	2e6d                	c.jal	5220 <vAssertCalled>
    4e68:	01892303          	lw	t1,24(s2)
    4e6c:	86a6                	c.mv	a3,s1
    4e6e:	8626                	c.mv	a2,s1
    4e70:	006485b3          	add	a1,s1,t1
    4e74:	854a                	c.mv	a0,s2
    4e76:	3b39                	c.jal	4b94 <prvInsertTimerInActiveList>
    4e78:	b5cd                	c.j	4d5a <prvTimerTask+0x64>

00004e7a <pvPortMalloc>:
    4e7a:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    4e7c:	842a                	c.mv	s0,a0
    4e7e:	8e70                	exec.it	#119     !jal	ra,43d0 <vTaskSuspendAll>
    4e80:	ce01a783          	lw	a5,-800(gp) # 603c <xHeapHasBeenInitialised.2693>
    4e84:	eb95                	c.bnez	a5,4eb8 <pvPortMalloc+0x3e>
    4e86:	ffc18093          	addi	ra,gp,-4 # 6358 <ucHeap+0x10>
    4e8a:	00020737          	lui	a4,0x20
    4e8e:	ff00f293          	andi	t0,ra,-16
    4e92:	ff070313          	addi	t1,a4,-16 # 1fff0 <__global_pointer$+0x19c94>
    4e96:	4485                	c.li	s1,1
    4e98:	cd818393          	addi	t2,gp,-808 # 6034 <xEnd>
    4e9c:	ce51a223          	sw	t0,-796(gp) # 6040 <xStart>
    4ea0:	ce01a423          	sw	zero,-792(gp) # 6044 <xStart+0x4>
    4ea4:	cc61ae23          	sw	t1,-804(gp) # 6038 <xEnd+0x4>
    4ea8:	cc01ac23          	sw	zero,-808(gp) # 6034 <xEnd>
    4eac:	ce91a023          	sw	s1,-800(gp) # 603c <xHeapHasBeenInitialised.2693>
    4eb0:	0062a223          	sw	t1,4(t0)
    4eb4:	0072a023          	sw	t2,0(t0)
    4eb8:	c419                	c.beqz	s0,4ec6 <pvPortMalloc+0x4c>
    4eba:	0441                	c.addi	s0,16
    4ebc:	00f47513          	andi	a0,s0,15
    4ec0:	c119                	c.beqz	a0,4ec6 <pvPortMalloc+0x4c>
    4ec2:	9841                	c.andi	s0,-16
    4ec4:	0441                	c.addi	s0,16
    4ec6:	00020637          	lui	a2,0x20
    4eca:	fff40593          	addi	a1,s0,-1
    4ece:	fee60693          	addi	a3,a2,-18 # 1ffee <__global_pointer$+0x19c92>
    4ed2:	00b6f663          	bgeu	a3,a1,4ede <pvPortMalloc+0x64>
    4ed6:	3e11                	c.jal	49ea <xTaskResumeAll>
    4ed8:	26ad                	c.jal	5242 <vApplicationMallocFailedHook>
    4eda:	4481                	c.li	s1,0
    4edc:	a895                	c.j	4f50 <pvPortMalloc+0xd6>
    4ede:	ce41a783          	lw	a5,-796(gp) # 6040 <xStart>
    4ee2:	ce418f93          	addi	t6,gp,-796 # 6040 <xStart>
    4ee6:	0047a803          	lw	a6,4(a5)
    4eea:	00887663          	bgeu	a6,s0,4ef6 <pvPortMalloc+0x7c>
    4eee:	0007a883          	lw	a7,0(a5)
    4ef2:	06089163          	bnez	a7,4f54 <pvPortMalloc+0xda>
    4ef6:	cd818e13          	addi	t3,gp,-808 # 6034 <xEnd>
    4efa:	fdc78ee3          	beq	a5,t3,4ed6 <pvPortMalloc+0x5c>
    4efe:	0007af03          	lw	t5,0(a5)
    4f02:	000fae83          	lw	t4,0(t6)
    4f06:	01efa023          	sw	t5,0(t6)
    4f0a:	0047af83          	lw	t6,4(a5)
    4f0e:	02000293          	li	t0,32
    4f12:	408f80b3          	sub	ra,t6,s0
    4f16:	010e8493          	addi	s1,t4,16
    4f1a:	0212f263          	bgeu	t0,ra,4f3e <pvPortMalloc+0xc4>
    4f1e:	00878633          	add	a2,a5,s0
    4f22:	00162223          	sw	ra,4(a2)
    4f26:	c3c0                	c.sw	s0,4(a5)
    4f28:	424c                	c.lw	a1,4(a2)
    4f2a:	ce418693          	addi	a3,gp,-796 # 6040 <xStart>
    4f2e:	0006a303          	lw	t1,0(a3)
    4f32:	00432383          	lw	t2,4(t1)
    4f36:	02b3e263          	bltu	t2,a1,4f5a <pvPortMalloc+0xe0>
    4f3a:	8e24                	exec.it	#94     !sw	t1,0(a2)
    4f3c:	c290                	c.sw	a2,0(a3)
    4f3e:	43d8                	c.lw	a4,4(a5)
    4f40:	c381a783          	lw	a5,-968(gp) # 5f94 <xFreeBytesRemaining>
    4f44:	40e78533          	sub	a0,a5,a4
    4f48:	c2a1ac23          	sw	a0,-968(gp) # 5f94 <xFreeBytesRemaining>
    4f4c:	8824                	exec.it	#28     !jal	ra,49ea <xTaskResumeAll>
    4f4e:	d4c9                	c.beqz	s1,4ed8 <pvPortMalloc+0x5e>
    4f50:	8526                	c.mv	a0,s1
    4f52:	a64d                	c.j	52f4 <__riscv_restore_0>
    4f54:	8fbe                	c.mv	t6,a5
    4f56:	87c6                	c.mv	a5,a7
    4f58:	b779                	c.j	4ee6 <pvPortMalloc+0x6c>
    4f5a:	869a                	c.mv	a3,t1
    4f5c:	bfc9                	c.j	4f2e <pvPortMalloc+0xb4>

00004f5e <vPortFree>:
    4f5e:	cd1d                	c.beqz	a0,4f9c <vPortFree+0x3e>
    4f60:	8414                	exec.it	#11     !jal	t0,52d0 <__riscv_save_0>
    4f62:	842a                	c.mv	s0,a0
    4f64:	ff050493          	addi	s1,a0,-16
    4f68:	8e70                	exec.it	#119     !jal	ra,43d0 <vTaskSuspendAll>
    4f6a:	ff442683          	lw	a3,-12(s0)
    4f6e:	ce418313          	addi	t1,gp,-796 # 6040 <xStart>
    4f72:	00032703          	lw	a4,0(t1)
    4f76:	4350                	c.lw	a2,4(a4)
    4f78:	02d66063          	bltu	a2,a3,4f98 <vPortFree+0x3a>
    4f7c:	fee42823          	sw	a4,-16(s0)
    4f80:	00932023          	sw	s1,0(t1)
    4f84:	ff442783          	lw	a5,-12(s0)
    4f88:	c381a083          	lw	ra,-968(gp) # 5f94 <xFreeBytesRemaining>
    4f8c:	001782b3          	add	t0,a5,ra
    4f90:	c251ac23          	sw	t0,-968(gp) # 5f94 <xFreeBytesRemaining>
    4f94:	8824                	exec.it	#28     !jal	ra,49ea <xTaskResumeAll>
    4f96:	aeb9                	c.j	52f4 <__riscv_restore_0>
    4f98:	833a                	c.mv	t1,a4
    4f9a:	bfe1                	c.j	4f72 <vPortFree+0x14>
    4f9c:	8082                	c.jr	ra

00004f9e <xPortGetFreeHeapSize>:
    4f9e:	c381a503          	lw	a0,-968(gp) # 5f94 <xFreeBytesRemaining>
    4fa2:	8082                	c.jr	ra

00004fa4 <prvTaskExitError>:
    4fa4:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    4fa6:	0ba00593          	li	a1,186
    4faa:	00001517          	auipc	a0,0x1
    4fae:	a2250513          	addi	a0,a0,-1502 # 59cc <_ITB_BASE_+0x310>
    4fb2:	24bd                	c.jal	5220 <vAssertCalled>
    4fb4:	8870                	exec.it	#53     !csrci	mstatus,8
    4fb6:	a001                	c.j	4fb6 <prvTaskExitError+0x12>

00004fb8 <vPortSetupTimer>:
    4fb8:	c3c1a783          	lw	a5,-964(gp) # 5f98 <mtime>
    4fbc:	43cc                	c.lw	a1,4(a5)
    4fbe:	4398                	c.lw	a4,0(a5)
    4fc0:	43d4                	c.lw	a3,4(a5)
    4fc2:	fed59de3          	bne	a1,a3,4fbc <vPortSetupTimer+0x4>
    4fc6:	62cd                	c.lui	t0,0x13
    4fc8:	0b028313          	addi	t1,t0,176 # 130b0 <__global_pointer$+0xcd54>
    4fcc:	006703b3          	add	t2,a4,t1
    4fd0:	c401a883          	lw	a7,-960(gp) # 5f9c <mtimecmp>
    4fd4:	00e3b533          	sltu	a0,t2,a4
    4fd8:	00b50833          	add	a6,a0,a1
    4fdc:	0078a023          	sw	t2,0(a7)
    4fe0:	0108a223          	sw	a6,4(a7)
    4fe4:	08000e13          	li	t3,128
    4fe8:	304e2073          	csrs	mie,t3
    4fec:	8082                	c.jr	ra

00004fee <vPortClearInterruptMask>:
    4fee:	30451073          	csrw	mie,a0
    4ff2:	8082                	c.jr	ra

00004ff4 <vPortSetInterruptMask>:
    4ff4:	30402573          	csrr	a0,mie
    4ff8:	3043f073          	csrci	mie,7
    4ffc:	8082                	c.jr	ra

00004ffe <pxPortInitialiseStack>:
    4ffe:	00000797          	auipc	a5,0x0
    5002:	fa678793          	addi	a5,a5,-90 # 4fa4 <prvTaskExitError>
    5006:	feb52e23          	sw	a1,-4(a0)
    500a:	fac52223          	sw	a2,-92(a0)
    500e:	f8352623          	sw	gp,-116(a0)
    5012:	f8f52023          	sw	a5,-128(a0)
    5016:	f8050513          	addi	a0,a0,-128
    501a:	8082                	c.jr	ra

0000501c <vPortSysTickHandler>:
    501c:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    501e:	c401a703          	lw	a4,-960(gp) # 5f9c <mtimecmp>
    5022:	00072803          	lw	a6,0(a4)
    5026:	67cd                	c.lui	a5,0x13
    5028:	0b078093          	addi	ra,a5,176 # 130b0 <__global_pointer$+0xcd54>
    502c:	00472883          	lw	a7,4(a4)
    5030:	001802b3          	add	t0,a6,ra
    5034:	0102b533          	sltu	a0,t0,a6
    5038:	011506b3          	add	a3,a0,a7
    503c:	8e34                	exec.it	#95     !sw	t0,0(a4)
    503e:	c354                	c.sw	a3,4(a4)
    5040:	b9eff0ef          	jal	ra,43de <xTaskIncrementTick>
    5044:	c119                	c.beqz	a0,504a <vPortSysTickHandler+0x2e>
    5046:	cacff0ef          	jal	ra,44f2 <vTaskSwitchContext>
    504a:	a46d                	c.j	52f4 <__riscv_restore_0>

0000504c <FreeRTOS_MTIMER_Handler>:
    504c:	7119                	c.addi16sp	sp,-128
    504e:	c006                	c.swsp	ra,0(sp)
    5050:	c20a                	c.swsp	sp,4(sp)
    5052:	c40e                	c.swsp	gp,8(sp)
    5054:	c612                	c.swsp	tp,12(sp)
    5056:	c816                	c.swsp	t0,16(sp)
    5058:	ca1a                	c.swsp	t1,20(sp)
    505a:	cc1e                	c.swsp	t2,24(sp)
    505c:	ce22                	c.swsp	s0,28(sp)
    505e:	d026                	c.swsp	s1,32(sp)
    5060:	d22a                	c.swsp	a0,36(sp)
    5062:	d42e                	c.swsp	a1,40(sp)
    5064:	d632                	c.swsp	a2,44(sp)
    5066:	d836                	c.swsp	a3,48(sp)
    5068:	da3a                	c.swsp	a4,52(sp)
    506a:	dc3e                	c.swsp	a5,56(sp)
    506c:	de42                	c.swsp	a6,60(sp)
    506e:	c0c6                	c.swsp	a7,64(sp)
    5070:	c2ca                	c.swsp	s2,68(sp)
    5072:	c4ce                	c.swsp	s3,72(sp)
    5074:	c6d2                	c.swsp	s4,76(sp)
    5076:	c8d6                	c.swsp	s5,80(sp)
    5078:	cada                	c.swsp	s6,84(sp)
    507a:	ccde                	c.swsp	s7,88(sp)
    507c:	cee2                	c.swsp	s8,92(sp)
    507e:	d0e6                	c.swsp	s9,96(sp)
    5080:	d2ea                	c.swsp	s10,100(sp)
    5082:	d4ee                	c.swsp	s11,104(sp)
    5084:	d6f2                	c.swsp	t3,108(sp)
    5086:	d8f6                	c.swsp	t4,112(sp)
    5088:	dafa                	c.swsp	t5,116(sp)
    508a:	dcfe                	c.swsp	t6,120(sp)
    508c:	c8c1a283          	lw	t0,-884(gp) # 5fe8 <pxCurrentTCB>
    5090:	0022a023          	sw	sp,0(t0)
    5094:	341022f3          	csrr	t0,mepc
    5098:	de96                	c.swsp	t0,124(sp)
    509a:	f83ff0ef          	jal	ra,501c <vPortSysTickHandler>
    509e:	c8c1a103          	lw	sp,-884(gp) # 5fe8 <pxCurrentTCB>
    50a2:	4102                	c.lwsp	sp,0(sp)
    50a4:	52f6                	c.lwsp	t0,124(sp)
    50a6:	34129073          	csrw	mepc,t0
    50aa:	000022b7          	lui	t0,0x2
    50ae:	88028293          	addi	t0,t0,-1920 # 1880 <sdudcFifoStatus>
    50b2:	30029073          	csrw	mstatus,t0
    50b6:	4082                	c.lwsp	ra,0(sp)
    50b8:	4232                	c.lwsp	tp,12(sp)
    50ba:	42c2                	c.lwsp	t0,16(sp)
    50bc:	4352                	c.lwsp	t1,20(sp)
    50be:	43e2                	c.lwsp	t2,24(sp)
    50c0:	4472                	c.lwsp	s0,28(sp)
    50c2:	5482                	c.lwsp	s1,32(sp)
    50c4:	5512                	c.lwsp	a0,36(sp)
    50c6:	55a2                	c.lwsp	a1,40(sp)
    50c8:	5632                	c.lwsp	a2,44(sp)
    50ca:	56c2                	c.lwsp	a3,48(sp)
    50cc:	5752                	c.lwsp	a4,52(sp)
    50ce:	57e2                	c.lwsp	a5,56(sp)
    50d0:	5872                	c.lwsp	a6,60(sp)
    50d2:	4886                	c.lwsp	a7,64(sp)
    50d4:	4916                	c.lwsp	s2,68(sp)
    50d6:	49a6                	c.lwsp	s3,72(sp)
    50d8:	4a36                	c.lwsp	s4,76(sp)
    50da:	4ac6                	c.lwsp	s5,80(sp)
    50dc:	4b56                	c.lwsp	s6,84(sp)
    50de:	4be6                	c.lwsp	s7,88(sp)
    50e0:	4c76                	c.lwsp	s8,92(sp)
    50e2:	5c86                	c.lwsp	s9,96(sp)
    50e4:	5d16                	c.lwsp	s10,100(sp)
    50e6:	5da6                	c.lwsp	s11,104(sp)
    50e8:	5e36                	c.lwsp	t3,108(sp)
    50ea:	5ec6                	c.lwsp	t4,112(sp)
    50ec:	5f56                	c.lwsp	t5,116(sp)
    50ee:	5fe6                	c.lwsp	t6,120(sp)
    50f0:	6109                	c.addi16sp	sp,128
    50f2:	30200073          	mret

000050f6 <xPortStartScheduler>:
    50f6:	ec3ff0ef          	jal	ra,4fb8 <vPortSetupTimer>
    50fa:	c8c1a103          	lw	sp,-884(gp) # 5fe8 <pxCurrentTCB>
    50fe:	4102                	c.lwsp	sp,0(sp)
    5100:	52f6                	c.lwsp	t0,124(sp)
    5102:	34129073          	csrw	mepc,t0
    5106:	000022b7          	lui	t0,0x2
    510a:	88028293          	addi	t0,t0,-1920 # 1880 <sdudcFifoStatus>
    510e:	30029073          	csrw	mstatus,t0
    5112:	4082                	c.lwsp	ra,0(sp)
    5114:	4232                	c.lwsp	tp,12(sp)
    5116:	42c2                	c.lwsp	t0,16(sp)
    5118:	4352                	c.lwsp	t1,20(sp)
    511a:	43e2                	c.lwsp	t2,24(sp)
    511c:	4472                	c.lwsp	s0,28(sp)
    511e:	5482                	c.lwsp	s1,32(sp)
    5120:	5512                	c.lwsp	a0,36(sp)
    5122:	55a2                	c.lwsp	a1,40(sp)
    5124:	5632                	c.lwsp	a2,44(sp)
    5126:	56c2                	c.lwsp	a3,48(sp)
    5128:	5752                	c.lwsp	a4,52(sp)
    512a:	57e2                	c.lwsp	a5,56(sp)
    512c:	5872                	c.lwsp	a6,60(sp)
    512e:	4886                	c.lwsp	a7,64(sp)
    5130:	4916                	c.lwsp	s2,68(sp)
    5132:	49a6                	c.lwsp	s3,72(sp)
    5134:	4a36                	c.lwsp	s4,76(sp)
    5136:	4ac6                	c.lwsp	s5,80(sp)
    5138:	4b56                	c.lwsp	s6,84(sp)
    513a:	4be6                	c.lwsp	s7,88(sp)
    513c:	4c76                	c.lwsp	s8,92(sp)
    513e:	5c86                	c.lwsp	s9,96(sp)
    5140:	5d16                	c.lwsp	s10,100(sp)
    5142:	5da6                	c.lwsp	s11,104(sp)
    5144:	5e36                	c.lwsp	t3,108(sp)
    5146:	5ec6                	c.lwsp	t4,112(sp)
    5148:	5f56                	c.lwsp	t5,116(sp)
    514a:	5fe6                	c.lwsp	t6,120(sp)
    514c:	6109                	c.addi16sp	sp,128
    514e:	30200073          	mret

00005152 <vPortEndScheduler>:
    5152:	8082                	c.jr	ra

00005154 <vPortYield>:
    5154:	30047073          	csrci	mstatus,8
    5158:	7119                	c.addi16sp	sp,-128
    515a:	c006                	c.swsp	ra,0(sp)
    515c:	c20a                	c.swsp	sp,4(sp)
    515e:	c40e                	c.swsp	gp,8(sp)
    5160:	c612                	c.swsp	tp,12(sp)
    5162:	c816                	c.swsp	t0,16(sp)
    5164:	ca1a                	c.swsp	t1,20(sp)
    5166:	cc1e                	c.swsp	t2,24(sp)
    5168:	ce22                	c.swsp	s0,28(sp)
    516a:	d026                	c.swsp	s1,32(sp)
    516c:	d22a                	c.swsp	a0,36(sp)
    516e:	d42e                	c.swsp	a1,40(sp)
    5170:	d632                	c.swsp	a2,44(sp)
    5172:	d836                	c.swsp	a3,48(sp)
    5174:	da3a                	c.swsp	a4,52(sp)
    5176:	dc3e                	c.swsp	a5,56(sp)
    5178:	de42                	c.swsp	a6,60(sp)
    517a:	c0c6                	c.swsp	a7,64(sp)
    517c:	c2ca                	c.swsp	s2,68(sp)
    517e:	c4ce                	c.swsp	s3,72(sp)
    5180:	c6d2                	c.swsp	s4,76(sp)
    5182:	c8d6                	c.swsp	s5,80(sp)
    5184:	cada                	c.swsp	s6,84(sp)
    5186:	ccde                	c.swsp	s7,88(sp)
    5188:	cee2                	c.swsp	s8,92(sp)
    518a:	d0e6                	c.swsp	s9,96(sp)
    518c:	d2ea                	c.swsp	s10,100(sp)
    518e:	d4ee                	c.swsp	s11,104(sp)
    5190:	d6f2                	c.swsp	t3,108(sp)
    5192:	d8f6                	c.swsp	t4,112(sp)
    5194:	dafa                	c.swsp	t5,116(sp)
    5196:	dcfe                	c.swsp	t6,120(sp)
    5198:	c8c1a283          	lw	t0,-884(gp) # 5fe8 <pxCurrentTCB>
    519c:	0022a023          	sw	sp,0(t0)
    51a0:	de86                	c.swsp	ra,124(sp)
    51a2:	b50ff0ef          	jal	ra,44f2 <vTaskSwitchContext>
    51a6:	c8c1a103          	lw	sp,-884(gp) # 5fe8 <pxCurrentTCB>
    51aa:	4102                	c.lwsp	sp,0(sp)
    51ac:	52f6                	c.lwsp	t0,124(sp)
    51ae:	34129073          	csrw	mepc,t0
    51b2:	000022b7          	lui	t0,0x2
    51b6:	88028293          	addi	t0,t0,-1920 # 1880 <sdudcFifoStatus>
    51ba:	30029073          	csrw	mstatus,t0
    51be:	4082                	c.lwsp	ra,0(sp)
    51c0:	4232                	c.lwsp	tp,12(sp)
    51c2:	42c2                	c.lwsp	t0,16(sp)
    51c4:	4352                	c.lwsp	t1,20(sp)
    51c6:	43e2                	c.lwsp	t2,24(sp)
    51c8:	4472                	c.lwsp	s0,28(sp)
    51ca:	5482                	c.lwsp	s1,32(sp)
    51cc:	5512                	c.lwsp	a0,36(sp)
    51ce:	55a2                	c.lwsp	a1,40(sp)
    51d0:	5632                	c.lwsp	a2,44(sp)
    51d2:	56c2                	c.lwsp	a3,48(sp)
    51d4:	5752                	c.lwsp	a4,52(sp)
    51d6:	57e2                	c.lwsp	a5,56(sp)
    51d8:	5872                	c.lwsp	a6,60(sp)
    51da:	4886                	c.lwsp	a7,64(sp)
    51dc:	4916                	c.lwsp	s2,68(sp)
    51de:	49a6                	c.lwsp	s3,72(sp)
    51e0:	4a36                	c.lwsp	s4,76(sp)
    51e2:	4ac6                	c.lwsp	s5,80(sp)
    51e4:	4b56                	c.lwsp	s6,84(sp)
    51e6:	4be6                	c.lwsp	s7,88(sp)
    51e8:	4c76                	c.lwsp	s8,92(sp)
    51ea:	5c86                	c.lwsp	s9,96(sp)
    51ec:	5d16                	c.lwsp	s10,100(sp)
    51ee:	5da6                	c.lwsp	s11,104(sp)
    51f0:	5e36                	c.lwsp	t3,108(sp)
    51f2:	5ec6                	c.lwsp	t4,112(sp)
    51f4:	5f56                	c.lwsp	t5,116(sp)
    51f6:	5fe6                	c.lwsp	t6,120(sp)
    51f8:	6109                	c.addi16sp	sp,128
    51fa:	30200073          	mret

000051fe <main>:
    51fe:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    5200:	8870                	exec.it	#53     !csrci	mstatus,8
    5202:	a1efb0ef          	jal	ra,420 <isr_Init>
    5206:	65f1                	c.lui	a1,0x1c
    5208:	4681                	c.li	a3,0
    520a:	4601                	c.li	a2,0
    520c:	20058593          	addi	a1,a1,512 # 1c200 <__global_pointer$+0x15ea4>
    5210:	4501                	c.li	a0,0
    5212:	85afb0ef          	jal	ra,26c <uart_Open>
    5216:	ab8fb0ef          	jal	ra,4ce <usbd_Init>
    521a:	e5cff0ef          	jal	ra,4876 <vTaskStartScheduler>
    521e:	a001                	c.j	521e <main+0x20>

00005220 <vAssertCalled>:
    5220:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    5222:	1141                	c.addi	sp,-16
    5224:	862a                	c.mv	a2,a0
    5226:	00000517          	auipc	a0,0x0
    522a:	74250513          	addi	a0,a0,1858 # 5968 <_ITB_BASE_+0x2ac>
    522e:	c602                	c.swsp	zero,12(sp)
    5230:	8e10                	exec.it	#71     !jal	ra,3c36 <printf>
    5232:	8420                	exec.it	#18     !jal	ra,46f6 <vTaskEnterCritical>
    5234:	47b2                	c.lwsp	a5,12(sp)
    5236:	c781                	c.beqz	a5,523e <vAssertCalled+0x1e>
    5238:	8804                	exec.it	#12     !jal	ra,470e <vTaskExitCritical>
    523a:	0141                	c.addi	sp,16
    523c:	a865                	c.j	52f4 <__riscv_restore_0>
    523e:	0001                	c.nop
    5240:	bfd5                	c.j	5234 <vAssertCalled+0x14>

00005242 <vApplicationMallocFailedHook>:
    5242:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    5244:	05e00593          	li	a1,94
    5248:	00000517          	auipc	a0,0x0
    524c:	6e850513          	addi	a0,a0,1768 # 5930 <_ITB_BASE_+0x274>
    5250:	3fc1                	c.jal	5220 <vAssertCalled>
    5252:	a04d                	c.j	52f4 <__riscv_restore_0>

00005254 <vApplicationIdleHook>:
    5254:	8000                	exec.it	#0     !jal	t0,52d0 <__riscv_save_0>
    5256:	1141                	c.addi	sp,-16
    5258:	3399                	c.jal	4f9e <xPortGetFreeHeapSize>
    525a:	c62a                	c.swsp	a0,12(sp)
    525c:	47b2                	c.lwsp	a5,12(sp)
    525e:	0141                	c.addi	sp,16
    5260:	a851                	c.j	52f4 <__riscv_restore_0>

00005262 <vApplicationTickHook>:
    5262:	8082                	c.jr	ra

00005264 <vApplicationGetIdleTaskMemory>:
    5264:	00022797          	auipc	a5,0x22
    5268:	ce478793          	addi	a5,a5,-796 # 26f48 <xIdleTaskTCB.3892>
    526c:	c11c                	c.sw	a5,0(a0)
    526e:	00021297          	auipc	t0,0x21
    5272:	0da28293          	addi	t0,t0,218 # 26348 <uxIdleTaskStack.3893>
    5276:	0055a023          	sw	t0,0(a1)
    527a:	10000313          	li	t1,256
    527e:	8e24                	exec.it	#94     !sw	t1,0(a2)
    5280:	8082                	c.jr	ra

00005282 <vApplicationGetTimerTaskMemory>:
    5282:	00022797          	auipc	a5,0x22
    5286:	d2678793          	addi	a5,a5,-730 # 26fa8 <xTimerTaskTCB.3899>
    528a:	c11c                	c.sw	a5,0(a0)
    528c:	00021297          	auipc	t0,0x21
    5290:	4bc28293          	addi	t0,t0,1212 # 26748 <uxTimerTaskStack.3900>
    5294:	0055a023          	sw	t0,0(a1)
    5298:	20000313          	li	t1,512
    529c:	8e24                	exec.it	#94     !sw	t1,0(a2)
    529e:	8082                	c.jr	ra

000052a0 <__riscv_save_12>:
    52a0:	7139                	c.addi16sp	sp,-64
    52a2:	4301                	c.li	t1,0
    52a4:	c66e                	c.swsp	s11,12(sp)
    52a6:	a019                	c.j	52ac <__riscv_save_10+0x4>

000052a8 <__riscv_save_10>:
    52a8:	7139                	c.addi16sp	sp,-64
    52aa:	5341                	c.li	t1,-16
    52ac:	c86a                	c.swsp	s10,16(sp)
    52ae:	ca66                	c.swsp	s9,20(sp)
    52b0:	cc62                	c.swsp	s8,24(sp)
    52b2:	ce5e                	c.swsp	s7,28(sp)
    52b4:	a019                	c.j	52ba <__riscv_save_4+0x4>

000052b6 <__riscv_save_4>:
    52b6:	7139                	c.addi16sp	sp,-64
    52b8:	5301                	c.li	t1,-32
    52ba:	d05a                	c.swsp	s6,32(sp)
    52bc:	d256                	c.swsp	s5,36(sp)
    52be:	d452                	c.swsp	s4,40(sp)
    52c0:	d64e                	c.swsp	s3,44(sp)
    52c2:	d84a                	c.swsp	s2,48(sp)
    52c4:	da26                	c.swsp	s1,52(sp)
    52c6:	dc22                	c.swsp	s0,56(sp)
    52c8:	de06                	c.swsp	ra,60(sp)
    52ca:	40610133          	sub	sp,sp,t1
    52ce:	8282                	c.jr	t0

000052d0 <__riscv_save_0>:
    52d0:	1141                	c.addi	sp,-16
    52d2:	c04a                	c.swsp	s2,0(sp)
    52d4:	c226                	c.swsp	s1,4(sp)
    52d6:	c422                	c.swsp	s0,8(sp)
    52d8:	c606                	c.swsp	ra,12(sp)
    52da:	8282                	c.jr	t0

000052dc <__riscv_restore_12>:
    52dc:	4db2                	c.lwsp	s11,12(sp)
    52de:	0141                	c.addi	sp,16

000052e0 <__riscv_restore_10>:
    52e0:	4d02                	c.lwsp	s10,0(sp)
    52e2:	4c92                	c.lwsp	s9,4(sp)
    52e4:	4c22                	c.lwsp	s8,8(sp)
    52e6:	4bb2                	c.lwsp	s7,12(sp)
    52e8:	0141                	c.addi	sp,16

000052ea <__riscv_restore_4>:
    52ea:	4b02                	c.lwsp	s6,0(sp)
    52ec:	4a92                	c.lwsp	s5,4(sp)
    52ee:	4a22                	c.lwsp	s4,8(sp)
    52f0:	49b2                	c.lwsp	s3,12(sp)
    52f2:	0141                	c.addi	sp,16

000052f4 <__riscv_restore_0>:
    52f4:	4902                	c.lwsp	s2,0(sp)
    52f6:	4492                	c.lwsp	s1,4(sp)
    52f8:	4422                	c.lwsp	s0,8(sp)
    52fa:	40b2                	c.lwsp	ra,12(sp)
    52fc:	0141                	c.addi	sp,16
    52fe:	8082                	c.jr	ra

00005300 <__libc_init_array>:
    5300:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    5302:	6499                	c.lui	s1,0x6
    5304:	6419                	c.lui	s0,0x6
    5306:	c4048793          	addi	a5,s1,-960 # 5c40 <__preinit_array_start>
    530a:	c4040413          	addi	s0,s0,-960 # 5c40 <__preinit_array_start>
    530e:	8c1d                	c.sub	s0,a5
    5310:	8409                	c.srai	s0,0x2
    5312:	c4048493          	addi	s1,s1,-960
    5316:	4901                	c.li	s2,0
    5318:	02891263          	bne	s2,s0,533c <__libc_init_array+0x3c>
    531c:	6499                	c.lui	s1,0x6
    531e:	ddbfa0ef          	jal	ra,f8 <_init>
    5322:	6419                	c.lui	s0,0x6
    5324:	c4048793          	addi	a5,s1,-960 # 5c40 <__preinit_array_start>
    5328:	c4040413          	addi	s0,s0,-960 # 5c40 <__preinit_array_start>
    532c:	8c1d                	c.sub	s0,a5
    532e:	8409                	c.srai	s0,0x2
    5330:	c4048493          	addi	s1,s1,-960
    5334:	4901                	c.li	s2,0
    5336:	00891863          	bne	s2,s0,5346 <__libc_init_array+0x46>
    533a:	bf6d                	c.j	52f4 <__riscv_restore_0>
    533c:	409c                	c.lw	a5,0(s1)
    533e:	0905                	c.addi	s2,1
    5340:	9782                	c.jalr	a5
    5342:	0491                	c.addi	s1,4
    5344:	bfd1                	c.j	5318 <__libc_init_array+0x18>
    5346:	409c                	c.lw	a5,0(s1)
    5348:	0905                	c.addi	s2,1
    534a:	9782                	c.jalr	a5
    534c:	0491                	c.addi	s1,4
    534e:	b7e5                	c.j	5336 <__libc_init_array+0x36>

00005350 <memcpy>:
    5350:	00a5c7b3          	xor	a5,a1,a0
    5354:	8b8d                	c.andi	a5,3
    5356:	00c508b3          	add	a7,a0,a2
    535a:	e7b9                	c.bnez	a5,53a8 <memcpy+0x58>
    535c:	478d                	c.li	a5,3
    535e:	04c7f563          	bgeu	a5,a2,53a8 <memcpy+0x58>
    5362:	00357713          	andi	a4,a0,3
    5366:	87aa                	c.mv	a5,a0
    5368:	04071c63          	bnez	a4,53c0 <memcpy+0x70>
    536c:	ffc8f813          	andi	a6,a7,-4
    5370:	fe080713          	addi	a4,a6,-32
    5374:	06e7e663          	bltu	a5,a4,53e0 <memcpy+0x90>
    5378:	0307f463          	bgeu	a5,a6,53a0 <memcpy+0x50>
    537c:	86ae                	c.mv	a3,a1
    537e:	873e                	c.mv	a4,a5
    5380:	4290                	c.lw	a2,0(a3)
    5382:	0711                	c.addi	a4,4
    5384:	fec72e23          	sw	a2,-4(a4)
    5388:	0691                	c.addi	a3,4
    538a:	ff076be3          	bltu	a4,a6,5380 <memcpy+0x30>
    538e:	fff7c713          	not	a4,a5
    5392:	983a                	c.add	a6,a4
    5394:	ffc87813          	andi	a6,a6,-4
    5398:	0811                	c.addi	a6,4
    539a:	97c2                	c.add	a5,a6
    539c:	010585b3          	add	a1,a1,a6
    53a0:	0117e863          	bltu	a5,a7,53b0 <memcpy+0x60>
    53a4:	00008067          	ret
    53a8:	ff157ee3          	bgeu	a0,a7,53a4 <memcpy+0x54>
    53ac:	00a007b3          	add	a5,zero,a0
    53b0:	8874                	exec.it	#61     !lbu	a4,0(a1)
    53b2:	0785                	c.addi	a5,1
    53b4:	8614                	exec.it	#75     !sb	a4,-1(a5)
    53b6:	0585                	c.addi	a1,1
    53b8:	ff17ece3          	bltu	a5,a7,53b0 <memcpy+0x60>
    53bc:	00008067          	ret
    53c0:	8874                	exec.it	#61     !lbu	a4,0(a1)
    53c2:	0785                	c.addi	a5,1
    53c4:	8614                	exec.it	#75     !sb	a4,-1(a5)
    53c6:	0037f713          	andi	a4,a5,3
    53ca:	0585                	c.addi	a1,1
    53cc:	d345                	c.beqz	a4,536c <memcpy+0x1c>
    53ce:	8874                	exec.it	#61     !lbu	a4,0(a1)
    53d0:	0785                	c.addi	a5,1
    53d2:	8614                	exec.it	#75     !sb	a4,-1(a5)
    53d4:	0037f713          	andi	a4,a5,3
    53d8:	0585                	c.addi	a1,1
    53da:	f37d                	c.bnez	a4,53c0 <memcpy+0x70>
    53dc:	f91ff06f          	j	536c <memcpy+0x1c>
    53e0:	0005a383          	lw	t2,0(a1)
    53e4:	0045a283          	lw	t0,4(a1)
    53e8:	0085af83          	lw	t6,8(a1)
    53ec:	00c5af03          	lw	t5,12(a1)
    53f0:	0105ae83          	lw	t4,16(a1)
    53f4:	0145ae03          	lw	t3,20(a1)
    53f8:	0185a303          	lw	t1,24(a1)
    53fc:	4dd0                	c.lw	a2,28(a1)
    53fe:	02458593          	addi	a1,a1,36
    5402:	ffc5a683          	lw	a3,-4(a1)
    5406:	02478793          	addi	a5,a5,36
    540a:	fc77ae23          	sw	t2,-36(a5)
    540e:	fe57a023          	sw	t0,-32(a5)
    5412:	fff7a223          	sw	t6,-28(a5)
    5416:	ffe7a423          	sw	t5,-24(a5)
    541a:	ffd7a623          	sw	t4,-20(a5)
    541e:	ffc7a823          	sw	t3,-16(a5)
    5422:	fe67aa23          	sw	t1,-12(a5)
    5426:	fec7ac23          	sw	a2,-8(a5)
    542a:	fed7ae23          	sw	a3,-4(a5)
    542e:	fae7e9e3          	bltu	a5,a4,53e0 <memcpy+0x90>
    5432:	b799                	c.j	5378 <memcpy+0x28>
    5434:	0000                	unimp
	...

00005438 <memmove>:
    5438:	0e060663          	beqz	a2,5524 <memmove+0xec>
    543c:	e7bff2ef          	jal	t0,52b6 <__riscv_save_4>
    5440:	4401                	c.li	s0,0
    5442:	84aa                	c.mv	s1,a0
    5444:	00b4c663          	blt	s1,a1,5450 <memmove+0x18>
    5448:	94b2                	c.add	s1,a2
    544a:	95b2                	c.add	a1,a2
    544c:	4681                	c.li	a3,0
    544e:	a011                	c.j	5452 <memmove+0x1a>
    5450:	4685                	c.li	a3,1
    5452:	0095e733          	or	a4,a1,s1
    5456:	8b0d                	c.andi	a4,3
    5458:	eb41                	c.bnez	a4,54e8 <memmove+0xb0>
    545a:	4751                	c.li	a4,20
    545c:	00e638b3          	sltu	a7,a2,a4
    5460:	00088663          	beqz	a7,546c <memmove+0x34>
    5464:	a051                	c.j	54e8 <memmove+0xb0>
    5466:	4401                	c.li	s0,0
    5468:	40c90633          	sub	a2,s2,a2
    546c:	00465713          	srli	a4,a2,0x4
    5470:	0712                	c.slli	a4,0x4
    5472:	00f67793          	andi	a5,a2,15
    5476:	00e58633          	add	a2,a1,a4
    547a:	40e58733          	sub	a4,a1,a4
    547e:	1771                	c.addi	a4,-4
    5480:	00069363          	bnez	a3,5486 <memmove+0x4e>
    5484:	863a                	c.mv	a2,a4
    5486:	ea9d                	c.bnez	a3,54bc <memmove+0x84>
    5488:	15f1                	c.addi	a1,-4
    548a:	14f1                	c.addi	s1,-4
    548c:	ff45a983          	lw	s3,-12(a1)
    5490:	ff85aa03          	lw	s4,-8(a1)
    5494:	ffc5aa83          	lw	s5,-4(a1)
    5498:	0005ab03          	lw	s6,0(a1)
    549c:	15c1                	c.addi	a1,-16
    549e:	14c1                	c.addi	s1,-16
    54a0:	0134a223          	sw	s3,4(s1)
    54a4:	0144a423          	sw	s4,8(s1)
    54a8:	0154a623          	sw	s5,12(s1)
    54ac:	0164a823          	sw	s6,16(s1)
    54b0:	fcc59ee3          	bne	a1,a2,548c <memmove+0x54>
    54b4:	0491                	c.addi	s1,4
    54b6:	0591                	c.addi	a1,4
    54b8:	02c0006f          	j	54e4 <memmove+0xac>
    54bc:	0005a983          	lw	s3,0(a1)
    54c0:	0045aa03          	lw	s4,4(a1)
    54c4:	0085aa83          	lw	s5,8(a1)
    54c8:	00c5ab03          	lw	s6,12(a1)
    54cc:	05c1                	c.addi	a1,16
    54ce:	04c1                	c.addi	s1,16
    54d0:	ff34a823          	sw	s3,-16(s1)
    54d4:	ff44aa23          	sw	s4,-12(s1)
    54d8:	ff54ac23          	sw	s5,-8(s1)
    54dc:	ff64ae23          	sw	s6,-4(s1)
    54e0:	fcc59ee3          	bne	a1,a2,54bc <memmove+0x84>
    54e4:	cf95                	c.beqz	a5,5520 <memmove+0xe8>
    54e6:	863e                	c.mv	a2,a5
    54e8:	4781                	c.li	a5,0
    54ea:	ca15                	c.beqz	a2,551e <memmove+0xe6>
    54ec:	00068c63          	beqz	a3,5504 <memmove+0xcc>
    54f0:	0005c703          	lbu	a4,0(a1)
    54f4:	0585                	c.addi	a1,1
    54f6:	00e48023          	sb	a4,0(s1)
    54fa:	0485                	c.addi	s1,1
    54fc:	0785                	c.addi	a5,1
    54fe:	fec799e3          	bne	a5,a2,54f0 <memmove+0xb8>
    5502:	a831                	c.j	551e <memmove+0xe6>
    5504:	15fd                	c.addi	a1,-1
    5506:	14fd                	c.addi	s1,-1
    5508:	0005c703          	lbu	a4,0(a1)
    550c:	15fd                	c.addi	a1,-1
    550e:	00e48023          	sb	a4,0(s1)
    5512:	14fd                	c.addi	s1,-1
    5514:	0785                	c.addi	a5,1
    5516:	fec799e3          	bne	a5,a2,5508 <memmove+0xd0>
    551a:	0585                	c.addi	a1,1
    551c:	0485                	c.addi	s1,1
    551e:	f421                	c.bnez	s0,5466 <memmove+0x2e>
    5520:	dcbff06f          	j	52ea <__riscv_restore_4>
    5524:	8082                	c.jr	ra

00005526 <memset>:
    5526:	433d                	c.li	t1,15
    5528:	872a                	c.mv	a4,a0
    552a:	02c37363          	bgeu	t1,a2,5550 <memset+0x2a>
    552e:	00f77793          	andi	a5,a4,15
    5532:	e3c1                	c.bnez	a5,55b2 <memset+0x8c>
    5534:	e5b5                	c.bnez	a1,55a0 <memset+0x7a>
    5536:	ff067693          	andi	a3,a2,-16
    553a:	8a3d                	c.andi	a2,15
    553c:	96ba                	c.add	a3,a4
    553e:	c30c                	c.sw	a1,0(a4)
    5540:	c34c                	c.sw	a1,4(a4)
    5542:	c70c                	c.sw	a1,8(a4)
    5544:	c74c                	c.sw	a1,12(a4)
    5546:	0741                	c.addi	a4,16
    5548:	fed76be3          	bltu	a4,a3,553e <memset+0x18>
    554c:	e211                	c.bnez	a2,5550 <memset+0x2a>
    554e:	8082                	c.jr	ra
    5550:	40c306b3          	sub	a3,t1,a2
    5554:	068a                	c.slli	a3,0x2
    5556:	00000297          	auipc	t0,0x0
    555a:	005686b3          	add	a3,a3,t0
    555e:	00c68067          	jr	12(a3)
    5562:	00b70723          	sb	a1,14(a4)
    5566:	00b706a3          	sb	a1,13(a4)
    556a:	00b70623          	sb	a1,12(a4)
    556e:	00b705a3          	sb	a1,11(a4)
    5572:	00b70523          	sb	a1,10(a4)
    5576:	00b704a3          	sb	a1,9(a4)
    557a:	00b70423          	sb	a1,8(a4)
    557e:	00b703a3          	sb	a1,7(a4)
    5582:	00b70323          	sb	a1,6(a4)
    5586:	00b702a3          	sb	a1,5(a4)
    558a:	00b70223          	sb	a1,4(a4)
    558e:	00b701a3          	sb	a1,3(a4)
    5592:	00b70123          	sb	a1,2(a4)
    5596:	00b700a3          	sb	a1,1(a4)
    559a:	00b70023          	sb	a1,0(a4)
    559e:	8082                	c.jr	ra
    55a0:	0ff5f593          	andi	a1,a1,255
    55a4:	00859693          	slli	a3,a1,0x8
    55a8:	8dd5                	c.or	a1,a3
    55aa:	01059693          	slli	a3,a1,0x10
    55ae:	8dd5                	c.or	a1,a3
    55b0:	b759                	c.j	5536 <memset+0x10>
    55b2:	00279693          	slli	a3,a5,0x2
    55b6:	00000297          	auipc	t0,0x0
    55ba:	9696                	c.add	a3,t0
    55bc:	8286                	c.mv	t0,ra
    55be:	fa8680e7          	jalr	-88(a3)
    55c2:	8096                	c.mv	ra,t0
    55c4:	17c1                	c.addi	a5,-16
    55c6:	8f1d                	c.sub	a4,a5
    55c8:	963e                	c.add	a2,a5
    55ca:	f8c373e3          	bgeu	t1,a2,5550 <memset+0x2a>
    55ce:	b79d                	c.j	5534 <memset+0xe>

000055d0 <snprintf>:
    55d0:	7139                	c.addi16sp	sp,-64
    55d2:	d636                	c.swsp	a3,44(sp)
    55d4:	1074                	c.addi4spn	a3,sp,44
    55d6:	ce06                	c.swsp	ra,28(sp)
    55d8:	d83a                	c.swsp	a4,48(sp)
    55da:	da3e                	c.swsp	a5,52(sp)
    55dc:	dc42                	c.swsp	a6,56(sp)
    55de:	de46                	c.swsp	a7,60(sp)
    55e0:	c636                	c.swsp	a3,12(sp)
    55e2:	2045                	c.jal	5682 <vsnprintf>
    55e4:	40f2                	c.lwsp	ra,28(sp)
    55e6:	6121                	c.addi16sp	sp,64
    55e8:	8082                	c.jr	ra
	...

000055ec <strlen>:
    55ec:	00357793          	andi	a5,a0,3
    55f0:	872a                	c.mv	a4,a0
    55f2:	e79d                	c.bnez	a5,5620 <strlen+0x34>
    55f4:	0511                	c.addi	a0,4
    55f6:	ffc52783          	lw	a5,-4(a0)
    55fa:	200787db          	ffb	a5,a5,zero
    55fe:	dbfd                	c.beqz	a5,55f4 <strlen+0x8>
    5600:	ffc54783          	lbu	a5,-4(a0)
    5604:	40e50733          	sub	a4,a0,a4
    5608:	ffd54683          	lbu	a3,-3(a0)
    560c:	ffe54603          	lbu	a2,-2(a0)
    5610:	c795                	c.beqz	a5,563c <strlen+0x50>
    5612:	c28d                	c.beqz	a3,5634 <strlen+0x48>
    5614:	ca05                	c.beqz	a2,5644 <strlen+0x58>
    5616:	fff70513          	addi	a0,a4,-1
    561a:	8082                	c.jr	ra
    561c:	fc068ce3          	beqz	a3,55f4 <strlen+0x8>
    5620:	00054783          	lbu	a5,0(a0)
    5624:	0505                	c.addi	a0,1
    5626:	00357693          	andi	a3,a0,3
    562a:	fbed                	c.bnez	a5,561c <strlen+0x30>
    562c:	8d19                	c.sub	a0,a4
    562e:	157d                	c.addi	a0,-1
    5630:	00008067          	ret
    5634:	ffd70513          	addi	a0,a4,-3
    5638:	00008067          	ret
    563c:	ffc70513          	addi	a0,a4,-4
    5640:	00008067          	ret
    5644:	ffe70513          	addi	a0,a4,-2
    5648:	8082                	c.jr	ra
	...

0000564c <vsnprintf_help>:
    564c:	cf018793          	addi	a5,gp,-784 # 604c <countNow>
    5650:	4190                	c.lw	a2,0(a1)
    5652:	0007a803          	lw	a6,0(a5)
    5656:	cec1a883          	lw	a7,-788(gp) # 6048 <count>
    565a:	00160693          	addi	a3,a2,1
    565e:	00180713          	addi	a4,a6,1
    5662:	01181863          	bne	a6,a7,5672 <vsnprintf_help+0x26>
    5666:	00060023          	sb	zero,0(a2)
    566a:	c194                	c.sw	a3,0(a1)
    566c:	c398                	c.sw	a4,0(a5)
    566e:	557d                	c.li	a0,-1
    5670:	8082                	c.jr	ra
    5672:	ff08cce3          	blt	a7,a6,566a <vsnprintf_help+0x1e>
    5676:	00a60023          	sb	a0,0(a2)
    567a:	c194                	c.sw	a3,0(a1)
    567c:	c398                	c.sw	a4,0(a5)
    567e:	4501                	c.li	a0,0
    5680:	8082                	c.jr	ra

00005682 <vsnprintf>:
    5682:	8800                	exec.it	#4     !jal	t0,52d0 <__riscv_save_0>
    5684:	87b6                	c.mv	a5,a3
    5686:	842a                	c.mv	s0,a0
    5688:	cec18493          	addi	s1,gp,-788 # 6048 <count>
    568c:	8532                	c.mv	a0,a2
    568e:	15fd                	c.addi	a1,-1
    5690:	00005637          	lui	a2,0x5
    5694:	c08c                	c.sw	a1,0(s1)
    5696:	cf018913          	addi	s2,gp,-784 # 604c <countNow>
    569a:	85be                	c.mv	a1,a5
    569c:	86a2                	c.mv	a3,s0
    569e:	64c60613          	addi	a2,a2,1612 # 564c <vsnprintf_help>
    56a2:	00092023          	sw	zero,0(s2)
    56a6:	8fafe0ef          	jal	ra,37a0 <do_printf>
    56aa:	00092703          	lw	a4,0(s2)
    56ae:	409c                	c.lw	a5,0(s1)
    56b0:	00e7c563          	blt	a5,a4,56ba <vsnprintf+0x38>
    56b4:	942a                	c.add	s0,a0
    56b6:	00040023          	sb	zero,0(s0)
    56ba:	b92d                	c.j	52f4 <__riscv_restore_0>

Disassembly of section .exec.itable:

000056bc <_ITB_BASE_>:
    56bc:	2d0052ef          	jal	t0,PC(31,21)|#0x52d0
    56c0:	220050ef          	jal	ra,PC(31,21)|#0x5220
    56c4:	551020ef          	jal	ra,PC(31,21)|#0x2d50
    56c8:	2f40506f          	j	PC(31,21)|#0x52f4
    56cc:	2d0052ef          	jal	t0,PC(31,21)|#0x52d0
    56d0:	55d020ef          	jal	ra,PC(31,21)|#0x2d5c
    56d4:	2f40506f          	j	PC(31,21)|#0x52f4
    56d8:	c741a503          	lw	a0,-908(gp) # 5fd0 <pD>
    56dc:	2d0052ef          	jal	t0,PC(31,21)|#0x52d0
    56e0:	438050ef          	jal	ra,PC(31,21)|#0x5438
    56e4:	2f40506f          	j	PC(31,21)|#0x52f4
    56e8:	2d0052ef          	jal	t0,PC(31,21)|#0x52d0
    56ec:	70e040ef          	jal	ra,PC(31,21)|#0x470e
    56f0:	4d1030ef          	jal	ra,PC(31,21)|#0x3cd0
    56f4:	557020ef          	jal	ra,PC(31,21)|#0x2d56
    56f8:	2f40506f          	j	PC(31,21)|#0x52f4
    56fc:	539020ef          	jal	ra,PC(31,21)|#0x2d38
    5700:	c841a503          	lw	a0,-892(gp) # 5fe0 <scsi_data>
    5704:	6f6040ef          	jal	ra,PC(31,21)|#0x46f6
    5708:	526050ef          	jal	ra,PC(31,21)|#0x5526
    570c:	2b6052ef          	jal	t0,PC(31,21)|#0x52b6
    5710:	2b6052ef          	jal	t0,PC(31,21)|#0x52b6
    5714:	350050ef          	jal	ra,PC(31,21)|#0x5350
    5718:	154050ef          	jal	ra,PC(31,21)|#0x5154
    571c:	2ea0506f          	j	PC(31,21)|#0x52ea
    5720:	c641a583          	lw	a1,-924(gp) # 5fc0 <epIn>
    5724:	00001517          	auipc	a0,0x1
    5728:	00000013          	nop
    572c:	1eb040ef          	jal	ra,PC(31,21)|#0x49ea
    5730:	459030ef          	jal	ra,PC(31,21)|#0x3c58
    5734:	473030ef          	jal	ra,PC(31,21)|#0x3c72
    5738:	2ea0506f          	j	PC(31,21)|#0x52ea
    573c:	2b6052ef          	jal	t0,PC(31,21)|#0x52b6
    5740:	2ea0506f          	j	PC(31,21)|#0x52ea
    5744:	6109a503          	lw	a0,1552(s3)
    5748:	6104a503          	lw	a0,1552(s1)
    574c:	08000593          	li	a1,128
    5750:	00440b93          	addi	s7,s0,4
    5754:	c681a583          	lw	a1,-920(gp) # 5fc4 <epOut>
    5758:	00002517          	auipc	a0,0x2
    575c:	00000013          	nop
    5760:	54d020ef          	jal	ra,PC(31,21)|#0x2d4c
    5764:	75f040ef          	jal	ra,PC(31,21)|#0x4f5e
    5768:	67b040ef          	jal	ra,PC(31,21)|#0x4e7a
    576c:	61042503          	lw	a0,1552(s0)
    5770:	0984a583          	lw	a1,152(s1)
    5774:	08600513          	li	a0,134
    5778:	08442503          	lw	a0,132(s0)
    577c:	04000593          	li	a1,64
    5780:	c8c1a783          	lw	a5,-884(gp) # 5fe8 <pxCurrentTCB>
    5784:	495030ef          	jal	ra,PC(31,21)|#0x3c94
    5788:	fff50593          	addi	a1,a0,-1 # 7757 <__global_pointer$+0x13fb>
    578c:	700002b7          	lui	t0,0x70000
    5790:	30047073          	csrci	mstatus,8
    5794:	09892583          	lw	a1,152(s2)
    5798:	08442283          	lw	t0,132(s0)
    579c:	02440513          	addi	a0,s0,36
    57a0:	02009023          	sh	zero,32(ra)
    57a4:	01040513          	addi	a0,s0,16
    57a8:	006503b3          	add	t2,a0,t1
    57ac:	00108293          	addi	t0,ra,1
    57b0:	0005c703          	lbu	a4,0(a1)
    57b4:	00042c23          	sw	zero,24(s0)
    57b8:	c581a783          	lw	a5,-936(gp) # 5fb4 <drv>
    57bc:	c8c1a703          	lw	a4,-884(gp) # 5fe8 <pxCurrentTCB>
    57c0:	c4c1a603          	lw	a2,-948(gp) # 5fa8 <bulkOutReq>
    57c4:	c481a603          	lw	a2,-952(gp) # 5fa4 <bulkInReq>
    57c8:	c5c1a503          	lw	a0,-932(gp) # 5fb8 <ep0Buff>
    57cc:	c6018d23          	sb	zero,-902(gp) # 5fd6 <packet_sent>
    57d0:	5c6040ef          	jal	ra,PC(31,21)|#0x45c6
    57d4:	47a000ef          	jal	ra,PC(31,21)|#0x47a
    57d8:	437030ef          	jal	ra,PC(31,21)|#0x3c36
    57dc:	fff48593          	addi	a1,s1,-1
    57e0:	fff38513          	addi	a0,t2,-1
    57e4:	fff30393          	addi	t2,t1,-1
    57e8:	fee78fa3          	sb	a4,-1(a5)
    57ec:	300467f3          	csrrsi	a5,mstatus,8
    57f0:	1a350513          	addi	a0,a0,419
    57f4:	0a058023          	sb	zero,160(a1)
    57f8:	08452503          	lw	a0,132(a0)
    57fc:	0844a603          	lw	a2,132(s1)
    5800:	08442e83          	lw	t4,132(s0)
    5804:	08442e03          	lw	t3,132(s0)
    5808:	0705a283          	lw	t0,112(a1)
    580c:	06100813          	li	a6,97
    5810:	0407a303          	lw	t1,64(a5)
    5814:	040402a3          	sb	zero,69(s0)
    5818:	04040223          	sb	zero,68(s0)
    581c:	02f50533          	mul	a0,a0,a5
    5820:	02d0a423          	sw	a3,40(ra)
    5824:	024a2803          	lw	a6,36(s4)
    5828:	01840513          	addi	a0,s0,24
    582c:	00d408b3          	add	a7,s0,a3
    5830:	00742623          	sw	t2,12(s0)
    5834:	00662023          	sw	t1,0(a2)
    5838:	00572023          	sw	t0,0(a4)
    583c:	00550333          	add	t1,a0,t0
    5840:	00440513          	addi	a0,s0,4
    5844:	000e2e83          	lw	t4,0(t3)
    5848:	0005a023          	sw	zero,0(a1)
    584c:	00042883          	lw	a7,0(s0)
    5850:	c541c883          	lbu	a7,-940(gp) # 5fb0 <configValue>
    5854:	c551c703          	lbu	a4,-939(gp) # 5fb1 <data_dir>
    5858:	cd01a783          	lw	a5,-816(gp) # 602c <xTimerQueue>
    585c:	cb81a783          	lw	a5,-840(gp) # 6014 <xSchedulerRunning>
    5860:	ca41a783          	lw	a5,-860(gp) # 6000 <uxSchedulerSuspended>
    5864:	c6c1a383          	lw	t2,-916(gp) # 5fc8 <host_num_of_bytes>
    5868:	c8c1a303          	lw	t1,-884(gp) # 5fe8 <pxCurrentTCB>
    586c:	c8c1a083          	lw	ra,-884(gp) # 5fe8 <pxCurrentTCB>
    5870:	c2418593          	addi	a1,gp,-988 # 5f80 <endpointEpOutDesc>
    5874:	c1418593          	addi	a1,gp,-1004 # 5f70 <compDesc>
    5878:	c6018ca3          	sb	zero,-903(gp) # 5fd5 <packet_received>
    587c:	c6018823          	sb	zero,-912(gp) # 5fcc <msc_failed_flag>
    5880:	6af027ab          	lwgp	a5,134828 # 27208 <dev_num_of_bytes>
    5884:	2b6052ef          	jal	t0,PC(31,21)|#0x52b6
    5888:	656040ef          	jal	ra,PC(31,21)|#0x4656
    588c:	79a030ef          	jal	ra,PC(31,21)|#0x379a
    5890:	474000ef          	jal	ra,PC(31,21)|#0x474
    5894:	5d0050ef          	jal	ra,PC(31,21)|#0x55d0
    5898:	3d0040ef          	jal	ra,PC(31,21)|#0x43d0
    589c:	543020ef          	jal	ra,PC(31,21)|#0x2d42
    58a0:	5de000ef          	jal	ra,PC(31,21)|#0x5de
    58a4:	263010ef          	jal	ra,PC(31,21)|#0x1a62
    58a8:	2ea0506f          	j	PC(31,21)|#0x52ea
