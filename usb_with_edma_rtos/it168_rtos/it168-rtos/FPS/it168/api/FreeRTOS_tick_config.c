/*
 * FreeRTOS Kernel V10.1.1
 * Copyright (C) 2018 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */

/* FreeRTOS includes. */
#include "FreeRTOS.h"

/* Platform includes. */
#include "platform.h"

/*-----------------------------------------------------------*/

/*
 * The application must provide a function that configures a peripheral to
 * create the FreeRTOS tick interrupt, then define configSETUP_TICK_INTERRUPT()
 * in FreeRTOSConfig.h to call the function. This file contains a function
 * that is suitable for use on the Andes AE210P.
 */
void vConfigureTickInterrupt( void )
{
	/* Enable Channel 0, 32-bit Timer with APB clock source. */
	DEV_PIT->CHANNEL[0].CTRL = 0x08 | 0x01;
	DEV_PIT->CHANNEL[0].RELOAD = ( PCLKFREQ / configTICK_RATE_HZ );
	DEV_PIT->INTEN |= 1;
	DEV_PIT->CHNEN |= 1;

	/* Priority must be set > 0 to trigger the interrupt */
//	__nds__plic_set_priority(IRQ_PIT_SOURCE, 1);

	/* Enable PLIC interrupt PIT source */
//	__nds__plic_enable_interrupt(IRQ_PIT_SOURCE);
}
/*-----------------------------------------------------------*/

void vClearTickInterrupt( void )
{
	DEV_PIT->INTST = 1;
}
/*-----------------------------------------------------------*/
