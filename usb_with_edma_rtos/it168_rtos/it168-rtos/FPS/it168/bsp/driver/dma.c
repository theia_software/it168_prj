
#include "dma.h"


//--------------------------------------------
void edma_Init(U32 ch, U32 src_addr, U32 dst_addr)
{
	RW_DMA_REG(DMA_CH_N_SRC_ADDR(ch)) = src_addr;
	RW_DMA_REG(DMA_CH_N_DST_ADDR(ch)) = dst_addr;
	RW_DMA_REG(DMA_CH_N_TRANS_SIZE(ch)) = 0;
	RW_DMA_REG(DMA_CH_N_CTRL(ch)) = 0;
}
//--------------------------------------------
// The total number of transferred bytes is TranSize * SrcWidth.
void edma_SetSrcParams(U32 ch, U32 trans_size, U32 burst_size, U32 width, U32 mode, U32 addctl, U32 reqsel)
{
	RW_DMA_REG(DMA_CH_N_TRANS_SIZE(ch)) = trans_size;
	RW_DMA_REG(DMA_CH_N_CTRL(ch)) |= (burst_size << 22)
								  |  (width << 20)
								  |  (mode << 17)
								  |  (addctl << 14)
								  |  (reqsel << 8);
}
//--------------------------------------------
void edma_SetDstParams(U32 ch, U32 width, U32 mode, U32 addctl, U32 reqsel)
{
	RW_DMA_REG(DMA_CH_N_CTRL(ch)) |= (width << 18)
								  |  (mode << 16)
								  |  (addctl << 12)
								  |  (reqsel << 4);
}
//--------------------------------------------
void edma_Start(U32 ch, U32 priority)
{
	// all interrupt are enabled
	RW_DMA_REG(DMA_CH_N_CTRL(ch)) |= (priority << 29)
								  |  (0x01 <<0);	//enable DMA

	//for debug
//	*((volatile U32*)0x00011000) = RW_DMA_REG(DMA_CH_N_SRC_ADDR(ch));
//	*((volatile U32*)0x00011010) = RW_DMA_REG(DMA_CH_N_DST_ADDR(ch));
//	*((volatile U32*)0x00011020) = RW_DMA_REG(DMA_CH_N_CTRL(ch));
//	*((volatile U32*)0x00011030) = RW_DMA_REG(DMA_CH_N_TRANS_SIZE(ch));
}
//--------------------------------------------
void edma_Stop(U32 ch)
{
	RW_DMA_REG(DMA_CH_N_CTRL(ch)) = 0;
}

//--------------------------------------------
U32 edma_Check_sts( )
{
	return RW_DMA_REG(DMA_INT_STATUS);
//	while (1) {
////		printf("TC%x\n", (RW_DMA_REG(DMA_INT_STATUS) >> (16 + ch)) & 1);
////		printf("TC abort %x\n", (RW_DMA_REG(DMA_INT_STATUS) >> (8 + ch)) & 1);
////		printf("TC err%x\n", (RW_DMA_REG(DMA_INT_STATUS) >> ch) & 1);
//		if ((RW_DMA_REG(DMA_INT_STATUS) >> (16 + ch)) & 1 == 1) {
//			break;
//		}
//	}
}

void edma_clear_sts( )
{
	RW_DMA_REG(DMA_INT_STATUS) = 0x10101;
}


