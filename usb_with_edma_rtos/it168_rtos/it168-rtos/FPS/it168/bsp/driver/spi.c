#include <ae250.h>
#include "spi.h"

#define REF_SPI_CLK				10*1024*1024
#define	INT_SPI_DEBUG_ENABLE	1

static U8 spi_isr_no[3] = {IRQ_SPI0_SOURCE, IRQ_SPI1_SOURCE, IRQ_SPI2_SOURCE};
U32 spi_irq_outvalue;
volatile U32 *debugSpiPtr;

//------------------------------------------------
void spi_Open(TSPIPara* pTSPIPara)
{
	U8 div;

	div = (REF_SPI_CLK/pTSPIPara->Clk/2)-1;

	RW_SPI_REG(SPI_TRANS_FMT, pTSPIPara->tSPIID) = ((pTSPIPara->AddrLen) <<16)
												 | ((pTSPIPara->Bit-1) << 8)
												 | (pTSPIPara->DataMerge << 7)
												 | (pTSPIPara->Lsb << 3)
												 | (pTSPIPara->SlvMode << 2)
												 | (pTSPIPara->Pol << 1)
												 | (pTSPIPara->Phs << 0);

	if(pTSPIPara->SlvMode == MASTER_MODE){
		RW_SPI_REG(SPI_TIMING, pTSPIPara->tSPIID) = (0x01 << 12)
			                    			      | (div << 0);
	}else{
		RW_SPI_REG(SPI_TIMING, pTSPIPara->tSPIID) = 0;
	}
}
//----------------------------------------------------------------------------
void spi_testISR(TSPIID id)
{
	U32 status = 0;

	status = RW_SPI_REG(SPI_INTR_ST, id);

#if INT_SPI_DEBUG_ENABLE == 1
	spi_irq_outvalue++;
	*debugSpiPtr = (spi_irq_outvalue << 16) | (id << 8) | (status & 0xFF);
#endif
}
//-------------------------------------------------------------
void spi_testISR0(void){ spi_testISR(SPI0); }
void spi_testISR1(void){ spi_testISR(SPI1); }
void spi_testISR2(void){ spi_testISR(SPI2); }
//-------------------------------------------------------------
void spi_RegisterISR(TSPIPara* pTSPIPara, void (*callback)())
{
#if INT_SPI_DEBUG_ENABLE == 1
	spi_irq_outvalue = 0;
	*(volatile U32*)(0x0001FF10) = 0;
	debugSpiPtr = (volatile U32*)(0x0001FF10);
#endif

	registerISR(spi_isr_no[pTSPIPara->tSPIID], callback);
}
//---------------------------------------------------
void spi_CheckSPIFinish(TSPIPara* pTSPIPara)
{
	U32 ret;
	ret = 0xFF;

	while(ret%2 == 1){
		ret = RW_SPI_REG(SPI_STATUS, pTSPIPara->tSPIID);
	}

	return;
}
//-------------------------------------------------------------
void spi_Master_ReadByte(TSPIPara* pTSPIPara, U8 reg, U32 buf, U32 size)
{
	U32 status, _size, i;
	volatile U8* pBuf_8;
	volatile U32* pBuf_32;

	if(pTSPIPara->DataMerge == DATAMERGE_ON)
		_size = size/4;
	else
		_size = size;

	i=_size;

	spi_CheckSPIFinish(pTSPIPara);

	RW_SPI_REG(SPI_TRANS_CTRL, pTSPIPara->tSPIID) = (0x01 << 30)	//enable command phase
												  | (0x09 << 24)	//dummy, read transfer mode
												  | (((size&0x1FF)-1) << 0); //data receive count

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pTSPIPara->tSPIID) = (size>>9);

	RW_SPI_REG(SPI_CTRL, pTSPIPara->tSPIID) = 0;					//disable DMA
	RW_SPI_REG(SPI_INTR_EN, pTSPIPara->tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT

	RW_SPI_REG(SPI_CMD, pTSPIPara->tSPIID) = reg;

	if(pTSPIPara->DataMerge == DATAMERGE_ON)
		pBuf_32 = (volatile U32*)buf;
	else
		pBuf_8 = (volatile U8*)buf;

	do{
		status = 0xFFFFFFFF;
		// check RXEMPTY
		while(status & (1<<14) != 0)
		{
			status = RW_SPI_REG(SPI_STATUS, pTSPIPara->tSPIID);
		}

		if(pTSPIPara->DataMerge == DATAMERGE_ON)
			pBuf_32[_size-i] = RW_SPI_REG(SPI_DATA, pTSPIPara->tSPIID);
		else
			pBuf_8[_size-i] = RW_SPI_REG(SPI_DATA, pTSPIPara->tSPIID);
	}while(--i);
}
//-------------------------------------------
void spi_Master_WriteByte(TSPIPara* pTSPIPara, U8 reg, U32 buf, U32 size)
{
	U32 status, _size, i;
	volatile U8* pBuf_8;
	volatile U32* pBuf_32;

	if(pTSPIPara->DataMerge == DATAMERGE_ON)
		_size = size/4;
	else
		_size = size;

	i=_size;

	spi_CheckSPIFinish(pTSPIPara);

	RW_SPI_REG(SPI_TRANS_CTRL, pTSPIPara->tSPIID) = (0x01 << 30)		//enable command phase
												  | (0x08 << 24)		//dummy, write transfer mode
												  | (((size&0x1FF)-1) << 12); //data transfer count

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pTSPIPara->tSPIID) = (size>>9);

	RW_SPI_REG(SPI_CTRL, pTSPIPara->tSPIID) = 0;					//disable DMA
	RW_SPI_REG(SPI_INTR_EN, pTSPIPara->tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT

	RW_SPI_REG(SPI_CMD, pTSPIPara->tSPIID) = reg;

	if(pTSPIPara->DataMerge == DATAMERGE_ON)
		pBuf_32 = (volatile U32*)buf;
	else
		pBuf_8 = (volatile U8*)buf;

	do{
		status = 0;
		// check TXEMPTY
		while(status & (1<<22) != 1)
		{
			status = RW_SPI_REG(SPI_STATUS, pTSPIPara->tSPIID);
		}

		if(pTSPIPara->DataMerge == DATAMERGE_ON)
			RW_SPI_REG(SPI_DATA, pTSPIPara->tSPIID) = pBuf_32[_size-i];
		else
			RW_SPI_REG(SPI_DATA, pTSPIPara->tSPIID) = pBuf_8[_size-i];
	}while(--i);
}

//-------------------------------------------------------------
void spi_Slave_ReadByte(TSPIPara* pTSPIPara, U8* reg, U32 buf, U32 size)
{
	U32 status, _size, i;
	volatile U8* pBuf_8;
	volatile U32* pBuf_32;

	if(pTSPIPara->DataMerge == DATAMERGE_ON)
		_size = size/4;
	else
		_size = size;

	i=_size;

	//reset RXFIFO
	RW_SPI_REG(SPI_CTRL, pTSPIPara->tSPIID) = (0x01 << 1);
	// check RXFIFO reset
	status = 0xFFFFFFFF;
	while(status & (1<<1) != 0)
	{
		status = RW_SPI_REG(SPI_CTRL, pTSPIPara->tSPIID);
	}

	RW_SPI_REG(SPI_CTRL, pTSPIPara->tSPIID) = (0x02 << 8);		//receive FIFO threshold

	RW_SPI_REG(SPI_INTR_EN, pTSPIPara->tSPIID) =(0x01 << 5)		//SlvCmdEn
											   |(0x01 << 4)		//enable the end of SPI transfer INT
											   |(0x01 << 2);	//RXFIFOIntEn
	// check SlvCmdInt
	status = 0;
	while(status & (1<<5) != 1)
	{
		status = RW_SPI_REG(SPI_INTR_ST, pTSPIPara->tSPIID);
	}
	*reg = RW_SPI_REG(SPI_CMD, pTSPIPara->tSPIID);
	RW_SPI_REG(SPI_INTR_ST, pTSPIPara->tSPIID) |= (1<<5);

	if(pTSPIPara->DataMerge == DATAMERGE_ON)
		pBuf_32 = (volatile U32*)buf;
	else
		pBuf_8 = (volatile U8*)buf;

	status = 0xFFFFFFFF;
	// check RXEMPTY
	while(status & (1<<14) != 0)
	{
		status = RW_SPI_REG(SPI_STATUS, pTSPIPara->tSPIID);
	}

	do{
		if(pTSPIPara->DataMerge == DATAMERGE_ON)
			pBuf_32[_size-i] = RW_SPI_REG(SPI_DATA, pTSPIPara->tSPIID);
		else
			pBuf_8[_size-i] = RW_SPI_REG(SPI_DATA, pTSPIPara->tSPIID);
	}while(--i);
}
//-------------------------------------------
void spi_Slave_WriteByte(TSPIPara* pTSPIPara, U32 buf, U32 size)
{
	U32 status, _size, i;
	volatile U8* pBuf_8;
	volatile U32* pBuf_32;

	if(pTSPIPara->DataMerge == DATAMERGE_ON)
		_size = size/4;
	else
		_size = size;

	i=_size;

	//reset TXFIFO
	RW_SPI_REG(SPI_CTRL, pTSPIPara->tSPIID) = (0x01 << 2);
	// check TXFIFO reset
	status = 0xFFFFFFFF;
	while(status & (1<<2) != 0)
	{
		status = RW_SPI_REG(SPI_CTRL, pTSPIPara->tSPIID);
	}

	RW_SPI_REG(SPI_CTRL, pTSPIPara->tSPIID) = (0x02 << 16);		//transmit FIFO threshold

	RW_SPI_REG(SPI_INTR_EN, pTSPIPara->tSPIID) =(0x01 << 5)		//SlvCmdEn
											   |(0x01 << 4)		//enable the end of SPI transfer INT
											   |(0x01 << 3);	//TXFIFOIntEn

	RW_SPI_REG(SPI_SLV_ST, pTSPIPara->tSPIID) = (0x01 << 16);	//ready

	if(pTSPIPara->DataMerge == DATAMERGE_ON)
		pBuf_32 = (volatile U32*)buf;
	else
		pBuf_8 = (volatile U8*)buf;

	do{
		status = 0;
		// check TXEMPTY
		while(status & (1<<22) != 1)
		{
			status = RW_SPI_REG(SPI_STATUS, pTSPIPara->tSPIID);
		}

		if(pTSPIPara->DataMerge == DATAMERGE_ON)
			RW_SPI_REG(SPI_DATA, pTSPIPara->tSPIID) = pBuf_32[_size-i];
		else
			RW_SPI_REG(SPI_DATA, pTSPIPara->tSPIID) = pBuf_8[_size-i];
	}while(--i);
}
