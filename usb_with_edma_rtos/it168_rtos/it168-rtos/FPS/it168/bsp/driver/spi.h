#ifndef _SPI_H_
#define _SPI_H_

#include "_types.h"

#define SPI_REG_BASE_ADDR				(0x91000000)
#define RW_SPI_REG(offset, SPI_set)		(*((volatile U32 *)(SPI_REG_BASE_ADDR + offset + SPI_set*0x00400000)))

#define SPI_IDREV						0x0000
#define SPI_TRANS_FMT					0x0010
#define SPI_TRANS_CTRL					0x0020
#define SPI_CMD							0x0024
#define SPI_ADDR						0x0028
#define SPI_DATA						0x002c
#define SPI_CTRL						0x0030
#define SPI_STATUS						0x0034
#define SPI_INTR_EN						0x0038
#define SPI_INTR_ST						0x003c
#define SPI_TIMING						0x0040
#define SPI_SLV_ST						0x0060
#define SPI_SLV_DATA_CNT				0x0064
#define SPI_RD_TRANS_CNT_MSB			0x0074
#define SPI_CONFIG						0x007c

typedef enum
{
	SPI0 = 0,
	SPI1 = 1,
	SPI2 = 2
}TSPIID;

typedef enum{
	PHASE_1EDGE = 0,
	PHASE_2EDGE
}TSPI_PHASE;

typedef enum{
	POLARITY_LOW = 0,
	POLARITY_HIGH
}TSPI_POLARITY;

typedef enum{
	MASTER_MODE = 0,
	SLAVE_MODE
}TSPI_SLVMODE;

typedef enum{
	MSB_MODE = 0,
	LSB_MODE
}TSPI_LSB;

typedef enum{
	DATAMERGE_OFF = 0,
	DATAMERGE_ON
}TSPI_DATAMERGE;

typedef enum{
	ADDRLEN_1BYTE = 0,
	ADDRLEN_2BYTE,
	ADDRLEN_3BYTE,
	ADDRLEN_4BYTE
}TSPI_ADDRLEN;

typedef struct
{
	TSPIID tSPIID;  // SPI ID
	U32 Clk;     // SPI clock

	// Data Format register
	U8  Phs;     // Clock Phase
	U8  Pol;     // Clock polarity
	U8  SlvMode;	// Master/Slave mode
	U8  Lsb;     // 0:MSB ;1:LSB
	U8  DataMerge; // Data merge mode
	U8  Bit;     // Data word length
	U8  AddrLen;	// Address length
}TSPIPara;

void spi_Open(TSPIPara* pTSPIPara);
void spi_RegisterISR(TSPIPara* pTSPIPara, void (*callback)());
void spi_testISR0(void);
void spi_testISR1(void);
void spi_testISR2(void);
void spi_Master_ReadByte(TSPIPara* pTSPIPara, U8 reg, U32 buf, U32 size);
void spi_Master_WriteByte(TSPIPara* pTSPIPara, U8 reg, U32 buf, U32 size);
void spi_Slave_ReadByte(TSPIPara* pTSPIPara, U8* reg, U32 buf, U32 size);
void spi_Slave_WriteByte(TSPIPara* pTSPIPara, U32 buf, U32 size);
#endif
