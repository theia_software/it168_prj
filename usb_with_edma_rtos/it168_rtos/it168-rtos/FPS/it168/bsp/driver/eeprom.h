#ifndef _EEPROM_H_
#define _EEPROM_H_

#include "iic.h"

S32 eeprom_WriteData(TI2CId ch, U32 addr, U32 buf, U32 count);
S32 eeprom_ReadData(TI2CId ch, U32 addr, U32 buf, U32 count);

#endif
