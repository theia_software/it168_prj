#ifndef _DMA_H_
#define _DMA_H_

#include "_types.h"

#define DMA_CH0	0x0
#define DMA_CH1	0x1
#define DMA_CH2 0x2
#define DMA_CH3 0x3
#define DMA_CH4 0x4
#define DMA_CH5 0x5
#define DMA_CH6 0x6
#define DMA_CH7 0x7

#define DMA_REG_BASE_ADDR				(0x70000000)
#define RW_DMA_REG(offset)				(*((volatile U32 *)(DMA_REG_BASE_ADDR + offset)))

#define DMA_IDREV						0x0000
#define DMA_CONFIG						0x0010
#define DMA_CTRL						0x0020
#define DMA_INT_STATUS					0x0030
#define DMA_CH_ENABLE					0x0034
#define DMA_CH_ABORT					0x0040
#define DMA_CH_N_CTRL(channel)			0x0044 + channel*0x14
#define DMA_CH_N_SRC_ADDR(channel)		0x0048 + channel*0x14
#define DMA_CH_N_DST_ADDR(channel)		0x004c + channel*0x14
#define DMA_CH_N_TRANS_SIZE(channel)	0x0050 + channel*0x14

/*channel n control priority*/
enum dma_priority
{
	DMA_CH_CTRL_PRIORITY_LOW = 0,
	DMA_CH_CTRL_PRIORITY_HIGH
};

/*channel n control burst size*/
enum dma_burst_size
{
	DMA_CH_CTRL_BURST_SIZE_1_TRANS = 0,
	DMA_CH_CTRL_BURST_SIZE_2_TRANS,
	DMA_CH_CTRL_BURST_SIZE_4_TRANS,
	DMA_CH_CTRL_BURST_SIZE_8_TRANS,
	DMA_CH_CTRL_BURST_SIZE_16_TRANS,
	DMA_CH_CTRL_BURST_SIZE_32_TRANS,
	DMA_CH_CTRL_BURST_SIZE_64_TRANS,
	DMA_CH_CTRL_BURST_SIZE_128_TRANS
};

/*channel n control source width*/
enum dma_width
{
	DMA_CH_CTRL_WIDTH_BYTE_TRANS = 0,
	DMA_CH_CTRL_WIDTH_HALF_WORD_TRANS,
	DMA_CH_CTRL_WIDTH_WORD_TRANS
};

/*channel n control source mode (normal/handshaking mode)*/
enum dma_mode
{
	DMA_CH_CTRL_NORMAL_MODE = 0,
	DMA_CH_CTRL_HANDSHAKING_MODE
};

/*channel n control source address control */
enum dma_addr_ctl
{
	DMA_CH_CTRL_INCREMENT_ADDR = 0,
	DMA_CH_CTRL_DECREMENT_ADDR,
	DMA_CH_CTRL_FIXED_ADDR
};

/*channel n control source request selection */
enum dma_req_sel
{
	DMA_CH_CTRL_REQ_SEL_NONE = 0,
	DMA_CH_CTRL_REQ_SEL_SPI0_TX =0,
	DMA_CH_CTRL_REQ_SEL_SPI0_RX,
	DMA_CH_CTRL_REQ_SEL_SPI1_TX,
	DMA_CH_CTRL_REQ_SEL_SPI1_RX,
	DMA_CH_CTRL_REQ_SEL_SPI2_TX,
	DMA_CH_CTRL_REQ_SEL_SPI2_RX,
	DMA_CH_CTRL_REQ_SEL_UART0_TX,
	DMA_CH_CTRL_REQ_SEL_UART0_RX,
	DMA_CH_CTRL_REQ_SEL_UART1_TX,
	DMA_CH_CTRL_REQ_SEL_UART1_RX,
	DMA_CH_CTRL_REQ_SEL_I2C0,
	DMA_CH_CTRL_REQ_SEL_I2C1
};

void edma_Init(U32 ch, U32 src_addr, U32 dst_addr);
void edma_SetSrcParams(U32 ch, U32 trans_size, U32 burst_size, U32 width, U32 mode, U32 addctl, U32 reqsel);
void edma_SetDstParams(U32 ch, U32 width, U32 mode, U32 addctl, U32 reqsel);
void edma_Start(U32 ch, U32 priority);
void edma_Stop(U32 ch);

#endif
