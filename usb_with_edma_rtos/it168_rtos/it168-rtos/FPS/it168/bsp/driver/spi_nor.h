#ifndef _SPI_NOR_H_
#define _SPI_NOR_H_

#include "_types.h"
#include "spi.h"



typedef struct
{
	U8  MID;              // manufacturer ID
	U8  TypeID;           // device ID
	U32 bytesPerSect;     // Sector size by Nor
	U32 pagesPerSect;	  // Num of page per sector
	U32 bytesPerPage;     // Num of bytes per page
	U32 pagesPerBlock;    // Num of pages per block
	U32 noOfBlocks;       // Num of blocks in Nor

	TSPIPara pSPIPara;	  // spi parameter
}TNorDev;


void nor_SpiOpen(TSPIID spi_id);
TNorDev* nor_ReadID(void);
void nor_EraseSector(U32 sec);
void nor_EraseBlock(U32 phyBlock);
void nor_ReadData(U32 addr, U32 buf, U32 count);
void nor_ReadPage(U32 page, U32 buf);
void nor_WritePage(U32 page, U32 buf);
void nor_WriteData(U32 addr, U32 buf, U32 count);
S32 nor_Test(U32 src_addr, U32 dst_addr);
#endif
