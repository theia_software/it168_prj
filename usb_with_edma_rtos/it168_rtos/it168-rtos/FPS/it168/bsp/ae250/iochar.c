/*
 * Copyright (c) 2012-2017 Andes Technology Corporation
 * All rights reserved.
 *
 */

#include "platform.h"
#include "uart.h"

int outbyte(int c)
{
	int ret;

	ret = uart_Tx(UART1, &c, 1);

	return c;
}

