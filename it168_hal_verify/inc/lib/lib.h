/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: lib.h
 */

#ifndef _LIB_H_
#define _LIB_H_

#include "_types.h"

#define NOP() \
    ({ __asm__ __volatile__ ("nop"); })

void set_dbg_val(uint32_t addr, uint32_t val);
void delay(const U32 cycle);

#endif

