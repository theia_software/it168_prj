#ifndef _ISR_H_
#define _ISR_H_


void ISR_setup(void);
void ISR_enable(void);
void uart0_irq_handler(void);
void uart1_irq_handler(void);
void dma_irq_handler(void);

#endif
