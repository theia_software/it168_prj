/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: sys.h
 */

#ifndef _SYS_H_
#define _SYS_H_

#include "_types.h"

/* Definition */
#define SYS_REG_BASE_ADDR					(0x10000000)
#define RW_SYS_REG(offset)					(*((volatile U32 *)(SYS_REG_BASE_ADDR + offset)))
#define SYS_REG_GEN_DATE					(0x00)
#define SYS_REG_GEN_VERSION					(0x04)
#define SYS_REG_GEN_GEN_TIME				(0x08)


#define SYSTEM_GEN_DATE						(0x00)
#define SYSTEM_GEN_VERSION					(0x04)
#define SYSTEM_GEN_TIME						(0x08)
#define SYSTEM_GEN_TIME_OFFSET				(0)
#define SYSTEM_GEN_TIME_MASK				(0xFFFFFF << SYS_GEN_TIME_OFFSET)
#define SYSTEM_FREQ_METER_INPUT_SEL			(0x1c)
#define SYSTEM_FREQ_METER_CYCLE_AMOUNT		(0x20)
#define SYSTEM_FREQ_METER_COUNTER_DONE		(0x24)
#define SYSTEM_FREQ_METER_READ_EN			(0x28)
#define SYSTEM_FREQ_METER_COUNTER_HIGH		(0x2c)
#define SYSTEM_FREQ_METER_COUNTER_LOW		(0x30)
#define SYSTEM_FREQ_METER_COUNTER_REF_CLK	(0x34)
#define SYSTEM_REG_RESET					(0x38)
#define SET_RESET_TCON_EN					SET_BIT0
#define CLR_RESET_TCON_DIS					CLR_BIT0
#define SET_RESET_SENSOR_EN					SET_BIT8
#define CLR_RESET_SENSOR_DIS				CLR_BIT8
#define SET_RESET_RISCV_EN					SET_BIT16
#define CLR_RESET_RISCV_DIS					CLR_BIT16
#define SET_RESET_DMA_EN					SET_BIT24
#define CLR_RESET_DMA_DIS					CLR_BIT24

#define SYSTEM_DEBUG_FLAG_MODULE_SEL		(0x3c)
#define SYSTEM_DEBUG_FLAG_MODULE_SEL_OFFSET	(0)
#define SYSTEM_DEBUG_FLAG_MODULE_SEL_MASK	(0x07 << SYSTEM_DEBUG_FLAG_MODULE_SEL_OFFSET)
#define SYSTEM_DEBUG_FLAG_MODULE_SEL_OUTPUT	(0x40)
#define SYSTEM_DEBUG_FLAG_MODULE_SEL_OUTPUT_OFFSET	(0)
#define SYSTEM_DEBUG_FLAG_MODULE_SEL_OUTPUT_MASK	(0xFF << SYSTEM_DEBUG_FLAG_MODULE_SEL_OUTPUT_OFFSET)
#define	SYSTEM_RESET						(0x58)
#define SYSTEM_RESET_SPI_SLV_EN				SET_BIT0
#define SYSTEM_RESET_SPI_SLV_01_EN			SET_BIT8
#define SYSTEM_RESET_BUS_EN					SET_BIT24

#define SYSTEM_RISCV_VERSION				(0x68)

#define SYS_RESET_DSP                       (0xc4)
    #define SET_RESET_DSP_EN                (SET_BIT0)
    #define CLR_RESET_DSP_DIS               (CLR_BIT0)

#define SYS_CLK_EN                          (0xd4)
    #define SYS_SRAM_CLK_EN                 (SET_BIT0)
    #define SYS_SRAM_CLK_DIS                (CLR_BIT0)
    #define SYS_DSP_CLK_EN                  (SET_BIT8)
    #define SYS_DSP_CLK_DIS                 (CLR_BIT8)
    #define SYS_DSP_SRAM_CLK_EN             (SET_BIT9)
    #define SYS_DSP_SRAM_CLK_DIS            (CLR_BIT9)
    #define SYS_RISCV_CLK_EN                (SET_BIT16)
    #define SYS_RISCV_CLK_DIS               (CLR_BIT16)
    #define SYS_OTHERS_CLK_EN               (SET_BIT24)
    #define SYS_OTHERS_CLK_DIS              (CLR_BIT24)

#define SYSTEM_FIFO_MASTER_00				(0x184)
#define SYSTEM_FIFO_MASTER_01				(0x184)
#define SYSTEM_FIFO_MASTER_02				(0x184)
#define SYSTEM_FIFO_MASTER_03				(0x184)
#define SYSTEM_FIFO_MASTER_04				(0x188)
#define SYSTEM_FIFO_MASTER_05				(0x188)
#define SYSTEM_FIFO_MASTER_08				(0x188)
#define SYSTEM_FIFO_MASTER_CLEAR_00			(0x18c)
#define SYSTEM_FIFO_MASTER_CLEAR_01			(0x18c)
#define SYSTEM_FIFO_MASTER_CLEAR_02			(0x18c)
#define SYSTEM_FIFO_MASTER_CLEAR_03			(0x18c)
#define SYSTEM_FIFO_MASTER_CLEAR_04			(0x190)
#define SYSTEM_FIFO_MASTER_CLEAR_05			(0x190)
#define SYSTEM_FIFO_MASTER_CLEAR_08			(0x190)

void sys_init(void);

#endif
