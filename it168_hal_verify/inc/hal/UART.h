#ifndef _UART_H_
#define _UART_H_

#include <_types.h>


#define UART_REG_BASE_ADDR					(0x92000000)
#define RW_UART_REG(offset, uart_id)		(*((volatile U32 *)(UART_REG_BASE_ADDR + offset + uart_id*0x00400000)))

#define UART_IDREV						0x0000
#define UART_HARDWARE_FIFO_DEPTH		0x0010
#define UART_OVER_SAMPLE_CONTROL		0x0014
#define UART_RBR_THR_DLL				0x0020
#define UART_INTERRUPT_ENABLE_DLM		0x0024
#define UART_INTERRUPT_FIFO_CONTROL		0x0028
#define UART_LINE_CONTROL				0x002c
#define UART_LINE_STATUS				0x0034


typedef enum
{
    UART0       = 0,
    UART1       = 1
}TUartID;

typedef enum
{
	PARITY_NONE	= 0,	// 000b
	PARITY_ODD	= 1,	// 001b
	PARITY_EVEN	= 5		// 101b
}TParity;

typedef enum
{
    STOP_BITS_1 = 0,	//stop bit 1
    STOP_BITS_2 = 1		//stop bit 1.5/2 based on WLS
}TStopBits;

U32 uart_Read_Id(TUartID id);
void uart_Dma_Open(TUartID id, U32 baud_rate, TParity parity, TStopBits stopbits);
void uart_Dma_WaitFinish(TUartID id);
int uart_Dma_CheckFinish(TUartID id);
void uart_Dma_Tx(TUartID id, U32 buf, U32 size);
void uart_Dma_Rx(TUartID id, U32 buf, U32 size);
void uart_Open(TUartID id, U32 baud_rate, TParity parity, TStopBits stopbits);
int uart_Tx(TUartID id, U32 buf, U32 size);
int uart_Rx(TUartID id, U32 buf, U32 size);

#endif
