/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: pe.h
 */

#ifndef _PE_H_
#define _PE_H_

#include <_types.h>


#define PE_REG_BASE_ADDR                    (0x60000000)
#define RW_PE_REG(offset)                   (*((volatile U32 *)(PE_REG_BASE_ADDR + (offset))))

#define PE_SRAM_SWITCH                      (0x000)
#define PE_SRAM_SWITCH_PE                   (0)
#define PE_SRAM_SWITCH_BUS                  (1)

#define PE_DONE                             (0x004)

#define PE_STATUS                           (0x008)

#define PE_IDU_TRIG                         (0x00c)


#endif

