#ifndef _I2C_H_
#define _I2C_H_

#include <_types.h>


#define I2C_REG_BASE_ADDR				(0x94000000)
#define RW_I2C_REG(offset, I2C_set)		(*((volatile U32 *)(I2C_REG_BASE_ADDR + offset + I2C_set*0x00400000)))

#define I2C_IDREV						0x0000
#define I2C_CONFIG						0x0010
#define I2C_INTERRUPT_ENABLE			0x0014
#define I2C_STATUS						0x0018
#define I2C_ADDRESS						0x001c
#define I2C_DATA						0x0020
#define I2C_CONTROL						0x0024
#define I2C_COMMAND						0x0028
#define I2C_SETUP						0x002c
#define I2C_TIMING_PARAMETER_MULTIPLIER	0x0030

typedef enum
{
	I2C0 = 0,
	I2C1 = 1
}TI2CId;

typedef enum {
    I2C_DIR_RX  = 0,
    I2C_DIR_TX  = 1
}TI2CDir;

U32 iic_Read_Id(TI2CId ch);
U32 iic_Read_Config(TI2CId ch);
void iic_Slave_Dma_TxData(TI2CId ch, U32 id, U32 buf, U32 size);
void iic_Slave_Dma_RxData(TI2CId ch, U32 id, U32 buf, U32 size);
void iic_Master_Dma_TxData(TI2CId ch, U32 address, U32 buf, U32 size);
void iic_Master_Dma_RxData(TI2CId ch, U32 address, U32 buf, U32 size);
#endif
