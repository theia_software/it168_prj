/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: iap.h
 */

#ifndef _IAP_H_
#define _IAP_H_

#include <ParameterSetting.h>
#include <sys.h>


/* Definition */
#define DSP_REG_BASE_ADDR                   (0x50000000UL)
#define RW_DSP_REG(offset)                  (*((volatile U32 *)(DSP_REG_BASE_ADDR + (offset))))

/* DSP verification */
#define CONFIG_DSP_VER_EN                   (CONFIG_IAP_TEST_EN)

/*
 * debug flag            +------------------+ CONFIG_FW_DEFINE_RAM_START
 *                       |      0x100       |
 * DSP_PAT_RAM_START     +------------------+
 *                       | DSP_PAT_RAM_LEN  |
 * DSP_ADDR_RANGE_MAX    +------------------+ CONFIG_SRAM_LEN
 *
 */
#define DSP_INST_LEN                        (0x100)

#define DSP_PAT_RAM_START                   ((DEBUG_FLAG_END + (0x100 - 1)) & ~(0x100 - 1))
#define DSP_PAT_RAM_LEN                     (CONFIG_SRAM_LEN - DSP_PAT_RAM_START)
#define DSP_ADDR_RANGE_MAX                  (CONFIG_SRAM_LEN)

/* status flag */
#define DSP_VER_FW_CHK_FAIL                 (SET_BIT31) // DEBUG_DSP_FW_CHK
#define DSP_VER_FW_CHK_LOAD_COEF_FAIL       (SET_BIT30) // DEBUG_DSP_FW_CHK
#define DSP_VER_OUT_8BIT_OFF                (31) // DEBUG_DSP_INSTRU

#define DSP_OP_CODE_MAP_OFF                 (24UL)
#define DSP_OP_DATA_MASK                    (BIT32_MASK(0, 24))
#define DSP_OP_DATA_CHK                     (~(DSP_OP_DATA_MASK))

#define DSP_OP_ADDR_MASK                    (BIT32_MASK(0, 24))
#define DSP_OP_ADDR_CHK                     (~(DSP_OP_ADDR_MASK))

#define DSP_OP_CONST_MASK                   (BIT32_MASK(0, 16))
#define DSP_OP_CONST_CHK                    (~(DSP_OP_CONST_MASK))

#define DSP_OP_DATA_SHIFT_MASK              (BIT32_MASK(0, 16))
#define DSP_OP_DATA_SHIFT_CHK               (~(DSP_OP_DATA_SHIFT_MASK))

#define DSP_OP_DATA_SHIFT_UNIT_MASK         (BIT32_MASK(0, 4))
#define DSP_OP_DATA_SHIFT_UNIT_CHK          (~(DSP_OP_DATA_SHIFT_UNIT_MASK))
#define DSP_OP_D1_SHIFT_OFF                 (12)
#define DSP_OP_D2_SHIFT_OFF                 (8)
#define DSP_OP_OUT1_SHIFT_OFF               (4)
#define DSP_OP_OUT2_SHIFT_OFF               (0)

#define DSP_OP_8BIT_OUTPUT_DATA             (1UL)

#define DSP_OP_CONV_FILTER_WD               (5UL)
#define DSP_OP_CONV_FILTER_HT               (5UL)
#define DSP_OP_CONV_COEF_IDX_MAX            (DSP_OP_CONV_FILTER_WD * DSP_OP_CONV_FILTER_HT)
#define DSP_OP_CONV_COEF_LEN_MAX            (DSP_OP_CONV_COEF_IDX_MAX << 2)
#define DSP_OP_CONV_COEF_MASK               (BIT32_MASK(0, 16))

#define READ_DSP_LUT_IDX(x)                 (((x) >> (8)) & (BIT32_MASK(0, 8)))
#define READ_DSP_LUT_OFF(x)                 ((x) & (BIT32_MASK(0, 8)))

#define DSP_OP_LOAD_LUT_CAL_TIME            (128UL)
#define DSP_LOAD_LUT_IDX_MAX                (DSP_OP_LOAD_LUT_CAL_TIME << 1)
#define DSP_LOAD_LUT_LEN                    (DSP_OP_LOAD_LUT_CAL_TIME << 2)
#define DSP_LUT_ROUND_MASK                  (BIT32_MASK(0, 8))

#define DSP_OP_MATRIX_IN_OUT_MASK           (BIT32_MASK(0, 8))
#define DSP_OP_MATRIX_IN_OUT_CHK            (~(DSP_OP_MATRIX_IN_OUT_MASK))
#define DSP_OP_MATRIX_IN_OUT_UNIT_MASK      (BIT32_MASK(0, 4))
#define DSP_OP_MATRIX_IN_OUT_UNIT_CHK       (~(DSP_OP_MATRIX_IN_OUT_UNIT_MASK))
#define DSP_OP_MATRIX_I_OFF                 (4)
#define DSP_OP_MATRIX_O_OFF                 (0)
#define DSP_OP_MATRIX_IO_MAX                (5)

/* 1D Convolution filter size: 5, 7, 9, 11 */
#define DSP_OP_SET_1D_CONV_FILTER_MIN       (5)
#define DSP_OP_SET_1D_CONV_FILTER_MAX       (11)
#define DSP_OP_SET_1D_CONV_FILTER_IDX_NUM   (4)
#define DSP_OP_SET_1D_CONV_FILTER_OFF       (4)
#define DSP_OP_SET_1D_CONV_X                (0)
#define DSP_OP_SET_1D_CONV_Y                (1)
#define DSP_OP_SET_1D_CONV_MASK             (BIT32_MASK(0, 8))
#define DSP_OP_SET_1D_CONV_CHK              (~(DSP_OP_MATRIX_IN_OUT_MASK))


#if (CONFIG_DSP_VER_EN == 1)
/* ADD, SUB, MUL */
#define DSP_TEST_ALU_HW_PAT_START_ADDR      (DSP_PAT_RAM_START)
#define DSP_TEST_ALU_HW_PAT_LEN             ((DSP_PAT_RAM_LEN) >> 1)

#define DSP_TEST_ALU_FW_PAT_OFF             (DSP_TEST_ALU_HW_PAT_LEN)

#define DSP_TEST_ALU_FW_PAT_START_ADDR      (DSP_TEST_ALU_HW_PAT_START_ADDR + \
                                             DSP_TEST_ALU_HW_PAT_LEN)
#define DSP_TEST_ALU_FW_PAT_LEN             (DSP_TEST_ALU_HW_PAT_LEN)

/* split HW & FW PAT RAM into three parts(addr1, add2, out_base) */
#define DSP_TEST_ALU_REGION_LEN_MAX         ((DSP_TEST_ALU_HW_PAT_LEN) / 3)
#define DSP_TEST_ALU_CAL_TIME_MAX           ((DSP_TEST_ALU_REGION_LEN_MAX) >> 2)
/* ==================================================================================== */

/* Convolution */
#define DSP_TEST_CONV_IMG_START_ADDR        (DSP_PAT_RAM_START)

/* split SRAM into three parts(CONV image, HW & FW out_base) */
#define DSP_TEST_CONV_IMG_LEN_MAX           (SET_DEC_ALIGN_4((DSP_ADDR_RANGE_MAX - \
                                                              DSP_TEST_CONV_IMG_START_ADDR) / 3))
#define DSP_TEST_CONV_FW_PAT_OFF            (DSP_TEST_CONV_IMG_LEN_MAX)
/* ==================================================================================== */

/* LUT */
#define DSP_TEST_LUT_TABLE_START_ADDR       (DSP_PAT_RAM_START)
#define DSP_TEST_LUT_IDX_START_ADDR         (DSP_TEST_LUT_TABLE_START_ADDR + DSP_LOAD_LUT_LEN)

/* split SRAM into three parts(LUT index, HW & FW out_base) */
#define DSP_TEST_LUT_IDX_LEN_MAX            (SET_DEC_ALIGN_4((DSP_ADDR_RANGE_MAX - \
                                                              DSP_TEST_LUT_IDX_START_ADDR) / 3))
#define DSP_TEST_LUT_CAL_TIME_MAX           ((DSP_TEST_LUT_IDX_LEN_MAX) >> 2)
#define DSP_TEST_LUT_FW_PAT_OFF             (DSP_TEST_LUT_IDX_LEN_MAX)
/* ==================================================================================== */

/* Matrix */
#define DSP_TEST_MATRIX_IN_START_ADDR       (DSP_PAT_RAM_START)

/* split SRAM into three parts(matrix in, HW & FW out_base) */
#define DSP_TEST_MATRIX_IN_LEN_MAX          (SET_DEC_ALIGN_4((DSP_ADDR_RANGE_MAX - \
                                                              DSP_TEST_MATRIX_IN_START_ADDR) / 3))
#define DSP_TEST_MATRIX_CAL_TIME_MAX        ((DSP_TEST_MATRIX_IN_LEN_MAX) >> 2)
#define DSP_TEST_MATRIX_FW_PAT_OFF          (DSP_TEST_MATRIX_IN_LEN_MAX)
/* ==================================================================================== */

/* Hamming distance */
#define DSP_TEST_HD_PAT_START_ADDR          (DSP_PAT_RAM_START)
#define DSP_TEST_HD_PAT_LEN                 (DSP_TEST_HD_PAT_LEN_MAX << 1)
#define DSP_TEST_HD_HW_OUT_BASE             (DSP_TEST_HD_PAT_START_ADDR + DSP_TEST_HD_PAT_LEN)
#define DSP_TEST_HD_FW_OUT_BASE             (DSP_TEST_HD_HW_OUT_BASE + DSP_TEST_HD_PAT_LEN)

/* split SRAM into four parts(HD pattern 1 & 2, HW & FW out_base) */
#define DSP_TEST_HD_PAT_LEN_MAX             (SET_DEC_ALIGN_4((DSP_ADDR_RANGE_MAX - \
                                                              DSP_TEST_HD_PAT_START_ADDR) / 4))
#define DSP_TEST_HD_CAL_TIME_MAX            ((DSP_TEST_HD_PAT_LEN_MAX) >> 2)
#define DSP_TEST_HD_FW_PAT_OFF              (DSP_TEST_HD_PAT_LEN_MAX)
/* ==================================================================================== */
#endif

#ifdef STATUS_FLAG
#undef STATUS_FLAG
#endif

/* debug RAM layout */
#define STATUS_FLAG                         (CONFIG_FW_DEFINE_RAM_START)
#define DEBUG_WDT_CNT_RAM                   (STATUS_FLAG + 0x04)
#define DEBUG_TCON_DONE_00_RAM              (STATUS_FLAG + 0x08)
#define DEBUG_TCON_DONE_01_RAM              (STATUS_FLAG + 0x0C)
#define DEBUG_TCON_DONE_02_RAM              (STATUS_FLAG + 0x10)
#define DEBUG_TCON_OUTPUT_00_RAM            (STATUS_FLAG + 0x14)
#define DEBUG_TCON_OUTPUT_01_RAM            (STATUS_FLAG + 0x18)
#define DEBUG_TCON_OUTPUT_02_RAM            (STATUS_FLAG + 0x1C)

#define DEBUG_ADC_DONE_00_RAM               (STATUS_FLAG + 0x20)
#define DEBUG_ADC_DONE_01_RAM               (STATUS_FLAG + 0x24)
#define DEBUG_ADC_DONE_02_RAM               (STATUS_FLAG + 0x28)
#define DEBUG_ADC_DONE_03_RAM               (STATUS_FLAG + 0x2C)
#define DEBUG_ADC_DONE_04_RAM               (STATUS_FLAG + 0x30)
#define DEBUG_ADC_DONE_05_RAM               (STATUS_FLAG + 0x34)
#define DEBUG_ADC_DONE_ALL_RAM              (STATUS_FLAG + 0x38)

#define DEBUG_DSP_DONE_RAM                  (STATUS_FLAG + 0x3C)
#define DEBUG_SYS_INT_IN_RAM                (STATUS_FLAG + 0x40)

#define DEBUG_SPI00_RAM                     (STATUS_FLAG + 0x44)
#define DEBUG_SPI01_RAM                     (STATUS_FLAG + 0x48)

#define DEBUG_DSP_DATA1                     (STATUS_FLAG + 0x4C)
#define DEBUG_DSP_DATA2                     (STATUS_FLAG + 0x50)
#define DEBUG_DSP_OUTPUT                    (STATUS_FLAG + 0x54)
#define DEBUG_DSP_INSTRU                    (STATUS_FLAG + 0x58)

#define DEBUG_DSP_CAL_LEN                   (STATUS_FLAG + 0x5C)
#define DEBUG_DSP_ERR_CODE                  (STATUS_FLAG + 0x60)
#define DEBUG_DSP_FW_CHK                    (STATUS_FLAG + 0x64)
#define DEBUG_DSP_FW_ADDR_OFF               (STATUS_FLAG + 0x68)
#define DEBUG_DSP_SHIFT                     (STATUS_FLAG + 0x6C)
#define DEBUG_DSP_MATRIX_IO                 (STATUS_FLAG + 0x70)
#define DEBUG_DSP_TEST_LOOP                 (STATUS_FLAG + 0x74)

#define DEBUG_IRQ25_RAM                     (STATUS_FLAG + 0x78)
#define DEBUG_IRQ27_RAM                     (STATUS_FLAG + 0x7c)
#define DEBUG_TRIGGER_TIMES                 (STATUS_FLAG + 0x80)
#define DEBUG_TRIGGER_CMP_ANS               (STATUS_FLAG + 0x84)

#define DEBUG_FLAG_END                      (DEBUG_TRIGGER_CMP_ANS + 0x04)
/* ==================================================================================== */

#define set_dbg_val(addr, val)              ((*((volatile U32 *)(addr))) = (U32)(val))
#define read_dbg_val(addr)                  (*((volatile U32 *)(addr)))

#define CHK_ALIGN_2                         (0x01ul)
#define CHK_ALIGN_4                         (0x03ul)
#define CHK_ALIGN_1K                        (0x400ul - 1)

#define TEST_ALIGN_2(a)                     (((a) & CHK_ALIGN_2) != 0)
#define TEST_ALIGN_4(a)                     (((a) & CHK_ALIGN_4) != 0)

#define SET_INC_ALIGN_2(x)                  (((x) + (CHK_ALIGN_2)) & ~(CHK_ALIGN_2))
#define SET_DEC_ALIGN_2(x)                  ((x) & ~(CHK_ALIGN_2))
#define SET_INC_ALIGN_4(x)                  (((x) + (CHK_ALIGN_4)) & ~(CHK_ALIGN_4))
#define SET_DEC_ALIGN_4(x)                  ((x) & ~(CHK_ALIGN_4))
#define SET_INC_ALIGN_1K(x)                 (((x) + (CHK_ALIGN_1K)) & ~(CHK_ALIGN_1K))
#define SET_DEC_ALIGN_1K(x)                 ((x) & ~(CHK_ALIGN_1K))

/* bit operation */
#define BIT32_MASK(off, len)                ((U32)((~((~0ul) << (len))) << (off)))

#define NOP() \
    ({ __asm__ __volatile__ ("nop"); })

#define DSP_RESET() \
    ({ RW_SYS_REG(SYS_RESET_DSP) |= (SET_RESET_DSP_EN); \
       RW_SYS_REG(SYS_RESET_DSP) &= (CLR_RESET_DSP_DIS); })


/* Register */
#define DSP_DATE                            (0x000)
#define DSP_VERSION                         (0x004)
#define DSP_START_TRIG                      (0x008)
#define DSP_INST_START_ADDR                 (0x00c)
#define DSP_INST_END_ADDR                   (0x010)
#define DSP_COEF_40                         (0x014)
#define DSP_COEF_41                         (0x018)
#define DSP_COEF_42                         (0x01c)
#define DSP_COEF_43                         (0x020)
#define DSP_COEF_44                         (0x024)
#define DSP_COEF_30                         (0x028)
#define DSP_COEF_31                         (0x02c)
#define DSP_COEF_32                         (0x030)
#define DSP_COEF_33                         (0x034)
#define DSP_COEF_34                         (0x038)
#define DSP_COEF_20                         (0x03c)
#define DSP_COEF_21                         (0x040)
#define DSP_COEF_22                         (0x044)
#define DSP_COEF_23                         (0x048)
#define DSP_COEF_24                         (0x04c)
#define DSP_COEF_10                         (0x050)
#define DSP_COEF_11                         (0x054)
#define DSP_COEF_12                         (0x058)
#define DSP_COEF_13                         (0x05c)
#define DSP_COEF_14                         (0x060)
#define DSP_COEF_00                         (0x064)
#define DSP_COEF_01                         (0x068)
#define DSP_COEF_02                         (0x06c)
#define DSP_COEF_03                         (0x070)
#define DSP_COEF_04                         (0x074)
#define DSP_HALF_IMAGE_WIDTH                (0x158)
    #define DSP_HALF_IMAGE_WIDTH_MAX        (0x80)

#define DSP_DONE                            (0x15c)
#define DSP_DONE_CLEAR                      (0x160)
#define DSP_DUMMY_01                        (0x164)
#define DSP_DUMMY_02                        (0x168)
#define DSP_T_RWM                           (0x16c)
#define DSP_HD_CNT                          (0x190)
    #define DSP_HD_CNT_MASK                 (BIT32_MASK(0, 6))
    #define DSP_HD_CNT_MAX                  (DSP_HD_CNT_MASK)
#define DSP_AES_INIT                        (0x194)
#define DSP_1D_COEF_00                      (0x198)
#define DSP_1D_COEF_01                      (0x19c)
#define DSP_1D_COEF_02                      (0x1a0)
#define DSP_1D_COEF_03                      (0x1a4)
#define DSP_1D_COEF_04                      (0x1a8)
#define DSP_1D_COEF_05                      (0x1ac)
#define DSP_1D_COEF_06                      (0x1b0)
#define DSP_1D_COEF_07                      (0x1b4)
#define DSP_1D_COEF_08                      (0x1b8)
#define DSP_1D_COEF_09                      (0x1bc)
#define DSP_1D_COEF_10                      (0x1c0)

#define DSP_1D_CONV_IMG_WD                  (0x1c4)
    #define DSP_1D_CONV_IMG_WD_MAX          (0x1fe)

#define DSP_1D_CONV_IMG_HT                  (0x1c8)
    #define DSP_1D_CONV_IMG_HT_MAX          (0x1fe)

#define DSP_CRC_AES_IN                      (0x1cc)
#define DSP_CRC_AES_OUT                     (0x1d0)
#define DSP_SHA_REORDER                     (0x1d4)
#define DSP_AES_REORDER                     (0x1d8)
#define DSP_AES_KEY_SEL                     (0x1dc)

#define DSP_REG_LEN                         (DSP_AES_KEY_SEL + 4)


/* DSP OP code */
typedef enum {
    DSP_OP_SET_ADDR1_BASE = 0x01,
    DSP_OP_SET_ADDR2_BASE,
    DSP_OP_SET_OUTPUT_BASE,
    DSP_OP_SET_CONST1,
    DSP_OP_SET_CONST2,
    DSP_OP_SET_DATA_SHIFT,
    DSP_OP_SET_8BIT_OUTPUT,
    DSP_OP_SET_MATRIX_IN_OUT,
    DSP_OP_ADD_16_16 = 0x10,
    DSP_OP_ADD_16_8,
    DSP_OP_ADD_16_C1,
    DSP_OP_ADD_16_C2,
    DSP_OP_SUB_16_16 = 0x30,
    DSP_OP_SUB_16_8,
    DSP_OP_SUB_16_C1,
    DSP_OP_SUB_16_C2,
    DSP_OP_MUL_16_16 = 0x50,
    DSP_OP_MUL_16_8,
    DSP_OP_MUL_16_C1,
    DSP_OP_MUL_16_C2,
    DSP_OP_MAC_16_C1_C2 = 0x72,
    DSP_OP_LOAD_COEF = 0xb2,
    DSP_OP_CONVOLUTION,
    DSP_OP_LOAD_LUT = 0x92,
    DSP_OP_LUT,
    DSP_OP_MATRIX = 0xd2,
    DSP_OP_HD = 0x90,
    DSP_OP_DMUL_8_8 = 0xf0,

    DSP_OP_SET_1D_CONV = 0x0b,
    DSP_OP_1D_CONV = 0xb6,

} DSP_OP_CODE;

/* DSP error code */
enum {
    DSP_ERR_OP_CODE             = (1UL << 0), // op code error
    DSP_ERR_PARAM               = (1UL << 1), // parameter error
    DSP_ERR_CAL_TIME            = (1UL << 2), // 8bit operation should align 4B
    DSP_ERR_SHIFT_BIT           = (1UL << 3), // shift value out of 4 bits range
    DSP_ERR_INST_OUT_OF_RAM     = (1UL << 4), // running out of instruction RAM
    DSP_ERR_RANGE_OUT_OF_SRAM   = (1UL << 5), // address range(addr + cal_time) can't exceed SRAM
    DSP_ERR_DONE_TIMEOUT        = (1UL << 6), // check DSP done fail, timeout
    DSP_ERR_COEF_NUM            = (1UL << 7), // coefficient num error
    DSP_ERR_MATRIX_IN_OUT       = (1UL << 8), // matrix in out num error
    DSP_ERR_CONV_IMAGE_W_H      = (1UL << 9), // convolution image width or height num error
    DSP_ERR_HD_CNT              = (1UL << 10), // hamming distance count error
    DSP_ERR_1D_CONV_FILTER_NUM  = (1UL << 11), // 1D convolution filter num error
};

#if (CONFIG_DSP_VER_EN == 1)
/* DSP verification status flag */
enum dsp_ver_status_flag {
    DSP_VER_STA_SYNC            = (1UL << 31),
    DSP_VER_STA_START           = (1UL << 30),
    DSP_VER_STA_START_ACK       = (1UL << 29),

    DSP_VER_RES_PASS            = 0,
    DSP_VER_RES_FAIL            = (1UL << 0),
    DSP_VER_SET_ERR_ADDR        = (1UL << 1), // dsp_ver_set_err_addr fail
    DSP_VER_SET_PARAM_ONLY      = (1UL << 2), // dsp_ver_set_param_only fail
    DSP_VER_ALU                 = (1UL << 3), // dsp_ver_alu fail
    DSP_VER_CONV                = (1UL << 4), // dsp_ver_conv fail
    DSP_VER_LUT                 = (1UL << 5), // dsp_ver_lut fail
    DSP_VER_MATRIX              = (1UL << 6), // dsp_ver_matrix fail
    DSP_VER_REG_DEF             = (1UL << 7), // default value of register fail
    DSP_VER_REG_ACCESS          = (1UL << 8), // register access fail
    DSP_VER_CLOCK               = (1UL << 9), // clock fail
    DSP_VER_HD                  = (1UL << 10), // hamming distance fail
    DSP_VER_DMUL                = (1UL << 11), // DMUL_8_8 fail
    DSP_VER_1D_CONV             = (1UL << 12), // 1D convolution fail
};
#endif

typedef struct {
    U32 addr1;
    U32 addr2;
    U32 out_base;
    U32 cal_time;
    U16 const_no[2];
    U8 d1_shift;
    U8 d2_shift;
    U8 out1_shift;
    U8 out2_shift;
    U8 matrix_i;
    U8 matrix_o;
    /* 1D Convolution*/
    U8 conv_xy;
    U8 conv_filter_num;
} DSP_PARAM_SETS;


/* Function Declaration */

/* ADD APIs */
U32 dsp_add_16_16(U32 addr1, U32 addr2, U32 out_base, U32 cal_time, U32 timeout, bool out_8bit);
U32 dsp_add_16_8(U32 addr1, U32 addr2, U32 out_base, U32 cal_time, U32 timeout, bool out_8bit);
U32 dsp_add_16_c1(U32 addr1, U32 c1, U32 out_base, U32 cal_time, U32 timeout, bool out_8bit);
U32 dsp_add_16_c2(U32 addr1, U32 c2, U32 out_base, U32 cal_time, U32 timeout, bool out_8bit);

/* SUB APIs */
U32 dsp_sub_16_16(U32 addr1, U32 addr2, U32 out_base, U32 cal_time, U32 timeout, bool out_8bit);
U32 dsp_sub_16_8(U32 addr1, U32 addr2, U32 out_base, U32 cal_time, U32 timeout, bool out_8bit);
U32 dsp_sub_16_c1(U32 addr1, U32 c1, U32 out_base, U32 cal_time, U32 timeout, bool out_8bit);
U32 dsp_sub_16_c2(U32 addr1, U32 c2, U32 out_base, U32 cal_time, U32 timeout, bool out_8bit);

/* MUL APIs */
U32 dsp_mul_16_16(U32 addr1, U32 addr2, U32 out_base, \
                  U8 d1_shift, U8 d2_shift, U8 out1_shift, \
                  U32 cal_time, U32 timeout, bool out_8bit);

U32 dsp_mul_16_8(U32 addr1, U32 addr2, U32 out_base, \
                 U8 d1_shift, U8 d2_shift, U8 out1_shift, \
                 U32 cal_time, U32 timeout, bool out_8bit);

U32 dsp_mul_16_c1(U32 addr1, U32 c1, U32 out_base, \
                  U8 d1_shift, U8 d2_shift, U8 out1_shift, \
                  U32 cal_time, U32 timeout, bool out_8bit);

U32 dsp_mul_16_c2(U32 addr1, U32 c2, U32 out_base, \
                  U8 d1_shift, U8 d2_shift, U8 out1_shift, \
                  U32 cal_time, U32 timeout, bool out_8bit);

U32 dsp_mac_16_c1_c2(U32 addr1, U32 c1, U32 c2, U32 out_base, \
                     U8 d1_shift, U8 d2_shift, U8 out1_shift, \
                     U32 cal_time, U32 timeout, bool out_8bit);

/* Convolution APIs */
U32 dsp_conv_exe(U32 image_addr, U32 image_width, U32 out_base, \
                 U8 out1_shift, U8 out2_shift, U32 cal_time, \
                 U32 timeout, bool out_8bit);

U32 dsp_1d_conv_exe(U32 image_addr, U32 image_width, U32 image_height, U32 out_base, \
                    U8 out1_shift, U8 out2_shift, U32 cal_time, \
                    U32 timeout, bool out_8bit, bool conv_xy, U8 filter_num);

/* LUT APIs */
U32 dsp_lut_load(U32 table_addr, U32 cal_time, U32 timeout);
U32 dsp_lut_exe(U32 idx_addr, U32 out_base, U32 cal_time, U32 timeout, bool out_8bit);
U32 dsp_lut_load_n_exe(U32 table_addr, U32 idx_addr, U32 out_base, \
                       U32 cal_time, U32 timeout, bool out_8bit);

/* Matrix API */
U32 dsp_matrix_exe(U32 addr1, U32 out_base, U8 matrix_in, U8 matrix_out, \
                   U8 out1_shift, U8 out2_shift, U32 cal_time, U32 timeout, bool out_8bit);

/* Hamming distance API */
U32 dsp_hd_exe(U32 addr1, U32 addr2, U32 out_base, U32 hd_cnt, U32 cal_time, U32 timeout);

/* DMUL 8 8 API */
U32 dsp_dmul_8_8(U32 addr1, U32 addr2, U32 out_base, \
                 U8 d1_shift, U8 d2_shift, U8 out1_shift, \
                 U32 cal_time, U32 timeout, bool out_8bit);

#if (CONFIG_DSP_VER_EN == 1)
void iap_ver(void);
#endif
#endif


