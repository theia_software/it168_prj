/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: ParameterSetting.h
 */

#ifndef _PARAMETERSETTING_H_
#define _PARAMETERSETTING_H_

#include <_types.h>
#include <lib.h>

#define CONFIG_UART_TEST_EN                 (0)
#define CONFIG_IIC_TEST_EN                  (0)
#define CONFIG_SPI_TEST_EN                  (0)
#define CONFIG_ET510_TEST_EN                (0)
#define CONFIG_IAP_TEST_EN                  (0)

#define CONFIG_FPGA                         (1)
#if (CONFIG_FPGA == 0)
/* ASIC version */
#define CONFIG_ASIC_V                       (0)
#endif

/* IT168 Global SRAM */
#define CONFIG_SRAM_START                   (0x00000000)
#define CONFIG_SRAM_LEN                     (0x00100000)

/* IT168 PE SRAM, only support 4B write */
#define CONFIG_PE_SRAM_START                (CONFIG_SRAM_START + CONFIG_SRAM_LEN)
#define CONFIG_PE_SRAM_LEN                  (0x0017a000)

/* memory layout */
#define CONFIG_RISCV_RAM_START              (CONFIG_SRAM_START)
#define CONFIG_RISCV_RAM_LEN                (0x20000)

#define CONFIG_FW_DEFINE_RAM_START          (CONFIG_RISCV_RAM_START + CONFIG_RISCV_RAM_LEN)
#define CONFIG_FW_DEFINE_RAM_LEN            (CONFIG_SRAM_LEN - CONFIG_RISCV_RAM_LEN)


#define STATUS_FLAG                         (0x00021ff8)

#endif

