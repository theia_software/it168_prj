#include <ParameterSetting.h>
#include <UART.h>
#include <I2C.h>
#include <lib.h>
#include <SPI_NOR.h>
#include <ISR.h>
#include <ts_et510d.h>
#include <eeprom.h>
#include <string.h>
#if (CONFIG_IAP_TEST_EN == 1)
#include <iap.h>
#endif





#if (CONFIG_UART_TEST_EN == 1) || (CONFIG_IIC_TEST_EN == 1) || \
    (CONFIG_SPI_TEST_EN == 1) || (CONFIG_ET510_TEST_EN == 1)
//--------------------------------------------------
S32 uart_Drv_Test_2(void)
{
	volatile U8* str;

	*((volatile U32*)0x00011100) = 0;

	str = (volatile U8*)0x00010000;

	strcpy((void *)str, "HELLO WORLD");
	uart_Open(UART0, 115200, PARITY_NONE, STOP_BITS_1);
	*((volatile U32*)0x00011100) = uart_Tx(UART0, 0x00010000, strlen((void *)str));

	return 0;
}
//--------------------------------------------------
S32 uart_Drv_Test_1(int isDMA)
{
	volatile U16 *ptr_1, *ptr_2;
	S32 i, err = 0;

	ptr_1 = (volatile U16*)0x00010000;
	for(i=0; i<128; i++){
		*ptr_1 = 0x55AA;
		ptr_1++;

	}

	ptr_1 = (volatile U16*)0x00010200;
	for(i=0; i<128; i++){
		*ptr_1 = 0x0000;
		ptr_1++;

	}

	*((volatile U32*)0x00011100) = 0;
	*((volatile U32*)0x00011110) = 0;

	ptr_1 = (volatile U16*)0x00010000;
	ptr_2 = (volatile U16*)0x00010200;

	if(isDMA){
		uart_Dma_Open(UART0, 57600, PARITY_NONE, STOP_BITS_1);
		uart_Dma_Open(UART1, 57600, PARITY_NONE, STOP_BITS_1);
		uart_Dma_Tx(UART0, 0x00010000, 0x10);
		uart_Dma_Rx(UART1, 0x00010200, 0x10);
		uart_Dma_WaitFinish(UART0);
	}else{
		uart_Open(UART0, 57600, PARITY_NONE, STOP_BITS_1);
		uart_Open(UART1, 57600, PARITY_NONE, STOP_BITS_1);
		*((volatile U32*)0x00011100) = uart_Tx(UART0, 0x00010000, 0x10);
		*((volatile U32*)0x00011110) = uart_Rx(UART1, 0x00010200, 0x10);
	}


	for(i=0; i<8; i++){
		if(*ptr_1 != *ptr_2){
			err++;
		}
		ptr_1++;
		ptr_2++;
	}

	return err;
}
//--------------------------------------------------
S32 iic_Slave_Drv_Test(U32 isTx)
{
	volatile U8* ptr;
	S32 i, err = 0;
	U8 iic_pattern[] = {254,13,157,19,183,5,55,168,56,221,
					252,164,3,159,107,113,146,175,62,35,
					73,41,86,80,73,85,98,84,215,189,
					1,37,247,125,20,99,215,133,55,94,
					216,69,123,38,51,130,238,100,214,202,
					168,188,245,81,65,33,220,201,117,144,
					14,17,193,182,220,62,172,133,113,213,
					147,212,49,56,161,168,154,226,102,38,
					96,41,244,30,207,211,100,204,234,253,
					140,17,98,70,54,203,61,90,216,35};


	if(!isTx){
		while(1){
			iic_Slave_Dma_RxData(I2C0, 0x6, 0x00010200, 0x64);

			ptr = (volatile U8*)0x00010200;
			for(i=0; i<100; i++){
				if(*ptr != iic_pattern[i]){
					err++;
				}
				ptr++;
			}

		}
	}else{

		iic_Slave_Dma_TxData(I2C0, 0x6, (U32)iic_pattern, 0x64);
	}

	return err;
}
//-------------------------------------------------
S32 spi_NorFlash_Drv_Test(void)
{
	TNorDev *pflashinfo;
	S32 err = 0;

	*((volatile U32*)0x00011100) = 0;
	*((volatile U32*)0x00011110) = 0;

	nor_SpiOpen(SPI2);
	pflashinfo = nor_ReadID();
	*((volatile U32*)0x00011100) = pflashinfo->MID;
	*((volatile U32*)0x00011110) = pflashinfo->TypeID;

	err = nor_Test(0x00020000, 0x00030000);

	return err;
}
//--------------------------------------------------
S32 et510d_Drv_Test(void)
{
	volatile U8* imgbuf;

	imgbuf = (volatile U8*)0x00010000;
	memset((void *)imgbuf, 0xAA, ET510D_PIXEL_SIZE);

	et510d_Init(SPI2);

	et510d_Type_Check();

	et510d_Scan_Frames(0);

	et510d_Scan_Csr();

	et510d_Stus();

	et510d_Dma_GetFrame((U32)imgbuf);

    return 0;
}
/*-----------------------------------------------------------*/
S32 eeprom_Test(void)
{
	U32 i, j, err = 0;
	U8	wbuf[16], rbuf[256];

	memset((void *)rbuf, 0, 256);

	for(i=0; i<16; i++){
		memset((void *)wbuf, (0x5A+i), 16);
		eeprom_WriteData(I2C0, i*16, (U32)wbuf, 16);
		delay(30000);
	}

	eeprom_ReadData(I2C0, 0, (U32)rbuf, 256);

	for(i = 0; i<16; i++){
		for(j=0; j<16; j++){
			if(rbuf[16*i+j] != (0x5A+i))
				err++;
		}
	}

	return err;
}
//------------------------------------------------------------------
S32 spi_Test(void)
{
	U32 i, err = 0;
	U8	mbuf[32], sbuf[32], reg;
	TSPIPara pSPIPara_m, pSPIPara_s;

	for(i=0; i<32; i++)
		mbuf[i] = (0x5A+i);
	memset((void *)sbuf, 0x11, 32);

	pSPIPara_m.tSPIID = SPI0;
	pSPIPara_m.Clk = 100000;

	pSPIPara_m.Phs = PHASE_2EDGE;		//sampling data at even edge
	pSPIPara_m.Pol = POLARITY_HIGH;		//high in the idle states
	pSPIPara_m.SlvMode = MASTER_MODE;	//master mode
	pSPIPara_m.Lsb = MSB_MODE;		//MSB first
	pSPIPara_m.DataMerge = DATAMERGE_OFF;//disable data merge mode
	pSPIPara_m.Bit = 8;		// 8 bits
	pSPIPara_m.AddrLen = ADDRLEN_1BYTE;	// 1 bytes address

	spi_Open(&pSPIPara_m);

	pSPIPara_s.tSPIID = SPI2;
	pSPIPara_s.Clk = 100000;

	pSPIPara_s.Phs = PHASE_2EDGE;		//sampling data at even edge
	pSPIPara_s.Pol = POLARITY_HIGH;		//high in the idle states
	pSPIPara_s.SlvMode = SLAVE_MODE;	//master mode
	pSPIPara_s.Lsb = MSB_MODE;		//MSB first
	pSPIPara_s.DataMerge = DATAMERGE_OFF;//disable data merge mode
	pSPIPara_s.Bit = 8;		// 8 bits
	pSPIPara_s.AddrLen = ADDRLEN_1BYTE;	// 1 bytes address

	spi_Open(&pSPIPara_s);

	spi_Master_WriteByte(&pSPIPara_m, 0x02, (U32)mbuf, 16);

	spi_Slave_ReadByte(&pSPIPara_s, &reg, (U32)sbuf, 16);

	for(i=0; i<16; i++){
		if(sbuf[i] != (0x5A+i))
			err++;
	}

	if(reg == 0x02){
		spi_Slave_WriteByte(&pSPIPara_s, (U32)sbuf, 16);

		memset((void *)mbuf, 0x11, 32);
		spi_Master_ReadByte(&pSPIPara_m, 0x02, (U32)mbuf, 16);

		for(i=0; i<16; i++){
			if(mbuf[i] != (0x5A+i))
				err++;
		}
	}

	return err;
}
#endif
//--------------------------------------------------
int main(void)
{
#if (CONFIG_UART_TEST_EN == 1) || (CONFIG_IIC_TEST_EN == 1) || \
    (CONFIG_SPI_TEST_EN == 1) || (CONFIG_ET510_TEST_EN == 1)
	S32 err = 0;

	ISR_setup();
	ISR_enable();

	set_dbg_val(STATUS_FLAG, 0x99);

#if (CONFIG_UART_TEST_EN == 1)
	err += uart_Drv_Test_1(0);

	err += uart_Drv_Test_2();
#elif (CONFIG_IIC_TEST_EN == 1)
	err += iic_Slave_Drv_Test(0);
#elif (CONFIG_SPI_TEST_EN == 1)
	err += spi_NorFlash_Drv_Test();
#elif (CONFIG_ET510_TEST_EN == 1)
	err += et510d_Drv_Test();
#endif

	set_dbg_val(STATUS_FLAG, err);
#else

#if (CONFIG_IAP_TEST_EN == 1)
    iap_ver();
#endif

#endif
}

