#ifndef __ATCWDT200_H
#define __ATCWDT200_H

#include <inttypes.h>
#include "general.h"


// ======================================================
// ATCWDT200 register definition
// ======================================================
// ATCWDT200 registers

typedef struct {
	volatile uint32_t IDREV;		// ID and Revision Register
	volatile uint32_t RESERVED[3];
	volatile uint32_t CTRL;			// Write-Protected Watchdog Timer Control Register
	volatile uint32_t RESTART;		// Watchdog Timer Restart Register
	volatile uint32_t WEN;			// Watchdog Timer Write Enable Register
	volatile uint32_t ST;			// Watchdog Timer Status Register
} ATCWDT200_RegDef;


// Write protect magic number
#ifndef ATCWDT200_WP_NUM
	#define ATCWDT200_WP_NUM	0x5aa5
#endif
// Restart WDT magic number
#ifndef ATCWDT200_RESTART_NUM
	#define ATCWDT200_RESTART_NUM	0xcafe
#endif

#define ATCWDT200_RSTTIME_128	0
#define ATCWDT200_RSTTIME_256	1
#define ATCWDT200_RSTTIME_512	2
#define ATCWDT200_RSTTIME_1024	3
#define ATCWDT200_RSTTIME_2048	4
#define ATCWDT200_RSTTIME_4096	5
#define ATCWDT200_RSTTIME_8192  6
#define ATCWDT200_RSTTIME_16384 7

#define ATCWDT200_INTTIME_64	0
#define ATCWDT200_INTTIME_256	1
#define ATCWDT200_INTTIME_1024	2
#define ATCWDT200_INTTIME_2048	3
#define ATCWDT200_INTTIME_4096	4
#define ATCWDT200_INTTIME_8192	5
#define ATCWDT200_INTTIME_16384	6
#define ATCWDT200_INTTIME_32768	7	// 2^15
#define ATCWDT200_INTTIME_131072     8	// 2^17
#define ATCWDT200_INTTIME_524288     9	// 2^19
#define ATCWDT200_INTTIME_2097152    10	// 2^21
#define ATCWDT200_INTTIME_8388608    11	// 2^23
#define ATCWDT200_INTTIME_33554432   12	// 2^25
#define ATCWDT200_INTTIME_134217728  13	// 2^27
#define ATCWDT200_INTTIME_536870912  14	// 2^29
#define ATCWDT200_INTTIME_2147483648 15	// 2^31

#define ATCWDT200_PCLK		1
#define ATCWDT200_EXTCLK	0

// ID and Revision Register
#define ATCWDT200_IDREV_ID_MASK		(BIT_MASK(31, 16))
#define ATCWDT200_IDREV_ID_OFFSET	(16)
#define ATCWDT200_IDREV_ID_DEFAULT	(0x0300)
#define ATCWDT200_IDREV_REVMAJOR_MASK	(BIT_MASK(15, 4))
#define ATCWDT200_IDREV_REVMAJOR_OFFSET	(4)
#define ATCWDT200_IDREV_REVMAJOR_DEFAULT	(0x200)
#define ATCWDT200_IDREV_REVMINOR_MASK	(BIT_MASK(3, 0))
#define ATCWDT200_IDREV_REVMINOR_OFFSET	(0)
#define ATCWDT200_IDREV_REVMINOR_DEFAULT	(2)
#define ATCWDT200_IDREV_DEFAULT ( \
		ATCWDT200_DEFAULT(IDREV, ID) |\
		ATCWDT200_DEFAULT(IDREV, REVMAJOR) |\
		ATCWDT200_DEFAULT(IDREV, REVMINOR) \
		)

// Write-Protected Watchdog Timer Control Register
#define ATCWDT200_CTRL_RSTTIME_MASK	(BIT_MASK(10, 8))
#define ATCWDT200_CTRL_RSTTIME_OFFSET	(8)
#define ATCWDT200_CTRL_RSTTIME_DEFAULT	(0)

#ifdef ATCWDT200_32BIT_TIMER
#define ATCWDT200_CTRL_INTTIME_MASK	(BIT_MASK(7, 4))
#else
// Bit 7 is fixed to 0.
#define ATCWDT200_CTRL_INTTIME_MASK	(BIT_MASK(6, 4))
#endif

#define ATCWDT200_CTRL_INTTIME_OFFSET	(4)
#define ATCWDT200_CTRL_INTTIME_DEFAULT	(0)
#define ATCWDT200_CTRL_RSTEN_MASK	(BIT_MASK(3, 3))
#define ATCWDT200_CTRL_RSTEN_OFFSET	(3)
#define ATCWDT200_CTRL_RSTEN_DEFAULT	(0)
#define ATCWDT200_CTRL_INTEN_MASK	(BIT_MASK(2, 2))
#define ATCWDT200_CTRL_INTEN_OFFSET	(2)
#define ATCWDT200_CTRL_INTEN_DEFAULT	(0)
#define ATCWDT200_CTRL_CLK_MASK		(BIT_MASK(1, 1))
#define ATCWDT200_CTRL_CLK_OFFSET	(1)
#define ATCWDT200_CTRL_CLK_DEFAULT	(0)
#define ATCWDT200_CTRL_EN_MASK		(BIT_MASK(0, 0))
#define ATCWDT200_CTRL_EN_OFFSET	(0)
#define ATCWDT200_CTRL_EN_DEFAULT	(0)
#define ATCWDT200_CTRL_DEFAULT ( \
		ATCWDT200_DEFAULT(CTRL, RSTTIME) |\
		ATCWDT200_DEFAULT(CTRL, INTTIME) |\
		ATCWDT200_DEFAULT(CTRL, RSTEN) |\
		ATCWDT200_DEFAULT(CTRL, INTEN) |\
		ATCWDT200_DEFAULT(CTRL, CLK) |\
		ATCWDT200_DEFAULT(CTRL, EN) \
		)

// Watchdog Timer Restart Register
#define ATCWDT200_RESTART_RESTART_MASK	(BIT_MASK(15, 0))
#define ATCWDT200_RESTART_RESTART_OFFSET	(0)
#define ATCWDT200_RESTART_RESTART_DEFAULT	(0)
#define ATCWDT200_RESTART_DEFAULT ( \
		ATCWDT200_DEFAULT(RESTART, RESTART) \
		)

// Watchdog Timer Write Enable Register
#define ATCWDT200_WEN_WEN_MASK	(BIT_MASK(15, 0))
#define ATCWDT200_WEN_WEN_OFFSET	(0)
#define ATCWDT200_WEN_WEN_DEFAULT	(0)
#define ATCWDT200_WEN_DEFAULT ( \
		ATCWDT200_DEFAULT(WEN, WEN) \
		)

// Watchdog Timer Status Register
#define ATCWDT200_ST_INTZERO_MASK	(BIT_MASK(0, 0))
#define ATCWDT200_ST_INTZERO_OFFSET	(0)
#define ATCWDT200_ST_INTZERO_DEFAULT	(0)
#define ATCWDT200_ST_DEFAULT ( \
		ATCWDT200_DEFAULT(ST, INTZERO) \
		)

// ======================================================
// ATCWDT200 access macro
// ======================================================
#define ATCWDT200_SET_FIELD(var, reg, field, value)	SET_FIELD(var, ATCWDT200_##reg##_##field##_##MASK, ATCWDT200_##reg##_##field##_##OFFSET, value)
#define ATCWDT200_GET_FIELD(var, reg, field)		GET_FIELD(var, ATCWDT200_##reg##_##field##_##MASK, ATCWDT200_##reg##_##field##_##OFFSET)
#define ATCWDT200_TEST_FIELD(var, reg, field)		TEST_FIELD(var, ATCWDT200_##reg##_##field##_##MASK)
#define ATCWDT200_PREPARE(reg, field, value)		PREPARE_FIELD(value, ATCWDT200_##reg##_##field##_##MASK, ATCWDT200_##reg##_##field##_##OFFSET )


#define ATCWDT200_DEFAULT(reg, field)			PREPARE_FIELD(ATCWDT200_##reg##_##field##_##DEFAULT, ATCWDT200_##reg##_##field##_##MASK, ATCWDT200_##reg##_##field##_##OFFSET )


#define ATCWDT200_CTRL_RSTTIME_128	ATCWDT200_PREPARE(CTRL, RSTTIME, ATCWDT200_RSTTIME_128)
#define ATCWDT200_CTRL_RSTTIME_256	ATCWDT200_PREPARE(CTRL, RSTTIME, ATCWDT200_RSTTIME_256)
#define ATCWDT200_CTRL_RSTTIME_512	ATCWDT200_PREPARE(CTRL, RSTTIME, ATCWDT200_RSTTIME_512)
#define ATCWDT200_CTRL_RSTTIME_1024	ATCWDT200_PREPARE(CTRL, RSTTIME, ATCWDT200_RSTTIME_1024)

#define ATCWDT200_CTRL_INTTIME_64	ATCWDT200_PREPARE(CTRL, INTTIME, ATCWDT200_INTTIME_64)
#define ATCWDT200_CTRL_INTTIME_256	ATCWDT200_PREPARE(CTRL, INTTIME, ATCWDT200_INTTIME_256)
#define ATCWDT200_CTRL_INTTIME_1024	ATCWDT200_PREPARE(CTRL, INTTIME, ATCWDT200_INTTIME_1024)
#define ATCWDT200_CTRL_INTTIME_2048	ATCWDT200_PREPARE(CTRL, INTTIME, ATCWDT200_INTTIME_2048)
#define ATCWDT200_CTRL_INTTIME_4096	ATCWDT200_PREPARE(CTRL, INTTIME, ATCWDT200_INTTIME_4096)
#define ATCWDT200_CTRL_INTTIME_8192	ATCWDT200_PREPARE(CTRL, INTTIME, ATCWDT200_INTTIME_8192)
#define ATCWDT200_CTRL_INTTIME_16384	ATCWDT200_PREPARE(CTRL, INTTIME, ATCWDT200_INTTIME_16384)

#define ATCWDT200_CTRL_RSTEN		ATCWDT200_PREPARE(CTRL, RSTEN, 0x1)
#define ATCWDT200_CTRL_INTEN		ATCWDT200_PREPARE(CTRL, INTEN, 0x1)

#define ATCWDT200_CTRL_CLK_PCLK		ATCWDT200_PREPARE(CTRL, CLK, ATCWDT200_PCLK)
#define ATCWDT200_CTRL_CLK_EXTCLK	ATCWDT200_PREPARE(CTRL, CLK, ATCWDT200_EXTCLK)

#define ATCWDT200_CTRL_EN		ATCWDT200_PREPARE(CTRL, EN, 0x1)

#endif //__ATCWDT200_H
