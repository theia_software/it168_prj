// Copyright (C) 2018, Andes Technology Corp. Confidential Proprietary


#include <inttypes.h>

#include "platform.h"
#include "core_v5.h"
#include "ndslib.h"
#include "interrupt.h"

#ifndef HEAP_SIZE
#define HEAP_SIZE 0x2000
#endif

nds_handler_p intr_handler_tab[INTERRUPT_NO];
nds_handler_p general_exc_handler_tab[GENERAL_EXC_NO];


static void soc_init(void) {
	DEV_SMU->CER = 0x7ff;
}

void default_general_exc_handler(uint32_t context_addr) {
	exit(-1);
}

static void setup_interrupt(uint32_t intr_no, nds_handler_p isr) {

	intr_handler_tab[intr_no] = isr;
}

static void enable_interrupt(uint32_t intr_no) {
	__nds__plic_set_priority(intr_no, 1);
	__nds__plic_enable_interrupt(intr_no);
}

static void disable_interrupt(uint32_t intr_no) {
	__nds__plic_set_priority(intr_no, 0);
}

static void clear_interrupt(uint32_t intr_no) {
}

void ae250_init(void) {
	int mmsc_cfg;
	int i;
	void *heap_base;
	int dlm_size;

	soc_init();

	set_csr(NDS_MSTATUS, MSTATUS_MPIE);

	set_csr(NDS_MIE, (1 << IRQ_M_EXT) | (1 << IRQ_BWE));

	mmsc_cfg = read_csr(NDS_MMSC_CFG);
	if (mmsc_cfg & 0x1000) {
		__nds__plic_set_feature(NDS_PLIC_FEATURE_VECTORED); 
	}

	__asm__ __volatile__ ("la %0, _stack" : "=r"(heap_base));

	dlm_size = (read_csr(NDS_MDCM_CFG) >> 15) & 0x1f;
	if ((dlm_size != 0) && (mmsc_cfg & 0x4000)) {
		heap_base += 0xA0000000;
	}
	heap_init(heap_base, HEAP_SIZE);

	intr_setup_ptr = setup_interrupt;
	intr_enable_ptr = enable_interrupt;
	intr_disable_ptr = disable_interrupt;
	intr_clear_ptr = clear_interrupt;

	for (i = 0; i < GENERAL_EXC_NO; i++) {
		general_exc_handler_tab[i] = default_general_exc_handler;
	}

}

