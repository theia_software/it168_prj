/*
 * Copyright (c) 2012-2017 Andes Technology Corporation
 * All rights reserved.
 *
 */

#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "config.h"
#include <stdarg.h>
#include "core_v5.h"
#include "ae250.h"
#include "atcwdt200.h"
#include "sim_control.h"
#include "atcpit100.h"
#include "atcrtc100.h"
//#include "uart.h"
//#include "gpio.h"
//#include "timer.h"

/*
 * Define 'NDS_PLIC_BASE' and 'NDS_PLIC_SW_BASE' before include platform
 * intrinsic header file to active PLIC/PLIC_SW related intrinsic functions.
 */
#define NDS_PLIC_BASE        PLIC_BASE
#define NDS_PLIC_SW_BASE     PLIC_SW_BASE
#include "nds_v5_platform.h"


#define INT_NO_PIT           3


#define INT_NO_RTC           1
#define INT_NO_RTC_ALARM     2

/*****************************************************************************
 * Peripheral device HAL declaration
 ****************************************************************************/
#define SIM_CONTROL_BASE	  (BMC_BASE + 0x80000)

#define WDT_BASE		      (0x90000000 + 0x00400000)
#define RTC_BASE	          (0x90000000 + 0x00800000)

#define AE250_SIM_CONTROL	((SIM_CONTROL_RegDef *)		SIM_CONTROL_BASE)
#define AE250_WDT		      ((ATCWDT200_RegDef *)		WDT_BASE)


#define DEV_SIM_CONTROL		   AE250_SIM_CONTROL
#define DEV_WDT			         AE250_WDT


#define DEV_PLMT             AE250_PLMT
#define DEV_DMA              AE250_DMA
#define DEV_SMU              AE250_SMU
#define DEV_UART1            AE250_UART1
#define DEV_UART2            AE250_UART2
#define DEV_PIT              AE250_PIT
#define DEV_RTC              AE250_RTC
#define DEV_GPIO             AE250_GPIO
#define DEV_I2C              AE250_I2C
#define DEV_SPI1             AE250_SPI1
#define DEV_SPI2             AE250_SPI2

/*****************************************************************************
 * Board specified
 ****************************************************************************/
#define GPIO_USED_MASK       0x7F    /* Which GPIOs to use */

#ifdef __cplusplus
}
#endif
#define printf(...)
#define sprintf(...)
#define snprintf(...)
#define write(...)
#endif	/* __PLATFORM_H__ */
