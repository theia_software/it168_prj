
#ifndef __ASM_INTERRUPT_H
#define __ASM_INTERRUPT_H

#ifndef INTERRUPT_NO
#define INTERRUPT_NO 16
#endif

#ifndef NMI_NO
#define NMI_NO 4
#endif

#ifndef GENERAL_EXC_NO
#define GENERAL_EXC_NO 16
#endif

#endif // __ASM_INTERRUPT_H
