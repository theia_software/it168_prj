// Copyright (C) 2018, Andes Technology Corp. Confidential Proprietary


#include "rand.h"


unsigned int nds_lfsr_seed = SEED;


int rand(void) {
	nds_lfsr_seed = (nds_lfsr_seed >> 1) ^ (-(nds_lfsr_seed & 1u) & 0xd0000001u);

	return ((int)nds_lfsr_seed);
}


