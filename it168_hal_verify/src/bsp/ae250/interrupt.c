#include <stdio.h>
#include <ISR.h>
#include "platform.h"
#include "interrupt.h"

//--------------------------------------

__attribute__((always_inline)) static inline void IRQ_ENTER(void)
{
	__asm volatile
	(
#ifdef __riscv_flen
		"addi sp, sp, -32*"REGSIZE"-16*"FPREGSIZE"\n"
#else
		"addi sp, sp, -32*"REGSIZE"     \n"
#endif
		PUSH " x1, 0(sp)                \n"
		"call IRQ_SAVE_CONTEXT          \n"
		//"csrrsi a5,mstatus,8            \n"		// allow nested
	);
}
//--------------------------------------
__attribute__((always_inline)) static inline void IRQ_EXIT(void)
{
	__asm volatile
	(
		"j IRQ_RESTORE_CONTEXT          \n"
	);
}
//--------------------------------------
__attribute__ ((naked, no_profile_instrument_function )) void entry_irq6(void)
{
	IRQ_ENTER();

	uart0_irq_handler();

	__nds__plic_complete_interrupt(IRQ_UART0_SOURCE);

	IRQ_EXIT();
}
//--------------------------------------
__attribute__ ((naked, no_profile_instrument_function )) void entry_irq7(void)
{
	IRQ_ENTER();

	uart1_irq_handler();

	__nds__plic_complete_interrupt(IRQ_UART1_SOURCE);

	IRQ_EXIT();
}
//--------------------------------------
__attribute__ ((naked, no_profile_instrument_function )) void entry_irq8(void)
{
	IRQ_ENTER();

	dma_irq_handler();

	__nds__plic_complete_interrupt(IRQ_DMA_SOURCE);

	IRQ_EXIT();
}
