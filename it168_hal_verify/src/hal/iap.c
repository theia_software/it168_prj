/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: iap.c
 */

#include <ParameterSetting.h>
#include <iap.h>
#include <string.h>
#include <ndslib.h>
#include <limits.h>

static U32 dsp_inst[(DSP_INST_LEN >> 2)];
static U32 *dsp_end;

#if (CONFIG_DSP_VER_EN == 1)
/* DSP verification FW check result */
static U32 dsp_fw_chk_res;
static U32 dsp_op;
static U32 dsp_test_loop;
static bool dsp_out_8bit;

static DSP_PARAM_SETS hw_param, fw_param;

static const U32 dsp_reg_def[DSP_REG_LEN >> 2];
static const U32 dsp_reg_def_vld_map[DSP_REG_LEN >> 2];
static const U32 dsp_reg_rw_vld_map[DSP_REG_LEN >> 2];

#if 0
static void dsp_ver_irq(void *id);
static void dsp_set_debug_flag(void);
#endif

static void gen_rand_pattern(void *addr, U32 len);
#if (CONFIG_FPGA == 0)
static U32 dsp_ver_clock_test(void);
#endif
static U32 dsp_ver_reset_n_reg_test(void);
static U32 dsp_ver_set_err_addr(void);
static U32 dsp_ver_set_param_only(void);
static U32 dsp_ver_alu(void);
static U32 dsp_ver_conv(void);
static U32 dsp_ver_lut(void);
static U32 dsp_ver_matrix(void);
static U32 dsp_ver_hd(void);
static U32 dsp_ver_dmul(void);
static U32 dsp_ver_1d_conv(void);

static U32 dsp_ver_fw_exe(void);
static void dsp_ver_fw_cmp(void);
#endif


static U32 dsp_write_inst(DSP_OP_CODE op_code, U32 data)
{
    if (*dsp_end) {
        if ((U32)dsp_end >= ((U32)dsp_inst + DSP_INST_LEN - 1))
            return DSP_ERR_INST_OUT_OF_RAM;

        dsp_end += 1;
    }

    *dsp_end = ((op_code << DSP_OP_CODE_MAP_OFF) | data);

    return 0;
}

static U32 dsp_set_param(DSP_OP_CODE op_code, U32 data)
{
    if ((op_code >= DSP_OP_SET_ADDR1_BASE) && (op_code <= DSP_OP_SET_OUTPUT_BASE)) {
        if ((data & DSP_OP_ADDR_CHK) || TEST_ALIGN_4(data))
            return DSP_ERR_PARAM;

    } else if ((op_code == DSP_OP_SET_CONST1) || (op_code == DSP_OP_SET_CONST2)) {
        if (data & DSP_OP_CONST_CHK)
            return DSP_ERR_PARAM;

    } else if (op_code == DSP_OP_SET_DATA_SHIFT) {
        if (data & DSP_OP_DATA_SHIFT_CHK)
            return DSP_ERR_PARAM;

    } else if (op_code == DSP_OP_SET_8BIT_OUTPUT) {
        if ((data != 0) && (data != DSP_OP_8BIT_OUTPUT_DATA))
            return DSP_ERR_PARAM;

    } else if (op_code == DSP_OP_SET_MATRIX_IN_OUT) {
        if (data & DSP_OP_MATRIX_IN_OUT_CHK)
            return DSP_ERR_PARAM;

    } else if (op_code == DSP_OP_SET_1D_CONV) {
        if (data & DSP_OP_SET_1D_CONV_CHK)
            return DSP_ERR_PARAM;

    } else {
        return DSP_ERR_OP_CODE;
    }

    return (dsp_write_inst(op_code, data));
}

static U32 dsp_set_add_op(DSP_OP_CODE op_code, U32 cal_time)
{
    if ((op_code < DSP_OP_ADD_16_16) || (op_code > DSP_OP_ADD_16_C2))
        return DSP_ERR_PARAM;

    if ((!cal_time) || (cal_time & DSP_OP_DATA_CHK) || \
        ((op_code == DSP_OP_ADD_16_8) && (cal_time & 1)))
        return DSP_ERR_CAL_TIME;

    return (dsp_write_inst(op_code, cal_time));
}

static U32 dsp_set_sub_op(DSP_OP_CODE op_code, U32 cal_time)
{
    if ((op_code < DSP_OP_SUB_16_16) || (op_code > DSP_OP_SUB_16_C2))
        return DSP_ERR_PARAM;

    if ((!cal_time) || (cal_time & DSP_OP_DATA_CHK) || \
        ((op_code == DSP_OP_SUB_16_8) && (cal_time & 1)))
        return DSP_ERR_CAL_TIME;

    return (dsp_write_inst(op_code, cal_time));
}

static U32 dsp_set_mul_op(DSP_OP_CODE op_code, U32 cal_time)
{
    if (((op_code < DSP_OP_MUL_16_16) || (op_code > DSP_OP_MUL_16_C2)) && \
        (op_code != DSP_OP_DMUL_8_8))
        return DSP_ERR_OP_CODE;

    if ((!cal_time) || (cal_time & DSP_OP_DATA_CHK) || \
        ((op_code == DSP_OP_MUL_16_8) && (cal_time & 1)))
        return DSP_ERR_CAL_TIME;

    return (dsp_write_inst(op_code, cal_time));
}

static U32 dsp_set_mac_op(U32 cal_time)
{
    if ((!cal_time) || (cal_time & DSP_OP_DATA_CHK))
        return DSP_ERR_CAL_TIME;

    return (dsp_write_inst(DSP_OP_MAC_16_C1_C2, cal_time));
}

static U32 dsp_set_conv_op(DSP_OP_CODE op_code, U32 cal_time)
{
    if (op_code == DSP_OP_LOAD_COEF) {
        if ((!cal_time) || (cal_time > DSP_OP_CONV_COEF_IDX_MAX))
            return DSP_ERR_COEF_NUM;

    } else if (op_code == DSP_OP_CONVOLUTION || op_code == DSP_OP_1D_CONV) {
        if (!cal_time)
            return DSP_ERR_CAL_TIME;

    } else {
        return DSP_ERR_OP_CODE;
    }

    return (dsp_write_inst(op_code, cal_time));
}

static U32 dsp_set_lut_op(DSP_OP_CODE op_code, U32 cal_time)
{
    if (op_code == DSP_OP_LOAD_LUT) {
        if (cal_time != DSP_OP_LOAD_LUT_CAL_TIME)
            return DSP_ERR_PARAM;

    } else if (op_code == DSP_OP_LUT) {
        if (!cal_time)
            return DSP_ERR_PARAM;

    } else {
        return DSP_ERR_PARAM;
    }

    return (dsp_write_inst(op_code, cal_time));
}

static U32 dsp_set_matrix_op(U32 cal_time)
{
    if ((!cal_time) || (cal_time & DSP_OP_DATA_CHK))
        return DSP_ERR_CAL_TIME;

    return (dsp_write_inst(DSP_OP_MATRIX, cal_time));
}

static U32 dsp_set_hd_op(U32 cal_time)
{
    if ((!cal_time) || (cal_time & DSP_OP_DATA_CHK))
        return DSP_ERR_CAL_TIME;

    return (dsp_write_inst(DSP_OP_HD, cal_time));
}

static void dsp_init(void)
{
    RW_DSP_REG(DSP_INST_START_ADDR) = (U32)dsp_inst;

    dsp_end = dsp_inst;
    *dsp_end = 0;
}

static U32 dsp_exe_chk_done(U32 timeout)
{
    const U32 no_timeout = !timeout;
#if (CONFIG_DSP_VER_EN == 1)
    U32 fw_check = dsp_ver_fw_exe();
#endif

    RW_DSP_REG(DSP_INST_END_ADDR) = (U32)dsp_end;
    RW_DSP_REG(DSP_START_TRIG) = 1;

    /* polling DSP done */
    do {
        if (RW_DSP_REG(DSP_DONE) == 1) {
            /* check DSP done & clear */
            RW_DSP_REG(DSP_DONE_CLEAR) = 1;
#if (CONFIG_DSP_VER_EN == 1)
            if (fw_check)
                dsp_ver_fw_cmp();
#endif
            return 0;
        }
    } while (no_timeout || timeout--);

    return DSP_ERR_DONE_TIMEOUT;
}

U32 dsp_add_16_16(U32 addr1, U32 addr2, U32 out_base, U32 cal_time, \
                  U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    dsp_init();

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_ADDR2_BASE, addr2);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (addr2 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_add_op(DSP_OP_ADD_16_16, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_add_16_8(U32 addr1, U32 addr2, U32 out_base, U32 cal_time, \
                 U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_8_range;
    U32 addr_16_range;

    dsp_init();

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_ADDR2_BASE, addr2);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_8_range = DSP_ADDR_RANGE_MAX - (cal_time << 1);
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (addr2 > addr_8_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_add_op(DSP_OP_ADD_16_8, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_add_16_c1(U32 addr1, U32 c1, U32 out_base, U32 cal_time, \
                  U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    dsp_init();

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_CONST1, c1);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_add_op(DSP_OP_ADD_16_C1, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_add_16_c2(U32 addr1, U32 c2, U32 out_base, U32 cal_time, \
                  U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    dsp_init();

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_CONST2, c2);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_add_op(DSP_OP_ADD_16_C2, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_sub_16_16(U32 addr1, U32 addr2, U32 out_base, U32 cal_time, \
                  U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    dsp_init();

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_ADDR2_BASE, addr2);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (addr2 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_sub_op(DSP_OP_SUB_16_16, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_sub_16_8(U32 addr1, U32 addr2, U32 out_base, U32 cal_time, \
                 U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_8_range;
    U32 addr_16_range;

    dsp_init();

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_ADDR2_BASE, addr2);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_8_range = DSP_ADDR_RANGE_MAX - (cal_time << 1);
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (addr2 > addr_8_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_sub_op(DSP_OP_SUB_16_8, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_sub_16_c1(U32 addr1, U32 c1, U32 out_base, U32 cal_time, \
                  U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    dsp_init();

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_CONST1, c1);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_sub_op(DSP_OP_SUB_16_C1, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_sub_16_c2(U32 addr1, U32 c2, U32 out_base, U32 cal_time, \
                  U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    dsp_init();

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_CONST2, c2);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_sub_op(DSP_OP_SUB_16_C2, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_mul_16_16(U32 addr1, U32 addr2, U32 out_base, \
                  U8 d1_shift, U8 d2_shift, U8 out1_shift, \
                  U32 cal_time, U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;
    U32 set_shift;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    if ((d1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (d2_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (out1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK)) {
        return DSP_ERR_SHIFT_BIT;
    }

    dsp_init();

    set_shift = (d1_shift << DSP_OP_D1_SHIFT_OFF) | \
                (d2_shift << DSP_OP_D2_SHIFT_OFF) | \
                (out1_shift << DSP_OP_OUT1_SHIFT_OFF);

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_ADDR2_BASE, addr2);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_DATA_SHIFT, set_shift);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (addr2 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_mul_op(DSP_OP_MUL_16_16, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_mul_16_8(U32 addr1, U32 addr2, U32 out_base, \
                 U8 d1_shift, U8 d2_shift, U8 out1_shift, \
                 U32 cal_time, U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_8_range;
    U32 addr_16_range;
    U32 set_shift;

    if ((d1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (d2_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (out1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK)) {
        return DSP_ERR_SHIFT_BIT;
    }

    dsp_init();

    set_shift = (d1_shift << DSP_OP_D1_SHIFT_OFF) | \
                (d2_shift << DSP_OP_D2_SHIFT_OFF) | \
                (out1_shift << DSP_OP_OUT1_SHIFT_OFF);

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_ADDR2_BASE, addr2);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_DATA_SHIFT, set_shift);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_8_range = DSP_ADDR_RANGE_MAX - (cal_time << 1);
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (addr2 > addr_8_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_mul_op(DSP_OP_MUL_16_8, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_mul_16_c1(U32 addr1, U32 c1, U32 out_base, \
                  U8 d1_shift, U8 d2_shift, U8 out1_shift, \
                  U32 cal_time, U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;
    U32 set_shift;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    if ((d1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (d2_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (out1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK)) {
        return DSP_ERR_SHIFT_BIT;
    }

    dsp_init();

    set_shift = (d1_shift << DSP_OP_D1_SHIFT_OFF) | \
                (d2_shift << DSP_OP_D2_SHIFT_OFF) | \
                (out1_shift << DSP_OP_OUT1_SHIFT_OFF);

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_CONST1, c1);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_DATA_SHIFT, set_shift);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_mul_op(DSP_OP_MUL_16_C1, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_mul_16_c2(U32 addr1, U32 c2, U32 out_base, \
                  U8 d1_shift, U8 d2_shift, U8 out1_shift, \
                  U32 cal_time, U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;
    U32 set_shift;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    if ((d1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (d2_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (out1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK)) {
        return DSP_ERR_SHIFT_BIT;
    }

    dsp_init();

    set_shift = (d1_shift << DSP_OP_D1_SHIFT_OFF) | \
                (d2_shift << DSP_OP_D2_SHIFT_OFF) | \
                (out1_shift << DSP_OP_OUT1_SHIFT_OFF);

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_CONST2, c2);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_DATA_SHIFT, set_shift);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_mul_op(DSP_OP_MUL_16_C2, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_mac_16_c1_c2(U32 addr1, U32 c1, U32 c2, U32 out_base, \
                     U8 d1_shift, U8 d2_shift, U8 out1_shift, \
                     U32 cal_time, U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;
    U32 set_shift;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    if ((d1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (d2_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (out1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK)) {
        return DSP_ERR_SHIFT_BIT;
    }

    dsp_init();

    /* (((d1 - cons1) >> d1_shift) x (const2 >> d2_shift)) >> out1_shift */
    set_shift = (d1_shift << DSP_OP_D1_SHIFT_OFF) | \
                (d2_shift << DSP_OP_D2_SHIFT_OFF) | \
                (out1_shift << DSP_OP_OUT1_SHIFT_OFF);

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_CONST1, c1);
    err_c |= dsp_set_param(DSP_OP_SET_CONST2, c2);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_DATA_SHIFT, set_shift);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_mac_op(cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

#if 0
U32 dsp_conv_load_coef(U32 coef_addr, U32 coef_num, U32 timeout)
{
    U32 err_c;
    U32 coef_range;


    dsp_init();

    /* load coef */
    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, coef_addr);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, DSP_COEF_80);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, 0);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    coef_range = DSP_ADDR_RANGE_MAX - DSP_OP_CONV_COEF_LEN_MAX;

    if (coef_addr > coef_range) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_conv_op(DSP_OP_LOAD_COEF, coef_num)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}
#endif

U32 dsp_conv_exe(U32 image_addr, U32 image_width, U32 out_base, \
                 U8 out1_shift, U8 out2_shift, U32 cal_time, \
                 U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;
    U32 set_shift;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    if ((out1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (out2_shift & DSP_OP_DATA_SHIFT_UNIT_CHK)) {
        return DSP_ERR_SHIFT_BIT;
    }

    if ((image_width >> 1) > DSP_HALF_IMAGE_WIDTH_MAX)
        return DSP_ERR_CONV_IMAGE_W_H;

    dsp_init();

    set_shift = (out1_shift << DSP_OP_OUT1_SHIFT_OFF) | \
                (out2_shift << DSP_OP_OUT2_SHIFT_OFF);

    /* CONVOLUTION */
    err_c = dsp_set_param(DSP_OP_SET_DATA_SHIFT, set_shift);
    err_c |= dsp_set_param(DSP_OP_SET_ADDR1_BASE, image_addr);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((image_addr > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    RW_DSP_REG(DSP_HALF_IMAGE_WIDTH) = (image_width >> 1);

    if (!(err_c = dsp_set_conv_op(DSP_OP_CONVOLUTION, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

#if 0
U32 dsp_conv_load_n_exe(U32 coef_addr, U32 coef_num, U32 image_addr, U32 image_width, \
                        U32 out_base, U8 out1_shift, U8 out2_shift, U32 cal_time, \
                        U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 coef_range;
    U32 addr_16_range;
    U32 set_shift;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    if ((out1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (out2_shift & DSP_OP_DATA_SHIFT_UNIT_CHK)) {
        return DSP_ERR_SHIFT_BIT;
    }

    dsp_init();

    set_shift = (out1_shift << DSP_OP_OUT1_SHIFT_OFF) | \
                (out2_shift << DSP_OP_OUT2_SHIFT_OFF);

    /* load coef */
    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, coef_addr);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, DSP_COEF_80);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, 0);
    err_c |= dsp_set_conv_op(DSP_OP_LOAD_COEF, coef_num);


    /* CONVOLUTION */
    err_c |= dsp_set_param(DSP_OP_SET_DATA_SHIFT, set_shift);
    err_c |= dsp_set_param(DSP_OP_SET_ADDR1_BASE, image_addr);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);
    coef_range = DSP_ADDR_RANGE_MAX - DSP_OP_CONV_COEF_LEN_MAX;

    if ((coef_addr > coef_range) || \
        (image_addr > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    RW_DSP_REG(DSP_HALF_IMAGE_WIDTH) = (image_width >> 1);

    if (!(err_c = dsp_set_conv_op(DSP_OP_CONVOLUTION, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}
#endif

U32 dsp_lut_load(U32 table_addr, U32 cal_time, U32 timeout)
{
    U32 err_c;
    U32 table_range;


    if (cal_time != DSP_OP_LOAD_LUT_CAL_TIME)
        return DSP_ERR_CAL_TIME;

    dsp_init();

    /* initial data shift parameter */
    err_c = dsp_set_param(DSP_OP_SET_DATA_SHIFT, 0);

    /* load table */
    err_c |= dsp_set_param(DSP_OP_SET_ADDR1_BASE, table_addr);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    table_range = DSP_ADDR_RANGE_MAX - DSP_LOAD_LUT_LEN;

    if (table_addr > table_range)
        return DSP_ERR_RANGE_OUT_OF_SRAM;

    if (!(err_c = dsp_set_lut_op(DSP_OP_LOAD_LUT, DSP_OP_LOAD_LUT_CAL_TIME)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_lut_exe(U32 idx_addr, U32 out_base, U32 cal_time, U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    dsp_init();

    /* initial data shift parameter */
    err_c = dsp_set_param(DSP_OP_SET_DATA_SHIFT, 0);

    /* LUT */
    err_c |= dsp_set_param(DSP_OP_SET_ADDR1_BASE, idx_addr);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((idx_addr > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_lut_op(DSP_OP_LUT, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_lut_load_n_exe(U32 table_addr, U32 idx_addr, U32 out_base, \
                       U32 cal_time, U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 table_range;
    U32 addr_16_range;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    dsp_init();

    /* initial data shift parameter */
    err_c = dsp_set_param(DSP_OP_SET_DATA_SHIFT, 0);

    /* load table */
    err_c |= dsp_set_param(DSP_OP_SET_ADDR1_BASE, table_addr);
    err_c |= dsp_set_lut_op(DSP_OP_LOAD_LUT, DSP_OP_LOAD_LUT_CAL_TIME);

    /* LUT */
    err_c |= dsp_set_param(DSP_OP_SET_ADDR1_BASE, idx_addr);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);
    table_range = DSP_ADDR_RANGE_MAX - DSP_LOAD_LUT_LEN;

    if ((table_addr > table_range) || \
        (idx_addr > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_lut_op(DSP_OP_LUT, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_matrix_exe(U32 addr1, U32 out_base, U8 matrix_in, U8 matrix_out, \
                          U8 out1_shift, U8 out2_shift, U32 cal_time, U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;
    U16 set_shift;
    U8 matrix_io;


    if ((matrix_in > DSP_OP_MATRIX_IO_MAX) || \
        (matrix_out > DSP_OP_MATRIX_IO_MAX) || \
        (matrix_in < matrix_out)) {
        return DSP_ERR_MATRIX_IN_OUT;
    }

    if (TEST_ALIGN_2(matrix_in) || TEST_ALIGN_2(matrix_out)) { // not aligned
        if (out_8bit) {
            if (cal_time % (matrix_in << 1))
                return DSP_ERR_MATRIX_IN_OUT;

        } else {
            if (cal_time % matrix_in)
                return DSP_ERR_MATRIX_IN_OUT;
        }
    } else {
        if (out_8bit) {
            if (cal_time % matrix_in)
                return DSP_ERR_MATRIX_IN_OUT;

        } else {
            if (cal_time % (matrix_in >> 1))
                return DSP_ERR_MATRIX_IN_OUT;
        }
    }

    if ((out1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (out2_shift & DSP_OP_DATA_SHIFT_UNIT_CHK)) {
        return DSP_ERR_SHIFT_BIT;
    }

    dsp_init();

    matrix_io = (matrix_in << DSP_OP_MATRIX_I_OFF) | \
                (matrix_out << DSP_OP_MATRIX_O_OFF);

    set_shift = (out1_shift << DSP_OP_OUT1_SHIFT_OFF) | \
                (out2_shift << DSP_OP_OUT2_SHIFT_OFF);

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_MATRIX_IN_OUT, matrix_io);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_DATA_SHIFT, set_shift);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_matrix_op(cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_hd_exe(U32 addr1, U32 addr2, U32 out_base, U32 hd_cnt, U32 cal_time, U32 timeout)
{
    U32 err_c;
    U32 addr_16_range;


    if (!hd_cnt || (cal_time % hd_cnt) || (hd_cnt & !DSP_HD_CNT_MASK))
        return DSP_ERR_HD_CNT;

    dsp_init();

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_ADDR2_BASE, addr2);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (addr2 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    RW_DSP_REG(DSP_HD_CNT) = hd_cnt;

    if (!(err_c = dsp_set_hd_op(cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_dmul_8_8(U32 addr1, U32 addr2, U32 out_base, \
                 U8 d1_shift, U8 d2_shift, U8 out1_shift, \
                 U32 cal_time, U32 timeout, bool out_8bit)
{
    U32 err_c;
    U32 addr_16_range;
    U32 set_shift;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    if ((d1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (d2_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (out1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK)) {
        return DSP_ERR_SHIFT_BIT;
    }

    dsp_init();

    set_shift = (d1_shift << DSP_OP_D1_SHIFT_OFF) | \
                (d2_shift << DSP_OP_D2_SHIFT_OFF) | \
                (out1_shift << DSP_OP_OUT1_SHIFT_OFF);

    err_c = dsp_set_param(DSP_OP_SET_ADDR1_BASE, addr1);
    err_c |= dsp_set_param(DSP_OP_SET_ADDR2_BASE, addr2);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_DATA_SHIFT, set_shift);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((addr1 > addr_16_range) || \
        (addr2 > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    if (!(err_c = dsp_set_mul_op(DSP_OP_DMUL_8_8, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

U32 dsp_1d_conv_exe(U32 image_addr, U32 image_width, U32 image_height, U32 out_base, \
                    U8 out1_shift, U8 out2_shift, U32 cal_time, \
                    U32 timeout, bool out_8bit, bool conv_xy, U8 filter_num)
{
    U32 err_c;
    U32 addr_16_range;
    U32 set_shift;


    if (out_8bit && TEST_ALIGN_2(cal_time))
        return DSP_ERR_CAL_TIME;

    if ((out1_shift & DSP_OP_DATA_SHIFT_UNIT_CHK) || \
        (out2_shift & DSP_OP_DATA_SHIFT_UNIT_CHK)) {
        return DSP_ERR_SHIFT_BIT;
    }

    if ((filter_num > DSP_OP_SET_1D_CONV_FILTER_MAX) ||  \
        (filter_num < DSP_OP_SET_1D_CONV_FILTER_MIN) ||  \
        !TEST_ALIGN_2(filter_num))
        return DSP_ERR_1D_CONV_FILTER_NUM;

    if (((conv_xy == DSP_OP_SET_1D_CONV_X) && \
         (image_width < filter_num)) || \
        ((conv_xy == DSP_OP_SET_1D_CONV_Y) && \
         (image_height < filter_num)))
        return DSP_ERR_CONV_IMAGE_W_H;

    dsp_init();

    set_shift = (out1_shift << DSP_OP_OUT1_SHIFT_OFF) | \
                (out2_shift << DSP_OP_OUT2_SHIFT_OFF);

    /* CONVOLUTION */
    err_c = dsp_set_param(DSP_OP_SET_DATA_SHIFT, set_shift);
    err_c |= dsp_set_param(DSP_OP_SET_ADDR1_BASE, image_addr);
    err_c |= dsp_set_param(DSP_OP_SET_OUTPUT_BASE, out_base);
    err_c |= dsp_set_param(DSP_OP_SET_8BIT_OUTPUT, out_8bit);
    err_c |= dsp_set_param(DSP_OP_SET_1D_CONV, \
                           ((filter_num << DSP_OP_SET_1D_CONV_FILTER_OFF) | conv_xy));

    if (err_c)
        return err_c;

    /* DSP input & output range can't exceed SRAM */
    addr_16_range = DSP_ADDR_RANGE_MAX - (cal_time << 2);

    if ((image_addr > addr_16_range) || \
        (out_base > addr_16_range)) {
        return DSP_ERR_RANGE_OUT_OF_SRAM;
    }

    RW_DSP_REG(DSP_1D_CONV_IMG_WD) = image_width;
    RW_DSP_REG(DSP_1D_CONV_IMG_HT) = image_height;

    if (!(err_c = dsp_set_conv_op(DSP_OP_1D_CONV, cal_time)))
        err_c = dsp_exe_chk_done(timeout);

    return err_c;
}

#if (CONFIG_DSP_VER_EN == 1)
void iap_ver(void)
{
    U32 err_c;


    //request_irq(IRQ_DSP_DONE, dsp_ver_irq, (void *)0);

    set_dbg_val(DEBUG_DSP_FW_CHK, 0);
    set_dbg_val(DEBUG_DSP_INSTRU, 0);
    set_dbg_val(DEBUG_DSP_ERR_CODE, 0);
    set_dbg_val(DEBUG_DSP_DONE_RAM, 0);
    set_dbg_val(DEBUG_DSP_TEST_LOOP, 0);

    do {
        /* waiting for sync flag by external device */
        while (read_dbg_val(STATUS_FLAG) != DSP_VER_STA_SYNC);

        set_dbg_val(DEBUG_DSP_TEST_LOOP, ++dsp_test_loop);

        set_dbg_val(STATUS_FLAG,  DSP_VER_STA_START);

        /* waiting for start ack */
        while (read_dbg_val(STATUS_FLAG) != DSP_VER_STA_START_ACK);

#if 0
        dsp_set_debug_flag();
#endif

        do {
#if (CONFIG_FPGA == 0)
            if ((err_c = dsp_ver_clock_test()))
                break;
#endif

            if ((err_c = dsp_ver_reset_n_reg_test()))
                break;

            if ((err_c = dsp_ver_set_err_addr()))
                break;

            if ((err_c = dsp_ver_set_param_only()))
                break;

            if ((err_c = dsp_ver_alu()))
                break;

            if ((err_c = dsp_ver_conv()))
                break;

            if ((err_c = dsp_ver_lut()))
                break;

            if ((err_c = dsp_ver_1d_conv()))
                break;

            if ((err_c = dsp_ver_matrix()))
                break;

            if ((err_c = dsp_ver_hd()))
                break;

            if ((err_c = dsp_ver_dmul()))
                break;

        } while (0);

        set_dbg_val(STATUS_FLAG, err_c);

    } while (1);
}

#if 0
static void dsp_ver_irq(void *id)
{
    static U32 dsp_done_count = 0;

    set_dbg_val(DEBUG_DSP_DONE_RAM, ++dsp_done_count);
}
#endif

#if 0
static void dsp_set_debug_flag(void)
{
#if 1
    /* sel 0x16 */
    RW_SYS_REG(SYS_DBG_FLAG_EN)         = 0x00000001; // enable debug flag
    RW_SYS_REG(SYS_PAD_GPIO_IN_SEL)     = 0x000000ff; // enable pad control
    RW_SYS_REG(SYS_DBG_FLAG_MOD_SEL)    = 0x00000066; // choose debug module
    RW_SYS_REG(SYS_DBG_FLAG_SUB_SEL_00) = 0x00000000; // debug signal in module
    RW_SYS_REG(SYS_DBG_FLAG_SUB_SEL_01) = 0x00160000; // debug signal in module
    /* debug flag should output 0x09 */
#else
    /* sel 0x3d */
    RW_SYS_REG(SYS_DBG_FLAG_EN)         = 0x00000001; // enable debug flag
    RW_SYS_REG(SYS_PAD_GPIO_IN_SEL)     = 0x000000ff; // enable pad control
    RW_SYS_REG(SYS_DBG_FLAG_MOD_SEL)    = 0x00000066; // choose debug module
    RW_SYS_REG(SYS_DBG_FLAG_SUB_SEL_00) = 0x00000000; // debug signal in module
    RW_SYS_REG(SYS_DBG_FLAG_SUB_SEL_01) = 0x003d0000; // debug signal in module
    /* debug flag should output 0x07 */
#endif
}
#endif

#if (CONFIG_FPGA == 0)
static U32 dsp_ver_clock_test(void)
{
    const U32 data1 = 0x11111111;
    const U32 data2 = 0x22222222;
    const U32 lut_idx = 0x11115555;
    U32 idx;
    U32 lut_correct;
    U32 lut_out;
    U32 add_out;
    U8 chk_out_err = 0;
    U8 lut_table[DSP_LOAD_LUT_LEN];


    for (idx = 0; idx < DSP_LOAD_LUT_LEN; ++idx) {
        lut_table[idx] = idx;
    }

    /*
     * idx = 0, reset > DSP add & load and exe LUT > output
     *
     * idx = 1, DSP SRAM clock off > reset > DSP add > add output should pass
     *                                       DSP LUT   LUT output should fail
     *
     * idx = 2, DSP clock off > reset > DSP add & LUT > no output
     *
     * idx = 3, DSP & SRAM clock on > reset > DSP add > add output should pass
     *                                        DSP LUT   LUT output should pass
     * notice: reset only reset DSP IP exclude DSP SRAM
     */
    for (idx = 0; idx < 4; ++idx) {

        add_out = 0;

        if (idx == 1) {
            RW_SYS_REG(SYS_CLK_EN) &= SYS_DSP_SRAM_CLK_DIS;

        } else if (idx == 2) {
            RW_SYS_REG(SYS_CLK_EN) &= SYS_DSP_CLK_DIS;

        } else if (idx == 3) {
            RW_SYS_REG(SYS_CLK_EN) |= (SYS_DSP_CLK_EN | SYS_DSP_SRAM_CLK_EN);
        }

        DSP_RESET();

        /* DSP add */
        dsp_init();

        dsp_set_param(DSP_OP_SET_ADDR1_BASE, (U32)&data1);
        dsp_set_param(DSP_OP_SET_ADDR2_BASE, (U32)&data2);
        dsp_set_param(DSP_OP_SET_OUTPUT_BASE, (U32)&add_out);

        dsp_set_add_op(DSP_OP_ADD_16_16, 1);

        RW_DSP_REG(DSP_INST_END_ADDR) = (U32)dsp_end;
        RW_DSP_REG(DSP_START_TRIG) = 1;

        delay(10);

        if (idx == 2) {
            /* check DSP done */
            if (RW_DSP_REG(DSP_DONE) != 0) {
                RW_DSP_REG(DSP_DONE_CLEAR) = 1;
                chk_out_err = 1;
                break;
            }

            if (add_out != 0) {
                chk_out_err = 2;
                break;
            }

        } else {
            /* check DSP done */
            if (RW_DSP_REG(DSP_DONE) != 1) {
                chk_out_err = 3;
                break;
            }
            RW_DSP_REG(DSP_DONE_CLEAR) = 1;

            if (add_out != (data1 + data2)) {
                chk_out_err = 4;
                break;
            }
        }

        /* DSP LUT */
        if (idx == 0) {
            dsp_lut_load((U32)lut_table, DSP_OP_LOAD_LUT_CAL_TIME, DSP_OP_LOAD_LUT_CAL_TIME);
            lut_correct = 0;
        }

        lut_out = 0;

        dsp_init();

        dsp_set_param(DSP_OP_SET_ADDR1_BASE, (U32)&lut_idx);
        dsp_set_param(DSP_OP_SET_OUTPUT_BASE, (idx ? (U32)&lut_out : (U32)&lut_correct));

        dsp_set_lut_op(DSP_OP_LUT, 1);

        RW_DSP_REG(DSP_INST_END_ADDR) = (U32)dsp_end;
        RW_DSP_REG(DSP_START_TRIG) = 1;

        delay(10);

        if (idx == 2) {
            /* check DSP done */
            if (RW_DSP_REG(DSP_DONE) != 0) {
                RW_DSP_REG(DSP_DONE_CLEAR) = 1;
                chk_out_err = 5;
                break;
            }

            if (add_out != 0) {
                chk_out_err = 6;
                break;
            }

        } else {
            /* check DSP done */
            if (RW_DSP_REG(DSP_DONE) != 1) {
                chk_out_err = 7;
                break;
            }
            RW_DSP_REG(DSP_DONE_CLEAR) = 1;

            if (((idx == 1) && (lut_out == lut_correct)) || \
                ((idx == 3) && (lut_out != lut_correct))) {
                chk_out_err = 8;
                break;
            }
        }
    }

    if (chk_out_err) {
        set_dbg_val(DEBUG_DSP_FW_CHK, ((chk_out_err << 8) | idx | DSP_VER_FW_CHK_FAIL));
        set_dbg_val(DEBUG_DSP_DATA1, lut_correct);
        set_dbg_val(DEBUG_DSP_DATA2, lut_idx);
        set_dbg_val(DEBUG_DSP_OUTPUT, lut_out);
        return (DSP_VER_RES_FAIL | DSP_VER_CLOCK);
    }

    return DSP_VER_RES_PASS;
}
#endif

static void dsp_ver_reg_dbg_info(U32 addr_off, U32 expect_data)
{
    set_dbg_val(DEBUG_DSP_FW_CHK, (addr_off | DSP_VER_FW_CHK_FAIL));
    set_dbg_val(DEBUG_DSP_DATA1, expect_data);

    /* actual data */
    set_dbg_val(DEBUG_DSP_DATA2, (RW_DSP_REG(addr_off) & dsp_reg_def_vld_map[addr_off >> 2]));
}

static U32 dsp_ver_reset_n_reg_test(void)
{
    U32 idx, off;
    U32 pattern, p_idx;
    U32 wdata;


    DSP_RESET();

    /* check default value */
    for (off = 0, idx = 0; off < DSP_REG_LEN; off += 4, ++idx) {
        if ((RW_DSP_REG(off) & dsp_reg_def_vld_map[idx]) != dsp_reg_def[idx]) {
            dsp_ver_reg_dbg_info(off, dsp_reg_def[idx]);
            return (DSP_VER_RES_FAIL | DSP_VER_REG_DEF);
        }
    }

    /* check register access */
    for (p_idx = 0; p_idx < 5; ++p_idx) {

        if (p_idx == 0)
            pattern = ~0;
        else if (p_idx == 1)
            pattern = 0x5a5a5a5a;
        else if (p_idx == 2)
            pattern = 0xa5a5a5a5;
        else if (p_idx == 3)
            pattern = 0;
        else
            pattern = (U32)rand();

        for (off = 0, idx = 0; off < DSP_REG_LEN; off += 4, ++idx) {

            wdata = pattern & dsp_reg_rw_vld_map[idx];
            RW_DSP_REG(off) = wdata;

            if ((RW_DSP_REG(off) & dsp_reg_rw_vld_map[idx]) != wdata) {
                dsp_ver_reg_dbg_info(off, wdata);
                return (DSP_VER_RES_FAIL | DSP_VER_REG_ACCESS);
            }
        }
    }

    DSP_RESET();

    return DSP_VER_RES_PASS;
}

static U32 dsp_ver_set_err_addr(void)
{
    /*
     * set DSP start > end address and trigger DSP
     * check HW, DSP should do nothing.
     */
    const U32 dsp_done_count = read_dbg_val(DEBUG_DSP_DONE_RAM);
    /* range 4 ~  DSP_INST_LEN */
    const U32 rand_len = SET_DEC_ALIGN_4((U32)rand() % DSP_INST_LEN) + 4;

    const U32 dsp_err_start = ((U32)dsp_inst + rand_len);
    U32 *dsp_err_end = dsp_inst;
    U32 op_code, data;


    RW_DSP_REG(DSP_INST_START_ADDR) = dsp_err_start;
    RW_DSP_REG(DSP_INST_END_ADDR) = (U32)dsp_err_end;

    do {
        op_code = ((U32)rand() % DSP_OP_SET_CONST2) + 1;
        data = (U32)rand();

        if (op_code <= DSP_OP_SET_OUTPUT_BASE)
            data &= DSP_OP_ADDR_MASK;
        else
            data &= DSP_OP_CONST_MASK;

        *dsp_err_end++ = ((op_code << DSP_OP_CODE_MAP_OFF) | data);

    } while ((U32)dsp_err_end <= dsp_err_start);

    RW_DSP_REG(DSP_START_TRIG) = 1;

    delay(rand_len);

    if (RW_DSP_REG(DSP_DONE) || \
        read_dbg_val(DEBUG_DSP_DONE_RAM) != dsp_done_count) {

        set_dbg_val(DEBUG_DSP_DATA1, RW_DSP_REG(DSP_INST_START_ADDR));
        set_dbg_val(DEBUG_DSP_DATA2, RW_DSP_REG(DSP_INST_END_ADDR));
        set_dbg_val(DEBUG_DSP_CAL_LEN, rand_len);

        return (DSP_VER_RES_FAIL | DSP_VER_SET_ERR_ADDR);
    }

    return DSP_VER_RES_PASS;
}

static U32 dsp_ver_set_param_only(void)
{
    /*
     * no computation code, set DSP parameters only
     * check HW, DSP should not hang.
     */
    /* range 0 ~  DSP_INST_LEN */
    U32 rand_len = SET_INC_ALIGN_4((U32)rand() % DSP_INST_LEN);

    const U32 dsp_nor_end = ((U32)dsp_inst + rand_len);
    U32 *dsp_nor_start = dsp_inst;
    U32 op_code, data, timeout;


    RW_DSP_REG(DSP_INST_START_ADDR) = (U32)dsp_nor_start;
    RW_DSP_REG(DSP_INST_END_ADDR) = dsp_nor_end;

    do {
        op_code = ((U32)rand() % DSP_OP_SET_CONST2) + 1;
        data = (U32)rand();

        if (op_code <= DSP_OP_SET_OUTPUT_BASE)
            data &= DSP_OP_ADDR_MASK;
        else
            data &= DSP_OP_CONST_MASK;

        *dsp_nor_start++ = ((op_code << DSP_OP_CODE_MAP_OFF) | data);

    } while ((U32)dsp_nor_start <= dsp_nor_end);

    RW_DSP_REG(DSP_START_TRIG) = 1;

    timeout = rand_len + 1;

    do {
        if (RW_DSP_REG(DSP_DONE) == 1) {
            /* check DSP done & clear */
            RW_DSP_REG(DSP_DONE_CLEAR) = 1;

            return DSP_VER_RES_PASS;
        }
    } while (timeout--);

    set_dbg_val(DEBUG_DSP_DATA1, RW_DSP_REG(DSP_INST_START_ADDR));
    set_dbg_val(DEBUG_DSP_DATA2, RW_DSP_REG(DSP_INST_END_ADDR));
    set_dbg_val(DEBUG_DSP_CAL_LEN, rand_len);

    return (DSP_VER_RES_FAIL | DSP_VER_SET_PARAM_ONLY);
}

static U32 dsp_ver_alu(void)
{
    DSP_PARAM_SETS dsp_param = {
        .addr1 = DSP_TEST_ALU_HW_PAT_START_ADDR,
    };
    U32 cal_len, timeout;
    U32 rand_op, rand_op_2;
    U32 err_c = 0;


    set_dbg_val(DEBUG_DSP_FW_ADDR_OFF, DSP_TEST_ALU_FW_PAT_OFF);

    dsp_param.const_no[0] = 0;
    dsp_param.const_no[1] = 0;
    dsp_param.d1_shift = 0;
    dsp_param.d2_shift = 0;
    dsp_param.out1_shift = 0;
    dsp_param.out2_shift = 0;

    rand_op = ((U32)rand() % 4);
    rand_op_2 = ((U32)rand() % 3);
    dsp_out_8bit = (U32)rand() & 1;

    do {
        dsp_param.cal_time = ((U32)rand() % DSP_TEST_ALU_CAL_TIME_MAX);

        /* 8 bit calculation */
        if (dsp_out_8bit || (rand_op == 1))
            dsp_param.cal_time = SET_DEC_ALIGN_2(dsp_param.cal_time);

    } while (dsp_param.cal_time == 0);

    if ((rand_op == 4) || (rand_op_2 == 2)) {
        dsp_param.d1_shift = ((U32)rand() & DSP_OP_DATA_SHIFT_UNIT_MASK);
        dsp_param.d2_shift = ((U32)rand() & DSP_OP_DATA_SHIFT_UNIT_MASK);
        dsp_param.out1_shift = ((U32)rand() & DSP_OP_DATA_SHIFT_UNIT_MASK);
    }

    cal_len = (dsp_param.cal_time << 2);
    timeout = (dsp_param.cal_time << 2);

    dsp_param.addr2 = (dsp_param.addr1 + cal_len);
    dsp_param.out_base = (dsp_param.addr2 + cal_len);

    set_dbg_val(DEBUG_DSP_DATA1, dsp_param.addr1);
    set_dbg_val(DEBUG_DSP_DATA2, dsp_param.addr2);
    set_dbg_val(DEBUG_DSP_CAL_LEN, cal_len);
    set_dbg_val(DEBUG_DSP_OUTPUT, dsp_param.out_base);

#if 0 /* initial HW & FW pattern SRAM */
    memset((void *)(dsp_param.addr1), 0, (cal_len << 2));
    memset((void *)(dsp_param.addr1 + DSP_TEST_ALU_FW_PAT_OFF), 0, (cal_len << 2));
#endif

    gen_rand_pattern((void *)dsp_param.addr1, cal_len);
    gen_rand_pattern((void *)dsp_param.addr2, cal_len);

    memcpy((void *)(dsp_param.addr1 + DSP_TEST_ALU_FW_PAT_OFF), \
           (void *)(dsp_param.addr1), cal_len);
    memcpy((void *)(dsp_param.addr2 + DSP_TEST_ALU_FW_PAT_OFF), \
           (void *)(dsp_param.addr2), cal_len);

    if (rand_op == 0) {
        if (rand_op_2 == 0) {
            err_c = dsp_add_16_16(dsp_param.addr1, dsp_param.addr2, dsp_param.out_base, \
                                  dsp_param.cal_time, timeout, dsp_out_8bit);

        } else if (rand_op_2 == 1) {
            err_c = dsp_sub_16_16(dsp_param.addr1, dsp_param.addr2, dsp_param.out_base, \
                                  dsp_param.cal_time, timeout, dsp_out_8bit);

        } else if (rand_op_2 == 2) {
            err_c = dsp_mul_16_16(dsp_param.addr1, dsp_param.addr2, \
                                  dsp_param.out_base, \
                                  dsp_param.d1_shift, dsp_param.d2_shift, \
                                  dsp_param.out1_shift, \
                                  dsp_param.cal_time, timeout, dsp_out_8bit);
        }
    } else if (rand_op == 1) {
        if (rand_op_2 == 0) {
            err_c = dsp_add_16_8(dsp_param.addr1, dsp_param.addr2, dsp_param.out_base, \
                                 dsp_param.cal_time, timeout, dsp_out_8bit);

        } else if (rand_op_2 == 1) {
            err_c = dsp_sub_16_8(dsp_param.addr1, dsp_param.addr2, dsp_param.out_base, \
                                 dsp_param.cal_time, timeout, dsp_out_8bit);

        } else if (rand_op_2 == 2) {
            err_c = dsp_mul_16_8(dsp_param.addr1, dsp_param.addr2, \
                                 dsp_param.out_base, \
                                 dsp_param.d1_shift, dsp_param.d2_shift, \
                                 dsp_param.out1_shift, \
                                 dsp_param.cal_time, timeout, dsp_out_8bit);
        }
    } else if (rand_op == 2) {
        dsp_param.const_no[0] = (rand() & DSP_OP_CONST_MASK);
        set_dbg_val(DEBUG_DSP_DATA2, dsp_param.const_no[0]);

        if (rand_op_2 == 0) {
            err_c = dsp_add_16_c1(dsp_param.addr1, dsp_param.const_no[0], \
                                  dsp_param.out_base, \
                                  dsp_param.cal_time, timeout, dsp_out_8bit);

        } else if (rand_op_2 == 1) {
            err_c = dsp_sub_16_c1(dsp_param.addr1, dsp_param.const_no[0], \
                                  dsp_param.out_base, \
                                  dsp_param.cal_time, timeout, dsp_out_8bit);

        } else if (rand_op_2 == 2) {
            err_c = dsp_mul_16_c1(dsp_param.addr1, dsp_param.const_no[0], \
                                  dsp_param.out_base, \
                                  dsp_param.d1_shift, dsp_param.d2_shift, \
                                  dsp_param.out1_shift, \
                                  dsp_param.cal_time, timeout, dsp_out_8bit);
        }
    } else if (rand_op == 3) {
        dsp_param.const_no[1] = (rand() & DSP_OP_CONST_MASK);
        set_dbg_val(DEBUG_DSP_DATA2, dsp_param.const_no[1]);

        if (rand_op_2 == 0) {
            err_c = dsp_add_16_c2(dsp_param.addr1, dsp_param.const_no[1], \
                                  dsp_param.out_base, \
                                  dsp_param.cal_time, timeout, dsp_out_8bit);

        } else if (rand_op_2 == 1) {
            err_c = dsp_sub_16_c2(dsp_param.addr1, dsp_param.const_no[1], \
                                  dsp_param.out_base, \
                                  dsp_param.cal_time, timeout, dsp_out_8bit);

        } else if (rand_op_2 == 2) {
            err_c = dsp_mul_16_c2(dsp_param.addr1, dsp_param.const_no[1], \
                                  dsp_param.out_base, \
                                  dsp_param.d1_shift, dsp_param.d2_shift, \
                                  dsp_param.out1_shift, \
                                  dsp_param.cal_time, timeout, dsp_out_8bit);
        }
    } else {
        dsp_param.const_no[0] = (rand() & DSP_OP_CONST_MASK);
        dsp_param.const_no[1] = (rand() & DSP_OP_CONST_MASK);

        set_dbg_val(DEBUG_DSP_DATA2, (dsp_param.const_no[1] << 16) | dsp_param.const_no[0]);

        err_c = dsp_mac_16_c1_c2(dsp_param.addr1, \
                                 dsp_param.const_no[0], dsp_param.const_no[1], \
                                 dsp_param.out_base, \
                                 dsp_param.d1_shift, dsp_param.d2_shift, \
                                 dsp_param.out1_shift, \
                                 dsp_param.cal_time, timeout, dsp_out_8bit);
    }

    if (err_c || dsp_fw_chk_res) {
        set_dbg_val(DEBUG_DSP_ERR_CODE, err_c);
        return (DSP_VER_RES_FAIL | DSP_VER_ALU);
    }

    return DSP_VER_RES_PASS;
}

static U32 dsp_ver_conv(void)
{
    const U32 image_addr = DSP_TEST_CONV_IMG_START_ADDR;
    U32 *coef_addr;
    U32 image_pixel, image_width, image_height;
    U32 cal_time, image_len;
    U32 out_base;
    U32 timeout, err_c = 0;
    U8 out1_shift, out2_shift;


    dsp_out_8bit = (U32)rand() & 1;

    coef_addr = (U32 *)(DSP_REG_BASE_ADDR + DSP_COEF_40);

    do {
        image_width  = ((((U32)rand() % DSP_HALF_IMAGE_WIDTH_MAX) + 1) << 1);
        image_height = SET_DEC_ALIGN_2((U32)rand() % DSP_TEST_CONV_IMG_LEN_MAX);

        image_pixel = image_width * image_height;
        image_len = (image_pixel << 1);

    } while ((image_width < DSP_OP_CONV_FILTER_WD) || \
             (image_height < DSP_OP_CONV_FILTER_HT) || \
             (image_len > DSP_TEST_CONV_IMG_LEN_MAX));

    timeout = image_pixel << 2;
    cal_time = (image_pixel >> 1);
    out_base = SET_INC_ALIGN_4(image_addr + image_len);

    out1_shift = ((U32)rand() & DSP_OP_DATA_SHIFT_UNIT_MASK);
    out2_shift = ((U32)rand() & DSP_OP_DATA_SHIFT_UNIT_MASK);

    set_dbg_val(DEBUG_DSP_DATA1, coef_addr);
    set_dbg_val(DEBUG_DSP_DATA2, image_addr);
    set_dbg_val(DEBUG_DSP_FW_ADDR_OFF, DSP_TEST_CONV_FW_PAT_OFF);

    set_dbg_val(DEBUG_DSP_CAL_LEN, image_len);
    set_dbg_val(DEBUG_DSP_OUTPUT, out_base);

#if 1
    gen_rand_pattern((void *)coef_addr, DSP_OP_CONV_COEF_LEN_MAX);
#else
    for (U32 idx = 0; idx < DSP_OP_CONV_COEF_IDX_MAX; ++idx) {
        (*coef_addr++) = 0x01 + idx;
    }
#endif
#if 1
    gen_rand_pattern((void *)image_addr, image_len);
#else
    for (U32 idx = 0; idx < image_len; idx += 2) {
        *(U16 *)(image_addr + idx) = 0x01 + (idx >> 1);
    }
#endif

    err_c = dsp_conv_exe(image_addr, image_width, out_base, \
                         out1_shift, out2_shift, cal_time, \
                         timeout, dsp_out_8bit);

    if (err_c) {
        set_dbg_val(DEBUG_DSP_ERR_CODE, err_c);
        return (DSP_VER_RES_FAIL | DSP_VER_CONV);
    }

    return DSP_VER_RES_PASS;
}

static U32 dsp_ver_lut(void)
{
    const U32 table_addr = DSP_TEST_LUT_TABLE_START_ADDR;
    const U32 idx_addr = DSP_TEST_LUT_IDX_START_ADDR;
    U32 out_base, cal_time, cal_len;
    U32 timeout, err_c = 0;


    set_dbg_val(DEBUG_DSP_DATA1, table_addr);
    set_dbg_val(DEBUG_DSP_DATA2, idx_addr);
    set_dbg_val(DEBUG_DSP_FW_ADDR_OFF, DSP_TEST_LUT_FW_PAT_OFF);

    dsp_out_8bit = ((U32)rand() & 1);

    do {
        cal_time = ((U32)rand() % DSP_TEST_LUT_CAL_TIME_MAX);

        if (dsp_out_8bit)
            cal_time = SET_DEC_ALIGN_2(cal_time);

    } while (cal_time == 0);

    cal_len = (cal_time << 2);
    timeout = cal_len + DSP_LOAD_LUT_LEN;

    out_base = idx_addr + cal_len;

    set_dbg_val(DEBUG_DSP_CAL_LEN, cal_len);
    set_dbg_val(DEBUG_DSP_OUTPUT, out_base);

#if 1
    gen_rand_pattern((void *)table_addr, DSP_LOAD_LUT_LEN);
    gen_rand_pattern((void *)idx_addr, cal_len);
#else
    memset((void *)table_addr, 0x01, DSP_LOAD_LUT_LEN);
    memset((void *)idx_addr, 0x01, cal_len);
#endif

    if ((U32)rand() & 1) {
        err_c = dsp_lut_load_n_exe(table_addr, idx_addr, out_base, \
                                   cal_time, timeout, dsp_out_8bit);

    } else {
        err_c = dsp_lut_load(table_addr, DSP_OP_LOAD_LUT_CAL_TIME, timeout);

        if (!err_c) {
            err_c = dsp_lut_exe(idx_addr, out_base, cal_time, timeout, dsp_out_8bit);
        }
    }

    if (err_c || dsp_fw_chk_res) {
        set_dbg_val(DEBUG_DSP_ERR_CODE, err_c);
        return (DSP_VER_RES_FAIL | DSP_VER_LUT);
    }

    return DSP_VER_RES_PASS;
}

static U32 dsp_ver_matrix(void)
{
    const U32 matrix_in_addr = DSP_TEST_MATRIX_IN_START_ADDR;
    U32 *coef_addr;
    U32 cal_time, cal_unit;
    U32 matrix_in_len;
    U32 out_base;
    U32 timeout, err_c = 0;
    U8 out1_shift, out2_shift;
    U8 matrix_in, matrix_out;


    set_dbg_val(DEBUG_DSP_DATA1, (DSP_REG_BASE_ADDR + DSP_COEF_40));
    set_dbg_val(DEBUG_DSP_DATA2, matrix_in_addr);
    set_dbg_val(DEBUG_DSP_FW_ADDR_OFF, DSP_TEST_MATRIX_FW_PAT_OFF);

    dsp_out_8bit = (U32)rand() & 1;

    coef_addr = (U32 *)(DSP_REG_BASE_ADDR + DSP_COEF_40);

    do {
        matrix_in  = ((U32)rand() % DSP_OP_MATRIX_IO_MAX) + 1; // M
        matrix_out = ((U32)rand() % DSP_OP_MATRIX_IO_MAX) + 1; // N

    } while (matrix_out > matrix_in);

    if (TEST_ALIGN_2(matrix_in) || TEST_ALIGN_2(matrix_out)) { // not aligned
        if (dsp_out_8bit)
            cal_unit = (matrix_in << 1);
        else
            cal_unit = matrix_in;

    } else {
        if (dsp_out_8bit)
            cal_unit = matrix_in;
        else
            cal_unit = (matrix_in >> 1);
    }

#if 1
    do {
        cal_time = cal_unit * (((U32)rand() & BIT32_MASK(0, 14)) + 1);
    } while (cal_time > DSP_TEST_MATRIX_CAL_TIME_MAX);
#else
    cal_time = cal_unit;
#endif

    matrix_in_len = (cal_time << 2);

    timeout = matrix_in_len << 1;
    out_base = SET_INC_ALIGN_4(matrix_in_addr + matrix_in_len);

    out1_shift = ((U32)rand() & DSP_OP_DATA_SHIFT_UNIT_MASK);
    out2_shift = ((U32)rand() & DSP_OP_DATA_SHIFT_UNIT_MASK);

    set_dbg_val(DEBUG_DSP_CAL_LEN, matrix_in_len);
    set_dbg_val(DEBUG_DSP_OUTPUT, out_base);
    set_dbg_val(DEBUG_DSP_MATRIX_IO, \
                (matrix_in << DSP_OP_MATRIX_I_OFF) | (matrix_out << DSP_OP_MATRIX_O_OFF));

#if 1
    gen_rand_pattern((void *)coef_addr, DSP_OP_CONV_COEF_LEN_MAX);
#else
    for (U32 m_idx = 0; m_idx < DSP_OP_MATRIX_IO_MAX; ++m_idx) {
        for (U32 n_idx = 0; n_idx < DSP_OP_MATRIX_IO_MAX; ++n_idx) {
            (*coef_addr++) = ((DSP_OP_MATRIX_IO_MAX - 1) - m_idx) * 10 + n_idx;
        }
    }
#endif
#if 1
    gen_rand_pattern((void *)matrix_in_addr, matrix_in_len);
#else
    for (U32 idx = 0; idx < matrix_in_len; idx += 2)
        (*(U16 *)(matrix_in_addr + idx)) = 0x01 + (idx >> 1);
#endif

    err_c = dsp_matrix_exe(matrix_in_addr, out_base, matrix_in, matrix_out, \
                           out1_shift, out2_shift, cal_time, timeout, dsp_out_8bit);

    if (err_c) {
        set_dbg_val(DEBUG_DSP_ERR_CODE, err_c);
        return (DSP_VER_RES_FAIL | DSP_VER_MATRIX);
    }

    return DSP_VER_RES_PASS;
}

static U32 dsp_ver_hd(void)
{
    const U32 out_base = DSP_TEST_HD_HW_OUT_BASE;
    const U32 addr1 = DSP_TEST_HD_PAT_START_ADDR;
    U32 addr2;
    U32 hd_cnt, cal_time, cal_len;
    U32 timeout, err_c = 0;

    dsp_out_8bit = 0;
    hd_cnt = ((U32)rand() % DSP_HD_CNT_MAX) + 1;

    do {
        cal_time = hd_cnt * (((U32)rand() % DSP_TEST_HD_CAL_TIME_MAX) + 1);
    } while (cal_time > DSP_TEST_HD_CAL_TIME_MAX);

    cal_len = (cal_time << 2);
    timeout = cal_len << 1;

    addr2 = addr1 + cal_len;

    set_dbg_val(DEBUG_DSP_DATA1, addr1);
    set_dbg_val(DEBUG_DSP_DATA2, addr2);
    set_dbg_val(DEBUG_DSP_FW_ADDR_OFF, DSP_TEST_HD_FW_PAT_OFF);

    set_dbg_val(DEBUG_DSP_CAL_LEN, cal_len);
    set_dbg_val(DEBUG_DSP_OUTPUT, out_base);

#if 1
    gen_rand_pattern((void *)addr1, cal_len);
#else
    for (U32 idx = 0; idx < cal_len; idx += 2)
        (*(U16 *)(addr1 + idx)) = 0x01 + (idx >> 1);
#endif
#if 1
    gen_rand_pattern((void *)addr2, cal_len);
#else
    for (U32 idx = 0; idx < cal_len; idx += 2)
        (*(U16 *)(addr2 + idx)) = 0x01 + (idx >> 1);
#endif

    err_c = dsp_hd_exe(addr1, addr2, out_base, hd_cnt, cal_time, timeout);

    if (err_c) {
        set_dbg_val(DEBUG_DSP_ERR_CODE, err_c);
        return (DSP_VER_RES_FAIL | DSP_VER_HD);
    }

    return DSP_VER_RES_PASS;
}

static U32 dsp_ver_dmul(void)
{
    DSP_PARAM_SETS dsp_param = {
        .addr1 = DSP_TEST_ALU_HW_PAT_START_ADDR,
    };
    U32 cal_len, timeout;
    U32 err_c = 0;


    set_dbg_val(DEBUG_DSP_FW_ADDR_OFF, DSP_TEST_ALU_FW_PAT_OFF);

    dsp_out_8bit = (U32)rand() & 1;

    do {
        dsp_param.cal_time = ((U32)rand() % DSP_TEST_ALU_CAL_TIME_MAX);

        /* 8 bit calculation */
        if (dsp_out_8bit)
            dsp_param.cal_time = SET_DEC_ALIGN_2(dsp_param.cal_time);

    } while (dsp_param.cal_time == 0);

    dsp_param.d1_shift = ((U32)rand() & DSP_OP_DATA_SHIFT_UNIT_MASK);
    dsp_param.d2_shift = ((U32)rand() & DSP_OP_DATA_SHIFT_UNIT_MASK);
    dsp_param.out1_shift = ((U32)rand() & DSP_OP_DATA_SHIFT_UNIT_MASK);

    cal_len = (dsp_param.cal_time << 2);
    timeout = (dsp_param.cal_time << 2);

    dsp_param.addr2 = (dsp_param.addr1 + cal_len);
    dsp_param.out_base = (dsp_param.addr2 + cal_len);

    set_dbg_val(DEBUG_DSP_DATA1, dsp_param.addr1);
    set_dbg_val(DEBUG_DSP_DATA2, dsp_param.addr2);
    set_dbg_val(DEBUG_DSP_CAL_LEN, cal_len);
    set_dbg_val(DEBUG_DSP_OUTPUT, dsp_param.out_base);

    gen_rand_pattern((void *)dsp_param.addr1, cal_len);
    gen_rand_pattern((void *)dsp_param.addr2, cal_len);

    err_c = dsp_dmul_8_8(dsp_param.addr1, dsp_param.addr2, \
                         dsp_param.out_base, \
                         dsp_param.d1_shift, dsp_param.d2_shift, \
                         dsp_param.out1_shift, \
                         dsp_param.cal_time, timeout, dsp_out_8bit);

    if (err_c || dsp_fw_chk_res) {
        set_dbg_val(DEBUG_DSP_ERR_CODE, err_c);
        return (DSP_VER_RES_FAIL | DSP_VER_DMUL);
    }

    return DSP_VER_RES_PASS;
}

static U32 dsp_ver_1d_conv(void)
{
    const U32 image_addr = DSP_TEST_CONV_IMG_START_ADDR;
    U32 *coef_addr;
    U32 image_pixel, image_width, image_height;
    U32 cal_time, image_len;
    U32 out_base;
    U32 timeout, err_c = 0;
    U8 out1_shift, out2_shift;
    U8 filter_num;
    bool conv_xy;


    dsp_out_8bit = 0;
    conv_xy = (U32)rand() & 1;

    coef_addr = (U32 *)(DSP_REG_BASE_ADDR + DSP_1D_COEF_00);

    do {
        image_width  = SET_INC_ALIGN_2(((U32)rand() % DSP_1D_CONV_IMG_WD_MAX) + 1);
        image_height = SET_INC_ALIGN_2(((U32)rand() % DSP_1D_CONV_IMG_HT_MAX) + 1);

        image_pixel = image_width * image_height;
        image_len = (image_pixel << 1);

#if 1
    /* Bypass HW BUG:
     * 1D convolution Y, DSP would output the incorrect data if filter size is 5.
     */
    } while (((conv_xy == DSP_OP_SET_1D_CONV_X) && \
              (image_width < DSP_OP_SET_1D_CONV_FILTER_MIN)) || \
             ((conv_xy == DSP_OP_SET_1D_CONV_Y) && \
              (image_height < 7)) || \
             (image_len > DSP_TEST_CONV_IMG_LEN_MAX));
#else
    } while (((conv_xy == DSP_OP_SET_1D_CONV_X) && \
              (image_width < DSP_OP_SET_1D_CONV_FILTER_MIN)) || \
             ((conv_xy == DSP_OP_SET_1D_CONV_Y) && \
              (image_height < DSP_OP_SET_1D_CONV_FILTER_MIN)) || \
             (image_len > DSP_TEST_CONV_IMG_LEN_MAX));
#endif

    /* filter size: 5, 7, 9, 11 */
    do {
head:
        filter_num = DSP_OP_SET_1D_CONV_FILTER_MIN + \
                     (((U32)rand() % DSP_OP_SET_1D_CONV_FILTER_IDX_NUM) << 1);

        if ((conv_xy == DSP_OP_SET_1D_CONV_Y) && filter_num == 5)
            goto head;

    } while (((conv_xy == DSP_OP_SET_1D_CONV_X) && \
              (image_width < filter_num)) || \
             ((conv_xy == DSP_OP_SET_1D_CONV_Y) && \
              (image_height < filter_num)));

    RW_DSP_REG(DSP_DUMMY_01) = filter_num;

    timeout = image_pixel << 2;
    cal_time = (image_pixel >> 1);
    out_base = SET_INC_ALIGN_4(image_addr + image_len);

    out1_shift = ((U32)rand() & DSP_OP_DATA_SHIFT_UNIT_MASK);
    out2_shift = ((U32)rand() & DSP_OP_DATA_SHIFT_UNIT_MASK);

    set_dbg_val(DEBUG_DSP_DATA1, coef_addr);
    set_dbg_val(DEBUG_DSP_DATA2, image_addr);
    set_dbg_val(DEBUG_DSP_FW_ADDR_OFF, DSP_TEST_CONV_FW_PAT_OFF);

    set_dbg_val(DEBUG_DSP_CAL_LEN, image_len);
    set_dbg_val(DEBUG_DSP_OUTPUT, out_base);

#if 1
    gen_rand_pattern((void *)coef_addr, filter_num << 2);
#else
    for (U32 idx = 0; idx < DSP_OP_CONV_COEF_IDX_MAX; ++idx) {
        (*coef_addr++) = 0x01 + idx;
    }
#endif
#if 1
    gen_rand_pattern((void *)image_addr, image_len);
#else
    for (U32 idx = 0; idx < image_len; idx += 2) {
        *(U16 *)(image_addr + idx) = 0x01 + (idx >> 1);
    }
#endif

    err_c = dsp_1d_conv_exe(image_addr, image_width, image_height, out_base, \
                            out1_shift, out2_shift, cal_time, \
                            timeout, dsp_out_8bit, conv_xy, filter_num);

    if (err_c) {
        set_dbg_val(DEBUG_DSP_ERR_CODE, err_c);
        return (DSP_VER_RES_FAIL | DSP_VER_1D_CONV);
    }

    return DSP_VER_RES_PASS;
}

static void dsp_test_fw_cal_add(U32 op_code)
{
    U32 cal_time = (fw_param.cal_time << 1);
    S32 ans;
    S16 *d1 = (S16 *)fw_param.addr1;
    S16 d2;

    void *out = (void *)fw_param.out_base;
    void *a2 = (void *)fw_param.addr2;


    do {
        if (op_code == DSP_OP_ADD_16_16) {
            d2 = *(S16 *)a2;
            a2 = ((U16 *)a2 + 1);
        } else if (op_code == DSP_OP_ADD_16_C1) {
            d2 = fw_param.const_no[0];
        } else if (op_code == DSP_OP_ADD_16_C2) {
            d2 = fw_param.const_no[1];
        } else { /* DSP_OP_ADD_16_8 */
            d2 = (*(S8 *)a2 << 8);
            a2 = ((U8 *)a2 + 1);
        }

        ans = (*d1++ + d2);

        /* check overflow */
        if (ans < SHRT_MIN)
            ans = SHRT_MIN;
        else if (ans > SHRT_MAX)
            ans = SHRT_MAX;

        if (dsp_out_8bit) {
            *(S8 *)out = (S8)(ans >> 8);
            out = (S8 *)out + 1;
        } else {
            /* output 16bit */
            *(S16 *)out = (S16)(ans);
            out = (S16 *)out + 1;
        }

    } while (--cal_time);
}

static void dsp_test_fw_cal_sub(U32 op_code)
{
    U32 cal_time = (fw_param.cal_time << 1);
    S32 ans;
    S16 *d1 = (S16 *)fw_param.addr1;
    S16 d2;

    void *out = (void *)fw_param.out_base;
    void *a2 = (void *)fw_param.addr2;


    do {
        if (op_code == DSP_OP_SUB_16_16) {
            d2 = *(S16 *)a2;
            a2 = ((U16 *)a2 + 1);
        } else if (op_code == DSP_OP_SUB_16_C1) {
            d2 = fw_param.const_no[0];
        } else if (op_code == DSP_OP_SUB_16_C2) {
            d2 = fw_param.const_no[1];
        } else { /* DSP_OP_SUB_16_8 */
            d2 = (*(S8 *)a2 << 8);
            a2 = ((U8 *)a2 + 1);
        }

        ans = (*d1++ - d2);

        /* check overflow */
        if (ans < SHRT_MIN)
            ans = SHRT_MIN;
        else if (ans > SHRT_MAX)
            ans = SHRT_MAX;

        if (dsp_out_8bit) {
            *(S8 *)out = (S8)(ans >> 8);
            out = (S8 *)out + 1;
        } else {
            /* output 16bit */
            *(S16 *)out = (S16)(ans);
            out = (S16 *)out + 1;
        }

    } while (--cal_time);
}

static void dsp_test_fw_cal_mul(U32 op_code)
{
    const U8 d1_shift = fw_param.d1_shift;
    const U8 d2_shift = fw_param.d2_shift;
    const U8 out1_shift = fw_param.out1_shift;

    U32 cal_time = (fw_param.cal_time << 1);
    S32 ans;
    S16 *d1 = (S16 *)fw_param.addr1;
    S16 d2;

    void *out = (void *)fw_param.out_base;
    void *a2 = (void *)fw_param.addr2;


    do {
        if (op_code == DSP_OP_MUL_16_16) {
            d2 = *(S16 *)a2;
            a2 = ((U16 *)a2 + 1);
        } else if (op_code == DSP_OP_MUL_16_C1) {
            d2 = fw_param.const_no[0];
        } else if (op_code == DSP_OP_MUL_16_C2) {
            d2 = fw_param.const_no[1];
        } else { /* DSP_OP_MUL_16_8 */
            d2 = (*(S8 *)a2 << 8);
            a2 = ((U8 *)a2 + 1);
        }

        ans = (((*d1++) >> d1_shift) * (d2 >> d2_shift)) >> out1_shift;

        /* check overflow */
        if (ans < SHRT_MIN)
            ans = SHRT_MIN;
        else if (ans > SHRT_MAX)
            ans = SHRT_MAX;

        if (dsp_out_8bit) {
            *(S8 *)out = (S8)(ans >> 8);
            out = (S8 *)out + 1;
        } else {
            /* output 16bit */
            *(S16 *)out = (S16)(ans);
            out = (S16 *)out + 1;
        }

    } while (--cal_time);
}

static void dsp_test_fw_cal_mac(void)
{
    const S16 c1 = fw_param.const_no[0];
    const S16 c2 = fw_param.const_no[1];
    const U8 d1_shift = fw_param.d1_shift;
    const U8 d2_shift = fw_param.d2_shift;
    const U8 out1_shift = fw_param.out1_shift;

    U32 cal_time = (fw_param.cal_time << 1);
    S32 ans;
    S16 *d1 = (S16 *)fw_param.addr1;

    void *out = (void *)fw_param.out_base;


    do {
        ans = (*d1++ - c1);

        /* check overflow */
        if (ans < SHRT_MIN)
            ans = SHRT_MIN;
        else if (ans > SHRT_MAX)
            ans = SHRT_MAX;

        ans = (((ans >> d1_shift) * (c2 >> d2_shift)) >> out1_shift);

        /* check overflow */
        if (ans < SHRT_MIN)
            ans = SHRT_MIN;
        else if (ans > SHRT_MAX)
            ans = SHRT_MAX;

        if (dsp_out_8bit) {
            *(S8 *)out = (S8)(ans >> 8);
            out = (S8 *)out + 1;
        } else {
            /* output 16bit */
            *(S16 *)out = (S16)(ans);
            out = (S16 *)out + 1;
        }

    } while (--cal_time);
}

static void dsp_test_fw_cal_conv(void)
{
    const U32 pixel = (fw_param.cal_time << 1);
    const U32 width = (RW_DSP_REG(DSP_HALF_IMAGE_WIDTH) << 1);
    const U32 height = (pixel / width);
    const S16 *image_addr = (S16 *)fw_param.addr1;
    const U8 out1_shift = fw_param.out1_shift;
    const U8 out2_shift = fw_param.out2_shift;

    U32 image_idx;
    U32 x, y;
    U32 filter_x, filter_y;
    S32 ans, sum;
    S16 *coef_reg;

    void *out = (S16 *)fw_param.out_base;


    for (y = 0; y < (height - (DSP_OP_CONV_FILTER_HT - 1)); ++y) {
        for (x = 0; x < (width - (DSP_OP_CONV_FILTER_WD - 1)); ++x) {

            sum = 0;
            /* convolution, read coefficient start from 80 */
            coef_reg = (S16 *)(DSP_REG_BASE_ADDR + DSP_COEF_40);

            for (filter_y = y; filter_y < (y + DSP_OP_CONV_FILTER_HT); ++filter_y) {
                for (filter_x = x; filter_x < (x + DSP_OP_CONV_FILTER_WD); ++filter_x) {

                    image_idx = (filter_y * width) + filter_x;

                    ans = (((*coef_reg) * (*(image_addr + image_idx))) >> out1_shift);

                    /* check overflow */
                    if (ans < SHRT_MIN)
                        ans = SHRT_MIN;
                    else if (ans > SHRT_MAX)
                        ans = SHRT_MAX;

                    sum += ans;
                    coef_reg += 2;
                }
            }

            sum >>= out2_shift;

            /* check overflow */
            if (sum < SHRT_MIN)
                sum = SHRT_MIN;
            else if (sum > SHRT_MAX)
                sum = SHRT_MAX;

            if (dsp_out_8bit) {
                *(S8 *)out = (S8)(sum >> 8);
                out = (S8 *)out + 1;
            } else {
                /* output 16bit */
                *(S16 *)out = (S16)(sum);
                out = (S16 *)out + 1;
            }
        }
    }
}

static void dsp_test_fw_cal_lut(void)
{
    U32 cal_time = (fw_param.cal_time << 1);
    S32 ans;
    S16 *table_addr = (S16 *)DSP_TEST_LUT_TABLE_START_ADDR;
    U16 *idx_addr = (U16 *)fw_param.addr1;
    U16 idx, off;
    S16 d1, d2;
    U8 rounding;

    void *out = (S16 *)fw_param.out_base;


    do {
        off = READ_DSP_LUT_OFF(*idx_addr);
        idx = READ_DSP_LUT_IDX(*idx_addr++);

        if (idx == (DSP_LOAD_LUT_IDX_MAX - 1)) {
            ans = *(S16 *)(table_addr + idx);
        } else {
            d1 = *(S16 *)(table_addr + idx);
            d2 = *(S16 *)(table_addr + idx + 1);

            /* interpolation */
            ans = ((d2 - d1) * off);

            if ((ans & DSP_LUT_ROUND_MASK) >= 128)
                rounding = 1;
            else
                rounding = 0;

            ans = d1 + (ans >> 8) + rounding; // divided by 256

            /* check overflow */
            if (ans < SHRT_MIN)
                ans = SHRT_MIN;
            else if (ans > SHRT_MAX)
                ans = SHRT_MAX;
        }

        if (dsp_out_8bit) {
            *(S8 *)out = (S8)(ans >> 8);
            out = (S8 *)out + 1;
        } else {
            /* output 16bit */
            *(S16 *)out = (S16)(ans);
            out = (S16 *)out + 1;
        }

    } while (--cal_time);
}

static void dsp_test_fw_cal_matrix(void)
{
    const U8 out1_shift = fw_param.out1_shift;
    const U8 out2_shift = fw_param.out2_shift;
    const U8 n = fw_param.matrix_i;
    const U8 m = fw_param.matrix_o;

    U32 cal_idx;
    U32 cal_time = (fw_param.cal_time << 1);
    S32 ans, sum;
    S16 *matrix_in_addr = (S16 *)fw_param.addr1;
    S16 *coef_reg;
    U8 n_idx, m_idx;

    void *out = (S16 *)fw_param.out_base;

    cal_idx = 0;
    do {

        for (m_idx = 0; m_idx < m; ++m_idx) {

            /* matrix, read coefficient start from 00 */
            coef_reg = (S16 *)(DSP_REG_BASE_ADDR + DSP_COEF_40 + \
                               (m_idx * DSP_OP_MATRIX_IO_MAX * 4));

            sum = 0;
            for (n_idx = 0; n_idx < n; ++n_idx) {

                ans = (((*coef_reg) * (*(matrix_in_addr + n_idx))) >> out1_shift);

                /* check overflow */
                if (ans < SHRT_MIN)
                    ans = SHRT_MIN;
                else if (ans > SHRT_MAX)
                    ans = SHRT_MAX;

                sum += ans;
                coef_reg += 2;
            }

            sum >>= out2_shift;

            /* check overflow */
            if (sum < SHRT_MIN)
                sum = SHRT_MIN;
            else if (sum > SHRT_MAX)
                sum = SHRT_MAX;

            if (dsp_out_8bit) {
                *(S8 *)out = (S8)(sum >> 8);
                out = (S8 *)out + 1;
            } else {
                /* output 16bit */
                *(S16 *)out = (S16)(sum);
                out = (S16 *)out + 1;
            }

        }

        matrix_in_addr += n;
        cal_idx += n;

    } while (cal_idx < cal_time);
}

static void dsp_test_fw_cal_hd(void)
{
    U32 hd_cnt;
    U32 cal_time = fw_param.cal_time;
    U32 sum, xor_res, bits_cnt;
    U32 *d1 = (U32 *)fw_param.addr1;
    U32 *d2 = (U32 *)fw_param.addr2;
    U32 *out = (U32 *)fw_param.out_base;

    gen_rand_pattern((void *)hw_param.out_base, 0x10000);

    do {
        hd_cnt = RW_DSP_REG(DSP_HD_CNT);
        sum = 0;

        do {
            bits_cnt = 0;
            xor_res =  *d1++ ^ *d2++;

            while (xor_res) {
                xor_res &= (xor_res - 1);
                bits_cnt += 1;
            }

            sum += bits_cnt;

            cal_time -= 1;
        } while (--hd_cnt);

        *out++ = sum;
    } while (cal_time);
}

static void dsp_test_fw_cal_dmul(void)
{
    const U8 d1_shift = fw_param.d1_shift;
    const U8 d2_shift = fw_param.d2_shift;
    const U8 out1_shift = fw_param.out1_shift;

    U32 cal_time;
    S32 ans;
    S8 *d1 = (S8 *)fw_param.addr1;
    S8 *d2 = (S8 *)fw_param.addr2;
    S8 *out = (S8 *)fw_param.out_base;


    if (dsp_out_8bit) {
        cal_time = (fw_param.cal_time << 1);
        d1 += 1;
        d2 += 1;
    } else {
        cal_time = (fw_param.cal_time << 2);
    }

    do {
        ans = (((*d1++) >> d1_shift) * ((*d2++) >> d2_shift)) >> out1_shift;

        if (dsp_out_8bit) {
            d1 += 1;
            d2 += 1;
        }

        /* check overflow */
        if (ans < SCHAR_MIN)
            ans = SCHAR_MIN;
        else if (ans > SCHAR_MAX)
            ans = SCHAR_MAX;

        *out++ = ans;

    } while (--cal_time);
}

static void dsp_test_fw_cal_1d_conv(void)
{
    U32 width = RW_DSP_REG(DSP_1D_CONV_IMG_WD);
    U32 height = RW_DSP_REG(DSP_1D_CONV_IMG_HT);
    const S16 *image_addr = (S16 *)fw_param.addr1;
    const U8 out1_shift = fw_param.out1_shift;
    const U8 out2_shift = fw_param.out2_shift;

    U32 image_idx;
    U32 x, y;
    U32 filter_idx;
    U32 filter_x, filter_y;
    S32 ans, sum;
    S16 *coef_reg;

    void *out = (S16 *)fw_param.out_base;


    if (fw_param.conv_xy == DSP_OP_SET_1D_CONV_X) {
        width -= (fw_param.conv_filter_num - 1);
    } else {
        height -= (fw_param.conv_filter_num - 1);
    }

    for (y = 0; y < height; ++y) {
        for (x = 0; x < width; ++x) {

            sum = 0;
            coef_reg = (S16 *)(DSP_REG_BASE_ADDR + DSP_1D_COEF_00);

            for (filter_idx = 0, filter_y = y, filter_x = x; \
                 filter_idx < fw_param.conv_filter_num; ++filter_idx) {

                image_idx = (filter_y * RW_DSP_REG(DSP_1D_CONV_IMG_WD)) + filter_x;

                ans = (((*coef_reg) * (*(image_addr + image_idx))) >> out1_shift);

                /* check overflow */
                if (ans < SHRT_MIN)
                    ans = SHRT_MIN;
                else if (ans > SHRT_MAX)
                    ans = SHRT_MAX;

                sum += ans;
                coef_reg += 2;

                if (fw_param.conv_xy == DSP_OP_SET_1D_CONV_X) {
                    filter_x += 1;
                } else {
                    filter_y += 1;
                }
            }

            sum >>= out2_shift;

            /* check overflow */
            if (sum < SHRT_MIN)
                sum = SHRT_MIN;
            else if (sum > SHRT_MAX)
                sum = SHRT_MAX;

            *(S16 *)out = (S16)(sum);
            out = (S16 *)out + 1;
        }
    }
}

static U32 dsp_ver_fw_exe(void)
{
    U32 *inst_addr = dsp_inst;
    U32 inst_data;
    U32 op_code;
    U32 fw_check = 0;


    dsp_fw_chk_res = 0;

    memset((void *)&hw_param, 0, sizeof(hw_param));
    memset((void *)&fw_param, 0, sizeof(fw_param));

    do {
        /* DSP instruction parser */
        inst_data = (*inst_addr & DSP_OP_DATA_MASK);
        op_code = (*inst_addr >> DSP_OP_CODE_MAP_OFF);

        if (op_code < DSP_OP_ADD_16_16) {

            /* parameter */
            if (op_code == DSP_OP_SET_ADDR1_BASE) {
                hw_param.addr1 = inst_data;
                fw_param.addr1 = inst_data;

            } else if (op_code == DSP_OP_SET_ADDR2_BASE) {
                hw_param.addr2 = inst_data;
                fw_param.addr2 = inst_data;

            } else if (op_code == DSP_OP_SET_OUTPUT_BASE) {
                hw_param.out_base = inst_data;
                fw_param.out_base = inst_data;

            } else if (op_code == DSP_OP_SET_CONST1) {
                hw_param.const_no[0] = inst_data;
                fw_param.const_no[0] = inst_data;

            } else if (op_code == DSP_OP_SET_CONST2) {
                hw_param.const_no[1] = inst_data;
                fw_param.const_no[1] = inst_data;

            } else if (op_code == DSP_OP_SET_DATA_SHIFT) {
                fw_param.d1_shift = ((inst_data >> DSP_OP_D1_SHIFT_OFF) & \
                                     DSP_OP_DATA_SHIFT_UNIT_MASK);
                fw_param.d2_shift = ((inst_data >> DSP_OP_D2_SHIFT_OFF) & \
                                     DSP_OP_DATA_SHIFT_UNIT_MASK);
                fw_param.out1_shift = ((inst_data >> DSP_OP_OUT1_SHIFT_OFF) & \
                                       DSP_OP_DATA_SHIFT_UNIT_MASK);
                fw_param.out2_shift = ((inst_data >> DSP_OP_OUT2_SHIFT_OFF) & \
                                       DSP_OP_DATA_SHIFT_UNIT_MASK);

                set_dbg_val(DEBUG_DSP_SHIFT, inst_data);

            } else if (op_code == DSP_OP_SET_8BIT_OUTPUT) {

            } else if (op_code == DSP_OP_SET_MATRIX_IN_OUT) {
                fw_param.matrix_i = ((inst_data >> DSP_OP_MATRIX_I_OFF) & \
                                     DSP_OP_MATRIX_IN_OUT_UNIT_MASK);
                fw_param.matrix_o = ((inst_data >> DSP_OP_MATRIX_O_OFF) & \
                                     DSP_OP_MATRIX_IN_OUT_UNIT_MASK);

            } else if (op_code == DSP_OP_SET_1D_CONV) {
                fw_param.conv_xy = inst_data & 1;
                fw_param.conv_filter_num = (inst_data >> DSP_OP_SET_1D_CONV_FILTER_OFF) & 0xf;
            }

        } else {

            /* calculate FW pattern */
            hw_param.cal_time = inst_data;
            fw_param.cal_time = inst_data;

            dsp_op = op_code;

            fw_check = 1;

            if (op_code <= DSP_OP_MAC_16_C1_C2) {

                fw_param.addr1 += DSP_TEST_ALU_FW_PAT_OFF;
                fw_param.addr2 += DSP_TEST_ALU_FW_PAT_OFF;
                fw_param.out_base += DSP_TEST_ALU_FW_PAT_OFF;

                if (op_code <= DSP_OP_ADD_16_C2) {
                    dsp_test_fw_cal_add(op_code);

                } else if (op_code <= DSP_OP_SUB_16_C2) {
                    dsp_test_fw_cal_sub(op_code);

                } else if (op_code <= DSP_OP_MUL_16_C2) {
                    dsp_test_fw_cal_mul(op_code);

                } else if (op_code == DSP_OP_MAC_16_C1_C2) {
                    dsp_test_fw_cal_mac();
                } else {
                    fw_check = 0;
                }

            } else if (op_code == DSP_OP_CONVOLUTION) {
                //fw_param.addr1 += DSP_TEST_CONV_FW_PAT_OFF;
                //fw_param.addr2 += DSP_TEST_CONV_FW_PAT_OFF;
                fw_param.out_base += DSP_TEST_CONV_FW_PAT_OFF;
                dsp_test_fw_cal_conv();

            } else if (op_code == DSP_OP_LUT) {
                //fw_param.addr1 += DSP_TEST_LUT_FW_PAT_OFF;
                //fw_param.addr2 += DSP_TEST_LUT_FW_PAT_OFF;
                fw_param.out_base += DSP_TEST_LUT_FW_PAT_OFF;
                dsp_test_fw_cal_lut();

            } else if (op_code == DSP_OP_MATRIX) {
                fw_param.out_base += DSP_TEST_MATRIX_FW_PAT_OFF;
                dsp_test_fw_cal_matrix();

            } else if (op_code == DSP_OP_HD) {
                fw_param.out_base += DSP_TEST_HD_FW_PAT_OFF;
                dsp_test_fw_cal_hd();

            } else if (op_code == DSP_OP_DMUL_8_8) {
                fw_param.out_base += DSP_TEST_ALU_FW_PAT_OFF;
                dsp_test_fw_cal_dmul();

            } else if (op_code == DSP_OP_1D_CONV) {
                fw_param.out_base += DSP_TEST_CONV_FW_PAT_OFF;
                dsp_test_fw_cal_1d_conv();

            } else {
                /* not supported yet */
                fw_check = 0;
            }

            if (fw_check) {
                set_dbg_val(DEBUG_DSP_INSTRU, \
                            (op_code | (dsp_out_8bit << DSP_VER_OUT_8BIT_OFF)));
            }
        }

        inst_addr += 1;
    } while (inst_addr <= dsp_end); /* include end address */

    return fw_check;
}

static void dsp_ver_fw_cmp(void)
{
    U32 idx, cal_num;
    void *hw_out = (void *)hw_param.out_base;
    void *fw_out = (void *)fw_param.out_base;


    if (dsp_op == DSP_OP_CONVOLUTION) {
        const U32 pixel = (fw_param.cal_time << 1);
        const U32 width = (RW_DSP_REG(DSP_HALF_IMAGE_WIDTH) << 1);
        const U32 height = pixel / width;

        cal_num = ((height - (DSP_OP_CONV_FILTER_HT - 1)) * \
                   (width - (DSP_OP_CONV_FILTER_WD - 1)));

    } else if (dsp_op == DSP_OP_MATRIX) {
        cal_num = ((fw_param.cal_time << 1) / fw_param.matrix_i) * fw_param.matrix_o;

    } else if (dsp_op == DSP_OP_HD) {
        cal_num = ((fw_param.cal_time / RW_DSP_REG(DSP_HD_CNT)) << 1);

    } else if (dsp_op == DSP_OP_1D_CONV) {
        const U32 width = RW_DSP_REG(DSP_1D_CONV_IMG_WD);
        const U32 height = RW_DSP_REG(DSP_1D_CONV_IMG_HT);

        if (fw_param.conv_xy == DSP_OP_SET_1D_CONV_X) {
            cal_num = (height * \
                       (width - (fw_param.conv_filter_num - 1)));

        } else {
            cal_num = ((height - (fw_param.conv_filter_num - 1)) * \
                       width);
        }

    } else {
        cal_num = (fw_param.cal_time << 1);
    }

    for (idx = 0; idx < cal_num; ++idx) {

        if (dsp_out_8bit) {
            if (*(U8 *)hw_out != *(U8 *)fw_out) {
                /* address offset of fail data, idx: U8 pointer */
                dsp_fw_chk_res = idx | DSP_VER_FW_CHK_FAIL;

                break;
            }
            hw_out = (S8 *)hw_out + 1;
            fw_out = (S8 *)fw_out + 1;

        } else {
            if (*(U16 *)hw_out != *(U16 *)fw_out) {
                /* address offset of fail data, idx: U16 pointer */
                dsp_fw_chk_res = (idx << 1) | DSP_VER_FW_CHK_FAIL;

                break;
            }
            hw_out = (S16 *)hw_out + 1;
            fw_out = (S16 *)fw_out + 1;
        }
    }

    /* 0: pass, 1: fail */
    set_dbg_val(DEBUG_DSP_FW_CHK, dsp_fw_chk_res);
}

static void gen_rand_pattern(void *addr, U32 len)
{
    U32 head_len = 0;
    U32 body_len = 0;
    U32 tail_len = 0;
    U32 idx;


    if (TEST_ALIGN_4((U32)addr))
        head_len = 4 - ((U32)addr & CHK_ALIGN_4);

    if (head_len > len) {
        head_len = len;
    } else {
        tail_len = (((U32)addr + len) & CHK_ALIGN_4);
        body_len = ((len - head_len - tail_len) >> 2);
    }

    for (idx = 0; idx < head_len; ++idx) {
        *(U8 *)addr = rand();
        addr = (U8 *)addr + 1;
    }

    for (idx = 0; idx < body_len; ++idx) {
        *(U32 *)addr = rand();
        addr = (U32 *)addr + 1;
    }

    for (idx = 0; idx < tail_len; ++idx) {
        *(U8 *)addr = rand();
        addr = (U8 *)addr + 1;
    }
}

static const U32 dsp_reg_def[DSP_REG_LEN >> 2] = {
    0x20190121, 0x50000003, 0x00000000, 0x00000000, // 0x0000
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0010
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0020
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0030
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0040
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0050
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0060
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0070
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0080
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0090
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00a0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00b0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00c0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00d0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00e0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00f0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0100
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0110
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0120
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0130
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0140
    0x00000000, 0x00000000, 0x00000080, 0x00000000, // 0x0150
    0x00000000, 0x00000000, 0x00000000, 0x00000003, // 0x0160
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0170
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0180
    0x00000004, 0x00000000, 0x00000000, 0x00000000, // 0x0190
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x01a0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x01b0
    0x00000000, 0x00000080, 0x00000080, 0x00000000, // 0x01c0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x01d0
};

static const U32 dsp_reg_def_vld_map[DSP_REG_LEN >> 2] = {
    0xffffffff, 0xffffffff, 0x00000000, 0xffffffff, // 0x0000
    0xffffffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x0010
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x0020
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x0030
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x0040
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x0050
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x0060
    0x0000ffff, 0x0000ffff, 0x00000000, 0x00000000, // 0x0070
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0080
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0090
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00a0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00b0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00c0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00d0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00e0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00f0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0100
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0110
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0120
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0130
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0140
    0x00000000, 0x00000000, 0x000000ff, 0x00000000, // 0x0150
    0x00000000, 0xffffffff, 0xffffffff, 0x00000003, // 0x0160
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0170
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0180
    0x0000007f, 0x00000000, 0x0000ffff, 0x0000ffff, // 0x0190
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x01a0
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x01b0
    0x0000ffff, 0x000001ff, 0x000001ff, 0x00000000, // 0x01c0
    0x00000000, 0x0000000f, 0x0000003f, 0x00000001, // 0x01d0
};

static const U32 dsp_reg_rw_vld_map[DSP_REG_LEN >> 2] = {
    0xffffffff, 0xffffffff, 0x00000000, 0xffffffff, // 0x0000
    0xffffffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x0010
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x0020
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x0030
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x0040
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x0050
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x0060
    0x0000ffff, 0x0000ffff, 0x00000000, 0x00000000, // 0x0070
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0080
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0090
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00a0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00b0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00c0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00d0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00e0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x00f0
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0100
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0110
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0120
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0130
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0140
    0x00000000, 0x00000000, 0x000000ff, 0x00000000, // 0x0150
    0x00000000, 0xffffffff, 0xffffffff, 0x00000000, // 0x0160
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0170
    0x00000000, 0x00000000, 0x00000000, 0x00000000, // 0x0180
    0x0000007f, 0x00000000, 0x0000ffff, 0x0000ffff, // 0x0190
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x01a0
    0x0000ffff, 0x0000ffff, 0x0000ffff, 0x0000ffff, // 0x01b0
    0x0000ffff, 0x000001ff, 0x000001ff, 0x00000000, // 0x01c0
    0x00000000, 0x0000000f, 0x0000003f, 0x00000001, // 0x01d0
};

#endif

