#include <SPI_NOR.h>
#include <DMA.h>
#include <ParameterSetting.h>

#define NOR_CMD_WREN    		0x06    // write enable
#define NOR_CMD_WRDI    		0x04    // write disable
#define NOR_CMD_RDID    		0x90    // read identification
#define NOR_CMD_RDSR1    		0x05    // read status 1 register
#define NOR_CMD_WRSR1    		0x01    // write status 1 register
#define NOR_CMD_RDSR2    		0x35    // read status 2 register
#define NOR_CMD_WRSR2    		0x31    // write status 2 register
#define NOR_CMD_READ    		0x03    // read data
#define NOR_CMD_FREAD   		0x0B    // fast read data
#define NOR_CMD_SE      		0x20    // sector erase
#define NOR_CMD_BE      		0xD8    // block erase
#define NOR_CMD_CE      		0xC7    // chip erase
#define NOR_CMD_PP      		0x02    // page program
#define NOR_CMD_QPP				0x32	// Quad input page program
#define NOR_CMD_QREAD			0x6B	// Quad read


#define WINBOND_MANUFACTURER_ID   	0xEF    // Manufacturer ID
#define W25Q32JW_DEVICE_ID   		0x15    // Device ID
#define W25Q16JV_DEVICE_ID   		0x14    // Device ID

#define NOR_CLK                	100000	//100k
#define MAX_WRITE_TIMES 		5
#define NOP()					({ __asm__ __volatile__ ("nop"); })
#define NOR_QUADMODE			1

U32 nor_ReadStatus1Reg(void);
U32 nor_ReadStatus2Reg(void);
void nor_SetStatus2Reg(U8 Reg);

TNorDev pNorDev;

//--------------------------------------------
void nor_delay(U32 cycle)
{
    volatile U32 idx;

    for (idx = 0; idx < cycle; ++idx)
        NOP();
}
//--------------------------------------------
void nor_SpiOpen(TSPIID spi_id)
{
	U32 status;

	pNorDev.noOfBlocks = 32;		// 2MB
	pNorDev.bytesPerPage = 256;  	// 256 byte per page
	pNorDev.pagesPerBlock = 256; 	// 64K per block
	pNorDev.bytesPerSect = 4096; 	// 4096 bytes per sector(4K)
	pNorDev.pagesPerSect = pNorDev.bytesPerSect/pNorDev.bytesPerPage;

	pNorDev.pSPIPara.tSPIID = spi_id;
	pNorDev.pSPIPara.Clk = NOR_CLK;

	pNorDev.pSPIPara.Phs = PHASE_2EDGE;		//sampling data at even edge
	pNorDev.pSPIPara.Pol = POLARITY_HIGH;		//high in the idle states
	pNorDev.pSPIPara.SlvMode = MASTER_MODE;	//master mode
	pNorDev.pSPIPara.Lsb = MSB_MODE;		//MSB first
	pNorDev.pSPIPara.DataMerge = DATAMERGE_ON;//enable data merge mode
	pNorDev.pSPIPara.Bit = 8;		// 8 bits
	pNorDev.pSPIPara.AddrLen = ADDRLEN_3BYTE;	// 3 bytes address

	spi_Open(&(pNorDev.pSPIPara));

	//Quad enable
	status = nor_ReadStatus2Reg();
	status |= (0x01<<1);
	nor_SetStatus2Reg(status&0xFF);
}

//---------------------------------------------------
void nor_CheckSPIFinish(void)
{
	U32 ret;
	ret = 0xFF;

	while(ret%2 == 1){
	  ret = RW_SPI_REG(SPI_STATUS, pNorDev.pSPIPara.tSPIID);
	}

	return;
}
//--------------------------------------------
void nor_WREnable(void)
{
	U32 statusReg = 0xFF, status = 0;

	nor_CheckSPIFinish();

	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
														| (0x07 << 24);	//none data transfer mode

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pNorDev.pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_INTR_EN, pNorDev.pSPIPara.tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT

	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_WREN;

	//	wait End of SPI transfer interrupt
	while(status & (1<<4) == 0)
	{
		status = RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID);
	}
	RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID) = 0x00000010;		//clear End of SPI transfer interrupt status

	// check write enable
	while((statusReg & 0x02) != 0x02)
	{
		statusReg = nor_ReadStatus1Reg();
	}
}
//--------------------------------------------
void nor_WRDisable(void)
{
	U32 statusReg = 0xFF, status = 0;

	nor_CheckSPIFinish();

	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
														| (0x07 << 24);	//none data transfer mode

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pNorDev.pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_INTR_EN, pNorDev.pSPIPara.tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT

	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_WRDI;

	//	wait End of SPI transfer interrupt
	while(status & (1<<4) == 0)
	{
		status = RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID);
	}
	RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID) = 0x00000010;		//clear End of SPI transfer interrupt status

}
//--------------------------------------------
U32 nor_ReadStatus1Reg(void)
{
	U32 status = 0;

	nor_CheckSPIFinish();

	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
														| (0x02 << 24);	//read only transfer mode

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pNorDev.pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_INTR_EN, pNorDev.pSPIPara.tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT
	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_RDSR1;

	status = RW_SPI_REG(SPI_DATA, pNorDev.pSPIPara.tSPIID);

	return status;
}
//--------------------------------------------
void nor_SetStatus1Reg(U8 Reg)
{
	U32 status = 0, statusReg = 0xFF;

	nor_WREnable();

	nor_CheckSPIFinish();

	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
														| (0x01 << 24);	//write only transfer mode

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pNorDev.pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_INTR_EN, pNorDev.pSPIPara.tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT

	RW_SPI_REG(SPI_DATA, pNorDev.pSPIPara.tSPIID) = Reg;
	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_WRSR1;

	//	wait End of SPI transfer interrupt
	while(status & (1<<4) == 0)
	{
		status = RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID);
	}
	RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID) = 0x00000010;		//clear End of SPI transfer interrupt status

	//check write complete
	while((statusReg & 0x01) != 0x00)
	{
		statusReg = nor_ReadStatus1Reg();
	}
}
//--------------------------------------------
U32 nor_ReadStatus2Reg(void)
{
	U32 status = 0;

	nor_CheckSPIFinish();

	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
														| (0x02 << 24);	//read only transfer mode

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pNorDev.pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_INTR_EN, pNorDev.pSPIPara.tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT
	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_RDSR2;

	status = RW_SPI_REG(SPI_DATA, pNorDev.pSPIPara.tSPIID);

	return status;
}
//--------------------------------------------
void nor_SetStatus2Reg(U8 Reg)
{
	U32 status = 0, statusReg = 0xFF;

	nor_WREnable();

	nor_CheckSPIFinish();

	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
														| (0x01 << 24);	//write only transfer mode

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pNorDev.pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_INTR_EN, pNorDev.pSPIPara.tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT

	RW_SPI_REG(SPI_DATA, pNorDev.pSPIPara.tSPIID) = Reg;
	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_WRSR2;

	//	wait End of SPI transfer interrupt
	while(status & (1<<4) == 0)
	{
		status = RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID);
	}
	RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID) = 0x00000010;		//clear End of SPI transfer interrupt status

	//check write complete
	while((statusReg & 0x01) != 0x00)
	{
		statusReg = nor_ReadStatus1Reg();
	}
}
//--------------------------------------------
TNorDev* nor_ReadID(void)
{
	U32 spi_flash_id, status = 0;

	nor_CheckSPIFinish();

	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
			                                            | (0x01 << 29)	//enable address phase
														| (0x02 << 24)	//read only transfer mode
														| ((2-1) << 0); //2 data receive

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pNorDev.pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_INTR_EN, pNorDev.pSPIPara.tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT
	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_RDID;

	//	wait End of SPI transfer interrupt
	while(status & (1<<4) == 0)
	{
		status = RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID);
	}
	RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID) = 0x00000010;		//clear End of SPI transfer interrupt status

	spi_flash_id = RW_SPI_REG(SPI_DATA, pNorDev.pSPIPara.tSPIID);
	pNorDev.MID = spi_flash_id & 0xFF;
	pNorDev.TypeID = spi_flash_id >> 8;

	return &pNorDev;
}

//--------------------------------------------
void nor_EraseSector(U32 sec)
{
	U32 status = 0, statusReg = 0xFF;
	U32 iSecAddr;

	iSecAddr = sec*pNorDev.bytesPerSect;

	nor_WREnable();

	nor_CheckSPIFinish();

	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
														| (0x01 << 29)	//enable address phase
														| (0x07 << 24);	//none data transfer mode

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pNorDev.pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_INTR_EN, pNorDev.pSPIPara.tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT

	RW_SPI_REG(SPI_ADDR, pNorDev.pSPIPara.tSPIID) = iSecAddr;
	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_SE;

	//	wait End of SPI transfer interrupt
	while(status & (1<<4) == 0)
	{
		status = RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID);
	}
	RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID) = 0x00000010;		//clear End of SPI transfer interrupt status

	//check write complete
	while((statusReg & 0x01) != 0x00)
	{
		statusReg = nor_ReadStatus1Reg();
	}

}
//--------------------------------------------
void nor_EraseBlock(U32 phyBlock)
{
	U32 status = 0, statusReg = 0xFF;
	U32 iBlkAddr;

	iBlkAddr = phyBlock*pNorDev.pagesPerBlock*pNorDev.bytesPerPage;

	nor_WREnable();

	nor_CheckSPIFinish();

	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
														| (0x01 << 29)	//enable address phase
														| (0x07 << 24);	//none data transfer mode

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pNorDev.pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_INTR_EN, pNorDev.pSPIPara.tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT

	RW_SPI_REG(SPI_ADDR, pNorDev.pSPIPara.tSPIID) = iBlkAddr;
	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_BE;

	//	wait End of SPI transfer interrupt
	while(status & (1<<4) == 0)
	{
		status = RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID);
	}
	RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID) = 0x00000010;		//clear End of SPI transfer interrupt status

	//check write complete
	while((statusReg & 0x01) != 0x00)
	{
		statusReg = nor_ReadStatus1Reg();
	}

}
//--------------------------------------------
void nor_ReadData(U32 addr, U32 buf, U32 count)
{

	U32 status = 0;
	U32 Reg;

	if((addr+count)> 0x1FFFFF)
		return;

	nor_CheckSPIFinish();

#if NOR_QUADMODE == 1
	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
														| (0x01 << 29)	//enable address phase
														| (0x09 << 24)	//dummy, read
														| (0x02 << 22)	//Quad I/O mode
														| (0x03 << 9)	//dummy data count
														| ((count-1) << 0); //data receive count
#else
	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
														| (0x01 << 29)	//enable address phase
														| (0x02 << 24)	//read only transfer mode
														| ((count-1) << 0); //data receive count
#endif

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pNorDev.pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_CTRL, pNorDev.pSPIPara.tSPIID) = (0x02 << 16)		//transmit FIFO threshold
												  | (0x02 << 8)			//receive FIFO threshold
												  |	(0x01 << 4)			//enable TX dma
												  | (0x01 << 3);		//enable RX dma
	RW_SPI_REG(SPI_INTR_EN, pNorDev.pSPIPara.tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT
	RW_SPI_REG(SPI_ADDR, pNorDev.pSPIPara.tSPIID) = addr;

#if NOR_QUADMODE == 1
	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_QREAD;
#else
	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_READ;
#endif

	Reg = (SPI_REG_BASE_ADDR + 0x2C + pNorDev.pSPIPara.tSPIID*0x00400000);
	edma_Init(DMA_CH4, Reg, buf);
	edma_SetSrcParams(DMA_CH4, (count/4), DMA_CH_CTRL_BURST_SIZE_1_TRANS, DMA_CH_CTRL_WIDTH_WORD_TRANS, DMA_CH_CTRL_HANDSHAKING_MODE, DMA_CH_CTRL_FIXED_ADDR, DMA_CH_CTRL_REQ_SEL_SPI2_RX);
	edma_SetDstParams(DMA_CH4, DMA_CH_CTRL_WIDTH_WORD_TRANS, DMA_CH_CTRL_NORMAL_MODE, DMA_CH_CTRL_INCREMENT_ADDR, DMA_CH_CTRL_REQ_SEL_NONE);
	edma_Start(DMA_CH4, DMA_CH_CTRL_PRIORITY_HIGH);

	//	wait End of SPI transfer interrupt
	while(status & (1<<4) == 0)
	{
		status = RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID);
	}
	RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID) = 0x00000010;		//clear End of SPI transfer interrupt status

}
//-------------------------------------------
void nor_ReadPage(U32 page, U32 buf)
{
	U32 iPgeAddr;

	iPgeAddr = page*pNorDev.bytesPerPage;

	nor_ReadData(iPgeAddr, buf, pNorDev.bytesPerPage);
}

//--------------------------------------------
void nor_WritePage(U32 page, U32 buf)
{
	U32 status = 0, statusReg = 0xFF;
	U32 iPgeAddr, Reg;

	iPgeAddr = page*pNorDev.bytesPerPage;

	nor_WREnable();

	nor_CheckSPIFinish();

#if NOR_QUADMODE == 1
	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
														| (0x01 << 29)	//enable address phase
														| (0x01 << 24)	//write only transfer mode
														| (0x02 << 22)	//Quad I/O mode
														| ((pNorDev.bytesPerPage-1) << 12); //data transfer count
#else
	RW_SPI_REG(SPI_TRANS_CTRL, pNorDev.pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
															| (0x01 << 29)	//enable address phase
															| (0x01 << 24)	//write only transfer mode
															| ((pNorDev.bytesPerPage-1) << 12); //data transfer count
#endif

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pNorDev.pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_CTRL, pNorDev.pSPIPara.tSPIID) = (0x02 << 16)		//transmit FIFO threshold
												  | (0x02 << 8)			//receive FIFO threshold
												  |	(0x01 << 4)			//enable TX dma
												  | (0x01 << 3);		//enable RX dma
	RW_SPI_REG(SPI_INTR_EN, pNorDev.pSPIPara.tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT
	RW_SPI_REG(SPI_ADDR, pNorDev.pSPIPara.tSPIID) = iPgeAddr;

#if NOR_QUADMODE == 1
	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_QPP;
#else
	RW_SPI_REG(SPI_CMD, pNorDev.pSPIPara.tSPIID) = NOR_CMD_PP;
#endif

	Reg = (SPI_REG_BASE_ADDR + 0x2C + pNorDev.pSPIPara.tSPIID*0x00400000);
	edma_Init(DMA_CH2, buf, Reg);
	edma_SetSrcParams(DMA_CH2, (pNorDev.bytesPerPage/4), DMA_CH_CTRL_BURST_SIZE_1_TRANS, DMA_CH_CTRL_WIDTH_WORD_TRANS, DMA_CH_CTRL_NORMAL_MODE, DMA_CH_CTRL_INCREMENT_ADDR, DMA_CH_CTRL_REQ_SEL_NONE);
	edma_SetDstParams(DMA_CH2, DMA_CH_CTRL_WIDTH_WORD_TRANS, DMA_CH_CTRL_HANDSHAKING_MODE, DMA_CH_CTRL_FIXED_ADDR, DMA_CH_CTRL_REQ_SEL_SPI2_TX);
	edma_Start(DMA_CH2, DMA_CH_CTRL_PRIORITY_HIGH);

	//	wait End of SPI transfer interrupt
	while(status & (1<<4) == 0)
	{
		status = RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID);
	}
	RW_SPI_REG(SPI_INTR_ST, pNorDev.pSPIPara.tSPIID) = 0x00000010;		//clear End of SPI transfer interrupt status

	//check write complete
	while((statusReg & 0x01) != 0x00)
	{
		statusReg = nor_ReadStatus1Reg();
	}
}
#define pRdBufAddr 0x50000;
#define pWrBufAddr 0x60000
//--------------------------------------------
void nor_WriteData(U32 addr, U32 buf, U32 count)
{
	U32 i, j;
	U32 uiPage, uiSector, uiRPage, uiWrSz, uiOffsetSz;
	S32 iRemain;
	volatile U8 *pWrBuf, *pBuf, *pRdBuf, *ptr;

	if((addr+count)> 0x1FFFFF)
		return;

	pRdBuf = (volatile U8*)pRdBufAddr;

	pBuf = (volatile U8*)buf;

	nor_SetStatus1Reg(0x00);

	uiPage = addr/pNorDev.bytesPerPage;

	uiSector = uiPage/pNorDev.pagesPerSect;

	uiRPage = pNorDev.pagesPerSect*uiSector;

	uiOffsetSz = (uiPage - uiRPage)*pNorDev.bytesPerPage;

	iRemain = (uiOffsetSz + count) -  pNorDev.bytesPerSect;

	if(iRemain>=0)
		uiWrSz = pNorDev.bytesPerSect - uiOffsetSz;
	else
		uiWrSz = count;

	while(1){

		// backup sector data
		pWrBuf = (volatile U8*)pWrBufAddr;

		// backup data
	//	memset(pWrBuf, 0, pNorDev.bytesPerSect);
		nor_ReadData((uiRPage*pNorDev.bytesPerPage), pWrBuf, pNorDev.bytesPerSect);
		nor_delay(20000);	//read need to delay

		// update data
		memcpy((pWrBuf+uiOffsetSz),pBuf,uiWrSz);

		// erase sector
		nor_EraseSector(uiSector);

		// write back
		for(i=0;i<pNorDev.pagesPerSect;i++,uiRPage++){
			U32 wcnt = 0;
			do{
				nor_WritePage(uiRPage,pWrBuf);

			//	memset(pRdBuf, 0, pNorDev.bytesPerPage);
				nor_ReadPage(uiRPage,pRdBuf);

				if (wcnt>MAX_WRITE_TIMES-1){
					wcnt++;
					goto nor_WRERR;
				}
				wcnt++;
			}while(memcmp(pWrBuf, pRdBuf, pNorDev.bytesPerPage)!=0);

			pWrBuf += pNorDev.bytesPerPage;
		}

		if(iRemain<=0) break;
		pBuf += uiWrSz;
		uiSector++;
		uiOffsetSz = 0;

		if(iRemain >= pNorDev.bytesPerSect){
			uiWrSz = pNorDev.bytesPerSect;
		}else{
			uiWrSz = iRemain;
		}
		iRemain -= uiWrSz;

	};

nor_WRERR:

	return;
}

//--------------------------------------------
S32 nor_Test(U32 src_addr, U32 dst_addr)
{
	S32 i=0, j, k, err = 0;
	volatile U8 *pWBuf, *pRBuf;

	for(i=0; i<10; i++){
		pWBuf = (volatile U8*)src_addr;
		pRBuf = (volatile U8*)dst_addr;
		memset(pWBuf,(0x5A+i),128);
		memset(pRBuf,0x00,128);

		nor_WriteData(i*512, pWBuf, 128);

		nor_ReadData(i*512, pRBuf, 128);
		nor_delay(30000);	//read need to delay

		for(j=0;j<128;j++){
			if(*pRBuf != *pWBuf)
				err++;
			pRBuf++;
			pWBuf++;
		}
	}

	return err;
}
