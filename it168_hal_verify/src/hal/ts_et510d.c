#include <dma.h>
#include <ts_et510d.h>

#define ET510D_SPI_CLK 100000	//100k

#define ET510D_REG_R		0x20
#define ET510D_REG_R_S		0x22
#define ET510D_REG_R_S_BW	0x23
#define ET510D_REG_W		0x24
#define ET510D_REG_W_S		0x26
#define ET510D_REG_W_S_BW	0x27
#define ET510D_REG_IMG_R	0x50

TSPIPara pSPIPara;
//---------------------------------------------------
void et510d_Init(TSPIID spi_id)
{
	pSPIPara.tSPIID = spi_id;
	pSPIPara.Clk = ET510D_SPI_CLK;

	pSPIPara.Phs = PHASE_2EDGE;		//sampling data at even edge
	pSPIPara.Pol = POLARITY_HIGH;		//high in the idle states
	pSPIPara.SlvMode = MASTER_MODE;	//master mode
	pSPIPara.Lsb = MSB_MODE;		//MSB first
	pSPIPara.DataMerge = DATAMERGE_OFF;//disable data merge mode
	pSPIPara.Bit = 8;		// 8 bits
	pSPIPara.AddrLen = ADDRLEN_1BYTE;	// 1 bytes address

	spi_Open(&pSPIPara);

}
//---------------------------------------------------
void et510d_CheckSPIFinish(void)
{
	U32 ret;
	ret = 0xFF;

	while(ret%2 == 1){
	  ret = RW_SPI_REG(SPI_STATUS, pSPIPara.tSPIID);
	}

	return;
}

//-------------------------------------------
void et510d_ReadByte(U16 reg, U32 buf, U32 size)
{
	U32 status, i = size;
	volatile U8* pBuf;

	et510d_CheckSPIFinish();

	RW_SPI_REG(SPI_TRANS_CTRL, pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
												| (0x01 << 29)	//enable address phase
												| (0x02 << 24)	//read only transfer mode
												| (((size&0x1FF)-1) << 0); //data receive count

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_CTRL, pSPIPara.tSPIID) = 0;	//disable DMA
	RW_SPI_REG(SPI_INTR_EN, pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_ADDR, pSPIPara.tSPIID) = (reg & 0xFF);
	RW_SPI_REG(SPI_CMD, pSPIPara.tSPIID) = ((reg >> 8) & 0xFF);

	pBuf = buf;
	do{
		status = 0xFFFFFFFF;
		// check RXEMPTY
		while(status & (1<<14) != 0)
		{
			status = RW_SPI_REG(SPI_STATUS, pSPIPara.tSPIID);
		}

		pBuf[size-i] = RW_SPI_REG(SPI_DATA, pSPIPara.tSPIID);
	}while(--i);
}
//-------------------------------------------
void et510d_WriteByte(U16 reg, U32 buf, U32 size)
{
	U32 status, i = size;
	volatile U8* pBuf;

	et510d_CheckSPIFinish();

	RW_SPI_REG(SPI_TRANS_CTRL, pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
												| (0x01 << 29)	//enable address phase
												| (0x01 << 24)	//write only transfer mode
												| (((size&0x1FF)-1) << 12); //data transfer count

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_CTRL, pSPIPara.tSPIID) = 0;	//disable DMA
	RW_SPI_REG(SPI_INTR_EN, pSPIPara.tSPIID) = 0;

	RW_SPI_REG(SPI_ADDR, pSPIPara.tSPIID) = (reg & 0xFF);
	RW_SPI_REG(SPI_CMD, pSPIPara.tSPIID) = ((reg >> 8) & 0xFF);

	pBuf = buf;
	do{
		status = 0;
		// check TXEMPTY
		while(status & (1<<22) != 1)
		{
			status = RW_SPI_REG(SPI_STATUS, pSPIPara.tSPIID);
		}

		RW_SPI_REG(SPI_DATA, pSPIPara.tSPIID) = pBuf[size-i];
	}while(--i);
}
//--------------------------------------
//ET510D : 0x03 / 0x0A / 0x05
int et510d_Type_Check(void)
{

	U8 buf1=0, buf2=0, buf3=0;
	U16 reg;

	reg = ((ET510D_REG_R<<8) | 0x00);
	et510d_ReadByte(reg, &buf1, 1);
	if(buf1 != 0xAA)
		return -1;

	reg = ((ET510D_REG_R<<8) | 0xFD);
	et510d_ReadByte(reg, &buf1, 1);

	reg = ((ET510D_REG_R<<8) | 0xFE);
	et510d_ReadByte(reg, &buf2, 1);

	reg = ((ET510D_REG_R<<8) | 0xFF);
	et510d_ReadByte(reg, &buf3, 1);
	if(buf1 == 0x03 && buf2 == 0x0A && buf3 == 0x05)
		return 0;
	else
		return -1;

}
//--------------------------------------
void et510d_Stus(void)
{
	U8 buf;
	U16 reg;

	reg = ((ET510D_REG_R<<8) | 0x00);
	et510d_ReadByte(reg, &buf, 1);
}
//--------------------------------------
void et510d_Scan_Frames(U8 num)
{
	U8 buf;
	U16 reg;

	reg = ((ET510D_REG_R<<8) | 0x2C);
	et510d_ReadByte(reg, &buf, 1);

	buf = num;

	reg = ((ET510D_REG_W<<8) | 0x2C);
	et510d_WriteByte(reg, &buf, 1);

}
//--------------------------------------
void et510d_Scan_Csr(void)
{
	U8 buf;
	U16 reg;

	reg = ((ET510D_REG_R<<8) | 0x2D);
	et510d_ReadByte(reg, &buf, 1);

	buf = (0<<6)	//Zone-1
		| (1<<4)	//scan go
		| (1<<3)	//test pattern
		| (1<<1)	//moving average filter
		| (1<<0);	//statistic engine

	reg = ((ET510D_REG_W<<8) | 0x2D);
	et510d_WriteByte(reg, &buf, 1);

}
//-------------------------------------------
void et510d_Dma_GetFrame(U32 buf)
{
	U32 size = ET510D_PIXEL_SIZE;
	U32 status;
	U32 src;

	et510d_CheckSPIFinish();

	RW_SPI_REG(SPI_TRANS_CTRL, pSPIPara.tSPIID) = (0x01 << 30)	//enable command phase
												| (0x02 << 24)	//read only transfer mode
												| (((size&0x1FF)-1) << 0); //data receive count

	RW_SPI_REG(SPI_RD_TRANS_CNT_MSB, pSPIPara.tSPIID) = (size>>9);

	RW_SPI_REG(SPI_CTRL, pSPIPara.tSPIID) = (0x02 << 16)		//transmit FIFO threshold
										  | (0x02 << 8)			//receive FIFO threshold
										  |	(0x01 << 4)			//enable TX dma
										  | (0x01 << 3);		//enable RX dma
	RW_SPI_REG(SPI_INTR_EN, pSPIPara.tSPIID) = (0x01 << 4);		//enable the end of SPI transfer INT

	RW_SPI_REG(SPI_CMD, pSPIPara.tSPIID) = ET510D_REG_IMG_R;

	src = (SPI_REG_BASE_ADDR + 0x2C + pSPIPara.tSPIID*0x00400000);
	edma_Init(DMA_CH4, src, buf);
	edma_SetSrcParams(DMA_CH4, size, DMA_CH_CTRL_BURST_SIZE_1_TRANS, DMA_CH_CTRL_WIDTH_BYTE_TRANS, DMA_CH_CTRL_HANDSHAKING_MODE, DMA_CH_CTRL_FIXED_ADDR, DMA_CH_CTRL_REQ_SEL_SPI2_RX);
	edma_SetDstParams(DMA_CH4, DMA_CH_CTRL_WIDTH_BYTE_TRANS, DMA_CH_CTRL_NORMAL_MODE, DMA_CH_CTRL_INCREMENT_ADDR, DMA_CH_CTRL_REQ_SEL_NONE);
	edma_Start(DMA_CH4, DMA_CH_CTRL_PRIORITY_HIGH);


	//	wait End of SPI transfer interrupt
	status = 0;
	while(status & (1<<4) == 0)
	{
		status = RW_SPI_REG(SPI_INTR_ST, pSPIPara.tSPIID);
	}
	RW_SPI_REG(SPI_INTR_ST, pSPIPara.tSPIID) = 0x00000010;		//clear End of SPI transfer interrupt status

}
