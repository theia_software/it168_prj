#include <ISR.h>
#include <UART.h>
#include <DMA.h>
#include <sys.h>
#include "platform.h"

#define SYSTEM_HW_TRIG_TYPE					(0x320)
#define SYSTEM_SW_TRIG_TYPE					(0x324)
#define TRIG_TYPE_EDGE						(0xffffffff)
#define INT_DEBUG_ENABLE					1

void setup_uart_00_irq(void);
void setup_uart_01_irq(void);
void setup_dma_irq(void);


U32 UART00_irq_outvalue = 0;
U32 UART01_irq_outvalue = 0;
U32 DMA_irq_outvalue = 0;
volatile U32 *debugUartPtr1;
volatile U32 *debugUartPtr2;
volatile U32 *debugDmaPtr;

void ISR_setup(void)
{

	UART00_irq_outvalue = 0;
	UART01_irq_outvalue = 0;
	DMA_irq_outvalue = 0;
	*(volatile U32*)(0x0001FF00) = 0;
	*(volatile U32*)(0x0001FF10) = 0;
	*(volatile U32*)(0x0001FF20) = 0;
	debugUartPtr1 = (volatile U32*)(0x0001FF00);
	debugUartPtr2 = (volatile U32*)(0x0001FF10);
	debugDmaPtr = (volatile U32*)(0x0001FF20);

	/* Disable the Machine external, timer and software interrupts until setup is done */
	clear_csr(NDS_MIE, MIP_MEIP | MIP_MTIP | MIP_MSIP);

	RW_SYS_REG(SYSTEM_HW_TRIG_TYPE) = TRIG_TYPE_EDGE;

	setup_uart_00_irq();
	setup_uart_01_irq();
	setup_dma_irq();
}
//--------------------------------------------
void ISR_enable(void)
{
	/* Enable the Machine External/Timer/Sofware interrupt bit in MIE. */
	set_csr(NDS_MIE, MIP_MEIP | MIP_MTIP | MIP_MSIP);
	/* Enable interrupts in general. */
	set_csr(NDS_MSTATUS, MSTATUS_MIE);
}
//--------------------------------------------
void setup_uart_00_irq(void)
{
	/* Priority must be set > 0 to trigger the interrupt */
	__nds__plic_set_priority(IRQ_UART0_SOURCE, 1);

	__nds__plic_enable_interrupt(IRQ_UART0_SOURCE);
}
//--------------------------------------------
void setup_uart_01_irq(void)
{
	/* Priority must be set > 0 to trigger the interrupt */
	__nds__plic_set_priority(IRQ_UART1_SOURCE, 1);

	__nds__plic_enable_interrupt(IRQ_UART1_SOURCE);
}
//--------------------------------------------
void setup_dma_irq(void)
{
	/* Priority must be set > 0 to trigger the interrupt */
	__nds__plic_set_priority(IRQ_DMA_SOURCE, 1);

	__nds__plic_enable_interrupt(IRQ_DMA_SOURCE);
}
//--------------------------------------------
void uart0_irq_handler(void)
{
	U32 status;

	status = RW_UART_REG(UART_INTERRUPT_FIFO_CONTROL, UART0);


#if INT_DEBUG_ENABLE == 1
	UART00_irq_outvalue++;
	*debugUartPtr1 = (UART00_irq_outvalue << 4) | (status & 0xF);
#endif
}
//--------------------------------------------
void uart1_irq_handler(void)
{
	U32 status;

	status = RW_UART_REG(UART_INTERRUPT_FIFO_CONTROL, UART1);

#if INT_DEBUG_ENABLE == 1
	UART01_irq_outvalue++;
	*debugUartPtr2 = (UART01_irq_outvalue << 4) | (status & 0xF);
#endif
}
//--------------------------------------------
void dma_irq_handler(void)
{
	U32 status;

	status = RW_DMA_REG(DMA_INT_STATUS);

#if INT_DEBUG_ENABLE == 1
	DMA_irq_outvalue++;
	*debugDmaPtr = (DMA_irq_outvalue << 24) | status;
#endif

}
