
#include <stdlib.h>
#include <ae250.h>
#include "UART.h"
#include "DMA.h"

#define REF_CLK	78*1024*1024

typedef struct{
	U32 baud_rate;
	U32 uart_clk_divide;
	U32 baudrate_divisor;
	U32 oscr;
}TUartDiv;

TUartDiv uartDiv[7] =
{
	{1200, 61, 17, 16},
	{9600, 43, 3, 16},
	{38400, 32, 1, 16},
	{57600, 21, 1, 16},
	{115200, 11, 2, 8},
	{230400, 11, 1, 8}
//	{460800, 1, 5, 8}
};

void uart_UClk_Set(U32 uart_clk_divide)
{
	// The frequency of uclk = system_clk(350MHz) / uart_clk_divide => TBD
	*(volatile U32* )(0x10000688) = uart_clk_divide;
}
//-------------------------------------------------------------
U32 uart_Read_Id(TUartID id)
{
	U32 uart_id = RW_UART_REG(UART_IDREV, id);
	return uart_id;
}
//-------------------------------------------------------------
// The divisor value = the frequency of uclk / (desired baud rate x OSCR(16))
void uart_Dma_Open(TUartID id, U32 baud_rate, TParity parity, TStopBits stopbits)
{
	U32 baud_rate_divisor_value, uclk_div, oscr;
	U32 div, i;
	S32 idx = -1;

	for(i=0; i<7; i++){
		if(baud_rate == uartDiv[i].baud_rate)
			idx = i;
	}

	if(idx < 0){
		//default 115200
		uclk_div = uartDiv[4].uart_clk_divide;
		baud_rate_divisor_value = uartDiv[4].baudrate_divisor;
		oscr = uartDiv[4].oscr;
	}else{
		uclk_div = uartDiv[idx].uart_clk_divide;
		baud_rate_divisor_value = uartDiv[idx].baudrate_divisor;
		oscr = uartDiv[idx].oscr;
	}

	uart_UClk_Set(uclk_div);
	RW_UART_REG(UART_OVER_SAMPLE_CONTROL, id) = oscr;

	RW_UART_REG(UART_LINE_CONTROL, id) = 0x00000080;	//DLAB = 1
	RW_UART_REG(UART_RBR_THR_DLL, id) = baud_rate_divisor_value & 0xFF;				//LSB
	RW_UART_REG(UART_INTERRUPT_ENABLE_DLM, id) = baud_rate_divisor_value>>8;		//MSB
	RW_UART_REG(UART_LINE_CONTROL, id) = 0x00000000;	//DLAB = 0
	RW_UART_REG(UART_INTERRUPT_ENABLE_DLM, id) = 0x00000002;			//enable THR interrupt
	RW_UART_REG(UART_LINE_CONTROL, id) = 0x00000003 + (parity<<3) + (stopbits<<2);
	RW_UART_REG(UART_INTERRUPT_FIFO_CONTROL, id) = 0x00000001 + (1 << 3);	//FIFO enable, enable DMA

}
//-------------------------------------------------------------
void uart_Dma_Tx(TUartID id, U32 buf, U32 size)
{
	U32 Reg, SrcReqSel;

	Reg = (UART_REG_BASE_ADDR + 0x20 + id*0x00400000);
 	SrcReqSel = (id == UART0)? DMA_CH_CTRL_REQ_SEL_UART0_TX:DMA_CH_CTRL_REQ_SEL_UART1_TX;

	edma_Init(DMA_CH2, buf, Reg);
	edma_SetSrcParams(DMA_CH2, size, DMA_CH_CTRL_BURST_SIZE_1_TRANS, DMA_CH_CTRL_WIDTH_BYTE_TRANS, DMA_CH_CTRL_NORMAL_MODE, DMA_CH_CTRL_INCREMENT_ADDR, DMA_CH_CTRL_REQ_SEL_NONE);
	edma_SetDstParams(DMA_CH2, DMA_CH_CTRL_WIDTH_BYTE_TRANS, DMA_CH_CTRL_HANDSHAKING_MODE, DMA_CH_CTRL_FIXED_ADDR, SrcReqSel);
	edma_Start(DMA_CH2, DMA_CH_CTRL_PRIORITY_HIGH);

}
//-------------------------------------------------------------
void uart_Dma_Rx(TUartID id, U32 buf, U32 size)
{
	U32 Reg, SrcReqSel;

	Reg = (UART_REG_BASE_ADDR + 0x20 + id*0x00400000);
	SrcReqSel = (id == UART0)? DMA_CH_CTRL_REQ_SEL_UART0_RX:DMA_CH_CTRL_REQ_SEL_UART1_RX;

	edma_Init(DMA_CH4, Reg, buf);
	edma_SetSrcParams(DMA_CH4, size, DMA_CH_CTRL_BURST_SIZE_1_TRANS, DMA_CH_CTRL_WIDTH_BYTE_TRANS, DMA_CH_CTRL_HANDSHAKING_MODE, DMA_CH_CTRL_FIXED_ADDR, SrcReqSel);
	edma_SetDstParams(DMA_CH4, DMA_CH_CTRL_WIDTH_BYTE_TRANS, DMA_CH_CTRL_NORMAL_MODE, DMA_CH_CTRL_INCREMENT_ADDR, DMA_CH_CTRL_REQ_SEL_NONE);
	edma_Start(DMA_CH4, DMA_CH_CTRL_PRIORITY_HIGH);

}
//-------------------------------------------------------------
void uart_Dma_WaitFinish(TUartID id)
{
	U32 status = 0x0;

	while((status & (0x01<<6)) == 0)
	{
		status = RW_UART_REG(UART_LINE_STATUS, id);
	}

}
//-------------------------------------------------------------
//-1: not finish, 0: finish
int uart_Dma_CheckFinish(TUartID id)
{
	U32 status = 0x0;

	status = RW_UART_REG(UART_LINE_STATUS, id);
	if((status & (0x01<<6)) == 0)
		return -1;
	else
		return 0;
}
//------------------------------------------------------------
// The divisor value = the frequency of uclk / (desired baud rate x OSCR(16))
void uart_Open(TUartID id, U32 baud_rate, TParity parity, TStopBits stopbits)
{
	U32 baud_rate_divisor_value, uclk_div, oscr;
	U32 div, i;
	S32 idx = -1;

	for(i=0; i<7; i++){
		if(baud_rate == uartDiv[i].baud_rate)
			idx = i;
	}

	if(idx < 0){
		//default 115200
		uclk_div = uartDiv[4].uart_clk_divide;
		baud_rate_divisor_value = uartDiv[4].baudrate_divisor;
		oscr = uartDiv[4].oscr;
	}else{
		uclk_div = uartDiv[idx].uart_clk_divide;
		baud_rate_divisor_value = uartDiv[idx].baudrate_divisor;
		oscr = uartDiv[idx].oscr;
	}

	uart_UClk_Set(uclk_div);
	RW_UART_REG(UART_OVER_SAMPLE_CONTROL, id) = oscr;

	RW_UART_REG(UART_LINE_CONTROL, id) = 0x00000080;	//DLAB = 1
	RW_UART_REG(UART_RBR_THR_DLL, id) = baud_rate_divisor_value & 0xFF;				//LSB
	RW_UART_REG(UART_INTERRUPT_ENABLE_DLM, id) = baud_rate_divisor_value>>8;		//MSB
	RW_UART_REG(UART_LINE_CONTROL, id) = 0x00000000;	//DLAB = 0
	RW_UART_REG(UART_INTERRUPT_ENABLE_DLM, id) = 0x00000002;	//enable THR interrupt
	RW_UART_REG(UART_LINE_CONTROL, id) = 0x00000003 + (parity<<3) + (stopbits<<2);
	RW_UART_REG(UART_INTERRUPT_FIFO_CONTROL, id) = 0x00000001;	//FIFO enable

}
//------------------------------------------------------------
int uart_Tx(TUartID id, U32 buf, U32 size)
{
	U32 i, status = 0, n=0;
	volatile U8* ptr = (volatile U8*)buf;

	while(size>0){

		// Wait for TxFIFO to be empty. (THRE)
		while(1){
			status = RW_UART_REG(UART_LINE_STATUS, id);
			if((status & (1<<5))) break;
		}

		// Put at most 16 data into TxFIFO
		for(i=0;i<16;i++){
			RW_UART_REG(UART_RBR_THR_DLL, id) = *ptr++;

			size--;
			n++;
			if(size<=0) break;
		}
	}

	return n;
}
//------------------------------------------------------------
int uart_Rx(TUartID id, U32 buf, U32 size)
{
	U32 status = 0, n=0;
	volatile U8* ptr = (volatile U8*)buf;

	while(size>0){
		// If RxFIFO is empty, stop getting data from FIFO.
		// Wait for RxFIFO to be data-ready.

		while(1){
			status = RW_UART_REG(UART_LINE_STATUS, id);
			if((status & (1<<0))) break;
		}

		// Get one data from RxFIFO
		*ptr++ = RW_UART_REG(UART_RBR_THR_DLL, id);

		size--;
		n++;
	}

	return n;
}
//-------------------------------------------------------------
