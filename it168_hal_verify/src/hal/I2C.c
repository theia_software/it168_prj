
#include <stdlib.h>
#include "I2C.h"
#include "DMA.h"

U32 iic_Read_Id(TI2CId ch)
{
	U32 i2c_id = RW_I2C_REG(I2C_IDREV, ch);
	return i2c_id;
}
//-----------------------------------------
U32 iic_Read_Config(TI2CId ch)
{
	U32 i2c_config = RW_I2C_REG(I2C_CONFIG, ch);
	return i2c_config;
}
//-----------------------------------------
void iic_Edma_StartIO(TI2CId ch, TI2CDir dir, U32 buf, U32 size)
{
	U32 Reg, SrcReqSel;
	U32 status = 0x0;

	Reg = (I2C_REG_BASE_ADDR + 0x20 + ch*0x00400000);
	SrcReqSel = (ch == I2C0)? DMA_CH_CTRL_REQ_SEL_I2C0:DMA_CH_CTRL_REQ_SEL_I2C1;

	if(dir == I2C_DIR_TX){

		edma_Init(DMA_CH2, buf, Reg);
		edma_SetSrcParams(DMA_CH2, size, DMA_CH_CTRL_BURST_SIZE_1_TRANS, DMA_CH_CTRL_WIDTH_BYTE_TRANS, DMA_CH_CTRL_NORMAL_MODE, DMA_CH_CTRL_INCREMENT_ADDR, DMA_CH_CTRL_REQ_SEL_NONE);
		edma_SetDstParams(DMA_CH2, DMA_CH_CTRL_WIDTH_BYTE_TRANS, DMA_CH_CTRL_HANDSHAKING_MODE, DMA_CH_CTRL_FIXED_ADDR, SrcReqSel);
		edma_Start(DMA_CH2, DMA_CH_CTRL_PRIORITY_HIGH);
	}else{

		edma_Init(DMA_CH4, Reg, buf);
		edma_SetSrcParams(DMA_CH4, size, DMA_CH_CTRL_BURST_SIZE_1_TRANS, DMA_CH_CTRL_WIDTH_BYTE_TRANS, DMA_CH_CTRL_HANDSHAKING_MODE, DMA_CH_CTRL_FIXED_ADDR, SrcReqSel);
		edma_SetDstParams(DMA_CH4, DMA_CH_CTRL_WIDTH_BYTE_TRANS, DMA_CH_CTRL_NORMAL_MODE, DMA_CH_CTRL_INCREMENT_ADDR, DMA_CH_CTRL_REQ_SEL_NONE);
		edma_Start(DMA_CH4, DMA_CH_CTRL_PRIORITY_HIGH);

	}


	//wait transaction completion
	while(1)
	{
		status = RW_I2C_REG(I2C_STATUS, ch);
		if((status & (1<<9))) break;
	}

	//clear the completion interrupt status
	RW_I2C_REG(I2C_STATUS, ch) = 0x0000ffff;

}
//-----------------------------------------
void iic_Slave_Dma_TxData(TI2CId ch, U32 id, U32 buf, U32 size)
{

	RW_I2C_REG(I2C_ADDRESS, ch) = id;
	RW_I2C_REG(I2C_SETUP, ch) = (0x00 << 24)	//T_SUDAT
							  | (0x04 << 21)	//T_SP
							  | (0x00 << 16)	//T_HDDAT
							  | (0x01 << 13)	//T_SCLRatio
							  | (0x1e << 4)		//T_SCLHi
							  | (0x01 << 3)		//DMAEn
							  | (0x00 << 2)		//Master->Slave mode
							  | (0x00 << 1)		//addressing->7 bit
							  | (0x01 << 0);	//IICEn

	RW_I2C_REG(I2C_CONTROL, ch) = (0x01 << 8) + size;	//slave transmit
	RW_I2C_REG(I2C_INTERRUPT_ENABLE, ch) = (0x01 << 9)	//CmpL
										 | (0x01 << 3);	//AddrHit

	iic_Edma_StartIO(ch, I2C_DIR_TX, buf, size);
}
//-----------------------------------------
void iic_Slave_Dma_RxData(TI2CId ch, U32 id, U32 buf, U32 size)
{

	RW_I2C_REG(I2C_ADDRESS, ch) = id;

	RW_I2C_REG(I2C_SETUP, ch) = (0x00 << 24)	//T_SUDAT
							  | (0x04 << 21)	//T_SP
							  | (0x00 << 16)	//T_HDDAT
							  | (0x01 << 13)	//T_SCLRatio
							  | (0x1e << 4)		//T_SCLHi
							  | (0x01 << 3)		//DMAEn
							  | (0x00 << 2)		//Master->Slave mode
							  | (0x00 << 1)		//addressing->7 bit
							  | (0x01 << 0);	//IICEn


	RW_I2C_REG(I2C_CONTROL, ch) = (0x00 << 8) + size;	//slave receive
	RW_I2C_REG(I2C_INTERRUPT_ENABLE, ch) = (0x01 << 9)	//CmpL
			   	   	   	   	   	   	     | (0x01 << 3);	//AddrHit

	iic_Edma_StartIO(ch, I2C_DIR_RX, buf, size);
}
//-----------------------------------------
void iic_Master_Dma_TxData(TI2CId ch, U32 address, U32 buf, U32 size)
{

	RW_I2C_REG(I2C_ADDRESS, ch) = address;
	RW_I2C_REG(I2C_SETUP, ch) = (0x00 << 24)	//T_SUDAT
							  | (0x04 << 21)	//T_SP
							  | (0x00 << 16)	//T_HDDAT
							  | (0x00 << 13)	//T_SCLRatio
							  | (0x2A << 4)		//T_SCLHi
							  | (0x01 << 3)		//DMAEn
							  | (0x01 << 2)		//Master->Master mode
							  | (0x00 << 1)		//addressing->7 bit
							  | (0x01 << 0);	//IICEn

	RW_I2C_REG(I2C_CONTROL, ch) = (0x01 << 12)	//Phase_start
								| (0x01 << 11)	//Phase_addr
								| (0x01 << 10)	//Phase_data
								| (0x01 << 9)	//Phase_stop
								| (0x00 << 8)	//Dir
								| size;			//DataCnt

	RW_I2C_REG(I2C_INTERRUPT_ENABLE, ch) = (0x01 << 9);	//CmpL

	RW_I2C_REG(I2C_COMMAND, ch) = 0x01;	//CMD

	iic_Edma_StartIO(ch, I2C_DIR_TX, buf, size);

}
//-----------------------------------------
void iic_Master_Dma_RxData(TI2CId ch, U32 address, U32 buf, U32 size)
{

	RW_I2C_REG(I2C_ADDRESS, ch) = address;

	RW_I2C_REG(I2C_SETUP, ch) = (0x00 << 24)	//T_SUDAT
							  | (0x04 << 21)	//T_SP
							  | (0x00 << 16)	//T_HDDAT
							  | (0x00 << 13)	//T_SCLRatio
							  | (0x2A << 4)		//T_SCLHi
							  | (0x01 << 3)		//DMAEn
							  | (0x01 << 2)		//Master->Master mode
							  | (0x00 << 1)		//addressing->7 bit
							  | (0x01 << 0);	//IICEn


	RW_I2C_REG(I2C_CONTROL, ch) = (0x01 << 12)	//Phase_start
								| (0x01 << 11)	//Phase_addr
								| (0x01 << 10)	//Phase_data
								| (0x01 << 9)	//Phase_stop
								| (0x01 << 8)	//Dir
								| size;			//DataCnt

	RW_I2C_REG(I2C_INTERRUPT_ENABLE, ch) = (0x01 << 9);	//CmpL

	RW_I2C_REG(I2C_COMMAND, ch) = 0x01;	//CMD

	iic_Edma_StartIO(ch, I2C_DIR_RX, buf, size);
}
