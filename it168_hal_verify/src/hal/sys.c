/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: sys.c
 */
#include <sys.h>

void sys_init(void)
{
	/* reset modules */
	RW_SYS_REG(SYSTEM_REG_RESET) |= (SET_RESET_TCON_EN | SET_RESET_SENSOR_EN | SET_RESET_RISCV_EN | SET_RESET_DMA_EN);


	/* release reset modules */
	RW_SYS_REG(SYSTEM_REG_RESET) &= (CLR_RESET_TCON_DIS | CLR_RESET_SENSOR_DIS | CLR_RESET_RISCV_DIS | CLR_RESET_DMA_DIS);
}


