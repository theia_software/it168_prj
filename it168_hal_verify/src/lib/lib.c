/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: lib.c
 */
#include "lib.h"

void set_dbg_val(uint32_t addr, uint32_t val)
{
	volatile uint32_t *CheckStatus;

	CheckStatus = (volatile uint32_t *)addr;
	*CheckStatus = val;
}

void delay(const U32 cycle)
{
    volatile U32 idx;

    for (idx = 0; idx < cycle; ++idx)
        NOP();
}

