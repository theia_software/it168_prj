
#ifndef _SPIMODULE_H_
#define _SPIMODULE_H_

#include <stdio.h>
#include <stdint.h>


/* Definition */
#define CONFIG_SPI_SLAVE_IDX                (CONFIG_SPI_SLAVE_00)
    #define CONFIG_SPI_SLAVE_00             (0)
    #define CONFIG_SPI_SLAVE_01             (1)

#define BASE_ADDRESS                        (0x00000000UL)
#define BASE_ADDRESS_LIMIT                  (0x00050000UL)

#define SPIS0_REG_BASE                      (0x20000000UL)
#define SPIS1_REG_BASE                      (0x60000000UL)

#if (CONFIG_SPI_SLAVE_IDX == CONFIG_SPI_SLAVE_00)
#define SPISN_REG_BASE                      (SPIS0_REG_BASE)
#else
#define SPISN_REG_BASE                      (SPIS1_REG_BASE)
#endif

/* SPI Slave no. */
#define SPISN_GEN_DATE                      (SPISN_REG_BASE + 0x00)
#define SPISN_GEN_VER                       (SPISN_REG_BASE + 0x04)
#define SPISN_POS_EDGE_SEL                  (SPISN_REG_BASE + 0x08)
#define SPISN_BASE_ADDR                     (SPISN_REG_BASE + 0x0c)
#define SPISN_INT_CLR                       (SPISN_REG_BASE + 0x10) // WC
#define SPISN_DUMMY_01                      (SPISN_REG_BASE + 0x14)
#define SPISN_DUMMY_02                      (SPISN_REG_BASE + 0x18)
#define SPISN_CMD_63_32                     (SPISN_REG_BASE + 0x1C) // RO
#define SPISN_CMD_31_00                     (SPISN_REG_BASE + 0x20) // RO
#define SPISN_MSB_INVERT                    (SPISN_REG_BASE + 0x24)

#define SPISN_SPEED_SEL                     (SPISN_REG_BASE + 0x28)
#define SPISN_MISO_DELAY_SEL_OFF            (0)
#define SPISN_MISO_DELAY_SEL_MASK           (0x3)
#define SET_SPISN_MISO_2ND_EDGE_SEL         (1 << 4)
#define SET_SPISN_MISO_1ST_EDGE_SEL         (0 << 4)
#define SET_SPISN_SPEED_MODE_EN             (1 << 5)

#define SPISN_DMY_BYTE                      (SPISN_REG_BASE + 0x2c)
#define SPISN_DMY_BYTE_MASK                 (0x7f)

#define SPISN_REG_LEN                       (SPISN_MSB_INVERT - SPISN_REG_BASE + 4)

/* SPI Slave 00 */
#define SPIS0_GEN_DATE                      (SPIS0_REG_BASE + 0x00)
#define SPIS0_GEN_VER                       (SPIS0_REG_BASE + 0x04)
#define SPIS0_POS_EDGE_SEL                  (SPIS0_REG_BASE + 0x08)
#define SPIS0_BASE_ADDR                     (SPIS0_REG_BASE + 0x0c)
#define SPIS0_INT_CLR                       (SPIS0_REG_BASE + 0x10) // WC
#define SPIS0_DUMMY_01                      (SPIS0_REG_BASE + 0x14)
#define SPIS0_DUMMY_02                      (SPIS0_REG_BASE + 0x18)
#define SPIS0_CMD_63_32                     (SPIS0_REG_BASE + 0x1C) // RO
#define SPIS0_CMD_31_00                     (SPIS0_REG_BASE + 0x20) // RO
#define SPIS0_MSB_INVERT                    (SPIS0_REG_BASE + 0x24)

/* SPI Slave 01 */
#define SPIS1_GEN_DATE                      (SPIS1_REG_BASE + 0x00)
#define SPIS1_GEN_VER                       (SPIS1_REG_BASE + 0x04)
#define SPIS1_POS_EDGE_SEL                  (SPIS1_REG_BASE + 0x08)
#define SPIS1_BASE_ADDR                     (SPIS1_REG_BASE + 0x0c)
#define SPIS1_INT_CLR                       (SPIS1_REG_BASE + 0x10) // WC
#define SPIS1_DUMMY_01                      (SPIS1_REG_BASE + 0x14)
#define SPIS1_DUMMY_02                      (SPIS1_REG_BASE + 0x18)
#define SPIS1_CMD_63_32                     (SPIS1_REG_BASE + 0x1C) // RO
#define SPIS1_CMD_31_00                     (SPIS1_REG_BASE + 0x20) // RO
#define SPIS1_MSB_INVERT                    (SPIS1_REG_BASE + 0x24)


void spi_clk_cmd(void);


class SPIModule
{
public:
    static void set_trig_mode(bool is_positive);
    static uint8_t get_trig_mode(void);
    static void reset_base_addr(void);
    static void set_base_addr(uint32_t address);
    static uint32_t get_base_addr(void);
};

#endif

