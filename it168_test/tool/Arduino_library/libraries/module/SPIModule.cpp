#include <stdio.h>
#include "SPIModule.h"
#include "SPIUtility.h"
#include "serial_command.h"
#include "common.h"

void spi_clk_cmd(void)
{
#define SPI_CLK_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[SPI_CLK_CMD_ARG_NUM_MAX];
    U32 argc;
    U32 spi_clk;
    const U8 arduino_clk = 16; // Arduino systom clock = 16MHz
    char *arg;


    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= SPI_CLK_CMD_ARG_NUM_MAX) {
            Serial.println(F("err: too many arguments"));
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 10);

#ifdef SERIALCOMMANDDEBUG
        Serial.print(argc);
        Serial.print(" argument ");
        print_hex(arg_ary[argc]);
#endif
        argc += 1;
    }

    if (argc != SPI_CLK_CMD_ARG_NUM_MAX) {
        Serial.print(F("err: too few arguments "));
        Serial.print(argc);
        return;
    }

    spi_clk = arg_ary[0];

    if ((spi_clk == 8) || (spi_clk == 4) || (spi_clk == 2) || (spi_clk == 1)) {
        char print_buf[64];
        snprintf(print_buf, sizeof(print_buf), \
                 "set Arduino SPI master clock to %luMHz", spi_clk);

        Serial.print(print_buf);

        /* config Arduino SPI master clock */
        SPIUtility::set_spi_clk_div(arduino_clk / spi_clk);

    } else {
        Serial.print(F("err: invalid SPI clock value "));
        Serial.print(spi_clk);
    }
}

void SPIModule::set_trig_mode(bool is_positive)
{
    SPIUtility::write_single32(SPISN_POS_EDGE_SEL, !is_positive);
}

uint8_t SPIModule::get_trig_mode(void)
{
    uint32_t mode = SPIUtility::read_single32(SPISN_POS_EDGE_SEL);

    return (uint8_t)(mode & 1);
}

void SPIModule::reset_base_addr(void)
{
    SPIUtility::write_single32(SPISN_BASE_ADDR, BASE_ADDRESS);
}

void SPIModule::set_base_addr(uint32_t address)
{
    SPIUtility::write_single32(SPISN_BASE_ADDR, address);
}

uint32_t SPIModule::get_base_addr(void)
{
    return SPIUtility::read_single32(SPISN_BASE_ADDR);
}

