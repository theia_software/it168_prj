
#include "serial_command.h"
#include "SPIUtility.h"
#include "SPIModule.h"
#include "common.h"
#include <SPI.h>

#define OP_READ_32              (0x32)
#define OP_WRITE_32             (0x33)
#define OP_READ_8               (0x20)
#define OP_WRITE_8              (0x24)
#define OP_READ_BURST_8         (0x22)
#define OP_WRITE_BURST_8        (0x26)
#define OP_READ_IMAGE_8         (0x50)


void spi_mode_cmd(void)
{
#define SPI_MODE_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[SPI_MODE_CMD_ARG_NUM_MAX];
    U32 argc;
    U32 spi_mode;
    char *arg;


    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= SPI_MODE_CMD_ARG_NUM_MAX) {
            Serial.println(F("err: too many arguments"));
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 10);

#ifdef SERIALCOMMANDDEBUG
        Serial.print(argc);
        Serial.print(" argument ");
        print_hex(arg_ary[argc]);
#endif
        argc += 1;
    }

    if (argc != SPI_MODE_CMD_ARG_NUM_MAX) {
        Serial.print(F("err: too few arguments "));
        Serial.print(argc);
        return;
    }

    spi_mode = arg_ary[0];

    Serial.print(F("set SPI mode "));
    Serial.println(spi_mode, DEC);
    SPIUtility::set_spi_mode(spi_mode);
}

void SPIUtility::setup(void)
{
    SPI.begin();

    SPI.setDataMode(SPI_MODE0);
    SPI.setClockDivider(SPI_CLOCK_DIV4);
}

void SPIUtility::readSingle32(uint32_t address, uint8_t output[4])
{
    unsigned char addr[4] = {0};

    if (TEST_ALIGN_4(address)) {
        Serial.println(F("\r\nerror: address not aligned"));
        return;
    }

    for (int i = 0; i < 4; i++)
        addr[i] = (address >> (24 - (i * 8))) & 0xFF;

    digitalWrite(SS, LOW);
    SPI.transfer(OP_READ_32);
    for (int b = 0; b < 4; b++)
        SPI.transfer(addr[b]);

    for (int b = 0; b < 4; b++)
        output[3 - b] = SPI.transfer(0x00);

    digitalWrite(SS, HIGH);
}

void SPIUtility::writeSingle32(uint32_t address, uint8_t input[4])
{
    unsigned char addr[4] = {0};

    if (TEST_ALIGN_4(address)) {
        Serial.println(F("\r\nerror: address not aligned"));
        return;
    }

    for (int i = 0; i < 4; i++)
        addr[i] = (address >> (24 - (i * 8))) & 0xFF;

    digitalWrite(SS, LOW);
    SPI.transfer(OP_WRITE_32);
    for (int b = 0 ; b < 4; b++)
        SPI.transfer(addr[b]);

    for (int b = 0; b < 4; b++)
        SPI.transfer(input[3 - b]);

    digitalWrite(SS, HIGH);
}

uint32_t SPIUtility::read_single32(uint32_t address)
{
    unsigned char addr[4] = {0};
    unsigned char output[4] = {0};
    uint32_t ret = 0;

    if (TEST_ALIGN_4(address)) {
        Serial.println(F("\r\nerror: address not aligned"));
        return 0;
    }

    for (int i = 0; i < 4; i++)
        addr[i] = (address >> (24 - (i * 8))) & 0xFF;

    digitalWrite(SS, LOW);
    SPI.transfer(OP_READ_32);
    for (int b = 0; b < 4; b++)
        SPI.transfer(addr[b]);

    for (int b = 0; b < 4; b++)
        output[b] = SPI.transfer(0x00);

    digitalWrite(SS, HIGH);

    ret |= ((uint32_t) output[0] << 24);
    ret |= ((uint32_t) output[1] << 16);
    ret |= ((uint32_t) output[2] << 8);
    ret |= ((uint32_t) output[3] << 0);
    return ret;
}

void SPIUtility::write_single32(uint32_t address, uint32_t value)
{
    unsigned char addr[4] = {0};
    unsigned char input[4] = {0};

    if (TEST_ALIGN_4(address)) {
        Serial.println(F("\r\nerror: address not aligned"));
        return;
    }

    for (int i = 0; i < 4; i++)
        addr[i] = (address >> (24 - (i * 8))) & 0xFF;

    for (int i = 0; i < 4; i++)
        input[i] = (value >> (24 - (i * 8))) & 0xFF;

    digitalWrite(SS, LOW);
    SPI.transfer(OP_WRITE_32);
    for (int b = 0; b < 4; b++)
        SPI.transfer(addr[b]);

    for (int b = 0; b < 4; b++)
        SPI.transfer(input[b]);

    digitalWrite(SS, HIGH);
}

void SPIUtility::readSingle8(uint8_t address_offset, uint8_t *output)
{
    digitalWrite(SS, LOW);
    SPI.transfer(OP_READ_8);
    SPI.transfer(address_offset);
    *output = SPI.transfer(0x00);
    digitalWrite(SS, HIGH);
}

void SPIUtility::writeSingle8(uint8_t address_offset, uint8_t *input)
{
    digitalWrite(SS, LOW);
    SPI.transfer(OP_WRITE_8);
    SPI.transfer(address_offset);
    SPI.transfer(input);
    digitalWrite(SS, HIGH);
}

uint8_t SPIUtility::read_single8(uint8_t address_offset)
{
    uint8_t output;

    digitalWrite(SS, LOW);
    SPI.transfer(OP_READ_8);
    SPI.transfer(address_offset);
    output = SPI.transfer(0x00);
    digitalWrite(SS, HIGH);

    return output;
}

void SPIUtility::write_single8(uint8_t address_offset, uint8_t input)
{
    digitalWrite(SS, LOW);
    SPI.transfer(OP_WRITE_8);
    SPI.transfer(address_offset);
    SPI.transfer(input);
    digitalWrite(SS, HIGH);
}

void SPIUtility::read_burst8(uint8_t address_offset, uint8_t *output, uint32_t output_size)
{
    digitalWrite(SS, LOW);
    SPI.transfer(OP_READ_BURST_8);
    SPI.transfer(address_offset);
    for (uint32_t b = 0; b < output_size; b++)
        output[b] = SPI.transfer(0x00);

    digitalWrite(SS, HIGH);
}

void SPIUtility::write_burst8(uint8_t address_offset, uint8_t *input, uint32_t input_size)
{
    digitalWrite(SS, LOW);
    SPI.transfer (OP_WRITE_BURST_8);
    SPI.transfer( address_offset );
    for (uint32_t b = 0; b < input_size; b++)
         SPI.transfer(input[b]);

    digitalWrite(SS, HIGH);
}

void SPIUtility::read_img(uint8_t *output, uint32_t output_size)
{
    digitalWrite(SS, LOW);
    SPI.transfer (OP_READ_IMAGE_8);
    for (uint32_t b = 0; b < output_size; b++)
        output[b] = SPI.transfer(0x00);

    digitalWrite(SS, HIGH);
}


void SPIUtility::set_spi_mode(uint8_t mode)
{
    const bool positive = 1;
    const bool negative = !positive;

    switch (mode) {
        case 0:
            SPIModule::set_trig_mode(positive);
            SPI.setDataMode(SPI_MODE0);
            break;
        case 1:
            SPIModule::set_trig_mode(negative);
            SPI.setDataMode(SPI_MODE1);
            break;
        case 2:
            SPIModule::set_trig_mode(negative);
            SPI.setDataMode(SPI_MODE2);
            break;
        case 3:
            SPIModule::set_trig_mode(positive);
            SPI.setDataMode(SPI_MODE3);
            break;

        default :
            Serial.print("[Error]<set_spi_mode> does not support mode : ");
            Serial.println(mode);
    }
}

void SPIUtility::set_spi_clk_div(uint8_t clock_divider)
{
    switch (clock_divider) {
      case 2:
          SPI.setClockDivider(SPI_CLOCK_DIV2);
          break;
      case 4:
          SPI.setClockDivider(SPI_CLOCK_DIV4);
          break;
      case 8:
          SPI.setClockDivider(SPI_CLOCK_DIV8);
          break;
      case 16:
          SPI.setClockDivider(SPI_CLOCK_DIV16);
          break;
      case 32:
          SPI.setClockDivider(SPI_CLOCK_DIV32);
          break;
      case 64:
          SPI.setClockDivider(SPI_CLOCK_DIV64);
          break;
      case 128:
          SPI.setClockDivider(SPI_CLOCK_DIV128);
          break;
      default:
          Serial.print("[Error]<set_spi_clk_div> does not support clock_divider: ");
          Serial.println(clock_divider);
    }
}

void SPIUtility::read_burst8_speed(uint8_t dmy, uint8_t address_offset, \
                                   uint8_t *output, uint32_t output_size)
{
    uint8_t dmy_read;

    digitalWrite(SS, LOW);
    SPI.transfer(OP_READ_BURST_8);
    SPI.transfer(address_offset);

    /* dummy byte */
    for (uint8_t i = 0; i < dmy; ++i)
        dmy_read = SPI.transfer(0);

    for (uint32_t b = 0; b < output_size; b++)
        output[b] = SPI.transfer(0);

    digitalWrite(SS, HIGH);
}

uint8_t SPIUtility::read_single8_speed(uint8_t dmy, uint8_t address_offset)
{
    uint8_t output;

    digitalWrite(SS, LOW);
    SPI.transfer(OP_READ_8);
    SPI.transfer(address_offset);

    /* dummy byte */
    for (uint8_t i = 0; i < dmy; ++i)
        output = SPI.transfer(0);

    output = SPI.transfer(0);
    digitalWrite(SS, HIGH);

    return output;
}

uint32_t SPIUtility::read_single32_speed(uint8_t dmy, uint32_t address)
{
    unsigned char addr[4] = {0};
    unsigned char output[4] = {0};
    unsigned char idx;
    uint32_t ret = 0;

    if (TEST_ALIGN_4(address)) {
        Serial.println(F("\r\nerror: address not aligned"));
        return 0;
    }

    for (idx = 0; idx < 4; idx++)
        addr[idx] = (address >> (24 - (idx * 8))) & 0xFF;

    digitalWrite(SS, LOW);
    SPI.transfer(OP_READ_32);
    for (idx = 0; idx < 4; idx++)
        SPI.transfer(addr[idx]);

    /* dummy byte */
    for (uint8_t i = 0; i < dmy; ++i)
        idx = SPI.transfer(0);

    for (idx = 0; idx < 4; idx++)
        output[idx] = SPI.transfer(0);

    digitalWrite(SS, HIGH);

    ret |= ((uint32_t) output[0] << 24);
    ret |= ((uint32_t) output[1] << 16);
    ret |= ((uint32_t) output[2] << 8);
    ret |= ((uint32_t) output[3] << 0);
    return ret;
}
