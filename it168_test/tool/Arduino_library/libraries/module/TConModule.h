
#ifndef _TCON_MODULE_H_
#define _TCON_MODULE_H_

#include <stdint.h>

#define TCON_GEN_DATE                       (0x30000000)
#define TCON_GEN_VER                        (0x30000004)
#define TCON_READ_EN                        (0x30000008)
#define TCON_DONE_CLEAR                     (0x30000008)
#define TCON_FORCE_STOP                     (0x3000000c)
#define TCON_SYNC_MODE                      (0x30000010)
#define TCON_CURRENT_STATE                  (0x30000014)
#define TCON_DUTY_CLK                       (0x30000018)
#define TCON_DONE                           (0x3000001c)

#define TCON_START_ADDR                     (0x30000020)
#define TCON_END_ADDR                       (0x30000024)
#define TCON_REPEAT_TIMES                   (0x30000028)
#define TCON_CLK_DIVIDE                     (0x30000044)
#define TCON_CURRENT_REPEAT_TIMES           (0x30000048)
#define TCON_OUTPUT_PIN_VALUE               (0x3000004c)
#define TCON_DUMMY_01                       (0x30000050)
#define TCON_DUMMY_02                       (0x30000054)
#define TCON_REGION_SIZE                    (0x300000c8)
#define TCON_SENSOR_TCON_INST_SEL           (0x300000cc)
#define TCON_SENSOR_SEL_TCON_PIN_SEL        (0x300000d0)
#define TCON_SENSOR_OUTPUT_VALUE            (0x30000150)

#define TCON_CURRENT_REGION_INDEX           (0x30000154)
#define TCON_GLOBAL_REPEAT_TIMES            (0x30000160)
#define TCON_GLOBAL_REPEAT_REGION           (0x30000164)
#define TCON_CURRENT_GLOBAL_REPEAT_COUNT    (0x30000168)
#define TCON_STEP_MODE_SEL                  (0x30000174)
#define TCON_STEP_TRIGGER                   (0x30000178)
#define TCON_FMC_OUTPUT_SEL                 (0x3000017c)
#define TCON_FMC_OUTPUT_PIN_VALUE           (0x30000180)

#define TCON_COUNT                          (3)
#define TCON_REGION                         (3)

class TConModule
{
  public:
    static void triggerStartAndCleanDoneAll();
    static void triggerStart(int index);
    static void triggerCleanDone(int index);
    static void triggerForceStop(int index);

    static void setSyncMode(int mode, int index);
    static int getSyncMode(int index);

    static int getCurrentState(int index);
    static int getDutyClock (int index);
    static int getDoneSignal(int index);

    static void setStartAddress(unsigned long address, int index , int region);
    static unsigned long getStartAddress(int index , int region);

    static void setEndAddress(unsigned long address, int index , int region);
    static unsigned long getEndAddress(int index , int region);

    static void setRepeatTimes(unsigned long times, int index , int region);
    static unsigned long getRepeatTimes(int index , int region);

    static void setDividedClock(int clk, int index);
    static unsigned int  getDividedClock(int index);

    static unsigned long getCurrentRepeatTimes(int index);
    static void getOutputPinValue(int index, unsigned char* output);

    static void setRegionSize(int r_size);
    static int getRegionSize();
    static void setOutputRegisterForSensor(int register_index);
    static int getOutputRegisterForSensor();
    static void setOutputPin(int pin, unsigned char input);
    static unsigned char getOutputPin(int pin);
    static void getSensorOutputValue(unsigned char *output_data);

    static uint8_t getCurrentRegion(uint8_t index);

    static void setGlobalRepeatTimes(uint32_t times);
    static uint32_t getGlobalRepeatTimes();
    static void setGlobalRepeatRegion(uint8_t enableRegion);
    static uint8_t getGlobalRepeatRegion();

    static uint32_t getCurrentGlobalRepeatCount(int index);

    static void setStepModeSel(uint8_t mode);
    static uint8_t getStepModeSel();

    static void triggerStepMode();

    static void setFMCOutputSel(uint8_t index);
    static uint8_t getFMCOutputSel();

    static void getFMCOutputPinValue(uint8_t *output_data);

    //Tool
    static uint32_t calTotalRunTimes(int index, int region);
};
#endif

