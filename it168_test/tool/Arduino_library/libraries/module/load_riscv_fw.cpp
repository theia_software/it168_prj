#include <Arduino.h>
#include "SPIUtility.h"
#include "common.h"


U32 load_fw_addr;

void setRiscvRam(unsigned int len, byte pgm_addr[][4])
{
    U32 wdata, rdata;


    print_str_hex("write RISCV FW start addr\t", load_fw_addr);

    for (U32 j = 0; j < len; ++j) {

        for (U32 i = 0; i < 4; ++i) {
            *((U8 *)(&wdata) + i) = pgm_read_byte_near(&pgm_addr[j][i]);
        }

        SPIUtility::write_single32(load_fw_addr, wdata);

        rdata = SPIUtility::read_single32(load_fw_addr);

        if (wdata != rdata) {
            Serial.println(F("write RISCV FW to SRAM fail"));
            print_str_hex("addr", load_fw_addr);
            print_str_hex("wdata", wdata);
            print_str_hex("rdata", rdata);
            while (1);
        }
        load_fw_addr = load_fw_addr + 4;
    }

    print_str_hex("write RISCV FW end addr\t\t", (load_fw_addr - 1));
}
