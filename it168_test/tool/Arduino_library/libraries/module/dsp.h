/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: dsp.h
 */

#ifndef _DSP_H_
#define _DSP_H_



/* Definition */
#define DSP_REG_BASE_ADDR                   (0x50000000UL)

/* Register */
#define DSP_GEN_DATE                        (0x50000000UL)
#define DSP_GEN_VER                         (0x50000004UL)
#define DSP_START_TRIG                      (0x50000008UL)
#define DSP_INST_START_ADDR                 (0x5000000cUL)
#define DSP_INST_END_ADDR                   (0x50000010UL)
#define DSP_COEF_80                         (0x50000014UL)
#define DSP_COEF_81                         (0x50000018UL)
#define DSP_COEF_82                         (0x5000001cUL)
#define DSP_COEF_83                         (0x50000020UL)
#define DSP_COEF_84                         (0x50000024UL)
#define DSP_COEF_85                         (0x50000028UL)
#define DSP_COEF_86                         (0x5000002cUL)
#define DSP_COEF_87                         (0x50000030UL)
#define DSP_COEF_88                         (0x50000034UL)
#define DSP_COEF_70                         (0x50000038UL)
#define DSP_COEF_71                         (0x5000003cUL)
#define DSP_COEF_72                         (0x50000040UL)
#define DSP_COEF_73                         (0x50000044UL)
#define DSP_COEF_74                         (0x50000048UL)
#define DSP_COEF_75                         (0x5000004cUL)
#define DSP_COEF_76                         (0x50000050UL)
#define DSP_COEF_77                         (0x50000054UL)
#define DSP_COEF_78                         (0x50000058UL)
#define DSP_COEF_60                         (0x5000005cUL)
#define DSP_COEF_61                         (0x50000060UL)
#define DSP_COEF_62                         (0x50000064UL)
#define DSP_COEF_63                         (0x50000068UL)
#define DSP_COEF_64                         (0x5000006cUL)
#define DSP_COEF_65                         (0x50000070UL)
#define DSP_COEF_66                         (0x50000074UL)
#define DSP_COEF_67                         (0x50000078UL)
#define DSP_COEF_68                         (0x5000007cUL)
#define DSP_COEF_50                         (0x50000080UL)
#define DSP_COEF_51                         (0x50000084UL)
#define DSP_COEF_52                         (0x50000088UL)
#define DSP_COEF_53                         (0x5000008cUL)
#define DSP_COEF_54                         (0x50000090UL)
#define DSP_COEF_55                         (0x50000094UL)
#define DSP_COEF_56                         (0x50000098UL)
#define DSP_COEF_57                         (0x5000009cUL)
#define DSP_COEF_58                         (0x500000a0UL)
#define DSP_COEF_40                         (0x500000a4UL)
#define DSP_COEF_41                         (0x500000a8UL)
#define DSP_COEF_42                         (0x500000acUL)
#define DSP_COEF_43                         (0x500000b0UL)
#define DSP_COEF_44                         (0x500000b4UL)
#define DSP_COEF_45                         (0x500000b8UL)
#define DSP_COEF_46                         (0x500000bcUL)
#define DSP_COEF_47                         (0x500000c0UL)
#define DSP_COEF_48                         (0x500000c4UL)
#define DSP_COEF_30                         (0x500000c8UL)
#define DSP_COEF_31                         (0x500000ccUL)
#define DSP_COEF_32                         (0x500000d0UL)
#define DSP_COEF_33                         (0x500000d4UL)
#define DSP_COEF_34                         (0x500000d8UL)
#define DSP_COEF_35                         (0x500000dcUL)
#define DSP_COEF_36                         (0x500000e0UL)
#define DSP_COEF_37                         (0x500000e4UL)
#define DSP_COEF_38                         (0x500000e8UL)
#define DSP_COEF_20                         (0x500000ecUL)
#define DSP_COEF_21                         (0x500000f0UL)
#define DSP_COEF_22                         (0x500000f4UL)
#define DSP_COEF_23                         (0x500000f8UL)
#define DSP_COEF_24                         (0x500000fcUL)
#define DSP_COEF_25                         (0x50000100UL)
#define DSP_COEF_26                         (0x50000104UL)
#define DSP_COEF_27                         (0x50000108UL)
#define DSP_COEF_28                         (0x5000010cUL)
#define DSP_COEF_10                         (0x50000110UL)
#define DSP_COEF_11                         (0x50000114UL)
#define DSP_COEF_12                         (0x50000118UL)
#define DSP_COEF_13                         (0x5000011cUL)
#define DSP_COEF_14                         (0x50000120UL)
#define DSP_COEF_15                         (0x50000124UL)
#define DSP_COEF_16                         (0x50000128UL)
#define DSP_COEF_17                         (0x5000012cUL)
#define DSP_COEF_18                         (0x50000130UL)
#define DSP_COEF_00                         (0x50000134UL)
#define DSP_COEF_01                         (0x50000138UL)
#define DSP_COEF_02                         (0x5000013cUL)
#define DSP_COEF_03                         (0x50000140UL)
#define DSP_COEF_04                         (0x50000144UL)
#define DSP_COEF_05                         (0x50000148UL)
#define DSP_COEF_06                         (0x5000014cUL)
#define DSP_COEF_07                         (0x50000150UL)
#define DSP_COEF_08                         (0x50000154UL)
#define DSP_HALF_IMAGE_WIDTH                (0x50000158UL)
#define DSP_DONE                            (0x5000015cUL)
#define DSP_DONE_CLEAR                      (0x50000160UL)
#define DSP_DUMMY_01                        (0x50000164UL)
#define DSP_DUMMY_02                        (0x50000168UL)
#define T_RWM                               (0x5000016cUL)


/* DSP OP code */
typedef enum {
    DSP_OP_SET_ADDR1_BASE = 0x01,
    DSP_OP_SET_ADDR2_BASE,
    DSP_OP_SET_OUTPUT_BASE,
    DSP_OP_SET_CONST1,
    DSP_OP_SET_CONST2,
    DSP_OP_SET_DATA_SHIFT,
    DSP_OP_SET_8BIT_OUTPUT,
    DSP_OP_SET_MATRIX_IN_OUT,
    DSP_OP_ADD_16_16 = 0x10,
    DSP_OP_ADD_16_8,
    DSP_OP_ADD_16_C1,
    DSP_OP_ADD_16_C2,
    DSP_OP_SUB_16_16 = 0x30,
    DSP_OP_SUB_16_8,
    DSP_OP_SUB_16_C1,
    DSP_OP_SUB_16_C2,
    DSP_OP_MUL_16_16 = 0x50,
    DSP_OP_MUL_16_8,
    DSP_OP_MUL_16_C1,
    DSP_OP_MUL_16_C2,
    DSP_OP_MAC_16_C1_C2 = 0x72,
    DSP_OP_LOAD_COEF = 0xb2,
    DSP_OP_CONVOLUTION,
    DSP_OP_LOAD_LUT = 0x92,
    DSP_OP_LUT,
    DSP_OP_MATRIX = 0xd2,
    DSP_OP_HD = 0x90,
    DSP_OP_DMUL_8_8 = 0xf0,
    DSP_OP_1D_CONV = 0xb6,

} DSP_OP_CODE;

/* DSP error code */
enum {
    DSP_ERR_OP_CODE             = (1UL << 0), // op code error
    DSP_ERR_PARAM               = (1UL << 1), // parameter error
    DSP_ERR_CAL_TIME            = (1UL << 2), // 8bit operation should align 4B
    DSP_ERR_SHIFT_BIT           = (1UL << 3), // shift value out of 4 bits range
    DSP_ERR_INST_OUT_OF_RAM     = (1UL << 4), // running out of instruction RAM
    DSP_ERR_RANGE_OUT_OF_SRAM   = (1UL << 5), // address range(addr + cal_time) can't exceed SRAM
    DSP_ERR_DONE_TIMEOUT        = (1UL << 6), // check DSP done fail, timeout
    DSP_ERR_COEF_NUM            = (1UL << 7), // coefficient num error
    DSP_ERR_MATRIX_IN_OUT       = (1UL << 8), // matrix in out num error
};

/* DSP verification status flag */
enum {
    DSP_VER_STA_SYNC            = (1UL << 31),
    DSP_VER_STA_START           = (1UL << 30),
    DSP_VER_STA_START_ACK       = (1UL << 29),

    DSP_VER_RES_PASS            = 0,
    DSP_VER_RES_FAIL            = (1UL << 0),
    DSP_VER_SET_ERR_ADDR        = (1UL << 1), // dsp_ver_set_err_addr fail
    DSP_VER_SET_PARAM_ONLY      = (1UL << 2), // dsp_ver_set_param_only fail
    DSP_VER_ALU                 = (1UL << 3), // dsp_ver_alu fail
    DSP_VER_CONV                = (1UL << 4), // dsp_ver_conv fail
    DSP_VER_LUT                 = (1UL << 5), // dsp_ver_lut fail
    DSP_VER_MATRIX              = (1UL << 6), // dsp_ver_matrix fail
    DSP_VER_REG_DEF             = (1UL << 7), // default value of register fail
    DSP_VER_REG_ACCESS          = (1UL << 8), // register access fail
    DSP_VER_CLOCK               = (1UL << 9), // clock fail
};

#endif


