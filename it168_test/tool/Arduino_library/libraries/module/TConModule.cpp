#include "TConModule.h"
#include "SPIUtility.h"
#include "SPIModule.h"


void print_array2( char* title, byte *arr, int len) {
  // todo
  Serial.print("=== [ ") ;
  Serial.print( title ) ;
  Serial.println("] ===") ;

  for ( int j = 0 ; j < len ; j++ ) {
    Serial.print( arr[j], HEX) ; Serial.print(",") ;
    if ( (j+1) % 16 == 0 ) {
      Serial.println("") ;
    }
  }
  Serial.println("") ;
};

void TConModule::triggerStartAndCleanDoneAll()
{
    unsigned char ret[4] = {0x00,0x00,0x00,0x00};

    ret[0] = ret[0] | 0x003f; //0b0000000000111111

    SPIUtility::writeSingle32(TCON_READ_EN, ret);
}

void TConModule::triggerStart(int index)
{
    if(index < 1 || index > TCON_COUNT)
    {
        Serial.print("[Error]<triggerStart> Not yet Support index :") ;
        Serial.println(index) ;
    }
    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_READ_EN, ret);

    //0:0000_0001 , 2:0000_0100, 4:0001_0000
    unsigned char mask = 1 << (2*(index - 1));
    ret[0] = ret[0] | mask;

    SPIUtility::writeSingle32(TCON_READ_EN, ret);
    return;
}

void TConModule::triggerCleanDone(int index)
{
    if(index < 1 || index > TCON_COUNT)
    {
        Serial.print("[Error]<triggerCleanDone> Not yet Support index : ") ;
        Serial.println(index) ;
    }

    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_DONE_CLEAR, ret);

    //1:0000_0010 , 3:0000_1000, 5:0010_0000
    unsigned char mask = 1 << (2*(index) - 1);
    ret[0] = ret[0] | mask;

    SPIUtility::writeSingle32(TCON_DONE_CLEAR, ret);
    return;
}

void TConModule::triggerForceStop(int index)
{
    if(index < 1 || index > TCON_COUNT)
    {
        Serial.print("[Error]<triggerForceStop> Not yet Support index : ") ;
        Serial.println(index) ;
    }

    unsigned char ret[4]  = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_FORCE_STOP, ret);

    //0:0000_0001 , 1:0000_0010, 2:0000_0100
    unsigned char mask = 1 << (index - 1);
    ret[0] = ret[0] | mask;

    SPIUtility::writeSingle32(TCON_FORCE_STOP, ret);
    return;
}


void TConModule::setSyncMode(int mode, int index)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<setSyncMode> Not yet Support index : ") ;
      Serial.println(index);
    }
    if(mode < 0 || mode > 1)
    {
      Serial.print("[Error]<setSyncMode> Not yet Support mode : ") ;
      Serial.println(mode);
    }

    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_SYNC_MODE, ret);

    //0:0000_0001 , 1:0000_0010, 2:0000_0100
    unsigned char mask = ~(1 << (index - 1));
    ret[0] = ret[0] & mask | (mode << index-1);
    SPIUtility::writeSingle32(TCON_SYNC_MODE, ret);
    return;
}

int TConModule::getSyncMode(int index)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<getSyncMode> Not yet Support index : ") ;
      Serial.println(index);
    }

    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_SYNC_MODE, ret);

    //0:0000_0001 , 1:0000_0010, 2:0000_0100
    unsigned char mask = 1 << (index - 1);
    ret[0] = ret[0] & mask;
    int mode = ret[0] >> (index - 1);
    return mode;
}


int TConModule::getCurrentState(int index)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<getCurrentState> Not yet Support index : ") ;
      Serial.println(index);
    }

    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_CURRENT_STATE, ret);

    //0000_0011, 0000_1100, 0011_0000
    unsigned char mask = 0b00000011 << 2*(index - 1);
    int c_status = (ret[0] & mask) >> 2*(index - 1);
    return c_status;
}

int TConModule::getDutyClock (int index)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<getDutyClock> Not yet Support index : ") ;
      Serial.println(index);
    }

    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_DUTY_CLK, ret);

    //0000_0001, 1:0000_0010, 2:0000_0100
    unsigned char mask = 0b00000001 << (index - 1);
    int clk = (ret[0] & mask) >> (index - 1);
    return clk;
}

int TConModule::getDoneSignal(int index)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<getDoneSignal> Not yet Support index : ") ;
      Serial.println(index);
    }

    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_DONE, ret);

    //0000_0001, 1:0000_0010, 2:0000_0100
    unsigned char mask = 0b00000001 << (index - 1);
    int sign = (ret[0] & mask) >> (index - 1);
    return sign;
}


void TConModule::setStartAddress(unsigned long address, int index , int region)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<setStartAddress> Not yet Support index : ") ;
      Serial.println(index);
    }
    if(region < 1 || region > TCON_REGION)
    {
      Serial.print("[Error]<setStartAddress> Not yet Support region : ") ;
      Serial.println(region);
    }
    if(address < BASE_ADDRESS || address > BASE_ADDRESS_LIMIT)
    {
      Serial.print("[Error]<setStartAddress> address is out of boundary : ") ;
      Serial.println(address, HEX);
    }

    unsigned long  new_register_address = TCON_START_ADDR + (index-1) * 0x38 + (region-1) * 0x0c;
    unsigned char ret[4] = {0x00};
    for (int i = 0; i < 4; i++) {
        ret[i] = (address >> (i*8)) & 0xFF;
    }
    SPIUtility::writeSingle32(new_register_address, ret);
    return;
}

static unsigned long TConModule::getStartAddress(int index , int region)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<getStartAddress> Not yet Support index : ") ;
      Serial.println(index);
    }
    if(region < 1 || region > TCON_REGION)
    {
      Serial.print("[Error]<getStartAddress> Not yet Support region : ") ;
      Serial.println(region);
    }

    unsigned long  new_register_address = TCON_START_ADDR + (index-1) * 0x38 + (region-1) * 0x0c;
    unsigned char ret[4] = {0x00};
    SPIUtility::readSingle32(new_register_address, ret);

    unsigned long addr = 0 ;
    addr |=  ((unsigned long) ret[3] << 24) ;
    addr |=  ((unsigned long) ret[2] << 16) ;
    addr |=  (ret[1] << 8) ;
    addr |=  (ret[0] << 0) ;

    return addr;
}

void TConModule::setEndAddress(unsigned long address, int index , int region)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<setEndAddress> Not yet Support index : ") ;
      Serial.println(index);
    }
    if(region < 1 || region > TCON_REGION)
    {
      Serial.print("[Error]<setEndAddress> Not yet Support region : ") ;
      Serial.println(region);
    }
    if(address < BASE_ADDRESS || address > BASE_ADDRESS_LIMIT)
    {
      Serial.print("[Error]<setEndAddress> address is out of boundary : ") ;
      Serial.println(address, HEX);
    }

    unsigned long new_register_address = TCON_END_ADDR + (index-1) * 0x38 + (region-1) * 0x0c;
    unsigned char ret[4] = {0x00};
    for (int i = 0; i < 4; i++) {
        ret[i] = (address >> (i*8)) & 0xFF;
    }
    SPIUtility::writeSingle32(new_register_address, ret);
    return;
}

unsigned long TConModule::getEndAddress(int index , int region)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<getEndAddress> Not yet Support index : ") ;
      Serial.println(index);
    }
    if(region < 1 || region > TCON_REGION)
    {
      Serial.print("[Error]<getEndAddress> Not yet Support region : ") ;
      Serial.println(region);
    }

    unsigned long new_register_address = TCON_END_ADDR + (index-1) * 0x38 + (region-1) * 0x0c;
    unsigned char ret[4] = {0x00};
    SPIUtility::readSingle32(new_register_address, ret);

    unsigned long addr = 0 ;
    addr |=  ((unsigned long) ret[3] << 24) ;
    addr |=  ((unsigned long) ret[2] << 16) ;
    addr |=  (ret[1] << 8) ;
    addr |=  (ret[0] << 0) ;
    return addr;
}


void TConModule::setRepeatTimes(unsigned long times, int index , int region)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<setRepeatTimes> Not yet Support index : ") ;
      Serial.println(index);
    }
    if(region < 1 || region > TCON_REGION)
    {
      Serial.print("[Error]<setRepeatTimes> Not yet Support region : ") ;
      Serial.println(region);
    }

    unsigned long new_register_address = TCON_REPEAT_TIMES + (index-1) * 0x38 + (region-1) * 0x0c;
    unsigned char ret[4] = {0x00};
    for (int i = 0; i < 4; i++) {
        ret[i] = (times >> (i*8)) & 0xFF;
    }
    SPIUtility::writeSingle32(new_register_address, ret);
    return;
}

unsigned long TConModule::getRepeatTimes(int index , int region)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<getRepeatTimes> Not yet Support index : ") ;
      Serial.println(index);
    }
    if(region < 1 || region > TCON_REGION)
    {
      Serial.print("[Error]<getRepeatTimes> Not yet Support region : ") ;
      Serial.println(region);
    }

    unsigned long new_register_address = TCON_REPEAT_TIMES + (index-1) * 0x38 + (region-1) * 0x0c;
    unsigned char ret[4] = {0x00};
    SPIUtility::readSingle32(new_register_address, ret);

    unsigned long times = 0 ;
    times |=  ((long) ret[3] << 24) ;
    times |=  ((long) ret[2] << 16) ;
    times |=  (ret[1] << 8) ;
    times |=  (ret[0] << 0) ;
    return times;
}


void TConModule::setDividedClock(int clk, int index)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<setDividedClock> Not yet Support index : ") ;
      Serial.println(index);
    }

    unsigned long new_register_address = TCON_CLK_DIVIDE + (index-1) * 0x38;
    unsigned char data[4] = {0x00};
    for (int i = 0; i < 2; i++) {
        data[i] = (clk >> (i*8)) & 0xFF;
    }

    unsigned char ret[4] = {0x00};
    SPIUtility::readSingle32(new_register_address, ret);
    ret[0] = data[0];
    ret[1] = data[1];
    SPIUtility::writeSingle32(new_register_address, ret);
    return;
}

unsigned int TConModule::getDividedClock(int index)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<getDividedClock> Not yet Support index : ") ;
      Serial.println(index);
    }

    unsigned long new_register_address = TCON_CLK_DIVIDE + (index-1) * 0x38;
    unsigned char ret[4] = {0x00};
    SPIUtility::readSingle32(new_register_address, ret);

    unsigned int clk = 0 ;
    clk |=  (ret[1] << 8) ;
    clk |=  (ret[0] << 0) ;
    return clk;
}


unsigned long TConModule::getCurrentRepeatTimes(int index)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<getCurrentRepeatTimes> Not yet Support index : ") ;
      Serial.println(index);
    }

    unsigned long new_register_address = TCON_CURRENT_REPEAT_TIMES + (index-1) * 0x38;
    unsigned char ret[4] = {};
    SPIUtility::readSingle32(new_register_address, ret);
    unsigned long times = 0 ;
    times |=  ((unsigned long) ret[3] << 24) ;
    times |=  ((unsigned long) ret[2] << 16) ;
    times |=  (ret[1] << 8) ;
    times |=  (ret[0] << 0);
    return times;
}

void TConModule::getOutputPinValue(int index , unsigned char* output)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<getOutputPinValue> Not yet Support index : ") ;
      Serial.println(index);
    }

    unsigned long new_register_address = TCON_OUTPUT_PIN_VALUE + (index-1) * 0x38;
    SPIUtility::readSingle32(new_register_address, output);
    return;
}

void TConModule::setRegionSize(int r_size)
{
    if(r_size < 0 || r_size > TCON_COUNT)
    {
      Serial.print("[Error]<setRegionSize> Not yet Support r_size : ") ;
      Serial.println(r_size);
    }

    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_REGION_SIZE, ret);

    //0000_0011
    unsigned char mask = ~(0b00000011);
    ret[0] = ret[0] & mask | r_size;
    SPIUtility::writeSingle32(TCON_REGION_SIZE, ret);
    return;
}

int TConModule::getRegionSize()
{
    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_REGION_SIZE, ret);

    //0000_0011
    unsigned char mask = 0b00000011;
    int r_size = ret[0] & mask;
    return r_size;
}

void TConModule::setOutputRegisterForSensor(int register_index)
{
    if(register_index < 0 || register_index > TCON_COUNT)
    {
      Serial.print("[Error]<setOutputRegisterForSensor> Not yet Support register_index : ") ;
      Serial.println(register_index);
    }

    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_SENSOR_TCON_INST_SEL, ret);

    //0000_0011
    unsigned char mask = ~(0b00000011);
    ret[0] = ret[0] & mask | (register_index-1);
    SPIUtility::writeSingle32(TCON_SENSOR_TCON_INST_SEL, ret);
    return;
}

int TConModule::getOutputRegisterForSensor()
{
    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_SENSOR_TCON_INST_SEL, ret);

    //0000_0011
    unsigned char mask = 0b00000011;
    int register_index = ret[0] & mask;
    return register_index+1;
}


void TConModule::setOutputPin(int pin, unsigned char input)
{
    if(pin < 0 || pin > 31)
    {
      Serial.print("[Error]<setOutputPin> Not yet Support pin : ") ;
      Serial.println(pin);
    }

    unsigned long new_register_address = TCON_SENSOR_SEL_TCON_PIN_SEL + pin * 4;
    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(new_register_address, ret);

    //0001_1111
    unsigned char mask = ~(0b00011111);
    ret[0] = ret[0] & mask  | input;
    SPIUtility::writeSingle32(new_register_address, ret);
    return;
}

unsigned char TConModule::getOutputPin(int pin)
{
    if(pin < 0 || pin > 31)
    {
      Serial.print("[Error]<getOutputPin> Not yet Support pin : ") ;
      Serial.println(pin);
    }

    unsigned long new_register_address = TCON_SENSOR_SEL_TCON_PIN_SEL + pin * 4;
    unsigned char ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(new_register_address, ret);

    unsigned char mask = 0b00011111;
    unsigned char output = ret[0] & mask;
    return output;
}

void TConModule::getSensorOutputValue(unsigned char *output_data)
{
    SPIUtility::readSingle32(TCON_SENSOR_OUTPUT_VALUE, output_data);
    return;
}

uint8_t TConModule::getCurrentRegion(uint8_t index)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<getCurrentRegion> Not yet Support index : ") ;
      Serial.println(index);
    }

    uint8_t ret[4] = {0x00,0x00,0x00,0x00};
    SPIUtility::readSingle32(TCON_CURRENT_REGION_INDEX + 4*(index - 1), ret);

    //0000_0011
    uint8_t mask = 0b00000011;
    uint8_t c_region = (ret[0] & mask) + 1;
    return c_region;
}

void TConModule::setGlobalRepeatTimes(uint32_t times)
{
    SPIUtility::write_single32(TCON_GLOBAL_REPEAT_TIMES, times);
}

uint32_t TConModule::getGlobalRepeatTimes(void)
{
    return SPIUtility::read_single32(TCON_GLOBAL_REPEAT_TIMES);
}

void TConModule::setGlobalRepeatRegion(uint8_t enableRegion)
{
  uint8_t regions = enableRegion & 0b111;
  uint8_t ret[4] = {regions, 0x00,0x00,0x00};
  SPIUtility::writeSingle32(TCON_GLOBAL_REPEAT_REGION, ret);
  return;
}

uint8_t TConModule::getGlobalRepeatRegion()
{
    uint8_t ret[4] = {0};
    SPIUtility::readSingle32(TCON_GLOBAL_REPEAT_REGION, ret);
    return ret[0] & 0b111;
}

uint32_t TConModule::getCurrentGlobalRepeatCount(int index)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<getCurrentGlobalRepeatCount> Not yet Support index : ") ;
      Serial.println(index);
    }

    return SPIUtility::read_single32(TCON_CURRENT_GLOBAL_REPEAT_COUNT + 4*(index - 1));
}

void TConModule::setStepModeSel(uint8_t mode)
{
    if( mode > 1)
    {
      Serial.print("[Error]<setStepModeSel> Not yet Support mode : ") ;
      Serial.println(mode);
    }

    uint8_t ret[4] = {mode & 0b1, 0x00,0x00,0x00};
    SPIUtility::writeSingle32(TCON_STEP_MODE_SEL, ret);
    return;
}

uint8_t TConModule::getStepModeSel()
{
    uint8_t ret[4] = {0};
    SPIUtility::readSingle32(TCON_STEP_MODE_SEL, ret);
    return ret[0] & 0b1;
}

void TConModule::triggerStepMode()
{
    uint8_t ret[4] = { 0x01,0x00,0x00,0x00};
    SPIUtility::writeSingle32(TCON_STEP_TRIGGER, ret);
    return;
}

void TConModule::setFMCOutputSel(uint8_t index)
{
    if(index < 1 || index > TCON_COUNT)
    {
      Serial.print("[Error]<setFMCOutputSel> Not yet Support index : ") ;
      Serial.println(index);
    }

    uint8_t i = (index-1) & 0b11;
    uint8_t ret[4] = {i, 0x00,0x00,0x00};
    SPIUtility::writeSingle32(TCON_FMC_OUTPUT_SEL, ret);
    return;
}


uint8_t TConModule::getFMCOutputSel()
{
    uint8_t ret[4] = {0};
    SPIUtility::readSingle32(TCON_FMC_OUTPUT_SEL, ret);
    return (ret[0] & 0b11) + 1;
}

void TConModule::getFMCOutputPinValue(uint8_t *output_data)
{
    SPIUtility::readSingle32(TCON_FMC_OUTPUT_PIN_VALUE, output_data);
    return;
}

//Tool
uint32_t TConModule::calTotalRunTimes(int index, int region)
{
    uint32_t s_addr = TConModule::getStartAddress( index , region);
    uint32_t e_addr = TConModule::getEndAddress( index , region);
    uint32_t repeat_times = TConModule::getRepeatTimes( index , region);
    uint32_t times = ((e_addr-s_addr)/4 + 1) * (repeat_times+1) + 2;
    return times;
}
