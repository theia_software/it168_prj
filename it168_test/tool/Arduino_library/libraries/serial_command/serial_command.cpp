/*******************************************************************************
SerialCommand - An Arduino library to tokenize and parse commands received over
a serial port.
Copyright (C) 2011-2013 Steven Cogswell  <steven.cogswell@gmail.com>
http://awtfy.com

See SerialCommand.h for version history.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
***********************************************************************************/

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "serial_command.h"

#include <SystemModule.h>
#include <SPIUtility.h>
#include <SPIModule.h>
#include <string.h>
#include <common.h>
#include <xmodem.h>


#define SET_CMD_ARG_NUM_MAX                 (2)
#define DUMP_CMD_ARG_NUM_MAX                (2)


serial_cmd scmd;

static void set_sram_cmd(void);
static void dump_sram_cmd(void);
static void unrecognized(void);
static void debug_clk_cmd(void);

void serial_cmd_setup(void)
{
    serial_cmd();
    scmd.add_cmd("xr", xmodem_load_fw_cmd);
    scmd.add_cmd("sys_clk", sys_clk_cmd);
    scmd.add_cmd("sys_clk_ver", sys_clk_ver_cmd);
    scmd.add_cmd("spi_clk", spi_clk_cmd);
    scmd.add_cmd("dbg_clk", debug_clk_cmd);
    scmd.add_cmd("spi_mode", spi_mode_cmd);
    scmd.add_cmd("sinfo", sys_info_cmd);
    scmd.add_cmd("ss", set_sram_cmd);
    scmd.add_cmd("ds", dump_sram_cmd);
    scmd.add_def_handler(unrecognized);
}

void serial_cmd_proc(void)
{
    serial_cmd_setup();

    while (1)
        scmd.read_serial();
}


// Constructor makes sure some things are set.
serial_cmd::serial_cmd()
{
    strncpy(delim, " ", MAXDELIMETER);  // strtok_r needs a null-terminated string
    term = '\r';     // return character, default terminator for commands
    num_cmd = 0;     // Number of callback handlers installed
    clear_buffer();
}

//
// Initialize the command buffer being processed to all null characters
//
void serial_cmd::clear_buffer()
{
    memset((void *)buffer, 0, sizeof(buffer));
    buf_pos = 0;
}

// Retrieve the next token ("word" or "argument") from the Command buffer.
// returns a NULL if no more tokens exist.
char *serial_cmd::next()
{
    char *nextToken;
    nextToken = strtok_r(NULL, delim, &last);
    return nextToken;
}

// This checks the Serial stream for characters, and assembles them into a buffer.
// When the terminator character (default '\r') is seen, it starts parsing the
// buffer for a prefix command, and calls handlers setup by add_cmd() member
void serial_cmd::read_serial()
{
    U32 i;
    // If we're using the Hardware port, check it.
    // Otherwise check the user-created SoftwareSerial Port
    while (Serial.available() > 0) {
        boolean matched;

        // Hardware serial port
        // Read single available character, there may be more waiting
        in_char = Serial.read();

        if (in_char == ESC) {
            delay(10); // workaround to bypass escape sequences
            do {
                in_char = Serial.read();
            } while (in_char != -1);
            continue;
        }

        if (in_char == term) { // Check for the terminator (default '\r') meaning end of command
            Serial.print(F("\r\n"));
#ifdef SERIALCOMMANDDEBUG
            Serial.print(F("Received: "));
            Serial.println(buffer);
#endif

            buf_pos = 0; // Reset to start of buffer
            token = strtok_r(buffer, delim, &last); // Search for command at start of buffer

            if (token == NULL)
                return;

            matched = false;

            if (strncmp(token, "?", SERIALCOMMANDBUFFER) == 0) {

                Serial.println("commands:");
                for (U32 new_line = 0, i = num_cmd; i >= 1;) {

                    Serial.print(cmd_list[--i].command);
                    Serial.print(" ");

                    if ((++new_line % 8) == 0)
                        Serial.println("");

                }
                matched = true;

            } else {

                for (i = 0; i < num_cmd; ++i) {
#ifdef SERIALCOMMANDDEBUG
                    Serial.print("Comparing [");
                    Serial.print(token);
                    Serial.print("] to [");
                    Serial.print(cmd_list[i].command);
                    Serial.println("]");
#endif
                    // Compare the found command against the list of known commands for a match
                    if (strncmp(token, cmd_list[i].command, SERIALCOMMANDBUFFER) == 0) {
#ifdef SERIALCOMMANDDEBUG
                        Serial.print("Matched Command: ");
                        Serial.println(token);
#endif
                        // Execute the stored handler function for the command
                        (*cmd_list[i].function)();
                        matched = true;
                        break;
                    }
                }
            }

            if (matched == false)
                (*def_handler)();

            Serial.println();
            Serial.flush();
            clear_buffer();

        } else if (isprint(in_char)) {
            Serial.print(in_char); // Echo back to serial stream

            /* Only printable characters into the buffer */
            if (buf_pos < (SERIALCOMMANDBUFFER - 1)) {
                buffer[buf_pos++] = in_char;  // Put character into buffer
                buffer[buf_pos] = '\0';      // Null terminate
            } else {
                // buffer full
            }

        } else {
            if (buf_pos && (in_char == BS)) { // backspace
                Serial.print("\b \b");
                buffer[--buf_pos] = '\0';
            } else {
                // not support yet
            }
        }
    }
}

// Adds a "command" and a handler function to the list of available commands.
// This is used for matching a found token in the buffer, and gives the pointer
// to the handler function to deal with it.
void serial_cmd::add_cmd(const char *command, void (*function)())
{
    if (num_cmd < MAXSERIALCOMMANDS) {
#ifdef SERIALCOMMANDDEBUG
        Serial.print(num_cmd);
        Serial.print("-");
        Serial.print("Adding command for ");
        Serial.println(command);
#endif

        if ((strlen(command) + 1) > SERIALCOMMANDBUFFER) {
            char print_buf[64];

            snprintf(print_buf, sizeof(print_buf), \
                     "warning: no enough command buf, %s %d", \
                      command, (strlen(command) + 1));

            Serial.println(print_buf);
        }

        strncpy(cmd_list[num_cmd].command, command, (SERIALCOMMANDBUFFER - 1));
        cmd_list[num_cmd].command[(SERIALCOMMANDBUFFER - 1)] = '\0';
        cmd_list[num_cmd++].function = function;

    } else {
        // In this case, you tried to push more commands into the buffer than it is compiled to hold.
        // Not much we can do since there is no real visible error assertion, we just ignore adding
        // the command
#if 1 //def SERIALCOMMANDDEBUG
        Serial.println(F("Too many handlers - recompile changing MAXSERIALCOMMANDS"));
#endif
    }
}

// This sets up a handler to be called in the event that the receveived command string
// isn't in the list of things with handlers.
void serial_cmd::add_def_handler(void (*function)(void))
{
    def_handler = function;
}

// This gets set as the default handler, and gets called when no other command matches.
static void unrecognized(void)
{
    Serial.print(F("err: unrecognized command"));
}

static void set_sram_cmd(void)
{
    U32 arg_ary[SET_CMD_ARG_NUM_MAX];
    U32 argc;
    char print_buf[64]; // must define char type to print string by println
    char *arg;

    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= SET_CMD_ARG_NUM_MAX) {
            Serial.print(F("err: too many arguments"));
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 16);

#ifdef SERIALCOMMANDDEBUG
        Serial.print(argc);
        Serial.print(" argument ");
        print_hex(arg_ary[argc]);
#endif
        argc += 1;
    }

    if (argc != SET_CMD_ARG_NUM_MAX) {
        Serial.print(F("err: too few arguments "));
        Serial.print(argc);
        return;
    }

    snprintf(print_buf, sizeof(print_buf), \
             "set SRAM 0x%08lx = 0x%08lx", \
              arg_ary[0], arg_ary[1]);

    Serial.print(print_buf);

    SPIUtility::write_single32(arg_ary[0], arg_ary[1]);
}

static void dump_sram_cmd(void)
{
    U32 arg_ary[DUMP_CMD_ARG_NUM_MAX];
    U32 argc;
    char print_buf[64]; // must define char type to print string by print
    char *arg;


    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= DUMP_CMD_ARG_NUM_MAX) {
            Serial.print(F("err: too many arguments"));
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 16);

#ifdef SERIALCOMMANDDEBUG
        Serial.print(argc);
        Serial.print(" argument ");
        print_hex(arg_ary[argc]);
#endif
        argc += 1;
    }

    if (argc < (DUMP_CMD_ARG_NUM_MAX - 1)) {
        Serial.print(F("err: too few arguments "));
        Serial.print(argc);
        return;
    }

    if (argc == 1)
        arg_ary[1] = 4;

    if (!(TEST_ALIGN_4(arg_ary[0])) && (arg_ary[1] == 4)) {
        read_dump_4(arg_ary[0]);

    } else {
        snprintf(print_buf, sizeof(print_buf), \
                 "dump SRAM 0x%08lx len 0x%08lx", \
                  arg_ary[0], arg_ary[1]);

        Serial.println(print_buf);

        hexdump(arg_ary[0], arg_ary[1]);
    }
}

/*
 * output system clock of ET760 to GPIO
 * GPIO 0 = sys_clk / 2
 * GPIO 1 = sys_clk / 4
 * GPIO 2 = sys_clk / 8
 * GPIO 3 = sys_clk / 16
 * GPIO 4 = sys_clk / 32
 * GPIO 5 = sys_clk / 64
 * GPIO 6 = sys_clk / 128
 * GPIO 7 = sys_clk / 256
 *
 */
static void debug_clk_cmd(void)
{
    SPIUtility::write_single32(0x10000350, 0x00000001); // enable debug flag
    SPIUtility::write_single32(0x100002f4, 0x000000ff); // enable pad control
    SPIUtility::write_single32(0x10000354, 0x00000077); // choose debug module
    SPIUtility::write_single32(0x1000036c, 0x00000000); // debug signal in module
    SPIUtility::write_single32(0x10000370, 0x05000000); // debug signal in module

    /* release all system reset
     * default value of system_reset_risc_v(0x10000038[16]) is 1
     */
    SPIUtility::write_single32(0x10000038, 0x00000000);

    Serial.println(F("set debug clock done"));
}

