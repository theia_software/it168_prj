
Arduino tool setup flow

1. select board type "Arduino Mega ADK"
   Tools > Board

2. import library
   copy all the folders under Arduino_library to the default path of Arduino library
   default path: C:\Users\<user>\Documents\Arduino\libraries\

3. before executing BinToHex_bin.py
   change input_file_path to the path of RSIC-V binary file
   change output_file_path to the default path of Arduino library


