#include <SensorTopModule.h>
#include <SPIModule.h>
#include <SystemModule.h>
#include <TConModule.h>
#include <Arduino.h>
#include <math.h>
#include <SPIUtility.h>
#include <common.h>
#include <load_riscv_fw.h>
#include <stdio.h>
#include <dsp.h>
#include <serial_command.h>
#include <SPI.h>


#define PRINT_ALLRAM_TEST                   (0)

#define WDT_RESET_OTHERS_MDL                (0)
#define MDL_CLK_OFF_EN                      (0)
#define RISCV_RESET_TEST                    (0)
#if (RISCV_RESET_TEST == 1)
    #define PRINT_INT_MESSAGE               (0)
#else
    #define PRINT_INT_MESSAGE               (0)
#endif
    #define PRINT_TCON_INT                  (0)
    #define PRINT_DSP_INT                   (0)
    #define PRINT_SENSORTOP_INT             (0)
        #define PRINT_SPI_INT               (0)
        #define SYS_RISCV_INT_TEST          (0)

#define SYS_POWER_VER                       (0)

/* system debug flag - reset tree test */
#define RESET_TREE_EN                       (0)

/* system bus busy test*/
#define SYS_BUS_BUSY_TEST                   (0)

/* system bus busy test*/
#define BUS_BUSY_TEST                       (1)
#define ANDES_SPI_TEST                      (1)
#define BBT_SPI_SINGLE_EN                   (0)
#define BBT_SPI_BURST_EN                    (1)
#define BBT_SPI_SPEED_EN                    (1)

/* system bus busy test*/
#define IT168_DMA_PRIORITY_TEST             (0)

#if (RESET_TREE_EN == 1)
void reset_tree_dbf0(void);
void reset_tree_dbf1(void);
#endif

void print_allram(void);
void print_allreg(void);
void print_dsp_cal(void);
void read_continous_RAMinfo(void);
void riscv_reset(void);
void print_int_main(void);
void print_tcon_int_info(void);
void print_dsp_int_info(void);
void print_sensortop_int_info(void);
void print_spi_int_info(void);
void sys_riscv_int_info(void);
void write_4byte(uint32_t write_addr, uint32_t data);

#if (SYS_POWER_VER == 1)
void sys_power_cmd(void);
#endif

void load_fw_cmd(void);

byte rx_data[4] = {0x01, 0x01, 0x01, 0x01};
byte tx_data[4] = {0x01, 0x02, 0x03, 0xff};
U32 WDT_reset_RISCV = 0x01000000;

extern U32 load_fw_addr;


#if (SYS_BUS_BUSY_TEST == 1)
uint32_t LoopCnt = 0;
void sbbt_adc_sram_check();
void sbbt_polling_tcon_start();
void sbbt_fifo_info();
void system_bus_busy_test();
#endif


#define DSP_TEST_FW_CHK_FAIL                (BIT(31)) // DEBUG_DSP_FW_CHK
#define DSP_TEST_FW_CHK_LOAD_COEF_FAIL      (BIT(30)) // DEBUG_DSP_FW_CHK
#define DSP_TEST_OUT_8BIT                   (BIT(31)) // DEBUG_DSP_INSTRU

#define DSP_OP_CONV_COEF_IDX_MAX            (81UL)
#define DSP_OP_CONV_COEF_LEN_MAX            (DSP_OP_CONV_COEF_IDX_MAX << 2)

/* debug RAM layout */
#define STATUS_FLAG                         (0x00012000UL)
#define DEBUG_WDT_CNT_RAM                   (STATUS_FLAG + 0x04)
#define DEBUG_TCON_DONE_00_RAM              (STATUS_FLAG + 0x08)
#define DEBUG_TCON_DONE_01_RAM              (STATUS_FLAG + 0x0C)
#define DEBUG_TCON_DONE_02_RAM              (STATUS_FLAG + 0x10)
#define DEBUG_TCON_OUTPUT_00_RAM            (STATUS_FLAG + 0x14)
#define DEBUG_TCON_OUTPUT_01_RAM            (STATUS_FLAG + 0x18)
#define DEBUG_TCON_OUTPUT_02_RAM            (STATUS_FLAG + 0x1C)

#define DEBUG_ADC_DONE_00_RAM               (STATUS_FLAG + 0x20)
#define DEBUG_ADC_DONE_01_RAM               (STATUS_FLAG + 0x24)
#define DEBUG_ADC_DONE_02_RAM               (STATUS_FLAG + 0x28)
#define DEBUG_ADC_DONE_03_RAM               (STATUS_FLAG + 0x2C)
#define DEBUG_ADC_DONE_04_RAM               (STATUS_FLAG + 0x30)
#define DEBUG_ADC_DONE_05_RAM               (STATUS_FLAG + 0x34)
#define DEBUG_ADC_DONE_ALL_RAM              (STATUS_FLAG + 0x38)

#define DEBUG_DSP_DONE_RAM                  (STATUS_FLAG + 0x3C)
#define DEBUG_SYS_INT_IN_RAM                (STATUS_FLAG + 0x40)

#define DEBUG_SPI00_RAM                     (STATUS_FLAG + 0x44)
#define DEBUG_SPI01_RAM                     (STATUS_FLAG + 0x48)

#define DEBUG_DSP_DATA1                     (STATUS_FLAG + 0x4C)
#define DEBUG_DSP_DATA2                     (STATUS_FLAG + 0x50)
#define DEBUG_DSP_OUTPUT                    (STATUS_FLAG + 0x54)
#define DEBUG_DSP_INSTRU                    (STATUS_FLAG + 0x58)

#define DEBUG_DSP_CAL_LEN                   (STATUS_FLAG + 0x5C)
#define DEBUG_DSP_ERR_CODE                  (STATUS_FLAG + 0x60)
#define DEBUG_DSP_FW_CHK                    (STATUS_FLAG + 0x64)
#define DEBUG_DSP_FW_ADDR_OFF               (STATUS_FLAG + 0x68)
#define DEBUG_DSP_SHIFT                     (STATUS_FLAG + 0x6C)
#define DEBUG_DSP_MATRIX_IO                 (STATUS_FLAG + 0x70)
#define DEBUG_DSP_TEST_LOOP                 (STATUS_FLAG + 0x74)

#define DEBUG_IRQ25_RAM                     (STATUS_FLAG + 0x78)
#define DEBUG_IRQ27_RAM                     (STATUS_FLAG + 0x7c)
#define DEBUG_TRIGGER_TIMES                 (STATUS_FLAG + 0x80)
#define DEBUG_TRIGGER_CMP_ANS               (STATUS_FLAG + 0x84)


void setup(void)
{
    Serial.begin(115200);
    digitalWrite(SS, HIGH); // ensure SS stays high

    Serial.println(F("\r\n\n\n\n - arduino & system setup -"));
#if (CONFIG_SPI_SLAVE_IDX == CONFIG_SPI_SLAVE_00)
    Serial.println(F(GETNAME(CONFIG_SPI_SLAVE_00)));
#else
    Serial.println(F(GETNAME(CONFIG_SPI_SLAVE_01)));
#endif

    /*
     * setup Arduino SPI, set mode 0
     * system clock 16MHz
     * default SPI divider: SPI_CLOCK_DIV4
     * SPI clk 4MHz = 16 / 4
     */
    SPIUtility::setup();

    Serial.println(F("reset ET760 SPI Slave 0 & 1"));
    /* reset ET760 SPI Slave */
    SPIUtility::write_single32(SYS_RESET_SPI_SLAVE, SYS_RESET_SPI_ALL);

    Serial.println(F(" - setup done -\r\n"));

    scmd.add_cmd("lfw", load_fw_cmd);
    scmd.add_cmd("jtag_en", jtag_en_cmd);
#if (IT168_DMA_PRIORITY_TEST == 1)
    scmd.add_cmd("168_dma_pri", it168_dma_priority_cmd);
#endif

#if (SYS_POWER_VER == 1)
    scmd.add_cmd("power", sys_power_cmd);
#endif
#if (PRINT_ALLRAM_TEST == 1)
    scmd.add_cmd("pallram", print_allram);
#endif
#if (WDT_RESET_OTHERS_MDL == 1)
    scmd.add_cmd("wdt_rm", WDT_reset_module);
#endif
#if(PRINT_INT_MESSAGE == 1)
    scmd.add_cmd("p_message", print_int_main);
#endif
#if (RISCV_RESET_TEST == 1)
    scmd.add_cmd("risc_r", riscv_reset);
#endif
#if (MDL_CLK_OFF_EN == 1)
    scmd.add_cmd("read_mdreg", WDT_reset_module);
#endif

#if (RESET_TREE_EN == 1)
    scmd.add_cmd("reset_tree", reset_tree);
#endif

#if (SYS_BUS_BUSY_TEST == 1)
    scmd.add_cmd("sbbt", system_bus_busy_test);
#endif

#if (BUS_BUSY_TEST == 1)
  scmd.add_cmd("bbt", bus_busy_test);
#endif

#if (ANDES_SPI_TEST == 1)
  scmd.add_cmd("ads", andes_spi_test);
#endif

    serial_cmd_proc();
}

void load_riscv_fw(U32 fw_addr)
{
    if (TEST_ALIGN_4(fw_addr)) {
        Serial.println(F("\r\nerror: address not aligned"));
        return;
    }

    //SPIUtility::write_single32(0x100002e0, WDT_reset_RISCV);

    /* RISC-V reset & load FW flow */
    Serial.print(F("set SYS_RESET_RISCV "));
    SPIUtility::write_single32(SYS_RESET_RISCV, RESET_RISCV);
    read_dump_4(SYS_RESET_RISCV);

    /* load bin file to SRAM fw_addr */
    load_fw_addr = fw_addr;
    setRiscvInstr();

    Serial.print(F("set SYS_RISCV_RESET_VECTOR "));
    SPIUtility::write_single32(SYS_RISCV_RESET_VECTOR, fw_addr);
    read_dump_4(SYS_RISCV_RESET_VECTOR);

    Serial.print(F("release SYS_RESET_RISCV "));
    SPIUtility::write_single32(SYS_RESET_RISCV, RELEASE_RESET_RISCV);
    read_dump_4(SYS_RESET_RISCV);

    Serial.println(F("\r\nRISC-V load code done"));
}

void loop(void)
{

}

void WDT_reset_module()
{
#if (WDT_RESET_OTHERS_MDL == 1)
    SPIUtility::write_single32(0x20000000, 0x22222222);
    SPIUtility::write_single32(0x30000000, 0x33333333);
    SPIUtility::write_single32(0x40000000, 0x44444444);
    SPIUtility::write_single32(0x50000000, 0x55555555);
    SPIUtility::write_single32(0x60000000, 0x66666666);
    delay(3000);
    Serial.print(F("\r\n0x20000000:"));
    SPIUtility::readSingle32(0x20000000, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
    Serial.println(F(""));

    Serial.print(F("\r\n0x30000000:"));
    SPIUtility::readSingle32(0x30000000, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
    Serial.println(F(""));
    Serial.print(F("\r\n0x40000000:"));
    SPIUtility::readSingle32(0x40000000, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
    Serial.println(F(""));
    Serial.print(F("\r\n0x50000000:"));
    SPIUtility::readSingle32(0x50000000, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
    Serial.println(F(""));
    Serial.print(F("\r\n0x60000000:"));
    SPIUtility::readSingle32(0x60000000, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
    Serial.println(F(""));
    //Serial.println(F("exit WDT_RM"));
#endif
}

#if (PRINT_ALLRAM_TEST == 1)
void print_allram(void)
{
    Serial.println(F("read All RAM test ram"));
    while (1) {
        Serial.println(F("wait process done"));
        SPIUtility::readSingle32(0x30000004, rx_data);
        if (rx_data[0] == 0xee) {
            SPIUtility::readSingle32(0x30000000, rx_data);

            if (rx_data[0] == 0) {
                Serial.println(F("pass!!"));

            } else {
                Serial.print(F("Fail, error cnt = "));

                for (int i = 3; i >= 0; i--) {
                    Serial.print(rx_data[i], HEX);
                    Serial.print(F(" "));
                }
                Serial.println("");

                for (int i = 3; i >= 0; i--) {
                    Serial.print(rx_data[i], HEX);
                    Serial.print(F(" "));
                }
                Serial.println("");

                SPIUtility::readSingle32(0x40000008, rx_data);
                Serial.print(F("1st fail addr = 0x"));
                for (int i = 3; i >= 0; i--) {
                    Serial.print(rx_data[i], HEX);
                    Serial.print(F(" "));
                }
                Serial.println("");
            }
            Serial.println(F("=======end of part ram======="));
        }
    }
}
#endif

#if (RISCV_RESET_TEST == 1)
void riscv_reset(void)
{
    delay(500);
    SPIUtility::readSingle32(STATUS_FLAG, rx_data);
    Serial.print(F("status check: "));
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i],HEX);
        Serial.print(F(" "));
    }
    Serial.println("");

    SPIUtility::write_single32(SYS_RESET_RISCV, RESET_RISCV);
    SPIUtility::write_single32(SYS_RESET_RISCV, RELEASE_RESET_RISCV);
    delay(50);

    SPIUtility::readSingle32(STATUS_FLAG, rx_data);
    Serial.print(F("after reset status check: "));

    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i],HEX);
        Serial.print(F(" "));
    }
    Serial.println("");
    delay(1000);
}
#endif

#if (PRINT_TCON_INT == 1)
void print_tcon_int_info(void)
{
    uint32_t MyOut = 0;

    Serial.print(F("DEBUG_TRIGGER_CMP_ANS:"));
    SPIUtility::readSingle32(DEBUG_TRIGGER_CMP_ANS, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
    Serial.println("");

    TranslateTo4byte(&MyOut, rx_data);

    if (MyOut != 0)
        Serial.println(F("trigger times != ADC/TCON int times!!!"));

    Serial.print(F("0x1000034c interrupt in/out out:"));
    SPIUtility::readSingle32(0x1000034c, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
#if 0
    Serial.print(F("\r\nout out:"));
    SPIUtility::readSingle32(0x100001f8, rx_data);
    for(int i = 3; i >= 0; i--)
    {
        Serial.print(rx_data[i],HEX);
        Serial.print(F(" "));
    }
    Serial.println("");
    Serial.print(F("current_state:"));
    SPIUtility::readSingle32(0x30000014, rx_data);
    for(int i = 3; i >= 0; i--)
    {
        Serial.print(rx_data[i],HEX);
        Serial.print(F(" "));
    }
#endif
    Serial.print(F("\r\ntcon_done:"));
    SPIUtility::readSingle32(0x3000001C, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nTriggerTimes:"));
    SPIUtility::readSingle32(DEBUG_TRIGGER_TIMES, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nTCON 0 done:"));
    SPIUtility::readSingle32(DEBUG_TCON_DONE_00_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nTCON 0 output:"));
    SPIUtility::readSingle32(DEBUG_TCON_OUTPUT_00_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nTCON 1 done:"));
    SPIUtility::readSingle32(DEBUG_TCON_DONE_01_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nTCON 1 output:"));
    SPIUtility::readSingle32(DEBUG_TCON_OUTPUT_01_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nTCON 2 done:"));
    SPIUtility::readSingle32(DEBUG_TCON_DONE_02_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nTCON 2 output:"));
    SPIUtility::readSingle32(DEBUG_TCON_OUTPUT_02_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
    Serial.println("");
}
#endif

#if (PRINT_SENSORTOP_INT == 1)
void print_sensortop_int_info(void)
{
    Serial.print(F("\r\nWDT cnt:"));
    SPIUtility::readSingle32(DEBUG_WDT_CNT_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nADC_all:"));
    SPIUtility::readSingle32(DEBUG_ADC_DONE_ALL_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nADC_00:"));
    SPIUtility::readSingle32(DEBUG_ADC_DONE_00_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
    Serial.print(rx_data[i], HEX);
    Serial.print(F(" "));
    }

    Serial.print(F("\r\nADC_01:"));
    SPIUtility::readSingle32(DEBUG_ADC_DONE_01_RAM, rx_data);
    for(int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nADC_02:"));
    SPIUtility::readSingle32(DEBUG_ADC_DONE_02_RAM, rx_data);
    for(int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nADC_03:"));
    //SPIUtility::readSingle32(0x00020000, rx_data);
    SPIUtility::readSingle32(DEBUG_ADC_DONE_03_RAM, rx_data);
    for(int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nADC_04:"));
    SPIUtility::readSingle32(DEBUG_ADC_DONE_04_RAM, rx_data);
    for(int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nADC_05:"));
    SPIUtility::readSingle32(DEBUG_ADC_DONE_05_RAM, rx_data);
    for(int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
    Serial.println("");
}
#endif

#if (PRINT_SPI_INT == 1)
void print_spi_int_info(void)
{
    Serial.print(F("SPI_00 output:"));
    SPIUtility::readSingle32(DEBUG_SPI00_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nSPI_01 output:"));
    SPIUtility::readSingle32(DEBUG_SPI01_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nWFI status:"));
    SPIUtility::readSingle32(0x100000d8, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
    Serial.println("");
}
#endif

#if (SYS_RISCV_INT_TEST == 1)
void sys_riscv_int_info(void)
{
    Serial.print(F("\r\nGPIO_IM 0x100002ac:"));
    SPIUtility::readSingle32(0x100002ac, rx_data);

    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i],HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\n0x100000a8:"));
    SPIUtility::readSingle32(0x100000a8, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i],HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nirq 25:"));
    SPIUtility::readSingle32(DEBUG_IRQ25_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i],HEX);
        Serial.print(F(" "));
    }

    Serial.print(F("\r\nirq 27:"));
    SPIUtility::readSingle32(DEBUG_IRQ27_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i],HEX);
        Serial.print(F(" "));
    }
    Serial.println("");

    Serial.print(F("\r\nirq29 SYS RISCV INT:"));
    SPIUtility::readSingle32(DEBUG_SYS_INT_IN_RAM, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
    Serial.println("");
}
#endif

#if (PRINT_DSP_INT == 1)
void print_dsp_int_info(void)
{
    Serial.print(F("\r\nDSP INT:"));
    SPIUtility::readSingle32(DEBUG_DSP_DONE_RAM, rx_data);

    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
    Serial.println(F(""));
}
#endif

#if (PRINT_INT_MESSAGE == 1)
void print_int_main(void)
{
    int z = 0;

    while (1) {
        delay(1000);
#if (PRINT_TCON_INT == 1)
        print_tcon_int_info();
#endif
#if (PRINT_DSP_INT == 1)
        print_dsp_int_info();
#endif
#if (PRINT_SENSORTOP_INT == 1)
        print_sensortop_int_info();
#endif
#if (PRINT_SPI_INT == 1)
        print_spi_int_info();
#endif
#if (SYS_RISCV_INT_TEST == 1)
        sys_riscv_int_info();
#endif

        Serial.print(F("LoopCnt:"));
        SPIUtility::readSingle32(STATUS_FLAG, rx_data);
        for (int i = 3; i >= 0; i--) {
            Serial.print(rx_data[i], HEX);
            Serial.print(F(" "));
        }
        Serial.println(F("\r\n====================="));
        z++;
#if (WDT_RESET_OTHERS_MDL == 1 | MDL_CLK_OFF_EN == 1)
        Serial.print(F("\r\n0x20000000:"));
        SPIUtility::readSingle32(0x20000000, rx_data);
        for (int i = 3; i >= 0; i--) {
            Serial.print(rx_data[i], HEX);
            Serial.print(F(" "));
        }
        Serial.println(F(""));

        Serial.print(F("\r\n0x30000000:"));
        SPIUtility::readSingle32(0x30000000, rx_data);
        for (int i = 3; i >= 0; i--) {
            Serial.print(rx_data[i], HEX);
            Serial.print(F(" "));
        }
        Serial.println(F(""));
                Serial.print(F("\r\n0x40000000:"));
        SPIUtility::readSingle32(0x40000000, rx_data);
        for (int i = 3; i >= 0; i--) {
            Serial.print(rx_data[i], HEX);
            Serial.print(F(" "));
        }
        Serial.println(F(""));
                Serial.print(F("\r\n0x50000000:"));
        SPIUtility::readSingle32(0x50000000, rx_data);
        for (int i = 3; i >= 0; i--) {
            Serial.print(rx_data[i], HEX);
            Serial.print(F(" "));
        }
        Serial.println(F(""));
        Serial.print(F("\r\n0x60000000:"));
        SPIUtility::readSingle32(0x60000000, rx_data);
        for (int i = 3; i >= 0; i--) {
            Serial.print(rx_data[i], HEX);
            Serial.print(F(" "));
        }
        Serial.println(F(""));
#endif
    }
}
#endif

void write_4byte(uint32_t write_addr, uint32_t data)
{
    byte w_data[4];

    w_data[3] = ((data >> 24) & 0xFF);
    w_data[2] = ((data >> 16) & 0xFF);
    w_data[1] = ((data >> 8) & 0xFF);
    w_data[0] = ((data >> 0) & 0xFF);
    SPIUtility::writeSingle32(write_addr, w_data);
}

void TranslateTo4byte(uint32_t *result, byte input[4])
{
    *result = 0x0;
    *result |= ((uint32_t)input[0] << 0);
    *result |= ((uint32_t)input[1] << 8);
    *result |= ((uint32_t)input[2] << 16);
    *result |= ((uint32_t)input[3] << 24);
}

#if (SYS_POWER_VER == 1)
void sys_power_cmd(void)
{
#define SYS_POWER_CMD_ARG_NUM_MAX   (2)
    U32 arg_ary[SYS_POWER_CMD_ARG_NUM_MAX];
    U32 argc;
    U32 group;
    U32 addr;
    U32 data;
    U8 power_down;
    char *arg;


    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= SYS_POWER_CMD_ARG_NUM_MAX) {
            Serial.println(F("err: too many arguments"));
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 10);

#ifdef SERIALCOMMANDDEBUG
        Serial.print(argc);
        Serial.print(" argument ");
        print_hex(arg_ary[argc]);
#endif
        argc += 1;
    }

    if (argc != SYS_POWER_CMD_ARG_NUM_MAX) {
        Serial.print(F("err: too few arguments "));
        Serial.print(argc);
        return;
    }

    if (arg_ary[1] > 1) {
        Serial.print(F("err: invalid power on(0)/down(1) argument "));
        Serial.print(arg_ary[1]);
        return;
    }

    group = arg_ary[0];
    power_down = arg_ary[1];

    {
        char print_buf[64]; // must define char type to print string by Serial.print
        snprintf(print_buf, sizeof(print_buf), \
                 "set group %lu power %s", \
                 group, (power_down ? "down" : "on"));

        Serial.print(print_buf);
    }

    if (group == 0) {
        addr = 0x10000388; // system_da_dig_pd
        data = SPIUtility::read_single32(addr);
        data = (power_down ? (data | BIT(0)) : (data & ~BIT(0)));
        SPIUtility::write_single32(addr, data);

    } else if (group == 1) {
        addr = 0x100000f0; // DLDO_STB_EN
        data = SPIUtility::read_single32(addr);
        data = (power_down ? (data | BIT(8)) : (data & ~BIT(8)));
        SPIUtility::write_single32(addr, data);

    } else if (group == 2) {
        addr = 0x100000ec; // BG_PD
        data = SPIUtility::read_single32(addr);
        data = (power_down ? (data | BIT(16)) : (data & ~BIT(16)));
        SPIUtility::write_single32(addr, data);

    } else if (group == 3) {
        addr = 0x100000e8; // ALDO12_PD
        data = SPIUtility::read_single32(addr);
        data = (power_down ? (data | BIT(24)) : (data & ~BIT(24)));
        SPIUtility::write_single32(addr, data);

    } else if (group == 4) {
        addr = 0x100000ec; // DLDO18_PD
        data = SPIUtility::read_single32(addr);
        data = (power_down ? (data | BIT(24)) : (data & ~BIT(24)));
        SPIUtility::write_single32(addr, data);

    } else if (group == 5) {
        addr = 0x100000f0; // MPX_MONDAC_PD
        data = SPIUtility::read_single32(addr);
        data = (power_down ? (data | BIT(24)) : (data & ~BIT(24)));
        SPIUtility::write_single32(addr, data);

        addr = 0x10000100; // MPX_OP_PD
        data = SPIUtility::read_single32(addr);
        data = (power_down ? (data | BIT32_MASK(0, 4)) : (data & ~BIT32_MASK(0, 4)));
        SPIUtility::write_single32(addr, data);

        /* RXG0 ADC PD */
        {
            addr = 0x1000010c; // RXG0_ADC_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(8)) : (data & ~BIT(8)));
            SPIUtility::write_single32(addr, data);

            addr = 0x1000010c; // RXG0_ADC_REF_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(16)) : (data & ~BIT(16)));
            SPIUtility::write_single32(addr, data);

            addr = 0x1000011c; // RXG0_PGA_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT32_MASK(0, 4)) : (data & ~BIT32_MASK(0, 4)));
            SPIUtility::write_single32(addr, data);
        }

        /* RXG1 ADC PD */
        {
            addr = 0x1000011c; // RXG1_ADC_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(8)) : (data & ~BIT(8)));
            SPIUtility::write_single32(addr, data);

            addr = 0x1000011c; // RXG1_ADC_REF_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(16)) : (data & ~BIT(16)));
            SPIUtility::write_single32(addr, data);

            addr = 0x1000012c; // RXG1_PGA_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT32_MASK(0, 4)) : (data & ~BIT32_MASK(0, 4)));
            SPIUtility::write_single32(addr, data);
        }

        /* RXG2 ADC PD */
        {
            addr = 0x1000012c; // RXG2_ADC_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(8)) : (data & ~BIT(8)));
            SPIUtility::write_single32(addr, data);

            addr = 0x1000012c; // RXG2_ADC_REF_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(16)) : (data & ~BIT(16)));
            SPIUtility::write_single32(addr, data);

            addr = 0x1000013c; // RXG2_PGA_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT32_MASK(0, 4)) : (data & ~BIT32_MASK(0, 4)));
            SPIUtility::write_single32(addr, data);
        }

        /* RXG3 ADC PD */
        {
            addr = 0x1000013c; // RXG3_ADC_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(8)) : (data & ~BIT(8)));
            SPIUtility::write_single32(addr, data);

            addr = 0x1000013c; // RXG3_ADC_REF_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(16)) : (data & ~BIT(16)));
            SPIUtility::write_single32(addr, data);

            addr = 0x1000014c; // RXG3_PGA_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT32_MASK(0, 4)) : (data & ~BIT32_MASK(0, 4)));
            SPIUtility::write_single32(addr, data);
        }

        /* RXG4 ADC PD */
        {
            addr = 0x1000014c; // RXG4_ADC_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(8)) : (data & ~BIT(8)));
            SPIUtility::write_single32(addr, data);

            addr = 0x1000014c; // RXG4_ADC_REF_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(16)) : (data & ~BIT(16)));
            SPIUtility::write_single32(addr, data);

            addr = 0x1000015c; // RXG4_PGA_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT32_MASK(0, 4)) : (data & ~BIT32_MASK(0, 4)));
            SPIUtility::write_single32(addr, data);
        }

        /* RXG5 ADC PD */
        {
            addr = 0x1000015c; // RXG5_ADC_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(8)) : (data & ~BIT(8)));
            SPIUtility::write_single32(addr, data);

            addr = 0x1000015c; // RXG5_ADC_REF_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(16)) : (data & ~BIT(16)));
            SPIUtility::write_single32(addr, data);

            addr = 0x1000016c; // RXG5_PGA_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT32_MASK(0, 4)) : (data & ~BIT32_MASK(0, 4)));
            SPIUtility::write_single32(addr, data);
        }

        /* RXG6 ADC PD */
        {
            addr = 0x10000194; // RXG6_ADC_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(0)) : (data & ~BIT(0)));
            SPIUtility::write_single32(addr, data);

            addr = 0x10000194; // RXG6_ADC_REF_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(8)) : (data & ~BIT(8)));
            SPIUtility::write_single32(addr, data);

            addr = 0x100001a0; // RXG6_PGA_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT32_MASK(24, 4)) : (data & ~BIT32_MASK(24, 4)));
            SPIUtility::write_single32(addr, data);
        }

        /* RXG7 ADC PD */
        {
            addr = 0x100001a4; // RXG7_ADC_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(0)) : (data & ~BIT(0)));
            SPIUtility::write_single32(addr, data);

            addr = 0x100001a4; // RXG7_ADC_REF_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT(8)) : (data & ~BIT(8)));
            SPIUtility::write_single32(addr, data);

            addr = 0x100001b0; // RXG7_PGA_PD
            data = SPIUtility::read_single32(addr);
            data = (power_down ? (data | BIT32_MASK(24, 4)) : (data & ~BIT32_MASK(24, 4)));
            SPIUtility::write_single32(addr, data);
        }

        addr = 0x10000170; // THEMO_PD
        data = SPIUtility::read_single32(addr);
        data = (power_down ? (data | BIT(8)) : (data & ~BIT(8)));
        SPIUtility::write_single32(addr, data);

    } else {
        Serial.print(F("err: invalid group"));
    }
}
#endif






void reset_tree_dbf0(void)
{
#if (RESET_TREE_EN == 1)
    SPIUtility::write_single32(0x10000350, 0x00000001); // enable debug flag
    SPIUtility::write_single32(0x100002f4, 0x000000ff); // enable pad control
    SPIUtility::write_single32(0x10000354, 0x00000077); // choose debug module
    SPIUtility::write_single32(0x1000036c, 0x00001500); // debug signal in module
    SPIUtility::write_single32(0x10000370, 0x01000000); // debug signal in module

    while (1) {
        SPIUtility::write_single32(0x10000038, 0x00000000); // set dma, riscv core/bus, sensortop, tcon
        delay(1000);
        SPIUtility::write_single32(0x10000038, 0x01010101); // reset dma, riscv core/bus, sensortop, tcon
        delay(1000);
        /*SPIUtility::write_single32(0x10000394, 0x00000001); // reset dma, riscv core/bus, sensortop, tcon
        delay(100);
        SPIUtility::write_single32(0x10000394, 0x00000000); // reset dma, riscv core/bus, sensortop, tcon
        delay(100);*/
        Serial.println("running reset tree, plz measure");
        Serial.println("GPIO 1: dma");
        Serial.println("GPIO 2: risc-v core");
        Serial.println("GPIO 3: risc-v bus");
        Serial.println("GPIO 4: sensor top");
        Serial.println("GPIO 5: tcon");
        Serial.println("GPIO 6: system (nothing)");
    }
#endif
}

void reset_tree_dbf1(void)
{
#if (RESET_TREE_EN == 1)
    SPIUtility::write_single32(0x10000350, 0x00000001); // enable debug flag
    SPIUtility::write_single32(0x100002f4, 0x000000ff); // enable pad control
    SPIUtility::write_single32(0x10000354, 0x00000077); // choose debug module
    SPIUtility::write_single32(0x1000036c, 0x00001500); // debug signal in module
    SPIUtility::write_single32(0x10000370, 0x00000000); // debug signal in module

    while (1) {
        //SPIUtility::write_single32(0x10000058, 0x00000000); // set bus, spi_01
        SPIUtility::write_single32(0x100000C4, 0x00000000); // set dsp
        delay(1000);
        SPIUtility::write_single32(0x10000058, 0x00010000); // reset bus, spi_01 (wc reg)
        SPIUtility::write_single32(0x100000C4, 0x00000001); // reset dsp
        delay(1000);
        Serial.println("running reset tree, plz measure");
        Serial.println("GPIO 2: dsp");
        Serial.println("GPIO 3: bus");
        Serial.println("GPIO 4: SPI_01");
    }
#endif
}

#if (SYS_BUS_BUSY_TEST == 1)
void sbbt_adc_sram_check()
{
    uint32_t i=0, j=0;
    uint32_t rd_data = 0;
    uint32_t adc_sram_base = 0x30000;
    uint32_t adc_num = 32;
    uint32_t adc_channel = 32;
    uint32_t adc_data_byte = 2;
    uint32_t print_num = 0;

    print_num = (adc_channel * adc_data_byte)/4;//print num per adc

    Serial.println("ADC data");

    for(i=0; i<adc_num; i++)
    {
        Serial.print("ADC ");
        Serial.println(i);
        adc_sram_base = 0x30000 + (i*adc_channel*adc_data_byte);
        for(j=0; j<print_num; j++){
            rd_data = SPIUtility::read_single32(adc_sram_base + (j*4));// adc data
            print_hexw(rd_data);
            Serial.print(" ");
        }
        Serial.print("\n");
    }

    print_str_hex("FIFO ISR Cnt\t", SPIUtility::read_single32(0x25000));

}

void sbbt_polling_tcon_start()
{
    LoopCnt++;

    if(LoopCnt==100)
    {
        SPIUtility::write_single32(0x30000008, 0x1);//Start Tcon 0
        sbbt_adc_sram_check();
        while(1);
    }

}
void sbbt_fifo_info()
{
    uint32_t rd_data=0;
    uint32_t flag=0;

    Serial.print(LoopCnt);// Arduino cnt
    Serial.print("\t");

    flag = SPIUtility::read_single32(0x25000);// FIFO ISR cnt
    Serial.print(flag);
    Serial.print("\t");

    rd_data = SPIUtility::read_single32(0x1000031c);// FIFO ov reg
    //print_hexw(rd_data);
    Serial.print(rd_data);
    Serial.print("\n");
    #if 0
    rd_data = SPIUtility::read_single32(0x30000);// ADC test pattrn
    print_hexw(rd_data);
    //Serial.print("\t");
    //rd_data = SPIUtility::read_single32(0x35008);// ADC test pattrn
    //print_hexw(rd_data);
    //rd_data = SPIUtility::read_single32(0x1000039c);// RiscV cnt
    //print_hexw(rd_data);
    //Serial.print(rd_data);
    Serial.print("\n");
    #endif

    #if 0
    if(LoopCnt==150)
    //if(flag==1)
    {
        #if 0   // FIFO sram info check
        print_str_hex("FIFO ISR Cnt\t", SPIUtility::read_single32(0x25000));
        print_str_hex("FIFO Over 0x31c\t", SPIUtility::read_single32(0x25004));
        print_str_hex("Adc pattern 0x80", SPIUtility::read_single32(0x40000080));
        print_str_hex("Tcon0 clk div 0x44", SPIUtility::read_single32(0x30000044));
        #endif
        while(1);
    }
#endif
}

void system_bus_busy_test(void)
{
    while(1)
    {
        sbbt_polling_tcon_start();
        //sbbt_fifo_info();
    }
}
#endif

#if (BUS_BUSY_TEST == 1)
void bus_busy_test(void)
{
    uint32_t read_data = 0;
    const uint32_t test_sram_addr = 0x9f000;
    const uint32_t test_sram_len = 0x200;

    uint8_t wdata[test_sram_len];
    uint8_t rdata[test_sram_len] = {0};

    char in_char;;
    int cnt;
    int test_loop = 0;

    SPIUtility::write_single32(0x9fffc, 0x00000001); // trigger fw start
    SPIModule::set_base_addr(test_sram_addr);

    memset((void *)wdata, 0x5a, test_sram_len);

    print_str_hex("\r\nstart trigger FW bus_busy_test\t", SPIUtility::read_single32(0x9fffc));

    Serial.print("\rstart bus busy test\n");
    Serial.print("\rpress space to stop\n");

    while(1) {
        print_str_hex("\r test_loop\t", ++test_loop);

#if BBT_SPI_SINGLE_EN
        for (cnt = 0; cnt < test_sram_len; cnt += 4) {
            SPIUtility::write_single32(test_sram_addr + cnt, 0x00000001);
            U32 temp = SPIUtility::read_single32(test_sram_addr + cnt);
            if (temp != 0x00000001) {
                Serial.print("\r\n single test 1 single32 rw test fail addr");
                print_str_hex("",test_sram_addr + cnt);
                print_str_hex("\r\n fail data = \t", temp);
                return;
            }
        }

        for (cnt = 0; cnt < test_sram_len; cnt += 4) {
            SPIUtility::write_single32(test_sram_addr + cnt, 0xffffffff);
            U32 temp = SPIUtility::read_single32(test_sram_addr + cnt);
            if (temp != 0xffffffff) {
                Serial.print("\r single test 2 single32 rw test fail addr");
                print_str_hex("",test_sram_addr + cnt);
                print_str_hex("\r\n fail data = \t", temp);
                return;
            }
        }
#endif
#if BBT_SPI_BURST_EN
        SPIUtility::write_burst8(0, wdata, test_sram_len);

#if BBT_SPI_SPEED_EN
        const U32 speed_mode_cfg =
                SET_SPISN_SPEED_MODE_EN | SET_SPISN_MISO_2ND_EDGE_SEL | \
                ((0 << SPISN_MISO_DELAY_SEL_OFF) & SPISN_MISO_DELAY_SEL_MASK);

        SPIUtility::write_single32(SPISN_SPEED_SEL, speed_mode_cfg);

        U32 spi_speed_mode_dmy = 1;
        SPIUtility::write_single32(SPISN_DMY_BYTE, spi_speed_mode_dmy);
        // SPIUtility::set_spi_clk_div(4);

        SPIUtility::read_burst8_speed(spi_speed_mode_dmy, 0, rdata, test_sram_len);
#else
        //use read_burst8 the first will be error
        SPIUtility::read_burst8(0, rdata, test_sram_len);
#endif

        for (cnt = 0; cnt < test_sram_len; cnt ++) {
            if (rdata[cnt] != wdata[cnt]) {
                Serial.print("\r\n burst test fail addr");
                print_str_hex("\r\n fail idx = \t", cnt);
                print_str_hex("\r\n fail data = \t", rdata[cnt]);
                return;
            }
        }

#if BBT_SPI_SPEED_EN
        /* reset SPI Slave */
       SPIUtility::write_single32(SPISN_SPEED_SEL, SET_SPISN_MISO_2ND_EDGE_SEL | \
                ((0 << SPISN_MISO_DELAY_SEL_OFF) & SPISN_MISO_DELAY_SEL_MASK));
       // SPIUtility::write_single32(SYS_RESET_SPI_SLAVE, SYS_RESET_SPI_ALL);
       // SPIUtility::set_spi_clk_div(4);
#endif
#endif

        in_char = Serial.read();

        if(in_char ==' ' ) {
          SPIUtility::write_single32(0x9fffc, 0x00000000); // reset dsp
          break;
        }
    }
}
#endif

#if (ANDES_SPI_TEST == 1)
void andes_spi_test(void)
{
#define SPI_TEST_CMD_ARG_NUM_MAX   (3)
#define SPI_SLAVE_ONLY             (0)

    unsigned char recv[200];
    U32 arg_ary[SPI_TEST_CMD_ARG_NUM_MAX];
    U32 argc;
    U8 test_mode;
    char *arg;
    int cnt = 0;

    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= SPI_TEST_CMD_ARG_NUM_MAX) {
            Serial.println(F("err: too many arguments"));
            return;
        }

        if (argc == 0)
            arg_ary[argc] = strtoul(arg, NULL, 16);
        else
            arg_ary[argc] = strtoul(arg, NULL, 16);

        Serial.print(argc);
        Serial.print(" argument ");
        print_hex(arg_ary[argc]);
        argc += 1;
    }

    if (arg_ary[0] == 0xFF) {
        switch (arg_ary[1]) {
            case 0:
                Serial.print("set MODE0 CPOL 0 CPHA 0");
                SPI.setDataMode(SPI_MODE0);
            break;
            case 1:
                Serial.print("set MODE0 CPOL 0 CPHA 1");
                SPI.setDataMode(SPI_MODE1);
            break;
            case 2:
                Serial.print("set MODE0 CPOL 1 CPHA 0");
                SPI.setDataMode(SPI_MODE2);
            break;
            case 3:
                Serial.print("set MODE0 CPOL 1 CPHA 1");
                SPI.setDataMode(SPI_MODE3);
            break;
            default:
            break;
        }
        return;
    } else if (arg_ary[0] == 0xFE) {
        /* send slave opcode
         * arg_ary[1] : CMD
         * arg_ary[2] : send how many bytes after
         *                   opcode and dummy to receive data
         */
        if (argc < 3) {
            Serial.println(F("\rtoo few arguments\n"));
            return;
        }
        int slave_opcode = arg_ary[1];
        digitalWrite(SS, LOW);

        SPI.transfer(arg_ary[1]);   // send opcode
        SPI.transfer(0);            // send dummy
        switch (slave_opcode) {
            case 0x5:
                /* start send clk to receive data */
                for (int i = 0; i < arg_ary[2]; i ++) {
                    recv[i] = SPI.transfer(0x00);
                }

                for (int i = 0; i < arg_ary[2]; i ++) {
                    print_str_hex("\r recv = \t", recv[i]);
                }
                break;
            case 0xB:
                delay(1000);
                for (int i = 0; i < arg_ary[2]; i ++) {
                    recv[i] = SPI.transfer(0x00);
                }
                for (int i = 0; i < arg_ary[2]; i ++) {
                    print_str_hex("\r recv = \t", recv[i]);
                }
                break;
            case 0x51:
                for (int i = 0; i < arg_ary[2]; i ++) {
                    SPI.transfer(i + 1);
                }
                break;
            default:
                Serial.print("\rundefine cmd\n");
                break;
        }
        digitalWrite(SS, HIGH);
        return;

    }

    Serial.println(F("\r\nstart spi test"));
//    digitalWrite(SS, LOW);

    if (arg_ary[0] != 0) {
        cnt = arg_ary[0];
        digitalWrite(SS, LOW);
#if (SPI_SLAVE_ONLY == 0)
        // send opcode and dummy
        SPI.transfer(0x1a);
        SPI.transfer(0x2a);
#endif
        for (int i = 0; i < cnt; i ++) {
            SPI.transfer(i + 1);

        }
        digitalWrite(SS, HIGH);

        for (int i = 1; i <= cnt; i ++) {
            print_str_hex("\r idx = \t", i);
        }

    } else {
        if (argc >= 2) {
            cnt = arg_ary[1];
        } else {
#if (SPI_SLAVE_ONLY == 1)
            cnt = 10;
#else
            cnt = 12;
#endif
        }

        for (int i = 0; i < cnt; i ++) {
            recv[i] = 0;
        }

        digitalWrite(SS, LOW);
        for (int i = 0; i < cnt; i ++) {
            recv[i] = SPI.transfer(0x00);
//            print_str_hex("\r recv = \t", recv[i]);
//            delay(500);
        }
        digitalWrite(SS, HIGH);

        for (int i = 0; i < cnt; i ++) {
            print_str_hex("\r recv = \t", recv[i]);
        }

    }
//    digitalWrite(SS, HIGH);
    Serial.println(F("\r\nstart spi end"));

}
#endif

void load_fw_cmd(void)
{
#define LOAD_FW_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[LOAD_FW_CMD_ARG_NUM_MAX];
    U32 argc;
    char *arg;

    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= LOAD_FW_CMD_ARG_NUM_MAX) {
            Serial.println(F("err: too many arguments"));
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 16);
        argc += 1;
    }

    if (argc)
        load_riscv_fw(arg_ary[0]);
    else
        load_riscv_fw(0);
}

void jtag_en_cmd(void)
{
#define JTAG_EN_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[JTAG_EN_CMD_ARG_NUM_MAX];
    U32 argc;
    U32 rdata;
    U32 jtag_en;
    char *arg;

    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= JTAG_EN_CMD_ARG_NUM_MAX) {
            Serial.println(F("err: too many arguments"));
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 16);
        argc += 1;
    }

    if (argc != JTAG_EN_CMD_ARG_NUM_MAX) {
        Serial.print(F("err: too few arguments "));
        Serial.print(argc);
        return;
    }

    if (arg_ary[0] > 1) {
        Serial.print(F("err: invalid argument "));
        Serial.print(arg_ary[1]);
        return;
    }

    jtag_en = arg_ary[0];

    if (jtag_en) {
        /* enable RISC-V JTAG pin */
        Serial.print(F("enable"));

    } else {
        /* disable RISC-V JTAG pin */
        Serial.print(F("disable"));
    }
    Serial.println(F(" RISC-V JTAG"));

    SPIUtility::write_single32(SYS_RISCV_FTAG_EN, jtag_en);

    rdata = SPIUtility::read_single32(SYS_RISCV_FTAG_EN);

    if (rdata != jtag_en) {
        Serial.println(F("set JTAG reg fail! "));
        Serial.print(F("SYS_RISCV_FTAG_EN "));
        print_hex(rdata);
    }
}

#if (RESET_TREE_EN == 1)
void reset_tree(void)
{
#define RESET_TREE_ARG_NUM_MAX   (1)
    U32 arg_ary[RESET_TREE_ARG_NUM_MAX];
    U32 argc;
    U32 group;
    U32 addr;
    U32 data;
    U8 power_down;
    char *arg;


    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= RESET_TREE_ARG_NUM_MAX) {
            Serial.println(F("err: too many arguments"));
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 10);

#ifdef SERIALCOMMANDDEBUG
        Serial.print(argc);
        Serial.print(" argument ");
        print_hex(arg_ary[argc]);
#endif
        argc += 1;
    }

    if (argc != RESET_TREE_ARG_NUM_MAX) {
        Serial.print(F("err: too few arguments "));
        Serial.print(argc);
        return;
    }

    if (arg_ary[0] > 1) {
        Serial.print(F("err: invalid reset tree(0)/reset tree(1) argument "));
        Serial.print(arg_ary[0]);
        return;
    }
    /*{
        char print_buf[64]; // must define char type to print string by Serial.print
        snprintf(print_buf, sizeof(print_buf), \
            "set group %lu power %s", \
            group, (power_down ? "down" : "on"));

        Serial.print(print_buf);
    }*/
    if (arg_ary[0] == 0)
        reset_tree_dbf0();
    else
        reset_tree_dbf1();
}
#endif

#if 0
#define PRINT_DSP_CAL_TEST                  (0)
    #define DSP_SET_NUM                     (3) // debug print num

#if (PRINT_DSP_CAL_TEST == 1)
void print_dsp_cal(void)
{
    U32 readAddr_dsp = 0x0012000 + 0x20;
    int setNumCnt = 0;

    Serial.println(F("read DSP Debug ram"));

    for (int j = 0; j < WriteTotal_Bytes; j++) {
        rx_data[0] = 0;
        rx_data[1] = 0;
        rx_data[2] = 0;
        rx_data[3] = 0;

        SPIUtility::readSingle32(readAddr_dsp, rx_data) ;

        if (setNumCnt == 0)
            Serial.print(F("index   :"));
        else if (setNumCnt == 1)
            Serial.print(F("Real ans    :"));
        else if (setNumCnt == 2)
            Serial.print(F("DSP output  :"));
#if 0
        else if(setNumCnt == 3)
            Serial.print("Read      :");
        else if(setNumCnt == 4)
            Serial.print("index     :");
#endif

        for (int i = 3; i >= 0; i--) {
            Serial.print(rx_data[i], HEX);
            Serial.print(" ");
        }

        setNumCnt++;
        if (setNumCnt >= DSP_SET_NUM) {
            Serial.println(F("\r\n=============="));
            setNumCnt = 0;
        }
        readAddr_dsp += 4;
        Serial.println("");
    }
    Serial.println(F("\r\n=======end of part ram======="));
    delay(5000);
}
#endif

#define PRINT_ALLREG_TEST                   (0)
    #define ALLREG_SET_NUM                  (4)

#if (PRINT_ALLREG_TEST == 1)
void print_allreg(void)
{
    U32 errorAddr = 0x00013000;
    U32 readAddr2 = 0x0002F800;
    U32 setNumCnt = 0;

    Serial.println(F("read All Reg test ram"));

    for (int j = 0; j < WriteTotal_Bytes; j++) {
        rx_data[0] = 0;
        rx_data[1] = 0;
        rx_data[2] = 0;
        rx_data[3] = 0;
        SPIUtility::readSingle32(readAddr2, rx_data);

        if (setNumCnt == 0)
            Serial.print(F("Rg Type   :"));
        else if (setNumCnt == 1)
            Serial.print(F("Rg Address:"));
        else if (setNumCnt == 2)
            Serial.print(F("Write/defaultValue:"));
        else if (setNumCnt == 3)
            Serial.print(F("Read      :"));
        else if (setNumCnt == 4)
            Serial.print(F("index     :"));

        for (int i = 3; i >= 0; i--) {
            Serial.print(rx_data[i], HEX);
            Serial.print(" ");
        }

        setNumCnt++;
        if (setNumCnt >= ALLREG_SET_NUM) {
            Serial.println(F("\r\n=============="));
            setNumCnt = 0;
        }
        readAddr2 += 4;
        Serial.println("");
    }

    Serial.print(F("error cnt:"));
    SPIUtility::readSingle32(errorAddr, rx_data);
    for (int i = 3; i >= 0; i--) {
        Serial.print(rx_data[i], HEX);
        Serial.print(F(" "));
    }
    Serial.println(F("\r\n=======end of part ram======="));
    delay(5000);
}
#endif

#define READ_CONTINOUS_RAM                  (0)

#if (READ_CONTINOUS_RAM == 1)
void read_continous_RAMinfo(void)
{
    U32 readAddr = 0x00011000;

    //read continues ram
    delay(100);
    Serial.println(F("read continues ram"));
    //for (int j = 0; j < 20; j++)
    for (int j = 0; j < WriteTotal_Bytes; j++)
    {
        SPIUtility::readSingle32(readAddr, rx_data) ;
        //for(int i = 3; i >= 0; i--)
        for (int i = 0; i < 4; i++) {
            Serial.print(rx_data[i], HEX);
            Serial.print(" ");
        }
        readAddr += 4;
        Serial.println("");
    }
    delay(10000);
    Serial.println(F("=======end of part ram======="));
}
#endif

#endif

#if (IT168_DMA_PRIORITY_TEST == 1)
#define ADDR1_START             0x1000
#define ADDR1_LEN               0x40000
#define ADDR1_TIMES             (ADDR1_LEN/4)
#define ADDR1_READ              (ADDR1_START + ADDR1_LEN)

#define ADDR0_START             0x100
#define ADDR0_LEN               0x100
#define ADDR0_READ              (ADDR0_START + ADDR0_LEN)
#define ADDR0_TIMES             (ADDR0_LEN/4)

#define HIGH_PRIORITY_SETTING   0x20280001
#define LOW_PRIORITY_SETTING    0x00280001
//if ch1 priority > ch0, ch0 process should wait until ch1 done
//else, ch0 process can interrupt ch1
void it168_dma_priority_cmd(void)
{
#define PRIORI_CMD_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[PRIORI_CMD_CMD_ARG_NUM_MAX];
    U32 argc;
    U32 rdata;
    U32 priority;
    char *arg;

    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= PRIORI_CMD_CMD_ARG_NUM_MAX) {
            Serial.println(F("err: too many arguments"));
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 16);
        argc += 1;
    }

    if (argc != PRIORI_CMD_CMD_ARG_NUM_MAX) {
        Serial.print(F("err: too few arguments "));
        Serial.print(argc);
        return;
    }

    if (arg_ary[0] > 1) {
        Serial.print(F("err: invalid argument "));
        Serial.print(arg_ary[1]);
        return;
    }

    priority = arg_ary[0];

    if (priority) {
        /* enable RISC-V JTAG pin */
        Serial.println(F("ch1 priority > ch 0"));

    }
    else {
        /* disable RISC-V JTAG pin */
        Serial.println(F("ch0 priority > ch 1"));
    }
    clean_sram();
    writesram();
    dma_setting(priority);
}
void clean_sram()
{
    uint32_t addr = 0, i = 0;
    Serial.println("sram initial start...");
    //for (i = 0; i < (ADDR1_START+ ADDR1_LEN*2); i++)
    for (i = 0; i < (500); i++)
    {
        SPIUtility::write_single32(addr, 0);
    }
    SPIUtility::write_single32((ADDR0_READ + 4), 0);
    Serial.println("sram initial done");

}
void writesram()
{
    Serial.println("\nch 1 write sram start: ");
    U32 addr = ADDR1_START, addr0 = ADDR0_START, value = 0x6666, i = 0;
    for (i = 0; i < ADDR1_TIMES; i++)
    {
        SPIUtility::write_single32(addr, value);
        //SPIUtility::write_single32(addr0, value);
        addr += 4;
    }
    Serial.println("\nch 1 write sram done: ");
    Serial.println("\nch 0 write sram start: ");
    value = 0x8888;
    for (i = 0; i < ADDR0_TIMES; i++)
    {
        SPIUtility::write_single32(addr0, value);
        addr0 += 4;
    }
    Serial.println("ch 0 write sram end: ");
}

void dma_setting(U32 pri_setting)
{
    U32 addr = ADDR1_START, i = 0, addr0 = ADDR0_START, temp = 0, ch1_pri_setting, ch0_pri_setting;
    /* -- ch 1 -- */
    //source address
    SPIUtility::write_single32(0x70000070, addr);

    //destination address
    SPIUtility::write_single32(0x70000074, (addr + ADDR1_LEN));

    //transfer size
    SPIUtility::write_single32(0x70000078, ADDR1_TIMES);

    /* -- ch 0 -- */
    //source address
    SPIUtility::write_single32(0x7000005C, addr0);

    //destination address
    SPIUtility::write_single32(0x70000060, (ADDR0_START + ADDR0_TIMES));

    //transfer size
    SPIUtility::write_single32(0x70000064, ADDR0_TIMES);

    if(pri_setting)
    {
        ch0_pri_setting = LOW_PRIORITY_SETTING;
        ch1_pri_setting = HIGH_PRIORITY_SETTING;
    }else
    {
        ch0_pri_setting = HIGH_PRIORITY_SETTING;
        ch1_pri_setting = LOW_PRIORITY_SETTING;
    }

    //ch1 control
    SPIUtility::write_single32(0x7000006c, ch1_pri_setting);

    //ch0 control
    SPIUtility::write_single32(0x70000058, ch0_pri_setting);

    while (SPIUtility::read_single32(ADDR0_READ+4) != 0x8888)
    {
        Serial.println("wait ch 1 done...");
    }
    Serial.println("ch 1 done!!");
    Serial.println("ch 0 done!!");
}
#endif

