#include <gpio_ae250.h>
#include <common.h>


void gpio_irq_setup(hal_gpio_pin_t gpio_pin, hal_irq_sel iqr_sel, hal_irq_trigger_type trg_type){
	//jsdbg GPIO interrupt
	//GPIO external interrupt
	//gpio_pin can select as HAL_INT_IN, HAL_GPIO_00, HAL_GPIO_01, HAL_GPIO_02, HAL_GPIO_03, HAL_GPIO_04, HAL_GPIO_05, HAL_GPIO_06, HAL_GPIO_07
	//iqr_sel can select as HAL_IRQ25, HAL_IRQ27, HAL_IRQ29
	//trg_type can select as HAL_IRQ_LEVEL, HAL_IRQ_EDGE, level trigger is high level to interrup and edge trigger is raising edge trigger
	uint32_t u32trg_gpio = 0;
	if(gpio_pin >= HAL_INT_IN && gpio_pin <= HAL_GPIO_07){
		hal_gpio_init(gpio_pin, HAL_INPUT);
		hal_gpio_get_intput(gpio_pin);
	}
	else return;

	u32trg_gpio = gpio_pin;
	if(gpio_pin != HAL_INT_IN){
		u32trg_gpio = gpio_pin-11;
	}

	if(iqr_sel == HAL_IRQ25){
		SetReg32( (uint32_t*)(rg_system_int_src_25_sel), (uint32_t)3, (uint32_t)0, (uint32_t)(u32trg_gpio) );
	}
	else if(iqr_sel == HAL_IRQ27){
		SetReg32( (uint32_t*)(rg_system_int_src_27_sel), (uint32_t)7, (uint32_t)4, (uint32_t)(u32trg_gpio) );
	}
	else if(iqr_sel == HAL_IRQ29){
		SetReg32( (uint32_t*)(rg_system_int_src_29_sel), (uint32_t)11, (uint32_t)8, (uint32_t)(u32trg_gpio) );
	}
	SetReg32( (uint32_t*)(rg_system_hw_trig_type), (uint32_t)iqr_sel, (uint32_t)iqr_sel, (uint32_t)(trg_type) );
	__nds__plic_enable_interrupt(iqr_sel);
}

/* interrupt implement
void irq25_handler(void){
//interrupt
}

void irq27_handler(void){
//interrupt
}

void irq29_handler(void){
//interrupt
}
*/

void hal_gpio_init (hal_gpio_pin_t gpio_pin, hal_gpio_mode_t gpio_mode ){
/*Verify in 20200406 by js
Describe:
This function is set gpio into input/output mode. gpio_mode set 0:output mode and  1:input mode*/
	if(gpio_mode > 1){
		return;
	}

	if(gpio_pin < HAL_UART1_RXD){
		SetReg32((uint32_t*)(&u32gpio_initial_hadle[0]),gpio_pin,gpio_pin,0x1);
		SetReg32((uint32_t*)(&u32gpio_mode_hadle_buf[0]),gpio_pin,gpio_pin,gpio_mode);
	}
	else if(gpio_pin >= HAL_UART1_RXD){
		SetReg32((uint32_t*)(&u32gpio_initial_hadle[1]),(gpio_pin-0x20),(gpio_pin-0x20),0x1);
		SetReg32((uint32_t*)(&u32gpio_mode_hadle_buf[1]),gpio_pin,gpio_pin,gpio_mode);
	}
	else
		return;
}


void hal_gpio_deinit (hal_gpio_pin_t gpio_pin){
	if(gpio_pin < HAL_UART1_RXD){
	//	u32gpio_initial_hadle[0] = u32gpio_initial_hadle[0] & (~(0x1<<gpio_pin));
		SetReg32((uint32_t*)(&u32gpio_initial_hadle[0]),gpio_pin,gpio_pin,0x0);
	}
	else if(gpio_pin >= HAL_UART1_RXD){
	//	u32gpio_initial_hadle[1] = u32gpio_initial_hadle[1] & (~(0x1<<(gpio_pin-32)));
		SetReg32((uint32_t*)(&u32gpio_initial_hadle[1]),(gpio_pin-0x20),(gpio_pin-0x20),0x0);
	}
	else
		return;

	if(gpio_pin <= HAL_UART1_RXD){
		switch(gpio_pin) {
			case HAL_SPI3_MOSI:
				/*rg_spi3_mosi_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_3_en), (uint32_t)4, (uint32_t)4, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi3_mosi_im), (uint32_t)22, (uint32_t)22, (uint32_t)(0));
				break;
			case HAL_SPI3_CK:
				/*rg_spi3_ck_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_3_en), (uint32_t)4, (uint32_t)4, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi3_ck_im), (uint32_t)24, (uint32_t)24, (uint32_t)(0) );
				break;
			case HAL_SPI3_HOLDN:
				/*rg_spi3_holdn_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_3_en), (uint32_t)4, (uint32_t)4, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi3_holdn_im), (uint32_t)26, (uint32_t)26, (uint32_t)(0) );
				break;
			case HAL_SPI2_CSN:
				/*rg_spi2_csn_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_2_en), (uint32_t)3, (uint32_t)3, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi2_csn_im), (uint32_t)0, (uint32_t)0, (uint32_t)(0) );
				break;
			case HAL_SPI2_MISO:
				/*rg_spi2_miso_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_2_en), (uint32_t)3, (uint32_t)3, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi2_miso_im), (uint32_t)2, (uint32_t)2, (uint32_t)(0) );
				break;
			case HAL_SPI2_WPN:
				/*rg_spi2_wpn_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_2_en), (uint32_t)3, (uint32_t)3, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi2_wpn_im), (uint32_t)4, (uint32_t)4, (uint32_t)(0) );
				break;
			case HAL_SPI0_CK:
				/*rg_spi0_ck_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_0_en), (uint32_t)1, (uint32_t)1, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi0_ck_im), (uint32_t)0, (uint32_t)0, (uint32_t)(0) );
				break;
			case HAL_SPI0_CSN:
				/*rg_spi0_csn_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_0_en), (uint32_t)1, (uint32_t)1, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi0_csn_im), (uint32_t)2, (uint32_t)2, (uint32_t)(0) );
				break;
			case HAL_SPI0_MOSI:
				/*rg_spi0_mosi_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_0_en), (uint32_t)1, (uint32_t)1, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi0_mosi_im), (uint32_t)4, (uint32_t)4, (uint32_t)(0) );
				break;
			case HAL_SPI0_MISO:
				/*rg_spi0_miso_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_0_en), (uint32_t)1, (uint32_t)1, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi0_miso_im), (uint32_t)6, (uint32_t)6, (uint32_t)(0) );
				break;
			case HAL_INT_IN:
				/*rg_interrupt_in_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_interrupt_in_im), (uint32_t)0, (uint32_t)0, (uint32_t)(0x1) );
				break;
			case HAL_GPIO_00:
				/*rg_gpio_00_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_00_in_output_sel), (uint32_t)16, (uint32_t)16, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_00_in_sel), (uint32_t)0, (uint32_t)0, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_00_im), (uint32_t)0, (uint32_t)0, (uint32_t)(0x1) );
				break;
			case HAL_GPIO_01:
				/*rg_gpio_01_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_01_in_output_sel), (uint32_t)17, (uint32_t)17, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_01_in_sel), (uint32_t)1, (uint32_t)1, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_01_im), (uint32_t)1, (uint32_t)1, (uint32_t)(0x0) );
				break;
			case HAL_GPIO_02:
				/*rg_gpio_02_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_02_in_output_sel), (uint32_t)18, (uint32_t)18, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_02_in_sel), (uint32_t)2, (uint32_t)2, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_02_im), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				break;
			case HAL_GPIO_03:
				/*rg_gpio_03_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_03_in_output_sel), (uint32_t)19, (uint32_t)19, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_03_in_sel), (uint32_t)3, (uint32_t)3, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_03_im), (uint32_t)3, (uint32_t)3, (uint32_t)(0x0) );
				break;
			case HAL_GPIO_04:
				/*rg_gpio_04_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_04_in_output_sel), (uint32_t)20, (uint32_t)20, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_04_in_sel), (uint32_t)4, (uint32_t)4, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_04_im), (uint32_t)4, (uint32_t)4, (uint32_t)(0x0) );
				break;
			case HAL_GPIO_05:
				/*rg_gpio_05_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_05_in_output_sel), (uint32_t)21, (uint32_t)21, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_05_in_sel), (uint32_t)5, (uint32_t)5, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_int_out_en), (uint32_t)8, (uint32_t)8, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_gpio_05_im), (uint32_t)5, (uint32_t)5, (uint32_t)(0) );
				break;
			case HAL_GPIO_06:
				/*rg_gpio_06_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_06_in_output_sel), (uint32_t)22, (uint32_t)22, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_06_in_sel), (uint32_t)6, (uint32_t)6, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_i2c_1_en), (uint32_t)6, (uint32_t)6, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_gpio_06_im), (uint32_t)6, (uint32_t)6, (uint32_t)(0) );
				break;
			case HAL_GPIO_07:
				/*rg_gpio_07_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_07_in_output_sel), (uint32_t)23, (uint32_t)23, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_07_in_sel), (uint32_t)7, (uint32_t)7, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_i2c_1_en), (uint32_t)6, (uint32_t)6, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_gpio_07_im), (uint32_t)7, (uint32_t)7, (uint32_t)(0) );
				break;
			case HAL_GPIO_08:
				/*rg_gpio_08_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_08_in_output_sel), (uint32_t)24, (uint32_t)24, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_08_in_sel), (uint32_t)8, (uint32_t)8, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_08_im), (uint32_t)8, (uint32_t)8, (uint32_t)(0x0) );
				break;
			case HAL_GPIO_09:
				/*rg_gpio_09_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_09_in_output_sel), (uint32_t)25, (uint32_t)25, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_09_in_sel), (uint32_t)9, (uint32_t)9, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_09_im), (uint32_t)9, (uint32_t)9, (uint32_t)(0x0) );
				break;
			case HAL_SPI2_MOSI:
				/*rg_spi2_mosi_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_2_en), (uint32_t)3, (uint32_t)3, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi2_mosi_im), (uint32_t)6, (uint32_t)6, (uint32_t)(0) );
				break;
			case HAL_SPI2_CK:
				/*rg_spi2_ck_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_2_en), (uint32_t)3, (uint32_t)3, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi2_ck_im), (uint32_t)8, (uint32_t)8, (uint32_t)(0) );
				break;
			case HAL_SPI2_HOLDN:
				/*rg_spi2_holdn_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_2_en), (uint32_t)3, (uint32_t)3, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi2_holdn_im), (uint32_t)10, (uint32_t)10, (uint32_t)(0) );
				break;
			case HAL_I2C_SCL_0:
				/*rg_i2c_scl_0_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_i2c_0_en), (uint32_t)5, (uint32_t)5, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_jtag_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_i2c_scl_0_im), (uint32_t)0, (uint32_t)0, (uint32_t)(0) );
				break;
			case HAL_I2C_SDA_0:
				/*rg_i2c_sda_0_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_i2c_0_en), (uint32_t)5, (uint32_t)5, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_jtag_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_i2c_sda_0_im), (uint32_t)2, (uint32_t)2, (uint32_t)(0) );
				break;
			case HAL_SPI3_CSN:
				/*rg_spi3_csn_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_3_en), (uint32_t)4, (uint32_t)4, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi3_csn_im), (uint32_t)16, (uint32_t)16, (uint32_t)(0) );
				break;
			case HAL_SPI3_MISO:
				/*rg_spi3_miso_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_3_en), (uint32_t)4, (uint32_t)4, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi3_miso_im), (uint32_t)18, (uint32_t)18, (uint32_t)(0) );
				break;
			case HAL_SPI3_WPN:
				/*rg_spi3_wpn_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_3_en), (uint32_t)4, (uint32_t)4, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_spi3_wpn_im), (uint32_t)20, (uint32_t)20, (uint32_t)(0) );
				break;
			case HAL_UART0_TXD:
				/*rg_uart0_txd_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_pad_uart0_txd_in_sel), (uint32_t)10, (uint32_t)10, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_system_uart0_txd_in_output_sel), (uint32_t)26, (uint32_t)26, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_uart_0_en), (uint32_t)7, (uint32_t)7, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_jtag_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_uart0_txd_im), (uint32_t)0, (uint32_t)0, (uint32_t)(0) );
				break;
			case HAL_UART0_RXD:
				/*rg_uart0_rxd_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_pad_uart0_rxd_in_sel), (uint32_t)11, (uint32_t)11, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_system_uart0_rxd_in_output_sel), (uint32_t)27, (uint32_t)27, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_uart_0_en), (uint32_t)7, (uint32_t)7, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_jtag_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_uart0_rxd_im), (uint32_t)2, (uint32_t)2, (uint32_t)(0) );
				break;
			case HAL_UART1_TXD:
				/*rg_uart1_txd_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_pad_uart1_txd_in_sel), (uint32_t)12, (uint32_t)12, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_system_uart1_txd_in_output_sel), (uint32_t)28, (uint32_t)28, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_uart_0_en), (uint32_t)7, (uint32_t)7, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_jtag_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_uart1_txd_im), (uint32_t)4, (uint32_t)4, (uint32_t)(0) );
				break;
			case HAL_UART1_RXD:
				/*rg_uart1_rxd_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_pad_uart1_rxd_in_sel), (uint32_t)13, (uint32_t)13, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_system_uart1_rxd_in_output_sel), (uint32_t)29, (uint32_t)29, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_uart_0_en), (uint32_t)7, (uint32_t)7, (uint32_t)(1) );
				SetReg32( (uint32_t*)(rg_jtag_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_uart1_rxd_im), (uint32_t)6, (uint32_t)6, (uint32_t)(0) );
				break;

			default:

				break;
		}
	}
	else
		return;
}



void hal_gpio_polling_handle(void){
	uint32_t i = 0,gpio_pin = 0xff;
	uint32_t gpio_mode = 0;

	for(i = HAL_SPI3_MOSI; i <= HAL_UART1_RXD; i++){
		gpio_pin = i;
		if(i < HAL_UART1_RXD){
			gpio_mode = ((u32gpio_mode_hadle_buf[0]>>i) & 0x1);

			if((((u32gpio_initial_hadle[0]>>i)&0x1))==0){
				gpio_pin = 0xff;
			}
		}
		else if(i >= HAL_UART1_RXD){
			gpio_mode = ((u32gpio_mode_hadle_buf[1]>>(i-0x20)) & 0x1);

			if((((u32gpio_initial_hadle[1]>>(i-0x20))&0x1))==0){
				gpio_pin = 0xff;
			}
		}

		switch(gpio_pin) {
			case HAL_SPI3_MOSI:
				/*rg_spi3_mosi_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_3_en), (uint32_t)4, (uint32_t)4, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi3_mosi_im), (uint32_t)22, (uint32_t)22, (uint32_t)(gpio_mode));
				break;
			case HAL_SPI3_CK:
				/*rg_spi3_ck_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_3_en), (uint32_t)4, (uint32_t)4, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi3_ck_im), (uint32_t)24, (uint32_t)24, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI3_HOLDN:
				/*rg_spi3_holdn_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_3_en), (uint32_t)4, (uint32_t)4, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi3_holdn_im), (uint32_t)26, (uint32_t)26, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI2_CSN:
				/*rg_spi2_csn_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_2_en), (uint32_t)3, (uint32_t)3, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi2_csn_im), (uint32_t)0, (uint32_t)0, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI2_MISO:
				/*rg_spi2_miso_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_2_en), (uint32_t)3, (uint32_t)3, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi2_miso_im), (uint32_t)2, (uint32_t)2, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI2_WPN:
				/*rg_spi2_wpn_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_2_en), (uint32_t)3, (uint32_t)3, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi2_wpn_im), (uint32_t)4, (uint32_t)4, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI0_CK:
				/*rg_spi0_ck_im
				*set 0:input 1:output*/
				SetReg32( (uint32_t*)(rg_spi_0_en), (uint32_t)1, (uint32_t)1, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi0_ck_im), (uint32_t)0, (uint32_t)0, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI0_CSN:
				/*rg_spi0_csn_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_0_en), (uint32_t)1, (uint32_t)1, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi0_csn_im), (uint32_t)2, (uint32_t)2, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI0_MOSI:
				/*rg_spi0_mosi_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_0_en), (uint32_t)1, (uint32_t)1, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi0_mosi_im), (uint32_t)4, (uint32_t)4, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI0_MISO:
				/*rg_spi0_miso_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_0_en), (uint32_t)1, (uint32_t)1, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi0_miso_im), (uint32_t)6, (uint32_t)6, (uint32_t)(gpio_mode) );
				break;
			case HAL_INT_IN:
				/*rg_interrupt_in_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_interrupt_in_im), (uint32_t)0, (uint32_t)0, (uint32_t)(gpio_mode) );
				break;
			case HAL_GPIO_00:
				/*rg_gpio_00_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_00_in_output_sel), (uint32_t)16, (uint32_t)16, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_00_in_sel), (uint32_t)0, (uint32_t)0, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_00_im), (uint32_t)0, (uint32_t)0, (uint32_t)(gpio_mode) );
				break;
			case HAL_GPIO_01:
				/*rg_gpio_01_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_01_in_output_sel), (uint32_t)17, (uint32_t)17, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_01_in_sel), (uint32_t)1, (uint32_t)1, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_01_im), (uint32_t)1, (uint32_t)1, (uint32_t)(gpio_mode) );
				break;
			case HAL_GPIO_02:
				/*rg_gpio_02_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_02_in_output_sel), (uint32_t)18, (uint32_t)18, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_02_in_sel), (uint32_t)2, (uint32_t)2, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_02_im), (uint32_t)2, (uint32_t)2, (uint32_t)(gpio_mode) );
				break;
			case HAL_GPIO_03:
				/*rg_gpio_03_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_03_in_output_sel), (uint32_t)19, (uint32_t)19, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_03_in_sel), (uint32_t)3, (uint32_t)3, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_03_im), (uint32_t)3, (uint32_t)3, (uint32_t)(gpio_mode) );
				break;
			case HAL_GPIO_04:
				/*rg_gpio_04_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_04_in_output_sel), (uint32_t)20, (uint32_t)20, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_04_in_sel), (uint32_t)4, (uint32_t)4, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_04_im), (uint32_t)4, (uint32_t)4, (uint32_t)(gpio_mode) );
				break;
			case HAL_GPIO_05:
				/*rg_gpio_05_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_05_in_output_sel), (uint32_t)21, (uint32_t)21, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_05_in_sel), (uint32_t)5, (uint32_t)5, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_int_out_en), (uint32_t)8, (uint32_t)8, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_05_im), (uint32_t)5, (uint32_t)5, (uint32_t)(gpio_mode) );
				break;
			case HAL_GPIO_06:
				/*rg_gpio_06_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_06_in_output_sel), (uint32_t)22, (uint32_t)22, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_06_in_sel), (uint32_t)6, (uint32_t)6, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_i2c_1_en), (uint32_t)6, (uint32_t)6, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_06_im), (uint32_t)6, (uint32_t)6, (uint32_t)(gpio_mode) );
				break;
			case HAL_GPIO_07:
				/*rg_gpio_07_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_07_in_output_sel), (uint32_t)23, (uint32_t)23, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_07_in_sel), (uint32_t)7, (uint32_t)7, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_i2c_1_en), (uint32_t)6, (uint32_t)6, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_07_im), (uint32_t)7, (uint32_t)7, (uint32_t)(gpio_mode) );
				break;
			case HAL_GPIO_08:
				/*rg_gpio_08_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_08_in_output_sel), (uint32_t)24, (uint32_t)24, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_08_in_sel), (uint32_t)8, (uint32_t)8, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_08_im), (uint32_t)8, (uint32_t)8, (uint32_t)(gpio_mode) );
				break;
			case HAL_GPIO_09:
				/*rg_gpio_09_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_gpio_09_in_output_sel), (uint32_t)25, (uint32_t)25, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_pad_gpio_09_in_sel), (uint32_t)9, (uint32_t)9, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_spi_slv_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_spi_1_en), (uint32_t)2, (uint32_t)2, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_gpio_09_im), (uint32_t)9, (uint32_t)9, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI2_MOSI:
				/*rg_spi2_mosi_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_2_en), (uint32_t)3, (uint32_t)3, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi2_mosi_im), (uint32_t)6, (uint32_t)6, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI2_CK:
				/*rg_spi2_ck_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_2_en), (uint32_t)3, (uint32_t)3, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi2_ck_im), (uint32_t)8, (uint32_t)8, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI2_HOLDN:
				/*rg_spi2_holdn_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_2_en), (uint32_t)3, (uint32_t)3, (uint32_t)(0) );
				SetReg32( (uint32_t*)(rg_spi2_holdn_im), (uint32_t)10, (uint32_t)10, (uint32_t)(gpio_mode) );
				break;
			case HAL_I2C_SCL_0:
				/*rg_i2c_scl_0_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_i2c_0_en), (uint32_t)5, (uint32_t)5, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_jtag_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_i2c_scl_0_im), (uint32_t)0, (uint32_t)0, (uint32_t)(gpio_mode) );
				break;
			case HAL_I2C_SDA_0:
				/*rg_i2c_sda_0_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_i2c_0_en), (uint32_t)5, (uint32_t)5, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_jtag_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_i2c_sda_0_im), (uint32_t)2, (uint32_t)2, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI3_CSN:
				/*rg_spi3_csn_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_3_en), (uint32_t)4, (uint32_t)4, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_spi3_csn_im), (uint32_t)16, (uint32_t)16, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI3_MISO:
				/*rg_spi3_miso_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_3_en), (uint32_t)4, (uint32_t)4, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_spi3_miso_im), (uint32_t)18, (uint32_t)18, (uint32_t)(gpio_mode) );
				break;
			case HAL_SPI3_WPN:
				/*rg_spi3_wpn_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_spi_3_en), (uint32_t)4, (uint32_t)4, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_spi3_wpn_im), (uint32_t)20, (uint32_t)20, (uint32_t)(gpio_mode) );
				break;
			case HAL_UART0_TXD:
				/*rg_uart0_txd_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_pad_uart0_txd_in_sel), (uint32_t)10, (uint32_t)10, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_uart0_txd_in_output_sel), (uint32_t)26, (uint32_t)26, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_uart_0_en), (uint32_t)7, (uint32_t)7, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_jtag_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_uart0_txd_im), (uint32_t)0, (uint32_t)0, (uint32_t)(gpio_mode) );
				break;
			case HAL_UART0_RXD:
				/*rg_uart0_rxd_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_pad_uart0_rxd_in_sel), (uint32_t)11, (uint32_t)11, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_uart0_rxd_in_output_sel), (uint32_t)27, (uint32_t)27, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_uart_0_en), (uint32_t)7, (uint32_t)7, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_jtag_en), (uint32_t)0, (uint32_t)0, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_uart0_rxd_im), (uint32_t)2, (uint32_t)2, (uint32_t)(gpio_mode) );
				break;
			case HAL_UART1_TXD:
				/*rg_uart1_txd_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_pad_uart1_txd_in_sel), (uint32_t)12, (uint32_t)12, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_uart1_txd_in_output_sel), (uint32_t)28, (uint32_t)28, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_uart_0_en), (uint32_t)7, (uint32_t)7, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_uart1_txd_im), (uint32_t)4, (uint32_t)4, (uint32_t)(gpio_mode) );
				break;
			case HAL_UART1_RXD:
				/*rg_uart1_rxd_im
				*set 0:output 1:input*/
				SetReg32( (uint32_t*)(rg_system_pad_uart1_rxd_in_sel), (uint32_t)13, (uint32_t)13, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_system_uart1_rxd_in_output_sel), (uint32_t)29, (uint32_t)29, (uint32_t)(0x1) );
				SetReg32( (uint32_t*)(rg_uart_0_en), (uint32_t)7, (uint32_t)7, (uint32_t)(0x0) );
				SetReg32( (uint32_t*)(rg_uart1_rxd_im), (uint32_t)6, (uint32_t)6, (uint32_t)(gpio_mode) );
				break;
			default:
				break;
		}
	}
}



void hal_gpio_set_output (hal_gpio_pin_t gpio_pin, hal_gpio_output_t gpio_data){

	hal_gpio_polling_handle();

	if(gpio_data > 1) gpio_data = 1;
	switch(gpio_pin) {
	    case HAL_SPI3_MOSI:
			SetReg32( (uint32_t*)(rg_spi3_mosi_in), (uint32_t)23, (uint32_t)23, (uint32_t)(gpio_data) );
	        break;
	    case HAL_SPI3_CK:
	        SetReg32( (uint32_t*)(rg_spi3_ck_in), (uint32_t)25, (uint32_t)25, (uint32_t)(gpio_data) );
	        break;
		case HAL_SPI3_HOLDN:
	        SetReg32( (uint32_t*)(rg_spi3_holdn_in), (uint32_t)27, (uint32_t)27, (uint32_t)(gpio_data) );
	        break;
		case HAL_SPI2_CSN:
	        SetReg32( (uint32_t*)(rg_spi2_csn_in), (uint32_t)1, (uint32_t)1, (uint32_t)(gpio_data) );
	        break;
		case HAL_SPI2_MISO:
	        SetReg32( (uint32_t*)(rg_spi2_miso_in), (uint32_t)3, (uint32_t)3, (uint32_t)(gpio_data) );
	        break;
		case HAL_SPI2_WPN:
	        SetReg32( (uint32_t*)(rg_spi2_wpn_in), (uint32_t)5, (uint32_t)5, (uint32_t)(gpio_data) );
	        break;
		case HAL_SPI0_CK:
	        SetReg32( (uint32_t*)(rg_spi0_ck_in), (uint32_t)1, (uint32_t)1, (uint32_t)(gpio_data) );
	        break;
		case HAL_SPI0_CSN:
	        SetReg32( (uint32_t*)(rg_spi0_csn_in), (uint32_t)3, (uint32_t)3, (uint32_t)(gpio_data) );
	        break;
		case HAL_SPI0_MOSI:
	        SetReg32( (uint32_t*)(rg_spi0_mosi_in), (uint32_t)5, (uint32_t)5, (uint32_t)(gpio_data) );
	        break;
		case HAL_SPI0_MISO:
	        SetReg32( (uint32_t*)(rg_spi0_miso_in), (uint32_t)7, (uint32_t)7, (uint32_t)(gpio_data) );
	        break;
		case HAL_INT_IN:
	        SetReg32( (uint32_t*)(rg_interrupt_in_in), (uint32_t)8, (uint32_t)8, (uint32_t)(gpio_data) );
	        break;
		case HAL_GPIO_00:
	        SetReg32( (uint32_t*)(rg_system_gpio_00_in_output_value), (uint32_t)0, (uint32_t)0, (uint32_t)(gpio_data) );
	        break;
		case HAL_GPIO_01:
	        SetReg32( (uint32_t*)(rg_system_gpio_01_in_output_value), (uint32_t)1, (uint32_t)1, (uint32_t)(gpio_data) );
	        break;
		case HAL_GPIO_02:
			SetReg32( (uint32_t*)(rg_system_gpio_02_in_output_value), (uint32_t)2, (uint32_t)2, (uint32_t)(gpio_data) );
	        break;
		case HAL_GPIO_03:
			SetReg32( (uint32_t*)(rg_system_gpio_03_in_output_value), (uint32_t)3, (uint32_t)3, (uint32_t)(gpio_data) );
	        break;
		case HAL_GPIO_04:
			SetReg32( (uint32_t*)(rg_system_gpio_04_in_output_value), (uint32_t)4, (uint32_t)4, (uint32_t)(gpio_data) );
	        break;
		case HAL_GPIO_05:
	        SetReg32( (uint32_t*)(rg_system_gpio_05_in_output_value), (uint32_t)5, (uint32_t)5, (uint32_t)(gpio_data) );
	        break;
		case HAL_GPIO_06:
	        SetReg32( (uint32_t*)(rg_system_gpio_06_in_output_value), (uint32_t)6, (uint32_t)6, (uint32_t)(gpio_data) );
	        break;
		case HAL_GPIO_07:
	        SetReg32( (uint32_t*)(rg_system_gpio_07_in_output_value), (uint32_t)7, (uint32_t)7, (uint32_t)(gpio_data) );
	        break;
		case HAL_GPIO_08:
	        SetReg32( (uint32_t*)(rg_system_gpio_08_in_output_value), (uint32_t)8, (uint32_t)8, (uint32_t)(gpio_data) );
	        break;
		case HAL_GPIO_09:
	        SetReg32( (uint32_t*)(rg_system_gpio_09_in_output_value), (uint32_t)9, (uint32_t)9, (uint32_t)(gpio_data) );
	        break;
		case HAL_SPI2_MOSI:
			SetReg32( (uint32_t*)(rg_spi2_mosi_in), (uint32_t)7, (uint32_t)7, (uint32_t)(gpio_data) );
			break;
		case HAL_SPI2_CK:
			SetReg32( (uint32_t*)(rg_spi2_ck_in), (uint32_t)9, (uint32_t)9, (uint32_t)(gpio_data) );
			break;
		case HAL_SPI2_HOLDN:
			SetReg32( (uint32_t*)(rg_spi2_holdn_in), (uint32_t)11, (uint32_t)11, (uint32_t)(gpio_data) );
			break;
		case HAL_I2C_SCL_0:
			SetReg32( (uint32_t*)(rg_i2c_scl_0_in), (uint32_t)1, (uint32_t)1, (uint32_t)(gpio_data) );
			break;
		case HAL_I2C_SDA_0:
			SetReg32( (uint32_t*)(rg_i2c_sda_0_in), (uint32_t)3, (uint32_t)3, (uint32_t)(gpio_data) );
			break;
		case HAL_SPI3_CSN:
			SetReg32( (uint32_t*)(rg_spi3_csn_in), (uint32_t)17, (uint32_t)17, (uint32_t)(gpio_data) );
			break;
		case HAL_SPI3_MISO:
			SetReg32( (uint32_t*)(rg_spi3_miso_in), (uint32_t)19, (uint32_t)19, (uint32_t)(gpio_data) );
			break;
		case HAL_SPI3_WPN:
			SetReg32( (uint32_t*)(rg_spi3_wpn_in), (uint32_t)21, (uint32_t)21, (uint32_t)(gpio_data) );
			break;
		case HAL_UART0_TXD:
			SetReg32( (uint32_t*)(rg_uart0_txd_in), (uint32_t)1, (uint32_t)1, (uint32_t)(gpio_data) );
			break;
		case HAL_UART0_RXD:
			SetReg32( (uint32_t*)(rg_uart0_rxd_in), (uint32_t)3, (uint32_t)3, (uint32_t)(gpio_data) );
			break;
		case HAL_UART1_TXD:
			SetReg32( (uint32_t*)(rg_uart1_txd_in), (uint32_t)5, (uint32_t)5, (uint32_t)(gpio_data) );
			break;
		case HAL_UART1_RXD:
			SetReg32((uint32_t*)(rg_uart1_rxd_in), (uint32_t)7, (uint32_t)7, (uint32_t)(gpio_data));
			break;
	    default:
	        break;
	}
}


uint32_t hal_gpio_get_intput (hal_gpio_pin_t gpio_pin){
	uint32_t	bitRead = 0;

	hal_gpio_polling_handle();

	switch(gpio_pin) {
	    case HAL_SPI3_MOSI:
			bitRead = BIT32_READ_BIT(ro_spi3_mosi_out,13);
	        break;
	    case HAL_SPI3_CK:
			bitRead = BIT32_READ_BIT(ro_spi3_ck_out,14);
			break;
		case HAL_SPI3_HOLDN:
			bitRead = BIT32_READ_BIT(ro_spi3_holdn_out,15);
			break;
		case HAL_SPI2_CSN:
			bitRead = BIT32_READ_BIT(ro_spi2_csn_out,4);
			break;
		case HAL_SPI2_MISO:
	        bitRead = BIT32_READ_BIT(ro_spi2_miso_out,5);
	        break;
		case HAL_SPI2_WPN:
	        bitRead = BIT32_READ_BIT(ro_spi2_wpn_out,6);
			break;
		case HAL_SPI0_CK:
			bitRead = BIT32_READ_BIT(ro_spi0_ck_out,0);
			break;
		case HAL_SPI0_CSN:
			bitRead = BIT32_READ_BIT(ro_spi0_csn_out,1);
			break;
		case HAL_SPI0_MOSI:
			bitRead = BIT32_READ_BIT(ro_spi0_mosi_out,2);
		    break;
		case HAL_SPI0_MISO:
			bitRead = BIT32_READ_BIT(ro_spi0_miso_out,3);
			break;
		case HAL_INT_IN:
	   	   	bitRead = BIT32_READ_BIT(ro_interrupt_in_out,0);
	        break;
		case HAL_GPIO_00:
			bitRead = BIT32_READ_BIT(ro_system_gpio_00_out,0);
			break;
		case HAL_GPIO_01:
			bitRead = BIT32_READ_BIT(ro_system_gpio_01_out,1);
	        break;
		case HAL_GPIO_02:
			bitRead = BIT32_READ_BIT(ro_system_gpio_02_out,2);
	        break;
		case HAL_GPIO_03:
			bitRead = BIT32_READ_BIT(ro_system_gpio_03_out,3);
			break;
		case HAL_GPIO_04:
			bitRead = BIT32_READ_BIT(ro_system_gpio_04_out,4);
			break;
		case HAL_GPIO_05:
			bitRead = BIT32_READ_BIT(ro_system_gpio_05_out,5);
		    break;
		case HAL_GPIO_06:
			bitRead = BIT32_READ_BIT(ro_system_gpio_06_out,6);
			break;
		case HAL_GPIO_07:
			bitRead = BIT32_READ_BIT(ro_system_gpio_07_out,7);
			break;
		case HAL_GPIO_08:
			bitRead = BIT32_READ_BIT(ro_system_gpio_08_out,8);
			break;
		case HAL_GPIO_09:
			bitRead = BIT32_READ_BIT(ro_system_gpio_09_out,9);
	        break;
		case HAL_SPI2_MOSI:
			bitRead = BIT32_READ_BIT(ro_spi2_mosi_out,7);
			break;
		case HAL_SPI2_CK:
			bitRead = BIT32_READ_BIT(ro_spi2_ck_out,8);
			break;
		case HAL_SPI2_HOLDN:
			bitRead = BIT32_READ_BIT(ro_spi2_holdn_out,9);
			break;
		case HAL_I2C_SCL_0:
			bitRead = BIT32_READ_BIT(ro_i2c_scl_0_out,16);
			break;
		case HAL_I2C_SDA_0:
			bitRead = BIT32_READ_BIT(ro_i2c_sda_0_out,17);
			break;
		case HAL_SPI3_CSN:
			bitRead = BIT32_READ_BIT(ro_spi3_csn_out,10);
			break;
		case HAL_SPI3_MISO:
			bitRead = BIT32_READ_BIT(ro_spi3_miso_out,11);
			break;
		case HAL_SPI3_WPN:
			bitRead = BIT32_READ_BIT(ro_spi3_wpn_out,12);
			break;
		case HAL_UART0_TXD:
			bitRead = BIT32_READ_BIT(ro_uart0_txd_out,18);
			break;
		case HAL_UART0_RXD:
			bitRead = BIT32_READ_BIT(ro_uart0_rxd_out,19);
			break;
		case HAL_UART1_TXD:
			bitRead = BIT32_READ_BIT(ro_uart1_txd_out,20);
			break;
		case HAL_UART1_RXD:
			bitRead = BIT32_READ_BIT(ro_uart1_rxd_out,21);
			break;
	    default:
	        break;
	}
	return ((uint32_t)bitRead);
}



inline void SetReg32(uint32_t* addr, uint32_t MSB, uint32_t LSB, uint32_t value){
	*((uint32_t*) addr) = (( (*addr) & (~(BIT32_MASK(LSB,(MSB-LSB+1)))) ) | (value<<LSB));
}



