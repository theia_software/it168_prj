/*
 * Project:      WDT (Watchdog Timer)
 *               Driver definitions
 */

#ifndef __DRIVER_TIME_H
#define __DRIVER_TIME_H

#include "Driver_Common.h"

#define TESTDEBUG

typedef void (*NDS_TIME_SignalEvent_t) (uint32_t event);  ///< Pointer to \ref NDS_WDT_SignalEvent : Signal WDT Event.



/*
  \fn          void Initialize (void)
  \brief       Init RISC-V time ticks and gen up time(g_ticks) in ms for start_timer_ms/sw_delay_ms use
  \return      no return

  \fn          uint32_t Gettime (void)
  \brief       Get up time in ms
  \return      return up time in ms

  \fn          int start_timer_ms (uint32_t  timeout_time_ms, NDS_TIME_SignalEvent_t  callback, bool noloop)
  \brief       This function starts the software timeout timer.
  \param[in]   timeout_time_ms : the timeout value in milliseconds, timeout_time_ms must > 1 ms.
  \param[in]   callback : is the callback to be registered with this timer. This callback will be called when the timer times out.
  \param[in]   noloop : set 1 for one shot,noloop set 0 for loop
  \return      return 0 : setup success.  return 1 : setup fail(timeout_time_ms <= 1ms)

  \fn          int sw_get_remaining_time_ms (uint32_t *  remaing_time)
  \brief       This function gets the remaining timeout value of the software timeput timer.
  \param[out]  remaing_time : is the pointer to the value of the remaining timeout, after the return of this function.
  \return      return 0 : success. Other wise : fail


  \fn          void sw_stop_timer_ms (void)
  \brief       This function stops the software timeout function.
  \return      no return

  \fn          void sw_delay_ms (uint32_t delay_ms)
  \brief       This function sets the delay time in milliseconds.
  \param[in] delay_ms : is the delay time in milliseconds.
  \return      no return

 */


typedef struct _NDS_DRIVER_TIME {
	void                 (*Initialize)      (void);  // Init RISC-V time ticks and gen up time(g_ticks) in ms for start_timer_ms/sw_delay_ms use
	uint32_t			 (*Gettime) 		(void);  // retuen up time in ms(g_ticks)
	int             	 (*start_timer_ms)  (uint32_t  timeout_time_ms, NDS_TIME_SignalEvent_t  callback, bool noloop); //This function starts the software timer.
	int					 (*sw_get_remaining_time_ms) (uint32_t *  remaing_time); //This function gets the remaining timeout value of the specified software timer.
	void              	 (*sw_stop_timer_ms) (void); //This function stops the software timeout function.
	void				 (*sw_delay_ms)		(uint32_t delay_ms); //This function sets the delay time in milliseconds.

}const NDS_DRIVER_TIME;

#endif /* __DRIVER_TIME_H */
