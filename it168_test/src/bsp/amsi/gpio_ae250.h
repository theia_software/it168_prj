#ifndef __GPIO_AE250_H
#define __GPIO_AE250_H

#include <ae250.h>
//#include <_types.h>
#include <stdio.h>
#include "platform.h"
#include <stdbool.h>

//#include <stdlib.h>
//#include <ParameterSetting.h>

//#define BIT32_MASK(off, len)	((uint32_t)((~((~0ul) << (len))) << (off)))
#define BIT32_READ_BIT(addr, sbit)	((uint32_t)(((*((uint32_t*)addr)) >> sbit) & 0x1))

//Register table
#define rg_gpio_00_im (0x100000b0)		//  gpio_00_im  0 0
#define rg_gpio_01_im (rg_gpio_00_im)	//  gpio_01_im  1 1
#define rg_gpio_02_im (rg_gpio_00_im)	//  gpio_02_im  2 2
#define rg_gpio_03_im (rg_gpio_00_im)	//  gpio_03_im  3 3
#define rg_gpio_04_im (rg_gpio_00_im)	//  gpio_04_im  4 4
#define rg_gpio_05_im (rg_gpio_00_im)	//  gpio_05_im  5 5
#define rg_gpio_06_im (rg_gpio_00_im)	//  gpio_06_im  6 6
#define rg_gpio_07_im (rg_gpio_00_im)	//  gpio_07_im  7 7
#define rg_gpio_08_im (rg_gpio_00_im)	//  gpio_08_im  8 8
#define rg_gpio_09_im (rg_gpio_00_im)	//  gpio_09_im  9 9

#define rg_system_rstb_mode (0x100000b8)//  system_rstb_mode  0 0

#define rg_spi_slv_en (0x10000674)	//  spi_slv_en  0 0
#define rg_spi_0_en (rg_spi_slv_en)	//  spi_0_en  1 1
#define rg_spi_1_en (rg_spi_0_en)	//  spi_1_en  2 2
#define rg_spi_2_en (rg_spi_0_en)	//  spi_3_en  3 3  RW  1'b1  pad enable for spi 3
#define rg_spi_3_en (rg_spi_0_en)	//  spi_3_en  4 4
#define rg_i2c_0_en (rg_spi_0_en)	//  i2c_0_en  5 5
#define rg_i2c_1_en (rg_spi_0_en)	//  i2c_1_en  6 6
#define rg_uart_0_en (rg_spi_slv_en)//  uart_0_en 7 7
//#define rg_uart_1_en (rg_spi_slv_en)//  uart_1_en 8 8
#define rg_int_out_en (rg_spi_slv_en)//  int_out_en  8 8


#define rg_system_pad_gpio_00_in_sel (0x100002f4)//  system_pad_gpio_00_in_sel 0 0
#define rg_system_pad_gpio_01_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_gpio_01_in_sel 1 1
#define rg_system_pad_gpio_02_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_gpio_02_in_sel 2 2
#define rg_system_pad_gpio_03_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_gpio_03_in_sel 3 3
#define rg_system_pad_gpio_04_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_gpio_04_in_sel 4 4
#define rg_system_pad_gpio_05_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_gpio_05_in_sel 5 5
#define rg_system_pad_gpio_06_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_gpio_06_in_sel 6 6
#define rg_system_pad_gpio_07_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_gio_07_in_sel 7 7
#define rg_system_pad_gpio_08_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_gpio_08_in_sel 8 8
#define rg_system_pad_gpio_09_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_gpio_09_in_sel 9 9
#define rg_system_pad_uart0_txd_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_uart0_txd_in_sel 10  10
#define rg_system_pad_uart0_rxd_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_uart0_rxd_in_sel 11  11
#define rg_system_pad_uart1_txd_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_uart1_txd_in_sel 12  12
#define rg_system_pad_uart1_rxd_in_sel (rg_system_pad_gpio_00_in_sel)//  system_pad_uart1_rxd_in_sel 13  13
#define rg_system_gpio_00_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_gpio_00_in_output_sel  16  16
#define rg_system_gpio_01_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_gpio_01_in_output_sel  17  17
#define rg_system_gpio_02_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_gpio_02_in_output_sel  18  18
#define rg_system_gpio_03_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_gpio_03_in_output_sel  19  19
#define rg_system_gpio_04_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_gpio_04_in_output_sel  20  20
#define rg_system_gpio_05_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_gpio_05_in_output_sel  21  21
#define rg_system_gpio_06_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_gpio_06_in_output_sel  22  22
#define rg_system_gpio_07_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_gpio_07_in_output_sel  23  23
#define rg_system_gpio_08_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_gpio_08_in_output_sel  24  24
#define rg_system_gpio_09_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_gpio_09_in_output_sel  25  25
#define rg_system_uart0_txd_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_uart0_txd_in_output_sel  26  26
#define rg_system_uart0_rxd_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_uart0_rxd_in_output_sel  27  27
#define rg_system_uart1_txd_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_uart1_txd_in_output_sel  28  28
#define rg_system_uart1_rxd_in_output_sel (rg_system_pad_gpio_00_in_sel)//  system_uart1_rxd_in_output_sel  29  29


#define rg_system_gpio_00_in_output_value (0x100002f8)//  system_gpio_00_in_output_value  0 0
#define rg_system_gpio_01_in_output_value (rg_system_gpio_00_in_output_value)//  system_gpio_01_in_output_value  1 1
#define rg_system_gpio_02_in_output_value (rg_system_gpio_00_in_output_value)//  system_gpio_02_in_output_value  2 2
#define rg_system_gpio_03_in_output_value (rg_system_gpio_00_in_output_value)//  system_gpio_03_in_output_value  3 3
#define rg_system_gpio_04_in_output_value (rg_system_gpio_00_in_output_value)//  system_gpio_04_in_output_value  4 4
#define rg_system_gpio_05_in_output_value (rg_system_gpio_00_in_output_value)//  system_gpio_05_in_output_value  5 5
#define rg_system_gpio_06_in_output_value (rg_system_gpio_00_in_output_value)//  system_gpio_06_in_output_value  6 6
#define rg_system_gpio_07_in_output_value (rg_system_gpio_00_in_output_value)//  system_gpio_07_in_output_value  7 7
#define rg_system_gpio_08_in_output_value (rg_system_gpio_00_in_output_value)//  system_gpio_08_in_output_value  8 8
#define rg_system_gpio_09_in_output_value (rg_system_gpio_00_in_output_value)//  system_gpio_09_in_output_value  9 9


#define ro_system_gpio_00_out (0x1000032c)				//  system_gpio_00_out  0 0
#define ro_system_gpio_01_out (ro_system_gpio_00_out)	//  system_gpio_01_out  1 1
#define ro_system_gpio_02_out (ro_system_gpio_00_out)	//  system_gpio_02_out  2 2
#define ro_system_gpio_03_out (ro_system_gpio_00_out)	//  system_gpio_03_out  3 3
#define ro_system_gpio_04_out (ro_system_gpio_00_out)	//  system_gpio_04_out  4 4
#define ro_system_gpio_05_out (ro_system_gpio_00_out)	//  system_gpio_05_out  5 5
#define ro_system_gpio_06_out (ro_system_gpio_00_out)	//  system_gpio_06_out  6 6
#define ro_system_gpio_07_out (ro_system_gpio_00_out)	//  system_gpio_07_out  7 7
#define ro_system_gpio_08_out (ro_system_gpio_00_out)	//  system_gpio_08_out  8 8
#define ro_system_gpio_09_out (ro_system_gpio_00_out)	//  system_gpio_09_out  9 9

#define ro_interrupt_in_out (0x1000034c)	//interrupt_in_out	0	0


#define rg_spi0_ck_im (0x10000678)		//  spi0_ck_im  0 0  RW  1'b0  polarity control of pad spi0_ck for gpio use    v v
#define rg_spi0_ck_in (rg_spi0_ck_im)	//  spi0_ck_in  1 1  RW  1'b0  output data of pad spi0_ck for gpio use   v v
#define rg_spi0_csn_im (rg_spi0_ck_im)	//  spi0_csn_im 2 2   RW  1'b0  polarity control of pad spi0_csn for gpio use   v v
#define rg_spi0_csn_in (rg_spi0_ck_im)	//  spi0_csn_in 3 3   RW  1'b0  output data of pad spi0_csn for gpio use    v v
#define rg_spi0_mosi_im (rg_spi0_ck_im)	//  spi0_mosi_im  4 4  RW  1'b0  polarity control of pad spi0_mosi for gpio use    v v
#define rg_spi0_mosi_in (rg_spi0_ck_im)	//  spi0_mosi_in  5 5  RW  1'b0  output data of pad spi0_mosi for gpio use   v v
#define rg_spi0_miso_im (rg_spi0_ck_im)	//  spi0_miso_im  6 6  RW  1'b0  polarity control of pad spi0_miso for gpio use    v v
#define rg_spi0_miso_in (rg_spi0_ck_im)	//  spi0_miso_in  7 7  RW  1'b0  output data of pad spi0_miso for gpio use   v v

#define rg_spi2_csn_im 		(0x1000067c)	//  spi2_csn_im 0 0   RW  1'b0  polarity control of pad spi2_csn for gpio use   v v
#define rg_spi2_csn_in 		(rg_spi2_csn_im)//  spi2_csn_in 1 1   RW  1'b0  output data of pad spi2_csn for gpio use    v v
#define rg_spi2_miso_im 	(rg_spi2_csn_im)//  spi2_miso_im  2 2  RW  1'b0  polarity control of pad spi2_miso for gpio use    v v
#define rg_spi2_miso_in 	(rg_spi2_csn_im)//  spi2_miso_in  3 3  RW  1'b0  output data of pad spi2_miso for gpio use   v v
#define rg_spi2_wpn_im 		(rg_spi2_csn_im)//  spi2_wpn_im 4 4   RW  1'b0  polarity control of pad spi2_wpn for gpio use   v v
#define rg_spi2_wpn_in 		(rg_spi2_csn_im)//  spi2_wpn_in 5 5   RW  1'b0  output data of pad spi2_wpn for gpio use    v v
#define rg_spi2_mosi_im 	(rg_spi2_csn_im)//  spi2_mosi_im  6 6
#define rg_spi2_mosi_in 	(rg_spi2_csn_im)//  spi2_mosi_in  7 7
#define rg_spi2_ck_im 		(rg_spi2_csn_im)//  spi2_ck_im  8 8
#define rg_spi2_ck_in 		(rg_spi2_csn_im)//  spi2_ck_in  9 9
#define rg_spi2_holdn_im 	(rg_spi2_csn_im)//  spi2_holdn_im 10  10
#define rg_spi2_holdn_in 	(rg_spi2_csn_im)//  spi2_holdn_in 11  11
#define rg_spi3_csn_im		(rg_spi2_csn_im)//  spi3_csn_im 16  16
#define rg_spi3_csn_in  	(rg_spi3_csn_im)//  spi3_csn_in 17  17
#define rg_spi3_miso_im 	(rg_spi3_csn_im)//  spi3_miso_im  18  18
#define rg_spi3_miso_in  	(rg_spi3_csn_im)//  spi3_miso_in  19  19
#define rg_spi3_wpn_im 		(rg_spi3_csn_im)//  spi3_wpn_im 20  20
#define rg_spi3_wpn_in 		(rg_spi3_csn_im)//  spi3_wpn_in 21  21
#define rg_spi3_mosi_im 	(rg_spi3_csn_im)//  spi3_mosi_im  22  22   RW  1'b0  polarity control of pad spi3_mosi for gpio use
#define rg_spi3_mosi_in 	(rg_spi3_csn_im)//  spi3_mosi_in  23  23   RW  1'b0  output data of pad spi3_mosi for gpio use
#define rg_spi3_ck_im 		(rg_spi3_csn_im)//  spi3_ck_im  24  24   RW  1'b0  polarity control of pad spi3_ck for gpio use
#define rg_spi3_ck_in 		(rg_spi3_csn_im)//  spi3_ck_in  25  25   RW  1'b0  output data of pad spi3_ck for gpio use
#define rg_spi3_holdn_im 	(rg_spi3_csn_im)//  spi3_holdn_im 26  26    RW  1'b0  polarity control of pad spi3_holdn for gpio use
#define rg_spi3_holdn_in 	(rg_spi3_csn_im)//  spi3_holdn_in 27  27    RW  1'b0  output data of pad spi3_holdn for gpio use


#define rg_i2c_scl_0_im (0x10000680)//  i2c_scl_0_im  0 0
#define rg_i2c_scl_0_in (rg_i2c_scl_0_im)//  i2c_scl_0_in  1 1
#define rg_i2c_sda_0_im (rg_i2c_scl_0_im)//  i2c_sda_0_im  2 2
#define rg_i2c_sda_0_in (rg_i2c_scl_0_im)//  i2c_sda_0_in  3 3

#define rg_uart0_txd_im (0x10000684)//  uart0_txd_im  0 0
#define rg_uart0_txd_in (rg_uart0_txd_im)//  uart0_txd_in  1 1
#define rg_uart0_rxd_im (rg_uart0_txd_im)//  uart0_rxd_im  2 2
#define rg_uart0_rxd_in (rg_uart0_txd_im)//  uart0_rxd_in  3 3
#define rg_uart1_txd_im (rg_uart0_txd_im)//  uart1_txd_im  4 4
#define rg_uart1_txd_in (rg_uart0_txd_im)//  uart1_txd_in  5 5
#define rg_uart1_rxd_im (rg_uart0_txd_im)//  uart1_rxd_im  6 6
#define rg_uart1_rxd_in (rg_uart0_txd_im)//  uart1_rxd_in  7 7


#define	ro_spi0_ck_out 	  (0x1000068c)		//	spi0_ck_out       	0	0
#define	ro_spi0_csn_out   (ro_spi0_ck_out)	//	spi0_csn_out      	1	1
#define	ro_spi0_mosi_out  (ro_spi0_ck_out)	//  spi0_mosi_out     	2	2
#define	ro_spi0_miso_out  (ro_spi0_ck_out)	//	spi0_miso_out     	3	3
#define	ro_spi2_csn_out   (ro_spi0_ck_out)	//	spi2_csn_out      	4	4
#define	ro_spi2_miso_out  (ro_spi0_ck_out)	//	spi2_miso_out     	5	5
#define	ro_spi2_wpn_out   (ro_spi0_ck_out)	//	spi2_wpn_out      	6	6
#define	ro_spi2_mosi_out  (ro_spi0_ck_out)	//	spi2_mosi_out     	7	7
#define	ro_spi2_ck_out    (ro_spi0_ck_out)	//  spi2_ck_out       	8	8
#define	ro_spi2_holdn_out (ro_spi0_ck_out)	//	spi2_holdn_out    	9	9
#define	ro_spi3_csn_out   (ro_spi0_ck_out)	//	spi3_csn_out      	10	10
#define	ro_spi3_miso_out  (ro_spi0_ck_out)	//	spi3_miso_out     	11	11
#define	ro_spi3_wpn_out   (ro_spi0_ck_out)	//	spi3_wpn_out      	12	12
#define	ro_spi3_mosi_out  (ro_spi0_ck_out)	//	spi3_mosi_out     	13	13
#define	ro_spi3_ck_out    (ro_spi0_ck_out)	//	spi3_ck_out       	14	14
#define	ro_spi3_holdn_out (ro_spi0_ck_out)	//	spi3_holdn_out    	15	15
#define	ro_i2c_scl_0_out  (ro_spi0_ck_out)	//	i2c_scl_0_out     	16	16
#define	ro_i2c_sda_0_out  (ro_spi0_ck_out)	//	i2c_sda_0_out     	17	17
#define	ro_uart0_txd_out  (ro_spi0_ck_out)	//	uart0_txd_out     	18	18
#define	ro_uart0_rxd_out  (ro_spi0_ck_out)	//	uart0_rxd_out     	19	19
#define	ro_uart1_txd_out  (ro_spi0_ck_out)	//	uart1_txd_out     	20	20
#define	ro_uart1_rxd_out  (ro_spi0_ck_out)	//	uart1_rxd_out	    21	21


#define rg_uart_clk_divide 	(0x10000688)//  uart_clk_divide 31  0
#define rg_interrupt_in_im 	(0x100000a8)//  interrupt_in_im 0 0
#define rg_interrupt_in_in 	(0x100001f8)//  interrupt_in_in  8 8
#define rg_jtag_en 			(0x100002d8)//  jtag_en 0 0
#define rg_pe_top_power_en 	(0x10001004)//  pe_top_power_en 0 0
#define rg_pe_top_iso_en 	(0x10001008)//  pe_top_iso_en 0 0
#define rg_pe_top_clk_en 	(0x10001000)//  pe_top_clk_en 0 0
#define rg_pe_sram_switch 	(0x60000000)//  pe_sram_switch  31  0
#define rg_otp_bank_01 		(0x10000504)//  otp_bank_01 31  0   RW  32'h018e515e  Analog-0
#define rg_otp_bank_02 		(0x10000508)//  otp_bank_02 31  0   RW  32'h000100ff  Analog-1



#define rg_system_hw_trig_type (0x10000320)// system_hw_trig_type	31	0
#define rg_system_sw_trig_type (0x10000324)// system_sw_trig_type	0	0

#define rg_system_int_src_25_sel (0x10000330)//	system_int_src_25_sel	3	0
#define rg_system_int_src_27_sel (0x10000330)//	system_int_src_27_sel	7	4
#define rg_system_int_src_29_sel (0x10000330)//	system_int_src_29_sel	11	8

/*****************************************************************************
 * PLIC_SW Interrupt source definition
 ****************************************************************************/
#define IRQ_SWINT                           (1)


typedef enum {
	HAL_SPI3_MOSI  	= 0,
	HAL_SPI3_CK    	= 1,
	HAL_SPI3_HOLDN 	= 2,
	HAL_SPI2_CSN   	= 3,
	HAL_SPI2_MISO  	= 4,
	HAL_SPI2_WPN   	= 5,
	HAL_SPI0_CK    	= 6,
	HAL_SPI0_CSN   	= 7,
	HAL_SPI0_MOSI  	= 8,
	HAL_SPI0_MISO  	= 9,
	HAL_INT_IN     	= 10,
	HAL_GPIO_00 	= 11,
	HAL_GPIO_01 	= 12,
	HAL_GPIO_02 	= 13,
	HAL_GPIO_03 	= 14,
	HAL_GPIO_04 	= 15,
	HAL_GPIO_05 	= 16,
	HAL_GPIO_06 	= 17,
	HAL_GPIO_07 	= 18,
	HAL_GPIO_08 	= 19,
	HAL_GPIO_09 	= 20,
	HAL_SPI2_MOSI 	= 21,
	HAL_SPI2_CK   	= 22,
	HAL_SPI2_HOLDN 	= 23,
	HAL_I2C_SCL_0 	= 24,
	HAL_I2C_SDA_0 	= 25,
	HAL_SPI3_CSN 	= 26,
	HAL_SPI3_MISO 	= 27,
	HAL_SPI3_WPN 	= 28,
	HAL_UART0_TXD 	= 29,
	HAL_UART0_RXD 	= 30,
	HAL_UART1_TXD 	= 31,
	HAL_UART1_RXD 	= 32,
	HAL_GPIO_MAX
}hal_gpio_pin_t;


typedef enum {
	HAL_OUTPUT = 0,
	HAL_INPUT = 1,
	HAL_MODE_MAX
}hal_gpio_mode_t;


typedef enum {
	HAL_INPUT_LOW = 0,
	HAL_INPUT_HIGH = 1,
	HAL_INPUT_MAX
}hal_gpio_input_t;


typedef enum {
	HAL_OUTPUT_LOW = 0,
	HAL_OUTPUT_HIGH = 1,
	HAL_OUTPUT_MAX
}hal_gpio_output_t;

typedef enum {
	HAL_IRQ25 = 25,
	HAL_IRQ27 = 27,
	HAL_IRQ29 = 29
}hal_irq_sel;

typedef enum {
	HAL_IRQ_LEVEL = 0,
	HAL_IRQ_EDGE = 1,
}hal_irq_trigger_type;


void gpio_irq_setup(hal_gpio_pin_t gpio_pin, hal_irq_sel iqr_sel, hal_irq_trigger_type trg_type);
void SetReg32(uint32_t* addr, uint32_t MSB, uint32_t LSB, uint32_t value);

void hal_gpio_init (hal_gpio_pin_t gpio_pin, hal_gpio_mode_t gpio_mode );
void hal_gpio_deinit (hal_gpio_pin_t gpio_pin);
void hal_gpio_set_output (hal_gpio_pin_t gpio_pin, hal_gpio_output_t gpio_data);
uint32_t hal_gpio_get_intput (hal_gpio_pin_t gpio_pin);


uint32_t u32gpio_initial_hadle[2];
uint32_t u32gpio_mode_hadle_buf[2];

#endif /* __GPIO_AE250_H */
