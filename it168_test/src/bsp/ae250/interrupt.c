/*
 * Copyright (c) 2012-2017 Andes Technology Corporation
 * All rights reserved.
 *
 */

#include <stdio.h>
#include "platform.h"

typedef void (*isr_func)(void);

void default_irq_handler(void)
{
	while(1);
}

void wdt_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void rtc_period_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void rtc_alarm_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void i2c0_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void i2c1_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void uart0_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void uart1_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void dma_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void spi0_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void spi1_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void spi2_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void spi3_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void irq25_handler(void) __attribute__((weak, alias("default_irq_handler")));
void irq27_handler(void) __attribute__((weak, alias("default_irq_handler")));
void irq29_handler(void) __attribute__((weak, alias("default_irq_handler")));

const isr_func irq_handler[] = {
	default_irq_handler,
	wdt_irq_handler,        //1
	rtc_period_irq_handler, //2
	rtc_alarm_irq_handler,  //3
	i2c0_irq_handler,       //4
	i2c1_irq_handler,       //5
	uart0_irq_handler,      //6
	uart1_irq_handler,      //7
	dma_irq_handler,        //8
	default_irq_handler,    //9
	default_irq_handler,    //10
	default_irq_handler,    //11
	default_irq_handler,    //12
	default_irq_handler,    //13
	default_irq_handler,    //14
	default_irq_handler,    //15
	spi0_irq_handler,       //16
	spi1_irq_handler,       //17
	spi2_irq_handler,       //18
	spi3_irq_handler,       //19
	default_irq_handler,    //20
	default_irq_handler,    //21
	default_irq_handler,    //22
	default_irq_handler,    //23
	default_irq_handler,    //24
	irq25_handler,    		//25
	default_irq_handler,    //26
	irq27_handler,          //27
	default_irq_handler,    //28
	irq29_handler,          //29
	default_irq_handler,    //30
	default_irq_handler     //31
};

void mext_interrupt(unsigned int irq_source)
{
	/* Enable interrupts in general to allow nested */
	set_csr(NDS_MSTATUS, MSTATUS_MIE);

	/* Do interrupt handler */
	irq_handler[irq_source]();

	__nds__plic_complete_interrupt(irq_source);
}
