/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: test_case_main.c
 */

#include <proj_config.h>

#if (CONFIG_PROJ_SEL == CONFIG_PROJ_TEST_CASE)
#include <test_case_config.h>
#include <test_case_spi.h>
#include <test_case_common.h>
#include <serial.h>
#include <string.h>
#include <sys.h>

#if (WAIT_FOR_INT_EN == 1)
U32 test;
#endif

#if (CONFIG_TEST_CASE_SEL == TEST_CASE_NORMAL)
static void test_case_init(void)
{
    /* initial DEBUG SRAM */
    memset((void *)STATUS_FLAG, 0, (DEBUG_FLAG_END - STATUS_FLAG));

    //RW_SYS_REG(0x370) |= (1 << 24);
#if SYS_RISCV_INT_TEST == 1
    RW_SYS_REG(SYS_VSYNC_IM) |= SET_VSYNC_IN;
    RW_SYS_REG(SYS_HSYNC_IM) |= SET_HSYNC_IN;
    RW_SYS_REG(SYS_GPIO_IM) = (SYS_GPIO_00_IN | SYS_GPIO_01_IN \
                               | SYS_GPIO_02_IN | SYS_GPIO_03_IN \
                               | SYS_GPIO_04_IN | SYS_GPIO_05_IN \
                               | SYS_GPIO_06_IN | SYS_GPIO_07_IN);
#endif

#if MXP0_OUTPUT_CLK == 1
    RW_SYS_REG(0x178) |= ((1 << 8) | (1 << 16));
    RW_SYS_REG(0x100) |= (1 << 0);
    set_dbg_val(0X100000F4, ((0x01) | (0x02 << 8)));            //MXP0 CLK EN & SEL
    RW_SYS_REG(0xe4) |= ((1 << 16) | (1 << 24));        //CLK_DIV_EN on
    //RW_SYS_REG(0xe4) &= (~(1 << 16) & ~(1 << 24));        //CLK_DIV_EN off
    RW_SYS_REG(0xe8) |= ((1 << 0) | (1 << 8) | (1 << 16));      //FREQ
#endif

#if MDL_CLK_OFF_EN == 1
    /*modify module date in order to check the reset condition*/
    //RW_SYS_REG(SYS_SPIS_CLK_EN) &= (SYS_SPIS_00_DIS & SYS_SPIS_01_DIS);
    RW_SYS_REG(SYS_SPIS_CLK_EN) &= (SYS_SPIS_01_DIS);

    RW_SYS_REG(SYS_TCON_CLK_EN) &= (SYS_TCON_0_CLK_DIS & SYS_TCON_1_CLK_DIS & SYS_TCON_2_CLK_DIS);

    RW_SYS_REG(SYS_CLK_EN) &= (SYS_DSP_CLK_DIS & SYS_DSP_SRAM_CLK_DIS & SYS_OTHERS_CLK_DIS);
#if (ALL_ADC_CLK_OFF_EN == 1)
    RW_SYS_REG(SYS_SENSOR_CLK_EN) &= (SYS_SENSOR_00_CLK_DIS &
                                      SYS_SENSOR_01_CLK_DIS &
                                      SYS_SENSOR_02_CLK_DIS &
                                      SYS_SENSOR_03_CLK_DIS &
                                      SYS_SENSOR_04_CLK_DIS &
                                      SYS_SENSOR_05_CLK_DIS &
                                      SYS_SENSOR_06_CLK_DIS &
                                      SYS_SENSOR_07_CLK_DIS &
                                      SYS_SENSOR_08_CLK_DIS &
                                      SYS_SENSOR_09_CLK_DIS &
                                      SYS_SENSOR_10_CLK_DIS &
                                      SYS_SENSOR_11_CLK_DIS &
                                      SYS_SENSOR_12_CLK_DIS &
                                      SYS_SENSOR_13_CLK_DIS &
                                      SYS_SENSOR_14_CLK_DIS &
                                      SYS_SENSOR_15_CLK_DIS);
#else
    RW_SYS_REG(SYS_SENSOR_CLK_EN) &= (SYS_SENSOR_00_CLK_DIS &
                                      SYS_SENSOR_01_CLK_DIS &
                                      SYS_SENSOR_02_CLK_DIS &
                                      SYS_SENSOR_03_CLK_DIS &
                                      SYS_SENSOR_04_CLK_DIS &
                                      SYS_SENSOR_05_CLK_DIS &
                                      SYS_SENSOR_06_CLK_DIS &
                                      SYS_SENSOR_07_CLK_DIS);
#endif

    //RW_SYS_REG(SYS_CLK_EN) &= (SYS_RISCV_CLK_DIS);

    //RW_SYS_REG(SYS_CLK_EN) &= (SYS_SRAM_CLK_DIS);  //dont open, maybe will die forever
#endif

#if (CONFIG_TEST_CASE_SEL == TEST_CASE_GPIO_DEBUG_FLAG)
    TestGPIO_OutHighLow_init();
#endif

#if (WAIT_FOR_INT_EN == 1)
    RW_SYS_REG(SYS_GPIO_IM) = 0xffff;
#if (WFI_MEASURE_WAKEUP_TIME == 1)
    RW_SYS_REG(SYS_GPIO_IM) = 0x0;
#endif
    RW_SYS_REG(SYS_PAD_GPIO_IN_SEL) |= ((0xff << 0) | (0xff << 8) | (0xff << 16));//
#endif

    tc_irq_setup();
}
#endif

void test_case_main(void)
{
#if (CONFIG_TEST_CASE_SEL == TEST_CASE_SPI)
    test_case_spi_main();
#endif

}

#endif

