/*
 * Copyright 2020 @IGISTEC. All rights reserved.
 * File: test_case_gpio.c
 */

#include <proj_config.h>
#include <test_case_config.h>

#if (CONFIG_PROJ_SEL == CONFIG_PROJ_TEST_CASE) && \
    (CONFIG_TEST_CASE_SEL == TEST_CASE_SPI)
#include <stdio.h>
#include <string.h>
#include <nds32_intrinsic.h>
#include <serial.h>
#include <test_case_spi.h>
#include <RTE_Device.h>

#include "Driver_SPI.h"
#include "Driver_USART.h"
#include "ae250.h"
#include "gpio_ae250.h"
#include "spi_ae250.h"


#define SPI_MASTER_MODE     0
#define SPI_SLAVE_MODE      !(SPI_MASTER_MODE)

// SPI commands
#define SPI_READ    0x0b
#define SPI_WRITE   0x51
#define SPI_DUMMY   0xff

#define TEST_DATA_SIZE            8
#define TOTAL_TRANSFER_SIZE       (TEST_DATA_SIZE + 2) // Total transfer size is cmd(1) + dummy(1) + data(TEST_DATA_SIZE)

#define print(s)          do {                                                \
                                USART_Dri->Send(s, sizeof(s) / sizeof(char)); \
                                wait_usart_complete();                        \
                          } while(0);

static volatile char spi_event_transfer_complete = 0;
static volatile int usart_event_complete = 0;

static int CPOL_CPHA_SEL = NDS_SPI_CPOL0_CPHA0;
static int MSB_LSB_SEL = NDS_SPI_MSB_LSB;

extern NDS_DRIVER_SPI Driver_SPI0;
extern int rx_threshold;
extern int tx_threshold;

#if SPI_MASTER_MODE
static void spi_prepare_data_in(uint8_t *data_in, uint8_t cmd, uint32_t dummy_cnt, uint8_t *w_data, uint32_t wcnt) {
    uint8_t *ptr = data_in;
    uint32_t i;

    // set 1-byte command
    *ptr++ = cmd;

    // set n-byte dummy
    for (i = 0; i < dummy_cnt; i++)
        *ptr++ = SPI_DUMMY;

    // set n-byte w_data
    for (i = 0; i < wcnt; i++)
        *ptr++ = *w_data++;
}
#endif

static void wait_complete(void) {
    while (!spi_event_transfer_complete) {
        serial_cmd_proc();
    }
    spi_event_transfer_complete = 0;
}

void spi_callback(uint32_t event) {
    int cntt = 0;
    switch (event) {
        case NDS_SPI_EVENT_TRANSFER_COMPLETE:
            spi_event_transfer_complete = 1;
            break;
        case NDS_SPI_EVENT_DATA_LOST:
            *(uint32_t *)0x40000 = 1;
            while(1) {
//                if (AE250_SPI0->INTRST & 0x1 || AE250_SPI0->INTRST & 0x4) {
//                    *(uint32_t*)(0x45000 + cntt * 4) = AE250_SPI0->DATA;
//
//                    AE250_SPI0->SLVST |= 1 << 17;
//                    AE250_SPI0->INTRST |= 0x5;
//                    cntt ++;
//                } else
//                {
//                    break;
//                }
            }
            break;
    }
}

static void cpol_cpha_select(void) {
#define CP_SEL_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[CP_SEL_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;

    while ((arg = serial_next()) != NULL) {

        if (argc >= CP_SEL_CMD_ARG_NUM_MAX) {
            printf("err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        switch (arg_ary[0]) {
            case 0:
                CPOL_CPHA_SEL = NDS_SPI_CPOL0_CPHA0;
                printf("\rNDS_SPI_CPOL0_CPHA0\n");
                break;
            case 1:
                CPOL_CPHA_SEL = NDS_SPI_CPOL0_CPHA1;
                printf("\rNDS_SPI_CPOL0_CPHA1\n");
                break;
            case 2:
                CPOL_CPHA_SEL = NDS_SPI_CPOL1_CPHA0;
                printf("\rNDS_SPI_CPOL1_CPHA0\n");
                break;
            case 3:
                CPOL_CPHA_SEL = NDS_SPI_CPOL1_CPHA1;
                printf("\rNDS_SPI_CPOL1_CPHA1\n");
                break;
            default:
                printf("\r error argument \n");
        }
    }

}

static void msb_lsb_select(void) {
#define MLSB_SEL_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[MLSB_SEL_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;

    while ((arg = serial_next()) != NULL) {

        if (argc >= MLSB_SEL_CMD_ARG_NUM_MAX) {
            printf("err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        switch (arg_ary[0]) {
            case 0:
                MSB_LSB_SEL = NDS_SPI_MSB_LSB;
                printf("\rNDS_SPI_MSB_LSB\n");
                break;
            case 1:
                MSB_LSB_SEL = NDS_SPI_LSB_MSB;
                printf("\rNDS_SPI_LSB_MSB\n");
                break;
            default:
                printf("\r error argument \n");
        }
    }

}

static void rx_threshold_set(void) {
#define RX_THR_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[RX_THR_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;

    while ((arg = serial_next()) != NULL) {

        if (argc >= RX_THR_CMD_ARG_NUM_MAX) {
            printf("err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }
    if (argc > 0) {
        rx_threshold = arg_ary[0];
        printf("\rset rx threshold %d\n", rx_threshold);
    }
}

static void tx_threshold_set(void) {
#define TX_THR_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[TX_THR_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;

    while ((arg = serial_next()) != NULL) {

        if (argc >= TX_THR_CMD_ARG_NUM_MAX) {
            printf("err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }
    if (argc > 0) {
        tx_threshold = arg_ary[0];
        printf("\rset tx threshold %d\n", tx_threshold);
    }
}

#if SPI_MASTER_MODE
int main(void) {
    uint8_t data_in[TOTAL_TRANSFER_SIZE] = {0};
    uint8_t data_out[TOTAL_TRANSFER_SIZE] = {0};
    uint8_t w_data[TEST_DATA_SIZE] = {0};
    uint32_t i;

    *((volatile uint32_t *)(0x10000504)) = 0x0baa7fde; //250MHz COB3

    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI1;
    NDS_DRIVER_USART *USART_Dri = &Driver_USART1;

    // initialize UART
    usart_init(USART_Dri);


    print("\n\r==== start demo SPI ====\n\r");

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);

    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);

    // configure the SPI to master, 8-bit data length and bus speed to 1MHz
    SPI_Dri->Control(NDS_SPI_MODE_MASTER |
             NDS_SPI_CPOL0_CPHA0 |
             NDS_SPI_MSB_LSB     |
             NDS_SPI_DATA_BITS(8), 1000000);

    print("\n\rmaster write/read test...");

    // prepare write data
    for (i = 0; i < TEST_DATA_SIZE; i++)
        w_data[i] = i;

    spi_prepare_data_in(data_in,SPI_WRITE , 1, w_data, TEST_DATA_SIZE);

    // write data to slave
    SPI_Dri->Send(data_in, TOTAL_TRANSFER_SIZE);
    wait_complete();

        for (i = 0; i < TEST_DATA_SIZE+2; i++)
                data_in[i] = 0;

    // read data from slave
    spi_prepare_data_in(data_in,SPI_READ, 1, 0, 0);

    SPI_Dri->Transfer(data_out, data_in, TOTAL_TRANSFER_SIZE);
    wait_complete();

    // check data
    for (i = 0; i < TEST_DATA_SIZE; i++) {
        if (data_out[i + 2] != w_data[i]) {
            print("check data FAIL!!\n\r");
            while (1);
        }
    }

    print("PASS!!\n\r");

    return 0;
}
#elif SPI_SLAVE_MODE

static void dma_rw_test(void) {
#define DMA_TRANSFER_SIZE       (10)
#define DMA_Log                 (1)
    NDS_SPI_STATUS status;
    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data[DMA_TRANSFER_SIZE] = {0};

    printf("\n\r==== start demo SPI ====\n\r");

    printf("\rdata address = %x\n", (unsigned int)data);

    hal_gpio_init(HAL_GPIO_05,HAL_OUTPUT);
    hal_gpio_init(HAL_GPIO_06,HAL_OUTPUT);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);

    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);

    // configure the SPI to slave, 8-bit data length
    SPI_Dri->Control(/*NDS_SPI_SLV_DATA_ONLY_TRANSFER | */
             NDS_SPI_MODE_SLAVE  |
             CPOL_CPHA_SEL |
             MSB_LSB_SEL     |
             NDS_SPI_DATA_BITS(8), 0);

    printf("\n\rslave write/read test...\n");

    while (1) {
        // configure the SPI to slave, 8-bit data length
        SPI_Dri->Control(/*NDS_SPI_SLV_DATA_ONLY_TRANSFER | */
                 NDS_SPI_MODE_SLAVE  |
                 CPOL_CPHA_SEL |
                 MSB_LSB_SEL     |
                 NDS_SPI_DATA_BITS(8), 0);

        // read data from master
        memset(data, 0 , DMA_TRANSFER_SIZE);
        SPI_Dri->Receive(data, DMA_TRANSFER_SIZE);
#if DMA_Log
        printf("\rstart wait\n");
        wait_complete();
        printf("\rend wait\n");
#else
        wait_complete();
#endif
        status = SPI_Dri->GetStatus();
#if DMA_Log
        printf("\rspi0 status busy : %x\n", status.busy);
        printf("\rspi0 status data_lost : %x\n", status.data_lost);
        printf("\rspi0 status mode_fault : %x\n", status.mode_fault);

        printf("\n\rreceive data length : %d\n", DMA_TRANSFER_SIZE);
        for (int cnt = 0; cnt < DMA_TRANSFER_SIZE; cnt ++) {
            printf("\rdata[%d] = %d\n", cnt, data[cnt]);
        }

        printf("\n\rsend CMD = %x\n", AE250_SPI0->CMD);
        for (int cnt = 0; cnt < DMA_TRANSFER_SIZE; cnt ++) {
            data[cnt] = cnt+1;
        }

        printf("\rsend data 1~10\n");
#endif
        SPI_Dri->Send(data, DMA_TRANSFER_SIZE);
        wait_complete();

    }
}

static void rx_int_test(void) {
#define RXINT_TRANSFER_SIZE       0x40
    NDS_SPI_STATUS status;
    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data[RXINT_TRANSFER_SIZE] = {0};

    printf("\n\r==== start demo SPI ====\n\r");

    hal_gpio_init(HAL_GPIO_05,HAL_OUTPUT);
    hal_gpio_init(HAL_GPIO_06,HAL_OUTPUT);
    printf("\rdata address = %x\n", (unsigned int)data);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);
    printf("\n\rInitialize done\n\r");
    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);
    printf("\n\rPowerControl done\n\r");
    // configure the SPI to slave, 8-bit data length
    SPI_Dri->Control(/*NDS_SPI_SLV_DATA_ONLY_TRANSFER | */
             NDS_SPI_MODE_SLAVE  |
             NDS_SPI_CPOL0_CPHA0 |
             NDS_SPI_MSB_LSB     |
             NDS_SPI_DATA_BITS(8), 0);
    printf("\n\rControl done\n\r");

    printf("\n\rslave write/read test...\n");

    printf("\r%x\n", AE250_SPI0->TRANSCTRL);

    while (1) {
        // read data from master
        *(uint32_t *)0x30000 = 0;
        *(uint32_t *)0x35000 = 0;
        memset(data, 0 , RXINT_TRANSFER_SIZE);
        SPI_Dri->Receive(data, 10);
        printf("\rRX threshould = %d\n", AE250_SPI0->CTRL >> 8 & 0xFF);
        printf("\rTX threshould = %d\n", AE250_SPI0->CTRL >> 16 & 0xFF);

        printf("\rdummy_cnt = %x\n", (AE250_SPI0->TRANSCTRL >> 9) & 0x3);

        printf("\rreceive CMD = %x\n", AE250_SPI0->CMD);

        printf("\rSTATUS SPIActive = %x\n", AE250_SPI0->STATUS & 0x1);

        printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
                (AE250_SPI0->STATUS >> 18 & 0xC0));
        printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
        printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);

        printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
                (AE250_SPI0->STATUS >> 22 & 0xC0));
        printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
        printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);

        printf("\rSLVDATACNT RCnt =  %x\n", AE250_SPI0->SLVDATACNT & 0x1FF);
        printf("\rSLVDATACNT WCnt =  %x\n", AE250_SPI0->SLVDATACNT >> 16 & 0x3FF);

        printf("\rSlvCmdInt =  %x\n", AE250_SPI0->INTRST >> 5 & 0x1);
        printf("\rEndInt =  %x\n", AE250_SPI0->INTRST >> 4 & 0x1);
        printf("\rTXFIFOInt =  %x\n", AE250_SPI0->INTRST >> 3 & 0x1);
        printf("\rRXFIFOInt =  %x\n", AE250_SPI0->INTRST >> 2 & 0x1);
        printf("\rTXFIFOURInt =  %x\n", AE250_SPI0->INTRST >> 1 & 0x1);
        printf("\rRXFIFOORInt =  %x\n", AE250_SPI0->INTRST & 0x1);

        printf("\rstart wait\n");
        wait_complete();
        hal_gpio_set_output(HAL_GPIO_06,HAL_OUTPUT_HIGH);

        printf("\rend wait\n");

        status = SPI_Dri->GetStatus();
        printf("\rspi0 status busy : %x\n", status.busy);
        printf("\rspi0 status data_lost : %x\n", status.data_lost);
        printf("\rspi0 status mode_fault : %x\n", status.mode_fault);
        for (int i =0; i < (*(uint32_t *)0x30000); i++) {
            unsigned int status_temp = (*(uint32_t *)(0x10000 + i*4));
            unsigned int slavedatacnt_temp = (*(uint32_t *)(0x20000 + i*4));
            unsigned int int_flag_temp = (*(uint32_t *)(0x25000 + i*4));

            if (*(uint32_t *)0x30000 == i + 1) {
                printf("\n\r==== Irq %d times is the end int ====\n", i);
            } else {
                printf("\n\r==== Irq %d times ====\n", i);
            }

            printf("\rSTATUS SPIActive = %x\n", status_temp & 0x1);

            printf("\rSTATUS RXNUM = %x\n", (status_temp >> 8 & 0x3F) |
                    (status_temp >> 18 & 0xC0));
            printf("\rSTATUS RXEMPTY = %x\n", status_temp >> 14 & 0x1);
            printf("\rSTATUS RXFULL = %x\n", status_temp >> 15 & 0x1);

            printf("\rSTATUS TXNUM = %x\n", (status_temp >> 16 & 0x3F) |
                    (status_temp >> 22 & 0xC0));
            printf("\rSTATUS TXEMPTY = %x\n", status_temp >> 22 & 0x1);
            printf("\rSTATUS TXFULL = %x\n", status_temp >> 23 & 0x1);

            printf("\rSLVDATACNT RCnt =  %x\n", slavedatacnt_temp & 0x1FF);
            printf("\rSLVDATACNT WCnt =  %x\n", slavedatacnt_temp >> 16 & 0x3FF);

            printf("\rSlvCmdInt =  %x\n", int_flag_temp >> 5 & 0x1);
            printf("\rEndInt =  %x\n", int_flag_temp >> 4 & 0x1);
            printf("\rTXFIFOInt =  %x\n", int_flag_temp >> 3 & 0x1);
            printf("\rRXFIFOInt =  %x\n", int_flag_temp >> 2 & 0x1);
            printf("\rTXFIFOURInt =  %x\n", int_flag_temp >> 1 & 0x1);
            printf("\rRXFIFOORInt =  %x\n", int_flag_temp & 0x1);
        }

        printf("\n\rreceive data length : %d\n", RXINT_TRANSFER_SIZE);
        for (int cnt = 0; cnt < RXINT_TRANSFER_SIZE; cnt ++) {
            printf("\rdata[%d] = %d\n", cnt, data[cnt]);
        }

        printf("\rreceive CMD = %x\n", AE250_SPI0->CMD);

        printf("\n\r==== After receive finish ====\n");
        printf("\rSTATUS SPIActive = %x\n", AE250_SPI0->STATUS & 0x1);

        printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
                (AE250_SPI0->STATUS >> 18 & 0xC0));
        printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
        printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);

        printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
                (AE250_SPI0->STATUS >> 22 & 0xC0));
        printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
        printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);

        printf("\rSLVDATACNT RCnt =  %x\n", AE250_SPI0->SLVDATACNT & 0x1FF);
        printf("\rSLVDATACNT WCnt =  %x\n", AE250_SPI0->SLVDATACNT >> 16 & 0x3FF);

        printf("\rSlvCmdInt =  %x\n", AE250_SPI0->INTRST >> 5 & 0x1);
        printf("\rEndInt =  %x\n", AE250_SPI0->INTRST >> 4 & 0x1);
        printf("\rTXFIFOInt =  %x\n", AE250_SPI0->INTRST >> 3 & 0x1);
        printf("\rRXFIFOInt =  %x\n", AE250_SPI0->INTRST >> 2 & 0x1);
        printf("\rTXFIFOURInt =  %x\n", AE250_SPI0->INTRST >> 1 & 0x1);
        printf("\rRXFIFOORInt =  %x\n", AE250_SPI0->INTRST & 0x1);

        hal_gpio_set_output(HAL_GPIO_06,HAL_OUTPUT_LOW);
//        while(1);
//        AE250_SPI0->CMD = 0x51;
        printf("\n\rsend CMD = %x\n", AE250_SPI0->CMD);
        for (int cnt = 0; cnt < RXINT_TRANSFER_SIZE; cnt ++) {
            data[cnt] = cnt+1;
        }

        printf("\rsend data 1~10\n");
        SPI_Dri->Send(data, RXINT_TRANSFER_SIZE);
        wait_complete();

    }
}

static void tx_int_test(void) {
#define TXINT_TRANSFER_SIZE       (0x20)
    NDS_SPI_STATUS status;
    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data[TXINT_TRANSFER_SIZE] = {0};

    printf("\n\r==== start demo SPI ====\n\r");

    hal_gpio_init(HAL_GPIO_05,HAL_OUTPUT);
    hal_gpio_init(HAL_GPIO_06,HAL_OUTPUT);
    printf("\rdata address = %x\n", (unsigned int)data);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);
    printf("\n\rInitialize done\n\r");
    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);
    printf("\n\rPowerControl done\n\r");
    // configure the SPI to slave, 8-bit data length
    SPI_Dri->Control(/*NDS_SPI_SLV_DATA_ONLY_TRANSFER | */
             NDS_SPI_MODE_SLAVE  |
             NDS_SPI_CPOL0_CPHA0 |
             NDS_SPI_MSB_LSB     |
             NDS_SPI_DATA_BITS(8), 0);
    printf("\n\rControl done\n\r");

    printf("\n\rslave write/read test...\n");

    printf("\r%x\n", AE250_SPI0->TRANSCTRL);

    while (1) {
        // read data from master
        *(uint32_t *)0x30000 = 0;
        *(uint32_t *)0x35000 = 0;
        memset(data, 0 , TXINT_TRANSFER_SIZE);
        SPI_Dri->Receive(data, 10);
        wait_complete();
        hal_gpio_set_output(HAL_GPIO_06,HAL_OUTPUT_HIGH);

        printf("\n\rreceive data length : %d\n", TXINT_TRANSFER_SIZE);
        for (int cnt = 0; cnt < TXINT_TRANSFER_SIZE; cnt ++) {
            printf("\rdata[%d] = %d\n", cnt, data[cnt]);
        }

        printf("\rreceive CMD = %x\n", AE250_SPI0->CMD);

        hal_gpio_set_output(HAL_GPIO_06,HAL_OUTPUT_LOW);
//        while(1);
//        AE250_SPI0->CMD = 0x51;
//        printf("\n\rsend CMD = %x\n", AE250_SPI0->CMD);
        for (int cnt = 0; cnt < TXINT_TRANSFER_SIZE; cnt ++) {
            data[cnt] = cnt+1;
        }

        printf("\rsend data 1~10\n");

        printf("\rRX threshould = %d\n", AE250_SPI0->CTRL >> 8 & 0xFF);
        printf("\rTX threshould = %d\n", AE250_SPI0->CTRL >> 16 & 0xFF);

        printf("\rdummy_cnt = %x\n", (AE250_SPI0->TRANSCTRL >> 9) & 0x3);

        printf("\rreceive CMD = %x\n", AE250_SPI0->CMD);

        printf("\rSTATUS SPIActive = %x\n", AE250_SPI0->STATUS & 0x1);

//        printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) | \
//                (AE250_SPI0->STATUS >> 18 & 0xC0));
//        printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
//        printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);

        printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) | \
                (AE250_SPI0->STATUS >> 22 & 0xC0));
        printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
        printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);

//        printf("\rSLVDATACNT RCnt =  %x\n", AE250_SPI0->SLVDATACNT & 0x1FF);
        printf("\rSLVDATACNT WCnt =  %x\n", AE250_SPI0->SLVDATACNT >> 16 & 0x3FF);

        printf("\rSlvCmdInt =  %x\n", AE250_SPI0->INTRST >> 5 & 0x1);
        printf("\rEndInt =  %x\n", AE250_SPI0->INTRST >> 4 & 0x1);
        printf("\rTXFIFOInt =  %x\n", AE250_SPI0->INTRST >> 3 & 0x1);
//        printf("\rRXFIFOInt =  %x\n", AE250_SPI0->INTRST >> 2 & 0x1);
        printf("\rTXFIFOURInt =  %x\n", AE250_SPI0->INTRST >> 1 & 0x1);
//        printf("\rRXFIFOORInt =  %x\n", AE250_SPI0->INTRST & 0x1);
        SPI_Dri->Send(data, TXINT_TRANSFER_SIZE);
        wait_complete();

        status = SPI_Dri->GetStatus();
        printf("\rspi0 status busy : %x\n", status.busy);
        printf("\rspi0 status data_lost : %x\n", status.data_lost);
        printf("\rspi0 status mode_fault : %x\n", status.mode_fault);
        for (int i =0; i < (*(uint32_t *)0x30000); i++) {
            unsigned int status_temp = (*(uint32_t *)(0x10000 + i*4));
            unsigned int slavedatacnt_temp = (*(uint32_t *)(0x20000 + i*4));
            unsigned int int_flag_temp = (*(uint32_t *)(0x25000 + i*4));

            if (*(uint32_t *)0x30000 == i + 1) {
                printf("\n\r==== Fill data[%d] ====\n", i);
            } else {
                printf("\n\r==== Fill data[%d] ====\n", i);
            }

            printf("\rSTATUS SPIActive = %x\n", status_temp & 0x1);

//            printf("\rSTATUS RXNUM = %x\n", (status_temp >> 8 & 0x3F) | \
//                    (status_temp >> 18 & 0xC0));
//            printf("\rSTATUS RXEMPTY = %x\n", status_temp >> 14 & 0x1);
//            printf("\rSTATUS RXFULL = %x\n", status_temp >> 15 & 0x1);

            printf("\rSTATUS TXNUM = %x\n", (status_temp >> 16 & 0x3F) | \
                    (status_temp >> 22 & 0xC0));
            printf("\rSTATUS TXEMPTY = %x\n", status_temp >> 22 & 0x1);
            printf("\rSTATUS TXFULL = %x\n", status_temp >> 23 & 0x1);

//            printf("\rSLVDATACNT RCnt =  %x\n", slavedatacnt_temp & 0x1FF);
            printf("\rSLVDATACNT WCnt =  %x\n", slavedatacnt_temp >> 16 & 0x3FF);

            printf("\rSlvCmdInt =  %x\n", int_flag_temp >> 5 & 0x1);
            printf("\rEndInt =  %x\n", int_flag_temp >> 4 & 0x1);
            printf("\rTXFIFOInt =  %x\n", int_flag_temp >> 3 & 0x1);
//            printf("\rRXFIFOInt =  %x\n", int_flag_temp >> 2 & 0x1);
            printf("\rTXFIFOURInt =  %x\n", int_flag_temp >> 1 & 0x1);
//            printf("\rRXFIFOORInt =  %x\n", int_flag_temp & 0x1);
        }

        printf("\n\r==== After send finish ====\n");
        printf("\rSTATUS SPIActive = %x\n", AE250_SPI0->STATUS & 0x1);

//        printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) | \
//                (AE250_SPI0->STATUS >> 18 & 0xC0));
//        printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
//        printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);

        printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
                (AE250_SPI0->STATUS >> 22 & 0xC0));
        printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
        printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);

        printf("\rSLVDATACNT RCnt =  %x\n", AE250_SPI0->SLVDATACNT & 0x1FF);
        printf("\rSLVDATACNT WCnt =  %x\n", AE250_SPI0->SLVDATACNT >> 16 & 0x3FF);

        printf("\rSlvCmdInt =  %x\n", AE250_SPI0->INTRST >> 5 & 0x1);
        printf("\rEndInt =  %x\n", AE250_SPI0->INTRST >> 4 & 0x1);
        printf("\rTXFIFOInt =  %x\n", AE250_SPI0->INTRST >> 3 & 0x1);
//        printf("\rRXFIFOInt =  %x\n", AE250_SPI0->INTRST >> 2 & 0x1);
        printf("\rTXFIFOURInt =  %x\n", AE250_SPI0->INTRST >> 1 & 0x1);
//        printf("\rRXFIFOORInt =  %x\n", AE250_SPI0->INTRST & 0x1);

    }
}

static void slv_cmd_test(void) {
#define CMD_TRANSFER_SIZE       (10)
    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data[DMA_TRANSFER_SIZE] = {0};

    printf("\n\r==== start demo SPI ====\n\r");

    printf("\rdata address = %x\n", (unsigned int)data);

    hal_gpio_init(HAL_GPIO_05,HAL_OUTPUT);
    hal_gpio_init(HAL_GPIO_06,HAL_OUTPUT);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);
    printf("\n\rInitialize done\n\r");
    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);
    printf("\n\rPowerControl done\n\r");
    // configure the SPI to slave, 8-bit data length
    SPI_Dri->Control(/*NDS_SPI_SLV_DATA_ONLY_TRANSFER | */
             NDS_SPI_MODE_SLAVE  |
             NDS_SPI_CPOL0_CPHA0 |
             NDS_SPI_MSB_LSB     |
             NDS_SPI_DATA_BITS(8), 0);
    printf("\n\rControl done\n\r");
    printf("\n\rslave write/read test...\n");
    AE250_SPI0->INTREN |= SPI_SLVCMD;

//    printf("\r==== Origin TX Status ====\n");
//    printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
//                                (AE250_SPI0->STATUS >> 22 & 0xC0));
//    printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
//    printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);
//    for (int cnt = 0; cnt < 16; cnt ++) {
//        data[cnt] = cnt+1;
//        AE250_SPI0->DATA = data[cnt];
//    }
//    printf("\r==== After Fill TX FIFO Status ====\n");
//    printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
//            (AE250_SPI0->STATUS >> 22 & 0xC0));
//    printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
//    printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);
//    AE250_SPI0->CTRL |= 0x7;
//    printf("\r==== After Reset TX FIFO Status ====\n");
//    printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
//            (AE250_SPI0->STATUS >> 22 & 0xC0));
//    printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
//    printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);
//
//    printf("\r==== Origin RX Status ====\n");
//    printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
//            (AE250_SPI0->STATUS >> 18 & 0xC0));
//    printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
//    printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);
    while (1) {
#if (RTE_SPI0_DMA_RX_EN == 0) // receive overrun test rx_threshold = 13 in RX int mode
        // read data from master
        memset(data, 0 , CMD_TRANSFER_SIZE);
        SPI_Dri->Receive(data, CMD_TRANSFER_SIZE);

        wait_complete();
#endif

#if (RTE_SPI0_DMA_TX_EN == 0) // send underrun test rx_threshold = 13 in TX int mode
        for (int cnt = 0; cnt < CMD_TRANSFER_SIZE; cnt ++) {
            data[cnt] = cnt+1;
        }
        SPI_Dri->Send(data, CMD_TRANSFER_SIZE);
        wait_complete();
#endif
        if (AE250_SPI0->CMD) {
            printf("\n\rreceive CMD = %x\n", AE250_SPI0->CMD);

            switch (AE250_SPI0->CMD) {
                case 0x5:
                    printf("\rUnderRun = %x\n", AE250_SPI0->SLVST >> 18 & 0x1);
                    printf("\rOverRun = %x\n", AE250_SPI0->SLVST >> 17 & 0x1);
                    printf("\rReady = %x\n", AE250_SPI0->SLVST >> 16 & 0x1);
                    printf("\rUSR_Status = %x\n", AE250_SPI0->SLVST & 0xFFFF);
                    if (AE250_SPI0->SLVST >> 16 & 0x1) {
                        AE250_SPI0->SLVST &= ~(1 << 16);
                    } else {
                        AE250_SPI0->SLVST |= 1 << 16;
                    }

                    if ((AE250_SPI0->SLVST & 0xFFFF) == 0) {
                        AE250_SPI0->SLVST |= 0xffff;
                    } else {
                        AE250_SPI0->SLVST &= ~0xFFFF;
                    }
                    break;
                case 0xB:
//                    printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
//                            (AE250_SPI0->STATUS >> 22 & 0xC0));
//                    printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
//                    printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);
                    for (int cnt = 0; cnt < CMD_TRANSFER_SIZE; cnt ++) {
                        data[cnt] = cnt+1;
                        AE250_SPI0->DATA = data[cnt];
                    }
//                    printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
//                            (AE250_SPI0->STATUS >> 22 & 0xC0));
//                    printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
//                    printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);
//                    SPI_Dri->Send(data, CMD_TRANSFER_SIZE);
//                    wait_complete();
                    break;
                case 0x51:
                    printf("\rstart receive data\n");
//                    memset(data, 0 , CMD_TRANSFER_SIZE);
//                    SPI_Dri->Receive(data, 10);
//                    wait_complete();

                    while((AE250_SPI0->STATUS >> 8 & 0x3F) == 0);
                    int rxnum = (AE250_SPI0->STATUS >> 8 & 0x3F) | \
                                    (AE250_SPI0->STATUS >> 18 & 0xC0);
//                    printf("\r==== After Fill RX FIFO Status ====\n");
//                    printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
//                            (AE250_SPI0->STATUS >> 18 & 0xC0));
//                    printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
//                    printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);
//                    AE250_SPI0->CTRL |= 0x7;
//                    printf("\r==== After Reset RX FIFO Status ====\n");
//                    printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
//                            (AE250_SPI0->STATUS >> 18 & 0xC0));
//                    printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
//                    printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);
                    for (int i = 0; i < rxnum; i++) {
                        printf("\rdata[%d] = %x\n", i, AE250_SPI0->DATA);
                    }
                    break;
                default:
                    break;
            }

            AE250_SPI0->CMD = 0;

        }
//        serial_cmd_proc();
    }
}

void test_case_spi_main(void) {
    *(uint32_t *)0x40000 = 0;
//    *(uint32_t *)0x10000058 |= (1 << 16);
    AE250_SPI0->CTRL |= 0x7;
    serial_cmd_add("rx_int", rx_int_test);
    serial_cmd_add("tx_int", tx_int_test);

    serial_cmd_add("dma_rw", dma_rw_test);
    serial_cmd_add("scmd", slv_cmd_test);

//    serial_cmd_add("cp", cpol_cpha_select);
//    serial_cmd_add("sb", msb_lsb_select);
    serial_cmd_add("rxth", rx_threshold_set);
    serial_cmd_add("txth", tx_threshold_set);

}
#endif
#endif
