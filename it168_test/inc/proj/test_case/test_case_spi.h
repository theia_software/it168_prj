/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: test_case_dsp.h
 */

#ifndef _TEST_CASE_GPIO_H_
#define _TEST_CASE_GPIO_H_
#include <proj_config.h>
#include <test_case_config.h>

#if (CONFIG_PROJ_SEL == CONFIG_PROJ_TEST_CASE) && \
    (CONFIG_TEST_CASE_SEL == TEST_CASE_SPI)

/* SPI verification status flag */
enum spi_ver_status_flag {
    SPI_VER_RES_PASS            = 0,
    SPI_VER_RES_FAIL            = (1ul << 0),
};

typedef U32 (*test_case)(void);

void test_case_spi_main(void);

#endif
#endif
