/*
 * Copyright 2018 @EGISTEC. All rights reserved.
 * File: common.h
 */

#ifndef _COMMON_H_
#define _COMMON_H_

#include <_types.h>


#define CHK_ALIGN_4                         (0x03ul)

#define TEST_ALIGN_4(a)                     (((a) & CHK_ALIGN_4) != 0)

#define SET_INC_ALIGN_4(x)                  (((x) + (CHK_ALIGN_4)) & ~(CHK_ALIGN_4))
#define SET_DEC_ALIGN_4(x)                  ((x) & ~(CHK_ALIGN_4))

/* bit operation */
#define BIT32_MASK(off, len)                ((U32)((~((~0ul) << (len))) << (off)))

#define SEED                                (0x87654321)


int rand(void);
void hexdump(U32 addr, U32 len);
void hexdump_align(U32 addr, U32 len);

#if (__STDC_VERSION__ >= 201112L)
    /* C11 support */
#else
#error "not C11"
#endif

#endif
