/*
 * Copyright 2018 @EGISTEC. All rights reserved.
 * File: serial.c
 */

#include <proj_config.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include <common.h>
#include <serial.h>
#include "Driver_USART.h"


#define putchar(c)                          outbyte(c)
#define getchar(c)                          inbyte(c)

#define CMD_BUF_LEN                         (24)
#define CMD_NUM_MAX                         (16)
#define CMD_PROMPT                          ("> ")
#define MAXDELIMETER                        (2)

#define SERIALCOMMANDDEBUG                  (1)
#undef SERIALCOMMANDDEBUG

enum {
    NUL   = 0x00,
    BS    = 0x08,
    TAB   = 0x09,
    LF    = 0x0a,
    CR    = 0x0d,
    ESC   = 0x1b,
    SPACE = 0X20,
    UP    = 0x41,
    DOWN  = 0x42,
    RIGHT = 0x43,
    LEFT  = 0x44,
    DEL   = 0x7f,
};

struct serial_cmd_callback {
    char command[CMD_BUF_LEN];
    void (*function)();
};

static struct serial_cmd_callback cmd_list[CMD_NUM_MAX];
static int in_char;
static char buffer[CMD_BUF_LEN];
static int  buf_pos; // Current position in the buffer
static char *token; // Returned token from the command buffer as returned by strtok_r
static char *last; // State variable used by strtok_r during processing

// null-terminated list of character to be used as delimeters for tokenizing (default " ")
static char delim[MAXDELIMETER];

static void (*def_handler)();
static int num_cmd;


// Retrieve the next token ("word" or "argument") from the Command buffer.
// returns a NULL if no more tokens exist.
char *serial_next(void)
{
    char *nextToken;
    nextToken = strtok_r(NULL, delim, &last);
    return nextToken;
}

static void clear_buffer(void)
{
    memset((void *)buffer, 0, sizeof(buffer));
    buf_pos = 0;
}

extern NDS_DRIVER_USART Driver_USART1;

void serial_cmd_proc(void)
{
    extern int putchar(int c);
    extern int getchar(void);

    while (Driver_USART1.RxAvaiable()) {
        bool matched;

        in_char = getchar();

        if (in_char == ESC) {
            do {
                in_char = getchar();
            } while (in_char != -1);
            continue;
        }

        if ((in_char == LF) || (in_char == CR)) {
            putchar('\n');
#ifdef SERIALCOMMANDDEBUG
            printf("Received: %s", buffer);
#endif

            buf_pos = 0; // Reset to start of buffer
            token = strtok_r(buffer, delim, &last); // Search for command at start of buffer

            if (token == NULL) {
                printf("%s", (char *)CMD_PROMPT);
                return;
            }

            matched = false;

            if (strncmp(token, "?", CMD_BUF_LEN) == 0) {

                printf("commands: ");
                for (U32 new_line = 0, i = num_cmd; i >= 1;) {

                    printf("%s ", cmd_list[--i].command);

                    if ((++new_line % 8) == 0)
                        printf("\n");

                }
                matched = true;

            } else {

                for (U32 i = 0; i < num_cmd; ++i) {
#ifdef SERIALCOMMANDDEBUG
                    printf("Comparing [%s] to [%s]\n", token, cmd_list[i].command);
#endif
                    // Compare the found command against the list of known commands for a match
                    if (strncmp(token, cmd_list[i].command, CMD_BUF_LEN) == 0) {
#ifdef SERIALCOMMANDDEBUG
                        printf("Matched Command: %s\n", token);
#endif
                        // Execute the stored handler function for the command
                        (*cmd_list[i].function)();
                        matched = true;
                        break;
                    }
                }
            }

            if (matched == false)
                (*def_handler)();

            putchar('\n');
            printf("%s", (char *)CMD_PROMPT);
            clear_buffer();

        } else if (isprint(in_char)) {
            putchar(in_char);

            /* Only printable characters into the buffer */
            if (buf_pos < (CMD_BUF_LEN - 1)) {
                buffer[buf_pos++] = in_char;  // Put character into buffer
                buffer[buf_pos] = '\0';      // Null terminate
            } else {
                printf("err: serial buffer full\n");
            }

        } else {
            if (buf_pos && (in_char == BS)) { // backspace
                printf("\b \b");
                buffer[--buf_pos] = '\0';
            } else {
                // not support yet
            }
        }
    }
}

// Adds a "command" and a handler function to the list of available commands.
// This is used for matching a found token in the buffer, and gives the pointer
// to the handler function to deal with it.
void serial_cmd_add(const char *command, void (*function)())
{
    if (num_cmd < CMD_NUM_MAX) {
#ifdef SERIALCOMMANDDEBUG
        printf("%u-Adding command for %s\n", num_cmd, command);
#endif

        if ((strlen(command) + 1) > CMD_BUF_LEN) {
            printf("warning: no enough command buf, %s %u", \
                   command, (strlen(command) + 1));
        }

        strncpy(cmd_list[num_cmd].command, command, (CMD_BUF_LEN - 1));
        cmd_list[num_cmd].command[(CMD_BUF_LEN - 1)] = '\0';
        cmd_list[num_cmd++].function = function;

    } else {
        printf("err: commands list full\n");
    }
}

static void serial_cmd_add_def_handler(void (*function)(void))
{
    def_handler = function;
}

static void unrecognized(void)
{
    printf("err: unrecognized commands");
}

extern void common_cmd_init(void);

void serial_cmd_init(void)
{
    strncpy(delim, " ", MAXDELIMETER);  // strtok_r needs a null-terminated string
    clear_buffer();

    common_cmd_init();

    serial_cmd_add_def_handler(unrecognized);

    printf("%s", (char *)CMD_PROMPT);
}

