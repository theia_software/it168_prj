/*
 * Copyright (c) 2012-2018 Andes Technology Corporation
 * All rights reserved.
 *
 */

#ifndef __TIME_AE250_H
#define __TIME_AE250_H

#include "ae250.h"
#include "Driver_TIME.h"
#include "platform.h"

#define HAL_MTIMER_INITIAL()
#define HAL_MTIME_ENABLE()                              set_csr(NDS_MIE, MIP_MTIP)
#define HAL_MTIME_DISABLE()                             clear_csr(NDS_MIE, MIP_MTIP);


// TIME Information (Run-Time)
typedef struct TIME_INFO {
  NDS_TIME_SignalEvent_t  cb_event;      // Event callback
  char                    oneshot;       //
  char                    status;        // timer running or not
  uint32_t				  timeout_time;	 // timeout_time_ms
  uint32_t				  target_time;	 // timeout_time_ms
} TIME_INFO;

typedef struct DELAY_INFO {
  char                    status;        // timer running or not
  uint32_t				  target_time;	 // timeout_time_ms
} DELAY_INFO;

// Time Resources definitions
typedef struct {
  char     capabilities; // Capabilities
  TIME_INFO              *timer1;          // Pointer to timer 1
  DELAY_INFO              *delay;          // Pointer to timer 1
} const TIME_RESOURCES;

void it168_timer_init(void);

#endif /* __TIME_AE250_H */
