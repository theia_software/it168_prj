
#ifndef _SYSTEM_MODULE_H_
#define _SYSTEM_MODULE_H_

#include "common.h"

#define SYS_RESET_SPI_SLAVE                 (0x10000058UL)
#if (CONFIG_SPI_SLAVE_IDX == CONFIG_SPI_SLAVE_00)
    #define SYS_RESET_SPI_SEL               (SYS_RESET_SPI_S0)
#else
    #define SYS_RESET_SPI_SEL               (SYS_RESET_SPI_S1)
#endif
    #define SYS_RESET_SPI_ALL               (SYS_RESET_SPI_S0 | SYS_RESET_SPI_S1)
    #define SYS_RESET_SPI_S0                (BIT(0))
    #define SYS_RESET_SPI_S1                (BIT(8))
#define SYS_GEN_DATE                        (0x10000000UL)
#define SYS_GEN_VER                         (0x10000004UL)
#define SYS_GEN_TIME                        (0x10000008UL)

#define SYS_RESET_RISCV                     (0x10000038ul)
    #define RESET_SYS                       (BIT(1))
    #define RESET_RISCV                     (BIT(16))
    #define RELEASE_RESET_RISCV             (0)

#define SYS_RISCV_RESET_VECTOR              (0x1000005cUL)
    #define SET_RISCV_VECTOR                (0)

#define SYS_CLK_EN                          (0x100000d4)
#define SET_SRAM_CLK_EN                     (BIT(0))
#define SET_DSP_CLK_EN                      (BIT(8))
#define SET_RISCV_CLK_EN                    (BIT(16))

#define SYS_OSC_160M                        (0x100000e8)
    #define SYS_OSC_160M_DEF                (0x00080001)
    #define SYS_OSC_160M_FREQ_DEF           (8)
    #define SYS_OSC_160M_EN                 (BIT(0))
    #define SYS_OSC_160M_FREQ_OFF           (16)
    #define SYS_OSC_160M_FREQ_MASK          (BIT32_MASK(SYS_OSC_160M_FREQ_OFF, 6)) // bit 16 ~ 21

#define SYS_FREQ_METER_INPUT_SEL            (0x1000001cul)
    #define SYS_FREQ_METER_INPUT_GPIO_0     (0)
    #define SYS_FREQ_METER_INPUT_FPIO_7     (1)
    #define SYS_FREQ_METER_INPUT_SPIS0      (2)
    #define SYS_FREQ_METER_INPUT_SPIS1      (3)
    #define SYS_FREQ_METER_INPUT_32K_OSC    (4)
#define SYS_FREQ_METER_CYCLE                (0x10000020ul)
#define SYS_FREQ_METER_COUNTER_DONE         (0x10000024ul)
#define SYS_FREQ_METER_TRIG                 (0x10000028ul)
    #define SET_SYS_FREQ_METER_TRIG         (1)
#define SYS_FREQ_METER_COUNTER_H            (0x1000002cul)
#define SYS_FREQ_METER_COUNTER_L            (0x10000030ul)
#define SYS_FREQ_METER_COUNTER              (0x10000034ul)

#define SYS_RISCV_FTAG_EN                   (0x100002d8UL)

U32 sys_clk_cfg(S32 clk_sel);
void sys_info_cmd(void);
void sys_clk_cmd(void);
void sys_clk_ver_cmd(void);

#endif

