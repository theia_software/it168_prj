#include <stdio.h>
#include <stdlib.h>
#include "dsp.h"
#include "TconModule.h"
#include "SystemModule.h"
#include "SensorTopModule.h"
#include "SPIUtility.h"
#include "SPIModule.h"
#include "serial_command.h"
#include "common.h"



U32 sys_clk_cfg(S32 clk_sel)
{
    U32 set_sys_clk;
    U32 read_sys_clk;
    S32 et760_sys_clk;
    S32 clk_step;

    /* config ET760 system clock */
    read_sys_clk = SPIUtility::read_single32(SYS_OSC_160M);
    et760_sys_clk = ((read_sys_clk & SYS_OSC_160M_FREQ_MASK) >> SYS_OSC_160M_FREQ_OFF);

    clk_step = ((et760_sys_clk < clk_sel) ? 1 : -1);

#if 0
    print_str_dec("clk_sel", clk_sel);
    print_str_dec("clk step", clk_step);
    print_str_hex("SYS_OSC_160M", set_sys_clk);
    print_str_dec("et760_sys_clk", et760_sys_clk);
#endif

    set_sys_clk = read_sys_clk;

    while (et760_sys_clk != clk_sel) {
        et760_sys_clk += clk_step;

        set_sys_clk &= ~SYS_OSC_160M_FREQ_MASK;
        set_sys_clk |= (et760_sys_clk << SYS_OSC_160M_FREQ_OFF);

        SPIUtility::write_single32(SYS_OSC_160M, set_sys_clk);
    }

    read_sys_clk = SPIUtility::read_single32(SYS_OSC_160M);

    if (read_sys_clk != set_sys_clk) {
        Serial.println(F("\r\nconfig ET760 system clock fail"));
        print_str_hex("set_sys_clk", set_sys_clk);
        print_str_hex("read_sys_clk", read_sys_clk);
        return 1;
    }
    return 0;
}

void sys_info_cmd(void)
{
    Serial.println(F("=== ET760 system info ==="));

    Serial.print(F("SYS_GEN_DATE\t"));
    read_dump_4(SYS_GEN_DATE);
    Serial.print(F("SYS_GEN_VER\t"));
    read_dump_4(SYS_GEN_VER);

    Serial.print(F("SPIS0_GEN_DATE\t"));
    read_dump_4(SPIS0_GEN_DATE);
    Serial.print(F("SPIS0_GEN_VER\t"));
    read_dump_4(SPIS0_GEN_VER);

    Serial.print(F("SPIS1_GEN_DATE\t"));
    read_dump_4(SPIS1_GEN_DATE);
    Serial.print(F("SPIS1_GEN_VER\t"));
    read_dump_4(SPIS1_GEN_VER);

    Serial.print(F("TCON_GEN_DATE\t"));
    read_dump_4(TCON_GEN_DATE);
    Serial.print(F("TCON_GEN_VER\t"));
    read_dump_4(TCON_GEN_VER);

    Serial.print(F("SENSOR_GEN_DATE\t"));
    read_dump_4(SENSOR_GEN_DATE);
    Serial.print(F("SENSOR_GEN_VER\t"));
    read_dump_4(SENSOR_GEN_VER);

    Serial.print(F("DSP_GEN_DATE\t"));
    read_dump_4(DSP_GEN_DATE);
    Serial.print(F("DSP_GEN_VER\t"));
    read_dump_4(DSP_GEN_VER);

    Serial.println(F("==================="));
}

void sys_clk_cmd(void)
{
#define SYS_CLK_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[SYS_CLK_CMD_ARG_NUM_MAX];
    U32 argc;
    U32 sys_clk;
    char *arg;


    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= SYS_CLK_CMD_ARG_NUM_MAX) {
            Serial.println(F("err: too many arguments"));
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 10);

#ifdef SERIALCOMMANDDEBUG
        Serial.print(argc);
        Serial.print(" argument ");
        print_hex(arg_ary[argc]);
#endif
        argc += 1;
    }

    if (argc != SYS_CLK_CMD_ARG_NUM_MAX) {
        Serial.print(F("err: too few arguments "));
        Serial.print(argc);
        return;
    }

    sys_clk = arg_ary[0];

    {
        char print_buf[64];
        snprintf(print_buf, sizeof(print_buf), \
                 "set ET760 system clock OSC160M_FERQ %lu", sys_clk);

        Serial.print(print_buf);
    }

    /* config ET760 system clock */
    sys_clk_cfg(sys_clk);
}

void sys_clk_ver_cmd(void)
{
#define SYS_CLK_VER_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[SYS_CLK_VER_CMD_ARG_NUM_MAX];
    U32 argc;
    U32 count, count_l, count_h;
    U32 freq_meter_cycle;
    const U8 arduino_clk = 16; // Arduino systom clock = 16MHz
    const U8 spi_clk = 1;
    char *arg;
    char print_buf[64];


    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= SYS_CLK_VER_CMD_ARG_NUM_MAX) {
            Serial.println(F("err: too many arguments"));
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 16);
        argc += 1;
    }

    freq_meter_cycle = ((argc) ? arg_ary[0] : 1024);

    {
        const U8 test_data = 0x5a;
        U8 chk_spi_no;

        SPIUtility::write_single32(SPISN_MSB_INVERT, 1);

        SPIUtility::write_single8(SPISN_DUMMY_01, test_data);
        chk_spi_no = SPIUtility::read_single8(SPISN_DUMMY_01);

        /* reset SPI Slave */
        SPIUtility::write_single32(SYS_RESET_SPI_SLAVE, SYS_RESET_SPI_ALL);

        if (chk_spi_no != (test_data ^ (1 << 7))) {
            Serial.println(F("err: SPI slave no."));
            return;
        }
    }

    /* config Arduino SPI master clock */
    SPIUtility::set_spi_clk_div(arduino_clk / spi_clk);

    snprintf(print_buf, sizeof(print_buf), \
             "set Arduino SPI master clock to %uMHz", spi_clk);
    Serial.println(print_buf);

#if (CONFIG_SPI_SLAVE_IDX == CONFIG_SPI_SLAVE_00)
    SPIUtility::write_single32(SYS_FREQ_METER_INPUT_SEL, SYS_FREQ_METER_INPUT_SPIS0);
#else
    SPIUtility::write_single32(SYS_FREQ_METER_INPUT_SEL, SYS_FREQ_METER_INPUT_SPIS1);
#endif

    print_str_hex("set freq_meter_cycle", freq_meter_cycle);
    SPIUtility::write_single32(SYS_FREQ_METER_CYCLE, freq_meter_cycle);
    SPIUtility::write_single32(SYS_FREQ_METER_TRIG, SET_SYS_FREQ_METER_TRIG);

    while (!SPIUtility::read_single32(SYS_FREQ_METER_COUNTER_DONE));

    count_h = SPIUtility::read_single32(SYS_FREQ_METER_COUNTER_H);
    count_l = SPIUtility::read_single32(SYS_FREQ_METER_COUNTER_L);
    count = SPIUtility::read_single32(SYS_FREQ_METER_COUNTER);

    print_str_hex("count_h", count_h);
    print_str_hex("count_l", count_l);
    print_str_hex("count  ", count);
    if (count != count_h + count_l)
        Serial.println(F("count error"));

    snprintf(print_buf, sizeof(print_buf), \
             "actual system clock: %luMHz", (count_h / (freq_meter_cycle >> 1)));
    Serial.println(print_buf);
}

