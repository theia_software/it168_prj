
#ifndef _SENSOR_TOP_MODULE_H_
#define _SENSOR_TOP_MODULE_H_

#define SENSOR_GEN_DATE                 (0x40000000)
#define SENSOR_GEN_VER                  (0x40000004)
#define SENSOR_DONE                     (0x40000008)
#define SENSOR_START_TRIG               (0x4000000c)
#define SENSOR_DONE_CLEAR               (0x40000010)
#define SENSOR_MINMAX_CLEAR             (0x40000014)
#define SENSOR_RST                      (0x40000018)
#define SENSOR_STATUS                   (0x4000001c)

#define SENSOR_CLK_DIV_CNT              (0x40000020)
#define SENSOR_TRI_SRC                  (0x40000024)
#define SENSOR_ARBITER_STATUS           (0x40000028)
#define SENSOR_ADC_ADDR_INCREASE        (0x4000002c)
#define SENSOR_ADC_ADDR_BASE_CLEAR      (0x40000030)

#define SENSOR_MAX                      (0x40001000)
#define SENSOR_MIN                      (0x40001004)
#define SENSOR_SUM_TIME                 (0x40001008)
#define SENSOR_CH_NUM                   (0x4000100c)
#define SENSOR_START_CH                 (0x40001010)
#define SENSOR_ATART_ADDR               (0x40001014)
#define SENSOR_SHIFT_BIT                (0x40001018)
#define SENSOR_REPEAT_CNT               (0x40001024)
#define SENSOR_DUMMY_1                  (0x40001028)
#define SENSOR_DUMMY_2                  (0x4000102c)

#endif

