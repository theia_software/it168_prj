
#ifndef _SPIUTILITY_h_
#define _SPIUTILITY_h_

#include <Arduino.h>
#include <stdint.h>


void spi_mode_cmd(void);

class SPIUtility
{
public:
    static void setup(void);
    static void readSingle32(uint32_t address, uint8_t output[4]);
    static void writeSingle32(uint32_t address, uint8_t input[4]);

    static void readSingle8(uint8_t address_offset, uint8_t *output);
    static void writeSingle8(uint8_t address_offset, uint8_t *input);

    static uint8_t read_single8(uint8_t address_offset);
    static void write_single8(uint8_t address_offset, uint8_t input);

    static void read_burst8(uint8_t address_offset, uint8_t *output, uint32_t output_size);
    static void write_burst8(uint8_t address_offset, uint8_t *input, uint32_t input_size);

    static void read_img(uint8_t *output, uint32_t output_size);

    static uint32_t read_single32(uint32_t address);
    static void write_single32(uint32_t address, uint32_t value);

    static void set_spi_mode(uint8_t mode);
    static void set_spi_clk_div(uint8_t clock_divider);

    static void read_burst8_speed(uint8_t dmy, uint8_t address_offset, \
                                  uint8_t *output, uint32_t output_size);
    static uint32_t read_single32_speed(uint8_t dmy, uint32_t address);
    static uint8_t read_single8_speed(uint8_t dmy, uint8_t address_offset);
};

#endif

