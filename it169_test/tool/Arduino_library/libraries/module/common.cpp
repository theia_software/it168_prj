
#include <Arduino.h>
#include <SPIModule.h>
#include <SPIUtility.h>
#include <common.h>
#include <stdio.h>

void print_hexw(U32 data)
{
    char print_buf[64]; // must define char type to print string by println

    snprintf(print_buf, sizeof(print_buf), "0x%08lx", data);
    Serial.print(print_buf);
}
void print_hex(U32 data)
{
    char print_buf[64]; // must define char type to print string by println

    snprintf(print_buf, sizeof(print_buf), "0x%08lx", data);
    Serial.println(print_buf);
}

void print_str_hex(char *str_p, U32 data)
{
    char print_buf[128]; // must define char type to print string by println

    snprintf(print_buf, sizeof(print_buf), "%s 0x%08lx", str_p, data);
    Serial.println(print_buf);
}

void print_str_dec(char *str_p, U32 data)
{
    char print_buf[64]; // must define char type to print string by println

    snprintf(print_buf, sizeof(print_buf), "%s %lu", str_p, data);
    Serial.println(print_buf);
}

/*
 * read & hexdump 4B
 */
U32 read_dump_4(U32 addr)
{
    U32 data;

    data = SPIUtility::read_single32(addr);
    print_hex(data);

    return data;
}

void hexdump(U32 addr, U32 len)
{
    U32 idx, off, one_round;
    U32 end_addr;
    U32 data;
    char dump[64] = {0};


    addr = SET_DEC_ALIGN_4(addr);
    len = SET_INC_ALIGN_4(len);

    end_addr = addr + len;

    for (idx = addr; idx < end_addr;) {
        off = snprintf(dump, sizeof(dump), "0x%08lx:", idx);
        one_round = (idx + 16);

        for (; (idx < one_round) && (idx < end_addr); idx += 4) {
            data = SPIUtility::read_single32(idx);
            off += snprintf((dump + off), sizeof(dump), " %08lx", data);
        }
        Serial.println(dump);
    }
    Serial.println("");
}

void hexdump_arduino(U32 addr, U32 len)
{
    U32 idx, off, one_round;
    U32 end_addr;
    char dump[64] = {0};


    end_addr = addr + len;

    for (idx = addr; idx < end_addr;) {
        off = snprintf(dump, sizeof(dump), "0x%08lx:", idx);
        one_round = (idx + 16);

        for (; (idx < one_round) && (idx < end_addr); ++idx) {
            off += snprintf((dump + off), sizeof(dump), " %02x", *((U8 *)idx));
        }
        Serial.println(dump);
    }
    Serial.println("");
}

