
#ifndef _COMMON_
#define _COMMON_

#include <stdint.h>

#define GETNAME(var)                        (#var)

#define CHK_ALIGN_2                         (0x01UL)
#define CHK_ALIGN_4                         (0x03UL)

#define TEST_ALIGN_2(a)                     (((a) & CHK_ALIGN_2) != 0)
#define TEST_ALIGN_4(a)                     (((a) & CHK_ALIGN_4) != 0)

#define SET_INC_ALIGN_4(x)                  (((x) + (CHK_ALIGN_4)) & ~(CHK_ALIGN_4))
#define SET_DEC_ALIGN_4(x)                  ((x) & ~(CHK_ALIGN_4))
#define SET_INC_ALIGN_2(x)                  (((x) + (CHK_ALIGN_2)) & ~(CHK_ALIGN_2))
#define SET_DEC_ALIGN_2(x)                  ((x) & ~(CHK_ALIGN_2))

/* bit operation */
#define BIT(n)                              (1UL << (n))
#define BIT32_MASK(off, len)                ((U32)((~((~0UL) << (len))) << (off)))

#define SET_BIT32(var, bit)                 ((var) = (var) | ((1UL) << (bit)))
#define CLR_BIT32(var, bit)                 ((var) = (var) & ~((1UL) << (bit)))
#define GET_BIT32(var, bit)                 (((var) >> (bit)) & 1UL)

#define SWAP(a, b)                          (((a) ^ (b)) && ((b) ^= (a) ^= (b), (a) ^= (b)))

/* bitmap */
#define SET_BMAP(a, b)                      ((a)[(b) >> 5] |= (1UL << ((b) & 31)))
#define CLR_BMAP(a, b)                      ((a)[(b) >> 5] &= ~(1UL << ((b) & 31)))
#define GET_BMAP(a, b)                      (((a)[(b) >> 5] & (1UL << ((b) & 31))) != 0)


typedef int32_t   S32;
typedef int16_t   S16;
typedef int8_t    S8;

typedef uint32_t  U32;
typedef uint16_t  U16;
typedef uint8_t   U8;

/* serial print APIs */
void print_hexw(U32 data);
void print_hex(U32 data);
void print_str_hex(char *str_p, U32 data);
void print_str_dec(char *str_p, U32 data);

/* read & dump RISC-V SRAM */
U32 read_dump_4(U32 addr);
void hexdump(U32 addr, U32 len);

/* dump Arduino memory */
void hexdump_arduino(U32 addr, U32 len);

#endif
