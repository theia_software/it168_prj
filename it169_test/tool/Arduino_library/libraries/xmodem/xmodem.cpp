#include <Arduino.h>
#include <SystemModule.h>
#include <SPIUtility.h>
#include <common.h>
#include <serial_command.h>

static int _inbyte(int msec)
{
    while (!Serial.available()) {
        delayMicroseconds(1000);
        if (msec-- <= 0)
            return -1;
    }
    return Serial.read();
}

static void _outbyte(unsigned char c)
{
    Serial.write(c);
}

static unsigned short crc16_ccitt(const unsigned char *buf, int sz)
{
    unsigned short crc = 0;
    while (--sz >= 0) {
        int i;
        crc ^= (unsigned short) *buf++ << 8;
        for (i = 0; i < 8; i++)
            if (crc & 0x8000)
                crc = crc << 1 ^ 0x1021;
            else
                crc <<= 1;
    }
    return crc;
}

#define SOH  0x01
#define STX  0x02
#define EOT  0x04
#define ACK  0x06
#define NAK  0x15
#define CAN  0x18
#define CTRLZ 0x1A
#define DLY_1S 1000
#define MAXRETRANS 25

static int check(int crc, const unsigned char *buf, int sz)
{
    if (crc) {
        unsigned short crc = crc16_ccitt(buf, sz);
        unsigned short tcrc = (buf[sz]<<8)+buf[sz+1];
        if (crc == tcrc)
            return 1;
    }
    else {
        int i;
        unsigned char cks = 0;
        for (i = 0; i < sz; ++i) {
            cks += buf[i];
        }
        if (cks == buf[sz])
        return 1;
    }
    return 0;
}

static void flushinput(void)
{
    while (_inbyte(((DLY_1S)*3)>>1) >= 0)
        ;
}

int32_t xmodem_receive(uint32_t dest, uint32_t destsz)
{
    uint32_t bufsz, crc = 0;
    uint32_t i, c, len = 0;
    uint32_t retry, retrans = MAXRETRANS;
    unsigned char xbuff[1030]; /* 1024 for XModem 1k + 3 head chars + 2 crc + nul */
    unsigned char *p;
    unsigned char trychar = 'C';
    unsigned char packetno = 1;


    if (dest & 3) {
        Serial.println(F("\r\nerror: address not aligned"));
        return -4;
    }

    for(;;) {
        for( retry = 0; retry < 16; ++retry) {
            if (trychar) _outbyte(trychar);
            if ((c = _inbyte((DLY_1S)<<1)) >= 0) {
                switch (c) {
                case SOH:
                    bufsz = 128;
                    goto start_recv;
                case STX:
                    bufsz = 1024;
                    goto start_recv;
                case EOT:
                    flushinput();
                    _outbyte(ACK);
                    return dest; /* normal end */
                    return len; /* normal end */
                case CAN:
                    if ((c = _inbyte(DLY_1S)) == CAN) {
                        flushinput();
                        _outbyte(ACK);
                        return -1; /* canceled by remote */
                    }
                    break;
                default:
                    break;
                }
            }
        }
        if (trychar == 'C') { trychar = NAK; continue; }
        flushinput();
        _outbyte(CAN);
        _outbyte(CAN);
        _outbyte(CAN);
        return -2; /* sync error */
    start_recv:
        if (trychar == 'C') crc = 1;
        trychar = 0;
        p = xbuff;
        *p++ = c;
        for (i = 0;  i < (bufsz+(crc?1:0)+3); ++i) {
            if ((c = _inbyte(DLY_1S)) < 0) goto reject;
            *p++ = c;
        }
        if (xbuff[1] == (unsigned char)(~xbuff[2]) &&
            (xbuff[1] == packetno || xbuff[1] == (unsigned char)packetno-1) &&
            check(crc, &xbuff[3], bufsz)) {
            if (xbuff[1] == packetno) {
                register uint32_t count = destsz - len;
                if (count > bufsz) count = bufsz;
                if (count > 0) {
                    uint32_t wdata, rdata;
                    unsigned char *pdata = &xbuff[3];

                    for (i = 0; i < (count >> 2) ; ++i) {

                        memcpy((void *)&wdata, (void *)pdata, 4);
                        pdata += 4;

                        SPIUtility::write_single32((uint32_t)dest, wdata);

                        rdata = SPIUtility::read_single32((uint32_t)(dest));

                        if (wdata != rdata) {
                            flushinput();
                            _outbyte(CAN);
                            _outbyte(CAN);
                            _outbyte(CAN);
                            return -5;
                        }
                        dest += 4;
                    }

                    len += count;
                }
                ++packetno;
                retrans = MAXRETRANS+1;
            }
            if (--retrans <= 0) {
                flushinput();
                _outbyte(CAN);
                _outbyte(CAN);
                _outbyte(CAN);
                return -3; /* too many retry error */
            }
            _outbyte(ACK);
            continue;
        }
reject:
        flushinput();
        _outbyte(NAK);
    }
}

void xmodem_load_fw_cmd(void)
{
#define XMODEM_LOAD_FW_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[XMODEM_LOAD_FW_CMD_ARG_NUM_MAX];
    U32 argc, addr;
    S32 rc;
    char *arg;

    argc = 0;
    while ((arg = scmd.next()) != NULL) {

        if (argc >= XMODEM_LOAD_FW_CMD_ARG_NUM_MAX) {
            Serial.println(F("err: too many arguments"));
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 16);
        argc += 1;
    }

    addr = (argc) ? arg_ary[0] : 0;

    Serial.println(F("reset system module"));
    SPIUtility::write_single32(SYS_RESET_RISCV, RESET_SYS);

    Serial.println(F("reset RISC-V"));
    SPIUtility::write_single32(SYS_RESET_RISCV, RESET_RISCV);

    rc = xmodem_receive(addr, ~0);

    if (rc < 0) {
        char print_buf[64]; // must define char type to print string by println
        snprintf(print_buf, sizeof(print_buf), "%s %ld", "\r\nerr: xmodem fail", rc);
        Serial.println(print_buf);

        if (rc == -5)
            Serial.println(F("SPI write/read check fail"));

        return;
    }

    Serial.print(F("\r\nxmodem load FW\r\naddr: "));
    print_hex(addr);

    Serial.print(F("len:  "));
    print_hex(rc);

    Serial.print(F("set RISC-V vector "));
    SPIUtility::write_single32(SYS_RISCV_RESET_VECTOR, addr);
    read_dump_4(SYS_RISCV_RESET_VECTOR);

    Serial.print(F("release RISC-V reset "));
    SPIUtility::write_single32(SYS_RESET_RISCV, RELEASE_RESET_RISCV);
    read_dump_4(SYS_RESET_RISCV);

    Serial.print(F("enable RISC-V clock"));
    SPIUtility::write_single32(SYS_CLK_EN, \
                               SET_RISCV_CLK_EN | SPIUtility::read_single32(SYS_CLK_EN));

    Serial.println(F("\r\nRISC-V load code done"));
}

