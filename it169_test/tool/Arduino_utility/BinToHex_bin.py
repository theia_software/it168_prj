import os
import math


input_file_path = 'C:\\...\\sa_andes\\base\\et760\\Debug\\output\\et760.bin'

output_file_path = 'C:\\...\\sa_andes\\tool\\Arduino_library\\libraries\\module\\load_riscv_fw.h'


output_fd = open(output_file_path, 'w')

totalByte = 0

RamSize1 = 'unsigned int WriteTotal_Bytes'
RamSize2 = ' = '
RamSizeEnd = ';\n'

RamName1 = 'const PROGMEM byte tx_data_init'
RamName2 = '['
RamName3 = '][4] = {\n'

FuncNameStart = 'void setRiscvInstr()\n{\n'
FuncName = 'setRiscvRam(WriteTotal_Bytes'
FuncNameComma = ', tx_data_init'
FuncName2 = ');\n'
FuncNameEnd = '}'

write_data = []
write_dataCollect = []
totalByteCollect = []
finalOutput = ''
totalsize = os.path.getsize(input_file_path)
tablLen = 3000      # This number must be in multiples of 4
tableNum = math.ceil(totalsize/tablLen)

with open(input_file_path, 'rb') as f:
    byte = f.read(1)    # Read 1 byte from file
    while byte:
        for tableIdx in range(tableNum):
            # init var
            write_data.clear()
            totalByte = 0

            while(totalByte < tablLen) and (byte):
                # te
                if(totalByte == tablLen-1):
                    totalByte = totalByte
                if ((totalByte % 4) == 0):
                    write_data.append('{')
                num = int.from_bytes(byte, 'big')
                convertdata = hex(num)
                write_data.append(convertdata.upper())
                if(((totalByte + 1) % 4) == 0):
                    write_data.append('},\n')
                else:
                    write_data.append(', ')
                totalByte += 1
                byte = f.read(1)
            temp = write_data.copy()
            totalByteCollect.append(totalByte)
            write_dataCollect.append(temp)

# write_data.append('};')

for tableIdx in range(tableNum):
    finalOutput = finalOutput + RamSize1 + \
        str(tableIdx) + RamSize2 + \
        str(math.ceil(totalByteCollect[tableIdx]/4)) + RamSizeEnd
for tableIdx in range(tableNum):
    finalOutput = finalOutput + RamName1 + \
        str(tableIdx) + RamName2 + \
        str(math.ceil(totalByteCollect[tableIdx]/4)) + RamName3
    for i in range(len(finalOutput)):
        output_fd.write(finalOutput[i])
    finalOutput = ''

    # process last char
    write_dataCollect[tableIdx][-1] = '}};\n\n'

    for i in range(len(write_dataCollect[tableIdx])):
        output_fd.write(write_dataCollect[tableIdx][i])

finalOutput = ''
finalOutput = 'void setRiscvRam(unsigned int WriteSize, byte InputRam[][4]);\n'
for i in range(len(finalOutput)):
    output_fd.write(finalOutput[i])
finalOutput = ''
finalOutput = finalOutput + FuncNameStart
for tableIdx in range(tableNum):
    finalOutput = finalOutput + FuncName + \
        str(tableIdx) + FuncNameComma + str(tableIdx) + FuncName2
finalOutput = finalOutput + FuncNameEnd
for i in range(len(finalOutput)):
    output_fd.write(finalOutput[i])
'''RamSize = RamSize + str(int(totalByte/4))+RamSizeEnd
RamName = RamSize+RamName1+str(int(totalByte/4))+RamName2
#write_data.insert(0,RamSize)
write_data.insert(0,RamName)'''

output_fd.close()
