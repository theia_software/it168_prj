/*
 * Copyright 2020 @IGISTEC. All rights reserved.
 * File: test_case_gpio.c
 */

#include <proj_config.h>
#include <test_case_config.h>

#if (CONFIG_PROJ_SEL == CONFIG_PROJ_TEST_CASE) && \
    (CONFIG_TEST_CASE_SEL == TEST_CASE_OTP)
#include <stdio.h>
#include <string.h>
#include <nds32_intrinsic.h>
#include <serial.h>
#include <test_case_otp.h>
#include <RTE_Device.h>
#include <sys.h>

#include "ae250.h"

static void wait_otp_done(void)
{
    while (1) {
        if ((RW_SYS_OTP_REG(OTP_BUSY_RELOAD_DONE) & OTP_BUSY_MASK) == 0) {
            break;
        }
    }
}

/*>>
 * 0 rg_otp_sel_ptm         0x2 PTM write mode = 3��b010
 * 0 ro_otp_busy            Wait until this value read 0x0
 * 1 rg_otp_sel_mode        0x2 OTP controller write mode = 2��b10
 * 1 rg_otp_sel_pdin        0x0 burn value 0
 * 1 rg_otp_sel_paio        0x00-0x1f Select bit from 0-31
 * 1 rg_otp_sel_pa          0x00-0xff Select OTP row address>
 * 2 wc_otp_start_trigger   0x1 Trigger OTP function
 * 3 ro_otp_busy            Wait until this value read 0x0
 */
static void command_burn_otp(U32 burn_adr, U32 burn_bit, U32 burn_val)
{

    if (burn_adr > OTP_ADR_MAX || burn_adr < OTP_ADR_MIN) {
        print_dbg(RED,  "\rOTP read address err: \n"
                  "\rOTP address range is form %x to %x\n",
                  OTP_ADR_MIN, OTP_ADR_MAX);
        return;
    } else if (burn_bit > OTP_BIT_MAX || burn_bit < OTP_BIT_MIN) {
        print_dbg(RED,  "\rOTP bit err: \n"
                  "\rOTP bit range is form %x to %x\n",
                  OTP_BIT_MIN, OTP_BIT_MAX);
        return;
    }

    RW_SYS_OTP_REG(SYS_OTP_SEL_PT) = OTP_SEL_PTM(0x2);
    wait_otp_done();

    RW_SYS_OTP_REG(SYS_OTP_SEL_PA) = OTP_SEL_MODE(OTP_SEL_MODE_WRITE) | \
                                     OTP_SEL_PDIN(burn_val) | \
                                     OTP_SEL_PAIO(burn_bit) | \
                                     OTP_SEL_PA(burn_adr);
    wait_otp_done();

    RW_SYS_OTP_REG(OTP_TRIGGER_RELOAD) = WC_OTP_TRIGGER;
    wait_otp_done();
}

/*
 * 0 rg_otp_sel_ptm         0x0 PTM read mode = 3��b000
 * 0 ro_otp_busy            Wait until this value read 0x0
 * 1 rg_otp_sel_mode        0x1 OTP controller read mode = 2��b01
 * 1 rg_otp_sel_pa          0x00-0xff Select OTP row address>
 * 1 rg_otp_sel_pas         0x0
 * 1 rg_otp_sel_paio        0x00  Select bit from 0-31
 * 2 wc_otp_start_trigger   0x1 Trigger OTP function
 * 3 ro_otp_busy            Wait until this value read 0x0
 * 4 ro_otp_pdout           Read OTP data
 */
static U32 command_read_otp(U32 read_adr)
{
    volatile int ret = 0;
    if (read_adr > OTP_ADR_MAX || read_adr < OTP_ADR_MIN) {
        print_dbg(RED,  "\rOTP read address  err: \n"
                  "\rOTP address range is form %x to %x\n",
                  OTP_ADR_MIN, OTP_ADR_MAX);
        return -1;
    }

    RW_SYS_OTP_REG(SYS_OTP_SEL_PT) = OTP_SEL_PTM(0x0);
    wait_otp_done();

    RW_SYS_OTP_REG(SYS_OTP_SEL_PA) = OTP_SEL_MODE(OTP_SEL_MODE_READ) | \
                                     OTP_SEL_PA(read_adr) | \
                                     OTP_SEL_PAS(0x0) | \
                                     OTP_SEL_PAIO(0x0);
    wait_otp_done();

    RW_SYS_OTP_REG(OTP_TRIGGER_RELOAD) = WC_OTP_TRIGGER;
    wait_otp_done();

    ret = RW_SYS_OTP_REG(OTP_PDOUT);
    return ret;
}

/*
 0 rg_otp_sel_ptm               0x2 PTM write mode = 3'b010
 0 ro_otp_busy                  Wait until this value read 0x0
 1 rg_otp_sel_mode              0x2 OTP controller write mode = 2'b10
 2 rg_otp_auto_burn_str_addr    0x00-0xff Select OTP row address for auto burn start point.
 3 wc_otp_auto_burn_tri         0x1 Trigger OTP auto burn
 4 ro_otp_busy                  Wait until this value read 0x0
 */
static void command_auto_burn_otp(U32 read_adr)
{
    if (read_adr > OTP_AUTO_BURN_MAX || read_adr < OTP_ADR_MIN) {
        print_dbg(RED,  "\rOTP auto burn address  err: \n"
                  "\rOTP address range is form %x to %x\n",
                  OTP_ADR_MIN, OTP_AUTO_BURN_MAX);
        return;
    }

    RW_SYS_OTP_REG(SYS_OTP_SEL_PT) = OTP_SEL_PTM(0x2);
    wait_otp_done();

//    RW_SYS_OTP_REG(SYS_OTP_SEL_PA) = 0;
//    wait_otp_done();
    RW_SYS_OTP_REG(SYS_OTP_SEL_PA) = OTP_SEL_MODE(OTP_SEL_MODE_WRITE);
    wait_otp_done();

    RW_SYS_OTP_REG(OTP_AUTO_BURN_STR_ADDR) = read_adr & OTP_AUTO_BURN_STR_ADDR_MASK;
    wait_otp_done();

    RW_SYS_OTP_REG(OTP_AUTO_BURN_TRI) = WC_OTP_AUTO_BURN_TRI;
    wait_otp_done();

}

/*
 0 rg_otp_sel_ptm                0x0 PTM read mode = 3'b000
 0 ro_otp_busy                   Wait until this value read 0x0
 1 rg_otp_sel_mode               0x1 OTP controller read mode = 2'b01
 1 rg_otp_sel_pa                 0x00-0xff Select OTP row address
 1 rg_otp_sel_pas                0x0
 1 rg_otp_sel_paio               0x00  Select bit from 0-31
 2 wc_otp_reload                 0x1
 */
static void command_reload_otp(U32 reload_adr, U32 reload_bit)
{
    if (reload_adr > OTP_RELOAD_MAX || reload_adr < OTP_ADR_MIN) {
        print_dbg(RED,  "\rOTP read address err: \n"
                  "\rOTP address range is form %x to %x\n",
                  OTP_ADR_MIN, OTP_RELOAD_MAX);
        return;
    } else if (reload_bit > OTP_BIT_MAX || reload_bit < OTP_BIT_MIN) {
        print_dbg(RED,  "\rOTP bit err: \n"
                  "\rOTP bit range is form %x to %x\n",
                  OTP_BIT_MIN, OTP_BIT_MAX);
        return;
    }

    RW_SYS_OTP_REG(SYS_OTP_SEL_PT) = OTP_SEL_PTM(0);
    wait_otp_done();

    RW_SYS_OTP_REG(SYS_OTP_SEL_PA) = OTP_SEL_MODE(OTP_SEL_MODE_READ) | \
                                     OTP_SEL_PA(reload_adr) | \
                                     OTP_SEL_PAS(0x0) | \
                                     OTP_SEL_PAIO(reload_bit);
    wait_otp_done();

    RW_SYS_OTP_REG(OTP_TRIGGER_RELOAD) = WC_OTP_RELOAD;

    wait_otp_done();
}

static void otp_manual_burn(U32 burn_adr, U32 burn_bit_s, U32 burn_bit_e, U32 burn_val)
{
    RW_SYS_OTP_REG(SYS_OTP_SEL_PT) = OTP_SEL_PTM(0x2) | OTP_MAN_CTRL_EN;
    delay_cycle(500);      //Tms > 1ns
    RW_SYS_OTP_REG(OTP_MAN) |= OTP_MAN_PCE_EN;
    delay_cycle(500);      //Tcs > 10us
    RW_SYS_OTP_REG(OTP_MAN) |= OTP_MAN_PPROG_EN;
    delay_cycle(500);      //Tpps > 5us

    for (int bit = burn_bit_s; bit <= burn_bit_e; bit++) {
        bool write_bit = ((burn_val >> bit) & 0x1);

        RW_SYS_OTP_REG(SYS_OTP_SEL_PA) = OTP_SEL_MODE(OTP_SEL_MODE_WRITE) | \
                                         OTP_SEL_PDIN(write_bit) | \
                                         OTP_SEL_PAIO(bit) | \
                                         OTP_SEL_PA(burn_adr);
        RW_SYS_OTP_REG(OTP_MAN) |= OTP_MAN_PWE_EN;
        delay_cycle(200);      //Tpw > 5us < 10us
        RW_SYS_OTP_REG(OTP_MAN) &= OTP_MAN_PWE_DIS;
        delay_cycle(500);      //Tpwi > 1us

    }
    delay_cycle(500);      //Tpph > 1us
    RW_SYS_OTP_REG(OTP_MAN) &= OTP_MAN_PPROG_DIS;
    delay_cycle(500);      //Tppr > 5us
    RW_SYS_OTP_REG(OTP_MAN) &= OTP_MAN_PCE_DIS;
    delay_cycle(500);      //Tmh > 1ns

    RW_SYS_OTP_REG(SYS_OTP_SEL_PT) = OTP_SEL_PTM(0x0);

}

static void otp_manual_read(U32 read_adr_s, U32 read_adr_e, U32 *read_data)
{
    RW_SYS_OTP_REG(SYS_OTP_SEL_PT) = OTP_SEL_PTM(0) | OTP_MAN_CTRL_EN;
    delay_cycle(1);      //Tms > 1ns
    RW_SYS_OTP_REG(OTP_MAN) |= OTP_MAN_PCE_EN;
    delay_cycle(625);      //Tcs > 10us

    //first pclk
    RW_SYS_OTP_REG(SYS_OTP_SEL_PA) = OTP_SEL_MODE(OTP_SEL_MODE_READ) | \
                                     OTP_SEL_PA(read_adr_s) | \
                                     OTP_SEL_PAS(0x0) | \
                                     OTP_SEL_PAIO(0x0);
    wait_otp_done();
//    delay_cycle(200);         //Tas > 1ns

    RW_SYS_OTP_REG(OTP_MAN) |= OTP_MAN_PCLK_EN;
    delay_cycle(155);       //Tah > 15ns

    RW_SYS_OTP_REG(OTP_MAN) &= OTP_MAN_PCLK_DIS;
    delay_cycle(200);       //Tcd (Tcd max 200 ns)

    *(U32*)(read_data) = RW_SYS_OTP_REG(OTP_DIRECT_PDOUT);

    //second pclk
    RW_SYS_OTP_REG(SYS_OTP_SEL_PA) = OTP_SEL_MODE(OTP_SEL_MODE_READ) | \
                                     OTP_SEL_PA((read_adr_s + 1)) | \
                                     OTP_SEL_PAS(0x0) | \
                                     OTP_SEL_PAIO(0x0);

    RW_SYS_OTP_REG(OTP_MAN) |= OTP_MAN_PCLK_EN;
    delay_cycle(100);      //Tcyc / 2 > 200 /2 ns

    RW_SYS_OTP_REG(OTP_MAN) &= OTP_MAN_PCLK_DIS;
    delay_cycle(100);      //Tcyc / 2 > 200 /2 ns

    read_data ++;

    for (int adr = read_adr_s + 2; adr <= read_adr_e; adr++) {
        *(U32*)(read_data) = RW_SYS_OTP_REG(OTP_DIRECT_PDOUT);
        RW_SYS_OTP_REG(SYS_OTP_SEL_PA) = OTP_SEL_MODE(OTP_SEL_MODE_READ) | \
                                         OTP_SEL_PA(adr) | \
                                         OTP_SEL_PAS(0x0) | \
                                         OTP_SEL_PAIO(0x0);
        RW_SYS_OTP_REG(OTP_MAN) |= OTP_MAN_PCLK_EN;
        delay_cycle(40);      //Tkh > 20 ns
        RW_SYS_OTP_REG(OTP_MAN) &= OTP_MAN_PCLK_DIS;
        delay_cycle(40);      //Tkl > 20 ns

//        delay_cycle(200);      //Tah > 15us
        read_data ++;

    }
    *(U32*)(read_data) = RW_SYS_OTP_REG(OTP_DIRECT_PDOUT);
    RW_SYS_OTP_REG(OTP_MAN) &= OTP_MAN_PCE_DIS;

    RW_SYS_OTP_REG(SYS_OTP_SEL_PT) = 0;

}

static void command_w_page_otp(U32 w_adr, U32 w_data)
{

    if (w_adr > OTP_ADR_MAX || w_adr < OTP_ADR_MIN) {
        print_dbg(RED,  "\rOTP read address err: \n"
                  "\rOTP address range is form %x to %x\n",
                  OTP_ADR_MIN, OTP_ADR_MAX);
        return;
    }
    for (int burn_bit = OTP_BIT_MIN; burn_bit <= OTP_BIT_MAX; burn_bit++) {
        command_burn_otp(w_adr, burn_bit, w_data >> burn_bit & 1);
    }

}

static void otp_dump(void)
{
    for (unsigned int otp_adr = OTP_ADR_MIN; otp_adr <= OTP_ADR_MAX;) {
        printf("0x%08x:", otp_adr);
        U32 one_round = (otp_adr + 4);

        for (; (otp_adr < one_round) && (otp_adr <= OTP_ADR_MAX); ++otp_adr) {
            printf(" %08x", (unsigned int)command_read_otp(otp_adr));
        }
        printf("\n");
    }
    printf("\n");

}
static void bank_dump(void)
{
    for (int rand_idx = OTP_BANK_0; rand_idx <= OTP_BANK_23; rand_idx += 4) {
        if (rand_idx == OTP_BANK_0 || (rand_idx % 0x10) == 0) {
            printf("\rOTP_BANK_%02d:", (rand_idx - OTP_BANK_0) / 4);
        }

        printf(" 0x%08x", (unsigned int)RW_SYS_OTP_REG(rand_idx));

        if ((rand_idx % 0x10) % 0xc == 0 && (rand_idx % 0x10) != 0) {
            printf("\n");
        }
    }
}

/* OTP INDEX        function describe
 * 0x00         :   BIT0 for protect bit, write 0x0 to enable function
 * 0x01~0x1f    :   if  (BIT30 xor BIT31) == 0x1 means the search index is valid to
 *                  auto-reload with BIT0~BIT7(limited between 0x20~0xe8) value
 * 0x20~0xef    :   storage setting to otp_bank_00~otp_bank_07
 *                  auto-reload will to set register otp_bank_00~otp_bank_07 if someone search index is valid .
 *                  BIT0~BIT7 can set value such as
 *                  0x20,0x28,0x30,0x38,0x40,0x48,0x50,0x58,0x60,0x68,
 *                  0x70,0x78,0x80,0x88,0x90,0x98,0xa0,0xa8,0xb0,0xb8,
 *                  0xc0,0xc8,0xd0,0xd8,0xe0,0xe8
 * 0xf0~0xff    :   to storage encode key
 */
U8 otp_index_page_search(void)
{
    uint8_t u8valid_index_page = 0;

    for(u8valid_index_page = 1; u8valid_index_page < 0x20; u8valid_index_page ++){
        if (0xffffffff == command_read_otp(u8valid_index_page)) {
            return u8valid_index_page;//return valid index page
        }
    }
    return 0xff;//valid index page are run out
}

U8 otp_new_bank_search(void)
{
    uint8_t valid_page = 0, u8bank_base = 0, u8blockVaild = 0;
    uint32_t u32read = 0;

    for (u8bank_base = 0x20; u8bank_base < 0xf0; u8bank_base = u8bank_base + 8) {
        u8blockVaild = 0;
        for (valid_page = u8bank_base; valid_page < (u8bank_base + 8); valid_page ++)
        {
            u32read = command_read_otp(valid_page);
            if (u32read == 0xffffffff) {
                u8blockVaild++;
            }
            if (u8blockVaild == 0x8) {
                return u8bank_base;
            }
        }
    }
    return 0xff;//no any block can use
}

static void burn_otp_test(void)
{
#define OTP_BURN_ARG_NUM_MAX       (3)
    U32 arg_ary[OTP_BURN_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;
    U32 burn_adr = 0;
    U32 burn_bit = 0;
    U32 burn_val = 0;

    while ((arg = serial_next()) != NULL) {

        if (argc >= OTP_BURN_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 16);
    }

    if (argc > 0) {
        if (arg_ary[0] > OTP_ADR_MAX || arg_ary[0] < OTP_ADR_MIN) {
            print_dbg(RED,  "\rOTP read address  err: \n"
                      "\rOTP address range is form %x to %x\n",
                      OTP_ADR_MIN, OTP_ADR_MAX);
            return;
        } else if (arg_ary[1] > OTP_BIT_MAX || arg_ary[1] < OTP_BIT_MIN) {
            print_dbg(RED,  "\rOTP bit err: \n"
                      "\rOTP bit range is form %x to %x\n",
                      OTP_BIT_MAX, OTP_BIT_MIN);
            return;
        } else {
            burn_adr = arg_ary[0];
            burn_bit = arg_ary[1];
            burn_val = arg_ary[2];
        }
    }

    printf("\rOTP Command Burn\n");
    command_burn_otp(burn_adr, burn_bit, burn_val);
}

static void read_otp_test(void)
{
#define OTP_READ_ARG_NUM_MAX       (1)
    U32 arg_ary[OTP_READ_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;
    U32 read_adr = 0;
    while ((arg = serial_next()) != NULL) {

        if (argc >= OTP_READ_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 16);
    }

    if (argc > 0) {
        if (arg_ary[0] > OTP_ADR_MAX || arg_ary[0] < OTP_ADR_MIN) {
            print_dbg(RED,  "\rOTP read address  err: \n"
                      "\rOTP address range is form %x to %x\n",
                      OTP_ADR_MIN, OTP_ADR_MAX);
            return;
        } else {
            read_adr = arg_ary[0];
        }
    }
    printf("\rOTP Command Read\n");
    printf("\rOTP_PDOUT = %x\n", (unsigned int)command_read_otp(read_adr));
}

static void otp_init(void)
{
    /* FPGA Need to Reset OTP 0xFF */
    for (int burn_adr = OTP_ADR_MIN; burn_adr <= OTP_ADR_MAX; burn_adr++) {
        command_w_page_otp(burn_adr, 0xffffffff);
    }

}

void auto_burn(void)
{
    U32 reload_addr = 0;
    U8 idxpage = 0x1, start_addr = 0;

    idxpage = otp_index_page_search();
    start_addr = otp_new_bank_search();

    if (idxpage == 0xff || idxpage == 0) {
        print_dbg(RED,  "\rSearch Index Page Fail\n");
        return;
    }
    if (start_addr == 0xff) {
        print_dbg(RED,  "\rBank page run out\n");
        return;
    }

    command_auto_burn_otp(start_addr);

    //to set valid bit to be valid
    reload_addr = ((~start_addr) & 0xff);
    reload_addr |= 0x7fffff00;
    command_w_page_otp(idxpage, reload_addr);

    //to set previous page index valid bit to be invalid
    //above of index 0
    if (idxpage > 1) {
        command_w_page_otp(idxpage - 1, 0x3fffffff);
    }

    return;
}

static U32 otp_manual_test(void)
{
    U32 rand_pattern[OTP_ADR_MAX + 1] = {0};
    U32 pattern_list[] = {0xffffffff, 0xaaaaaaaa, 0x55555555, 0x0};
    U32 read_data[0x80] = {0};

    for (int rand_idx = 0; rand_idx <= OTP_ADR_MAX; rand_idx ++) {
        rand_pattern[rand_idx] = (U32)rand();

    }

    for (int patn_iter = 0; patn_iter <= sizeof(pattern_list) / sizeof(U32); patn_iter++) {
        otp_init();

        for (int burn_adr = OTP_ADR_MIN; burn_adr <= OTP_ADR_MAX; burn_adr++) {
            if (patn_iter < sizeof(pattern_list) / sizeof(U32)) {
                otp_manual_burn(burn_adr, OTP_BIT_MIN, OTP_BIT_MAX,\
                                 pattern_list[patn_iter]);
            } else {
                otp_manual_burn(burn_adr, OTP_BIT_MIN, OTP_BIT_MAX,\
                                 rand_pattern[burn_adr]);
            }
        }
        otp_dump();
        otp_manual_read(OTP_ADR_MIN, OTP_ADR_MAX, read_data);

        /* compare data */
        if (patn_iter < sizeof(pattern_list) / sizeof(U32)) {
            for (int idx = 0; idx <= OTP_ADR_MAX; idx++) {
                if (read_data[idx] != pattern_list[patn_iter]) {
                    print_dbg(RED, "\rRead Burn test Fail Address = %x\n", idx);
                    print_dbg(RED, "\rFail Value is %x\n", pattern_list[patn_iter]);
                    otp_dump();
                    return OTP_VER_RES_FAIL;
                }
            }
        } else {
            for (int idx = 0; idx <= OTP_ADR_MAX; idx++) {
                if (read_data[idx] != rand_pattern[idx]) {
                    print_dbg(RED, "\rRead Burn test Fail Address = %x\n", idx);
                    print_dbg(RED, "\rFail Value is %x\n", read_data[idx]);
                    otp_dump();
                    return OTP_VER_RES_FAIL;
                }
            }
        }
    }
    return OTP_VER_RES_PASS;

}

static void otp_auto_burn_test(void)
{
#define OTP_AUTO_BURN_ARG_NUM_MAX       (1)
    U32 arg_ary[OTP_AUTO_BURN_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;
    U32 read_adr = 0;
    while ((arg = serial_next()) != NULL) {

        if (argc >= OTP_AUTO_BURN_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 16);
    }

    if (argc > 0) {
        if (arg_ary[0] > OTP_ADR_MAX || arg_ary[0] < OTP_ADR_MIN) {
            print_dbg(RED,  "\rOTP read address err: \n"
                      "\rOTP address range is form %x to %x\n",
                      OTP_ADR_MIN, OTP_ADR_MAX);
            return;
        } else {
            read_adr = arg_ary[0];
        }
    }
    printf("\rOTP Command Auto Burn\n");
    otp_init();
    for (int rand_idx = OTP_BANK_0; rand_idx < OTP_BANK_8; rand_idx += 4) {
        RW_SYS_OTP_REG(rand_idx) = (U32)rand();
    }
    bank_dump();

    command_auto_burn_otp(read_adr);
    otp_dump();

    for (int rand_idx = OTP_BANK_0; rand_idx < OTP_BANK_8; rand_idx += 4) {
        int otp_read_adr = read_adr + (rand_idx - OTP_BANK_0) / 4;
        int read_data = command_read_otp(otp_read_adr);

        if (read_data != RW_SYS_OTP_REG(rand_idx)) {
            print_dbg(RED,  "\rCompare Data err: \n"
                      "\rFail REG Address is 0x10000%x\n"
                      "\rFail OTP Address is 0x%x\n"
                      "\rREG Data = 0x%x, OTP Data = 0x%x\n",
                      rand_idx, otp_read_adr, RW_SYS_OTP_REG(rand_idx), read_data);
        }
    }

}

static U32 otp_reload_test(void)
{
    U32 reload_adr = 0;
    int burn_data = 0xffffffff;

    otp_init();

    while (reload_adr <2) {
        reload_adr = (U32)rand() % 0x69;
    }


    for (int rand_idx = OTP_BANK_0; rand_idx <= OTP_BANK_23; rand_idx += 4) {
        RW_SYS_OTP_REG(rand_idx) = 0x0;
    }

    command_w_page_otp(1, 0x7fffff00 | (U8)(~(reload_adr)));

    for (int burn_adr = reload_adr; burn_adr <= OTP_ADR_MAX; burn_adr++) {
        burn_data = burn_adr | burn_adr << 8 | burn_adr << 16 | burn_adr << 24;
        for (int burn_bit = OTP_BIT_MIN; burn_bit <= OTP_BIT_MAX; burn_bit++) {
            command_burn_otp(burn_adr, burn_bit, burn_data >> (burn_bit % 0x8) & 1);
        }
    }

    command_reload_otp(0, 0);

    for (int read_adr = reload_adr; read_adr < reload_adr + 0x8; read_adr ++) {
        U32 ans = command_read_otp(read_adr);
        U32 read_bank = OTP_BANK_0 + (read_adr - reload_adr) * 0x4;
        if (RW_SYS_OTP_REG(read_bank) != ans) {
            print_dbg(YELLOW, "\rreload_adr = %x\n",reload_adr);

            print_dbg(RED,  "\rCompare Data err: \n"
                            "\rFail REG Address is 0x10000%x\n"
                            "\rFail OTP Address is 0x%x\n"
                            "\rREG Data = 0x%x, OTP Data = 0x%x\n",
                            read_bank, read_adr,
                            RW_SYS_OTP_REG(read_bank), ans);
            bank_dump();
            otp_dump();
            return OTP_VER_RES_FAIL;
        }
    }

    return OTP_VER_RES_PASS;
}

static U32 otp_read_burn_test(void)
{
    U32 rand_pattern[OTP_ADR_MAX + 1] = {0};
    U32 pattern_list[] = {0xffffffff, 0xaaaaaaaa, 0x55555555, 0x0};

    for (int rand_idx = 0; rand_idx <= OTP_ADR_MAX; rand_idx ++) {
        rand_pattern[rand_idx] = (U32)rand();

    }

    for (int patn_iter = 0; patn_iter <= sizeof(pattern_list) / sizeof(U32); patn_iter++) {
        otp_init();

        for (int burn_adr = OTP_ADR_MIN; burn_adr <= OTP_ADR_MAX; burn_adr++) {
            for (int burn_bit = OTP_BIT_MIN; burn_bit <= OTP_BIT_MAX; burn_bit++) {
                if (patn_iter < sizeof(pattern_list) / sizeof(U32)) {
                    command_burn_otp(burn_adr, burn_bit, \
                                     pattern_list[patn_iter] >> burn_bit & 0x1);
                } else {
                    command_burn_otp(burn_adr, burn_bit, \
                                     rand_pattern[burn_adr] >> burn_bit & 0x1);
                }

            }
        }

        for (int read_adr = 0; read_adr <= OTP_ADR_MAX; read_adr++) {
            U32 ans = command_read_otp(read_adr);
            if (patn_iter < sizeof(pattern_list) / sizeof(U32)) {
                if (ans != pattern_list[patn_iter]) {
                    print_dbg(RED, "\rRead Burn test Fail Address = %x\n", read_adr);
                    print_dbg(RED, "\rFail Value is %x\n", ans);
                    otp_dump();
                    return OTP_VER_RES_FAIL;
                }
            } else {
                if (ans != rand_pattern[read_adr]) {
                    print_dbg(RED, "\rRead Burn test Fail Address = %x\n", read_adr);
                    print_dbg(RED, "\rFail Value is %x\n", ans);
                    otp_dump();
                    return OTP_VER_RES_FAIL;
                }
            }
        }
    }
    return OTP_VER_RES_PASS;
}

static void otp_auto_reload_test(void)
{
#define OTP_AUTO_RELOAD_NUM_MAX       (1)
    int burn_data = 0xffffffff;
    U32 arg_ary[OTP_AUTO_RELOAD_NUM_MAX];
    U32 argc = 0;
    char *arg;
    U32 reload_adr = 0;

    while ((arg = serial_next()) != NULL) {

        if (argc >= OTP_AUTO_RELOAD_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 16);
    }

    if (argc > 0) {
        if (arg_ary[0] > OTP_RELOAD_MAX || arg_ary[0] < OTP_ADR_MIN) {
            print_dbg(RED,  "\rOTP reload address  err: \n"
                      "\rOTP address range is form %x to %x\n",
                      OTP_ADR_MIN, OTP_RELOAD_MAX);
            return;
        } else {
            reload_adr = arg_ary[0];
        }
    }

    printf("\rotp initial test\n");

    print_dbg(YELLOW, "\r==== Reset to 0xff ====\n");
    for (int burn_adr = OTP_ADR_MIN; burn_adr <= OTP_ADR_MAX; burn_adr++) {
        command_w_page_otp(burn_adr, burn_data);

    }

    burn_data = 0x3fffff00 | (U8)(~(0x20));
    for (int burn_adr = 1; burn_adr < reload_adr - 1;burn_adr ++) {
        command_w_page_otp(burn_adr, burn_data);
    }

    burn_data = 0x7fffff00 | (U8)(~(reload_adr));
    command_w_page_otp(reload_adr - 1, burn_data);

    for (int burn_adr = reload_adr; burn_adr <= OTP_ADR_MAX; burn_adr++) {
        burn_data = burn_adr | burn_adr << 8 | burn_adr << 16 | burn_adr << 24;
        for (int burn_bit = OTP_BIT_MIN; burn_bit <= OTP_BIT_MAX; burn_bit++) {
            command_burn_otp(burn_adr, burn_bit, burn_data >> (burn_bit % 0x8) & 1);
        }
    }

}

static void otp_auto_test(void)
{
    U32 err_c;
    unsigned int otp_test_loop = 0;

    test_case case_list[] = {
        otp_read_burn_test,
        otp_manual_test,
        otp_reload_test,
    };

    printf("OTP test start\n");

    do {
        printf("test_loop 0x%08x\n", ++otp_test_loop);

        for (int iter = 0; iter < sizeof(case_list) / sizeof (test_case); iter ++) {

            if ((err_c = case_list[iter]())) {
                break;
            }
        }

        if (err_c) {
            print_dbg(RED, "err_c 0x%08x\n", err_c);
            break;
        }

    } while (1);
}

void test_case_otp_main(void)
{
//    RW_SYS_OTP_REG(OTP_PARAM_T_PW) = 0;
//    RW_SYS_OTP_REG(OTP_PARAM_T_WCYC) = 0;
//    RW_SYS_OTP_REG(OTP_PARAM_T_KH) = 2;
//    RW_SYS_OTP_REG(OTP_PARAM_T_CYC) = 13;
//    otp_reload_test();
    serial_cmd_add("cb", burn_otp_test);
    serial_cmd_add("cr", read_otp_test);

    serial_cmd_add("oi", otp_init);
    serial_cmd_add("od", otp_dump);

    serial_cmd_add("ab", auto_burn);
    serial_cmd_add("abt", otp_auto_burn_test);
    serial_cmd_add("alt", otp_auto_reload_test);

    serial_cmd_add("oat", otp_auto_test);

}
#endif
