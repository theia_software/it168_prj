/*
 * Copyright 2020 @IGISTEC. All rights reserved.
 * File: test_case_gpio.c
 */

#include <proj_config.h>
#include <test_case_config.h>

#if (CONFIG_PROJ_SEL == CONFIG_PROJ_TEST_CASE) && \
    (CONFIG_TEST_CASE_SEL == TEST_CASE_SPI)
#include <stdio.h>
#include <string.h>
#include <nds32_intrinsic.h>
#include <serial.h>
#include <test_case_spi.h>
#include <RTE_Device.h>
#include <sys.h>

#include "Driver_SPI.h"
#include "Driver_USART.h"
#include "ae250.h"
#include "gpio_ae250.h"
#include "spi_ae250.h"


extern NDS_DRIVER_SPI Driver_SPI0;

#define TEST_DATA_SIZE            8
#define TOTAL_TRANSFER_SIZE       (TEST_DATA_SIZE + 2) // Total transfer size is cmd(1) + dummy(1) + data(TEST_DATA_SIZE)

static volatile char spi_event_transfer_complete = 0;
static volatile int usart_event_complete = 0;

static int cpol_cpha_sel = NDS_SPI_CPOL0_CPHA0;
static int msb_lsb_sel = NDS_SPI_MSB_LSB;

volatile rec_spi_stat_t stat_record[30];
volatile int stat_record_cnt = 0;

#if SPI_TEST_MASTER_MODE
static void spi_prepare_data_in(uint8_t *data_in, uint8_t cmd, uint32_t dummy_cnt,
                                uint8_t *w_data, uint32_t wcnt)
{
    uint8_t *ptr = data_in;
    uint32_t i;

    // set 1-byte command
    *ptr++ = cmd;

    // set n-byte dummy
    for (i = 0; i < dummy_cnt; i++)
        *ptr++ = 0x2a;

    // set n-byte w_data
    for (i = 0; i < wcnt; i++)
        *ptr++ = *w_data++;
}
#endif

static void wait_complete(void)
{
    while (!spi_event_transfer_complete) {
        serial_cmd_proc();

    }
    spi_event_transfer_complete = 0;
}

void spi_callback(uint32_t event)
{
    int cntt = 0;
    switch (event) {
        case NDS_SPI_EVENT_TRANSFER_COMPLETE:
            spi_event_transfer_complete = 1;
            break;
        case NDS_SPI_EVENT_DATA_LOST:
//            *(uint32_t *)0x40000 = 1;
            while (1) {
//                if (AE250_SPI0->INTRST & 0x1 || AE250_SPI0->INTRST & 0x4) {
//                    *(uint32_t*)(0x45000 + cntt * 4) = AE250_SPI0->DATA;
//
//                    AE250_SPI0->SLVST |= 1 << 17;
//                    AE250_SPI0->INTRST |= 0x5;
//                    cntt ++;
//                } else
//                {
//                    break;
//                }
            }
            break;
    }
}

static void cpol_cpha_select(void)
{
#define CP_SEL_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[CP_SEL_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;

    while ((arg = serial_next()) != NULL) {

        if (argc >= CP_SEL_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        switch (arg_ary[0]) {
            case 0:
                cpol_cpha_sel = NDS_SPI_CPOL0_CPHA0;
                printf("\rNDS_SPI_CPOL0_CPHA0\n");
                break;
            case 1:
                cpol_cpha_sel = NDS_SPI_CPOL0_CPHA1;
                printf("\rNDS_SPI_CPOL0_CPHA1\n");
                break;
            case 2:
                cpol_cpha_sel = NDS_SPI_CPOL1_CPHA0;
                printf("\rNDS_SPI_CPOL1_CPHA0\n");
                break;
            case 3:
                cpol_cpha_sel = NDS_SPI_CPOL1_CPHA1;
                printf("\rNDS_SPI_CPOL1_CPHA1\n");
                break;
            default:
                print_dbg(RED, "\r error argument \n");
                break;
        }
    }

}

static void msb_lsb_select(void)
{
#define MLSB_SEL_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[MLSB_SEL_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;

    while ((arg = serial_next()) != NULL) {

        if (argc >= MLSB_SEL_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        switch (arg_ary[0]) {
            case 0:
                msb_lsb_sel = NDS_SPI_MSB_LSB;
                printf("\rNDS_SPI_MSB_LSB\n");
                break;
            case 1:
                msb_lsb_sel = NDS_SPI_LSB_MSB;
                printf("\rNDS_SPI_LSB_MSB\n");
                break;
            default:
                print_dbg(RED, "\r error argument \n");
                break;
        }
    }

}

static void transmode_dump(int mode, int *para)
{
    switch (mode) {
        case 0:
            *para = SPI_TRANSMODE_WRnRD;
            printf("SPI_TRANSMODE_WRnRD\n");
            break;
        case 1:
            *para = SPI_TRANSMODE_WRONLY;
            printf("SPI_TRANSMODE_WRONLY\n");
            break;
        case 2:
            *para = SPI_TRANSMODE_RDONLY;
            printf("SPI_TRANSMODE_RDONLY\n");
            break;
        case 3:
            *para = SPI_TRANSMODE_WR_RD;
            printf("SPI_TRANSMODE_WR_RD\n");
            break;
        case 4:
            *para = SPI_TRANSMODE_RD_WR;
            printf("SPI_TRANSMODE_RD_WR\n");
            break;
        case 5:
            *para = SPI_TRANSMODE_WR_DMY_RD;
            printf("SPI_TRANSMODE_WR_DMY_RD\n");
            break;
        case 6:
            *para = SPI_TRANSMODE_RD_DMY_WR;
            printf("SPI_TRANSMODE_RD_DMY_WR\n");
            break;
        case 7:
            *para = SPI_TRANSMODE_NONEDATA;
            printf("SPI_TRANSMODE_NONEDATA\n");
            break;
        case 8:
            *para = SPI_TRANSMODE_DMY_WR;
            printf("SPI_TRANSMODE_DMY_WR\n");
            break;
        case 9:
            *para = SPI_TRANSMODE_DMY_RD;
            printf("SPI_TRANSMODE_DMY_RD\n");
            break;
        default:
            print_dbg(RED, "error \n");
            break;
    }
}

static void trans_mode_select(void)
{
#define TRANSMODE_SEL_CMD_ARG_NUM_MAX   (2)
    U32 arg_ary[TRANSMODE_SEL_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;

    while ((arg = serial_next()) != NULL) {

        if (argc >= TRANSMODE_SEL_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        if (arg_ary[0] == TX_TRANSMODE_MODE) {
            printf("\rtx trans mode set : ");
            transmode_dump(arg_ary[1], &tx_transmode);
            printf("\r%x\n", tx_transmode);
        } else if (arg_ary[0] == RX_TRANSMODE_MODE) {
            printf("\rrx trans mode set : ");
            transmode_dump(arg_ary[1], &rx_transmode);
            printf("\r%x\n", rx_transmode);

        } else if (arg_ary[0] == TR_TRANSMODE_MODE) {
            printf("\rtx rx trans mode set : ");
            transmode_dump(arg_ary[1], &tr_transmode);
            printf("\r%x\n", tr_transmode);
        }

    }

}

static void dummy_cnt_set(void)
{
#define DUMMY_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[DUMMY_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;

    while ((arg = serial_next()) != NULL) {

        if (argc >= DUMMY_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }
    if (argc > 0) {
        dummy_cnt = arg_ary[0];
        printf("\rset dummy_cnt = %d\n", dummy_cnt);
    }
}

static void rx_threshold_set(void)
{
#define RX_THR_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[RX_THR_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;

    while ((arg = serial_next()) != NULL) {

        if (argc >= RX_THR_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }
    if (argc > 0) {
        rx_threshold = arg_ary[0];
        printf("\rset rx threshold %d\n", rx_threshold);
    }
}

static void tx_threshold_set(void)
{
#define TX_THR_CMD_ARG_NUM_MAX   (1)
    U32 arg_ary[TX_THR_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;

    while ((arg = serial_next()) != NULL) {

        if (argc >= TX_THR_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }
    if (argc > 0) {
        tx_threshold = arg_ary[0];
        printf("\rset tx threshold %d\n", tx_threshold);
    }
}

static void timing_select(void)
{
#define TRANSMODE_SEL_CMD_ARG_NUM_MAX   (2)
    U32 arg_ary[TRANSMODE_SEL_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;

    while ((arg = serial_next()) != NULL) {

        if (argc >= TRANSMODE_SEL_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        if (arg_ary[0] == TIMING_SCLK_DIV) {
            printf("\rsclk_div set : ");
            test_sclk_div = arg_ary[1];
            printf("%d\n", test_sclk_div);

        } else if (arg_ary[0] == TIMING_CSHT) {
            printf("\rcsht set : ");
            test_csht = arg_ary[1];
            printf("%d\n", test_csht);

        } else if (arg_ary[0] == TIMING_CS2SCLK) {
            printf("\rcs2clk set : ");
            test_cs2clk = arg_ary[1];
            printf("%d\n", test_cs2clk);
        } else {
            print_dbg(RED,  "\rerr:\n\r0 : sclk div\n"
                      "\r1 : csht\n"
                      "\r2 : cs2clk\n");
        }

    }

}

static void fifo_status_dump(dump_type type)
{
    switch (type) {
        case SPI_RX_STATUS:
            printf("\rRX threshould = %d\n", AE250_SPI0->CTRL >> 8 & 0xFF);

            printf("\rSTATUS SPIActive = %x\n", AE250_SPI0->STATUS & 0x1);
            printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
                   (AE250_SPI0->STATUS >> 18 & 0xC0));
            printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
            printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);
#if SPI_TEST_MASTER_MODE
            printf("\rTRANSCTRL RdTranCnt =  %x\n", AE250_SPI0->TRANSCTRL & 0x1FF);
#else
            printf("\rSLVDATACNT RCnt =  %x\n", AE250_SPI0->SLVDATACNT & 0x3FF);
#endif


            printf("\rSlvCmdInt =  %x\n", AE250_SPI0->INTRST >> 5 & 0x1);
            printf("\rEndInt =  %x\n", AE250_SPI0->INTRST >> 4 & 0x1);
            printf("\rRXFIFOInt =  %x\n", AE250_SPI0->INTRST >> 2 & 0x1);
            printf("\rRXFIFOORInt =  %x\n", AE250_SPI0->INTRST & 0x1);
            break;
        case SPI_TX_STATUS:
            printf("\rTX threshould = %d\n", AE250_SPI0->CTRL >> 16 & 0xFF);

            printf("\rSTATUS SPIActive = %x\n", AE250_SPI0->STATUS & 0x1);

            printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
                   (AE250_SPI0->STATUS >> 22 & 0xC0));
            printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
            printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);

#if SPI_TEST_MASTER_MODE
            printf("\rTRANSCTRL WrTranCnt =  %x\n", AE250_SPI0->TRANSCTRL >> 12 & 0x1FF);
#else
            printf("\rSLVDATACNT WCnt =  %x\n", AE250_SPI0->SLVDATACNT >> 16 & 0x3FF);
#endif

            printf("\rSlvCmdInt =  %x\n", AE250_SPI0->INTRST >> 5 & 0x1);
            printf("\rEndInt =  %x\n", AE250_SPI0->INTRST >> 4 & 0x1);
            printf("\rTXFIFOInt =  %x\n", AE250_SPI0->INTRST >> 3 & 0x1);
            printf("\rTXFIFOURInt =  %x\n", AE250_SPI0->INTRST >> 1 & 0x1);
            break;
        case SPI_RX_TX_STATUS:
            printf("\rRX threshould = %d\n", AE250_SPI0->CTRL >> 8 & 0xFF);
            printf("\rTX threshould = %d\n", AE250_SPI0->CTRL >> 16 & 0xFF);

            printf("\rSTATUS SPIActive = %x\n", AE250_SPI0->STATUS & 0x1);
            printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
                   (AE250_SPI0->STATUS >> 18 & 0xC0));
            printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
            printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);

            printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
                   (AE250_SPI0->STATUS >> 22 & 0xC0));
            printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
            printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);

#if SPI_TEST_MASTER_MODE
            printf("\rTRANSCTRL RdTranCnt =  %x\n", AE250_SPI0->TRANSCTRL & 0x1FF);
            printf("\rTRANSCTRL WrTranCnt =  %x\n", AE250_SPI0->TRANSCTRL >> 12 & 0x1FF);
#else
            printf("\rSLVDATACNT RCnt =  %x\n", AE250_SPI0->SLVDATACNT & 0x3FF);
            printf("\rSLVDATACNT WCnt =  %x\n", AE250_SPI0->SLVDATACNT >> 16 & 0x3FF);
#endif


            printf("\rSlvCmdInt =  %x\n", AE250_SPI0->INTRST >> 5 & 0x1);
            printf("\rEndInt =  %x\n", AE250_SPI0->INTRST >> 4 & 0x1);
            printf("\rTXFIFOInt =  %x\n", AE250_SPI0->INTRST >> 3 & 0x1);
            printf("\rRXFIFOInt =  %x\n", AE250_SPI0->INTRST >> 2 & 0x1);
            printf("\rTXFIFOURInt =  %x\n", AE250_SPI0->INTRST >> 1 & 0x1);
            printf("\rRXFIFOORInt =  %x\n", AE250_SPI0->INTRST & 0x1);
            break;
    }
}

static void status_record_dump(volatile rec_spi_stat_t *stat_rec, dump_type type)
{
    if (stat_rec) {
        switch (type) {
            case SPI_RX_STATUS:
                printf("\rSTATUS SPIActive = %x\n", stat_rec->status_b.SPIActive);

                printf("\rSTATUS RXNUM = %x\n", stat_rec->status_b.RXNUM_L |
                       (stat_rec->status_b.RXNUM_H << 8));
                printf("\rSTATUS RXEMPTY = %x\n", stat_rec->status_b.RXEMPTY);
                printf("\rSTATUS RXFULL = %x\n", stat_rec->status_b.RXFULL);

#if SPI_TEST_MASTER_MODE
                printf("\rTRANSCTRL RdTranCnt =  %x\n", stat_rec->mst_data_cnt_b.RdTranCnt);
#else
                printf("\rSLVDATACNT RCnt =  %x\n", stat_rec->slv_data_cnt_b.RCnt);
#endif


                printf("\rSlvCmdInt =  %x\n", stat_rec->irq_b.SlvCmdInt);
                printf("\rEndInt =  %x\n", stat_rec->irq_b.EndInt);
                printf("\rRXFIFOInt =  %x\n", stat_rec->irq_b.RXFIFOInt);
                printf("\rRXFIFOORInt =  %x\n", stat_rec->irq_b.RXFIFOORInt);
                break;
            case SPI_TX_STATUS:
                printf("\rSTATUS SPIActive = %x\n", stat_rec->status_b.SPIActive);

                printf("\rSTATUS TXNUM = %x\n", stat_rec->status_b.TXNUM_L | \
                       (stat_rec->status_b.TXNUM_H << 8));
                printf("\rSTATUS TXEMPTY = %x\n", stat_rec->status_b.TXEMPTY);
                printf("\rSTATUS TXFULL = %x\n", stat_rec->status_b.TXFULL);

#if SPI_TEST_MASTER_MODE
                printf("\rTRANSCTRL WrTranCnt =  %x\n", stat_rec->mst_data_cnt_b.WrTranCnt);
#else
                printf("\rSLVDATACNT WCnt =  %x\n", stat_rec->slv_data_cnt_b.WCnt);
#endif

                printf("\rSlvCmdInt =  %x\n", stat_rec->irq_b.SlvCmdInt);
                printf("\rEndInt =  %x\n", stat_rec->irq_b.EndInt);
                printf("\rTXFIFOInt =  %x\n", stat_rec->irq_b.TXFIFOInt);
                printf("\rTXFIFOURInt =  %x\n", stat_rec->irq_b.TXFIFOURInt);
                break;
            case SPI_RX_TX_STATUS:
                printf("\rSTATUS SPIActive = %x\n", stat_rec->status_b.SPIActive);

                printf("\rSTATUS RXNUM = %x\n", stat_rec->status_b.RXNUM_L |
                       (stat_rec->status_b.RXNUM_H << 8));
                printf("\rSTATUS RXEMPTY = %x\n", stat_rec->status_b.RXEMPTY);
                printf("\rSTATUS RXFULL = %x\n", stat_rec->status_b.RXFULL);
                printf("\rSTATUS TXNUM = %x\n", stat_rec->status_b.TXNUM_L | \
                       (stat_rec->status_b.TXNUM_H << 8));
                printf("\rSTATUS TXEMPTY = %x\n", stat_rec->status_b.TXEMPTY);
                printf("\rSTATUS TXFULL = %x\n", stat_rec->status_b.TXFULL);

#if SPI_TEST_MASTER_MODE
                printf("\rTRANSCTRL RdTranCnt =  %x\n", stat_rec->mst_data_cnt_b.RdTranCnt);
                printf("\rTRANSCTRL WrTranCnt =  %x\n", stat_rec->mst_data_cnt_b.WrTranCnt);
#else
                printf("\rSLVDATACNT RCnt =  %x\n", stat_rec->slv_data_cnt_b.RCnt);
                printf("\rSLVDATACNT WCnt =  %x\n", stat_rec->slv_data_cnt_b.WCnt);
#endif

                printf("\rSlvCmdInt =  %x\n", stat_rec->irq_b.SlvCmdInt);
                printf("\rEndInt =  %x\n", stat_rec->irq_b.EndInt);
                printf("\rRXFIFOInt =  %x\n", stat_rec->irq_b.RXFIFOInt);
                printf("\rTXFIFOInt =  %x\n", stat_rec->irq_b.TXFIFOInt);
                printf("\rRXFIFOORInt =  %x\n", stat_rec->irq_b.RXFIFOORInt);
                printf("\rTXFIFOURInt =  %x\n", stat_rec->irq_b.TXFIFOURInt);
                break;
        }
    }
}

static void sys_reset_cmd(void)
{
    *(uint32_t *)0x10000058 |= (1 << 16);

}

#if (SPI_TEST_MASTER_MODE == 1)
static void control_select(void)
{
#define CONTROL_SEL_CMD_ARG_NUM_MAX   (4)
    U32 arg_ary[CONTROL_SEL_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;

    while ((arg = serial_next()) != NULL) {

        if (argc >= CONTROL_SEL_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        switch (arg_ary[0]) {
            case CONTROL_CMD :
                if (arg_ary[1]) {
                    printf("\rcmd enable\n");
                    mst_cmd_en = SPI_CONTROL_CMD_EN;
                    mst_cmd = arg_ary[2];
                    printf("\rcmd is %x\n", mst_cmd);
                } else {
                    printf("\rcmd disable");
                    mst_cmd = 0;
                    mst_cmd_en = arg_ary[1];
                }
                break;
            case CONTROL_ADDR :
                if (arg_ary[1]) {
                    printf("\radr enable\n");
                    mst_adr_en = SPI_CONTROL_ADDR_EN;
                    mst_address = arg_ary[2];
                    mst_adr_len = arg_ary[3];
                    printf("\raddr is %x\n", mst_address);
                    printf("\raddr length is %x bytes\n", mst_adr_len + 1);
                } else {
                    printf("\radr disable\n");
                    mst_address = 0;
                    mst_adr_len = 0;
                    mst_adr_en = arg_ary[1];
                }
                break;
            case CONTROL_ADDR_FMT :
                if (arg_ary[1]) {
                    printf("\radr fmt enable\n");
                    mst_adr_fmt_en = SPI_CONTROL_ADDR_FMT_EN;
                } else {
                    printf("\radr fmt disable\n");
                    mst_adr_fmt_en = arg_ary[1];
                }
                break;
            case CONTROL_DUAL_QUAD_FMT :
                if (arg_ary[1] > 0 && arg_ary[1] < 3) {
                    printf("\rdual quad enable\n");
                    mst_dual_quad_en = SPI_CONTROL_ADDR_DUAL_QUAL_EN(arg_ary[1]);
                    if (arg_ary[1] == 0) {
                        printf("\rsingle mode\n");
                    } else if (arg_ary[1] == 1) {
                        printf("\rdual mode\n");
                    } else if (arg_ary[1] == 2) {
                        printf("\rquad mode\n");
                    } else {
                        printf("\runknown mode\n");
                        mst_dual_quad_en = 0;
                    }
                } else if (arg_ary[1] == 0) {
                    printf("\rdual quad disable\n");
                    mst_dual_quad_en = 0;
                } else {
                    print_dbg(RED,  "\rdual_quad set err:\n"
                              "\r0 : Regular (Single) mode\n"
                              "\r1 : Dual I/O mode\n"
                              "\r2 : Quad I/O mode\n");
                }
                break;
            case CONTROL_TOKEN_EN :
                if (arg_ary[1]) {
                    printf("\rtoken enable\n");
                    mst_token_en = SPI_CONTROL_TOKEN_EN;
                    mst_token_value = SPI_CONTROL_TOKEN_VALUE((arg_ary[2] & 0x1));
                    printf("\rtoken value is %x\n", mst_token_value);
                } else {
                    printf("\rtoken disable\n");
                    mst_token_en = arg_ary[1];
                    mst_token_value = 0;
                }
                break;
            case CONTROL_CS_PIN_EN:
                if (arg_ary[1]) {
                    printf("\rfw control cs pin enable\n");
                    RW_SYS_REG(SPI0_IO_CTRL) |= CSN_FW_CTRL_EN;
                    RW_SYS_REG(SPI0_IO_CTRL) |= CSN_IN_HIGH;
                } else {
                    printf("\rfw control cs pin disable\n");
                    RW_SYS_REG(SPI0_IO_CTRL) &= CSN_FW_CTRL_DIS;
                    RW_SYS_REG(SPI0_IO_CTRL) &= CSN_IN_LOW;

                }
                break;
            default:
                print_dbg(RED,  "\rerr:\n\r0 : cmd enable\n"
                          "\r1 : addr enable\n"
                          "\r2 : addr fmt enable\n"
                          "\r3 : dual quad enable\n"
                          "\r4 : token enable\n");
                break;
        }

    }

}

void dma_rw_test(void)
{
#define DMA_RW_SIZE             (20)
#define DMA_CMD_ARG_NUM_MAX     (1)
    U32 arg_ary[DMA_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    int data_num = DMA_RW_SIZE;
    char *arg;

    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data_in[DMA_RW_SIZE] = {0};
    uint8_t data_out[DMA_RW_SIZE] = {0};

    hal_gpio_init(HAL_GPIO_00, HAL_OUTPUT);

    while ((arg = serial_next()) != NULL) {

        if (argc >= DMA_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        if (arg_ary[0] > DMA_RW_SIZE) {
            print_dbg(RED, "\rerr: data number too many\n");
            return;
        } else {
            data_num = arg_ary[0];
        }
    }

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);

    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);

    // configure the SPI to master, 8-bit data length and bus speed to 1MHz
    SPI_Dri->Control(NDS_SPI_MODE_MASTER |
                     cpol_cpha_sel |
                     msb_lsb_sel     |
                     NDS_SPI_DATA_BITS(8), 1000000);

    print_dbg(YELLOW, "\n\r==== start master DMA read/write test ====\n\r");

    // prepare write data
    for (int i = 0; i < data_num; i++) {
        data_in[i] = i + 1;
    }

//    spi_prepare_data_in(data_in, 0x1a , 1, w_data, TEST_DATA_SIZE);

    if(RW_SYS_REG(SPI0_IO_CTRL) & CSN_FW_CTRL_CHK)
        RW_SYS_REG(SPI0_IO_CTRL) &= CSN_IN_LOW;
    // write data to slave
    SPI_Dri->Send(data_in, data_num);
    wait_complete();

    if(RW_SYS_REG(SPI0_IO_CTRL) & CSN_FW_CTRL_CHK)
        RW_SYS_REG(SPI0_IO_CTRL) |= CSN_IN_HIGH;
    {/* test CSHT */
//        for (int i = 0; i < data_num; i++) {
//            for (int j = 0; j < 0xFFFFFFF; j++)
//                data_in[i] = i + 1;
//        }
//        printf("\rdelay\n");
//        AE250_SPI0->DATA = 1;
//        AE250_SPI0->DATA = 2;
//        AE250_SPI0->DATA = 3;
//        AE250_SPI0->DATA = 4;
//        AE250_SPI0->CMD = 0;
    }

    for (int i = 0; i < data_num; i++)
        printf("\rw_data[%d] = %x\n", i, data_in[i]);

    // read data from slave
    SPI_Dri->Transfer(data_out, data_in, data_num);
    wait_complete();

    for (int i = 0; i < data_num; i++) {
        printf("\rdata_out[%d] = %x\n", i, data_out[i]);
    }

}

static void rx_int_test(void)
{
#define RXINT_TRANSFER_SIZE         (20)
#define RXINT_CMD_ARG_NUM_MAX       (1)
    U32 arg_ary[DMA_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    int data_num = RXINT_TRANSFER_SIZE;
    char *arg;

    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data_in[DMA_RW_SIZE] = {0};
    uint8_t data_out[DMA_RW_SIZE] = {0};

    while ((arg = serial_next()) != NULL) {

        if (argc >= RXINT_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        if (arg_ary[0] > RXINT_TRANSFER_SIZE) {
            print_dbg(RED, "\rerr: data number too many\n");
            return;
        } else {
            data_num = arg_ary[0];
        }
    }

    print_dbg(YELLOW, "\n\r==== start master RX int test ====\n\r");

    hal_gpio_init(HAL_GPIO_00, HAL_OUTPUT);

    printf("\rdata_in address = %x\n", (unsigned int)data_in);
    printf("\rdata_out address = %x\n", (unsigned int)data_out);
    printf("\rdata length = %d\n", (unsigned int)data_num);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);

    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);

    // configure the SPI to master, 8-bit data length and bus speed to 1MHz
    SPI_Dri->Control(NDS_SPI_MODE_MASTER |
                     cpol_cpha_sel |
                     msb_lsb_sel     |
                     NDS_SPI_DATA_BITS(8), 1000000);

    stat_record_cnt = 0;

    // prepare write data
    for (int i = 0; i < data_num; i++) {
        data_in[i] = 0;
    }

    print_dbg(YELLOW, "\n\r==== Start receive ====\n");
    fifo_status_dump(SPI_RX_STATUS);

    // read data from slave
    SPI_Dri->Transfer(data_out, data_in, data_num);
    wait_complete();

    for (int i = 0; i < stat_record_cnt; i++) {
        if (stat_record_cnt == i + 1) {
            print_dbg(YELLOW, "\n\r==== Irq %d times is the end int ====\n", i);
        } else {
            print_dbg(YELLOW, "\n\r==== Irq %d times ====\n", i);
        }
        status_record_dump(&stat_record[i], SPI_RX_STATUS);
    }

    for (int i = 0; i < data_num; i++) {
        printf("\rdata_out[%d] = %x\n", i, data_out[i]);
    }

    print_dbg(YELLOW, "\n\r==== After receive finish ====\n");
    fifo_status_dump(SPI_RX_STATUS);

}

static void tx_int_test(void)
{
#define TXINT_TRANSFER_SIZE       (20)
#define TXINT_CMD_ARG_NUM_MAX       (1)
    U32 arg_ary[TXINT_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    int data_num = TXINT_TRANSFER_SIZE;
    char *arg;

    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data[TXINT_TRANSFER_SIZE] = {0};

    while ((arg = serial_next()) != NULL) {

        if (argc >= TXINT_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        if (arg_ary[0] > TXINT_TRANSFER_SIZE) {
            print_dbg(RED, "\rerr: data number too many\n");
            return;
        } else {
            data_num = arg_ary[0];
        }
    }

    print_dbg(YELLOW, "\n\r==== start master TX int test ====\n");

    hal_gpio_init(HAL_GPIO_00, HAL_OUTPUT);

    printf("\rdata address = %x\n", (unsigned int)data);
    printf("\rdata length = %d\n", (unsigned int)data_num);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);

    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);

    // configure the SPI to master, 8-bit data length and bus speed to 1MHz
    SPI_Dri->Control(NDS_SPI_MODE_MASTER |
                     cpol_cpha_sel |
                     msb_lsb_sel     |
                     NDS_SPI_DATA_BITS(8), 1000000);

    stat_record_cnt = 0;

    printf("\n\rreceive data length : %d\n", data_num);
    for (int cnt = 0; cnt < data_num; cnt ++) {
        data[cnt] = cnt + 1;
    }

    print_dbg(YELLOW, "\n\r==== Start send ====\n");

    printf("\rsend data 1~%d\n", data_num);
    printf("\rdummy_cnt = %x\n", (AE250_SPI0->TRANSCTRL >> 9) & 0x3);
    printf("\rreceive CMD = %x\n", AE250_SPI0->CMD);

    fifo_status_dump(SPI_TX_STATUS);

    SPI_Dri->Send(data, data_num);
    wait_complete();

    for (int i = 0; i <= stat_record_cnt; i++) {
        if (i == stat_record_cnt) {
            print_dbg(YELLOW, "\n\r==== End Int ====\n");
        } else {
            print_dbg(YELLOW, "\n\r==== Fill data[%d] ====\n", i);
        }
        status_record_dump(&stat_record[i], SPI_TX_STATUS);
    }
    print_dbg(YELLOW, "\n\r==== After send finish ====\n");
    fifo_status_dump(SPI_TX_STATUS);

}

static void mst_rst_test(void)
{
#define RST_TRANSFER_SIZE           (16)
#define TXINT_CMD_ARG_NUM_MAX       (1)
    U32 arg_ary[TXINT_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    int tx_rx_sel = 0;
    char *arg;

    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data_in[RST_TRANSFER_SIZE] = {0};
    uint8_t data_out[RST_TRANSFER_SIZE] = {0};

    while ((arg = serial_next()) != NULL) {

        if (argc >= TXINT_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        if (arg_ary[0] > 1 || arg_ary[0] < 0) {
            print_dbg(RED, "\rerr:\n\r0 : TX \n\r1 : RX\n");
            return;
        } else {
            tx_rx_sel = arg_ary[0];
        }
    }

    print_dbg(YELLOW, "\n\r==== start master reset test ====\n\r");

    hal_gpio_init(HAL_GPIO_00, HAL_OUTPUT);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);

    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);

    // configure the SPI to master, 8-bit data length and bus speed to 1MHz
    SPI_Dri->Control(NDS_SPI_MODE_MASTER |
                     cpol_cpha_sel |
                     msb_lsb_sel     |
                     NDS_SPI_DATA_BITS(8), 1000000);

    if (tx_rx_sel == 0) {
        AE250_SPI0->CTRL = 0x2 << 16;
        print_dbg(YELLOW, "\n\r==== Origin TX Status ====\n");
        printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
               (AE250_SPI0->STATUS >> 22 & 0xC0));
        printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
        printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);
        printf("\rTXTHRES = %x\n", AE250_SPI0->CTRL >> 16 & 0xFF);

        for (int cnt = 0; cnt < RST_TRANSFER_SIZE; cnt ++) {
            AE250_SPI0->DATA = cnt + 1;
        }
        print_dbg(YELLOW, "\n\r==== After Fill TX FIFO Status ====\n");
        printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
               (AE250_SPI0->STATUS >> 22 & 0xC0));
        printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
        printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);
        printf("\rTXTHRES = %x\n", AE250_SPI0->CTRL >> 16 & 0xFF);

        AE250_SPI0->CTRL |= 0x4;

        print_dbg(YELLOW, "\n\r==== After Reset TX FIFO Status ====\n");
        printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
               (AE250_SPI0->STATUS >> 22 & 0xC0));
        printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
        printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);
        printf("\rTXTHRES = %x\n", AE250_SPI0->CTRL >> 16 & 0xFF);
    } else {
        // prepare write data
        for (int i = 0; i < RST_TRANSFER_SIZE; i++) {
            data_in[i] = 0;
        }

        print_dbg(YELLOW, "\n\r==== Origin RX Status ====\n");
        printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
               (AE250_SPI0->STATUS >> 18 & 0xC0));
        printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
        printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);
        printf("\rRXTHRES = %x\n", AE250_SPI0->CTRL >> 8 & 0xFF);

        // read data from slave
        SPI_Dri->Transfer(data_out, data_in, RST_TRANSFER_SIZE);
        wait_complete();

        print_dbg(YELLOW, "\n\r==== After Fill RX FIFO Status ====\n");
        printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
               (AE250_SPI0->STATUS >> 18 & 0xC0));
        printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
        printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);
        printf("\rRXTHRES = %x\n", AE250_SPI0->CTRL >> 8 & 0xFF);

        AE250_SPI0->CTRL |= 0x2;

        print_dbg(YELLOW, "\n\r==== After Reset RX FIFO Status ====\n");
        printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
               (AE250_SPI0->STATUS >> 18 & 0xC0));
        printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
        printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);
        printf("\rRXTHRES = %x\n", AE250_SPI0->CTRL >> 8 & 0xFF);
    }
}

void test_case_spi_main(void)
{
    *(uint32_t *)0x1000065c = 0;    // disable decoder
    AE250_SPI0->CTRL |= 0x7;    // reset spi fifo
//    *((volatile uint32_t *)(0x10000504)) = 0x0baa7fde; //250MHz COB3

    serial_cmd_add("rx_int", rx_int_test);
    serial_cmd_add("tx_int", tx_int_test);

    serial_cmd_add("dma_rw", dma_rw_test);

    serial_cmd_add("rst", mst_rst_test);


    serial_cmd_add("cp", cpol_cpha_select);
    serial_cmd_add("sb", msb_lsb_select);
    serial_cmd_add("tm", trans_mode_select);
    serial_cmd_add("dct", dummy_cnt_set);
    serial_cmd_add("rxth", rx_threshold_set);
    serial_cmd_add("txth", tx_threshold_set);
    serial_cmd_add("tim", timing_select);
    serial_cmd_add("ctr", control_select);

    serial_cmd_add("srst", sys_reset_cmd);

}

#elif (SPI_TEST_SLAVE_MODE == 1)
static void dma_rw_test(void)
{
#define DMA_RW_SIZE             (12)
#define DMA_Log                 (1)
#define DMA_CMD_ARG_NUM_MAX     (2)
    U32 arg_ary[DMA_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    bool data_only = 0;
    int data_num = DMA_RW_SIZE;
    char *arg;

    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data[DMA_RW_SIZE] = {0};

    while ((arg = serial_next()) != NULL) {

        if (argc >= DMA_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        data_only = arg_ary[0];
        if (arg_ary[1] > DMA_RW_SIZE) {
            print_dbg(RED, "\rerr: data number too many\n");
            return;
        } else {
            data_num = arg_ary[1];
        }
    }

    print_dbg(YELLOW, "\n\r==== start slave DMA read/write test ====\n\r");

    printf("\rdata address = %x\n", (unsigned int)data);
    printf("\rdata length = %d\n", (unsigned int)data_num);

    hal_gpio_init(HAL_GPIO_00, HAL_OUTPUT);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);

    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);

    while (1) {
        if (data_only) {
            printf("\rset data only mode\n");
            // configure the SPI to slave, 8-bit data length
            SPI_Dri->Control(
                NDS_SPI_SLV_DATA_ONLY_TRANSFER |
                NDS_SPI_MODE_SLAVE  |
                cpol_cpha_sel |
                msb_lsb_sel     |
                NDS_SPI_DATA_BITS(8), 0);

        } else {
            printf("\rset command mode\n");
            // configure the SPI to slave, 8-bit data length
            SPI_Dri->Control(
                NDS_SPI_MODE_SLAVE  |
                cpol_cpha_sel |
                msb_lsb_sel     |
                NDS_SPI_DATA_BITS(8), 0);
        }

        // read data from master
        memset(data, 0, DMA_RW_SIZE);
        SPI_Dri->Receive(data, data_num);

#if DMA_Log
        printf("\rstart wait\n");
        wait_complete();
        printf("\rend wait\n");

        printf("\rreceive CMD = %x\n", AE250_SPI0->CMD);

        for (int cnt = 0; cnt < data_num; cnt ++) {
            printf("\rdata[%d] = %d\n", cnt, data[cnt]);
        }

        printf("\rsend data 1~%d\n", data_num);
        for (int cnt = 0; cnt < data_num; cnt ++) {
            data[cnt] = cnt + 1;

        }

#else
        wait_complete();
        for (int cnt = 0; cnt < DMA_TRANSFER_SIZE; cnt ++) {
            data[cnt] = cnt+1;
        }
#endif

        SPI_Dri->Send(data, data_num);
        wait_complete();

    }
}

static void transfer_test(void)
{
#define DMA_TRANSFER_SIZE       (12)
#define DMA_CMD_ARG_NUM_MAX     (2)
    U32 arg_ary[DMA_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    bool data_only = 0;
    int data_num = DMA_TRANSFER_SIZE;
    char *arg;

    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data_in[DMA_TRANSFER_SIZE] = {0};
    uint8_t data_out[DMA_TRANSFER_SIZE] = {0};

    while ((arg = serial_next()) != NULL) {

        if (argc >= DMA_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        data_only = arg_ary[0];
        if (arg_ary[1] > DMA_TRANSFER_SIZE) {
            print_dbg(RED, "\rerr: data number too many\n");
            return;
        } else {
            data_num = arg_ary[1];
        }
    }

    print_dbg(YELLOW, "\n\r==== start slave DMA read/write test ====\n\r");

    printf("\rdata_in address = %x\n", (unsigned int)data_in);
    printf("\rdata_out address = %x\n", (unsigned int)data_out);
    printf("\rdata length = %d\n", (unsigned int)data_num);

    hal_gpio_init(HAL_GPIO_00, HAL_OUTPUT);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);

    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);

    if (data_only) {
        printf("\rset data only mode\n");
        // configure the SPI to slave, 8-bit data length
        SPI_Dri->Control(
            NDS_SPI_SLV_DATA_ONLY_TRANSFER |
            NDS_SPI_MODE_SLAVE  |
            cpol_cpha_sel |
            msb_lsb_sel     |
            NDS_SPI_DATA_BITS(8), 0);

    } else {
        printf("\rset command mode\n");
        // configure the SPI to slave, 8-bit data length
        SPI_Dri->Control(
            NDS_SPI_MODE_SLAVE  |
            cpol_cpha_sel |
            msb_lsb_sel     |
            NDS_SPI_DATA_BITS(8), 0);
    }

    while (1) {
        stat_record_cnt = 0;
        // read data from master
        memset(data_in, 0, DMA_TRANSFER_SIZE);
        memset(data_out, 0, DMA_TRANSFER_SIZE);

        printf("\rsend data 1~%d\n", data_num);
        for (int cnt = 0; cnt < data_num; cnt ++) {
            data_in[cnt] = cnt + 1;

        }
        print_dbg(YELLOW, "\n\r==== Start transfer ====\n");

        printf("\rdummy_cnt = %x\n", (AE250_SPI0->TRANSCTRL >> 9) & 0x3);

        printf("\rreceive CMD = %x\n", AE250_SPI0->CMD);

        fifo_status_dump(SPI_RX_TX_STATUS);

        SPI_Dri->Transfer(data_out, data_in, data_num);
        wait_complete();

        printf("\rreceive CMD = %x\n", AE250_SPI0->CMD);

        for (int cnt = 0; cnt < data_num; cnt ++) {
            printf("\rreceive data[%d] = %d\n", cnt, data_out[cnt]);
        }
        for (int i = 0; i < stat_record_cnt; i++) {
            if (stat_record_cnt == i + 1) {
                print_dbg(YELLOW, "\n\r==== Irq %d times is the end int ====\n", i);
            } else {
                print_dbg(YELLOW, "\n\r==== Irq %d times ====\n", i);
            }
            status_record_dump(&stat_record[i], SPI_RX_TX_STATUS);
        }

        print_dbg(YELLOW, "\n\r==== After transfer finish ====\n");

        fifo_status_dump(SPI_RX_TX_STATUS);
    }
}

static void rx_int_test(void)
{
#define RXINT_TRANSFER_SIZE         (20)
#define RXINT_CMD_ARG_NUM_MAX       (1)
    U32 arg_ary[DMA_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    int data_num = RXINT_TRANSFER_SIZE;
    char *arg;

    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data[RXINT_TRANSFER_SIZE] = {0};

    while ((arg = serial_next()) != NULL) {

        if (argc >= RXINT_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        if (arg_ary[0] > RXINT_TRANSFER_SIZE) {
            print_dbg(RED, "\rerr: data number too many\n");
            return;
        } else {
            data_num = arg_ary[0];
        }
    }

    print_dbg(YELLOW, "\n\r==== start slave RX int test ====\n\r");

    hal_gpio_init(HAL_GPIO_00, HAL_OUTPUT);

    printf("\rdata address = %x\n", (unsigned int)data);
    printf("\rdata length = %d\n", (unsigned int)data_num);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);

    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);

    // configure the SPI to slave, 8-bit data length
    SPI_Dri->Control(/*NDS_SPI_SLV_DATA_ONLY_TRANSFER | */
        NDS_SPI_MODE_SLAVE  |
        NDS_SPI_CPOL0_CPHA0 |
        NDS_SPI_MSB_LSB     |
        NDS_SPI_DATA_BITS(8), 0);

    while (1) {
        // read data from master
        stat_record_cnt = 0;

        memset(data, 0, data_num);
        SPI_Dri->Receive(data, data_num);

        print_dbg(YELLOW, "\n\r==== Start receive ====\n");

        printf("\rdummy_cnt = %x\n", (AE250_SPI0->TRANSCTRL >> 9) & 0x3);

        printf("\rreceive CMD = %x\n", AE250_SPI0->CMD);

        fifo_status_dump(SPI_RX_STATUS);

        printf("\rstart wait\n");
        wait_complete();
        printf("\rend wait\n");

        for (int i = 0; i < stat_record_cnt; i++) {
            if (stat_record_cnt == i + 1) {
                print_dbg(YELLOW, "\n\r==== Irq %d times is the end int ====\n", i);
            } else {
                print_dbg(YELLOW, "\n\r==== Irq %d times ====\n", i);
            }
            status_record_dump(&stat_record[i], SPI_RX_STATUS);
        }

        printf("\n\rreceive data length : %d\n", data_num);
        for (int cnt = 0; cnt < data_num; cnt ++) {
            printf("\rdata[%d] = %d\n", cnt, data[cnt]);
        }

        printf("\rreceive CMD = %x\n", AE250_SPI0->CMD);

        print_dbg(YELLOW, "\n\r==== After receive finish ====\n");

        fifo_status_dump(SPI_RX_STATUS);

        printf("\rsend receive data\n");
        SPI_Dri->Send(data, data_num);
        wait_complete();

    }
}

static void tx_int_test(void)
{
#define TXINT_TRANSFER_SIZE       (20)
#define TXINT_CMD_ARG_NUM_MAX       (1)
    U32 arg_ary[TXINT_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    int data_num = TXINT_TRANSFER_SIZE;
    char *arg;

    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data[TXINT_TRANSFER_SIZE] = {0};

    while ((arg = serial_next()) != NULL) {

        if (argc >= TXINT_CMD_ARG_NUM_MAX) {
            print_dbg(RED, "err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 10);
    }

    if (argc > 0) {
        if (arg_ary[0] > TXINT_TRANSFER_SIZE) {
            print_dbg(RED, "\rerr: data number too many\n");
            return;
        } else {
            data_num = arg_ary[0];
        }
    }

    print_dbg(YELLOW, "\n\r==== start slave TX int test ====\n");

    hal_gpio_init(HAL_GPIO_00, HAL_OUTPUT);

    printf("\rdata address = %x\n", (unsigned int)data);
    printf("\rdata length = %d\n", (unsigned int)data_num);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);

    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);

    // configure the SPI to slave, 8-bit data length
    SPI_Dri->Control(/*NDS_SPI_SLV_DATA_ONLY_TRANSFER | */
        NDS_SPI_MODE_SLAVE  |
        NDS_SPI_CPOL0_CPHA0 |
        NDS_SPI_MSB_LSB     |
        NDS_SPI_DATA_BITS(8), 0);

    while (1) {
        stat_record_cnt = 0;

        // read data from master
        memset(data, 0, data_num);
        SPI_Dri->Receive(data, data_num);
        wait_complete();

        printf("\n\rreceive data length : %d\n", data_num);
        for (int cnt = 0; cnt < data_num; cnt ++) {
            printf("\rdata[%d] = %d\n", cnt, data[cnt]);
        }

        printf("\rreceive CMD = %x\n", AE250_SPI0->CMD);

        print_dbg(YELLOW, "\n\r==== Start send ====\n");

        printf("\rsend data 1~%d\n", data_num);
        printf("\rdummy_cnt = %x\n", (AE250_SPI0->TRANSCTRL >> 9) & 0x3);
        printf("\rreceive CMD = %x\n", AE250_SPI0->CMD);

        fifo_status_dump(SPI_TX_STATUS);

        SPI_Dri->Send(data, data_num);
        wait_complete();

        for (int i = 0; i <= stat_record_cnt; i++) {
            if (i == stat_record_cnt) {
                print_dbg(YELLOW, "\n\r==== End Int ====\n");
            } else {
                print_dbg(YELLOW, "\n\r==== Fill data[%d] ====\n", i);
            }
            status_record_dump(&stat_record[i], SPI_TX_STATUS);
        }
        print_dbg(YELLOW, "\n\r==== After send finish ====\n");
        fifo_status_dump(SPI_TX_STATUS);

    }
}

static void slv_cmd_test(void)
{
#define CMD_TRANSFER_SIZE       (10)
    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data[CMD_TRANSFER_SIZE] = {0};

    print_dbg(YELLOW, "\n\r==== start slave CMD test ====\n\r");

    printf("\rdata address = %x\n", (unsigned int)data);

    hal_gpio_init(HAL_GPIO_00, HAL_OUTPUT);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);

    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);

    // configure the SPI to slave, 8-bit data length
    SPI_Dri->Control(/*NDS_SPI_SLV_DATA_ONLY_TRANSFER | */
        NDS_SPI_MODE_SLAVE  |
        NDS_SPI_CPOL0_CPHA0 |
        NDS_SPI_MSB_LSB     |
        NDS_SPI_DATA_BITS(8), 0);

    AE250_SPI0->INTREN = SPI_SLVCMD;
    while (1) {
        if (receive_cmd) {
            print_dbg(YELLOW, "\n\rreceive CMD = %x\n", AE250_SPI0->CMD);

            switch (receive_cmd) {
                case SPI_READ_STATUS:
                    printf("\rUnderRun = %x\n", AE250_SPI0->SLVST >> 18 & 0x1);
                    printf("\rOverRun = %x\n", AE250_SPI0->SLVST >> 17 & 0x1);
                    printf("\rReady = %x\n", AE250_SPI0->SLVST >> 16 & 0x1);
                    printf("\rUSR_Status = %x\n", AE250_SPI0->SLVST & 0xFFFF);
                    if (AE250_SPI0->SLVST >> 16 & 0x1) {
                        AE250_SPI0->SLVST &= ~(1 << 16);
                    } else {
                        AE250_SPI0->SLVST |= 1 << 16;
                    }

                    if ((AE250_SPI0->SLVST & 0xFFFF) == 0) {
                        AE250_SPI0->SLVST |= 0xffff;
                    } else {
                        AE250_SPI0->SLVST &= ~0xFFFF;
                    }
                    break;
                case SPI_READ_SINGLE:
                    for (int cnt = 0; cnt < CMD_TRANSFER_SIZE; cnt ++) {
                        data[cnt] = cnt + 1;
                        AE250_SPI0->DATA = data[cnt];
                    }

                    break;
                case SPI_WRITE_SINGLE:
                    printf("\rstart receive data\n");

                    while ((AE250_SPI0->STATUS >> 8 & 0x3F) == 0);
                    int rxnum = (AE250_SPI0->STATUS >> 8 & 0x3F) | \
                                (AE250_SPI0->STATUS >> 18 & 0xC0);

                    for (int i = 0; i < rxnum; i++) {
                        printf("\rdata[%d] = %x\n", i, AE250_SPI0->DATA);
                    }

                    break;
                default:
                    break;
            }
            receive_cmd = 0;
        }
        serial_cmd_proc();
    }
}

static void slv_rst_test(void)
{
#define RST_TRANSFER_SIZE       (16)
    NDS_DRIVER_SPI *SPI_Dri = &Driver_SPI0;
    uint8_t data[RST_TRANSFER_SIZE] = {0};

    print_dbg(YELLOW, "\n\r==== start slave reset test ====\n\r");

    printf("\rdata address = %x\n", (unsigned int)data);

    hal_gpio_init(HAL_GPIO_00, HAL_OUTPUT);

    // initialize SPI
    SPI_Dri->Initialize(spi_callback);

    // power up the SPI peripheral
    SPI_Dri->PowerControl(NDS_POWER_FULL);

    // configure the SPI to slave, 8-bit data length
    SPI_Dri->Control(/*NDS_SPI_SLV_DATA_ONLY_TRANSFER | */
        NDS_SPI_MODE_SLAVE  |
        NDS_SPI_CPOL0_CPHA0 |
        NDS_SPI_MSB_LSB     |
        NDS_SPI_DATA_BITS(8), 0);

    AE250_SPI0->INTREN = SPI_SLVCMD;
    while (1) {
        AE250_SPI0->CTRL = 0x2 << 16;
        print_dbg(YELLOW, "\n\r==== Origin TX Status ====\n");
        printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
               (AE250_SPI0->STATUS >> 22 & 0xC0));
        printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
        printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);
        printf("\rTXTHRES = %x\n", AE250_SPI0->CTRL >> 16 & 0xFF);

        for (int cnt = 0; cnt < RST_TRANSFER_SIZE; cnt ++) {
            AE250_SPI0->DATA = cnt + 1;
        }
        print_dbg(YELLOW, "\n\r==== After Fill TX FIFO Status ====\n");
        printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
               (AE250_SPI0->STATUS >> 22 & 0xC0));
        printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
        printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);
        printf("\rTXTHRES = %x\n", AE250_SPI0->CTRL >> 16 & 0xFF);

        AE250_SPI0->CTRL |= 0x4;

        print_dbg(YELLOW, "\n\r==== After Reset TX FIFO Status ====\n");
        printf("\rSTATUS TXNUM = %x\n", (AE250_SPI0->STATUS >> 16 & 0x3F) |
               (AE250_SPI0->STATUS >> 22 & 0xC0));
        printf("\rSTATUS TXEMPTY = %x\n", AE250_SPI0->STATUS >> 22 & 0x1);
        printf("\rSTATUS TXFULL = %x\n", AE250_SPI0->STATUS >> 23 & 0x1);
        printf("\rTXTHRES = %x\n", AE250_SPI0->CTRL >> 16 & 0xFF);


        AE250_SPI0->CTRL = 0x2 << 8;
        print_dbg(YELLOW, "\n\r==== Origin RX Status ====\n");
        printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
               (AE250_SPI0->STATUS >> 18 & 0xC0));
        printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
        printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);
        printf("\rRXTHRES = %x\n", AE250_SPI0->CTRL >> 8 & 0xFF);

        while (receive_cmd != SPI_WRITE_SINGLE);
        printf("\rreceive CMD = %x\n", receive_cmd);
        print_dbg(YELLOW, "\n\r==== After Fill RX FIFO Status ====\n");
        printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
               (AE250_SPI0->STATUS >> 18 & 0xC0));
        printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
        printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);
        printf("\rRXTHRES = %x\n", AE250_SPI0->CTRL >> 8 & 0xFF);

        AE250_SPI0->CTRL |= 0x2;

        print_dbg(YELLOW, "\n\r==== After Reset RX FIFO Status ====\n");
        printf("\rSTATUS RXNUM = %x\n", (AE250_SPI0->STATUS >> 8 & 0x3F) |
               (AE250_SPI0->STATUS >> 18 & 0xC0));
        printf("\rSTATUS RXEMPTY = %x\n", AE250_SPI0->STATUS >> 14 & 0x1);
        printf("\rSTATUS RXFULL = %x\n", AE250_SPI0->STATUS >> 15 & 0x1);
        printf("\rRXTHRES = %x\n", AE250_SPI0->CTRL >> 8 & 0xFF);

        receive_cmd = 0;

    }
}

void test_case_spi_main(void)
{
    *(uint32_t *)0x1000065c = 0;    // disable decoder
    AE250_SPI0->CTRL |= 0x7;    // reset spi fifo

    serial_cmd_add("rx_int", rx_int_test);
    serial_cmd_add("tx_int", tx_int_test);

    serial_cmd_add("dma_rw", dma_rw_test);
    serial_cmd_add("trs", transfer_test);

    serial_cmd_add("scmd", slv_cmd_test);
    serial_cmd_add("rst", slv_rst_test);


    serial_cmd_add("cp", cpol_cpha_select);
    serial_cmd_add("sb", msb_lsb_select);
    serial_cmd_add("tm", trans_mode_select);
    serial_cmd_add("dct", dummy_cnt_set);
    serial_cmd_add("rxth", rx_threshold_set);
    serial_cmd_add("txth", tx_threshold_set);
    serial_cmd_add("srst", sys_reset_cmd);

}
#endif
#endif
