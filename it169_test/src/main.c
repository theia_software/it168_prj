/*
 * Copyright 2018 @EGISTEC. All rights reserved.
 * File: main.c
 */

#include <proj_config.h>
#include <sys.h>
#include <serial.h>

#include <Driver_USART.h>
#include <stdio.h>
#include <string.h>
#include <Driver_WDT.h>
#include <core_v5.h>
#include <gpio_ae250.h>
#include <usart_ae250.h>

#if (CONFIG_PROJ_SEL == CONFIG_PROJ_TEST_CASE)
#include <test_case_main.h>
#endif

/* USART Driver */
extern NDS_DRIVER_USART Driver_USART0;

volatile char usart_event_complete;

void wait_usart_complete(void)
{
    while (!usart_event_complete);
    usart_event_complete = 0;
}

void user_usart_callback(uint32_t event)
{
    switch (event) {
    case NDS_USART_EVENT_RECEIVE_COMPLETE:
    case NDS_USART_EVENT_TRANSFER_COMPLETE:
    case NDS_USART_EVENT_SEND_COMPLETE:
    case NDS_USART_EVENT_TX_COMPLETE:
        /* Success: continue task */
        usart_event_complete = 1;
        break;

    case NDS_USART_EVENT_RX_TIMEOUT:
        while(1);
        break;

    case NDS_USART_EVENT_RX_OVERFLOW:
    case NDS_USART_EVENT_TX_UNDERFLOW:
        while(1);
        break;
    }
}

int loader_main(void)
{
//    *(uint32_t *)(0x10000504) = 0x0baf7fde;
//    *((volatile uint32_t *)(0x10000504)) = 0x0baa7fde; //250MHz COB3
    static NDS_DRIVER_USART *uart_drv = &Driver_USART0;

    /* Initialize the USART driver */
    uart_drv->Initialize(user_usart_callback);
    /* Power up the USART peripheral */
    uart_drv->PowerControl(NDS_POWER_FULL);
    /* Configure the USART to 115200 Bits/sec */
    uart_drv->Control(NDS_USART_MODE_ASYNCHRONOUS |
                      NDS_USART_DATA_BITS_8 |
                      NDS_USART_PARITY_NONE |
                      NDS_USART_STOP_BITS_1 |
                      NDS_USART_FLOW_CONTROL_NONE, 115200);

    /* Enable Receiver and Transmitter lines */
    uart_drv->Control(NDS_USART_CONTROL_TX, 1);
    uart_drv->Control(NDS_USART_CONTROL_RX, 1);

    printf("Boot form ROM\n");
    serial_cmd_init();

#if (CONFIG_PROJ_SEL == CONFIG_PROJ_TEST_CASE)
    test_case_main();
#endif

    while (1)
        serial_cmd_proc();

    return 0;
}


int main(void)
{
#if 0
    sys_init();
#endif

    loader_main();

    while (1);

    return 0;
}

