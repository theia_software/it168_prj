/*
 * Copyright 2018 @EGISTEC. All rights reserved.
 * File: sys.c
 */

#include <sys.h>


void sys_init(void)
{
    RW_SYS_REG(SYS_HW_INT_TRIG_TYPE) = HW_INT_ALL_TRIG_TYPE_EDGE;
    RW_SYS_REG(SYS_SW_INT_TRIG_TYPE) = SW_INT_TRIG_TYPE_EDGE;

    RW_SYS_REG(SYS_TCON_CLK_EN) = SYS_TCON_0_CLK_EN | SYS_TCON_1_CLK_EN | SYS_TCON_2_CLK_EN;
    RW_SYS_REG(SYS_SENSOR_CLK_EN) = SYS_SENSOR_00_CLK_EN | SYS_SENSOR_01_CLK_EN | \
                                    SYS_SENSOR_02_CLK_EN | SYS_SENSOR_03_CLK_EN | \
                                    SYS_SENSOR_04_CLK_EN | SYS_SENSOR_05_CLK_EN | \
                                    SYS_SENSOR_06_CLK_EN | SYS_SENSOR_07_CLK_EN;
    RW_SYS_REG(SYS_CLK_EN) |= SYS_DSP_CLK_EN;

    /* reset modules */
    RW_SYS_REG(SYS_RESET_00) |= (SET_RESET_TCON_EN | SET_RESET_SENSOR_EN);
    RW_SYS_REG(SYS_RESET_DSP) |= (SET_RESET_DSP_EN);

    /* release reset modules */
    RW_SYS_REG(SYS_RESET_00) &= (CLR_RESET_TCON_DIS & CLR_RESET_SENSOR_DIS);
    RW_SYS_REG(SYS_RESET_DSP) &= (CLR_RESET_DSP_DIS);

    RW_SYS_REG(SYS_RESET_02_WC) = SET_RESET_BUS;

    /* initial GPIO output MUX select */
    RW_SYS_REG(SYS_PAD_GPIO_IN_SEL) = (SYS_PAD_GPIO_SEL_MASK | SYS_GPIO_OUT_SEL_MASK);

    /* set system interrupt source */
    RW_SYS_REG(SYS_INT_SRC_SEL) = (SYS_INT_SRC_25_SEL | SYS_INT_SRC_27_SEL | SYS_INT_SRC_29_SEL);

    /* set external pin interrupt to H active */
    RW_SYS_REG(SYS_EXT_INTR_LVL_TRIG_SEL) = SET_SYS_GPIO_00_INTR_LVL_TRIG_SEL_H | \
                                            SET_SYS_GPIO_00_INTR_LVL_TRIG_SEL_H | \
                                            SET_SYS_GPIO_01_INTR_LVL_TRIG_SEL_H | \
                                            SET_SYS_GPIO_02_INTR_LVL_TRIG_SEL_H | \
                                            SET_SYS_GPIO_03_INTR_LVL_TRIG_SEL_H | \
                                            SET_SYS_GPIO_04_INTR_LVL_TRIG_SEL_H | \
                                            SET_SYS_GPIO_05_INTR_LVL_TRIG_SEL_H | \
                                            SET_SYS_GPIO_06_INTR_LVL_TRIG_SEL_H | \
                                            SET_SYS_GPIO_07_INTR_LVL_TRIG_SEL_H | \
                                            SET_SYS_INTR_IN_INTR_LVL_TRIG_SEL_H | \
                                            SET_SYS_HSYNC_INTR_LVL_TRIG_SEL_H | \
                                            SET_SYS_VSYNC_INTR_LVL_TRIG_SEL_H;
}

