/*
 * Copyright 2018 @EGISTEC. All rights reserved.
 * File: lib.c
 */

#include <common.h>
#include <serial.h>

void delay_cycle(U32 cycle)
{
    volatile U32 idx;

    for (idx = 0; idx < cycle; ++idx)
        NOP();
}

int rand(void)
{
    static unsigned int nds_lfsr_seed = SEED;

    nds_lfsr_seed = (nds_lfsr_seed >> 1) ^ (-(nds_lfsr_seed & 1u) & 0xd0000001u);

    return (int)nds_lfsr_seed;
}

void hexdump(U32 addr, U32 len)
{
    U32 idx, one_round;
    U32 end_addr;


    end_addr = addr + len;

    for (idx = addr; idx < end_addr;) {
        printf("0x%08x:", idx);
        one_round = (idx + 16);

        for (; (idx < one_round) && (idx < end_addr); ++idx) {
            printf(" %02x", *((U8 *)idx));
        }
        printf("\n");
    }
    printf("\n");
}

void hexdump_align(U32 addr, U32 len)
{
    U32 *data;
    U32 one_round;
    U32 end_addr;


    addr = SET_INC_ALIGN_4(addr);
    end_addr = addr + len;

    for (data = (U32 *)addr; (U32)data < end_addr;) {
        printf("0x%08x:", (U32)data);
        one_round = (U32)(data + 4);

        for (; ((U32)data < one_round) && ((U32)data < end_addr); ++data) {
            printf(" %08x", *data);
        }
        printf("\n");
    }
    printf("\n");
}

