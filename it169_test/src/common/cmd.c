/*
 * Copyright 2018 @EGISTEC. All rights reserved.
 * File: cmd.c
 */

#include <proj_config.h>
#include <serial.h>
#include <sys.h>
#include <spi_slave.h>


static void set_sram_cmd(void)
{
#define SET_SRAM_CMD_ARG_NUM_MAX (2)
    U32 arg_ary[SET_SRAM_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    char *arg;


    while ((arg = serial_next()) != NULL) {

        if (argc >= SET_SRAM_CMD_ARG_NUM_MAX) {
            printf("err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 16);
    }

    if (argc != SET_SRAM_CMD_ARG_NUM_MAX) {
        printf("err: too few arguments");
        return;
    }

    *(volatile U32 *)(arg_ary[0]) = arg_ary[1];
}

static void dump_sram_cmd(void)
{
#define DUMP_SRAM_CMD_ARG_NUM_MAX (2)
    U32 arg_ary[DUMP_SRAM_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    const char cursor_up[] = "\033[1A";
    char *arg;


    while ((arg = serial_next()) != NULL) {

        if (argc >= DUMP_SRAM_CMD_ARG_NUM_MAX) {
            printf("err: too many arguments");
            return;
        }

        arg_ary[argc++] = strtoul(arg, NULL, 16);
    }

    if (argc < (DUMP_SRAM_CMD_ARG_NUM_MAX - 1)) {
        printf("err: too few arguments");
        return;
    }

    if (argc == 1)
        arg_ary[1] = 4;

    if (!(TEST_ALIGN_4(arg_ary[0])) && (arg_ary[1] == 4)) {
        printf("0x%08x", *(volatile U32 *)(arg_ary[0]));

    } else {
        ((TEST_ALIGN_4(arg_ary[0])) ? \
         hexdump(arg_ary[0], arg_ary[1]) : hexdump_align(arg_ary[0], arg_ary[1]));
        printf("%s", cursor_up);
    }
}

static void sys_info_cmd(void)
{
    printf("===============================\n" \
           " system info\n" \
           "===============================\n");

    printf("SYS_GEN_DATE         0x%08x\n", RW_SYS_REG(SYS_GEN_DATE));
    printf("SYS_GEN_VERSION      0x%08x\n", RW_SYS_REG(SYS_GEN_VERSION));

    printf("SPI_SLAVE_DATE       0x%08x\n", RW_SPIS_REG(SPI_SLAVE_DATE));
    printf("SPI_SLAVE_VERSION    0x%08x\n", RW_SPIS_REG(SPI_SLAVE_VERSION));

    printf("-------------------------------");
}

static void xmodem_rx_cmd(void)
{
#define XMODEM_RX_CMD_ARG_NUM_MAX   (1)

    extern int32_t xmodem_receive(unsigned char *dest, uint32_t destsz);
    U32 arg_ary[XMODEM_RX_CMD_ARG_NUM_MAX];
    U32 argc = 0;
    U32 addr;
    S32 rc;
    char *arg;


    while ((arg = serial_next()) != NULL) {

        if (argc >= XMODEM_RX_CMD_ARG_NUM_MAX) {
            printf("err: too many arguments");
            return;
        }

        arg_ary[argc] = strtoul(arg, NULL, 16);
        argc += 1;
    }

    addr = (argc) ? arg_ary[0] : 0;

    rc = xmodem_receive((unsigned char *)addr, ~0);

    if (rc < 0) {
        printf("\nerr: xmodem fail %d", rc);
        return;
    }

    printf("\nxmodem receive\n"
           "addr: 0x%08x\n"
           "size: %d(0x%08x)", addr, rc, rc);
}


void common_cmd_init(void)
{
    serial_cmd_add("sinfo", sys_info_cmd);
    serial_cmd_add("ss", set_sram_cmd);
    serial_cmd_add("ds", dump_sram_cmd);
    serial_cmd_add("xr", xmodem_rx_cmd);
}

