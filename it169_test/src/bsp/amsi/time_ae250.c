/*
 * Copyright (c) 2012-2018 Andes Technology Corporation
 * All rights reserved.
 *
 */

#include "timer.h"
#include "time_ae250.h"
#include "usart_ae250.h"
#include "RTE_Device.h"
#include <stdio.h>
#include <string.h>

uint32_t g_ticks = 0;
static char time_list = 0;
static TIME_INFO time1_info = {0};
static DELAY_INFO delay_info = {0};

extern NDS_DRIVER_USART Driver_USART1;

volatile char daley_complete = 0;
static bool MTime_init = 0;
static int time_scale = 1; // ms
static const TIME_RESOURCES it168_time = {
	1,
	&time1_info,
	&delay_info,
};

void trigger_mswi(void)
{
	/* Set PLIC_SW source 1 to trigger Machine software interrupt */
	__nds__plic_sw_set_pending(1);
}

void setup_mswi(void)
{
	/* Machine SWI is connected to PLIC_SW source 1 */
	__nds__plic_sw_set_priority(1, 1);
	__nds__plic_sw_enable_interrupt(1);
	set_csr(NDS_MIE, MIP_MSIP);
}

void mswi_handler(void)
{
	uint32_t swid = 0;
	swid = __nds__plic_sw_claim_interrupt();
	/*
	 * It is triggered by Machine mtime interrupt handler.
	 * Output messgaes to indicate Machine mtime is alive.
	 */
	if (it168_time.timer1->status) {
		if (!it168_time.timer1->oneshot) {
			it168_time.timer1->target_time = g_ticks + it168_time.timer1->timeout_time;
			//setup_mswi();
		} else {
			it168_time.timer1->status = 0;
		}

		it168_time.timer1->cb_event(1);
	}

	__nds__plic_sw_complete_interrupt(1);
}

static void int_mtimer(uint32_t ms)
{
	HAL_MTIMER_INITIAL();
	/* Active machine timer */
	//DEV_PLMT->MTIMECMP = DEV_PLMT->MTIME + (40 * KHz);
	DEV_PLMT->MTIMECMP = DEV_PLMT->MTIME + msec_to_tick(ms); // setup system tick interval
	HAL_MTIME_ENABLE();
	MTime_init = 1;
}

void mtime_handler(void)
{
	HAL_MTIME_DISABLE();
	g_ticks = g_ticks + 1;
	int_mtimer(time_scale); // set Mtimer time out as 1 ms

	if ((it168_time.timer1->target_time == g_ticks)) {
		trigger_mswi();
	}
	if (it168_time.delay->target_time == g_ticks)
		daley_complete = 1;

}

void sw_delay_ms(uint32_t delay_ms){
	if (!MTime_init)
		it168_timer_init();

	daley_complete = 0;
	it168_time.delay->status = 1;
	it168_time.delay->target_time = g_ticks + delay_ms;
	while(!daley_complete);
}

static int sw_start_timer_ms(uint32_t  timeout_time_ms, NDS_TIME_SignalEvent_t  callback, bool noloop )
{
	if (!MTime_init)
		it168_timer_init();

	if (timeout_time_ms > 1) {
		it168_time.timer1->cb_event = callback;
		it168_time.timer1->timeout_time = timeout_time_ms;
		it168_time.timer1->target_time = g_ticks + timeout_time_ms;
		it168_time.timer1->status = 1;
		it168_time.timer1->oneshot = noloop;
		setup_mswi();
		return 0;
	} else {
		return -1;
	}
}

static void sw_stop_timer_ms(void)
{
	it168_time.timer1->status = 0;
	it168_time.timer1->target_time = 0;
}

static uint32_t sw_get_time(void)
{
	return g_ticks;
}

static int sw_get_remaining_time_ms(uint32_t* remaing_time)
{
	if (!MTime_init)
		it168_timer_init();

	if (it168_time.timer1->status){
		*remaing_time = it168_time.timer1->target_time - g_ticks;
		if (*remaing_time > 0)
			return 0;
		else
			return -1;
	}
	return 1;
}

void it168_timer_init(void)
{
	int_mtimer(time_scale);
}

NDS_DRIVER_TIME Driver_TIME = {
	it168_timer_init,
	sw_get_time,
	sw_start_timer_ms,
	sw_get_remaining_time_ms,
	sw_stop_timer_ms,
	sw_delay_ms,
};

