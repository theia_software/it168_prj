/*
 * Copyright 2018 @EGISTEC. All rights reserved.
 * File: iochar.c
 */
#include "Driver_USART.h"
#include "platform.h"


extern NDS_DRIVER_USART Driver_USART0;
extern void wait_usart_complete(void);


int out_byte(int c)
{
    Driver_USART0.Send(&c, 1);
    wait_usart_complete();
    return c;
}

int outbyte(int c)
{
    if (c == '\n') {
        Driver_USART0.Send("\r", 1);
        wait_usart_complete();
    }
    Driver_USART0.Send(&c, 1);
    wait_usart_complete();
    return c;
}

int inbyte(void)
{
    if (Driver_USART0.RxAvaiable()) {
        char c;

        Driver_USART0.Receive(&c, 1);
        wait_usart_complete();
        return c;
    }
    return -1;
}

