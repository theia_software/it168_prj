/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: test_case_common.h
 */

#ifndef _TEST_CASE_COMMON_H_
#define _TEST_CASE_COMMON_H_
#include <proj_config.h>
#include <test_case_config.h>

#if (CONFIG_PROJ_SEL == CONFIG_PROJ_TEST_CASE) && \
    (CONFIG_TEST_CASE_SEL == TEST_CASE_NORMAL)
void tc_tcon_setting(void);
void tc_gen_waveform(void);
void power_5off(void);

#endif

#endif
