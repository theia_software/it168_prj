/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: test_case_dsp.h
 */

#ifndef _TEST_CASE_GPIO_H_
#define _TEST_CASE_GPIO_H_
#include <proj_config.h>
#include <test_case_config.h>

#if (CONFIG_PROJ_SEL == CONFIG_PROJ_TEST_CASE) && \
    (CONFIG_TEST_CASE_SEL == TEST_CASE_OTP)

#define YELLOW "\033[1;33m"
#define RED "\033[0;32;31m"
#define RESET "\033[0m"

#define print_dbg(color, str, args...)      printf(color str RESET, ##args)

#define OTP_ADR_MIN                         (0)
#define OTP_ADR_MAX                         (0x7F)

#define OTP_AUTO_BURN_SIZE                  (0x7)   //total size 8 * 4bytes
#define OTP_AUTO_BURN_MAX                   (OTP_ADR_MAX - OTP_AUTO_BURN_SIZE)

#define OTP_RELOAD_SIZE                     (0x8)   //total size 8 * 4bytes
#define OTP_RELOAD_MAX                      (OTP_ADR_MAX - OTP_RELOAD_SIZE)

#define OTP_BIT_MIN                         (0)
#define OTP_BIT_MAX                         (31)

/* DSP verification status flag */
enum otp_ver_status_flag {
    OTP_VER_RES_PASS            = 0,
    OTP_VER_RES_FAIL            = (1ul << 0),
    OTP_VER_FW_CHK_FAIL         = (1ul << 31),
};

typedef U32 (*test_case)(void);

void test_case_otp_main(void);

#endif
#endif
