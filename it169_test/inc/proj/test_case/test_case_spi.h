/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: test_case_dsp.h
 */

#ifndef _TEST_CASE_GPIO_H_
#define _TEST_CASE_GPIO_H_
#include <proj_config.h>
#include <test_case_config.h>

#if (CONFIG_PROJ_SEL == CONFIG_PROJ_TEST_CASE) && \
    (CONFIG_TEST_CASE_SEL == TEST_CASE_SPI)

#define SPI_TEST_MASTER_MODE                    (0)
#define SPI_TEST_SLAVE_MODE                     !(SPI_TEST_MASTER_MODE)

#define SPI0_VERIFACTION                        (1)

#define TEST_MASTER_FIFO_RESET                  (0)
#define TEST_MASTER_SCLK_DIV                    (0)
#define TEST_MASTER_CSHT                        (0)
#define TEST_MASTER_CS2SCLK                     (0)

/* SPI control register */
#define SPI_CONTROL_CMD_EN                      (0x1 << 30)
#define SPI_CONTROL_ADDR_EN                     (0x1 << 29)
#define SPI_CONTROL_ADDR_FMT_EN                 (0x1 << 28)
#define SPI_CONTROL_ADDR_DUAL_QUAL_EN(mode)     (mode << 22)
#define SPI_CONTROL_TOKEN_EN                    (0x1 << 21)
#define SPI_CONTROL_TOKEN_VALUE(val)            (val << 11)

#define SPI_CONTROL_ADDR_LEN(len)               (len << 16)

#define YELLOW "\033[1;33m"
#define RED "\033[0;32;31m"
#define RESET "\033[0m"

#define print_dbg(color, str, args...)      printf(color str RESET, ##args)

typedef struct record_spi_status {
    union {
        uint32_t  ctrl;                 /*<!-save SPI Control Register>*/
        struct {
            uint32_t  Reserved          :  8;  //0:7
            uint32_t  RXTHRES           :  8;  //8:15
            uint32_t  TXTHRES           :  8;  //16:23
            uint32_t  Reserved1         :  8;  //24:31
        } ctrl_b;
    };
    union {
        uint32_t  status;               /*<!-save SPI Status Register>*/
        struct {
            uint32_t  SPIActive         :  1;  //0
            uint32_t  Reserved          :  7;  //1:7
            uint32_t  RXNUM_L           :  6;  //8:13
            uint32_t  RXEMPTY           :  1;  //14
            uint32_t  RXFULL            :  1;  //15
            uint32_t  TXNUM_L           :  6;  //16:21
            uint32_t  TXEMPTY           :  1;  //22
            uint32_t  TXFULL            :  1;  //23
            uint32_t  RXNUM_H           :  2;  //24:25
            uint32_t  Reserved1         :  2;  //26:27
            uint32_t  TXNUM_H           :  2;  //28:29
            uint32_t  Reserved2         :  2;  //30:31
        } status_b;
    };
    union {
        uint32_t  irq;                  /*<!-save SPI Interrupt Status Register>*/
        struct {
            uint32_t  RXFIFOORInt       :  1;  //0
            uint32_t  TXFIFOURInt       :  1;  //1
            uint32_t  RXFIFOInt         :  1;  //2
            uint32_t  TXFIFOInt         :  1;  //3
            uint32_t  EndInt            :  1;  //4
            uint32_t  SlvCmdInt         :  1;  //5
            uint32_t  Reserved          :  26;  //6:31
        } irq_b;
    };
    union {
        uint32_t  slv_data_cnt;                  /*<!-save SPI Slave Data Count Register>*/
        struct {
            uint32_t  RCnt              :  10;  //0:9
            uint32_t  Reserved          :  6;  //10:15
            uint32_t  WCnt              :  10;  //16:25
            uint32_t  Reserved1         :  6;  //26:31
        } slv_data_cnt_b;
    };

    union {
        uint32_t  mst_data_cnt;                  /*<!-save SPI Master Data Count Register>*/
        struct {
            uint32_t  RdTranCnt         :  9;   //0:8
            uint32_t  Reserved          :  3;   //9:11
            uint32_t  WrTranCnt         :  9;   //12:20
            uint32_t  Reserved1         :  11;   //21:31
        } mst_data_cnt_b;
    };

}rec_spi_stat_t;

enum spi_transmode_set {
    TX_TRANSMODE_MODE           = (0),
    RX_TRANSMODE_MODE           = (1),
    TR_TRANSMODE_MODE           = (2), // TX RX at the same time

};

enum spi_timing_set {
    TIMING_SCLK_DIV             = (0),
    TIMING_CSHT                 = (1),
    TIMING_CS2SCLK              = (2),

};

enum spi_control_set {
    CONTROL_CMD                 = (0),
    CONTROL_ADDR                = (1),
    CONTROL_ADDR_FMT            = (2),
    CONTROL_DUAL_QUAD_FMT       = (3),
    CONTROL_TOKEN_EN            = (4),
    CONTROL_CS_PIN_EN           = (5),

};

// SPI Slave commands
enum spi_slave_command {
    SPI_READ_STATUS             = (0x05),
    SPI_READ_STATUS_DUAL        = (0x15),
    SPI_READ_STATUS_QUAD        = (0x25),
    SPI_READ_SINGLE             = (0x0b),
    SPI_READ_DUAL               = (0x0c),
    SPI_READ_QUAD               = (0x0e),
    SPI_WRITE_SINGLE            = (0x51),
    SPI_WRITE_DUAL              = (0x52),
    SPI_WRITE_QUAD              = (0x54),
    SPI_DUMMY                   = (0xff),
};

/* SPI verification status flag */
enum spi_ver_status_flag {
    SPI_VER_RES_PASS            = 0,
    SPI_VER_RES_FAIL            = (1ul << 0),
};

typedef enum  {
    SPI_RX_STATUS               = (0),
    SPI_TX_STATUS               = (1),
    SPI_RX_TX_STATUS            = (2),

}dump_type;


extern int tx_threshold;
extern int rx_threshold;
extern int tx_transmode;
extern int rx_transmode;
extern int tr_transmode;

extern int test_sclk_div;
extern int test_csht;
extern int test_cs2clk;

#if (SPI_TEST_MASTER_MODE == 1)
extern int mst_cmd_en;
extern int mst_adr_en;
extern int mst_adr_len;
extern int mst_adr_fmt_en;
extern int mst_dual_quad_en;
extern int mst_token_en;
extern int mst_cmd;
extern int mst_address;
extern int mst_token_value;
#endif

extern int dummy_cnt;
extern volatile int receive_cmd;

void test_case_spi_main(void);

#endif
#endif
