/*
 * Copyright 2018 @IGISTEC. All rights reserved.
 * File: test_case_config.h
 */

#ifndef _TEST_CASE_CONFIG_H_
#define _TEST_CASE_CONFIG_H_

#include <proj_config.h>

#if (CONFIG_PROJ_SEL == CONFIG_PROJ_TEST_CASE)

#define CONFIG_TEST_CASE_SEL                (TEST_CASE_OTP)
#define TEST_CASE_NONE                      (0)
#define TEST_CASE_NORMAL                    (1)
#define TEST_CASE_ALL_REG                   (2)
#define TEST_CASE_SPI                       (3)
#define TEST_CASE_OTP                       (4)


#if (CONFIG_TEST_CASE_SEL == TEST_CASE_NORMAL)
/* SRAM layout */
#define TCON_RAM_START                      (CONFIG_RISCV_RAM_START + \
                                             CONFIG_RISCV_RAM_LEN) // 0x00008000
#define TCON_RAM_LEN                        (0xA000UL) // 40KB

#define RESERVED_00_START                   (TCON_RAM_START + TCON_RAM_LEN)
#define RESERVED_00_LEN                     (0xD800UL) // 54KB

#define BG_8B_IMG_START                     (RESERVED_00_START + RESERVED_00_LEN)
#define BG_8B_IMG_LEN                       (0x10000UL) // 64KB

#define ADC_RAM_START                       (BG_8B_IMG_START + BG_8B_IMG_LEN)
#define ADC_RAM_LEN                         (0x20800UL) // 130KB

#if ((ADC_RAM_START + ADC_RAM_LEN) > CONFIG_SRAM_LEN)
#error "SRAM overflow"
#endif

/* function option */
#define WDT_RESET_ENABLE                    (0)
    #define WDT_RESET_OTHERS_MDL            (0)
    #define MXP0_OUTPUT_CLK                 (0)
#define WAIT_FOR_INT_EN                     (0)
    #define WFI_MEASURE_WAKEUP_TIME         (1)
    #define POWER_G5_OFF_EN                 (1)
    #define POWER_G3_OFF_EN                 (1)

#define SENSORTOP_FUNC_ENABLE               (1)
#define TCON_FUNC_ENABLE                    (1)
    #define I_D_CACHE_TEST_SPEED_EN         (1)
#define MDL_CLK_OFF_EN                      (0)

#define SENSORTOP_TEST_PATTERN_MODE         (SINGLE_MODE)
    #define SINGLE_MODE                     (1)
    #define RAMP_MODE                       (2)

#define SENSORTOP_READ_DATA_MODE            (NORMAL_MODE)
    #define NORMAL_MODE                     (1)
    #define BITMAP_MODE                     (2)

#define ALLREG_TEST_CASE                    (REG_RW_TEST)
    #define REG_DEFAULT_VALUE_TEST          (0)
    #define REG_RW_TEST                     (1)

/* interrupt test option */
#define SYS_RISCV_INT_TEST                  (1)
#define MTIME_INT_TEST                      (0)
#define DSP_INT_TEST                        (0)
#define SENSORTOP_INT_TEST                  (1)
#define SPI_INT_TEST                        (1)
#define TCON_INT_TEST                       (1)
    #define TCON_INT_TYPE                   (TCON_WAIT_ALL_TCON_DONE)
        #define TCON_WAIT_ALL_TCON_DONE     (0)
        #define TCON_SEPARATE_DONE          (1)

#if (WDT_RESET_ENABLE == 1)
#define RTC_INT_TEST                        (1)
#endif

/* waveform address */
#define TC_WAVE_START_ADDR1                 (TCON_RAM_START)
#define TC_WAVE_END_ADDR1                   (TCON_RAM_START+4)
#define TC_WAVE_START_ADDR2                 (TCON_RAM_START)
#define TC_WAVE_END_ADDR2                   (TCON_RAM_START+4)
#define TC_WAVE_START_ADDR3                 (TCON_RAM_START)
#define TC_WAVE_END_ADDR3                   (TCON_RAM_START+4)

/* TCON Setting */
#define TCON_TRIGGER_SRC                    (SRC_TCON_SELF)
    #define SRC_VSYNC                       (0) // select trigger src as vsync
    #define SRC_TCON_SELF                   (1) // tcon trigger by risc-v
#define TC_TCON_GLOBAL_REPEAT_TIMES         (5)
#define R1_REPEATIMES                       (0)
#define R2_REPEATIMES                       (0)
#define R3_REPEATIMES                       (0)
#define TC_TCON_GLOBAL_REGION_SIZE          (2)
#define TC_TCON_ENABLE_REGION_MAP           (0b111)
#define STEP_MODE                           (0)

#define TC_TCON_CLK_DIV                     (0x4d)

/* SensorTop setting */
#define ADC_START_ADDR                      (ADC_RAM_START)
#define ADC_CLK_DIVIDE                      (0x01) // ADC clk = 78.125 / ((ADC_CLK_DIVIDE+1)*2)

#define ADC_USE_NUM                         (8)//(16)
#define ADC_USE_CHANNEL                     (32)

#define ADC_INCREASE_OFFSET                 ((ADC_USE_NUM) * (ADC_USE_CHANNEL) * 2) // *2: 2bytes for each channel
#define ADC_EACH_ADC_INC                    ((ADC_USE_CHANNEL) * 2) // increment of ADC output

#define SENSORTOP_MODE                      (AD7699)
    #define AD7699                          (0)
    #define GENERAL_SPI                     (1)
    #define VOLTAGE_MODE                    (2)
    #define CURRENT_MODE                    (3)

#define ADC_TRIGGER_SRC                     (TRI_SRC_TCON)
    #define TRI_SRC_MCU                     (0)
    #define TRI_SRC_TCON                    (1)
    #define TRI_SRC_SENSOR                  (2)

/* debug RAM layout */
#define STATUS_FLAG                         (RESERVED_00_START) // 0x12000
#define DEBUG_WDT_CNT_RAM                   (STATUS_FLAG + 0x04)
#define DEBUG_TCON_DONE_00_RAM              (STATUS_FLAG + 0x08)
#define DEBUG_TCON_DONE_01_RAM              (STATUS_FLAG + 0x0C)
#define DEBUG_TCON_DONE_02_RAM              (STATUS_FLAG + 0x10)
#define DEBUG_TCON_OUTPUT_00_RAM            (STATUS_FLAG + 0x14)
#define DEBUG_TCON_OUTPUT_01_RAM            (STATUS_FLAG + 0x18)
#define DEBUG_TCON_OUTPUT_02_RAM            (STATUS_FLAG + 0x1C)

#define DEBUG_ADC_DONE_00_RAM               (STATUS_FLAG + 0x20)
#define DEBUG_ADC_DONE_01_RAM               (STATUS_FLAG + 0x24)
#define DEBUG_ADC_DONE_02_RAM               (STATUS_FLAG + 0x28)
#define DEBUG_ADC_DONE_03_RAM               (STATUS_FLAG + 0x2C)
#define DEBUG_ADC_DONE_04_RAM               (STATUS_FLAG + 0x30)
#define DEBUG_ADC_DONE_05_RAM               (STATUS_FLAG + 0x34)
#define DEBUG_ADC_DONE_ALL_RAM              (STATUS_FLAG + 0x38)

#define DEBUG_DSP_DONE_RAM                  (STATUS_FLAG + 0x3C)
#define DEBUG_SYS_INT_IN_RAM                (STATUS_FLAG + 0x40)

#define DEBUG_SPI00_RAM                     (STATUS_FLAG + 0x44)
#define DEBUG_SPI01_RAM                     (STATUS_FLAG + 0x48)

#define DEBUG_DSP_DATA1                     (STATUS_FLAG + 0x4C)
#define DEBUG_DSP_DATA2                     (STATUS_FLAG + 0x50)
#define DEBUG_DSP_OUTPUT                    (STATUS_FLAG + 0x54)
#define DEBUG_DSP_INSTRU                    (STATUS_FLAG + 0x58)

#define DEBUG_DSP_CAL_LEN                   (STATUS_FLAG + 0x5C)
#define DEBUG_DSP_ERR_CODE                  (STATUS_FLAG + 0x60)
#define DEBUG_DSP_FW_CHK                    (STATUS_FLAG + 0x64)
#define DEBUG_DSP_FW_ADDR_OFF               (STATUS_FLAG + 0x68)
#define DEBUG_DSP_SHIFT                     (STATUS_FLAG + 0x6C)
#define DEBUG_DSP_MATRIX_IO                 (STATUS_FLAG + 0x70)
#define DEBUG_DSP_TEST_LOOP                 (STATUS_FLAG + 0x74)

#define DEBUG_IRQ25_RAM                     (STATUS_FLAG + 0x78)
#define DEBUG_IRQ27_RAM                     (STATUS_FLAG + 0x7c)
#define DEBUG_TRIGGER_TIMES                 (STATUS_FLAG + 0x80)
#define DEBUG_TRIGGER_CMP_ANS               (STATUS_FLAG + 0x84)

#define DEBUG_FLAG_END                      (DEBUG_TRIGGER_CMP_ANS + 0x04)

#if (DEBUG_FLAG_END >= (RESERVED_00_START + RESERVED_00_LEN))
#error "DEBUG RAM overflow"
#endif

#endif

#endif
#endif

