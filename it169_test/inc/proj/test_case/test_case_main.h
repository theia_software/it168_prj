#ifndef _TEST_CASE_MAIN_H_
#define _TEST_CASE_MAIN_H_

#include <proj_config.h>

#if (CONFIG_PROJ_SEL == CONFIG_PROJ_TEST_CASE)
void test_case_main(void);
#endif
#endif
