/*
 * Copyright 2018 @EGISTEC. All rights reserved.
 * File: serial.h
 */

#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <stdlib.h>

int printf(const char *fmt, ...);
int sprintf(char *buffer, const char *fmt, ...);

char *serial_next(void);
void serial_cmd_init(void);
void serial_cmd_proc(void);
void serial_cmd_add(const char *, void(*)(void));

#endif

