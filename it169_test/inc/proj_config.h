/*
 * Copyright 2018 @EGISTEC. All rights reserved.
 * File: proj_config.h
 */

#ifndef _PROJ_CONFIG_H_
#define _PROJ_CONFIG_H_
#include <common.h>
#include <_types.h>

#define CONFIG_PROJ_SEL                         (CONFIG_PROJ_TEST_CASE)
#define CONFIG_PROJ_TEST_CASE                   (3)

#define CONFIG_FPGA                             (1)
#if (CONFIG_FPGA == 0)
#define CONFIG_ASIC_V                           (0)
#endif

/* IT169 Global SRAM */
#define CONFIG_SRAM_START                       (0x00000000)
#define CONFIG_SRAM_LEN                         (0x00060000) /* 384KB */

/* memory layout */
#define CONFIG_RISCV_ROM_START                  (0x30000000)
#define CONFIG_RISCV_ROM_LEN                    (0x8000) /* 32KB */

#define CONFIG_RISCV_ROM_STACK_START            (0x8000)

#endif

