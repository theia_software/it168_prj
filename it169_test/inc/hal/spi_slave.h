/*
 * Copyright 2018 @EGISTEC. All rights reserved.
 * File: spi_slave.h
 */

#ifndef _SPI_SLAVE_H_
#define _SPI_SLAVE_H_

#include <proj_config.h>

/* Definition */
#define SPIS_REG_BASE_ADDR                  (0x20000000UL)
#define RW_SPIS_REG(offset)                 (*((volatile U32 *)(SPIS_REG_BASE_ADDR + (offset))))

#define SPI_SLAVE_DATE                      (0x00)
#define SPI_SLAVE_VERSION                   (0x04)
#define SPI_SLAVE_POS_EDGE_SEL              (0x08)
#define SPI_SLAVE_BASE_ADDR                 (0x0C)
#define SPI_SLAVE_INTR_CLR                  (0x10)
#define SPI_SLAVE_DUMMY_01                  (0x14)
#define SPI_SLAVE_DUMMY_02                  (0x18)
#define SPI_SLAVE_CMD_63_32                 (0x1c)
#define SPI_SLAVE_CMD_31_00                 (0x20)

#endif

