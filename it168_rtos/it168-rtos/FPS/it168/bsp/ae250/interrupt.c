/*
 * Copyright (c) 2012-2017 Andes Technology Corporation
 * All rights reserved.
 *
 */

#include <stdio.h>
#include <isr.h>
#include "platform.h"

typedef void (*isr_func)(void);

void default_irq_handler(void)
{
	printf("Default interrupt handler\n");
}


void wdt_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void rtc_period_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void rtc_alarm_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void i2c0_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void i2c1_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void uart0_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void uart1_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void dma_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void fifo_overflow_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void usb_reqirq_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void usb_reqfiq_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void usb_frametoggle_handler(void) __attribute__((weak, alias("default_irq_handler")));
void spi0_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void spi1_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void spi2_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void can_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void lin_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void sys25_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void sys27_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void iapdone_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void sys29_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));
void spislave_irq_handler(void) __attribute__((weak, alias("default_irq_handler")));

isr_func irq_handler[] = {
	default_irq_handler,	//0
	wdt_irq_handler,		//1
	rtc_period_irq_handler,	//2
	rtc_alarm_irq_handler,	//3
	i2c0_irq_handler,		//4
	i2c1_irq_handler,		//5
	uart0_irq_handler,		//6
	uart1_irq_handler,		//7
	dma_irq_handler,		//8
	fifo_overflow_irq_handler,	//9
	default_irq_handler,	//10
	usb_reqirq_irq_handler,	//11
	usb_reqfiq_irq_handler,	//12
	usb_frametoggle_handler,	//13
	default_irq_handler,	//14
	default_irq_handler,	//15
	spi0_irq_handler,		//16
	spi1_irq_handler,		//17
	spi2_irq_handler,		//18
	can_irq_handler,		//19
	lin_irq_handler,		//20
	default_irq_handler,	//21
	default_irq_handler,	//22
	default_irq_handler,	//23
	default_irq_handler,	//24
	sys25_irq_handler,		//25
	default_irq_handler,	//26
	sys27_irq_handler,		//27
	iapdone_irq_handler,	//28
	sys29_irq_handler,		//29
	spislave_irq_handler	//30
};


void mext_interrupt(unsigned int irq_source)
{
	/* Enable interrupts in general to allow nested */
	set_csr(NDS_MSTATUS, MSTATUS_MIE);

	/* Do interrupt handler */
	if(irq_handler[irq_source])
		irq_handler[irq_source]();

	__nds__plic_complete_interrupt(irq_source);
}
//--------------------------------------------------
void registerISR(unsigned int irq_source, void (*fun)())
{
	irq_handler[irq_source] = fun;

	isr_SetupIrq(irq_source);
}
