#ifndef USB_DEVICE_H
#define USB_DEVICE_H

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

typedef struct{
	TaskHandle_t classtask;

	TaskHandle_t ctrltask;

	QueueHandle_t xQueue;
}TUSBDCfg;

int usbd_Init(void);

#endif
