#include <usbd_msc.h>
#include <ae250.h>
#include <_types.h>
#include "usbd.h"

#define MSG_CONTROL_CMD	( 100UL )

static U8 usb_isr_no[3] = {IRQ_USB_WAKEUPIRQ_SOURCE, IRQ_USB_USBIRQ_SOURCE, IRQ_USB_FRAMETOGGLE_SOURCE};
TUSBDCfg pUSBDCfg;

U32 isrDebug = 0;
//------------------------------------------------------------------
void usbd_IOISR(void)
{
	const uint32_t msg = MSG_CONTROL_CMD;

	isrDebug++;

	*((volatile U32*)0x00030000) = isrDebug;
	xQueueSendFromISR( pUSBDCfg.xQueue, &msg, 0U );
}
//------------------------------------------------------------------
void usbd_CtrlTsk(void)
{
	uint32_t ulReceivedValue;
	const TickType_t xBlockTime = pdMS_TO_TICKS( 2000UL );	//2000ms

	while(1){
		if(xQueueReceive( pUSBDCfg.xQueue, &ulReceivedValue, xBlockTime ) == pdPASS){

			if(ulReceivedValue == MSG_CONTROL_CMD){
				printf("ctrl task: %d\r\n", ulReceivedValue);
				mscIsr();
			}
		}
	}
}
//------------------------------------------------------------------
int usbd_Init(void)
{
	BaseType_t ret_class = 0, ret_ctrl = 0;

	*((volatile U32*)0x00030000) = 0;
	memset(&pUSBDCfg, 0, sizeof(TUSBDCfg));

	pUSBDCfg.xQueue = xQueueCreate( 10, sizeof( uint32_t ) );

	registerISR(IRQ_USB_USBIRQ_SOURCE, usbd_IOISR);

	ret_class = xTaskCreate( mscApp, "USB Class", 1024, NULL, tskIDLE_PRIORITY + 1, &pUSBDCfg.classtask );
	if( ret_class != pdPASS )
	{
		return -1;
	}

	ret_ctrl = xTaskCreate( usbd_CtrlTsk, "USB Ctrl", 1024, NULL, tskIDLE_PRIORITY + 1, &pUSBDCfg.ctrltask );
	if( ret_ctrl != pdPASS )
	{
		goto usbd_Init_Exit;
	}

	return 0;

usbd_Init_Exit:
	if(ret_class == pdPASS)
		vTaskDelete( pUSBDCfg.classtask );

	if(ret_ctrl == pdPASS)
		vTaskDelete( pUSBDCfg.ctrltask );

	return -1;
}
//------------------------------------------------------------
