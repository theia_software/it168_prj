//---------------------------------------------------------------------
// \file   eeprom.c
// \brief  eeprom read/write function for CAT24AA01TDI-GT3
//---------------------------------------------------------------------
#include "eeprom.h"

#define EEPROM_ADDRESS		0x50
#define EEPROM_MAX_CAPACITY	256


//---------------------------------------------------------
S32 eeprom_WriteData(TI2CId ch, U32 addr, U32 buf, U32 count)
{
	U32 i;
	U8	_buf[32], *ptr;

	memset(_buf, 0, 32);

	ptr = (U8*)buf;

	if(count > 16)
		return -1;

	if((addr+count)> EEPROM_MAX_CAPACITY)
		return -1;

	_buf[0] = addr;
	for(i=0;i<count;i++)
		_buf[1+i]=ptr[i];

	iic_Master_Dma_TxData(ch, EEPROM_ADDRESS, _buf, (count+1));

	return 0;
}
//---------------------------------------------------------
S32 eeprom_ReadData(TI2CId ch, U32 addr, U32 buf, U32 count)
{
	if((addr+count)> EEPROM_MAX_CAPACITY)
		return -1;

	eeprom_WriteData(ch, addr, 0, 0);

	iic_Master_Dma_RxData(ch, EEPROM_ADDRESS, buf, count);

	return 0;
}
