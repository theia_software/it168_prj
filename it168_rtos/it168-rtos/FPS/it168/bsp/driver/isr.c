#include "isr.h"
#include "uart.h"
#include "dma.h"

#define	SYSTEM_HW_TRIG_TYPE					(0x320)
#define SYSTEM_SW_TRIG_TYPE					(0x324)
#define TRIG_TYPE_EDGE						(0xffffffff)

void isr_Init(void)
{

	/* Disable the Machine external, timer and software interrupts until setup is done */
	clear_csr(NDS_MIE, MIP_MEIP | MIP_MTIP | MIP_MSIP);

	RW_SYS_REG(SYSTEM_HW_TRIG_TYPE) = TRIG_TYPE_EDGE;

	/* Enable the Machine External/Timer/Sofware interrupt bit in MIE. */
	set_csr(NDS_MIE, MIP_MEIP | MIP_MTIP | MIP_MSIP);
	/* Enable interrupts in general. */
	set_csr(NDS_MSTATUS, MSTATUS_MIE);

}
//--------------------------------------------
void isr_SetupIrq(unsigned int irq_source)
{
	/* Priority must be set > 0 to trigger the interrupt */
	__nds__plic_set_priority(irq_source, 1);

	__nds__plic_enable_interrupt(irq_source);
}
//--------------------------------------------
void isr_Disable(void)
{
	clear_csr(NDS_MSTATUS, MSTATUS_MIE);
}
//--------------------------------------------
void isr_Enable(void)
{
	set_csr(NDS_MSTATUS, MSTATUS_MIE);
}
