#ifndef _TS_ET510_H_
#define _TS_ET510_H_

#include "spi.h"

#define ET510D_PIXEL_SIZE	103*52

void et510d_Init(TSPIID spi_id);
int et510d_Type_Check(void);
void et510d_Stus(void);
void et510d_Scan_Csr(void);
void et510d_Scan_Frames(U8 num);
void et510d_Dma_GetFrame(U32 buf);
#endif
