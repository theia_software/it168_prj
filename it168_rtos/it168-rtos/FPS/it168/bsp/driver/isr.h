#ifndef _ISR_H_
#define _ISR_H_

#include "_types.h"
#include <platform.h>


void isr_Init(void);
void isr_SetupIrq(unsigned int irq_source);
void isr_Disable(void);
void isr_Enable(void);

#endif
